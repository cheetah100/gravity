BUILD:
mvn clean package -DskipTests

DEVELOPMENT RUN: 
mvn spring-boot:run

DEPLOYED RUN:
java -jar gravity.jar -Dspring.config.location=application.yml &

DOCKER BUILD IMAGE:
docker build -f Dockerfile -t devcentre/gravity .

DOCKER RUN IMAGE:
docker run -p 8080:8080 devcentre/gravity

DOCKER COMPOSE:
docker-compose up
