/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.services.HttpCallService;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class SsoLogonActionTest {
	
	@Autowired
	private HttpCallService httpCallTool;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
	
	@BeforeEach
	public void before() throws Exception {
	}

	/**
	 * The clientId and clientSecret being used in this test are that of the NaasDemoKavita
	 * application.
	 * In the staging environment for this test to run successfully the userID mentioned in the 
	 * NAAS URL needs to have a user profile created in the staging environment for the 
	 * Spark/Webex account. 
	 * @throws Exception
	 */
	@Test
	@Disabled
	public void testSssoLogin() throws Exception {
		
		Credential credential = new Credential();
		credential.setId("cred_one");
		credential.setIdentifier("a6e72344609441daa9c19dddaf44801f");
		credential.setSecret("b9CeB9d717AF4416B8F84e250fB07717");
		credential.setResource("https://cloudsso-test.cisco.com/as/token.oauth2");
		
		String token = this.httpCallTool.getSsoToken(credential);
		assertNotNull(token);
		
		System.out.println("Token is " + token);
		String body = "{" +
		    "\"integration\": \"FORCED_NO_CUSTOMIZATION\"," +
		    "\"template\": \"HelloSparkTemplate\"," +
		    "\"interestType\": \"general\"," +
		    "\"minUserAccessLevel\": \"GUEST\"," +
		    "\"changeObject\": {" +
		        "\"changeType\": \"NONE\"," +
		        "\"objectType\": \"icsNotif\"," +
		        "\"data\": {}" +
		    "}," +
		    "\"deliveryMethods\": [" +
		        "{" +
		            "\"type\": \"SPARK\"" +
		        "}" +
		    "]" +
		"}";
		
		String naasUrl = "https://apmx-stage.cisco.com/devlab/apicem/notifications/v1/notification/user/rajdixit/";
		
		Map<String,String> headers= new HashMap<>();
		
		headers.put("Authorization", "Bearer " + token);
		headers.put("Content-Type", "application/json");

		String result = this.httpCallTool.execute(body, null, naasUrl, "POST", headers,null, 1, 1).getBody();
		System.out.print(result);
	}
}
