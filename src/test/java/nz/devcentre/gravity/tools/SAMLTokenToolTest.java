package nz.devcentre.gravity.tools;

import static org.mockito.Mockito.*;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.services.HttpCallService;

@ExtendWith(MockitoExtension.class)
public class SAMLTokenToolTest {

	@InjectMocks
	private SAMLTokenTool samlToken;
	
	@Mock
	private HttpCallService tool;
	
	@Mock
	private DocumentBuilderFactory documentBuilderFactory;
	
	@Test
	public void testGetToken() throws Exception {
		
		Credential testCredential = getTestCredential();
		
		when(tool.execute(any(String.class), 
				any(), 
				any(String.class), 
				any(String.class), 
				any(Map.class), 
				any(), any(), any())).thenReturn(new CallResponse("<testTag>myToken</testTag>"));
		
		when(documentBuilderFactory.newDocumentBuilder()).thenReturn(DocumentBuilderFactory.newInstance().newDocumentBuilder());
		
		String token = samlToken.getSamlToken(testCredential);
				
		Assertions.assertEquals("myToken",token);
	}
	
	@Test
	public void testGetTokenException() throws Exception {
		Credential testCredential = getTestCredential();

		CallResponse response = new CallResponse("<testTag myToken</testTag>");

		when(tool.execute(any(String.class), 
				any(), 
				any(String.class), 
				any(String.class), 
				any(Map.class), 
				any(), any(), any())).thenReturn(response);
		
		when(samlToken.getSamlToken(testCredential)).thenThrow(new Exception());
		
		Assertions.assertThrows(Exception.class, () -> {
			samlToken.getSamlToken(testCredential);
		}, "Exception was expected");
		
	}
	
	
	private Credential getTestCredential() {
		Credential credential = new Credential();
		credential.setMethod("oauth_saml");
		credential.setRequestMethod("GET");
		credential.getResponseFields().put("samlTokenRequestBody", "test body");
		credential.getResponseFields().put("samlTokenUrl", "testUrl");
		credential.getResponseFields().put("samlTokenTag", "testTag");
		credential.getResponseFields().put("samlTokenUrlMethodType", "testMethod");
		credential.setId("test_credential");
		credential.setMethod("oauth_saml");
		credential.setIdentifier("testUser");
		credential.setSecret("testPassword");
		credential.setResource("https://cloudsso-test.cisco.com/as/token.oauth2");
		return credential;
	}
	

}
