/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.TeamController;
import nz.devcentre.gravity.controllers.UserController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.transfer.Interval;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class IntervalToolsTest {

	public static String BOARD_ID = "fiscal_months";

	public static String PHASE_ID = "test_phase";
	public static String TEMPLATE_ID = "fiscal_months";
	public static String GROUP_ID = "test_group";

	@Configuration
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Autowired
	private CalendarBoardTool calendarBoardTool;

	@Autowired
	private IntervalTools quarterTools;

	@Autowired
	private BoardController boardController;

	@Autowired
	private UserController userController;

	@Autowired
	private TeamController teamController;

	@BeforeEach
	public void setUp() throws Exception {
		try {

			Board newBoard = boardController.getBoard("fiscal_months");

		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Board in Exception");
		}
		try {

			userController.getUser("system");

		} catch (Exception e) {

			User systemUser = new User();
			systemUser.setName("system");
			systemUser.setFirstname("System");
			systemUser.setFirstname("Administrator");
			userController.createUser(systemUser);
		}

		try {
			teamController.getTeam("administrators");
		} catch (Exception e) {

			Map<String, String> members = new HashMap<String, String>();
			members.put("system", "ADMIN");

			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);
			teamController.createTeam(team);
		}

		calendarBoardTool.initTestBoard();

	}

	@Test
	public void testGetFiscalData() throws Exception

	{

		GregorianCalendar calendar = new GregorianCalendar(2016, 10, 03);
		// Getting Date object
		Date date = calendar.getTime();

		Interval interval = quarterTools.getInterval("fiscal_months", date);

		assertEquals(0, interval.getDaysElapsed());

	}

	@Test
	public void testGetFiscalDataforQuarter() throws Exception

	{
		GregorianCalendar calendar = new GregorianCalendar(2016, 10, 03);
		// Getting Date object
		Date date = calendar.getTime();

		Interval interval = quarterTools.getInterval("quarter", date);

		assertEquals(0, interval.getDaysElapsed());

	}

	@Test
	public void testGetFiscalDataForUnknown() throws Exception

	{
		GregorianCalendar calendar = new GregorianCalendar(2018, 04, 30);
		// Getting Date object
		Date date = calendar.getTime();

		Interval interval = quarterTools.getInterval(" unknownBoard", date);

		assertEquals(0, interval.getDaysElapsed());

	}
}
