/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import com.google.gson.Gson;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.controllers.TransformCache;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.TransformChain;
import nz.devcentre.gravity.repository.BoardResourceRepository;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TransformToolTest {

	@Autowired
	TransformController transformController;

	@Autowired
	TransformCache transformCache;

	@Autowired
	TestBoardTool tool;

	@Autowired
	ResourceController resourceController;

	@Autowired
	private BoardController boardController;

	@Autowired
	private BoardResourceRepository boardResourceRepository;

	public static final String RESOURCE_ID = "bydr";

	public static final String RESOURCE_BODY = "[\n    {\n        \"$match\": {\n            \"$and\": [\n                !#asg_id, \n                !#quarter, \n                !#kpi\n            ] \n        }\n    },\n    {\n        \"$lookup\": {\n            \"from\": \"metric_status\",\n            \"localField\": \"status\",\n            \"foreignField\": \"_id\",\n            \"as\": \"metric_status\"\n        }\n    },\n    {\n        \"$unwind\": \"$metric_status\"\n    },\n    {\n        \"$lookup\": {\n            \"from\": \"kpi\",\n            \"localField\": \"kpi\",\n            \"foreignField\": \"_id\",\n            \"as\": \"kpi_actual\"\n        }\n    },    \n    {\n        \"$unwind\": \"$kpi_actual\"\n    },\n    {\n        \"$addFields\": {\n            \"status_order\": \"$metric_status.order\",\n            \"category_id\":\"$kpi_actual.metric_category\"\n        }\n    },\n    {\n        \"$group\": { \"_id\": { \n            \"category\":\"$category_id\", \n            \"quarter\":\"$quarter\" \n            },\n            \"order\": {$max: \"$status_order\" } \n        }\n    },\n    {\n        \"$lookup\": {\n            \"from\": \"metric_status\",\n            \"localField\": \"order\",\n            \"foreignField\": \"order\",\n            \"as\": \"metric_status\"\n        }\n    },\n    {\n        \"$unwind\": \"$metric_status\"\n    },\n    {\n        \"$addFields\": {\n            \"category\": \"$_id.category\",\n            \"quarter\": \"$_id.quarter\",\n            \"metric_status\":\"$metric_status._id\",\n            \"_id\": {$concat: [ \"$_id.category\", \"-\", \"$_id.quarter\" ]}\n        }\n    }\n]"

			+ "[{ \"$match\" : { \"$and\" : [ { \"$or\" : [ { \"metadata.phase\" : \"test_phase\"} , { \"metadata.phase\" : \"next_phase\"}]}]}}]";

	public static final String TRANSFORM_BODY = "{\n"
			+ "    \"_id\" : \"resiliency_apps_bydr_ce9a755b-581b-4745-92e8-4743d3cf9a5c\",\n"
			+ "    \"_class\" : \"nz.devcentre.gravity.model.TransformChain\",\n" + "    \"transforms\" : {\n"
			+ "        \"b\" : {\n" + "            \"transformer\" : \"resource\",\n" + "            \"order\" : 1,\n"
			+ "            \"configuration\" : {\n" + "                \"board\" : \"test_board\",\n"
			+ "                \"resource\" : \"bydr\"\n" + "            }\n" + "        }\n" + "    },\n"
			+ "    \"id\" : \"resiliency_apps_bydr\",\n"
			+ "    \"version\" : \"ce9a755b-581b-4745-92e8-4743d3cf9a5c\",\n"
			+ "    \"name\" : \"Resilinecy Applications By DR\",\n" + "    \"active\" : true,\n"
			+ "    \"publish\" : false,\n" + "    \"metadata\" : {\n" + "        \"creator\" : \"peterjha\",\n"
			+ "        \"created\" : \"Apr 12, 2012 11:56:04 AM\",\n" + "        \"modifiedby\" : \"bmi_nprod_dev\",\n"
			+ "        \"modified\" : \"Apr 12, 2012 11:56:04 AM\"\n" + "    }\n" + "}";

	public static final String PIVOT_COST_LANDSCAPE = "{\n" + "    \"_id\" : \"pivot_cost_landscape\",\n"
			+ "    \"_class\" : \"nz.devcentre.gravity.model.TransformChain\",\n" + "    \"transforms\" : {\n"
			+ "        \"a\" : {\n" + "            \"transformer\" : \"pivot\",\n" + "            \"order\" : 0,\n"
			+ "            \"configuration\" : {\n" + "                \"board\" : \"cost_elements\",\n"
			+ "                \"group\" : [],\n" + "                \"lookup\" : [],\n"
			+ "                \"xaxis\" : \"service_id\",\n" + "                \"yaxis\" : \"quarter\",\n"
			+ "                \"total\" : \"value\"\n" + "            }\n" + "        }\n" + "    },\n"
			+ "    \"type\" : \"pivot\",\n" + "    \"name\" : \"pivot_cost_landscape\",\n" + "    \"active\" : false,\n"
			+ "    \"publish\" : false,\n" + "    \"metadata\" : {\n" + "        \"creator\" : \"peterjha\",\n"
			+ "        \"created\" : \"Apr 12, 2012 11:56:04 AM\",\n" + "        \"modifiedby\" : \"bmi_nprod_dev\",\n"
			+ "        \"modified\" : \"Apr 12, 2012 11:56:04 AM\"\n" + "    }\n" + "}";

	public static final String BOARD_ID = "test_board";

	public static final String COST_ELEMENTS_BOADRD_ID = "cost_elements";

	public static final String COST_TRANSFORMER = "pivot_cost_landscape";

	public static final String TRANSFORM_ID = "resiliency_apps_bydr";

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void setUp() throws Exception {

		// create test board]
		this.tool.initTestBoard();

		Board costElementsBoard = TestBoardTool.getTestBoard(COST_ELEMENTS_BOADRD_ID, BoardType.OBJECT);

		boardController.createBoard(costElementsBoard);
		// create board resource
		this.resourceController.createResourceByJsonString(BOARD_ID, RESOURCE_ID, RESOURCE_BODY, null, "txt");

		String resource = this.resourceController.getResource(BOARD_ID, RESOURCE_ID);

		this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(BOARD_ID, resource);

		Gson gson = new Gson();
		TransformChain transformChain = gson.fromJson(TRANSFORM_BODY, TransformChain.class);

		// create transform

		transformController.createTransformChain(transformChain, false);

		TransformChain transformChainCost = gson.fromJson(PIVOT_COST_LANDSCAPE, TransformChain.class);

		System.out.println("Transform Chain cost=" + transformChainCost);

		// create transform

		transformController.createTransformChain(transformChainCost,false);

	}

	@Test
	public void transformParameterTestByTransformId() throws Exception {

		Map<String, String> parameters = transformController.listTransformParameters(TRANSFORM_ID);
		assertEquals(3, parameters.size());
	}

	// no paramters exists in the given tranform and output is empty
	@Test
	public void transformParameterTestByTransformIdFailure() throws Exception {

		Map<String, String> parameters = transformController.listTransformParameters(COST_TRANSFORMER);
		assertEquals(0, parameters.size());
	}

	@Test
	public void UpdatetransformParameterTestByTransformId() throws Exception {

		TransformChain transformChain = transformController.updateTransformParameters(TRANSFORM_ID);

		assertEquals("{kpi=, asg_id=, quarter=}", transformChain.getParameters().toString());

	}

	// no parameter to update
	@Test
	public void UpdatetransformParameterTestByTransformIdFailure() throws Exception {

		TransformChain transformChain = transformController.updateTransformParameters(COST_TRANSFORMER);

		assertEquals("{}", transformChain.getParameters().toString());

	}

	@Test
	public void UpdatetransformParameterTestForAll() throws Exception {

		transformController.updateAllTransformParameters();
		assertTrue(true);

	}

}
