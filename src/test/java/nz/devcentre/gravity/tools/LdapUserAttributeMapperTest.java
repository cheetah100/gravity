/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ldap.core.NameAwareAttributes;

@ExtendWith(MockitoExtension.class)
public class LdapUserAttributeMapperTest {

	
	LdapUserAttributesMapper ldapUserAttributeMapper = new LdapUserAttributesMapper();
	
	@Test
	public void mapFromAttributesTest() throws Exception{
		NameAwareAttributes attrs = new NameAwareAttributes();
		attrs.put("attr1", "val1");
		attrs.put("attr2", "val2");
		attrs.put("attr3", "val3");
		
		Map<String, Object> testMap = new HashMap<String, Object>();
		testMap.put("attr1", "val1");
		testMap.put("attr2", "val2");
		testMap.put("attr3", "val3");
		Map<String, Object> userMap = ldapUserAttributeMapper.mapFromAttributes(attrs);
		System.out.println(userMap);
		Assertions.assertEquals(testMap,userMap);
	}
	
	@Test
	public void getManagerTest() {
		Assertions.assertEquals("jpavlick", ldapUserAttributeMapper.getManager( "CN=jpavlick,OU=Employees,OU=Cisco Users,DC=cisco,DC=com"));
		Assertions.assertEquals("jpavlick", ldapUserAttributeMapper.getManager( "CN=jpavlick"));
		Assertions.assertEquals("", ldapUserAttributeMapper.getManager(""));
	}
	
	@Test
	public void parseLdapTimestampTest() {
		Assertions.assertEquals("2020-10-04 23:42:53", ldapUserAttributeMapper.parseLdapTimestamp( "20201004234253.0Z"));
	}
	
	@Test
	public void parseLdapTimestampTestException() {
		Assertions.assertEquals(null, ldapUserAttributeMapper.parseLdapTimestamp( "20201005e064253.0Z"));
	}
	
}
