package nz.devcentre.gravity.tools;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.repository.BatchRepository;
import nz.devcentre.gravity.services.IntegrationService;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration(locations = { "/test-controllers.xml" })
public class IntegrationServiceTest {
	
	@InjectMocks
	IntegrationService integrationService;
	
	@Mock
	BatchRepository batchRepository;
	
	@Test
	public void testCreateAndUpdateBatch() {
		Batch batch = new Batch();
		batch.setNotificationId("test_integration");
		
		Batch batchResponse = new Batch();
		batchResponse.setNotificationId("test_integration");
		batchResponse.setId("aaa");
		
		when(batchRepository.insert(any(Batch.class))).thenReturn(batchResponse);
		Integration integration = new Integration();
		integration.setId("test_integration");
		
		Batch createdBatch = this.integrationService.createBatch(integration, 1, new Date());
		
		assertNotNull(createdBatch);
		assertNotNull( createdBatch.getId());
		assertEquals(createdBatch.getNotificationId(), "test_integration");
		verify(this.batchRepository).insert(any(Batch.class));
		
		integrationService.updateBatch(createdBatch);
		verify(this.batchRepository).save(createdBatch);
	}

	@Test
	public void testGetLatestBatch() {

		Batch batchResponse = new Batch();
		batchResponse.setNotificationId("test_integration");
		batchResponse.setId("aaa");

		when(this.batchRepository.findByNotificationIdOrderByMaxProcessDateDesc(any())).thenReturn(batchResponse);
		Batch latestBatch = integrationService.getLatestBatch(getTestIntegration("test","mongo"));
		assertNotNull(latestBatch);
		verify(this.batchRepository).findByNotificationIdOrderByMaxProcessDateDesc(any());
	}
	
	private Integration getTestIntegration( String value, String connector ) {
		Integration integration = new Integration();
		integration.setId(value);
		integration.setDescription(value);
		integration.setConnector(connector);
		return integration; 
	}

}
