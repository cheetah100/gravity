/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;

@ExtendWith(MockitoExtension.class)
public class FilterToolsTest {
		
	@Mock
	TestBoardTool testBoardTool;
	
	@Test
	public void testCreateMapFilter() throws Exception {
		Map<String,String> map = new HashMap<>();
		map.put("name", "CONTAINS:something");
		map.put("age", "GREATERTHAN:30");
		map.put("city", "Auckland");
		map.put("comment", "My city is : AWESOME");

		Map<String,String> values = new HashMap<>();
		values.put("name", "something");
		values.put("age", "30");
		values.put("city", "Auckland");
		values.put("comment", "My city is : AWESOME");		
	}
		
	@Test
	public void testReplaceFilterValues() {
		
		Map<String,Condition> conditions = new HashMap<>();
		conditions.put("cond1", new Condition( "name", Operation.EQUALTO, "peter"));
		conditions.put("cond2", new Condition( "eye_color", Operation.EQUALTO, "green"));
		conditions.put("cond3", new Condition( "car", Operation.EQUALTO, "honda"));
		Filter filter = new Filter();
		filter.setConditions(conditions);
		
		Map<String,Object> fieldMap = new HashMap<>();
		fieldMap.put("name", "jim");
		fieldMap.put("eye_color", "pink");
		fieldMap.put("car", "tesla");
		fieldMap.put("suburb", "sunnynook");
		
		Filter newFilter = FilterTools.replaceFilterValues(filter, fieldMap);
		
		assertEquals( "jim", newFilter.getConditions().get("cond1").getValue() );
		assertEquals( "pink", newFilter.getConditions().get("cond2").getValue() );
		assertEquals( "tesla", newFilter.getConditions().get("cond3").getValue() );
	}
	
	@Test
	public void testCreateSimpleFilter() {
		Filter filter = FilterTools.createSimpleFilter("name", Operation.BEFORE, "myvalue");
		assertEquals( "dynamic", filter.getName());
		assertEquals( "myvalue", filter.getConditions().get("a").getValue());
	}	
}
