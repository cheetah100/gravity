package nz.devcentre.gravity.tools;

/** GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.FilterController;
import nz.devcentre.gravity.controllers.RuleController;
import nz.devcentre.gravity.controllers.TeamController;
import nz.devcentre.gravity.controllers.UserController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AccessType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.repository.CardEventRepository;

public class CalendarBoardTool {

	public static String BOARD_ID = "fiscal_months";
	public static String BOARD_ID_UPD = "test_board_upd"; // Using a separate board for any updates to ensure other
															// tests don't break
	public static String PHASE_ID = "test_phase";
	public static String TEMPLATE_ID = "test_board";
	public static String GROUP_ID = "test_group";
	public static String TASK_ID = "test_task";
	public static String TASK_ID2 = "test_task_2";
	public static String TASK_ID3 = "test_task_3";
	public static String TASK_ID4 = "test_task_4";
	public static String TASK_ID5 = "test_task_5";
	public static String RULE_ID = "test_rule";
	public static String RULE_ID2 = "test_rule_2";
	public static String TEST_TEMPLATE_NAME = "test_board";

	@Autowired
	private BoardController controller;

	@Autowired
	private CardController cardController;

	@Autowired
	CardEventRepository cardEventRepository;

	@Autowired
	private RuleController ruleController;

	@Autowired
	private FilterController filterController;

	@Autowired
	private UserController userController;

	@Autowired
	private TeamController teamController;

	public void initTestBoard() throws Exception {

		try {
			controller.getBoard(BOARD_ID);
			return;
		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Board1");
		}

		try {
			System.out.println("before user");
			userController.getUser("system");
		} catch (Exception e) {
			System.out.println("exception in user");
			User systemUser = new User();
			systemUser.setName("system");
			systemUser.setFirstname("System");
			systemUser.setFirstname("Administrator");
			userController.createUser(systemUser);
		}

		try {
			System.out.println("before Team");
			teamController.getTeam("administrators");
		} catch (Exception e) {
			System.out.println("exeption in team ");
			Map<String, String> members = new HashMap<String, String>();
			members.put("system", "ADMIN");

			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);
			teamController.createTeam(team);
		}

		Board board = controller.createBoard(getTestBoard("fiscal_months"));
		System.out.println(" board id name =" + board.getName());

		Card card = cardController.createCard(BOARD_ID,
				getTestCard(BOARD_ID, "FM000002", "20171", "2016-08-28", "2016-09-27"), true);
		System.out.println("card start Date =" + card.getField("start_date"));
		System.out.println("card end Date =" + card.getField("end_date"));
		card = cardController.createCard(BOARD_ID,
				getTestCard(BOARD_ID, "FM000003", "20172", "2016-09-28", "2016-10-27"), true);
		System.out.println("card start Date =" + card.getField("start_date"));
		System.out.println("card end Date =" + card.getField("end_date"));
		card = cardController.createCard(BOARD_ID,
				getTestCard(BOARD_ID, "FM00004", "20173", "2016-10-28", "2016-11-27"), true);
		System.out.println("card start Date =" + card.getField("start_date"));
		System.out.println("card end Dattruee =" + card.getField("end_date"));
		card = cardController.createCard(BOARD_ID,
				getTestCard(BOARD_ID, "FM000005", "20174", "2016-11-28", "2016-12-27"), true);
		System.out.println("card start Date =" + card.getField("start_date"));
		System.out.println("card end Date =" + card.getField("end_date"));
		card = cardController.createCard(BOARD_ID,
				getTestCard(BOARD_ID, "FM000006", "20175", "2016-12-28", "2017-01-27"), true);
		System.out.println("card start Date =" + card.getField("start_date"));
		System.out.println("card end Date =" + card.getField("end_date"));

	}

	public void initTestUpdBoard() throws Exception {

		try {
			controller.getBoard(BOARD_ID_UPD);
			return;
		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Update Board");
		}

		controller.createBoard(getTestBoard(BOARD_ID_UPD));

		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "FM000002", "20171", "2016-08-28", "2016-09-27"),
				true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "FM000003", "20172", "2016-09-28", "2016-10-27"),
				true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "FM000004", "20173", "2016-10-28", "2016-11-27"),
				true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "FM000005", "20174", "2016-11-28", "2016-12-27"),
				true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "FM000006", "20175", "2016-12-28", "2017-01-27"),
				true);

	}

	public Card addFieldBouquet(Card card) {
		Map<String, Object> fields = card.getFields();
		fields.put("booleanField", true);
		fields.put("integerField", Integer.valueOf(1));
		fields.put("longField", Long.valueOf(100));
		fields.put("doubleField", Double.valueOf(200.20));
		List<String> list = new ArrayList<String>();
		list.add("one");
		list.add("two");
		list.add("three");
		fields.put("listField", list);
		return card;
	}

	public void generateFilters() throws Exception {
		try {
			filterController.getFilter(BOARD_ID, "test_filter");

		} catch (ResourceNotFoundException e) {
			Filter equalsfilter = this.getFilter("test_filter", "metadata.creator", Operation.EQUALTO, "system");
			filterController.createFilter(BOARD_ID, equalsfilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "test_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getTestPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "replacementFilter");

		} catch (ResourceNotFoundException e) {
			Filter replacementfilter = this.getReplacementTestFilter();
			filterController.createFilter(BOARD_ID, replacementfilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "next_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getNextPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}
		try {
			filterController.getFilter(BOARD_ID, "test_phase_next_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getTestPhaseNextPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}
	}

	public void generateRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = this.getTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);
	}

	public void generateBrokenRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = this.getBrokenTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);
	}

	public void generateTaskRuleWithConditions(String boardId, String ruleName, String fieldName, Operation operation,
			String value, boolean task, RuleType ruleType) throws Exception {
		Rule rule = null;
		if (task) {
			rule = this.getTaskRuleWithTaskConditions(ruleName, fieldName, operation, value, ruleType);
		} else {
			rule = this.getTaskRuleWithAutomationConditions(ruleName, fieldName, operation, value, ruleType);
		}
		ruleController.createRule(boardId, rule);
	}

	public Board getTestBoard(String name) throws Exception {
		Map<String, Phase> phases = new HashMap<String, Phase>();
		phases.put("test_phase", getTestPhase("Test Phase", 1));
		phases.put("next_phase", getTestPhase("Next Phase", 2));

		Phase archivePhase = getTestPhase("Archive", 3);
		archivePhase.setInvisible(true);
		phases.put("archive", archivePhase);

		Map<String, View> views = new HashMap<String, View>();
		views.put("testview", getTestView("Test View"));

		Map<String, String> roles = new HashMap<String, String>();
		roles.put("test_user", "WRITE");

		Filter equalsfilter = this.getFilter("test_filter", "metadata.creator", Operation.EQUALTO, "system");

		Board board = new Board();
		board.setName(name);
		board.setBoardType(BoardType.OBJECT);
		board.setPhases(phases);
		board.setViews(views);
		board.setOrderField("name");
		board.setPermissions(roles);
		return board;
	}

	public View getTestView(String name) {
		View view = new View();
		view.setName(name);
		// view.setAccess(AccessType.WRITE);

		Map<String, ViewField> fields = new HashMap<String, ViewField>();
		fields.put("name", getTestViewField("name", 50, 0));
		fields.put("phone", getTestViewField("phone", 50, 1));

		view.setFields(fields);
		return view;
	}

	public ViewField getTestViewField(String name, int length, int index) {
		ViewField vf = new ViewField();
		vf.setIndex(index);
		// vf.setLength(length);
		vf.setName(name);
		return vf;
	}

	public Phase getTestPhase(String name, Integer index) {
		Phase phase = new Phase();
		phase.setDescription(name);
		phase.setIndex(index);
		phase.setName(name);
		try {
			phase.setId(IdentifierTools.getIdFromNamedModelClass(phase));
		} catch (Exception ignore) {

		}
		return phase;
	}

	public Rule getTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}

	// Get a non-working test rule for error testing
	public Rule getBrokenTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		// Create actions that will fail upon execution
		Map<String, Action> actions = new HashMap<>();
		Action action = new Action();
		action.setOrder(1);
		action.setType("script");
		actions.put("action", action);
		rule.setActions(actions);
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}

	// Get a test rule to check condition evaluation
	public Rule getTaskRuleWithTaskConditions(String name, String fieldName, Operation operation, String value,
			RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		Map<String, Condition> taskConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		taskConditions.put("testCondition", condition);
		rule.setTaskConditions(taskConditions);
		return rule;
	}

	// Get a test rule to check condition evaluation
	public Rule getTaskRuleWithAutomationConditions(String name, String fieldName, Operation operation, String value,
			RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		Map<String, Condition> automationConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		automationConditions.put("testCondition", condition);
		rule.setAutomationConditions(automationConditions);
		return rule;
	}

	public Filter getFilter(String name, String fieldName, Operation operation, String value) {
		Condition condition = new Condition();
		condition.setConditionType(ConditionType.PROPERTY);
		condition.setFieldName(fieldName);
		condition.setValue(value);
		condition.setOperation(operation);
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("a", condition);

		Map<String, String> permissions = new HashMap<String, String>();
		permissions.put("test_user", "test_role");

		Filter filter = new Filter();
		filter.setName(name);
		filter.setAccess(AccessType.READ);
		filter.setOwner("system");
		filter.setConditions(conditions);
		filter.setPermissions(permissions);
		filter.setPhase(PHASE_ID);
		return filter;
	}

	private Filter getTestPhaseNextPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("test_phase_next_phase_filter");
		filter.setId("test_phase_next_phase_filter");
		filter.setPhase("test_phase|next_phase");

		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");

		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);

		return filter;
	}

	private Filter getTestPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("test_phase_filter");
		filter.setId("test_phase_filter");
		filter.setPhase("test_phase");

		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");

		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);

		return filter;
	}

	private Filter getReplacementTestFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("replacementFilter");
		filter.setId("replacementFilter");
		filter.setPhase("test_phase");

		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Tim");

		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);

		return filter;
	}

	private Filter getNextPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("next_phase_filter");
		filter.setId("next_phase_filter");
		filter.setPhase("next_phase");

		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");

		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);

		return filter;
	}

	public static String getIdFromPath(String input) {
		int i = input.lastIndexOf("/");
		if (i > -1) {
			return input.substring(i + 1);
		}
		return "";
	}

	public static Card getTestCard(String template, String name, String quarter, String startDate, String endDate) {

		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", name);
		fields.put("quarter", quarter);
		fields.put("start_date", startDate);
		fields.put("end_date", endDate);

		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}

	public Card getTestCard() throws Exception {
		Collection<Card> cards = cardController.getCards(CalendarBoardTool.BOARD_ID, null, null, null, null, false,
				false, false);
		return cards.iterator().next();
	}
	
	/*
	public Template createTemplate(String boardId) throws Exception {
		return templateController.createTemplate(boardId, getTestTemplate(boardId));
		return null;
	}
	*/

	public static TemplateField getTestTemplateField(String name, FieldType type) {
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(type);
		return templateField;
	}

	/**
	 * Create a dummy optionlist field in the template- no actual board
	 * 
	 * @param name
	 * @param index
	 * @return
	 */
	public static TemplateField getTestTemplateFieldOptionList(String name) {
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(FieldType.LIST);
		templateField.setOptionlist("owner_board");
		return templateField;
	}
}
