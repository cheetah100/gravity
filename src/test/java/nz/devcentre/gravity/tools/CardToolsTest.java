package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class CardToolsTest {

	@Test
	public void testOrderCards() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateModified() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetNumberFromObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBooleanFromObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testCardToMap() {
		fail("Not yet implemented");
	}

	@Test
	public void testMongoDBObjectToCard() {
		fail("Not yet implemented");
	}

	@Test
	public void testCardToMongoDBObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testCardsToList() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCardFieldChanges() {
		fail("Not yet implemented");
	}

	@Test
	public void testValidateParameters() {
		fail("Not yet implemented");
	}

	@Test
	public void testCorrectCardFieldTypes() {
		fail("Not yet implemented");
	}

	@Test
	public void testCorrectFieldType() {
		fail("Not yet implemented");
	}

	@Test
	public void testFieldToStringWithString() throws Exception {
		String fieldToString = CardTools.fieldToString("TestMe",null);
		assertEquals("TestMe", fieldToString);
	}
	
	@Test
	public void testFieldToStringWithNumber() throws Exception {
		Number n = new Double( 10967l);
		String fieldToString = CardTools.fieldToString(n,null);
		assertEquals("10967", fieldToString);
	}
	
	@Test
	public void testFieldToStringWithLargeNumber() throws Exception {
		Number n = new Double( 4042675945665l);
		String fieldToString = CardTools.fieldToString(n,null);
		assertEquals("4042675945665", fieldToString);
	}

	@Test
	public void testFieldToStringWithDecimal() throws Exception {
		Number n = new Double( 4042675.945665);
		String fieldToString = CardTools.fieldToString(n, "#.#");
		assertEquals("4042675.945665", fieldToString);
	}
	
	@Test
	public void testFieldToStringException() throws Exception {
		Assertions.assertThrows(Exception.class, () -> {
			CardTools.fieldToString( new Date(),null);
		}, "Exception was expected");
	}

}
