package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.Credential;

public class JWTTokenToolTest {

	@Test
	public void testMakeJwtToken() throws UnsupportedEncodingException {
		Credential credential = new Credential();
		credential.setIdentifier("test");
		Map<String,String> responseFields = new HashMap<>();
		responseFields.put("jwtSubject", "test");
		responseFields.put("jwtName", "test");
		responseFields.put("jwtExpireTime", "5000");
		credential.setResponseFields(responseFields);
		credential.setSecret("lhdksjhgskjgfhkshgkshfhgkskgshkfgksgfskdfgksdskgfhkshfdgshkgfksgshk4355353454353534543fd");
		String token = JWTTokenTool.makeJwtToken(credential);
		assertNotNull(token);
	}
	
	@Test
	public void testCalculateExpiry() throws UnsupportedEncodingException {
		long expected = System.currentTimeMillis() + 5000;
		Date expiryDate = JWTTokenTool.calculateExpiryTime("5000");
		long expiryTime = expiryDate.getTime();
		long difference = Math.abs(expiryTime - expected);
		System.out.print(difference);
		assertTrue(difference < 1000);
	}

}
