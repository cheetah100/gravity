package nz.devcentre.gravity.tools;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.naming.directory.SearchControls;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Disabled;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ldap.control.PagedResultsDirContextProcessor;
import org.springframework.ldap.core.LdapOperations;

import nz.devcentre.gravity.services.LdapOperationsCallbackImpl;

@ExtendWith(MockitoExtension.class)
public class LdapOperationsCallbackImplTest {

	@InjectMocks
	LdapOperationsCallbackImpl ldapOperationsCallbackImpl;

	@Mock
	LdapOperations operations;

	@Test
	@Disabled
	public void doWithLdapOperationsTest() {
		List<Map<String, Object>> oneResult = new LinkedList<Map<String, Object>>();
		when(operations.search(anyString(), anyString(), any(SearchControls.class), any(LdapUserAttributesMapper.class),
				any(PagedResultsDirContextProcessor.class))).thenReturn(oneResult);
		// when(processor.hasMore()).thenReturn(false);
		List<Map<String, Object>> totalResult = ldapOperationsCallbackImpl.doWithLdapOperations(operations);

		assertEquals(oneResult, totalResult);
	}

}
