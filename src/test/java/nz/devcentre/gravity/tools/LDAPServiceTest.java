package nz.devcentre.gravity.tools;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ldap.core.support.LdapContextSource;

import nz.devcentre.gravity.services.LdapService;


@ExtendWith(MockitoExtension.class)
public class LDAPServiceTest {

	@InjectMocks
	LdapService ldapservice;
	
	@Mock
	LdapContextSource contextSourceMock;
	
	
	/*
	 * TODO Why Test NullPointerException ?
	 */
	@Test
	public void getAllUserTest() { 
		Assertions.assertThrows(NullPointerException.class, () -> {
			List<Map<String,Object>> userlist =  ldapservice.getAllUsers("cn=iu", contextSourceMock);
			Assertions.assertEquals(0,userlist.size());
		}, "Exception was expected");
	}
	  
	 
}
