/** GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.FilterController;
import nz.devcentre.gravity.controllers.RuleController;
import nz.devcentre.gravity.controllers.TeamController;
import nz.devcentre.gravity.controllers.UserController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AccessType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.repository.CardEventRepository;

public class MetricfierTestTool {

	public static String BOARD_ID = "test_source_board";
	public static String BOARD_ID_UPD = "test_board_upd"; // Using a separate board for any updates to ensure other
															// tests don't break	
	public static String RESOLUTION_BOARD = "m2e_aggregation_resolutions";
	public static String QUARTER_BOARD = "quarter";
	public static String FISCAL_MONTHS_BOARD = "fiscal_months";
	public static String COST_BOARD = "cost";
	public static String REVENUE_BOARD = "revenue";
	public static String TARGET_BOARD_ID = "test_target_board";
	public static String PHASE_ID = "test_phase";
	public static String TEMPLATE_ID = "test_board";
	public static String GROUP_ID = "test_group";
	public static String TASK_ID = "test_task";
	public static String TASK_ID2 = "test_task_2";
	public static String TASK_ID3 = "test_task_3";
	public static String TASK_ID4 = "test_task_4";
	public static String TASK_ID5 = "test_task_5";
	public static String RULE_ID = "test_rule";
	public static String RULE_ID2 = "test_rule_2";
	//public static String CARD_ID = "TE000003";
	public static String TEST_TEMPLATE_NAME = "test_board";

	@Autowired
	private BoardController controller;

	@Autowired
	private CardController cardController;

	@Autowired
	CardEventRepository cardEventRepository;

	@Autowired
	private RuleController ruleController;

	@Autowired
	private FilterController filterController;

	@Autowired
	private UserController userController;

	@Autowired
	private TeamController teamController;

	public void initTestBoard() throws Exception {

		try {
			controller.getBoard(BOARD_ID);
			return;
		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Board1");
		}

		try {
			
			userController.getUser("system");
		} catch (Exception e) {
			
			User systemUser = new User();
			systemUser.setName("system");
			systemUser.setFirstname("System");
			systemUser.setFirstname("Administrator");
			userController.createUser(systemUser);
		}

		try {
			
			teamController.getTeam("administrators");
		} catch (Exception e) {
			
			Map<String, String> members = new HashMap<String, String>();
			members.put("system", "ADMIN");
			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);
			teamController.createTeam(team);
		}

		Board board = controller.createBoard(getTestBoard(BOARD_ID));
		//Template testTemplate = getTestTemplate(BOARD_ID);
		//templateController.createTemplate(BOARD_ID, testTemplate);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000001",
				"Individual Contributor",10,1000,10,"Non-Binary","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "IT Software Engineer", "APJC", "2020-02-10", "INDIA","WO000002","Individual Contributor",
				10,5000,10,"Woman","INDIA", "Asian","WO000012"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "AMER", "2020-02-10", "UNITED STATES","WO000003","Individual Contributor",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
	
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Director - People Leader", "APJC", "2020-02-10", "INDIA","WO000004","Individual Contributor",
				10,5000,10,"Man","INDIA", "Asian","WO000012"),true);
	
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000005","Individual Contributor",
				10,1000,10,"Man","United States of America", "Asian","WO000012"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000006","Individual Contributor",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Renewals Manager", "AMER", "2020-02-10", "UNITED STATES","WO000007","Individual Contributor",
				10,1000,10,"Woman","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Leader, Software Engineerin", "AMER", "2020-02-10", "UNITED STATES","WO000008","Manager - People Leader",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Leader, Technical Systems Engineering", "AMER", "2020-02-10", "UNITED STATES","WO000009","Manager - People Leader",
				10,1000,10,"Woman","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO00011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian", "WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "CANADA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);

		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "APJC", "2020-02-10", "AUSTRALIA","WO000010","Individual Contributo",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);

		
		board = controller.createBoard(getTestBoard(RESOLUTION_BOARD));
		//testTemplate = getTestTemplateForResolutions(RESOLUTION_BOARD);
		//templateController.createTemplate(RESOLUTION_BOARD, testTemplate);
		cardController.createCard(RESOLUTION_BOARD, getResolutionCard(RESOLUTION_BOARD, "daily","Daily",1.0));
		cardController.createCard(RESOLUTION_BOARD, getResolutionCard(RESOLUTION_BOARD, "seven","7 Days",7.0));
		cardController.createCard(RESOLUTION_BOARD, getResolutionCard(RESOLUTION_BOARD, "thirty","30 Days",30.0));
		cardController.createCard(RESOLUTION_BOARD, getResolutionCard(RESOLUTION_BOARD, "quarter","90 Days",90.0));
		cardController.createCard(RESOLUTION_BOARD, getResolutionCard(RESOLUTION_BOARD, "year","356 Days",30.0));
		
		board = controller.createBoard(getTestBoard(QUARTER_BOARD));
		//testTemplate = setTestTemplateForQuarter(QUARTER_BOARD);
		//templateController.createTemplate(QUARTER_BOARD, testTemplate);
		cardController.createCard(QUARTER_BOARD,setQuarterCard(QUARTER_BOARD,"20211","2021-02-28", "2021-05-29","Q4 FY2021",2021.0));
		
		board = controller.createBoard(getTestBoard(FISCAL_MONTHS_BOARD));
		//testTemplate = setTestTemplateForFiscalMonths(FISCAL_MONTHS_BOARD);
        //templateController.createTemplate(FISCAL_MONTHS_BOARD, testTemplate);
 	    cardController.createCard(FISCAL_MONTHS_BOARD,setFiscalMonthsCard(FISCAL_MONTHS_BOARD,"20193","2019-02-24", "2019-01-27","Q3FY19 Feb"));
 	    
 	    board = controller.createBoard(getTestBoard(REVENUE_BOARD));
		//testTemplate = setTestTemplateForRevenue(REVENUE_BOARD);
        //templateController.createTemplate(REVENUE_BOARD, testTemplate);
	    cardController.createCard(REVENUE_BOARD,setRevenueCard(REVENUE_BOARD,"AMER","Dan", "UNITED STATES",1000000,"United States","active", "2019-01-27"));
	    cardController.createCard(REVENUE_BOARD,setRevenueCard(REVENUE_BOARD,"AMER","Joe", "UNITED STATES", 900000,"United States","active", "2020-01-27"));
	    cardController.createCard(REVENUE_BOARD,setRevenueCard(REVENUE_BOARD,"AMER","Chirs", "UNITED STATES",8000000,"United States","active", "2021-01-27"));
	    cardController.createCard(REVENUE_BOARD,setRevenueCard(REVENUE_BOARD,"APJC","Krish", "INDIA",8000000,"India","active", "2021-01-27"));
	    cardController.createCard(REVENUE_BOARD,setRevenueCard(REVENUE_BOARD,"APJC","Ram", "INDIA",7000000,"India","active", "2021-01-27"));
	    
	    board = controller.createBoard(getTestBoard(COST_BOARD));
		//testTemplate = setTestTemplateForRevenue(COST_BOARD);
        //templateController.createTemplate(COST_BOARD, testTemplate);
	    cardController.createCard(COST_BOARD,setCostCard(COST_BOARD,"AMER","Dan", "UNITED STATES",700000,"United States","active_cost", "2019-01-27"));
	    cardController.createCard(COST_BOARD,setCostCard(COST_BOARD,"AMER","Dan", "UNITED STATES",600000,"United States","active_cost", "2020-01-27"));
	    cardController.createCard(COST_BOARD,setCostCard(COST_BOARD,"AMER","Dan", "UNITED STATES",500000,"United States","active_cost", "2021-01-27"));
	    cardController.createCard(COST_BOARD,setCostCard(COST_BOARD,"APJC","Krish", "INDIA",400000,"India","active_cost", "2021-01-27"));
	    cardController.createCard(COST_BOARD,setCostCard(COST_BOARD,"APJC","RAm", "INDIA",300000,"India","active_cost", "2021-01-27"));

	}
     	
	
	public void initTestUpdBoard() throws Exception {

		try {
			controller.getBoard(BOARD_ID_UPD);
			return;
		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Update Board");
		}
		controller.createBoard(getTestBoard(BOARD_ID_UPD));
    	//Template testTemplate = getTestTemplate(BOARD_ID_UPD);
		//templateController.createTemplate(BOARD_ID_UPD, testTemplate);
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000001",
				"Individual Contributor",10,1000,10,"Non-Binary","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "IT Software Engineer", "APJC", "2020-02-10", "INDIA","WO000002","Individual Contributor",
				10,5000,10,"Woman","INDIA", "Asian","WO000012"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Business Analyst", "AMER", "2020-02-10", "UNITED STATES","WO000003","Individual Contributor",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
	
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Director - People Leader", "APJC", "2020-02-10", "INDIA","WO000004","Individual Contributor",
				10,5000,10,"Man","INDIA", "Asian","WO000012"),true);
	
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000005","Individual Contributor",
				10,1000,10,"Man","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Software Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000006","Individual Contributor",
				10,1000,10,"Woman","United States of America", "Asian","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Renewals Manager", "AMER", "2020-02-10", "UNITED STATES","WO000007","Individual Contributor",
				10,1000,10,"Woman","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Leader, Software Engineerin", "AMER", "2020-02-10", "UNITED STATES","WO000008","Manager - People Leader",
				10,1000,10,"Man","United States of America", "Asian","WO000012"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Leader, Technical Systems Engineering", "AMER", "2020-02-10", "UNITED STATES","WO000009","Manager - People Leader",
				10,1000,10,"Woman","United States of America", "White","WO000011"),true);
		
		cardController.createCard(BOARD_ID, getTestCard(BOARD_ID, "Data Engineer", "AMER", "2020-02-10", "UNITED STATES","WO000010","Individual Contributo",
				10,1000,10,"Woman","United States of America", "Asian","WO000012"),true);
	

	}
	
	
		
	public Card addFieldBouquet(Card card) {
		Map<String, Object> fields = card.getFields();
		fields.put("booleanField", true);
		fields.put("integerField", new Integer(1));
		fields.put("longField", new Long(100));
		fields.put("doubleField", new Double(200.20));
		List<String> list = new ArrayList<String>();
		list.add("one");
		list.add("two");
		list.add("three");
		fields.put("listField", list);
		return card;
	}

	public void generateFilters() throws Exception {
		try {
			filterController.getFilter(BOARD_ID, "test_filter");

		} catch (ResourceNotFoundException e) {
			Filter equalsfilter = this.getFilter("test_filter", "metadata.creator", Operation.EQUALTO, "system");
			filterController.createFilter(BOARD_ID, equalsfilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "test_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getTestPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "replacementFilter");

		} catch (ResourceNotFoundException e) {
			Filter replacementfilter = this.getReplacementTestFilter();
			filterController.createFilter(BOARD_ID, replacementfilter);
		}

		try {
			filterController.getFilter(BOARD_ID, "next_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getNextPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}
		try {
			filterController.getFilter(BOARD_ID, "test_phase_next_phase_filter");

		} catch (ResourceNotFoundException e) {
			Filter phasefilter = this.getTestPhaseNextPhaseFilter();
			filterController.createFilter(BOARD_ID, phasefilter);
		}
	}

	public void generateRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = this.getTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);
	}

	public void generateBrokenRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = this.getBrokenTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);
	}

	public void generateTaskRuleWithConditions(String boardId, String ruleName, String fieldName, Operation operation,
			String value, boolean task, RuleType ruleType) throws Exception {
		Rule rule = null;
		if (task) {
			rule = this.getTaskRuleWithTaskConditions(ruleName, fieldName, operation, value, ruleType);
		} else {
			rule = this.getTaskRuleWithAutomationConditions(ruleName, fieldName, operation, value, ruleType);
		}
		ruleController.createRule(boardId, rule);
	}

	public Board getTestBoard(String name) throws Exception {
		Map<String, Phase> phases = new HashMap<String, Phase>();
		phases.put("test_phase", getTestPhase("Test Phase", 1));
		phases.put("next_phase", getTestPhase("Next Phase", 2));
		Phase archivePhase = getTestPhase("Archive", 3);
		archivePhase.setInvisible(true);
		phases.put("archive", archivePhase);
		Map<String, View> views = new HashMap<String, View>();
		views.put("testview", getTestView("Test View"));
		Map<String, String> roles = new HashMap<String, String>();
		roles.put("test_user", "WRITE");
		Filter equalsfilter = this.getFilter("test_filter", "metadata.creator", Operation.EQUALTO, "system");
		Board board = new Board();
		board.setName(name);
		board.setBoardType(BoardType.OBJECT);
		board.setPhases(phases);
		board.setViews(views);
		board.setOrderField("name");
		board.setPermissions(roles);
		return board;
	}

	public View getTestView(String name) {
		View view = new View();
		view.setName(name);
		Map<String, ViewField> fields = new HashMap<String, ViewField>();
	    fields.put("name", getTestViewField("name", 50, 0));
		fields.put("phone", getTestViewField("phone", 50, 1));
		view.setFields(fields);
		return view;
	}

	public ViewField getTestViewField(String name, int length, int index) {
		ViewField vf = new ViewField();
		vf.setIndex(index);
		vf.setName(name);
		return vf;
	}

	public Phase getTestPhase(String name, Integer index) {
		Phase phase = new Phase();
		phase.setDescription(name);
		phase.setIndex(index);
		phase.setName(name);
		try {
			phase.setId(IdentifierTools.getIdFromNamedModelClass(phase));
		} catch (Exception ignore) {

		}
		return phase;
	}

	public Rule getTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}

	// Get a non-working test rule for error testing
	public Rule getBrokenTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		// Create actions that will fail upon execution
		Map<String, Action> actions = new HashMap<>();
		Action action = new Action();
		action.setOrder(1);
		action.setType("script");
		actions.put("action", action);
		rule.setActions(actions);
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}

	// Get a test rule to check condition evaluation
	public Rule getTaskRuleWithTaskConditions(String name, String fieldName, Operation operation, String value,
			RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		Map<String, Condition> taskConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		taskConditions.put("testCondition", condition);
		rule.setTaskConditions(taskConditions);
		return rule;
	}

	// Get a test rule to check condition evaluation
	public Rule getTaskRuleWithAutomationConditions(String name, String fieldName, Operation operation, String value,
			RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		Map<String, Condition> automationConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		automationConditions.put("testCondition", condition);
		rule.setAutomationConditions(automationConditions);
		return rule;
	}

	public Filter getFilter(String name, String fieldName, Operation operation, String value) {
		Condition condition = new Condition();
		condition.setConditionType(ConditionType.PROPERTY);
		condition.setFieldName(fieldName);
		condition.setValue(value);
		condition.setOperation(operation);
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("a", condition);
		Map<String, String> permissions = new HashMap<String, String>();
		permissions.put("test_user", "test_role");
		Filter filter = new Filter();
		filter.setName(name);
		filter.setAccess(AccessType.READ);
		filter.setOwner("system");
		filter.setConditions(conditions);
		filter.setPermissions(permissions);
		filter.setPhase(PHASE_ID);
		return filter;
	}

	private Filter getTestPhaseNextPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("test_phase_next_phase_filter");
		filter.setId("test_phase_next_phase_filter");
		filter.setPhase("test_phase|next_phase");
		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);
		return filter;
	}

	private Filter getTestPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("test_phase_filter");
		filter.setId("test_phase_filter");
		filter.setPhase("test_phase");
		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);
		return filter;
	}

	private Filter getReplacementTestFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("replacementFilter");
		filter.setId("replacementFilter");
		filter.setPhase("test_phase");
		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Tim");
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);
		return filter;
	}

	private Filter getNextPhaseFilter() throws Exception {
		Filter filter = new Filter();
		filter.setName("next_phase_filter");
		filter.setId("next_phase_filter");
		filter.setPhase("next_phase");
		Condition condition = new Condition();
		condition.setFieldName("name");
		condition.setOperation(Operation.EQUALTO);
		condition.setValue("Bob");
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("test_condition", condition);
		filter.setConditions(conditions);
		return filter;
	}

	public static String getIdFromPath(String input) {
		int i = input.lastIndexOf("/");
		if (i > -1) {
			return input.substring(i + 1);
		}
		return "";
	}

	
	public Card getTestCard(String boardId,String publicJobTitle  ,String theaterCd, String hireDt, String regionCd, String supervisorWorkerId,  String role, float gradeLevel, 
			float baseSalary, float yearsInJobCareerLevel, String gender, String workCountryName, String ethnicOriginName, String vpId) {

		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("public_job_title", publicJobTitle);
		fields.put("theater_cd", theaterCd);
		fields.put("hire_dt", hireDt);
		fields.put("region_cd", regionCd);
		fields.put("worker_id", supervisorWorkerId);
		fields.put("role", role );
		fields.put("grade_level",gradeLevel );
		fields.put("base_salary", baseSalary);
		fields.put("years_in_job_career_level", yearsInJobCareerLevel );
		fields.put("gender", gender );
		fields.put("work_country_name", workCountryName);
		fields.put("ethnic_origin_name",ethnicOriginName );
		fields.put("vp_id", vpId);
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}

	public Card setQuarterCard(String boardId,String code  ,String endDate, String startDate, String name, double fiscalYear) throws Exception {
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("code", code);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		fields.put("endDate", format.parse(endDate));
		fields.put("startDate", format.parseObject(startDate));
		fields.put("name", name);		
		fields.put("fiscalYear", fiscalYear);
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);		
		return newCard;		
	}
	
	public Card setFiscalMonthsCard(String boardId,String quarter ,String endDate, String startDate, String name) throws Exception {
	
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("quarter", quarter);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		fields.put("end_date", format.parse(endDate));
		fields.put("start_date", format.parseObject(startDate));
		fields.put("name", name);
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}
		   
	public Card setRevenueCard(String boardId,String theater_cd,String leader, String region_cd, int  revenue , String workCountryName, String validity, String entry_date) throws Exception {
		
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("theater_cd", theater_cd);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		fields.put("entry_date", format.parse(entry_date));
		fields.put("leader", leader);
		fields.put("region_cd", region_cd);
		fields.put("revenue", revenue);
		fields.put("workCountryName", workCountryName);
		fields.put("validity", validity);
		
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}
	
	
public Card setCostCard(String boardId,String theater_cd,String leader, String region_cd, int cost , String workCountryName, String validity, String entry_date) throws Exception {
		
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("theater_cd", theater_cd);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		fields.put("entry_date", format.parse(entry_date));
		fields.put("leader", leader);
		fields.put("region_cd", region_cd);
		fields.put("cost", cost);
		fields.put("workCountryName", workCountryName);
		fields.put("validity", validity);
		
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}
	public Card getResolutionCard(String boardId,String id  ,String name, double period)
	{
		Map<String, Object> fields = new HashMap<String, Object>();
	    fields.put("_id", id);
	    fields.put("name", name);
	    fields.put("period", period);
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}
	
	public Card getTestCard() throws Exception {
		Collection<Card> cards = cardController.getCards(MetricfierTestTool.BOARD_ID, null, null, null, null, false,
				false, false);
		return cards.iterator().next();
	}

	/*
	public Template createTemplate(String boardId) throws Exception {
		return templateController.createTemplate(boardId, getTestTemplate(boardId));
	}
	*/

	public static TemplateField getTestTemplateField(String name, FieldType type) {
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(type);
		return templateField;
	}

	/**
	 * Create a dummy optionlist field in the template- no actual board
	 * 
	 * @param name
	 * @param index
	 * @return
	 */
	public static TemplateField getTestTemplateFieldOptionList(String name) {
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(FieldType.LIST);
		templateField.setOptionlist("owner_board");
		return templateField;
	}

	/*
	public static Template getTestTemplate() {
		return getTestTemplate(TEMPLATE_ID);
	}
	*/

	/*
	public static Template getTestTemplate(String templateName) {
		// Set Groups and Fields

		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("public_job_title", MetricfierTestTool.getTestTemplateField("public_job_title", 1, FieldType.STRING));
		fields1.put("theater_cd", MetricfierTestTool.getTestTemplateField("theater_cd", 2, FieldType.STRING));
		fields1.put("hire_dt", MetricfierTestTool.getTestTemplateField("hire_dt", 3, FieldType.DATE));
		fields1.put("region_cd", MetricfierTestTool.getTestTemplateField("region_cd", 4, FieldType.STRING));
		fields1.put("role", MetricfierTestTool.getTestTemplateField("role", 5, FieldType.STRING));
		fields1.put("worker_id", MetricfierTestTool.getTestTemplateField("worker_id", 6, FieldType.STRING));
		fields1.put("grade_level", MetricfierTestTool.getTestTemplateField("grade_level", 7, FieldType.NUMBER));
		fields1.put("base_salary", MetricfierTestTool.getTestTemplateField("base_salary", 8, FieldType.NUMBER));
		fields1.put("years_in_job_career_level", MetricfierTestTool.getTestTemplateField("years_in_job_career_level", 9, FieldType.NUMBER));
		fields1.put("gender", MetricfierTestTool.getTestTemplateField("gender", 10, FieldType.STRING));
		fields1.put("work_country_name", MetricfierTestTool.getTestTemplateField("work_country_name", 11, FieldType.STRING));
		fields1.put("ethnic_origin_name", MetricfierTestTool.getTestTemplateField("ethnic_origin_name", 12, FieldType.STRING));
		fields1.put("vp_id", MetricfierTestTool.getTestTemplateField("vp_id", 13, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
	    cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
	}
	*/

	/*
	public static Template getTestTemplateForResolutions(String templateName) {
		// Set Groups and Fields
		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("name", MetricfierTestTool.getTestTemplateField("_id", 1, FieldType.STRING));
		fields1.put("name", MetricfierTestTool.getTestTemplateField("name", 1, FieldType.STRING));
		fields1.put("period", MetricfierTestTool.getTestTemplateField("period", 2, FieldType.NUMBER));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
	}
	*/
	
	/*
	public static Template setTestTemplateForQuarter(String templateName) {
		
		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("_id", MetricfierTestTool.getTestTemplateField("_id", 1, FieldType.STRING));
		fields1.put("code", MetricfierTestTool.getTestTemplateField("code", 1, FieldType.STRING));
		fields1.put("endDate", MetricfierTestTool.getTestTemplateField("endDate", 2, FieldType.DATE));
		fields1.put("startDate", MetricfierTestTool.getTestTemplateField("startDate", 3, FieldType.DATE));
		fields1.put("name", MetricfierTestTool.getTestTemplateField("name", 4, FieldType.STRING));
		fields1.put("fiscalYear", MetricfierTestTool.getTestTemplateField("fiscalYear", 5, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Quarter Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("quarter", group1);
		template.setGroups(groups);
		return template;
		
	}
	*/

	/*
	public static Template setTestTemplateForFiscalMonths(String templateName) {
		
		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("_id", MetricfierTestTool.getTestTemplateField("_id", 1, FieldType.STRING));
		fields1.put("name", MetricfierTestTool.getTestTemplateField("name", 2, FieldType.STRING));
		fields1.put("end_date", MetricfierTestTool.getTestTemplateField("end_date", 3, FieldType.DATE));
		fields1.put("start_date", MetricfierTestTool.getTestTemplateField("start_date", 4, FieldType.DATE));
		fields1.put("title", MetricfierTestTool.getTestTemplateField("title", 5, FieldType.STRING));
		fields1.put("quarter", MetricfierTestTool.getTestTemplateField("quarter", 6, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
		
	}
	*/

	/*
	public static Template setTestTemplateForCost(String templateName) {
		
		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("_id", MetricfierTestTool.getTestTemplateField("_id", 1, FieldType.STRING));
		fields1.put("region_cd", MetricfierTestTool.getTestTemplateField("region_cd", 2, FieldType.STRING));
		fields1.put("work_country_name", MetricfierTestTool.getTestTemplateField("work_country_name", 3, FieldType.STRING));
		fields1.put("entry_date", MetricfierTestTool.getTestTemplateField("entry_date", 4, FieldType.DATE));
		fields1.put("cost", MetricfierTestTool.getTestTemplateField("cost", 5, FieldType.NUMBER));
		fields1.put("validity", MetricfierTestTool.getTestTemplateField("validity", 6, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
		
	}
	*/
	
	/*
	public static Template setTestTemplateForRevenue(String templateName) {
		
		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("_id", MetricfierTestTool.getTestTemplateField("_id", 1, FieldType.STRING));
		fields1.put("region_cd", MetricfierTestTool.getTestTemplateField("region_cd", 2, FieldType.STRING));
		fields1.put("work_country_name", MetricfierTestTool.getTestTemplateField("work_country_name", 3, FieldType.STRING));
		fields1.put("entry_date", MetricfierTestTool.getTestTemplateField("entry_date", 4, FieldType.DATE));
		fields1.put("revenue", MetricfierTestTool.getTestTemplateField("revenue", 5, FieldType.NUMBER));
		fields1.put("validity", MetricfierTestTool.getTestTemplateField("validity", 6, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
		
	}
	
	public void initTargetBoard() throws Exception {
		
		try {
			controller.getBoard(TARGET_BOARD_ID);
			return;
		} catch (ResourceNotFoundException e) {
			System.out.println("Creating new Test Board2");
		}

		try {
			
			userController.getUser("system");
		} catch (Exception e) {
			System.out.println("exception in user");
			User systemUser = new User();
			systemUser.setName("system");
			systemUser.setFirstname("System");
			systemUser.setFirstname("Administrator");
			userController.createUser(systemUser);
		}

		try {
			
			teamController.getTeam("administrators");
		} catch (Exception e) {
			System.out.println("exeption in team ");
			Map<String, String> members = new HashMap<String, String>();
			members.put("system", "ADMIN");
			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);
			teamController.createTeam(team);
		}

		Board board = controller.createBoard(getTestBoard(TARGET_BOARD_ID));
		System.out.println(" board id name =" + board.getName());
		Template testTemplate = getTargetTestTemplate(TARGET_BOARD_ID);
		templateController.createTemplate(TARGET_BOARD_ID, testTemplate);
	}
	*/
	
	/*
	public static Template getTargetTestTemplate(String templateName) {
		// Set Groups and Fields

		Map<String, TemplateField> fields1 = new HashMap<String, TemplateField>();
		fields1.put("metric", MetricfierTestTool.getTestTemplateField("metric", 1, FieldType.STRING));		
		fields1.put("value", MetricfierTestTool.getTestTemplateField("value", 2, FieldType.STRING));
		fields1.put("resolution", MetricfierTestTool.getTestTemplateField("resolution", 3, FieldType.STRING));
		fields1.put("entity", MetricfierTestTool.getTestTemplateField("entity", 4, FieldType.STRING));
		Template template = new Template();
		template.setName(templateName);
		List<String> cardTitleFields = new ArrayList<String>();
		cardTitleFields.add("name");
		cardTitleFields.add("Fiscal Months");
		template.setCardTitle(cardTitleFields);
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		TemplateGroup group1 = new TemplateGroup();
		group1.setName("Fiscal Months  Details");
		group1.setFields(fields1);
		group1.setIndex(1);
		groups.put("Fiscal Months", group1);
		template.setGroups(groups);
		return template;
	}
	*/

	
}
