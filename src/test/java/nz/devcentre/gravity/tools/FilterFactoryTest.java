/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;

public class FilterFactoryTest {

	@Test
	public void testSingleConditionFilter() {
		Filter filter = FilterFactory.singleConditionFilter("name", Operation.EQUALTO, "James Bond");
		Map<String, Condition> conditions = filter.getConditions();
		assertTrue(conditions.size()==1);
		assertTrue(conditions.containsKey("a"));
		Condition condition = conditions.get("a");
		assertEquals("name", condition.getFieldName());
		assertEquals("James Bond", condition.getValue());
		assertEquals(Operation.EQUALTO, condition.getOperation());

	}
	@Test
	public void testStringMapFilter() {
		Map<String,String> map=new HashMap<String, String>();
		map.put("name","James");
		map.put("code","007");
		Filter filter = FilterFactory.stringMapFilter(map);
		Map<String, Condition> conditions = filter.getConditions();
		assertTrue(conditions.size()==2);
		Condition condition = conditions.get("code");
		assertEquals("code", condition.getFieldName());
		assertEquals("007", condition.getValue());
		assertEquals(Operation.EQUALTO, condition.getOperation());
		
	}
}
