/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.DBObject;

import nz.devcentre.gravity.model.PivotType;

public class MongoQueryBuilderUtilityTest {
	
	@Test
	public void testBuildSortQuery() throws JsonProcessingException {
		Map<String, String> fields = new LinkedHashMap<>();
		fields.put("name", "1");
		fields.put("balance", "-1");
		fields.put("age", "-1");
		String sortQuery = MongoQueryBuilderUtility.buildSortQuery(fields);
		assertEquals("{ \"$sort\" : { \"name\" : \"1\", \"balance\" : \"-1\", \"age\" : \"-1\" } }", sortQuery);
	}

	@Test
	public void testBuildAddFieldString() {
		Map<String, String> fields = new LinkedHashMap<>();
		fields.put("name", "id.name");
		fields.put("balance", "balance");
		fields.put("age", "id.age");
		String addFieldString = MongoQueryBuilderUtility.buildAddFieldString(fields);
		assertEquals(
				"{ \"$addFields\" : { \"name\" : \"$id.name\", \"balance\" : \"$balance\", \"age\" : \"$id.age\" } }",
				addFieldString);
	}

	@Test
	public void testBuildLookUpQuery() {
		String lookUpQuery = MongoQueryBuilderUtility.buildLookUpQuery("department", "department_id", "_id",
				"employee_department");
		assertEquals(
				"{ \"$lookup\" : { \"from\" : \"department\" , \"localField\" : \"department_id\" , \"foreignField\" : \"_id\" , \"as\" : \"employee_department\"}}",
				lookUpQuery);

	}

	@Test
	public void testBuildUnWindString() {
		String unWindString = MongoQueryBuilderUtility.buildUnWindString("zip_codes");
		assertEquals("{ \"$unwind\" : { \"path\" : \"$zip_codes\", \"preserveNullAndEmptyArrays\" : true } }",
				unWindString);
	}

	@Test
	public void testBuildProjectionString() throws JsonProcessingException {
		Map<String, Integer> fields = new LinkedHashMap<>();
		fields.put("name", 1);
		fields.put("age", 1);
		fields.put("email", 1);
		fields.put("_id", 0);
		String projectionString = MongoQueryBuilderUtility.buildProjectionString(fields);
		assertEquals("{ \"$project\" : { \"name\" : \"1\", \"age\" : \"1\", \"email\" : \"1\", \"_id\" : \"0\" } }",
				projectionString);
	}
	@Test
	public void testBuildFacetQuery() {
		String buildFacetQuery = MongoQueryBuilderUtility.buildFacetQuery(75, 20, true);
		assertEquals("{ \"$facet\" : { \"data\" : [{ \"$sort\" : { \"_id\" : -1 } }], \"pageInfo\" : [{ \"$count\" : \"totalRecords\" }] } }", buildFacetQuery);
		buildFacetQuery = MongoQueryBuilderUtility.buildFacetQuery(75, 20, false);
		assertEquals("{ \"$facet\" : { \"data\" : [{ \"$sort\" : { \"_id\" : -1 } }, { \"$skip\" : 75 }, { \"$limit\" : 20 }], \"pageInfo\" : [{ \"$count\" : \"totalRecords\" }] } }",buildFacetQuery);
	}
	@Test
	public void testBuildInQuery() {
		List<String> values=Arrays.asList(new String[] {"California","Oregon","Washington"});
		String buildInQuery = MongoQueryBuilderUtility.buildInQuery("state", values);
		assertEquals("{ \"state\" : { \"$in\" : [\"California\", \"Oregon\", \"Washington\"] } }", buildInQuery);
		List<Integer> intValues=Arrays.asList(new Integer[] {1500,2000,2500});
		buildInQuery = MongoQueryBuilderUtility.buildInQuery("code", intValues);
		assertEquals("{ \"code\" : { \"$in\" : [1500, 2000, 2500] } }", buildInQuery);
	}
	@Test
	public void testBuildGroupByQuery() {
		List<String> fields=Arrays.asList(new String[] {"country","state"});
		DBObject buildGroupByQuery = MongoQueryBuilderUtility.buildGroupByQuery(fields,PivotType.SUM,"county","county");
		assertEquals("{ \"$group\" : { \"_id\" : { \"country\" : \"$country\" , \"state\" : \"$state\"} , \"county\" : { \"$sum\" : \"$county\"}}}", buildGroupByQuery.toString());
		buildGroupByQuery = MongoQueryBuilderUtility.buildGroupByQuery(fields,PivotType.MAX,"county","county");
		assertEquals("{ \"$group\" : { \"_id\" : { \"country\" : \"$country\" , \"state\" : \"$state\"} , \"county\" : { \"$max\" : \"$county\"}}}", buildGroupByQuery.toString());
		buildGroupByQuery = MongoQueryBuilderUtility.buildGroupByQuery(fields,PivotType.MIN,"county","county");
		assertEquals("{ \"$group\" : { \"_id\" : { \"country\" : \"$country\" , \"state\" : \"$state\"} , \"county\" : { \"$min\" : \"$county\"}}}", buildGroupByQuery.toString());
		buildGroupByQuery = MongoQueryBuilderUtility.buildGroupByQuery(fields,PivotType.COUNT,"county",null);
		assertEquals("{ \"$group\" : { \"_id\" : { \"country\" : \"$country\" , \"state\" : \"$state\"} , \"county_count\" : { \"$sum\" : 1}}}", buildGroupByQuery.toString());
		buildGroupByQuery = MongoQueryBuilderUtility.buildGroupByQuery(fields,PivotType.AVERAGE,"county","county");
		assertEquals("{ \"$group\" : { \"_id\" : { \"country\" : \"$country\" , \"state\" : \"$state\"} , \"county\" : { \"$avg\" : \"$county\"}}}", buildGroupByQuery.toString());
		
	}

}
