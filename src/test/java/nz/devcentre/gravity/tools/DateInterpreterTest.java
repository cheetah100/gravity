/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration(locations = { "/date-interpreter-test.xml"})
public class DateInterpreterTest {
	
	@Autowired
	private DateInterpreter dateInterpreter;
	
	@Test
	public void testNotNull() throws Exception {
		assertNotNull(dateInterpreter);
	}
	
	@Test
	public void testSimpleMatches() throws Exception {
		assertTrue(dateInterpreter.isSimpleTodayExpression("today"));
		assertTrue(dateInterpreter.isSimpleTodayExpression("toDay"));
		assertTrue(dateInterpreter.isSimpleTodayExpression(" toDay "));
		assertFalse(dateInterpreter.isSimpleTodayExpression("now"));
	}
	
	@Test
	public void testComplexMatches() throws Exception {
		assertTrue(dateInterpreter.isDateFormula("today"));
		assertTrue(dateInterpreter.isDateFormula(" today   +   3   days"));
		assertTrue(dateInterpreter.isDateFormula(" today   -   5555   days"));
		assertTrue(dateInterpreter.isDateFormula("today-4days"));
		assertTrue(dateInterpreter.isDateFormula("today+4days"));
		assertFalse(dateInterpreter.isDateFormula("today4days"));
	}
	
	@Test
	public void testConversion_futureDate() throws Exception {
		final Calendar calFutureDate = Calendar.getInstance();
		calFutureDate.add(Calendar.DAY_OF_YEAR, 4);
		
		int dayOfMonth = calFutureDate.get(Calendar.DAY_OF_MONTH);
		int month = calFutureDate.get(Calendar.MONTH);
		int year = calFutureDate.get(Calendar.YEAR);
		
		final Date interpretDate = dateInterpreter.interpretDateFormula("today+4days");
		
		Calendar calInterpreted = Calendar.getInstance();
		calInterpreted.setTime(interpretDate);

		assertEquals(dayOfMonth, calInterpreted.get(Calendar.DAY_OF_MONTH));
		assertEquals(month, calInterpreted.get(Calendar.MONTH));
		assertEquals(year, calInterpreted.get(Calendar.YEAR));
	}
	
	@Test
	public void testConversion_pastDate() throws Exception {
		final Calendar calFutureDate = Calendar.getInstance();
		calFutureDate.add(Calendar.DAY_OF_YEAR, -3);
		
		int dayOfMonth = calFutureDate.get(Calendar.DAY_OF_MONTH);
		int month = calFutureDate.get(Calendar.MONTH);
		int year = calFutureDate.get(Calendar.YEAR);
		
		final Date interpretDate = dateInterpreter.interpretDateFormula("today-3days");
		
		Calendar calInterpreted = Calendar.getInstance();
		calInterpreted.setTime(interpretDate);
		
		assertEquals(dayOfMonth, calInterpreted.get(Calendar.DAY_OF_MONTH));
		assertEquals(month, calInterpreted.get(Calendar.MONTH));
		assertEquals(year, calInterpreted.get(Calendar.YEAR));
	}
}
