/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.Interval;

import org.junit.jupiter.api.Test;

import com.mongodb.DBObject;

import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.PivotType;

public class MongoQueryToolTest {

	@Test
	public void testMatch() {

		Map<String, Condition> conditions = new HashMap<>();
		conditions.put("a", new Condition("name", Operation.EQUALTO, "jim"));
		conditions.put("b", new Condition("age", Operation.GREATERTHAN, "10"));
		conditions.put("c", new Condition("isfootball", Operation.NOTNULL, "fart"));
		
		DBObject match = MongoQueryTool.match(conditions);
		assertEquals( "{ \"$match\" : { \"$and\" : [ { \"name\" : \"jim\"} , { \"age\" : { \"$gt\" : 10}} , { \"isfootball\" : { \"$ne\" :  null }}]}}", match.toString());
		
	}
	
	@Test
	public void testMatchMetric() {

		Map<String, Condition> conditions = new HashMap<>();
		conditions.put("c", new Condition("criticality", Operation.EQUALTO, "C1|C2"));
		conditions.put("b", new Condition("monitoring_priority", Operation.NUMBEREQUALTO, "1"));
		conditions.put("a", new Condition("data_classification", Operation.EQUALTO, "highly_confidential|restricted"));
		conditions.put("d", new Condition("resourcetype", Operation.EQUALTO, "Host"));
		
		DBObject match = MongoQueryTool.match(conditions);
		assertEquals( String.join("", "{ \"$match\" : { \"$and\" : [ { \"$or\" : [ ",
				"{ \"data_classification\" : \"highly_confidential\"} , ",
				"{ \"data_classification\" : \"restricted\"}]} , ",
				"{ \"monitoring_priority\" : 1} , { \"$or\" : [ ",
				"{ \"criticality\" : \"C1\"} , { \"criticality\" : \"C2\"}]} , ",
				"{ \"resourcetype\" : \"Host\"}]}}"), match.toString());
	}
	
	@Test
	public void testMatchInterval() {
		
		Interval interval = new Interval( 1000000l, 2000000l);
		DBObject match = MongoQueryTool.matchInterval("mydate", interval);
		assertEquals( "{ \"$match\" : { \"mydate\" : { \"$gte\" : { \"$date\" : \"1970-01-01T00:16:40.000Z\"} , \"$lt\" : { \"$date\" : \"1970-01-01T00:33:20.000Z\"}}}}", match.toString());
	}
	
	
	@Test
	public void testAddFields() {
		
		List<String> fields = new ArrayList<String>();
		fields.add("age");
		fields.add("city");
		fields.add("colour");
		
		DBObject addFields = MongoQueryTool.addFields("test",fields,":");
		System.out.println("test = " +addFields.toString());

		assertEquals("{ \"$addFields\" : { \"test\" : { \"$concat\" : [ \"$_id.age\" , \":\" , \"$_id.city\" , \":\" , \"$_id.colour\"]}}}",addFields.toString());
		
	//	assertEquals( "{ \"$addFields\" : { \"test\"\"colour\" : \"pink\" , \"city\" : \"$city\" , \"age\" : \"$age\"}}", addFields.toString().);
	}

	/**
	 * { "$lookup" : { "from" : "target_collection" , "localField" : "target_id" , "foreignField" : "_id" , "as" : "target_id"}}
	 */
	@Test
	public void testLookup() {
		DBObject lookup = MongoQueryTool.lookup("target_collection", "target_id");
		assertEquals( "{ \"$lookup\" : { \"from\" : \"target_collection\" , \"localField\" : \"target_id\" , \"foreignField\" : \"_id\" , \"as\" : \"target_id\"}}", lookup.toString());
	}

	/**
	 * { $unwind: { path: "$sizes" } }
	 */
	@Test
	public void testUnwindWithNoArrayIndex() {
		DBObject unwind = MongoQueryTool.unwind("field", null);
		assertEquals( "{ \"$unwind\" : { \"path\" : \"$field\"}}", unwind.toString());
	}

	/**
	 * { $unwind: { path: "$sizes", includeArrayIndex: "arrayIndex" } }
	 */
	@Test
	public void testUnwindWithArrayIndex() {
		DBObject unwind = MongoQueryTool.unwind("field", "index");
		assertEquals( "{ \"$unwind\" : { \"path\" : \"$field\" , \"includeArrayIndex\" : \"index\"}}", unwind.toString());
	}
	
	
	/**
	 * { "$group" : { "_id" : { "groupfield1" : "$groupfield1" , "groupfield2" : "$groupfield2"} , "total" : { "$sum" : "$total"}}}
	 */
	@Test
	public void testGroup() {
		List<String> fields = new ArrayList<>();
		fields.add("groupfield1");
		fields.add("groupfield2");
		
		DBObject group = MongoQueryTool.group(fields, "total", PivotType.SUM);
		assertEquals( "{ \"$group\" : { \"_id\" : { \"groupfield1\" : \"$groupfield1\" , \"groupfield2\" : \"$groupfield2\"} , \"total\" : { \"$sum\" : \"$total\"}}}", group.toString());
	}

}
