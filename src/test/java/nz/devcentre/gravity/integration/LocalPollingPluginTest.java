/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.anyBoolean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.services.QueryService;

@ExtendWith(MockitoExtension.class)
public class LocalPollingPluginTest {
	
	@InjectMocks
	private LocalPollingPlugin plugin;
	
	@Mock
	private AutomationEngine automationEngine;
	
	@Mock
	private SecurityTool securityTool;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock 
	private ResourceController resourceController;	
	
	@Mock
	private QueryService listTools;
	
	@Mock
	private IntegrationService integrationService;
	@Test
	public void testStart() throws Exception {
		
		Map<String,Object> config = new HashMap<>();
		Map<String,Action> actions = new HashMap<>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
		
		verify(this.timerManager, times(1)).activateIntegration(integration,plugin);
	}

	@Test
	public void testExecute() throws Exception {
		
		Card card = new Card();
		card.setId("test-card");
		
		Map<String,Object> fields = new HashMap<String,Object>();
		fields.put("name", "test");
		card.setFields(fields);
		
		Collection<Card> collection = new ArrayList<Card>();
		collection.add(card);
		
		when( listTools.dynamicQuery(anyString(), anyString(),any(),any(),anyBoolean())).thenReturn(collection);
		
		Map<String,Object> config = new HashMap<String,Object>();
		config.put("board","test_board");
				
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		Batch batch = new Batch();
		when(integrationService.createBatch(any(Integration.class), any(Integer.class), any(Date.class))).thenReturn(batch);
		
		plugin.execute(integration);
	}

}
