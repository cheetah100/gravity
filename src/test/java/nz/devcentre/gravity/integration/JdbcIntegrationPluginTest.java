/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.managers.JdbcTemplateManager;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class JdbcIntegrationPluginTest {
	
	@InjectMocks
	private JdbcIntegrationPlugin plugin;
	
	@Mock
	private AutomationEngine automationEngine;
	
	@Mock
	private SecurityTool securityTool;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock 
	private ResourceController resourceController;	
	
	@Mock
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Mock 
	private JdbcTemplate jdbcTemplate;

	@Test
	public void testStart() throws Exception {
		Map<String,Object> config = new HashMap<>();
		Map<String,Action> actions = new HashMap<>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
		
		verify(this.timerManager, times(1)).activateIntegration(integration,plugin);
	}

	@Test
	public void testExecute() throws Exception {
		Map<String,Object> config = new HashMap<>();
		config.put("board","test_board");
		config.put("resource","resource");
		config.put("collection","test_collection");
		
		when(this.jdbcTemplateManager.getTemplate(any())).thenReturn(this.jdbcTemplate);
		
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		plugin.execute(integration);
	}

}
