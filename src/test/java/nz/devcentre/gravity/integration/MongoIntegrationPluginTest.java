/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.managers.MongoTemplateManager;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class MongoIntegrationPluginTest {
	
	@InjectMocks
	private MongoIntegrationPlugin plugin;
	
	@Mock
	private AutomationEngine automationEngine;
	
	@Mock
	private SecurityTool securityTool;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock 
	private ResourceController resourceController;
	
	@Mock
	private MongoTemplateManager mongoTemplateManager;
	
	@Mock
	private MongoTemplate mongoTemplate;
	
	@Mock
	private DB db;
	
	@Mock
	private DBCollection dBCollection;
	
	@Mock
	AggregationOutput aggregationOutput;
	
	@Mock
	Iterable<DBObject> iterable;
	
	
	/*  
	 * TODO Expecting NullPointerException ?
	 */
	@Test
	public void testExecute() throws Exception {

		Map<String,Object> config = new HashMap<>();
		config.put("board","test_board");
		config.put("resource","resource");
		config.put("collection","test_collection");
		
		when(this.mongoTemplateManager.getTemplate(any())).thenReturn(this.mongoTemplate);
		when(this.mongoTemplate.getDb()).thenReturn(this.db);
		when(this.db.getCollection("test_collection")).thenReturn(dBCollection);
		when(this.resourceController.getResource("test_board", "resource")).thenReturn("[\r\n" + 
				"    {\r\n" + 
				"        \"$match\":  { \"Name\":\"Jerry\" }\r\n" + 
				"    }\r\n" + 
				"]");
		when(this.dBCollection.aggregate(any())).thenReturn(aggregationOutput);
		when(this.aggregationOutput.results()).thenReturn(iterable);
		
		List<DBObject> it = new ArrayList<DBObject>();
		DBObject dbo = new BasicDBObject();
		dbo.put("_id", "test_id");
		it.add(dbo);

		when(this.iterable.iterator()).thenReturn(it.iterator());

		Map<String,Object> map = new HashMap<String,Object>();
		map.put("_id", 5l);
		map.put("name", "value");

		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		
		Assertions.assertThrows(NullPointerException.class, () -> {
			plugin.execute(integration);
		}, "NullPointerException was expected");
		
	}

	@Test
	public void testStart() throws Exception {
		Map<String,Object> config = new HashMap<>();
		Map<String,Action> actions = new HashMap<>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
		
		verify(this.timerManager, times(1)).activateIntegration(integration,plugin);
	}

}
