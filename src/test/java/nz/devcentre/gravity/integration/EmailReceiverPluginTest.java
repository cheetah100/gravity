/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.EmailService;

@ExtendWith(MockitoExtension.class)
public class EmailReceiverPluginTest {
	
	@InjectMocks
	EmailReceiverPlugin plugin;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock
	private AutomationEngine automationEngine;
	
	@Mock
	private SecurityTool securityTool;
	
	@BeforeEach
	public void setUp() throws Exception { 
	}
	
	@Test
	public void testStart() throws Exception {
		Map<String,Object> config = new HashMap<>();
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
		
		verify(this.timerManager, times(1)).activateIntegration(integration,plugin);
	}
	
	@Test
	@Disabled
	// Ignoring because test fails under test in SonarCube
	public void testReceiveEmail() throws Exception {
		
		EmailService emailSenderAction = new EmailService(); 
		emailSenderAction.sendEmail("Unit Test Message - for Receiving [valid]", 
				"<html><body><h1>Unit Test Email - Testing for correct receive</h1></body></html>", 
				"test@devcentre.org", 
				null,
				"test@cisco.com",
				"test@cisco.com",
				 true, null, null);
		
		// Wait for delivery
		Thread.sleep(5000);
		
		Credential cred = new Credential();
		cred.setId("test_cred");
		cred.setIdentifier("test@devcentre.org");
		cred.setSecret("flubber1964");
		cred.setResource("mail.devcentre.org");
		cred.setMethod("imaps");
		
		when(this.securityTool.getSecureCredential("test_cred")).thenReturn(cred);
		
		Map<String,Object> config = new HashMap<>();
		config.put("subject_filter", "[valid]");
		config.put("from_filter", "test@cisco.com");
		config.put("credential", "test_cred");
		config.put("processed_folder", "processed");
		
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		this.plugin.execute(integration);
		
		verify(this.automationEngine, atLeast(1)).executeIntegrationActions(any(), any(), any());
		
	}
	
	@Test
	public void testIsValidMatchTrue() {
		
		List<String> list = new ArrayList<String>();
		list.add("cat");
		list.add("test");
		
		boolean validMatch = this.plugin.isValidMatch(list, "This is a Test message");
		assertTrue(validMatch);
	}
	
	@Test
	public void testIsValidMatchFalse() {
		
		List<String> list = new ArrayList<String>();
		list.add("cat");
		list.add("test");
		
		boolean validMatch = this.plugin.isValidMatch(list, "This is a Funny message");
		assertFalse(validMatch);
	}
	
}
