/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.services.IntegrationService;

@ExtendWith(MockitoExtension.class)
public class IntegrationMessageListenerTest {
	
	@Mock
	private Integration integration;
	
	@Mock
	private Message message;
	
	@Mock
	private MessageProperties messageProperties;
	
	@Mock
	private IntegrationService integrationService;

	@Test
	public void testOnMessage() throws Exception {
		
		IntegrationMessageListener integrationMessageListener =
				new IntegrationMessageListener(integration, integrationService);
		
		byte[] bodyArray = "{'field':'value'}".getBytes();
		
		when(message.getMessageProperties()).thenReturn(this.messageProperties);
		when(message.getBody()).thenReturn(bodyArray);
		Map<String,Object> values = new HashMap<String,Object>();
		values.put("body", "{'field':'value'}");
		when(messageProperties.getHeaders()).thenReturn(values);
		Batch batch = new Batch();
		when(integrationService.createBatch(any(Integration.class), any(Integer.class), any(Date.class))).thenReturn(batch);
		integrationMessageListener.onMessage(message);		
		verify(this.integrationService, times(1)).execute(any(),any(),any());
	}
}
