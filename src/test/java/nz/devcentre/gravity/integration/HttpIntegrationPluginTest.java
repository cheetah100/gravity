/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.services.HttpCallService;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.tools.CallResponse;

@ExtendWith(MockitoExtension.class)
public class HttpIntegrationPluginTest {
	
	@InjectMocks
	private HttpIntegrationPlugin plugin;
	
	@Mock
	private HttpCallService tool;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock
	private IntegrationService integrationService;

	@Test
	public void testStart() throws Exception {
		Map<String,Object> config = new HashMap<>();
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
		
		verify(this.timerManager, times(1)).activateIntegration(integration,plugin);
	}

	@Test
	public void testExecuteWithListResponse() throws Exception {
		
		if(this.tool==null) {
			throw new Exception("No Tool");
		}
		
		when(this.tool.execute(anyString(), any(), any(), any(), any(), any(), any(), any()))
		.thenReturn( new CallResponse("[ { 'id':'A' }, { 'id':'B' }, { 'id':'C' } ]" ));
		
		Map<String,Object> config = new HashMap<>();
		config.put("url","http://echo.jsontest.com/key/value/one/two");
		config.put("list","");
		config.put("type","GET");
		
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		Batch batch = new Batch();
		when(integrationService.createBatch(any(Integration.class), any(Integer.class), any(Date.class))).thenReturn(batch);

		plugin.execute(integration);
		
		verify(this.tool, times(1)).execute(anyString(), any(), anyString(), anyString(), any(), anyString(), any(), any());
		
		verify( this.integrationService, times(3) ).execute(any(), any(), any());
	}

	@Test
	public void testExecuteWithMapResponse() throws Exception {
		
		when(this.tool.execute(anyString(), any(), anyString(), anyString(), any(), anyString(), any(), any()))
		.thenReturn( new CallResponse("{ 'total_records' : 300, 'data': [ { 'id':'A' }, { 'id':'B' }, { 'id':'C' } ] }" ));
		
		Map<String,Object> config = new HashMap<>();
		config.put("url","http://echo.jsontest.com/key/value/one/two");
		config.put("list","data");
		config.put("type","GET");
		
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		plugin.execute(integration);
		
		verify(this.tool, times(1)).execute(anyString(), any(), anyString(), anyString(), any(), anyString(), any(), any());
		
		verify( this.integrationService, times(3) ).execute(any(), any(), any());
	}

	@Test
	public void testMultipleCallExecuteWithMapResponse() throws Exception {
		
		when(this.tool.execute(anyString(), any(), anyString(), anyString(), any(), anyString(), any(), any())).thenReturn(
				new CallResponse("{ 'total_records' : 9, 'data': [ { 'id':'A' }, { 'id':'B' }, { 'id':'C' } ] }"),
				new CallResponse("{ 'total_records' : 9, 'data': [ { 'id':'D' }, { 'id':'E' }, { 'id':'F' } ] }"),
						new CallResponse("{ 'total_records' : 9, 'data': [ { 'id':'G' }, { 'id':'H' }, { 'id':'I' } ] }"));
		
		Map<String,Object> config = new HashMap<>();
		config.put("url","http://echo.jsontest.com/key/value/{limit}/{offset}");
		config.put("list","data");
		config.put("type","GET");
		config.put("total_field","total_records");
		config.put("records","3");
		
		Map<String,Action> actions = new HashMap<String,Action>();
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		plugin.execute(integration);
		
		verify(this.tool, times(3)).execute(anyString(), any(), anyString(), anyString(), any(), anyString(), any(), any());
		
		verify( this.integrationService, times(9) ).execute(any(), any(), any());
	}
	
	@Test
	public void testGetListByReferenceSimpleMapCase() {
		
		Map<String,Object> data = new HashMap<String,Object>();
		List<Object> results = new ArrayList<Object>();
		Map<String,Object> record = new HashMap<String,Object>();
		record.put("name", "Joe");
		record.put("age", 4);
		record.put("email", "joe@test.com");
		results.add(record);
		data.put("data", results);
		
		Collection<?> result = this.plugin.getListByReference("data", data);
		
		assertNotNull(result);
		assertEquals(1, result.size());
		
		Map returnRecord = (Map) result.iterator().next();
		String email = (String) returnRecord.get("email");
		assertEquals("joe@test.com",email);
	}
	
	@Test
	public void testGetListByReferenceSimpleListCase() {
		
		Map<String,Object> data = new HashMap<String,Object>();
		List<Object> results = new ArrayList<Object>();
		List<Object> record = new ArrayList<Object>();
		record.add("Joe");
		record.add(4);
		record.add("joe@test.com");
		results.add(record);
		data.put("data", results);
		
		Collection<?> result = this.plugin.getListByReference("data", data);
		
		assertNotNull(result);
		assertEquals(1, result.size());
		
		List returnRecord = (List) result.iterator().next();
		String email = (String) returnRecord.get(2);
		assertEquals("joe@test.com",email);
	}
	
}
