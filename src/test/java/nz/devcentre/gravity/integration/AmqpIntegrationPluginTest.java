/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class AmqpIntegrationPluginTest {

	@InjectMocks
	private AmqpIntegrationPlugin plugin;
	
	@Mock
	SecurityTool securityTool;
	
	@Mock
	AutomationEngine automationEngine;
	
	@Test
	public void testStart() throws Exception {
		
		Credential cred = new Credential();
		cred.setId("test_cred");
		cred.setIdentifier("guest");
		cred.setSecret("guest");
		cred.setResource("amqp://localhost:5672");
		when( securityTool.getSecureCredential("test_cred")).thenReturn(cred);
		
		Map<String,Object> config = new HashMap<>();
		config.put("credential", "test_cred");
		config.put("queue","test_queue");
		
		Map<String,Action> actions = new HashMap<String,Action>();
		
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);
		
		this.plugin.start(integration);
	}
}
