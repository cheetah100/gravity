/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;

import nz.devcentre.gravity.model.Integration;

@ExtendWith(MockitoExtension.class)
public class IntegrationJobTest {
	
	@Mock
	private JobExecutionContext context;
	
	@Mock 
	private JobDetail jobDetail;
	
	@Mock
	private JobDataMap jobDataMap;
	
	@Mock
	private IntegrationPlugin plugin;
	
	@Mock
	private Integration integration;

	@Test
	public void testExecute() throws Exception {
		
		when(this.context.getJobDetail()).thenReturn(jobDetail);
		when(jobDetail.getJobDataMap()).thenReturn(jobDataMap);
		when(jobDataMap.get("plugin")).thenReturn(plugin);
		when(jobDataMap.get("integration")).thenReturn(integration);
		
		IntegrationJob job = new IntegrationJob();
		job.execute(this.context);
		
		verify(this.plugin, times(1)).execute(integration);
	}
	
	@Test
	public void testExecuteException() throws Exception {
		
		when(this.context.getJobDetail()).thenReturn(jobDetail);
		when(jobDetail.getJobDataMap()).thenReturn(jobDataMap);
		when(jobDataMap.get("plugin")).thenReturn(plugin);
		when(jobDataMap.get("integration")).thenReturn(integration);
		Mockito.doThrow( new Exception("Boo")).when(this.plugin).execute(integration);
		
		Assertions.assertThrows(Exception.class, () -> {
			IntegrationJob job = new IntegrationJob();
			job.execute(this.context);
		}, "Exception was expected");
	}

}
