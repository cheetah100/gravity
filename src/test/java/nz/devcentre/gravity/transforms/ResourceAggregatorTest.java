/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ResourceAggregatorTest {

	public static final String RESOURCE_ID="test_board_transform_resource";
	public static final String RESOURCE_BODY="[{ \"$match\" : { \"$and\" : [ { \"$or\" : [ { \"metadata.phase\" : \"test_phase\"} , { \"metadata.phase\" : \"next_phase\"}]}]}}]";
	
	@Autowired
	ResourceAggregator resourceAggregator;
	
	@Autowired
	TestBoardTool tool;
	
	@Autowired
	ResourceController resourceController;
	
	@Autowired
	SecurityTool securityTool;
	
	
	@Configuration
	@Import({ FongoConfiguration.class})
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Test
	public void testBBSimpleValue() {
		
		ResourceAggregator ra = new ResourceAggregator();
		String result = ra.getParameter("field_name", "value");
		assertEquals("{\"field_name\":\"value\"}",result);
		
		assertEquals("{}", ra.getParameter("name", null));
	}

	@Test
	public void testMultipleValue() {
		
		ResourceAggregator ra = new ResourceAggregator();
		String result = ra.getParameter("field_name", "value1|value2|value3");
		assertEquals("{\"\\$or\": [{\"field_name\":\"value1\"},{\"field_name\":\"value2\"},{\"field_name\":\"value3\"}]}",result);
	}
	
	@Test
	public void testAATransform() throws Exception {
		this.tool.initTestBoard();
		this.securityTool.iAmSystem();
		this.resourceController.createResourceByJsonString(TestBoardTool.BOARD_ID, RESOURCE_ID, RESOURCE_BODY, null, "txt");
		Transform transform=new Transform();
		transform.setConfiguration(ImmutableMap.of("board",TestBoardTool.BOARD_ID,"resource",RESOURCE_ID));
		Map<String, Object> results = this.resourceAggregator.transform(new HashMap<>(), transform, false, null);
		assertFalse(results.isEmpty());
		
	}
	
}
