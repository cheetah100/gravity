/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.DataTracker;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.QueryService;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class DataAnalyticsTest {
	
	@Autowired
	private DataAnalytics dataAnalytics;
	
	@Autowired
	private QueryService listTools;
	
	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	SecurityTool securityTool;
	
	@Autowired
	BoardsCache boardsCache;
	
	public static Boolean initialized = false;
	
	@Configuration
    @Import({FongoConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }

	@BeforeEach
	public void setUp() throws Exception {
		this.securityTool.iAmSystem();
		
		if (!initialized) {
			tool.initTestBoard();
			tool.initTestBoardInvalidCards();
			tool.generateFilters();
			initialized = true;
		}
	}

	@Test
	@Disabled
	public void testTransformFullBoard() throws Exception {
		Map<String,Object> input = new HashMap<String,Object>();
		
		Map<String,Object> config = new HashMap<String,Object>();
		config.put("board", TestBoardTool.BOARD_ID);
		
		Transform transform = new Transform();
		transform.setConfiguration(config);
		
		Map<String,Object> output = dataAnalytics.transform(input, transform, false, null);
		
		assertNotNull(output);
		
		Collection<Card> cards = listTools.query(TestBoardTool.BOARD_ID, null, null, null, false);
		
		// Field from the test board that was initialized in the setUp method
		String testBoardField = "name";
		
		
		System.out.println(cards.size());
		
		// Check that the total number of cards in the output data tracker 
		// matches the number of cards in the board
		assertEquals(Integer.valueOf(cards.size()), 
			((DataTracker)output.get(testBoardField)).getTotalNumber());
		
		// Check that the total filled out fields in the output data tracker 
		// matches the number of cards in the board
		assertEquals(Integer.valueOf(cards.size()), 
			((DataTracker)output.get(testBoardField)).getValidCount());
	}
	
	@Test
	@Disabled
	public void testTransformFullBoardMissingFields() throws Exception {
		Map<String,Object> input = new HashMap<String,Object>();
		
		Map<String,Object> config = new HashMap<String,Object>();
		config.put("board", TestBoardTool.BOARD_ID_INVALID_CARDS);
		
		Transform transform = new Transform();
		transform.setConfiguration(config);
	
		Map<String,Object> output = dataAnalytics.transform(input, transform, false, null);
		
		assertNotNull(output);
		
		Collection<Card> cards = listTools.query(TestBoardTool.BOARD_ID_INVALID_CARDS, null, null, null, false);
		
		// Field from the test board that was initialized in the setUp method
		String testBoardField = "name";
		
		// Check that the total number of cards in the output data tracker 
		// matches the number of cards in the board
		assertEquals(Integer.valueOf(cards.size()), 
			((DataTracker)output.get(testBoardField)).getTotalNumber());
		
		// Check that the total filled out fields in the output data tracker 
		// is 2 as defined in the TestBoardTool
		assertEquals(Integer.valueOf(2), 
			((DataTracker)output.get(testBoardField)).getValidCount());
	}
	
	@Test
	@Disabled
	public void testTransformWithUnmatchedBatchId() throws Exception {
		Map<String,Object> input = new HashMap<String,Object>();
		
		Map<String,Object> config = new HashMap<String,Object>();
		config.put("board", TestBoardTool.BOARD_ID);
		
		Map<String, Object> dynamicConfigs = new HashMap<>();
		dynamicConfigs.put("batchId", "blah");
		
		Transform transform = new Transform();
		transform.setConfiguration(config);
		
		Map<String,Object> output = dataAnalytics.transform(input, transform, false, dynamicConfigs);
		
		assertNotNull(output);
		
		// Field from the test board that was initialized in the setUp method
		String testBoardField = "name";
		
		// The filter containing the batch ID should not have returned any cards
		assertEquals(Integer.valueOf(0), 
				((DataTracker)output.get(testBoardField)).getTotalNumber());
	}
}
