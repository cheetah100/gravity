/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.services.QueryService;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class ConcatenatorTest {
	
	@Autowired
	private Concatenator concatenator;
	
	@Autowired
	private QueryService listTools;
	
	@Autowired
	private TestBoardTool tool;
	
	private static Card card;
	
	public static Boolean initialized = false;
	
	@Configuration
    @Import({FongoConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }

	@BeforeEach
	public void setUp() throws Exception {
		if (!initialized) {
			tool.initTestBoard();
			tool.generateFilters();
			initialized = true;
		}
		card = tool.getTestCard();
	}

	@Test
	@Disabled
	public void testTransform() throws Exception {
		Map<String,Object> input = new HashMap<String,Object>();
		
		Map<String,Object> config = new HashMap<String,Object>();
		config.put("board", TestBoardTool.BOARD_ID);
		config.put("filter", "test_filter");
		config.put("view", "testview");
		
		Transform transform = new Transform();
		transform.setConfiguration(config);
		
		Map<String,Object> output = concatenator.transform(input, transform, false, null);
		
		assertNotNull(output);
		
		Collection<Card> cards = listTools.query(TestBoardTool.BOARD_ID, null, "test_filter", null, false);
		
		assertTrue(output.containsKey(card.getId()));
	}
	
	private Collection<Card> getFakeCards(){
		Collection<Card> cards = new ArrayList<Card>();
		Card card = new Card();
		card.setId("test_card");
		cards.add(card);
		return cards;
	}

}
