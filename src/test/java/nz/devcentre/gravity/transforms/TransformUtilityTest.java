/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class TransformUtilityTest {
	
	private Transform transform;
	
	@Autowired
	SecurityTool securityTool;
	
	@Autowired
	TestBoardTool tool;
	
	@Autowired
	CardController controller;
	
	@Autowired 
	private PivotAggregator pivotAggregator;
	
	@Autowired
	private TransposeAggregator transposeAggregator;
	
	@Autowired
	private Aggregator aggregator;
	
	@Autowired
	private BoardBuilder boardBuilder;
	
	@Autowired
	private Concatenator concatenator;
	
	@Autowired
	private ListTransformer listTransformer;
	
	@Configuration
    @Import({FongoConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
	
	@BeforeEach
	public void setUp() throws Exception {
		this.securityTool.iAmSystem();
		Transform transform=new Transform();
		
		Map<String,Object> config=new HashMap<>();
		config.put("group",Arrays.asList(new String[]{"field1","field2"}));
		config.put("group1","field1|field2");
		config.put("group2",new ArrayList<String>());
		List<Map<String,String>> groupObj=new LinkedList<>();
		groupObj.add(ImmutableMap.of("board","board1","field","field1"));
		groupObj.add(ImmutableMap.of("board","board2","field","field2"));
		config.put("groupObj", groupObj);
		
		transform.setConfiguration(config);
		this.transform=transform;
		tool.initTestBoard();
	}
	@Test
	public void getAggregationStringTest() {
		
		assertTrue(",total: {$avg:\"$total\"}".equals(TransformUtility.getAggregationString("average", "total")));
		assertTrue(",total: {$sum:1}".equals(TransformUtility.getAggregationString("count", "total")));
		assertTrue(",total: {$min:\"$total\"}".equals(TransformUtility.getAggregationString("min", "total")));
		assertTrue(",total: {$max:\"$total\"}".equals(TransformUtility.getAggregationString("max", "total")));
		assertTrue(",total: {$sum:\"$total\"}".equals(TransformUtility.getAggregationString(null, "total")));
	}
	@Test
	public void objectToListTest() {
		List<String> objectToList = TransformUtility.objectToList(Arrays.asList(new String[]{"Test1","Test2"}));
		assertTrue(objectToList.size()==2);
		
		List<String> objectToList2 = TransformUtility.objectToList("Test1|Test2");
		assertTrue(objectToList2.size()==2);
		
		assertTrue(objectToList.equals(objectToList2));
		
		List<String> objectToList3 = TransformUtility.objectToList(null);
		assertTrue(objectToList3.isEmpty());
		
		List<String> objectToList4 = TransformUtility.objectToList(new String[]{"Test1","Test2"});
		assertTrue(objectToList4.isEmpty());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getValueTest() {
		List<String> groupByList1= new ArrayList<String>((List<String>) TransformUtility.getValue(transform, "group"));
		List<String> groupByList2= new ArrayList<String>((List<String>) TransformUtility.getValue(transform, "group1"));
		assertTrue(groupByList1.size()==groupByList2.size());
		assertTrue(groupByList1.equals(groupByList2));
		List<String> groupByList3= new ArrayList<String>((List<String>) TransformUtility.getValue(transform, "unknown"));
		assertTrue(groupByList3.isEmpty());
		
		List<String> groupByList4= new ArrayList<String>((List<String>) TransformUtility.getValue(transform, "group2"));
		assertTrue(groupByList4.isEmpty());
		
		List<String> groupByList5= new ArrayList<String>((List<String>) TransformUtility.getValue(transform, "groupObj.field"));
		assertTrue(groupByList5.equals(groupByList1));
	}
	
	@Test
	public void getValueListTest() {
		List<String> groupByList=  TransformUtility.getValueList(transform, "groupObj.field");
		assertTrue(groupByList.size()==2);
		
		List<String> groupByList1= TransformUtility.getValueList(transform, "group");
		assertTrue(groupByList1.size()==2);
		
		List<String> groupByList2= TransformUtility.getValueList(transform, "group1");
		assertTrue(groupByList2.size()==2);
		
		List<String> groupByList3= TransformUtility.getValueList(transform, "unknown");
		assertTrue(groupByList3.isEmpty());
		
		List<String> groupByList4= TransformUtility.getValueList(transform, "group2");
		assertTrue(groupByList4.isEmpty());
	}
	
	@Test
	public void getMapValueTest() {
		Object mapValue = TransformUtility.getMapValue(ImmutableMap.of("key","value"),"key");
		assertTrue(mapValue.equals("value"));
		
		Object mapValueNull = TransformUtility.getMapValue(ImmutableMap.of("key","value"),"key1");
		assertNull(mapValueNull);
	}
	
	@Test
	public void transformPivotTest() {
		try {
				Transform transform=new Transform();
				Map<String,Object> config=new HashMap<String, Object>();
				config.put("board","test_board");
				config.put("filter",null);
				config.put("lookup", "owner.name");
				config.put("group",Arrays.asList(new String[] {"phase"}));
				config.put("sort",Arrays.asList(new String[] {"phase:1"}));
				config.put("total","balance");
				config.put("yaxis","phase");
				config.put("xaxis","phone");
				config.put("ylabel","ylabel");
				config.put("addxtotal","addytotal");
				config.put("addytotal","addytotal");
				transform.setConfiguration(config);
				Map<String, Object> transformResult = this.pivotAggregator.transform(null, transform, false, null);
			
			assertNotNull(transformResult);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	@Test
	public void transformTransposeTest() {
		try {
			Transform transform=new Transform();
			Map<String,Object> config=new HashMap<String, Object>();
			config.put("board","test_board");
			config.put("filter",null);
			config.put("lookup", "owner.name");
			config.put("group","phase");
			config.put("sort",Arrays.asList(new String[] {"phase:1"}));
			config.put("transpose",Arrays.asList(new String[] {"phase|balance"}));
			config.put("total","balance");
			config.put("yaxis","phase");
			config.put("xaxis","phone");
			config.put("ylabel","ylabel");
			config.put("addxtotal","addytotal");
			config.put("addytotal","addytotal");
			transform.setConfiguration(config);
			
			Map<String, Object> transformResult = this.transposeAggregator.transform(null, transform, false, null);
			
			assertNotNull(transformResult);
			
			
			//Test With Pipeline
			List<String> pipeLineList = Arrays.asList(new String[] {
					 "{'$group':{'_id':{'quarter':'$quarter','service':'$service_id'},'items':{'$push':{'phase':'$metadata.phase','value':{'$cond': { 'if': { '$eq': [ '$value', '' ] }, 'then': 0, 'else': '$value' }}}}}}",
			          "{'$project':{'tmp':{'$arrayToObject':{'$zip':{'inputs':['$items.phase','$items.value']}}},'value':{'$sum':'$items.value'}}}",
			          "{'$addFields':{'tmp.quarter':'$_id.quarter','tmp.service':'$_id.service','tmp.total':'$value'}}",
			          "{'$replaceRoot':{'newRoot':'$tmp'}}"
			});
			config.put("pipeline",pipeLineList);
			transform.setConfiguration(config);
			
			Map<String, Object> transformResult1 = this.transposeAggregator.transform(null, transform, false, null);
			assertNotNull(transformResult1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	@Test
	public void transformAggregatorTest() {
		try {
			Transform transform = new Transform();
			Map<String, Object> config = new HashMap<String, Object>();
			config.put("board", "test_board");
			config.put("filter", null);
			config.put("group", "phase");
			config.put("total", "balance");
			transform.setConfiguration(config);
			Map<String, Object> transformResult = this.aggregator.transform(null, transform, false, null);
			assertNotNull(transformResult);

			// With total as NULL
			config.put("total", null);
			transform.setConfiguration(config);
			Map<String, Object> transformResult1 = this.aggregator.transform(null, transform, false, null);
			assertNotNull(transformResult1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
	@Test
	public void transformBoardBuilderTest() {
	
		//BoardBuilder
		try {
			Transform transform = new Transform();
			Map<String, Object> config = new HashMap<String, Object>();
			config.put("board", "test_board");
			config.put("filter", null);
			config.put("phase", "test_phase");
			config.put("view", null);
			config.put("allcards", "true");
			config.put("descending", "true");
			transform.setConfiguration(config);
			Map<String, Object> transform2 = this.boardBuilder.transform(new HashMap<String, Object>(), transform, false, null);
			assertNotNull(transform2);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Test
	public void transformConcatenatorTest() {
		try {
			Transform transform = new Transform();
			Map<String, Object> config = new HashMap<String, Object>();
			config.put("board", "test_board");
			config.put("filter", null);
			config.put("view", null);
			config.put("allcards", "true");
			transform.setConfiguration(config);
			
			Map<String, Object> transform2 = this.concatenator.transform(new HashMap<String, Object>(), transform, false, null);
			
			assertNotNull(transform2);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	@Test
	public void transFormListTransformerTest() {
		try {
				Transform transform = new Transform();
				Map<String, Object> config = new HashMap<String, Object>();
				config.put("board", "test_board");
				config.put("filter", null);
				config.put("lookup", "owner.name");
				config.put("sort", "phase:1|balance:-1");
				transform.setConfiguration(config);
				Map<String, Object> transform2 = this.listTransformer.transform(new HashMap<String, Object>(), transform, false, null);
				assertNotNull(transform2);
				
				//Single Sort Field
				config.put("sort", "phase:1");
				transform.setConfiguration(config);
				transform2 = this.listTransformer.transform(new HashMap<String, Object>(), transform, false, null);
				assertNotNull(transform2);
				
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
