package nz.devcentre.gravity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;

import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * Created on 11/13/2017.
 */
@EnableMongoRepositories(basePackages = "nz.devcentre.gravity.repository")
@Configuration
public class FongoConfiguration extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return "gravity";
    }

    @Bean
    @Override
    public Mongo mongo() {
        // uses fongo for in-memory tests
        return new Fongo("gravity").getMongo();
    }
    
    @Bean
    public IdentifierTools identifierTools() {
        return new IdentifierTools();
    }

    @Override
    protected String getMappingBasePackage() {
        return "nz.devcentre.gravity.model";
    }
}
