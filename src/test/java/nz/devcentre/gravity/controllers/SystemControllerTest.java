/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.ExceptionEvent;
import nz.devcentre.gravity.model.transfer.ExamineData;
import nz.devcentre.gravity.repository.ExceptionRepository;
import nz.devcentre.gravity.security.SecurityTool;

import java.util.Random;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SystemControllerTest {
	public static String BOARD_ID = "test_board";
	protected static Random RND = new Random();

	@Autowired
	private SystemController controller;

	@Autowired
	private ExceptionRepository exceptionRepository;
	
	@Autowired
	private TestBoardTool testBoardTool;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Configuration
	@Import({FongoConfiguration.class, RabbitConfiguration.class})
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig{
	       
	}
	
	@Test
	public void testExceptionEvents() throws Exception {
		
		controller.dismissExceptionEvents();
		List<ExceptionEvent> exceptionEvents = controller.getExceptionEvents();
		assertEquals( 0, exceptionEvents.size());
		
		ExceptionEvent ev = new ExceptionEvent();
		ev.setActive(true);
		ev.setDescription("Test");
		ev.setBody("Test Body");
		exceptionRepository.save(ev);
		
		List<ExceptionEvent> newExceptionEvents = controller.getExceptionEvents();
		assertEquals( 1, newExceptionEvents.size());
		ExceptionEvent exceptionEvent = newExceptionEvents.get(0);
		assertEquals( "Test", exceptionEvent.getDescription());
		assertEquals( "Test Body", exceptionEvent.getBody());
		assertTrue( exceptionEvent.isActive());
		
		controller.dismissExceptionEvents();
		List<ExceptionEvent> dismissedExceptionEvents = controller.getExceptionEvents();
		assertEquals( 0, dismissedExceptionEvents.size());
	}
	
	@Test
	public void testGetSystemState() throws Exception {
		this.testBoardTool.initTestBoard();
		this.controller.verifyCollections();
		this.controller.getSystemReportDetail();
		//this.controller.dismissSystemAlerts();
	}
	
	@Test
	public void testGetRevision() {
		Map<String, Object> result = this.controller.getCommitId();
		assertFalse(result.isEmpty());
	}
	
	@Test
	public void examineBoardTest() throws Exception {
		securityTool.iAmSystem();
		testBoardTool.initTestBoard();	
		ExamineData examineData = new ExamineData();
		examineData.setAll(true);
		examineData.setBoardId(BOARD_ID);
		examineData.setDelay(0);
		this.controller.examineBoard(examineData);// Without Delay
		examineData.setDelay(1);
		this.controller.examineBoard(examineData);// With Delay   
	}
}
