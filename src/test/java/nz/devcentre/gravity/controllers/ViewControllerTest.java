/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ViewControllerTest {

	@Autowired
	private ViewController controller;

	@Autowired
	TestBoardTool tool;

	@Autowired
	BoardsCache cache;

	private static String testViewId = "test_view";

	@BeforeEach
	public void setUp() throws Exception {
		tool.initTestBoard();
	}

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Test
	public void createViewTest() throws Exception {
		View view = getTestView(ViewControllerTest.testViewId);
		View newView = controller.createView(TestBoardTool.BOARD_ID, view);
		String viewId = newView.getId();
		assertTrue(viewId.equalsIgnoreCase(ViewControllerTest.testViewId));
		//Create Same View Again
		try {
			controller.createView(TestBoardTool.BOARD_ID, view);
		}catch (ValidationException e) {
			// TODO: handle exception
			assertNotNull(e);
		}
		
	}

	@Test
	public void updateViewTest() throws Exception {
		View view = this.controller.getView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		view.setName("Test View Update");
		this.controller.updateView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, view);
		view = this.controller.getView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		assertTrue("Test View Update".equalsIgnoreCase(view.getName()));
		
		try {
			this.controller.updateView(TestBoardTool.BOARD_ID, "Invalid_View", view);
		}catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void testGetView() throws Exception {
		View view = this.controller.getView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		assertTrue(view.getId().equalsIgnoreCase(ViewControllerTest.testViewId));
		// Get View Exception
		try {
			this.controller.getView(TestBoardTool.BOARD_ID, null);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		
	}

	@Test
	public void testListViewDetails() throws Exception {
		Map<String, View> listViewDetails = this.controller.listViewDetails(TestBoardTool.BOARD_ID);
		assertNotNull(listViewDetails);
	}

	@Test
	@Disabled
	public void testGetViewWithTemplateData() throws Exception {
		//Template viewWithTemplateData = this.controller.getViewWithTemplateData(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		//assertFalse(viewWithTemplateData.getFields().isEmpty());
	}
	 

	@Test
	public void zzztestDeleteView() throws Exception {
		this.controller.deleteView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		// Get View Exception
		try {
			this.controller.getView(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}

		// Delete View Exception
		try {
			this.controller.deleteView(TestBoardTool.BOARD_ID, null);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		// Delete View Exception
		try {
			this.controller.deleteView(null,  ViewControllerTest.testViewId);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void testViewField() throws Exception {
		ViewField viewField = this.controller.getViewField(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, "phone");
		assertTrue("phone".equalsIgnoreCase(viewField.getId()));
		
		//Delete View field
		this.controller.deleteViewField(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, viewField.getId());
		
		//Create View Field
		ViewField saveViewField = this.controller.saveViewField(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, viewField);
		assertTrue("phone".equalsIgnoreCase(saveViewField.getId()));
		
		//Get Field Exception
		try {
			this.controller.getViewField(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, "invalid_field");
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		try {
			this.controller.deleteViewField(TestBoardTool.BOARD_ID, ViewControllerTest.testViewId, null);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		try {
			this.controller.deleteViewField(null, ViewControllerTest.testViewId, null);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		try {
			this.controller.deleteViewField(TestBoardTool.BOARD_ID, null, null);
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}
		try {
			this.controller.deleteViewField(TestBoardTool.BOARD_ID, "invalid_view", "invalid_field");
		} catch (ResourceNotFoundException e) {
			assertNotNull(e);
		}

	}


	@Test
	public void testListViews() throws Exception {
		Map<String, String> listViews = this.controller.listViews(TestBoardTool.BOARD_ID);
		assertNotNull(listViews);
	}

	@Test
	public void testGetPermissions() throws Exception {
		Map<String, String> permissions = this.controller.getPermissions(TestBoardTool.BOARD_ID,
				ViewControllerTest.testViewId);
		assertNotNull(permissions);
	}
	
	

	@Test
	public void testAddPermissions() throws Exception {
		Map<String, String> permissions = new HashMap<String, String>();
		permissions.put("bond", "test_role");

		this.controller.addPermissions(TestBoardTool.BOARD_ID, testViewId, false, permissions);
		
		Map<String, String>  permissionsUpdated = this.controller.getPermissions(TestBoardTool.BOARD_ID,ViewControllerTest.testViewId);
		assertNotNull(permissionsUpdated);
		assertTrue(permissionsUpdated.containsKey("bond"));
		
		this.controller.addPermissions(TestBoardTool.BOARD_ID, testViewId, true, permissions);
	}

	@Test
	public void testDeletePermissions() throws Exception {
		this.controller.deletePermissions(TestBoardTool.BOARD_ID, testViewId, "bond");
		Map<String, String>  permissionsUpdated = this.controller.getPermissions(TestBoardTool.BOARD_ID,ViewControllerTest.testViewId);
		assertFalse(permissionsUpdated.containsKey("bond"));
	}

	@Test
	public void testAddPermissionsException() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.addPermissions(TestBoardTool.BOARD_ID, "unknown_alien_view", false, new HashMap<>());
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testDeletePermissionsException() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.deletePermissions(TestBoardTool.BOARD_ID, "unknown_alien_view", "bond");
		}, "ResourceNotFoundException was expected");
		
	}
	
	private View getTestView(String viewName) throws Exception {

		Map<String, ViewField> fields = new HashMap<String, ViewField>();
		fields.put("name", getTestViewField("name"));
		fields.put("address", getTestViewField("address"));
		fields.put("phone", getTestViewField("phone"));

		Map<String, String> permissions = new HashMap<String, String>();
		permissions.put("test_user", "test_role");

		View view = new View();
		view.setId(viewName);
		view.setName(viewName);
		view.setFields(fields);
		view.setPermissions(permissions);

		return view;
	}

	private ViewField getTestViewField(String id) throws Exception {
		ViewField vf = new ViewField();
		vf.setId(id);
		vf.setName(id);
		vf.setLabel(id);
		return vf;
	}

}
