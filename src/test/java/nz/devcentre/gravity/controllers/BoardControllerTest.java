/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.transfer.BoardData;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class BoardControllerTest {

	protected static Random RND = new Random();

	@Autowired
	private BoardController controller;

	@Autowired
	private TestBoardTool tool;
		
	@Autowired
	private SecurityTool securityTool;

	@Autowired
	private BoardsCache boardCache;
	
	private Board testBoard;
	
	public static String BOARD_ID = "test_board";

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void setUp() throws Exception {
		this.tool.initTestBoard();
		this.securityTool.iAmSystem();
		this.testBoard = TestBoardTool.getTestBoard("Test Board " + RND.nextInt(9999999), BoardType.OBJECT);
	}

	@Test
	public void testCreateBoard() throws Exception {
		Board newBoard = controller.createBoard(this.testBoard);
		String newBoardId = newBoard.getId();
		Board checkBoard = controller.getBoard(newBoardId);
		assertEquals(checkBoard.getName(), testBoard.getName());
	}

	@Test
	public void testCreateUpdateBoard() throws Exception {

		if (controller == null) {
			fail("No Controller");
		}

		Board board = TestBoardTool.getTestBoard("Test Board " + RND.nextInt(9999999), BoardType.OBJECT);
		Board newBoard = controller.createBoard(board);
		String newBoardId = newBoard.getId();

		Board checkBoard = controller.getBoard(newBoardId);
		assertEquals(checkBoard.getName(), board.getName());

		checkBoard.setName("Updated Board");
		controller.updateBoard(checkBoard, newBoardId);

		Board updatedBoard = controller.getBoard(newBoardId);
		assertEquals(updatedBoard.getName(), checkBoard.getName());
	}
	
	@Test
	public void testUpdateBoardWithoutPermissions() throws Exception {

		if (controller == null) {
			fail("No Controller");
		}

		Board board = TestBoardTool.getTestBoard("Test Board " + RND.nextInt(9999999), BoardType.OBJECT);
		Board newBoard = controller.createBoard(board);
		String newBoardId = newBoard.getId();

		Board checkBoard = controller.getBoard(newBoardId);
		assertEquals(checkBoard.getName(), board.getName());
		assertTrue( checkBoard.getPermissions().containsKey("administrators"));
		assertTrue( checkBoard.getPermissions().containsKey("system"));
		
		Board updateBoard = TestBoardTool.getTestBoard(newBoardId, BoardType.OBJECT);
		updateBoard.setPermissions(new HashMap<String,String>());
		updateBoard.setUniqueid(null);
		updateBoard.setVersion(null);
		updateBoard.setName("Updated Board");
		
		controller.updateBoard(updateBoard, newBoardId);

		Board updatedBoard = controller.getBoard(newBoardId);
		assertEquals(updatedBoard.getName(), checkBoard.getName());
		assertTrue( updatedBoard.getPermissions().containsKey("administrators"));
		assertTrue( updatedBoard.getPermissions().containsKey("system"));

	}

	@Test
	public void testListBoards() throws Exception {
		tool.initTestBoard();
		boardCache.clearCache();
		Map<String, String> listBoards = controller.listBoards();
		assertTrue(listBoards.containsKey(TestBoardTool.BOARD_ID));		
	}
	
	@Test
	public void testGetBoard() throws Exception {
		tool.initTestBoard();
		Board board = controller.getBoard(TestBoardTool.BOARD_ID);
		assertEquals("Test Board", board.getName());
	}

	@Test
	public void testGetFieldValues() throws Exception {
		Set<Object> results = this.controller.getFieldValues(BOARD_ID, "name", null, true);
		assertNotNull(results);
	}


	@Test
	@Disabled
	public void testGetCard() throws Exception {
		fail("No Test");
	}

	@Test
	public void testGetBoardUsers() throws Exception {
		tool.initTestBoard();
		controller.getBoardUsers(TestBoardTool.BOARD_ID, "WRITE");
	}

	@Test
	public void testGetHistory() throws Exception {
		String start = "01-01-2016";
		String end = "12-31-2019";
		Board newBoard = controller.createBoard(this.testBoard);
		String boardId = newBoard.getId();
		this.controller.getHistory(boardId, "alerts", null, start, end);
		this.controller.getHistoryEvent(boardId, "test_phase", "TE10001", "alerts", null);
		this.controller.getHistoryList(boardId, null, start, end, null);
		this.controller.getStatsByStart(boardId, start, start, end, end, start, end, null);
		this.controller.getStatistics(boardId, start, start, start, end, start, end, null);
	}

	@Test
	public void testGetCurrentId() throws Exception {
		Board newBoard = controller.createBoard(this.testBoard);
		String boardId = newBoard.getId();
		try {
			this.controller.getCurrentId(boardId, "TEST");
		} catch (Exception e) {
			assertTrue(e instanceof ResourceNotFoundException);
		}

		try {
			this.controller.getCurrentId(null, "TEST");
		} catch (Exception e) {
			assertTrue(e instanceof ResourceNotFoundException);
		}
	}

	@Test
	public void testSetCurrentId() throws Exception {
		Board newBoard = controller.createBoard(this.testBoard);
		String boardId = newBoard.getId();
		BoardData boardData = new BoardData();
		
		boardData.setId("TEST");
		boardData.setValue(1001);
		
		try {
			this.controller.setId(boardId, boardData);
		} catch (Exception e) {
			assertTrue(e instanceof ResourceNotFoundException);
		}

		try {
			this.controller.setId(null, boardData);
		} catch (Exception e) {
			assertTrue(e instanceof ResourceNotFoundException);
		}
	}

	@Test
	public void getBoardUnqiueTest() throws Exception {
		this.tool.initTestBoard();
		Board newBoard = this.controller.getBoard(BOARD_ID);
		String boardId = newBoard.getId();
		this.controller.getBoard(boardId, "name");

	}

	@Test
	public void getBoardUsersTest() throws Exception {
		this.tool.initTestBoard();
		Board newBoard = this.controller.getBoard(BOARD_ID);
		String boardId = newBoard.getId();
		Map<String, String> perms = this.controller.getPermissions(boardId);
		assertNotNull(perms);
		Map<String, User> users = this.controller.getBoardUsers(boardId, "WRITE");
		assertNotNull(users);
		// Add Permission- No Replace
		this.controller.addPermissions(boardId, false, ImmutableMap.of("readOnly", "READ_ONLY"));
		// Add Permission- Replace
		this.controller.addPermissions(boardId, true, ImmutableMap.of("readOnly", "READ_ONLY"));
	}

	@Test
	public void testSearchTest() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("CONTAINS", "S");
		Collection<Card> results = this.controller.searchCards(BOARD_ID, "name", Operation.CONTAINS, "S", true);
		assertNotNull(results);		
	}
	
	@Test
	public void testSearchWithMapTest() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("CONTAINS", "S");
		Collection<Card> results = this.controller.searchWithMap(BOARD_ID, map);
		assertNotNull(results);
	}
	
	@Test
	public void testSearchWithMapAndViewTest() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("CONTAINS", "S");
		map.put("view", "view");
		map.put("order", "blah");
		map.put("descending", "true");
		Collection<Card> results = this.controller.searchWithMap(BOARD_ID, map);
		assertNotNull(results);
	}

	@Test
	public void boardLockTest() throws Exception {

		Map<String, String> map = new HashMap<>();
		map.put("name", "Peter");
		Card card = this.controller.searchForOne(BOARD_ID, map);
		assertNotNull(card);
		assertTrue(this.controller.lock(BOARD_ID, card.getId()));
		this.controller.getLocks(BOARD_ID);
		this.controller.getLockDetails(BOARD_ID, card.getId());
		this.controller.unlock(BOARD_ID, card.getId());
	}
	
	@Test public void boardUniqueFieldValueTest() throws Exception {
		Set<String> result=this.controller.getBoard(BOARD_ID,"balance");
		assertFalse(result.isEmpty());
	}
	
	@Test public void boardDistinctFieldValueTest() throws Exception {
		List<?> result=this.controller.getDistinctFieldsFromBoard(BOARD_ID,"balance");
		assertFalse(result.isEmpty());
	}
	
	@Test
	public void boardReferenceTest() throws Exception {
		
		Board board1 = this.controller.createBoard( TestBoardTool.getTestBoard("BOARD_101", BoardType.OBJECT));
		Board board2 = this.controller.createBoard( TestBoardTool.getTestBoard("BOARD_102", BoardType.OBJECT));
		
		assertNotNull(this.controller.getBoard(board1.getId()));
		assertNotNull(this.controller.getBoard(board2.getId()));
		
		this.tool.addReferenceField(board2.getId(), "optionfield", board1.getId());
		
		List<Map<String, Object>> boardRefrences = this.controller.getBoardRefrences(board1.getId());
		assertFalse(boardRefrences.isEmpty());
		
		List<Map<String, Object>> boardRefrences2 = this.controller.getBoardRefrences(board2.getId());
		assertTrue(boardRefrences2.isEmpty());
	}
	
	@Test
	public void testUpdateBoardConfigHistory() throws Exception {
		
		Board board = TestBoardTool.getTestBoard("board_config_change_test", BoardType.OBJECT);
		Board newBoard = controller.createBoard(board);
		
		String oldVersion=newBoard.getVersion();
		
		newBoard.setVersion(null);
		newBoard.setUniqueid(null);
		newBoard.setId(null);
		
		this.boardCache.clearCache();
		this.controller.createBoard(newBoard);
		
		List<Map<String, Object>> boardConfigHistory = this.controller.boardConfigHistory("board_config_change_test");
		
		assertTrue(boardConfigHistory.size()==2);
		
		Board boardNew = this.controller.activateBoardVersion("board_config_change_test", oldVersion);
		
		assertTrue(boardNew.isActive());
		
		this.controller.activateBoardVersion("board_config_change_test", oldVersion);
		
		this.controller.deletePermissions("board_config_change_test", null);
	
	}
	
	@Test
	public  void testUpdateBoardConfigHistoryNotFound()
	{
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.activateBoardVersion("board_config_change_test", "unknown_alien_version");
		}, "ResourceNotFoundException was expected");
	}
}
