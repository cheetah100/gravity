/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CredentialControllerTest {
	
	@Autowired
	private CredentialController credentialController;
	
	@Autowired
	private SecurityTool securityTool;
	
	private String msg = "Hi! How are you?";
	
	public static final String CREDID="CRED_TEST";
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
	
	@Test
	public void testCreateCredential() throws Exception {
		Credential cred = new Credential();
		cred.setDescription("test");
		cred.setId(CREDID);
		cred.setName(CREDID);
		cred.setMethod("test");
		cred.setIdentifier("test");
		cred.setRequestMethod("test");
		cred.setResource("test");
		cred.setSecret("test");
		
		
		Credential result = credentialController.createCredential(cred);

		assertEquals(CREDID, result.getId());
	}
	
	@Test
	public void testGetCredential() throws Exception {
		Credential cred = credentialController.getCredential(CREDID);
		assertEquals(CREDID, cred.getName());
	}
	
	@Test
	public void testUpdateCredential() throws Exception {
		String newName = CREDID + "_2";
		Credential cred = new Credential();
		cred.setDescription("test");
		cred.setId(CREDID);
		cred.setName(newName);
		cred.setMethod("test");
		cred.setIdentifier("test");
		cred.setRequestMethod("test");
		cred.setResource("test");
		cred.setSecret("test");
		
		
		Credential result = credentialController.updateCredential(CREDID, cred);

		assertEquals(newName, result.getName());
	}
	
	@Test
	public void testUpdateCredentialNotFoundException() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			Credential cred=new Credential();
			this.credentialController.updateCredential("unknown_cred", cred);

		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	@Disabled
	public void testEncryptAndDecrypt() throws Exception {
		String encrypted = securityTool.encrypt(msg);
		assertNotNull(encrypted);
		String decrypted = securityTool.decrypt(encrypted);
		assertEquals(msg, decrypted);
	}
	
}
