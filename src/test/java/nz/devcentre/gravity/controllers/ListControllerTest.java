/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.ListResource;
import nz.devcentre.gravity.model.Option;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.transfer.ListOption;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ListControllerTest {

	@Autowired
	TestBoardTool tool;
	@Autowired
	ListController listController;
	
	@Autowired
	ViewController viewController;
	
	@Autowired
	CardController cardController;

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void before() throws Exception {
		this.tool.initTestBoard();
		View createView = tool.getTestView("test_view");
		this.viewController.createView(TestBoardTool.BOARD_ID, createView);
	}


	@Test
	@Disabled
	public void getCardListFromBoardTest() {
		try {
			Map<String, Option> cardListFromBoard = this.listController.getCardListFromBoard(TestBoardTool.BOARD_ID,
					null, null, null, "name", null, null);
			
			Collection<Card> cardList = this.cardController.getCards(TestBoardTool.BOARD_ID, null, null, null, null, true, false, false);
			assertTrue(cardList.size()==cardListFromBoard.size());
			
			ListResource list = this.listController.getList(TestBoardTool.BOARD_ID, TestBoardTool.BOARD_ID, null);
			assertTrue(list.getItems().size()==cardListFromBoard.size());
			
			List<ListOption> basicList = this.listController.getBasicList(TestBoardTool.BOARD_ID, TestBoardTool.BOARD_ID, null);
			assertTrue(basicList.size()==cardListFromBoard.size());
			
			Map<String, Map<String, String>> listsForView = this.listController.listsForView(TestBoardTool.BOARD_ID);
			
			assertTrue(listsForView.isEmpty());
			
			//assertTrue(this.listController.listsForView(TestBoardTool.BOARD_ID, "test_view").isEmpty());
			
			//this.listController.getListMap(TestBoardTool.BOARD_ID, TestBoardTool.BOARD_ID, null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
