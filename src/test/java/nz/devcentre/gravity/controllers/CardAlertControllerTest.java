/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardAlert;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.repository.CardAlertRepository;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.AlertService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration 
public class CardAlertControllerTest {
	
	protected static String VIEW_ID = "testview";
	
	@Autowired
	private CardAlertController cardAlertController;
	
	@Autowired
	protected CardAlertRepository cardAlertRepository;

	@Autowired
	protected CardEventRepository cardEventRepository;
	
	@Autowired
	protected AlertService alertService;
	
	CardAlert alert;
	
	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	private SecurityTool securityTool;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
    
	@BeforeEach
	public void before() throws Exception {		
		tool.initTestBoard();
		MockitoAnnotations.openMocks(this);
		//cardAlertController.setAlertRepository(cardAlertRepository);
	}
	 
	@Test
	public void testGetAlert() throws Exception {
		
		securityTool.iAmSystem();
		
		Card card = tool.getTestCard();		

		CardAlert saveAlert = alertService.storeAlert("Test Alert", card.getBoard(), card.getId(), "alert", "system", null, null);
		
		CardEvent readAlert = cardAlertController.getAlert(saveAlert.getUuid());
		
		assertEquals(saveAlert.getUuid(), readAlert.getUuid());
		
		Collection<CardAlert> before = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, card.getId());
		assertEquals(1,before.size());
		
		cardAlertController.dismissAlert(TestBoardTool.BOARD_ID, saveAlert.getUuid());

		Collection<CardAlert> after = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, card.getId());
		assertEquals(0,after.size());
	}	
	
	
	@Test
	public void testSaveAndDismissAlerts() throws Exception {
		
		securityTool.iAmSystem();
		
		Card card = tool.getTestCard();		
		assertNotNull(card);

		CardAlert saveAlert = alertService.storeAlert("Test Alert", card.getBoard(), card.getId(), "alert", "system", null, null);
		
		assertNotNull(saveAlert);
		
		Collection<CardAlert> before = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, card.getId());
		assertEquals(1,before.size());
		
		cardAlertController.dismissAlert(TestBoardTool.BOARD_ID,saveAlert.getUuid());

		Collection<CardAlert> after = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, card.getId());
		assertEquals(0,after.size());
	}	
	
	
	@Test
	public void testSaveAlert() throws Exception {
		securityTool.iAmSystem();
		
		Map<String, Object> data = new HashMap<String, Object>();
		
		data.put("value", "save alert");
		data.put("user", "test user");
		data.put("time", null);
		data.put("level", "Test level");
				
		Card testCard = tool.getTestCard();
		
		Collection<CardAlert> before = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, testCard.getId());
		assertEquals(0,before.size());
		
		CardAlert alert = new CardAlert();
		alert.setBoard(TestBoardTool.BOARD_ID);
		alert.setCard(testCard.getId());
		alert.setCategory("test");
		
		CardEvent testEvent = cardAlertController.saveAlert( alert);
		
		Collection<CardAlert> after = cardAlertController.getAlerts(TestBoardTool.BOARD_ID, testCard.getId());
		assertEquals(1,after.size());
		
		cardAlertController.dismissAlert(TestBoardTool.BOARD_ID, testEvent.getUuid());
	}
}

