/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Application;
import nz.devcentre.gravity.model.Module;
import nz.devcentre.gravity.model.Widget;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ApplicationControllerTest {
	
	private static final String TEST_APP = "test_app";
	private static final String WIDGET_ID = "test_widget";
	private static final String CREATE_MODULE_TEST = "create_module_test";
	
	@Autowired
	private ApplicationController controller;
	
	@Autowired
	private ApplicationCache cache;
	
	@Autowired
	private SecurityTool securityTool;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
	
	@BeforeEach
	public void setup() throws Exception {
		try {
			controller.getApplication(TEST_APP);
		} catch( ResourceNotFoundException e){
			Application app = getTestApplication();
			controller.createApplication(app);
			cache.clearCache();
			securityTool.iAmSystem();
		}
	}
	
	@Test
	public void testGetApplication() throws Exception{
		Application app = controller.getApplication(TEST_APP);
		assertNotNull(app);
		assertEquals( TEST_APP, app.getId());
		assertEquals( TEST_APP, app.getName());
		// assertEquals( 2, app.getPermissions().size());
	}
	
	@Test
	public void testGetApplicationNotFound() throws Exception{
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			controller.getApplication("notfound");
		}, "Exception was expected");
		
	}
	
	@Test
	public void testGetModule() throws Exception{
		Module module = controller.getModule(TEST_APP, "two");
		module = controller.createModule(module, TEST_APP);
		assertNotNull(module);
		assertEquals( "two", module.getId());
		assertEquals( "two", module.getName());
	}
	
	@Test
	public void testGetModuleNotFound() throws Exception{
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			controller.getModule(TEST_APP, "notfound");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testGetModuleAppNotFound() throws Exception{
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			controller.getModule("notfound", "notfound");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testListApplications() throws Exception{
		Map<String, String> listApplications = controller.listApplications(false);
		assertNotNull(listApplications);
		assertEquals( 1, listApplications.size());
		String next = listApplications.keySet().iterator().next();
		assertEquals( TEST_APP, next);
		
		listApplications = controller.listApplications(true);
		assertNotNull(listApplications);
		assertEquals( 1, listApplications.size());
		next = listApplications.keySet().iterator().next();
		assertEquals( TEST_APP, next);
	}
	
	@Test
	public void testGetModuleUI() throws Exception{
		Map<String, Object> moduleUI = controller.getModuleUI(TEST_APP, "one");
		assertNotNull(moduleUI);
		assertEquals( "value", moduleUI.get("field"));
	}
	
	@Test
	public void testGetModuleUISingle() throws Exception{
		Object moduleUI = controller.getModuleUI(TEST_APP, "one", "field");
		assertNotNull(moduleUI);
		assertEquals( "value", moduleUI);
	}
	
	@Test
	public void testCreateModule() throws Exception{
		Module module = getTestModule("Create Module Test");
		controller.createModule(module, TEST_APP);
		Module result = controller.getModule(TEST_APP,CREATE_MODULE_TEST);
		assertEquals(CREATE_MODULE_TEST, result.getId());
	}
	
	@Test
	@Disabled
	public void testGetCreateModulePathProvided() throws Exception{
		Module module = getTestModule("Create Module Test");
		//module.setPath("some/junk/path");
		Assertions.assertThrows(Exception.class, () -> {
			controller.createModule(module, TEST_APP);
		}, "Exception was expected");
	}
	
	@Test
	public void testReplaceRoles() throws Exception{
		Map<String,String> newRoles = new HashMap<String,String>();
		newRoles.put("replace-test", "WRITE");
		controller.addPermissions(TEST_APP, true, newRoles);
		cache.clearCache();
		
		Map<String, String> roles = controller.getPermissions(TEST_APP);
		assertNotNull(roles);
		assertEquals("WRITE", roles.get("replace-test"));

	}
	
	@Test
	public void testAddAndDeleteRoles() throws Exception{
		
		Map<String,String> newPermissions = new HashMap<String,String>();
		newPermissions.put("add-test", "WRITE");
		controller.addPermissions(TEST_APP, false, newPermissions);
		cache.clearCache();
		
		Map<String, String> permissions = controller.getPermissions(TEST_APP);
		assertNotNull(permissions);
		assertEquals("WRITE", permissions.get("add-test"));
		
		controller.deletePermission(TEST_APP, "add-test");
		cache.clearCache();
		
		permissions = controller.getPermissions(TEST_APP);
		assertNotNull(permissions);
		assertNull(permissions.get("add-test"));
	}
	
	@Test
	public void testUpdateApplication() throws Exception{
		Application app = controller.getApplication(TEST_APP);
		app.setName("Updated App");
		controller.updateApplication(app, TEST_APP);
		cache.clearCache();
		Application updatedApp = controller.getApplication(TEST_APP);
		assertEquals("Updated App", updatedApp.getName());
		updatedApp.setName("test_app");
		controller.updateApplication(updatedApp, TEST_APP);
		cache.clearCache();
	}

	@Test
	public void testUpdateModule() throws Exception{
		Module module = controller.getModule(TEST_APP, "one");
		module.setName("Updated Module");
		controller.updateModule(module, TEST_APP, "one");
		cache.clearCache();
		Module updatedModule = controller.getModule(TEST_APP, "one");
		assertEquals("Updated Module", updatedModule.getName());
	}

	@Test
	public void testUpdateModuleUI() throws Exception{
		
		List<Object> testList = new ArrayList<Object>();
		testList.add("test_ui");
		controller.updateModuleUI(testList, TEST_APP, "one", "update");
		cache.clearCache();
		
		Object item = controller.getModuleUI(TEST_APP, "one", "update");
		assertNotNull(item);
		if(!(item instanceof Collection)){
			fail("Item not instance of List- is " + item.getClass().getCanonicalName());
		}
	}

	@Test
	public void testZZDeleteApplication() throws Exception{
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			controller.deleteApplication(TEST_APP);
			cache.clearCache();
			controller.getApplication(TEST_APP);

		}, "Exception was expected");
	}
	
	@Test
	public void testCreateWidget() throws Exception {
		Widget widget = new Widget();
		widget.setName("HeatMap");
		widget.setTitle("Widget title");
		widget.setDraggable(true);
		Widget retVal = controller.createWidget(widget, TEST_APP);
		// Delete the widget - For other tests to continue
		controller.deleteWidget(TEST_APP, retVal.getId());
	}
	
	@Test
	public void testGetWidget() throws Exception {
		Widget widget = new Widget();
		widget.setName(WIDGET_ID);
		widget.setTitle("Widget title");
		widget.setDraggable(true);
		widget.setPhase("unpublished");
		Widget result = controller.createWidget(widget, TEST_APP);
		Widget newWiget = controller.getWidget(TEST_APP, result.getId());
		// Delete the widget - For other tests to continue
		controller.deleteWidget(TEST_APP, newWiget.getId());
	}

	@Test
	public void testGetWidgets() throws Exception {
		Widget widget1 = new Widget();
		widget1.setName(WIDGET_ID);
		widget1.setTitle("Widget title");
		widget1.setDraggable(true);
		widget1.setPhase("unpublished");
		widget1 = controller.createWidget(widget1, TEST_APP);
		Widget widget2 = new Widget();
		widget2.setName(WIDGET_ID + "2");
		widget2.setTitle("Widget title 2");
		widget2.setDraggable(true);
		widget2.setPhase("unpublished");
		widget2 = controller.createWidget(widget2, TEST_APP);
		List<Widget> retVal = controller.getWidgetsByApplication(TEST_APP);
		assertEquals(2, retVal.size());
		// Delete the widget - For other tests to continue
		controller.deleteWidget(TEST_APP, widget1.getId());
		controller.deleteWidget(TEST_APP, widget2.getId());
	}

	@Test
	public void testGetWidgetsByPhase() throws Exception {
		Widget widget1 = new Widget();
		widget1.setName(WIDGET_ID);
		widget1.setTitle("Widget title");
		widget1.setDraggable(true);
		widget1.setPhase("unpublished");
		widget1 = controller.createWidget(widget1, TEST_APP);
		Widget widget2 = new Widget();
		widget2.setName(WIDGET_ID + "2");
		widget2.setTitle("Widget title 2");
		widget2.setDraggable(true);
		widget2.setPhase("published");
		widget2 = controller.createWidget(widget2, TEST_APP);
		List<Widget> retVal = controller.getWidgetsByApplicationAndPhase(TEST_APP, "unpublished");
		assertEquals(1, retVal.size());
		// Delete the widget - For other tests to continue
		controller.deleteWidget(TEST_APP, widget1.getId());
		controller.deleteWidget(TEST_APP, widget2.getId());
	}
	
	@Test
	public void testCreateAllWidgets() throws Exception {
		Widget widget1 = new Widget();
		widget1.setName(WIDGET_ID);
		widget1.setTitle("Widget title");
		widget1.setDraggable(true);
		widget1.setPhase("unpublished");
		Widget widget2 = new Widget();
		widget2.setName(WIDGET_ID + "2");
		widget2.setTitle("Widget title 2");
		widget2.setDraggable(true);
		widget2.setPhase("published");
		List<Widget> widgets = new ArrayList<Widget>();
		widgets.add(widget1);
		widgets.add(widget2);
		controller.createAllWidgets(widgets,TEST_APP);
		List<Widget> retVal = controller.getWidgetsByApplication(TEST_APP);
		assertEquals(2, retVal.size());
		
		Map<String, String> widgetListByApplication = this.controller.getWidgetListByApplication(TEST_APP);
		assertEquals("Widget title 2", widgetListByApplication.get("WI000002"));
		// Delete the widget - For other tests to continue
		controller.deleteWidget(TEST_APP, widget1.getId());
		controller.deleteWidget(TEST_APP, widget2.getId());
	}
	
	
	private Map<String, Object> getTestUI(String field, String value) {
		Map<String,Object> ui = new HashMap<String,Object>();
		ui.put(field, value);
		return ui;
	}
	
	private Map<String, String> getTestRoles(String name, String access) {
		Map<String,String> roles = new HashMap<String,String>();
		roles.put(name, access);
		return roles;
	}
	
	private Application getTestApplication() throws Exception{
		Application app = new Application();
		app.setName(TEST_APP);
		app.setId(TEST_APP);
		app.setDescription("Test Description");
		Map<String,Module> modules = new HashMap<String,Module>();
		modules.put("one", getTestModule("one"));
		modules.put("two", getTestModule("two"));
		modules.put("three", getTestModule("three"));
		app.setModules(modules);
		return app;
	}
	
	private Module getTestModule( String name ) throws Exception {
		Module mod = new Module();
		mod.setName(name);
		mod.setDescription(name);
		mod.setPermissions(getTestRoles("test_user","ADMIN"));
		mod.setUi(getTestUI("field","value"));
		return mod;
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testListApplicationDescriptions() throws Exception {
		Map<String, Object> listApplicationDescriptions = this.controller.listApplicationDescriptions();
		Object object = listApplicationDescriptions.get(TEST_APP);
		assertTrue(object instanceof Map);
		Map<String,String> appItem=(Map<String, String>) object;
		assertTrue(appItem.containsKey("name") && appItem.containsKey("description"));
		assertEquals("Test Description",appItem.get("description"));
		assertEquals(TEST_APP,appItem.get("name"));
	}
	
	@Test
	public void testListModules() throws Exception {
		Map<String, String> listModules = this.controller.listModules(TEST_APP);
		assertTrue(listModules.containsKey("one"));
		assertEquals("one", listModules.get("one"));
	}
	
	
	
	@Test
	public void testVersionApplication() throws Exception {
		Application application = this.controller.getApplication(TEST_APP);
		String currentVersion=application.getVersion();
		
		application.setVersion(null);
		application.setId(TEST_APP);
		application.setUniqueid(null);
		
		this.controller.createApplication(application);
		application = this.controller.getApplication(TEST_APP);
		assertNotEquals(currentVersion, application.getVersion());
		
		List<Application> oldApplication = this.controller.getOldApplication(TEST_APP);
		assertFalse(oldApplication.isEmpty());

		application =this.controller.activateApplication(TEST_APP, currentVersion);
		this.cache.clearCache();
		oldApplication = this.controller.getOldApplication(TEST_APP);
		assertFalse(oldApplication.isEmpty());
		application = this.controller.getApplication(TEST_APP);
		assertEquals(currentVersion, application.getVersion());
		
	}
}
