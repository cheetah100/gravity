/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.transfer.CardResult;
import nz.devcentre.gravity.model.transfer.CardUpdateEnvelope;
import nz.devcentre.gravity.model.transfer.MoveCard;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.tools.CardTools;

@ExtendWith(SpringExtension.class)
@ContextConfiguration // (locations = { "/test-controllers.xml" })
public class CardControllerTest {

	protected static String VIEW_ID = "testview";

	@Autowired
	CardController controller;
	
	@Autowired
	BoardController boardController;

	@Autowired
	TestBoardTool tool;

	@Autowired
	private FilterController filterController;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private SecurityTool securityTool;

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void before() throws Exception {
		tool.initTestBoard();
		this.securityTool.iAmSystem();
	}

	@Test
	public void testGetCard() throws Exception {
		Card card = controller.getCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, null, false, false);
		assertNotNull(card);
		
		// validate references and allfields
		card = controller.getCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, null, true, true);
		assertNotNull(card);

		// Test Overload Method
		card = controller.getCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, null, false);
		assertNotNull(card);
	}

	@Test
	public void testGetCard2() throws Exception {

		Card card = controller.getCard2(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID);
		assertNotNull(card);

		// Test Card to Map
		Map<String, Object> cardMap = CardTools.cardToMap(card);
		assertFalse(cardMap.isEmpty());

		// Test Card to Map -card as NULL
		cardMap = CardTools.cardToMap(null);
		assertNull(cardMap);

		try {
			card = controller.getCard2(TestBoardTool.BOARD_ID, "INVALID_ID");
			fail("Exception Thrown");
		} catch (ResourceNotFoundException exception) {
			assertTrue(true);
		}
		try {
			card = controller.getCard2(TestBoardTool.BOARD_ID, null);
			fail("Exception Thrown");
		} catch (ResourceNotFoundException exception) {
			assertTrue(true);
		}

	}
	
	@Test
	public void testGetCardWithNoCard() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			controller.getCard(TestBoardTool.BOARD_ID, "", null, false, false);
		}, "ResourceNotFoundException was expected");
		
	}

	@Test
	public void testCreateDeleteCard() throws Exception {

		Card createdCard = controller.createCard(TestBoardTool.BOARD_ID,
				TestBoardTool.getTestCard("Create Test", TestBoardTool.BOARD_ID, 300), false);

		assertNotNull(createdCard);

		Card foundCard = controller.getCard(TestBoardTool.BOARD_ID, createdCard.getId(), "all", false, false);

		assertNotNull(foundCard);
		assertEquals("Create Test", foundCard.getFields().get("name"));

		Object balance = foundCard.getFields().get("balance");
		assertEquals("java.lang.Double", balance.getClass().getName());
		assertEquals(300.0, balance);

		controller.deleteCard(TestBoardTool.BOARD_ID, foundCard.getId());

		try {
			foundCard = controller.getCard(TestBoardTool.BOARD_ID, createdCard.getId(), null, false, false);
			fail("Should throw ResourceNotFound");
		} catch (ResourceNotFoundException e) {
			// Do Nothing
		}
	}

	/* Deprecated API
	@Test
	public void testUpdateField() throws Exception {

		Card card = tool.getTestCard();

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("field", "name");
		body.put("value", "updatedValue");

		controller.updateField(TestBoardTool.BOARD_ID, card.getId(), "name", body);

		Card updatedCard = controller.getCard(TestBoardTool.BOARD_ID, card.getId(), "all", false, false);

		Object object = updatedCard.getFields().get("name");

		assertTrue(object instanceof String);
		assertEquals(object, "updatedValue");

	}
	*/

	@Test
	public void testExamineCard() throws Exception {
		Boolean card = controller.examineCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID);
		assertNotNull(card);
	}
	
	/*
	@Test
	public void testExecuteRuleOnCardTaskSuccess() throws Exception {
		String response = controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.TASK_ID);
		assertNotNull(response);
	}

	@Test
	public void testExecuteRuleOnCardCompulsorySuccess() throws Exception {
		String response = controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.RULE_ID);
		assertNotNull(response);
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void testExecuteRuleOnCardNoCard() throws Exception {
		controller.executeRuleOnCard(TestBoardTool.BOARD_ID, "fail", TestBoardTool.TASK_ID);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testExecuteRuleOnCardNoBoard() throws Exception {
		controller.executeRuleOnCard("fail", TestBoardTool.CARD_ID, TestBoardTool.TASK_ID);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testExecuteRuleOnCardNoRule() throws Exception {
		controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, "fail");
	}
	
	@Test
	public void testExecuteTaskOnCardTaskConditionsNotMet() throws Exception {
		String response = controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.TASK_ID3);
		assertTrue(response.contains("Task conditions not met"));
	}
	
	@Test
	public void testExecuteTaskOnCardAutomationConditionsNotMet() throws Exception {
		String response = controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.TASK_ID4);
		assertTrue(response.contains("Automation conditions not met"));
	}

	@Test
	public void testExecuteRuleOnCardAutomationConditionsNotMet() throws Exception {
		String response = controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.TASK_ID5);
		assertTrue(response.contains("Automation conditions not met"));
	}

	
	@Test(expected = ResourceNotFoundException.class)
	public void testExecuteRuleOnCardResourceNotFoundException() throws Exception {
		controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, "InvalidRuleId");
	}
	
	@Test(expected = AutomationExecutionException.class)
	public void testExecuteTaskOnCardAutomationExecutionException() throws Exception {
		controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.TASK_ID2);
	}
	
	@Test(expected = AutomationExecutionException.class)
	public void testExecuteRuleOnCardAutomationExecutionException() throws Exception {
		controller.executeRuleOnCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, TestBoardTool.RULE_ID2);
	}
	
	*/

	@Test
	public void testExplainCard() throws Exception {
		Map<String, Map<String, Boolean>> explain = controller.explainCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID);
		assertNotNull(explain);
	}

	@Test
	public void testMoveCardWithReason() throws Exception {
		Card createdCard = controller.createCard(TestBoardTool.BOARD_ID,
				TestBoardTool.getTestCard("Create Test", TestBoardTool.BOARD_ID, 300), false);

		MoveCard move = new MoveCard();
		
		controller.moveCardWithReasons(TestBoardTool.BOARD_ID, createdCard.getId(), move);// Empty Phase

		move.setPhase("next-phase");
		move.setReason("Test");

		Card card = controller.moveCardWithReasons(TestBoardTool.BOARD_ID, createdCard.getId(), move);
		assertNotNull(card);

		move.setPhase("new-phase");
		
		controller.deleteCard(TestBoardTool.BOARD_ID, card.getId());
		try {
			controller.moveCardWithReasons(TestBoardTool.BOARD_ID, card.getId(), move);
		} catch (Exception e) {
			// TODO: handle exception
			assertTrue(e instanceof ResourceNotFoundException);
		}

	}

	@Test
	public void testCreateCard() throws Exception {
		Card createdCard = TestBoardTool.getTestCard("Create Test", TestBoardTool.BOARD_ID, 300);
		createdCard.addField("phase", "current");
		this.controller.createCard(TestBoardTool.BOARD_ID, createdCard, false);

		assertNotNull(createdCard);

		controller.deleteCard(TestBoardTool.BOARD_ID, createdCard.getId());

	}

	@Test
	public void testCreateAllCard() throws Exception {
		Card createdCard1 = controller.createCard(TestBoardTool.BOARD_ID,
				TestBoardTool.getTestCard("Create Test1", TestBoardTool.BOARD_ID, 300), false);

		Card createdCard2 = controller.createCard(TestBoardTool.BOARD_ID,
				TestBoardTool.getTestCard("Create Test2", TestBoardTool.BOARD_ID, 300), false);

		List<Card> cardList = new ArrayList<Card>();
		cardList.add(createdCard1);
		cardList.add(createdCard2);

		List<CardResult> cardResults = controller.createAllCards(TestBoardTool.BOARD_ID, cardList, false);

		assertNotNull(cardResults);

		for (CardResult result : cardResults) {
			controller.deleteCard(TestBoardTool.BOARD_ID, result.getCard().getId());
		}
		cardResults = controller.createAllCards("INVALID_BOARD_ID", cardList, false);
	}

	@Test
	public void testCreateCardFromFields() throws Exception {
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", "John D");
		fields.put("phone", "0201000999");
		fields.put("address", "Warehouse Way, Auckland");
		fields.put("balance", "100");

		Card card = controller.createCardFromFields(TestBoardTool.BOARD_ID, TestBoardTool.TEMPLATE_ID, "Black", fields);

		assertNotNull(card);

		controller.deleteCard(TestBoardTool.BOARD_ID, card.getId());
	}

	@Test
	public void testFilterConditionReplacement() throws Exception {
		tool.generateFilters();
		Filter filter = filterController.getFilter(TestBoardTool.BOARD_ID, "replacementFilter");
		assertNotNull(filter);
		Collection<Card> list = controller.getCards(TestBoardTool.BOARD_ID, "test_phase", "replacementFilter", null,
				"name", true, false, false);
		assertTrue(list.size() == 1);
		Card card = (Card) ((ArrayList<Card>) list).get(0);
		assertTrue("Tim".equals(card.getField("name")));
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", "Kevin|Tim");
		list = controller.getCardsDynamicFilter(TestBoardTool.BOARD_ID, "test_phase", "replacementFilter", null, "name",
				true, false, false, false, parameters);
		assertTrue(list.size() == 2);
		card = (Card) ((ArrayList<Card>) list).get(0);
		assertTrue("Kevin".equals(card.getField("name")));
	}

	@Test
	public void testUpdateCard() throws Exception {
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("field", "name");
		body.put("value", "updatedValue");

		Card card = tool.getTestCard();

		Card updatedCard = controller.updateCard(TestBoardTool.BOARD_ID, card.getId(), body, null, false);

		Object object = updatedCard.getFields().get("value");

		assertTrue(object instanceof String);
		assertEquals(object, "updatedValue");
	}

	@Test
	public void testGetCardsDetailWithOrder() throws Exception {
		tool.initTestBoard();

		Collection<Card> cardsDetail = controller.getCards(TestBoardTool.BOARD_ID, null, null, "all", "name", false,
				false, false);
		// assertEquals(9, cardsDetail.size());
		assertTrue(cardsDetail.size() > 0);
		Card card = cardsDetail.iterator().next();
		// Sometimes the test returns Darren when run by itself... when all tests are
		// ran at a go it returns Joe as Darren's record may be getting archived
		// assertTrue("Darren".equals(card.getFields().get("name")) ||
		// "Joe".equals(card.getFields().get("name")));
	}

	@Test
	public void testGetCardsDetailWithBoardOrder() throws Exception {
		tool.initTestBoard();
		Collection<Card> cardsDetail = controller.getCards(TestBoardTool.BOARD_ID, null, null, null, "name", false,
				false, false);
		assertTrue(cardsDetail.size() > 0);
		Card card = cardsDetail.iterator().next();
		// assertTrue("Darren".equals(card.getFields().get("name")) ||
		// "Joe".equals(card.getFields().get("name")));
	}

	@Test
	public void testGetCardsDetailNoOrder() throws Exception {
		tool.initTestBoard();
		Collection<Card> cards = controller.getCards(TestBoardTool.BOARD_ID, null, null, null, null, false,
				false, false);
		assertEquals(8, cards.size());
	}

	@Test
	@Disabled
	public void getCardListTest() throws Exception {
		//Map<String, String> getCardList = this.controller.getCardList(TestBoardTool.BOARD_ID, "test_phase", null,
		//		false);
		//assertFalse(getCardList.isEmpty());

		List<String> cardIds = new ArrayList<>();
		//cardIds.addAll(getCardList.keySet());
		List<Card> cardList = this.controller.getCardListById(TestBoardTool.BOARD_ID, null, true, cardIds);
		assertFalse(cardList.isEmpty());
		// Cards to List
		List<Map<String, Object>> cardsToList = CardTools.cardsToList(cardList);
		assertFalse(cardsToList.isEmpty());

		//getCardList = this.controller.getCardList(TestBoardTool.BOARD_ID, "archive", null, false);
		//assertTrue(getCardList.isEmpty());

	}

	@Test
	public void testCreateCardOverload() throws Exception {

		Card createdCard = controller.createCard(TestBoardTool.BOARD_ID,
				TestBoardTool.getTestCard("Create Test2", TestBoardTool.BOARD_ID, 300));
		assertNotNull(createdCard);
		List<Card> cardList = new ArrayList<>();
		cardList.add(TestBoardTool.getTestCard("Test Card1", TestBoardTool.BOARD_ID, 100));
		cardList.add(TestBoardTool.getTestCard("Test Card2", TestBoardTool.BOARD_ID, 200));
		List<CardResult> result = this.controller.createAllCards(TestBoardTool.BOARD_ID, cardList);
		assertFalse(result.isEmpty());
	}

	@Test
	public void testUpdateCardOverLoad() throws Exception {
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("field", "name");
		body.put("value", "updatedValue");
		body.put("phase", "next-phase");
		Map<String, Object> reasons = new HashMap<String, Object>();
		reasons.put("phase", "Unit Testing");
		reasons.put("value", "Unit Testing");
		body.put("reasons", reasons);

		Card card = tool.getTestCard();

		Card updatedCard = this.controller.updateCard(TestBoardTool.BOARD_ID, card.getId(), body, null);

		Object object = updatedCard.getFields().get("value");

		assertTrue(object instanceof String);
		assertEquals(object, "updatedValue");

		this.controller.updateCard(TestBoardTool.BOARD_ID, card.getId(), body, null);// no update

		body.clear();
		body.put("phase", "archive");
		updatedCard = this.controller.updateCard(TestBoardTool.BOARD_ID, card.getId(), body, null);// phase archive
		assertTrue(updatedCard.getPhase().equalsIgnoreCase("archive"));
	}

	@Test
	public void updateCardsBulkTest() throws Exception {
		Card card1 = tool.getTestCard();
		Card card2 = tool.getTestCard();
		CardUpdateEnvelope envelope = new CardUpdateEnvelope();

		List<String> ids = new ArrayList<>();
		ids.add(card1.getId());
		ids.add(card2.getId());
		envelope.setIds(ids);
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("phase", "archive");
		envelope.setFields(body);
		Map<String, String> result = this.controller.updateCards(TestBoardTool.BOARD_ID, envelope, null);
		// assertTrue(result.size()>0);

	}
	@Test
	public void testGetCardsPaginated() {
		try {
			this.controller.getCardsPaginated(TestBoardTool.BOARD_ID, null, null, null, null, true, true, 1, 1, false);
		}catch (Exception e) {
			// TODO: handle exception
			
		}
		
	}
	
	@Test
	@Disabled
	/**
	 * Need to examine whether this API is needed.
	 * @throws Exception
	 */
	public void testCardReferences() throws Exception {
		
		Board board1=this.boardController.createBoard(TestBoardTool.getTestBoard("board_101", BoardType.OBJECT));
		Board board2=this.boardController.createBoard(TestBoardTool.getTestBoard("board_102", BoardType.OBJECT));
		this.tool.addReferenceField(board1.getId(), "optionfield", board2.getId());
		board1 = this.boardController.getBoard(board1.getId());
		board2 = this.boardController.getBoard(board2.getId());
		
		Card createdCardForBoard1 = controller.createCard(board1.getId(),
				TestBoardTool.getTestCard("Create Test10", board1.getId(), 300));
		
		Card createdCardForBoard2 = controller.createCard(board2.getId(),
				TestBoardTool.getTestCard("Create Test10", board2.getId(), 300));
		
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("optionfield", createdCardForBoard1.getId());
		
		controller.updateCard(board2.getId(), createdCardForBoard2.getId(), body, null);
		
		//Test Card Reference
		List<Map<String, Object>> cardDetailsExtra = this.controller.getCardDetailsExtra(board1.getId(), createdCardForBoard1.getId());
		assertFalse(cardDetailsExtra.isEmpty());
		
		//Test Card Reference consolidated
		List<Map<String, Object>> cardDetailsConsolidated = this.controller.getCardDetailsConsolidated(board1.getId(), createdCardForBoard1.getId());
		assertFalse(cardDetailsConsolidated.isEmpty());
		
		//Test Card Reference Examine
		boolean examineCardReferences = this.controller.examineCardReferences(board1.getId(), createdCardForBoard1.getId());
		assertTrue(examineCardReferences);
	}
	
	@Test
	public void  testCardServiceBuildFacetQuery() {
		String buildFacetQuery = this.cardService.buildFacetQuery(10, 10, false);
		assertTrue(buildFacetQuery.length() > 0);
		String buildFacetQuery2 = this.cardService.buildFacetQuery(1, 1, true);
		assertTrue(buildFacetQuery2.length() > 0);
	}
	
}
