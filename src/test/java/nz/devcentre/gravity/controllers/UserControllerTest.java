/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Application;
import nz.devcentre.gravity.model.Preference;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {
	
	@Autowired
	private UserController userController;

	@Mock
	SecurityTool securityTool=Mockito.mock(SecurityTool.class);

	@Autowired
	private TeamController teamController;
	
	public static final String TEAMID="USER_TEST_TEAM";
	
	@Configuration
	@Import({ FongoConfiguration.class})
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void createUserTest() throws Exception {
		Team team = new Team();
		team.setName(TEAMID);
		team.setId(TEAMID);
		team.setRoles(ImmutableMap.of("ADMIN", "ADMIN"));
		team.setMembers(ImmutableMap.of("system", "system"));
		this.teamController.createTeam(team);
		
		User user=new User();
		user.setName("bond");
		user.setEmail("bond007@cisco.com");
		user.setFirstname("James");
		user.setSurname("Bond");
		user.setTeams(ImmutableMap.of(TEAMID,""));
		
		user=this.userController.createUser(user);
		assertTrue(user.getId().equals("bond"));
	}
	
	@Test
	public void createUserTestExistsException() throws Exception {
		
		User user=new User();
		user.setId("bond");
		user.setName("bond");
		user.setEmail("bond007@cisco.com");
		user.setFirstname("James");
		user.setSurname("Bond");
		user.setTeams(ImmutableMap.of(TEAMID,""));
		
		Assertions.assertThrows(ValidationException.class, () -> {
			this.userController.createUser(user);
		}, "ValidationException was expected");
	}
	
	@Test
	public void listUserTest() throws Exception {
		Map<String, String> listUsers = this.userController.listUsers();
		assertTrue(listUsers.containsKey("bond"));
	}

	@Test
	public void getUserTest() throws Exception {
		User user = this.userController.getUser("bond");
		assertTrue(user.getTeams().containsKey(TEAMID));
		assertTrue("bond007@cisco.com".equals(user.getEmail()));
	}

	@Test
	public void updateUserTeamsTest() throws Exception {
		Map<String, String> updateUserTeams = this.userController.updateUserTeams("bond",new HashMap<>());
		assertTrue(updateUserTeams.isEmpty());
		
		updateUserTeams = this.userController.updateUserTeams("bond",ImmutableMap.of(TEAMID,""));
		assertFalse(updateUserTeams.isEmpty());
	}
	
	@Test
	public void updateUserTest() throws Exception {
		
		User user=new User();
		
		user.setName("bond");
		user.setEmail("bond007@gmail.com");
		user.setFirstname("James");
		user.setSurname("Bond");
		user.setTeams(ImmutableMap.of(TEAMID,""));
		
		this.userController.updateUser("bond", user);
		
		User user2 = this.userController.getUser("bond");
		
		assertTrue(user2.getEmail().equalsIgnoreCase(user.getEmail()));
		
		//All Blank or Null values should not change the existing user
		user.setTeams(null);
		user.setFirstname(null);
		user.setSurname(null);
		user.setName(null);
		User updateUser = this.userController.updateUser("bond",user);
		assertTrue(updateUser.getTeams().equals(user2.getTeams()));
		assertTrue(updateUser.getName().equals(user2.getName()));
		
	}
	@Test
	public void updateUserNotFoundException() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			User user=new User();
			this.userController.updateUser("unknown_alien",user);
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void getUserNotFoundExceptionTest() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.userController.getUser("unknown_alien");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void getUserNotFoundException2Test() throws Exception {
		Assertions.assertThrows(ValidationException.class, () -> {
			this.userController.getUser("");
		}, "ValidationException was expected");
	}
	
	@Test
	public void getUserApplicationTest() throws Exception {
		Application application=new Application();
		application.setName("dummy app");
		application.setId("dummyApp");
		application.setDescription("Testing Get App for User Mock");
		
		ReflectionTestUtils.setField(this.userController, "defaultApplication", "dummyApp");
		ReflectionTestUtils.setField(this.userController, "applicationCache", Mockito.mock(ApplicationCache.class));
		Mockito.doReturn(application).when(this.userController.applicationCache).getItem("dummyApp");
		
		Application app = this.userController.getApplication();
		assertEquals(application.getDescription(), app.getDescription());
		
		Preference pref=new Preference();
		pref.setName("dummy");
		pref.setUserId("dummy");
		pref.setValues(ImmutableMap.of("app","dummyApp"));
		
		
		ReflectionTestUtils.setField(this.userController, "userPreferencesController", Mockito.mock(UserPreferencesController.class));
		Mockito.doReturn(pref).when(this.userController.userPreferencesController).getPreference("app");
		
		app = this.userController.getApplication();
		assertEquals(application.getDescription(), app.getDescription());
	}
	
	@Test
	public void zzzzTestDeleteUser() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.userController.deleteUser("bond");
			this.userController.getUser("bond");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void zzzTestDeleteUser() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.userController.deleteUser("");
		}, "ResourceNotFoundException was expected");
	}
}
