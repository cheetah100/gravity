/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class RuleControllerTest {

	@Autowired
	private RuleController controller;
	
	@Autowired
	TestBoardTool tool;
	
	@Autowired
	private RuleCache ruleCache;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
    
	@BeforeEach
	public void before() throws Exception {
		tool.initTestBoard();
	}

	@Test
	public void testCreate() throws Exception {
		Rule rule = getTestRule("Test Rule1", "name", Operation.EQUALTO, "Smith", ConditionType.PROPERTY);
		Rule newRule = controller.createRule(TestBoardTool.BOARD_ID, rule);
		ruleCache.clearCache();
		
		Rule loadedRule = controller.getRule(TestBoardTool.BOARD_ID, newRule.getId());
		assertEquals("Test Rule1",loadedRule.getName());
		
		loadedRule.setName("Updated Rule");
		controller.updateRule(TestBoardTool.BOARD_ID, newRule.getId(), loadedRule);
		
		Rule loadedRule2 = controller.getRule(TestBoardTool.BOARD_ID, newRule.getId());
		assertEquals("Updated Rule",loadedRule2.getName());
	}
	
	@Test
	public void testListRules() throws Exception {
		Rule rule = getTestRule("Test Rule2", "name", Operation.EQUALTO, "Smith", ConditionType.PROPERTY);
		controller.createRule(TestBoardTool.BOARD_ID, rule);
		ruleCache.clearCache();
		Map<String, String> listRules = controller.listRules(TestBoardTool.BOARD_ID,true);
		assertTrue(listRules.containsKey("test_rule2"));
		
		controller.listRules(TestBoardTool.BOARD_ID,false);
		assertFalse(listRules.isEmpty());
	}
	
	protected static  Rule getTestRule(String ruleName, 
			String conditionName, 
			Operation operation, 
			String value, 
			ConditionType type) throws IOException {
		
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		Condition condition = new Condition(conditionName,operation,value);
		condition.setConditionType(type);
		conditions.put(conditionName,condition);
		
		LinkedHashMap<String,String> properties = new LinkedHashMap<String,String>();
		
		//fibre-template fields
		properties.put("title","test-title");
		properties.put("detail","required");
		properties.put("deadline","2014-02-21");
		properties.put("size","3");
		properties.put("priority","1");
		properties.put("productowner","peter");
		properties.put("developer","kevin");
		properties.put("system","software");

		List<String> parameterList = new ArrayList<String>();
		parameterList.add("title");
		parameterList.add("detail");
		parameterList.add("deadline");
		parameterList.add("size");
		parameterList.add("priority");
		parameterList.add("productowner");
		parameterList.add("developer");
		parameterList.add("system");
		
		Action action = ActionTestTool.newTestActionPP("testAction", 1, "execute", "testPlugin", 
				"samId", "executeRule", properties, parameterList );
		
		Map<String,Action> actions = new HashMap<String,Action>();
		actions.put("testAction", action);
		
		Rule rule = new Rule();
		rule.setName(ruleName);
		rule.setAutomationConditions(conditions);
		rule.setTaskConditions(null);
		rule.setActions(actions);
		return rule;
	}
	
	@Test
	public void testProcessGraph() throws Exception {
		this.controller.processGraph(TestBoardTool.BOARD_ID, Mockito.mock(Model.class));
	}
	
	@Test
	public void testRuleGetModel() throws Exception {
		Map<String, Object> model = this.controller.getModel(TestBoardTool.BOARD_ID);
		assertFalse(model.isEmpty());
	}
	
	@Test
	public void testInvalidateRuleCache() throws Exception {
		Map<String, String> listRules = this.controller.listRules(TestBoardTool.BOARD_ID, false);
		Map<String, String> invalidateRuleCache = this.controller.invalidateRuleCache(TestBoardTool.BOARD_ID);
		assertFalse(invalidateRuleCache.equals(listRules));
	}
	
	@Test
	public void testRuleHistoryNotFound() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.getRuleHistory(TestBoardTool.BOARD_ID, "unknown_alien_rule");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testUpdateRuleNotFound() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.updateRule(TestBoardTool.BOARD_ID, "unknown_alien_rule", null);
		}, "ResourceNotFoundException was expected");
	}
	
	@Test 
	public void testGetRuleNotFound() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.getRule(TestBoardTool.BOARD_ID, "unknown_alien_rule");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testRuleActivateVersionNotFound() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.activateVersion(TestBoardTool.BOARD_ID, "test_rule2","unknown_rule_version");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testRuleActivateVersion() throws Exception {
		Rule rule = this.controller.getRule(TestBoardTool.BOARD_ID, "test_rule2");
		
		String oldVersion=rule.getVersion();
		
		rule.setId(null);
		rule.setVersion(null);
		rule.setUniqueid(null);
		
		Rule createRule = this.controller.createRule(TestBoardTool.BOARD_ID, rule);
		
		assertFalse(createRule.getVersion().equals(oldVersion));
		
		assertTrue(createRule.isActive());
		
		Rule activateVersion = this.controller.activateVersion(TestBoardTool.BOARD_ID, "test_rule2",oldVersion);
		
		assertTrue(activateVersion.isActive());
		assertTrue(activateVersion.getVersion().equals(oldVersion));
		
		activateVersion = this.controller.activateVersion(TestBoardTool.BOARD_ID, "test_rule2",oldVersion);
		assertTrue(activateVersion.getVersion().equals(oldVersion));
		
		
	}
	
	@Test
	public void testZZDeleteRule() throws Exception {
		Rule rule = this.controller.getRule(TestBoardTool.BOARD_ID, "test_rule2");
		String activeVersion=rule.getVersion();
		this.controller.deleteRule(TestBoardTool.BOARD_ID, "test_rule2");
		
		List<Rule> ruleHistory = this.controller.getRuleHistory(TestBoardTool.BOARD_ID, "test_rule2");
		for(Rule r:ruleHistory) {
			if(r.getVersion().equals(activeVersion)) {
				assertFalse(r.isActive());
			}else {
				assertFalse(r.isActive());
			}
			
		}
	}
	
	@Test 
	public void testOnDemandRuleNotFound() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.controller.executeOnDemandRule(TestBoardTool.BOARD_ID, "on_demand_rule");
		}, "ResourceNotFoundException was expected");
	}
	
	@Test 
	public void testExecuteOnDemandRuleValidationException() throws Exception {
		
		Rule rule = getTestRule("Test Rule1", "name", Operation.EQUALTO, "Smith", ConditionType.PROPERTY);
		rule.setRuleType(RuleType.COMPULSORY);
		Rule newRule = controller.createRule(TestBoardTool.BOARD_ID, rule);
		ruleCache.clearCache();
		Assertions.assertThrows(ValidationException.class, () -> {
			controller.executeOnDemandRule(TestBoardTool.BOARD_ID, newRule.getId());
			this.controller.deleteRule(TestBoardTool.BOARD_ID, newRule.getId());
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testExecuteOnDemandRule() throws Exception {
		
		Rule rule = getTestRule("Test Rule1", "name", Operation.EQUALTO, "Smith", ConditionType.PROPERTY);
		rule.setRuleType(RuleType.ONDEMAND);
		Rule newRule = controller.createRule(TestBoardTool.BOARD_ID, rule);
		ruleCache.clearCache();
		
		ResponseEntity<Object> response = controller.executeOnDemandRule(TestBoardTool.BOARD_ID, newRule.getId());
		this.controller.deleteRule(TestBoardTool.BOARD_ID, newRule.getId());
		assertNotNull(response.getBody());
	}
}
