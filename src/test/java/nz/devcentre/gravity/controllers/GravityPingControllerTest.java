/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import javax.servlet.ServletException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class GravityPingControllerTest extends Mockito {
	private GravityPingController gravityPingController;
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	
	@Autowired
	private PublicController publicController;
	
	@BeforeEach
	public void setUp() {
		gravityPingController = new GravityPingController();
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
	}

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Test
	public void testGravityPing() throws ServletException, IOException {
		request.addHeader("test-ping", "ping");
		request.addParameter("test-ping", "ping");
		gravityPingController.doGet(request, response);
	}
	
	@Test
	public void testGravityPingBootStrap() throws Exception {
		request.addHeader("test-ping", "ping");
		request.addParameter("test-ping", "ping");
		publicController.ping(request);
	}	
}
