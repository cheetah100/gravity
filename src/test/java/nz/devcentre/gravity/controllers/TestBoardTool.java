/** GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AccessType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.tools.IdentifierTools;

public class TestBoardTool {
	
	public static String BOARD_ID = "test_board";
	public static String BOARD_ID_UPD = "test_board_upd"; // Using a separate board for any updates to ensure other tests don't break
	public static String BOARD_ID_INVALID_CARDS = "test_board_invalid_cards"; // Using a separate board for any updates to ensure other tests don't break
	public static String BOARD_ID_OPTIONLIST = "test_optionlist";
	public static String PHASE_ID = "test_phase";
	public static String TEMPLATE_ID = "test_board";
	public static String GROUP_ID = "test_group";
	public static String TASK_ID = "test_task";
	public static String TASK_ID2 = "test_task_2";
	public static String TASK_ID3 = "test_task_3";
	public static String TASK_ID4 = "test_task_4";
	public static String TASK_ID5 = "test_task_5";
	public static String RULE_ID = "test_rule";
	public static String RULE_ID2 = "test_rule_2";
	public static String CARD_ID = "TE000003";
	public static String TEST_TEMPLATE_NAME = "test_board";


	@Autowired
	private BoardController controller;
	
	@Autowired
	private CardController cardController;

	@Autowired
	CardEventRepository cardEventRepository;
	
	@Autowired
	private RuleController ruleController;
	
	@Autowired
	private FilterController filterController;
	
	@Autowired
	private UserController userController;
	
	@Autowired
	private TeamController teamController;
	
	public void initTestBoard() throws Exception{
		
		try {
			controller.getBoard(BOARD_ID);
			return;
		} catch( ResourceNotFoundException e){
			System.out.println("Creating new Test Board");
		}
		
		try{
			userController.getUser("system");
		} catch(Exception e){
			User systemUser = new User();
			systemUser.setName("system");
			systemUser.setFirstname("System");
			systemUser.setFirstname("Administrator");
			systemUser.setKey("password");
			userController.createUser(systemUser);
		}
		
		try{
			teamController.getTeam("administrators");
		} catch(Exception e) {
			Map<String,String> members = new HashMap<String,String>();
			members.put("system", "ADMIN");
			
			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);
			teamController.createTeam(team);
		}
		
		controller.createBoard(getTestBoard( "Test Board", BoardType.OBJECT ));
	
		generateRule(BOARD_ID, "Test Task", RuleType.TASK);
		generateBrokenRule(BOARD_ID, "Test Task 2", RuleType.TASK);
		generateTaskRuleWithConditions(BOARD_ID, "Test Task 3", "name", Operation.EQUALTO, "testName", true, RuleType.TASK);
		generateTaskRuleWithConditions(BOARD_ID, "Test Task 4", "name", Operation.EQUALTO, "testName", false, RuleType.TASK);
		generateTaskRuleWithConditions(BOARD_ID, "Test Task 5", "name", Operation.EQUALTO, "testName", false, RuleType.COMPULSORY);

		generateRule(BOARD_ID, "Test Rule", RuleType.COMPULSORY);
		generateBrokenRule(BOARD_ID, "Test Rule 2", RuleType.COMPULSORY);
		
		cardController.createCard(BOARD_ID, getTestCard("Sam",BOARD_ID,500),false);
		cardController.createCard(BOARD_ID, getTestCard("Joe",BOARD_ID,1000),false);
		cardController.createCard(BOARD_ID, getTestCard("Tim",BOARD_ID,1500),false);
		cardController.createCard(BOARD_ID, getTestCard("Peter",BOARD_ID,2000),false);
		cardController.createCard(BOARD_ID, getTestCard("Olly",BOARD_ID,2500),false);
		cardController.createCard(BOARD_ID, getTestCard("Kevin",BOARD_ID,3000),false);
		cardController.createCard(BOARD_ID, getTestCard("Darren",BOARD_ID,3500),false);
		cardController.createCard(BOARD_ID, getTestCard("Simon",BOARD_ID,4000),false);
		cardController.createCard(BOARD_ID, getTestCard("Smith",BOARD_ID,4500),false);
	}
	
	public void initTestUpdBoard() throws Exception{
		
		try {
			controller.getBoard(BOARD_ID_UPD);
			return;
		} catch( ResourceNotFoundException e){
			System.out.println("Creating new Test Update Board");
		}
		
		controller.createBoard(getTestBoard( BOARD_ID_UPD, BoardType.OBJECT ));
	
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Sam",BOARD_ID_UPD,500)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Joe",BOARD_ID_UPD,1000)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Tim",BOARD_ID_UPD,1500)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Peter",BOARD_ID_UPD,2000)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Olly",BOARD_ID_UPD,2500)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Kevin",BOARD_ID_UPD,3000)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Darren",BOARD_ID_UPD,3500)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Simon",BOARD_ID_UPD,4000)),false);
		cardController.createCard(BOARD_ID_UPD, addFieldBouquet(getTestCard("Smith",BOARD_ID_UPD,4500)),false);
	}
	
	// Creates a board with cards that have missing fields for validation testing
	public void initTestBoardInvalidCards() throws Exception{
		
		try {
			controller.getBoard(BOARD_ID_INVALID_CARDS);
			return;
		} catch( ResourceNotFoundException e){
			System.out.println("Creating new Test Board Invalid Fields");
		}
		Board testBoard = getTestBoard(BOARD_ID_INVALID_CARDS, BoardType.OBJECT);
		testBoard.setInvalidCardPhase("test_phase");
		controller.createBoard(testBoard);
	
		cardController.createCard(BOARD_ID_INVALID_CARDS, getTestCard("Sam",BOARD_ID_INVALID_CARDS,500),false);
		cardController.createCard(BOARD_ID_INVALID_CARDS, getTestCard(null,BOARD_ID_INVALID_CARDS,1000),false);
		cardController.createCard(BOARD_ID_INVALID_CARDS, getTestCard("Tim",BOARD_ID_INVALID_CARDS,1500),false);
		cardController.createCard(BOARD_ID_INVALID_CARDS, getTestCard(null,BOARD_ID_INVALID_CARDS,2000),false);
	}
	
	public void initTestOptionlistBoard() throws Exception{
		
		try {
			controller.getBoard(BOARD_ID_OPTIONLIST);
			return;
		} catch( ResourceNotFoundException e){
			System.out.println("Creating new Test Optionlist Board");
		}
		
		controller.createBoard(getTestBoard( BOARD_ID_OPTIONLIST, BoardType.OPTIONLIST ));
	}

	public Card addFieldBouquet(Card card) {
		Map<String, Object> fields = card.getFields();
		fields.put("booleanField", true);
		fields.put("integerField", Integer.valueOf(1));
		fields.put("longField", Long.valueOf(100));
		fields.put("doubleField", Double.valueOf(200.20));
		List<String> list = new ArrayList<String>();
		list.add("one"); list.add("two"); list.add("three");
		fields.put("listField", list);
		return card;
	}
	
	public void generateFilters() throws Exception {
		
		try {
			filterController.getFilter(BOARD_ID, "test_filter");
		} catch (ResourceNotFoundException e){
			Filter equalsfilter = getFilter("test_filter","metadata.creator", Operation.EQUALTO,"system");
			filterController.createFilter(BOARD_ID, equalsfilter);
		}
		
		try {
			filterController.getFilter(BOARD_ID, "test_phase_filter");
		} catch (ResourceNotFoundException e){
			Filter phasefilter = getTestFilter("test_phase_filter","test_phase","name","Bob");
			filterController.createFilter(BOARD_ID, phasefilter);
		}
		
		try {
			filterController.getFilter(BOARD_ID, "replacementFilter");
		} catch (ResourceNotFoundException e){
			Filter replacementfilter = getTestFilter("replacementFilter","test_phase","name","Tim");
			filterController.createFilter(BOARD_ID, replacementfilter);
		}
		
		try {
			filterController.getFilter(BOARD_ID, "next_phase_filter");
		} catch (ResourceNotFoundException e){
			Filter phasefilter = getTestFilter("next_phase_filter","next_phase","name","Bob");
			filterController.createFilter(BOARD_ID, phasefilter);
		}
		
		try {
			filterController.getFilter(BOARD_ID, "test_phase_next_phase_filter");
		} catch (ResourceNotFoundException e){
			Filter phasefilter = getTestFilter("test_phase_next_phase_filter","test_phase|next_phase","name","Bob");
			filterController.createFilter(BOARD_ID, phasefilter);
		}
	}
	
	public void generateRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = getTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);		
	}
	
	public void generateBrokenRule(String boardId, String ruleName, RuleType ruleType) throws Exception {
		Rule rule = getBrokenTestRule(ruleName, ruleType);
		ruleController.createRule(boardId, rule);
	}
	
	public void generateTaskRuleWithConditions(String boardId, String ruleName, String fieldName,
			Operation operation, String value, boolean task, RuleType ruleType) throws Exception {
		Rule rule = null;
		if( task ) {
			rule = getTaskRuleWithTaskConditions(ruleName, fieldName, operation, value, ruleType);
		} else {
			rule = getTaskRuleWithAutomationConditions(ruleName, fieldName, operation, value, ruleType);
		}
		ruleController.createRule(boardId, rule);
	}
			
	public static Board getTestBoard( String name, BoardType type ) throws Exception{
		Map<String,Phase> phases = new HashMap<String,Phase>();		
		phases.put("test_phase", getTestPhase( "Test Phase", 1));
		phases.put("next_phase", getTestPhase( "Next Phase", 2));
		
		Phase archivePhase = getTestPhase( "Archive", 3);
		archivePhase.setInvisible(true);
		phases.put("archive", archivePhase);
		
		Map<String,View> views = new HashMap<>();
		views.put("testview", getTestView("Test View"));
		
		Map<String,String> roles = new HashMap<>();
		roles.put("test_user", "WRITE");
		
		//Filter equalsfilter = getFilter("test_filter","metadata.creator", Operation.EQUALTO,"system");
		
		Board board = new Board();
		board.setName(name);
		board.setBoardType(type);
		board.setPhases(phases);
		board.setViews(views);
		board.setOrderField("name");
		board.setPermissions(roles);
		board.setPrefix("TE");
		board.setFields(getTestFields());
		return board;
	}
	
	public static View getTestView(String name){
		View view = new View();
		view.setName(name);
		
		Map<String,ViewField> fields = new HashMap<String,ViewField>();
		fields.put("name", getTestViewField("name", 50, 0));
		fields.put("phone", getTestViewField("phone", 50, 1));
		
		view.setFields(fields);
		return view;
	}
	
	public static ViewField getTestViewField( String name, int length, int index){
		ViewField vf = new ViewField();
		vf.setIndex(index);
		vf.setName(name);
		return vf;
	}
	
	public static Phase getTestPhase(String name, Integer index) {
		Phase phase = new Phase();
		phase.setDescription(name);
		phase.setIndex(index);
		phase.setName(name);
		try {
			phase.setId(IdentifierTools.getIdFromNamedModelClass(phase));
		}
		catch (Exception ignore) {
			
		}
		return phase;
	}
	
	public static Rule getTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}
	
	// Get a non-working test rule for error testing
	public static Rule getBrokenTestRule(String name, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		// Create actions that will fail upon execution
		Map<String, Action> actions = new HashMap<>();
		Action action = new Action();
		action.setOrder(1);
		action.setType("script");
		actions.put("action", action);
		rule.setActions(actions);
		rule.setAutomationConditions(new HashMap<String, Condition>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		return rule;
	}
	
	// Get a test rule to check condition evaluation
	public static Rule getTaskRuleWithTaskConditions(String name, String fieldName, 
			Operation operation, String value, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setAutomationConditions(new HashMap<String, Condition>());
		Map<String, Condition> taskConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		taskConditions.put("testCondition", condition);
		rule.setTaskConditions(taskConditions);
		return rule;
	}

	// Get a test rule to check condition evaluation
	public static Rule getTaskRuleWithAutomationConditions(String name, String fieldName, 
			Operation operation, String value, RuleType ruleType) {
		Rule rule = new Rule();
		rule.setRuleType(ruleType);
		rule.setName(name);
		rule.setActions(new HashMap<String, Action>());
		rule.setTaskConditions(new HashMap<String, Condition>());
		Map<String, Condition> automationConditions = new HashMap<>();
		Condition condition = new Condition(fieldName, operation, value);
		automationConditions.put("testCondition", condition);
		rule.setAutomationConditions(automationConditions);
		return rule;
	}
	
	
	public static Filter getFilter(String name, String fieldName, Operation operation, String value) {
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		conditions.put("a",new Condition(fieldName,operation,value));
		
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("test_user", "test_role");
		
		Filter filter = new Filter();
		filter.setName(name);
		filter.setAccess(AccessType.READ);
		filter.setOwner("system");
		filter.setConditions(conditions);
		filter.setPermissions(permissions);
		filter.setPhase(PHASE_ID);
		return filter;
	}
	
	public static Filter getTestFilter( String name, String phase, String field, String value) throws Exception{
		Filter filter = new Filter();
		filter.setName(name);
		filter.setId(name);
		filter.setPhase(phase);
		
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		conditions.put("test_condition", new Condition(field,Operation.EQUALTO,value));
		filter.setConditions(conditions);
		return filter;
	}
	
	public static String getIdFromPath(String input){
		int i = input.lastIndexOf("/");
		if( i>-1){
			return input.substring(i+1);
		} 
		return "";
	}
	
	public static Card getTestCard( String name, String template, Integer balance ){
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", name);
		fields.put("phone", "0201000999");
		fields.put("address", "Warehouse Way, Auckland");
		fields.put("balance", balance.toString());
		Card newCard = new Card();
		newCard.setCreator("smith");
		newCard.setFields(fields);
		newCard.setPhase(PHASE_ID);
		return newCard;
	}
	
	public Card getTestCard() throws Exception{
		Collection<Card> cards = cardController.getCards(TestBoardTool.BOARD_ID, null, null, null, null, false, false, false);
		return cards.iterator().next();
	}
	
	public void addReferenceField(String boardId, String fieldname, String targetBoardId) throws Exception {
		Board board = this.controller.getBoard(boardId);			
		TemplateField field = getTestTemplateField(fieldname, FieldType.STRING);
		field.setOptionlist(targetBoardId);
		board.getFields().put(fieldname, field);		
		this.controller.updateBoard(board, boardId);
	}
	
	public static TemplateField getTestTemplateField(String name, FieldType type){
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(type);
		return templateField;
	}
	
	/**
	 * Create a dummy optionlist field in the template- no actual board
	 * @param name
	 * @param index
	 * @return
	 */
	public static TemplateField getTestTemplateFieldOptionList(String name, String optionList){
		TemplateField templateField = new TemplateField();
		templateField.setName(name);
		templateField.setLabel(name);
		templateField.setType(FieldType.LIST);
		templateField.setOptionlist(optionList);
		return templateField;
	}
	
	public static Map<String,TemplateField> getTestFields(){

		Map<String,TemplateField> fields = new HashMap<String,TemplateField>();
		fields.put("name", TestBoardTool.getTestTemplateField("name", FieldType.STRING));
		fields.put("phone", TestBoardTool.getTestTemplateField("phone", FieldType.STRING));
		fields.put("balance", TestBoardTool.getTestTemplateField("balance", FieldType.NUMBER));
		fields.put("street", TestBoardTool.getTestTemplateField("street", FieldType.STRING));
		fields.put("suburb", TestBoardTool.getTestTemplateField("suburb", FieldType.STRING));
		fields.put("city", TestBoardTool.getTestTemplateField("city", FieldType.STRING));
		fields.put("country", TestBoardTool.getTestTemplateField("country", FieldType.STRING));
		//fields.put("owner", TestBoardTool.getTestTemplateFieldOptionList("owner", "owner_board"));
		return fields;
		
	}
}
