/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class TeamControllerTest {
	@Autowired
	private TeamController teamController;

	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	private SecurityTool securityTool;

	private static final String TEAMID="ADMIN_TEST_TEAM";

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void before() throws Exception {
		tool.initTestBoard();
		securityTool.iAmSystem();
	}
	
	@Test
	public void createTeam()throws Exception{
		Team team=new Team();
		team.setName("ADMIN-TEST");
		team.setId(TEAMID);
		this.teamController.createTeam(team);
		assertTrue(this.teamController.getTeam(TEAMID).getName().equals("ADMIN-TEST"));
	}
	
	@Test
	public void createTeamException() throws Exception {
		Team team=new Team();
		team.setName("ADMIN-TEST");
		team.setId(TEAMID);
		Assertions.assertThrows(ValidationException.class, () -> {
			this.teamController.createTeam(team);
		}, "ValidationException was expected");
	}
	
	@Test
	public void createTeamMembers() throws Exception{
		this.teamController.addMembers(TEAMID, ImmutableMap.of("system","system"));
		assertFalse(this.teamController.getTeam(TEAMID).getMembers().isEmpty());
	}
	
	@Test
	public void createTeamRoles() throws Exception{
		this.teamController.addRoles(TEAMID, ImmutableMap.of("ADMIN","ADMIN"));
		assertFalse(this.teamController.getTeam(TEAMID).getRoles().isEmpty());
	}
		
	@Test
	public void createTeamOwners() throws Exception{
		this.teamController.addOwners(TEAMID, ImmutableMap.of("system","system"));
		assertFalse(this.teamController.getTeam(TEAMID).getOwners().isEmpty());
	}
	
	@Test
	public void getTeamUsers() throws Exception{
		Map<String,User> user=this.teamController.getTeamUsers(TEAMID);
		assertNotNull(user);
		
	}
	
	@Test
	public void getTeamById()throws Exception{
		Team team=this.teamController.getTeam(TEAMID);
		assertNotNull(team);
	}
	
	@Test
	public void getTeam() throws Exception {
		Map<String, String> teamDetails=teamController.listTeams();
		assertFalse(teamDetails.isEmpty());
	}
	
	
	@Test
	public void getTeamDetails() throws Exception {
		Map<String, Team> teamDetails=teamController.getTeamDetails();
		assertFalse(teamDetails.isEmpty());
	}
	
	@Test
	public void updateTeam() throws Exception{
		Team team=new Team();
		team.setDescription("Test Team Descriprtion");
		team=this.teamController.updateTeam(team, TEAMID);
		assertTrue(team.getId().equals(TEAMID));
	}
	
	@Test
	public void zzDeleteTeamTest()throws Exception{
		this.teamController.deleteMember(TEAMID, "system");
		this.teamController.deleteOwner(TEAMID, "system");
		this.teamController.deleteRole(TEAMID, "admin");
		this.teamController.deleteRole(TEAMID, "ADMIN");
		this.teamController.deleteTeam(TEAMID);
		try {
		this.teamController.deleteTeam(null);
		}catch (Exception e) {
			assertTrue(e instanceof FileNotFoundException);
		}
	}
	
	@Test
	public void teamAliasTest() throws Exception {
		
		List<String> aliasToAdd=Arrays.asList("ADMIN_TEST_TEAM_ART","ADMIN_TEST_TEAM2_ART");
		
		this.teamController.addTeamAlias(TEAMID, aliasToAdd);
		Team team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().containsAll(aliasToAdd));
		
		//Null Payload
		this.teamController.addTeamAlias(TEAMID, null);
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().containsAll(aliasToAdd));
		//Empty List
		this.teamController.addTeamAlias(TEAMID, new ArrayList<String>());
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().containsAll(aliasToAdd));
		
		
		//Remove Alias Test
		//Null Payload
		this.teamController.removeTeamAlias(TEAMID, null);
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().containsAll(aliasToAdd));
		//Empty List
		this.teamController.removeTeamAlias(TEAMID, new ArrayList<String>());
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().containsAll(aliasToAdd));
		
		this.teamController.removeTeamAlias(TEAMID, Arrays.asList("ADMIN_TEST_TEAM2_ART"));
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().contains("ADMIN_TEST_TEAM_ART"));
		assertFalse(team.getAliases().contains("ADMIN_TEST_TEAM2_ART"));
		
		this.teamController.removeTeamAlias(TEAMID, aliasToAdd);
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().isEmpty());
		
		this.teamController.removeTeamAlias(TEAMID, aliasToAdd);
		team=this.teamController.getTeam(TEAMID);
		assertTrue(team.getAliases().isEmpty());
		
	}
	
	@Test
	public void refreshMembersTest() throws Exception {
		Team team = this.teamController.getTeam(TEAMID);
		Map<String, String> members =new LinkedHashMap<>(team.getMembers());
		members.put("bond", "");
		this.teamController.refreshMembers(TEAMID, members);
		team = this.teamController.getTeam(TEAMID);
		assertTrue(team.getMembers().containsKey("bond"));
		assertTrue(team.getMembers().size()==members.size());
	}
	
	@Test
	public void testAddMembers() throws Exception {
		Map<String,String> teams=new LinkedHashMap<String, String>();
		teams.put(TEAMID,TEAMID);
		this.securityTool.addUserToTeams("bond007", teams);
		Team team = this.teamController.getTeam(TEAMID);
		assertTrue(team.getMembers().containsKey("bond007"));
		this.securityTool.addUserToTeams("bond007", null);
	}

	@Test
	public void getModelTest() throws Exception {
		
		assertFalse(this.teamController.getModel().isEmpty());
	}
}
