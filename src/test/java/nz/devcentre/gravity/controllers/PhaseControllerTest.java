package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class PhaseControllerTest {

	/*
	 * @Autowired private BoardController boardController;
	 */
	public static String BOARD_ID = "test_board";
	public static String UNIT_TEST = "unit_test";
	public static String TEST_PHASE = "test_phase";

	@Autowired
	private PhaseController phaseController;

	@Autowired
	private TestBoardTool testBoardTool;
	
	@Autowired
	private SecurityTool securityTool;

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void setUp() throws Exception {
		this.testBoardTool.initTestBoard();
		securityTool.iAmSystem();
	}

	@Test
	public void getPhaseTest() throws Exception {
		this.phaseController.getPhase(BOARD_ID, TEST_PHASE);
		try {
			this.phaseController.getPhase(BOARD_ID, "test_phase1");
		} catch (Exception e) {
			assertTrue(e instanceof ResourceNotFoundException);
		}
	}

	@Test
	public void createUpdatePhaseTest() throws Exception {
		Phase newPhase = this.testBoardTool.getTestPhase(UNIT_TEST, 5);
		Phase result = this.phaseController.createPhase(BOARD_ID, newPhase);
		assertNotNull(result);

		try {
			this.phaseController.createPhase(BOARD_ID, newPhase);
		} catch (Exception e) {
			assertTrue(e instanceof ValidationException);
		}
		// Update Phase Testing
		newPhase.setIndex(10);
		result = this.phaseController.updatePhase(BOARD_ID, UNIT_TEST, newPhase);
		assertNotNull(result);

		
	}

	
	@Test
	@Disabled
	public void aaGetCardsListTest() throws Exception {
		//Map<String, String> cardList = this.cardController.getCardList(BOARD_ID, TEST_PHASE, null, true);
		List<String> cards = new ArrayList<>();
		//cardList.keySet().forEach(id -> {
		//	cards.add(id);
		//});
		cards.add("INVALID_ID");
		this.phaseController.getCards(BOARD_ID, TEST_PHASE, null, true, cards);

		Collection<Card> result = this.phaseController.getCards(BOARD_ID, "archive", null, false, cards);
		assertTrue(result.isEmpty());
		this.phaseController.getCards(BOARD_ID, "archive", null, true, cards);

	}

	
	@Test
	public void zzDeletePhaseTest() throws Exception {
		this.phaseController.deletePhase(BOARD_ID, UNIT_TEST);
		this.phaseController.deletePhase(BOARD_ID, "next_phase");
		// Should Throw Exception
		try {
			this.phaseController.deletePhase(BOARD_ID, TEST_PHASE);
		} catch (Exception e) {
			// TODO: handle exception
			assertTrue(e instanceof ValidationException);
		}
		try {
			this.phaseController.deletePhase(BOARD_ID, null);
		} catch (Exception e) {
			// TODO: handle exception
			assertTrue(true);
		}
		try {
			this.phaseController.deletePhase(null, null);
		} catch (Exception e) {
			// TODO: handle exception
			assertTrue(true);
		}
	}
}
