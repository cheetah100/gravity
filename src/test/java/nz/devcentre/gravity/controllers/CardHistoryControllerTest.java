/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "/test-controllers.xml" })
public class CardHistoryControllerTest {
	
	protected static String VIEW_ID = "testview";
	
	@Autowired
	private CardHistoryController cardHistoryController;
	
	
	@Mock
	CardEventRepository eventRepository;
	
	@Mock
	CardEvent cardEvent;
	
	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	private SecurityTool securityTool;
	
	@BeforeEach
	public void before() throws Exception {
		tool.initTestBoard();
		this.securityTool.iAmSystem();
		MockitoAnnotations.openMocks(this);
		cardHistoryController.setEventRepository(eventRepository);
	}
		
	@Test
	public void testGetHistoryList() throws Exception {
		
		Card card = tool.getTestCard();
		when(eventRepository.findByBoardAndCard(TestBoardTool.BOARD_ID, card.getId())).thenReturn(getMockEvents(TestBoardTool.BOARD_ID, card.getId()));
		Collection<CardEvent> historyList = cardHistoryController.getHistoryList(
				TestBoardTool.BOARD_ID, 
				card.getId());
		CardEvent event = historyList.iterator().next();
		
		assertNotNull(event);
		// assertEquals( event.getDetail(), "Creating Card");
		when(eventRepository.findByUuid(event.getUuid())).thenReturn(createCardEvent(TestBoardTool.BOARD_ID, card.getId()));
		CardEvent history = cardHistoryController.getHistory(
				TestBoardTool.BOARD_ID, 
				card.getId(),
				event.getUuid());
		
		assertNotNull(history);
		assertEquals(history.getDetail(), event.getDetail());		
	}
	
	@Test
	public void testGetHistory() throws Exception {
		Card card = tool.getTestCard();
		
		CardEvent event = new CardEvent();
		
		when(eventRepository.findByUuid(event.getUuid())).thenReturn(createCardEvent(TestBoardTool.BOARD_ID, card.getId()));
		
		CardEvent history = cardHistoryController.getHistory(
				TestBoardTool.BOARD_ID, 
				card.getId(),
				event.getUuid());
		
		assertNotNull(history);
	}

	@Test
	public void testSaveHistory() throws Exception {
				
		Card card = tool.getTestCard(); 
		
		Map<String,Object> body = new HashMap<String,Object>();
		body.put("detail","Test History");
		
		CardEvent saveHistory = cardHistoryController.saveHistory(
				TestBoardTool.BOARD_ID, 
				card.getId(),
				body);
		
		assertNotNull(saveHistory);
		assertNotNull(saveHistory.getDetail());
	}
	
	private List<CardEvent> getMockEvents(String boardId, String cardId){
		List<CardEvent> dbEvents = new ArrayList<CardEvent>();
		dbEvents.add(createCardEvent(boardId, cardId));
		return dbEvents;
		
	}
	
	private CardEvent createCardEvent(String boardId, String cardId){
		CardEvent event = new CardEvent();
		event.setBoard(boardId);
		event.setCard(cardId);
		//event.setUuid(UUID.randomUUID().toString());
		event.setDetail("Detail");
		return event;
	}
}
