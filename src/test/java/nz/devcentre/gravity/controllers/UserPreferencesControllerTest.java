/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Preference;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class UserPreferencesControllerTest {

	@Autowired
	private UserPreferencesController controller;

	@Autowired
	private TestBoardTool tool;

	@Autowired
	private SecurityTool securityTool;
	
	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void before() throws Exception {
		tool.initTestBoard();
	}

	@Test
	public void testCreateAndGetPreference() throws Exception {

		this.securityTool.iAmSystem();

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("field", "value");

		Preference p = new Preference();
		p.setName("Test");
		p.setValues(values);

		Preference createdPreference = controller.createPreference(p);

		assertNotNull(createdPreference);

		String id = createdPreference.getId();
		Preference preference = controller.getPreference(id);

		assertNotNull(preference);
		assertEquals("Test", preference.getName());
		assertEquals("value", preference.getValues().get("field"));
	}

	@Test
	public void testGetAllPreferences() throws Exception {

		this.securityTool.iAmSystem();

		Map<String, Object> values = new HashMap<String, Object>();
		values.put("field", "value");

		Preference p = new Preference();
		p.setName("TestAll");
		p.setValues(values);

		controller.createPreference(p);

		Collection<Preference> allPreferences = controller.getAllPreferences();

		boolean found = false;
		for (Preference p2 : allPreferences) {
			if (p2.getName().equals("TestAll")) {
				found = true;
				break;
			}
		}

		assertTrue(found);
	}
		
	@Test
	public void testMiscPreferenceOPerations() throws Exception {
		
		this.securityTool.iAmSystem();
		
		Map<String, String> result = this.controller.listPreferences();
		assertFalse(result.isEmpty());

		this.controller.getPreferenceForUserId(SecurityTool.SYSTEM);
		this.controller.getPreference("test");
		this.controller.getPreference("test2");
		
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("field", "value");
		Preference newP = new Preference();
		newP.setName("Misc");
		newP.setValues(values);
		controller.createPreference(newP);
		
		Preference p = this.controller.getPreferenceForUserId(SecurityTool.SYSTEM, "misc");
		this.controller.createPreference(SecurityTool.SYSTEM, p);
		
		this.controller.updatePreference(SecurityTool.SYSTEM, p, p.getId());
		this.controller.updatePreference( p, p.getId());
		try {
			this.controller.updatePreference(SecurityTool.SYSTEM, p, "invalidID");
		}catch (Exception e) {
			// TODO: handle exception
			assertTrue(e instanceof ValidationException);
		}
		p.setName("misc-test");
		this.controller.createPreference(SecurityTool.SYSTEM, p);
		List<Preference> preferenceList=new ArrayList<>();
		preferenceList.add(p);
		
		this.controller.deletePreference(p.getId());
	}
	
	@Test
	public void testZCanary() throws Exception {	
		this.securityTool.iAmSystem();
		String currentUser = SecurityTool.getCurrentUser();
		assertEquals("system", currentUser);
	}
}
