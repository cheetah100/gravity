package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.model.TransformChain;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransformControllerTest {
	
	@Autowired
	TransformController transformController;
	
	@Autowired
	TransformCache transformCache; 
	
	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Test
	public void testCreateTransformChain() throws Exception {
		
		TransformChain transformChain = new TransformChain();
		transformChain.setName("Test Transform Chain");
		transformChain.setDescription("Test Description");
		TransformChain createdTransformChain =transformController.createTransformChain(transformChain, false);
		String firstVersion=createdTransformChain.getVersion();
		
		assertNotNull( createdTransformChain);
		assertEquals("test_transform_chain", createdTransformChain.getId());
		assertEquals("Test Transform Chain", createdTransformChain.getName());
		assertEquals("Test Description", createdTransformChain.getDescription());
		this.transformCache.clearCache();
		transformChain.setId(createdTransformChain.getId());
		transformChain.setActive(true);
		transformChain.setVersion(null);
		transformChain.setUniqueid(null);
		createdTransformChain = transformController.createTransformChain(transformChain, false);
		assertNotEquals(firstVersion, createdTransformChain.getVersion());
	}

	@Test
	public void testUpdateTransformChain() throws Exception {
		TransformChain transformChain = new TransformChain();
		transformChain.setName("Test Transform Chain Update");
		transformChain.setDescription("Original");
		TransformChain createdTransformChain = 
				transformController.createTransformChain(transformChain,false);
		
		String createdVersion = createdTransformChain.getVersion();
		
		assertNotNull( createdTransformChain);
		assertEquals("test_transform_chain_update", createdTransformChain.getId());
		assertEquals("Test Transform Chain Update", createdTransformChain.getName());
		assertEquals("Original", createdTransformChain.getDescription());
		
		TransformChain original = 
				transformController.getTransformChain("test_transform_chain_update", null);
		
		TransformChain modified = new TransformChain();
		modified.setId(original.getId());
		modified.setName(original.getName());
		modified.setDescription("Updated");
		transformController.updateTransformChain(original.getId(), modified, false);
		
		transformCache.clearCache();
		TransformChain updated = 
				transformController.getTransformChain("test_transform_chain_update", null);

		assertNotNull(updated);
		assertEquals("test_transform_chain_update", updated.getId());
		assertEquals("Test Transform Chain Update", updated.getName());
		assertEquals("Updated", updated.getDescription());
		
		String updatedVersion = updated.getVersion();
		assertFalse( createdVersion.equals(updatedVersion));
	}

	@Test
	public void testActivateTransformChain() throws Exception {
		TransformChain transformChain = new TransformChain();
		transformChain.setName("Test Transform Chain Activate");
		transformChain.setDescription("Original");
		TransformChain createdTransformChain = 
				transformController.createTransformChain(transformChain, false);
				
		transformController.getTransformChain("test_transform_chain_activate", null);
		TransformChain original = transformController.getTransformChain("test_transform_chain_activate", null);
		assertNotNull(original);
		assertEquals("test_transform_chain_activate", original.getId());
		assertEquals("Test Transform Chain Activate", original.getName());
		assertEquals("Original", original.getDescription());
		
		String originalVerison = original.getVersion();

		TransformChain modified = new TransformChain();
		modified.setId(original.getId());
		modified.setName(original.getName());
		modified.setDescription("Next");
		
		transformCache.clearCache();
		TransformChain updated = transformController.updateTransformChain(modified.getId(), modified, false);	
		assertNotNull(updated);
		assertEquals("test_transform_chain_activate", updated.getId());
		assertEquals("Test Transform Chain Activate", updated.getName());
		assertEquals("Next", updated.getDescription());
		assertFalse( original.getVersion().equals(updated.getVersion()));
		
		transformController.activateTransformChain("test_transform_chain_activate", createdTransformChain.getVersion());
		transformCache.clearCache();
		
		TransformChain activeTransform = transformController.getTransformChain("test_transform_chain_activate", null);
		assertNotNull(activeTransform);
		assertEquals("test_transform_chain_activate", activeTransform.getId());
		assertEquals("Test Transform Chain Activate", activeTransform.getName());
		assertNotEquals(updated.getVersion(), activeTransform.getVersion());
		assertEquals("Original", activeTransform.getDescription());
		
		TransformChain transformChain2 = transformController.getTransformChain("test_transform_chain_activate", originalVerison);
		assertTrue(transformChain2.isActive());
		
		transformController.activateTransformChain("test_transform_chain_activate", originalVerison);
		transformCache.clearCache();
		transformController.activateTransformChain("test_transform_chain_activate", originalVerison);
		TransformChain activateTransformChain = transformController.getTransformChain("test_transform_chain_activate", null);
		assertEquals(originalVerison, activateTransformChain.getVersion());
	}

	@Test
	public void testListTransformChains() throws Exception {
		TransformChain transformChain = new TransformChain();
		transformChain.setName("Test Transform Chain List");
		transformChain.setDescription("Original");
		transformController.createTransformChain(transformChain, false);
		transformCache.clearCache();
		
		Map<String, String> listTransformChains = transformController.listTransformChains(false);
		assertTrue( listTransformChains.size() > 0);
		assertTrue( listTransformChains.containsKey("test_transform_chain_list"));	
		listTransformChains = transformController.listTransformChains(true);
		assertTrue( listTransformChains.size() > 0);
	}

	@Test
	public void testDeleteTransformChain() throws Exception {
		TransformChain transformChain = new TransformChain();
		transformChain.setName("Test Transform Chain Delete");
		transformChain.setDescription("Original");
		TransformChain created = transformController.createTransformChain(transformChain, false);
		
		transformController.deleteTransformChain(created.getId());
		transformCache.clearCache();
		
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			TransformChain deleted = transformController.getTransformChain("test_transform_chain_delete", null);
			assertNull(deleted);
		}, "ResourceNotFoundException was expected");
	}
	
	@Test
	public void testGetTransformChainException() throws Exception {
		Assertions.assertThrows(ValidationException.class, () -> {
			transformController.getTransformChain(null, null);
		}, "ValidationException was expected");
	}
	
	@Test
	public void testGetTransformChainException1() throws Exception {
		Assertions.assertThrows(ValidationException.class, () -> {
			transformController.getTransformChain("unknown_alien", "1001");
		}, "ValidationException was expected");
	}
	
	@Test
	public void testListTransformChainsByType() throws Exception {
		TransformChain transformChain=new TransformChain();
		transformChain.setName("Test Pivot Type");
		transformChain.setType("Pivot");
		TransformChain createTransformChain = transformController.createTransformChain(transformChain, false);
		Map<String, String> listTransformChainsByType = transformController.listTransformChainsByType("Pivot", false);
		assertTrue(listTransformChainsByType.containsKey(createTransformChain.getId()));
		listTransformChainsByType = transformController.listTransformChainsByType("Pivot", true);
		assertTrue(listTransformChainsByType.containsKey(createTransformChain.getId()));
		
		listTransformChainsByType = transformController.listTransformChainsByType("Unknown_Alien_Type", true);
		assertTrue(listTransformChainsByType.isEmpty());
	}
	
	@Test
	public void testListTransformTypeChainException1() throws Exception {
		Assertions.assertThrows(ValidationException.class, () -> {
			transformController.listTransformChainsByType("", false);
		}, "ValidationException was expected");
	}
	
	@Test
	public void testGetDatasourcesForTransform() throws Exception {
		TransformChain transformChain=new TransformChain();
		transformChain.setName("Test Data Source");
		Transform transform=new Transform();
		Map<String,Object> configuration=new HashMap<String, Object>();
		configuration.put("board","data_source");
		transform.setConfiguration(configuration);
		transform.setTransformer("concat");
		Map<String,Transform> transforms=new HashMap<String, Transform>();
		transforms.put("a",transform);
		transformChain.setTransforms(transforms);
		TransformChain createTransformChain = transformController.createTransformChain(transformChain, false);
		
		Set<String> datasourcesForTransform = transformController.getDatasourcesForTransform(createTransformChain.getId());
		assertTrue(datasourcesForTransform.contains("data_source"));
		
	}
}
