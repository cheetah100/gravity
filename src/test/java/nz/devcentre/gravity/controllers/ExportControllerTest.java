/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;

import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.importer.TransferStatus;
import nz.devcentre.gravity.model.importer.TransferStatusEnum;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ExportControllerTest {

	@Autowired
	private ExportController exportController;
	
	@Autowired
	private TestBoardTool testBoardTool;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Configuration
	@Import({ FongoConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}
	
	@BeforeEach
	public void setUp() throws Exception {
		this.testBoardTool.initTestBoard();
		this.testBoardTool.generateFilters();
	}

	@Test
	public void testCreateExport() throws Exception {
		this.securityTool.iAmSystem();
		HttpServletResponse response=new MockHttpServletResponse();
		this.exportController.export(response, TestBoardTool.BOARD_ID, null, null, "all");
		assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",response.getContentType());
		assertEquals("attachment; filename=" + TestBoardTool.BOARD_ID + ".xlsx", response.getHeader("Content-Disposition"));

	}
	
	@Test
	public void testCreateExportAccessDenied() throws Exception {
		this.securityTool.iAm("unknown_alien");
		HttpServletResponse response=new MockHttpServletResponse();
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			this.exportController.export(response, TestBoardTool.BOARD_ID, null, null, "all");
		}, "AccessDeniedException was expected");

	}
	
	@Test
	public void testStartExportJob() throws Exception {
		this.securityTool.iAmSystem();
		HttpServletResponse response=new MockHttpServletResponse();
		String startExportJob = this.exportController.startExportJob(response, TestBoardTool.BOARD_ID, null, null, "all");
		assertEquals(this.exportController.getStatus().getId(), startExportJob);
	}
	
	@Test
	public void testStartExportJobDynamicFilter() throws Exception {
		this.securityTool.iAmSystem();
		HttpServletResponse response=new MockHttpServletResponse();
		Filter dynamicFilter=new Filter();
		Map<String,Condition> conditionMap=new HashMap<>();
		conditionMap.put("0",new Condition("balance",Operation.NUMBEREQUALTO, "4000|4500"));
		dynamicFilter.setConditions(conditionMap);
		TransferStatus transferStatus = new TransferStatus();
		transferStatus.setStatus(TransferStatusEnum.READY);
		this.exportController.setStatus(transferStatus);
		
		String startExportJobDynamicFilter = this.exportController.startExportJobDynamicFilter(response, TestBoardTool.BOARD_ID, null, "all", "test_filter", dynamicFilter);
		
		assertEquals(this.exportController.getStatus().getId(), startExportJobDynamicFilter);
	}
	@Test
	public void testExportCards() throws Exception {
		this.securityTool.iAmSystem();
		HttpServletResponse response=new MockHttpServletResponse();
		this.exportController.exportCards(response,TestBoardTool.BOARD_ID, null, new ArrayList<String>());
		assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",response.getContentType());
		assertEquals("attachment; filename=" + TestBoardTool.BOARD_ID + ".xlsx", response.getHeader("Content-Disposition"));
	}
	@Test
	public void testDownloadTemplateData() throws Exception {
		this.securityTool.iAmSystem();
		HttpServletResponse response=new MockHttpServletResponse();
		this.exportController.downloadTemplateData(response, TestBoardTool.BOARD_ID, TestBoardTool.BOARD_ID, null);
		assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",response.getContentType());
		assertEquals("attachment; filename=" + TestBoardTool.BOARD_ID + ".xlsx", response.getHeader("Content-Disposition"));
	}
	@Test
	public void testGenerateWorkbook() throws Exception {
		this.securityTool.iAmSystem();
		Filter dynamicFilter=new Filter();
		Map<String,Condition> conditionMap=new HashMap<>();
		conditionMap.put("0",new Condition("balance",Operation.NUMBEREQUALTO,"4000|4500"));
		dynamicFilter.setConditions(conditionMap);
		
		Workbook wb = this.exportController.generateWorkbook(TestBoardTool.BOARD_ID, null, "test_filter", null, dynamicFilter);
		Sheet sheet = wb.getSheetAt(0);
		wb.close();
		assertEquals(TestBoardTool.BOARD_ID,sheet.getSheetName());
		wb = this.exportController.generateWorkbook(TestBoardTool.BOARD_ID, null, null, null, dynamicFilter);
		wb.close();
	}
}
