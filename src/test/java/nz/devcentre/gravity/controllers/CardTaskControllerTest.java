/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.transfer.TaskAction;
import nz.devcentre.gravity.model.transfer.TaskActionBody;
import nz.devcentre.gravity.repository.CardTaskRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.TaskService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CardTaskControllerTest {

	protected static String VIEW_ID = "testview";

	@Autowired
	private CardTaskController cardTaskController;

	@Autowired
	private TestBoardTool tool;

	@Autowired
	CardTaskRepository cardTaskRepository;

	@Autowired
	SecurityTool securityTool;

	@Autowired
	TaskService taskService;

	// Make this a static variable as multiple instances of the
	// CardTaskControllerTest may be created
	private static Card card;

	public static Boolean initialized = false;

	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void before() throws Exception {
		this.securityTool.iAmSystem();
		if (!initialized) {		
			tool.initTestBoard();
			card = tool.getTestCard();
			initialized = true;
		}
	}

	@Test
	public void testDeleteTask() throws Exception {

		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());

		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());

		assertNotNull(tasks);
		assertEquals(tasks.size(), 1);

		
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), task.getTaskid());
		this.taskService.deleteTask(taskToDelete);
		
		tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, task.getUuid());
		assertNotNull(tasks);
		assertEquals(tasks.size(), 0);

	}

	@Test
	public void testTaskSummary() throws Exception {

		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(1, tasks.size());

		TaskActionBody body = new TaskActionBody();
		body.setTaskAction(TaskAction.COMPLETE);
		cardTaskController.taskAction(card.getBoard(), card.getId(), task.getTaskid(), body);

		CardTask complete = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());
		assertTrue(complete.getComplete());

		tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(1, tasks.size());

		Map<String, Integer> summary = cardTaskController.getTaskSummary(TestBoardTool.BOARD_ID, card.getId());
		Integer n = (Integer) summary.get("complete");
		assertEquals(1, n.intValue());
		n = (Integer) summary.get("all");
		assertEquals(n.intValue(), 1);

		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), task.getTaskid());
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testGetTasksCompleteAndRevert() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(tasks.size(), 1);

		TaskActionBody body = new TaskActionBody();
		body.setTaskAction(TaskAction.ASSIGN);
		cardTaskController.taskAction(card.getBoard(), card.getId(), task.getTaskid(), body);

		CardTask taken = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());

		assertEquals("system", SecurityTool.getCurrentUser());
		assertEquals("system", taken.getUser());
		assertTrue(!taken.getComplete());

		body = new TaskActionBody();
		body.setTaskAction(TaskAction.COMPLETE);
		cardTaskController.taskAction(card.getBoard(), card.getId(), TestBoardTool.TASK_ID, body);

		CardTask complete = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());
		assertTrue(complete.getComplete());

		body = new TaskActionBody();
		body.setTaskAction(TaskAction.REVERT);
		cardTaskController.taskAction(card.getBoard(), card.getId(), TestBoardTool.TASK_ID, body);

		CardTask reverted = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());
		assertTrue(!reverted.getComplete());

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), reverted.getTaskid());
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testTaskReverts() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(true);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(tasks.size(), 1);

		cardTaskController.revertTasks(card.getBoard(), task.getTaskid());

		CardTask reverted = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());
		assertTrue(!reverted.getComplete());

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), reverted.getTaskid());
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testTaskRevert() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(true);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(tasks.size(), 1);

		taskService.revertTask(card.getBoard(), card.getId(), task.getTaskid());

		CardTask reverted = cardTaskController.getTask(card.getBoard(), card.getId(), task.getTaskid());
		assertTrue(!reverted.getComplete());

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), reverted.getTaskid());
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testGetTasksByStatusTrueForCardId() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(true);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasksByStatusForCardId(TestBoardTool.BOARD_ID, card.getId(),
				true);

		for (CardTask cardTask : tasks) {
			assertEquals(cardTask.getComplete(), true);
			break;

		}

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), TestBoardTool.TASK_ID);
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testGetTasksByStatusFalseForCardId() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasksByStatusForCardId(TestBoardTool.BOARD_ID, card.getId(),
				false);

		for (CardTask cardTask : tasks) {
			assertEquals(cardTask.getComplete(), false);
			break;

		}

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), TestBoardTool.TASK_ID);
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testGetTasksByStatusEmptyForCardId() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		Collection<CardTask> tasks = cardTaskController.getTasksByStatusForCardId(TestBoardTool.BOARD_ID,
				card.getId() + "123", false);

		assertNotNull(tasks);
		assertEquals(0, tasks.size());

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), TestBoardTool.TASK_ID);
		this.taskService.deleteTask(taskToDelete);

	}

	@Test
	public void testIsTaskCompleteTrue() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(true);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		boolean status = cardTaskController.isTaskComplete(TestBoardTool.BOARD_ID, card.getId(), TestBoardTool.TASK_ID);

		assertEquals(status, true);

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), TestBoardTool.TASK_ID);
		this.taskService.deleteTask(taskToDelete);

	}
	@Test
	public void testIsTaskCompleteFasle() throws Exception {

		securityTool.iAmSystem();
		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(TestBoardTool.TASK_ID);
		task.setCategory("test_category");
		task.setDetail("Test Task");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		cardTaskRepository.save(task);

		boolean status = cardTaskController.isTaskComplete(TestBoardTool.BOARD_ID, card.getId(), TestBoardTool.TASK_ID);

		assertEquals(status, false);

		// Clean up to allow subsequent tests to run
		CardTask taskToDelete = this.taskService.getTask(card.getBoard(),  card.getId(), TestBoardTool.TASK_ID);
		this.taskService.deleteTask(taskToDelete);

	}
}
