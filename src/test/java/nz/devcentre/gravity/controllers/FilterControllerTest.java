/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.AccessType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class FilterControllerTest {
	
	@Autowired
	private FilterController controller;
	
	@Autowired
	TestBoardTool tool;
	
	@Autowired
	BoardsCache cache;
	
	@Autowired
	IdentifierTools identifierTools;
	
	@Autowired
	private SecurityTool securityTool;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
    }
	
	@BeforeEach
	public void before() throws Exception {
		this.securityTool.iAmSystem();
		tool.initTestBoard();
		tool.generateFilters();
		checkTestFilters();
		cache.clearCache();
	}

	@Test
	public void testCreateUpdateAndDeleteFilter() throws Exception {
		Filter filter = getTestFilter("Test Filter "+ BoardControllerTest.RND.nextInt(9999999), "name", Operation.EQUALTO, "Smith");
		Filter newFilter = controller.createFilter(TestBoardTool.BOARD_ID, filter);
		String filterId = newFilter.getId();//TestBoardTool.getIdFromPath(newFilter.getPath());
		newFilter.setName("Updated Filter");
		
		controller.updateFilter(TestBoardTool.BOARD_ID, filterId, newFilter);
		Filter changedFilter = controller.getFilter(TestBoardTool.BOARD_ID, filterId);
		assertEquals( changedFilter.getName(), "Updated Filter");
		assertEquals( changedFilter.getAccess(), filter.getAccess());
		assertEquals( changedFilter.getOwner(), filter.getOwner());
		
		controller.deleteFilter(TestBoardTool.BOARD_ID, filterId);
	}
	
	@Test
	public void testSaveFilterFieldAndDeleteFilterField() throws Exception {
		Filter filter = this.controller.createFilter(TestBoardTool.BOARD_ID, this.getTestFilter("testAddFilter", "name", Operation.EQUALTO, "John Snow"));
		Condition filterCondition = new Condition("balance",Operation.GREATERTHANOREQUALTO,"3000");
		
		this.controller.saveFilterField(TestBoardTool.BOARD_ID, filter.getId(), filterCondition);
		Filter filter2 = this.controller.getFilter(TestBoardTool.BOARD_ID, filter.getId());
		
		assertEquals(2, filter2.getConditions().size());
		assertEquals(filterCondition, filter2.getConditions().get("balance"));
		
		this.controller.deleteFilterField(TestBoardTool.BOARD_ID, filter.getId(), "balance");
		filter2 = this.controller.getFilter(TestBoardTool.BOARD_ID, filter.getId());
		assertEquals(1, filter2.getConditions().size());
		assertFalse(filter2.getConditions().containsKey("balance"));
		
	}

	@Test
	public void testListFilters() throws Exception {
		checkTestFilters();
		Map<String, String> listFilters = controller.listFilters(TestBoardTool.BOARD_ID);
		assertTrue(listFilters.containsKey("equalsfilter"));
	}

	@Test
	public void testEquals() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "equalsfilter");
		assertNotNull(cards);
		assertEquals(1,cards.size());
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assert( fields.get("name").equals("Smith"));
		}
	}

	@Test
	@Disabled
	public void testContains() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "containsfilter");
		assertNotNull(cards);
		assertEquals(1,cards.size());
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assert( fields.get("name").equals("Smith"));
		}
	}
	
	@Test
	public void testNotEquals() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "notequalsfilter");
		assertNotNull(cards);
		
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assertNotSame( "Smith", fields.get("name"));
		}
	}
	
	@Test
	public void testGreaterThan() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "greaterthanfilter");
		assertNotNull(cards);
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assertTrue( (Double)fields.get("balance") > 2000l);
		}
	}

	@Test
	public void testGreaterThanOrEqualTo() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "greaterthanorequaltofilter");
		assertNotNull(cards);
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assertTrue( (Double)fields.get("balance") >= 2000l);
		}
	}
	
	@Test
	public void testLessThan() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "lessthanfilter");
		assertNotNull(cards);
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assertTrue( (Double)fields.get("balance") < 2000l);
		}
	}

	@Test
	public void testLessThanOrEqualTo() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "lessthanorequaltofilter");
		assertNotNull(cards);
		for( Card card : cards ){
			Map<String, Object> fields = card.getFields();
			assertTrue( (Double)fields.get("balance") <= 2000l);
		}
	}
	
	@Test
	public void testNotNull() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "notnullfilter");
		assertNotNull(cards);
		assertTrue(cards.size()>7);
	}

	@Test
	public void testIsNull() throws Exception {
		Collection<Card> cards = controller.executeFilter(TestBoardTool.BOARD_ID, null, "isnullfilter");
		assertNotNull(cards);
		assertTrue(cards.size()>7);
	}
	
	@Test
	public void testGetPermissions() throws Exception{
		Map<String, String> permissions = controller.getPermissions(TestBoardTool.BOARD_ID, "test_filter");
		assertNotNull(permissions);
		String testUserPermission = permissions.get("test_user");
		assertEquals("test_role", testUserPermission);
	}
	
	@Test
	public void testAddPermissionsNoReplace() throws Exception{
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("new-user", "new-role");
		controller.addPermissions(TestBoardTool.BOARD_ID, "test_filter", false, permissions);
		cache.clearCache();
		
		Map<String, String> newPermissions = controller.getPermissions(TestBoardTool.BOARD_ID, "test_filter");
		assertNotNull(newPermissions);
		String testUserPermission = newPermissions.get("new-user");
		assertEquals("new-role", testUserPermission);
	}
	
	@Test
	public void testDeletePermission() throws Exception{
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("delete-user", "delete-role");
		controller.addPermissions(TestBoardTool.BOARD_ID, "test_filter", false, permissions);
		cache.clearCache();
		
		Map<String, String> newPermissions = controller.getPermissions(TestBoardTool.BOARD_ID, "test_filter");
		assertNotNull(newPermissions);
		String testUserPermission = newPermissions.get("delete-user");
		assertEquals("delete-role", testUserPermission);
		
		controller.deletePermissions(TestBoardTool.BOARD_ID, "test_filter", "delete-user");
		cache.clearCache();
		
		Map<String, String> afterPermissions = controller.getPermissions(TestBoardTool.BOARD_ID, "test_filter");
		assertNotNull(afterPermissions);
		testUserPermission = afterPermissions.get("delete-user");
		assertNull(testUserPermission);
	}
	
	private void checkTestFilters() throws Exception{
		Map<String, String> listFilters = controller.listFilters(TestBoardTool.BOARD_ID);
		Collection<Filter> f = new ArrayList<Filter>();
		f.add(getTestFilter( "containsfilter", "name", Operation.CONTAINS, "Smi" ));
		f.add(getTestFilter( "equalsfilter", "name", Operation.EQUALTO, "Smith" ));
		f.add(getTestFilter( "notequalsfilter", "name", Operation.NOTEQUALTO, "Smith" ));
		f.add(getTestFilter( "greaterthanfilter", "balance", Operation.GREATERTHAN, "2000" ));
		f.add(getTestFilter( "greaterthanorequaltofilter", "balance", Operation.GREATERTHANOREQUALTO, "2000" ));
		f.add(getTestFilter( "lessthanfilter", "balance", Operation.LESSTHAN, "2000" ));
		f.add(getTestFilter( "lessthanorequaltofilter", "balance", Operation.LESSTHANOREQUALTO, "2000" ));
		f.add(getTestFilter( "isnullfilter", "nofieldhere", Operation.ISNULL, "" ));
		f.add(getTestFilter( "notnullfilter", "name", Operation.NOTNULL, "" ));
		
		for( Filter filter : f){
			if( !listFilters.containsKey(filter.getName())){
				controller.createFilter( TestBoardTool.BOARD_ID, filter );
			}			
		}
	}
		
	private Filter getTestFilter(String filterName, String fieldName, Operation operation, String value) {
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		conditions.put("a", new Condition(fieldName,operation,value));
		
		Filter filter = new Filter();
		filter.setName(filterName);
		filter.setConditions(conditions);
		filter.setOwner("smith");
		filter.setAccess(AccessType.WRITE);
		return filter;
	}

	@Test
	public void testSequenceGenerator() throws Exception {

		Long seq = (Long) identifierTools.getNextSequence("test","cardno");
			seq = (Long) identifierTools.getNextSequence("test","cardno");
		}
	
}
