/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.CardLock;
import nz.devcentre.gravity.model.Lock;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CardsLockCacheTest {

	@InjectMocks
	CardsLockCache cardsLockCache;
	
	@Autowired
	SecurityTool securityTool;
	
	@Mock
	RabbitTemplate rabbitTemplate;
	
	@Mock
	FanoutExchange gravityCardLockingExchange;
	
	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}
	
	@BeforeEach
	public void setUp() throws Exception{
	
		this.rabbitTemplate=Mockito.mock(RabbitTemplate.class);
		this.gravityCardLockingExchange=Mockito.mock(FanoutExchange.class);
		this.cardsLockCache=new CardsLockCache();
	
		MockitoAnnotations.openMocks(this);
		Mockito.when(Mockito.mock(FanoutExchange.class).getName()).thenReturn("mock-quque");
		Mockito.doNothing().when(Mockito.mock(RabbitTemplate.class)).convertAndSend("mock-queue","",new CardLock());
		
		this.cardsLockCache.actualLock(new CardLock(TestBoardTool.BOARD_ID, "TE0001", "system", true));
		this.cardsLockCache.actualLock(new CardLock(TestBoardTool.BOARD_ID, "TE0002", "system", true));
	}
	
	@Test
	public void actualLockTest() {
		MockitoAnnotations.openMocks(this) ;
		Lock lock = this.cardsLockCache.getLock(TestBoardTool.BOARD_ID, "TE0001");
		
		assertNotNull(lock);
		
		lock = this.cardsLockCache.getLock(TestBoardTool.BOARD_ID, "UnknownCard");
		assertNull(lock);
		
	}
	@Test
	public void getLocksTest() {
		Map<String, Lock> locks = this.cardsLockCache.getLocks(TestBoardTool.BOARD_ID);
		assertTrue(locks.containsKey("TE0001"));
	}
	
	@Test
	public void isLockedTest() throws Exception {
		try {
			this.securityTool.iAmSystem();

			assertFalse(this.cardsLockCache.isLocked(TestBoardTool.BOARD_ID, "TE0001"));
			
			assertFalse(this.cardsLockCache.isLocked(TestBoardTool.BOARD_ID, "UnknownCard"));
			
			this.securityTool.iAm("test");
			assertTrue(this.cardsLockCache.isLocked(TestBoardTool.BOARD_ID, "TE0001"));
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Test
	public void onMessageTest() throws JsonProcessingException {
		CardLock cardLock = new CardLock(TestBoardTool.BOARD_ID, "TE0002", "system", false);
		Message message = new Message(SerializationUtils.serialize(cardLock), new MessageProperties());
		this.cardsLockCache.onMessage(message);
		assertFalse(this.cardsLockCache.isLocked(TestBoardTool.BOARD_ID, "TE0002"));
	}
	
	@Test
	public void zzzUnlockTest() {
		Lock lock = this.cardsLockCache.getLock(TestBoardTool.BOARD_ID, "TE0002");
		lock.setTimeStamp(new Date(new Date().getTime()-6000000));
		this.cardsLockCache.unlock();
		lock= this.cardsLockCache.getLock(TestBoardTool.BOARD_ID, "TE0002");
		assertNull(lock);
	}
	
	@Test
	public void zzUnlockTest() throws Exception {
		this.securityTool.iAm("mock-user");
		assertFalse(this.cardsLockCache.unlock(TestBoardTool.BOARD_ID, "TE0001"));
		
		this.securityTool.iAmSystem();
		assertTrue(this.cardsLockCache.unlock(TestBoardTool.BOARD_ID, "TE0001"));
	}
	
	@Test
	public void lockTest() throws Exception {
			this.cardsLockCache.lock(TestBoardTool.BOARD_ID, "Mock-Card");
			assertFalse(this.cardsLockCache.lock(TestBoardTool.BOARD_ID, "TE0001"));
			this.securityTool.iAmSystem();
			assertTrue(this.cardsLockCache.lock(TestBoardTool.BOARD_ID, "TE0001"));
			
	}
}
