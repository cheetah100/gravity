package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.BoardResource;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ResourceControllerTest {

	@Autowired
	TestBoardTool tool;
	
	@Autowired
	ResourceController resourceController;
	
	@Autowired
	private ResourceCache resourceCache;
	
	public static final String RESOURCE_ID="test_board_test_resource";
	
	public static final String RESOURCE_BODY="[{ \"$match\" : { \"$and\" : [ { \"$or\" : [ { \"metadata.phase\" : \"test_phase\"} , { \"metadata.phase\" : \"next_phase\"}]}]}}]";
	
	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@Test
	public void createTest() throws Exception {
		this.tool.initTestBoard();
		this.resourceController.createResourceByJsonString(TestBoardTool.BOARD_ID, RESOURCE_ID, "Dummy Resource Content", null, "txt");
		String resource = this.resourceController.getResource(TestBoardTool.BOARD_ID, RESOURCE_ID);
		assertEquals("Dummy Resource Content", resource);
	}
	
	@Test
	public void createUpdateTest() throws Exception {
		this.resourceController.createResourceByJsonString(TestBoardTool.BOARD_ID, RESOURCE_ID, RESOURCE_BODY, null, "txt");
		this.resourceCache.clearCache();
		String resource = this.resourceController.getResource(TestBoardTool.BOARD_ID, RESOURCE_ID);
		assertEquals(RESOURCE_BODY, resource);	
	}

	@Test
	public void createTestFile() throws Exception {
		
		MockMultipartFile file =
                new MockMultipartFile(
                        "test",
                        "test.json",
                        MediaType.APPLICATION_JSON_UTF8_VALUE,
                        "{'name':'test'}".getBytes("utf-8"));

		this.resourceController.createResourceByFile(TestBoardTool.BOARD_ID, "file_resource", file, null);
		String resource = this.resourceController.getResource(TestBoardTool.BOARD_ID, "file_resource");
		assertEquals("{'name':'test'}", resource);
	}

	
	@Test
	public void getResourceJsonTest() throws Exception {
		this.resourceController.createResourceByJsonString(TestBoardTool.BOARD_ID, RESOURCE_ID, RESOURCE_BODY, "unit_test_version", "txt");
		this.resourceCache.clearCache();
		BoardResource resourceJson = this.resourceController.getResourceJson(TestBoardTool.BOARD_ID, RESOURCE_ID);
		assertEquals("unit_test_version", resourceJson.getVersion());
		
	}
	
	@Test
	public void getResourceFileTest() throws Exception {
		ResponseEntity<byte[]>re = this.resourceController.getResourceFile(TestBoardTool.BOARD_ID, "file_resource");
		byte[] response = re.getBody();
		String json = new String( response, "utf-8");
		assertEquals("{'name':'test'}", json);
	}
	
	@Test
	public void resourceActivateVersionTest() throws Exception {
		//With Version and Active as False
		this.resourceController.createResourceByJsonString(TestBoardTool.BOARD_ID, RESOURCE_ID, RESOURCE_BODY, "unit_test_version", "txt");
		this.resourceCache.clearCache();
		this.resourceController.deleteResource(TestBoardTool.BOARD_ID,RESOURCE_ID);
		assertNull(this.resourceController.getResourceJson(TestBoardTool.BOARD_ID, RESOURCE_ID));
		
		BoardResource activateResource = this.resourceController.activateResource(TestBoardTool.BOARD_ID, RESOURCE_ID, "unit_test_version");
		activateResource = this.resourceController.getResourceJson(TestBoardTool.BOARD_ID, RESOURCE_ID);
		assertEquals("unit_test_version", activateResource.getVersion());
		assertTrue(activateResource.isActive());
	}
	
	@Test
	public void resourceActivateVersionExceptionTest() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.resourceController.activateResource(TestBoardTool.BOARD_ID, RESOURCE_ID, "unknown_alien_version");
		}, "ResourceNotFoundException was expected");
		
	}
		
	@Test
	public void listResourcesTest() throws Exception {
		Map<String, String> listResources = this.resourceController.listResources(TestBoardTool.BOARD_ID, false);
		assertTrue(listResources.containsKey(RESOURCE_ID));
	}
	
	@Test
	public void listAllResourcesTest() throws Exception {
		Map<String, String> listResources = this.resourceController.listResources(TestBoardTool.BOARD_ID, true);
		assertTrue(listResources.containsKey(RESOURCE_ID));
	}
	
	@Test
	public void zDeleteResourcesTest() throws Exception {
		this.resourceController.deleteResource(TestBoardTool.BOARD_ID, RESOURCE_ID);
		List<BoardResource> resourceJsonHistory = this.resourceController.getResourceJsonHistory(TestBoardTool.BOARD_ID, RESOURCE_ID);
		assertTrue(resourceJsonHistory.size()==3);
	}
	
	@Test
	public void zDeleteResourcesExceptionTest() throws Exception {
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			this.resourceController.deleteResource(TestBoardTool.BOARD_ID,"unknown_alien_resource");
		}, "ResourceNotFoundException was expected");		
	}

}
