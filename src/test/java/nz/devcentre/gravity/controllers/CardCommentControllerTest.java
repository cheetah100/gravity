/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.Iterator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardComment;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CardCommentControllerTest {
	
	protected static String VIEW_ID = "testview";
	
	@Autowired
	private CardCommentController cardCommentController;	
	
	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	private SecurityTool securityTool;
	
	// Make this a static variable as multiple instances of the CardTaskControllerTest may be created
	private static Card card;
	
	public static Boolean initialized = false;
	
    @Configuration
    @Import({FongoConfiguration.class, RabbitConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig{
       
    }
	
	@BeforeEach
	public void before() throws Exception {
		securityTool.iAmSystem();
		if (!initialized) {
			tool.initTestBoard();
			card = tool.getTestCard();
			initialized = true;
		}
	}
	
	/**
	 * testSaveComment - test the saveComment method that takes a single comment instead of a map
	 * @throws Exception
	 */

	@Test
	public void testSaveComment() throws Exception {
		
		Collection<CardComment> comments = cardCommentController.getComments(TestBoardTool.BOARD_ID, card.getId());
		
		int initialSize = comments.size();
		
		CardComment newComment = cardCommentController.saveComment(TestBoardTool.BOARD_ID,
				card.getId(),
				"This is a new test comment!");
		
		assertNotNull(newComment);
		
		comments = cardCommentController.getComments(TestBoardTool.BOARD_ID, card.getId());
		
		assertTrue(comments.size()>initialSize);
		CardComment event = null;
		Iterator<CardComment> itr = comments.iterator();
		
		for ( ; itr.hasNext(); ) {
			event = itr.next();
			if (event.getUuid().equals(newComment.getUuid()))
				break;
		}
		assertEquals( event.getDetail(), newComment.getDetail());		
	}	

	/**
	 * testSaveAndGetComments - test the saveComment method that takes a map of comments
	 * @throws Exception
	 */
	
	@Test
	public void testSaveAndGetComments() throws Exception {
		
		Collection<CardComment> comments = cardCommentController.getComments(TestBoardTool.BOARD_ID, card.getId());
		int initialSize = comments.size();
		
		CardComment newComment = cardCommentController.saveComment(TestBoardTool.BOARD_ID, card.getId(), "testing");
		
		assertNotNull(newComment);
		
		comments = cardCommentController.getComments(TestBoardTool.BOARD_ID, card.getId());
		
		assertTrue(comments.size()>initialSize);
		CardEvent event = null;
		Iterator<CardComment> itr = comments.iterator();
		for ( ; itr.hasNext(); ) {
			event = itr.next();
			if (event.getUuid().equals(newComment.getUuid()))
				break;
		}
		assertEquals( event.getDetail(), newComment.getDetail());		
	}
	
}
