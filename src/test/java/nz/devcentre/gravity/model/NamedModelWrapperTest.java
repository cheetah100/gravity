package nz.devcentre.gravity.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

public class NamedModelWrapperTest {

	@Test
	public void testWrap() {
		
		Metadata metadata = new Metadata();
		metadata.setCreated(new Date());
		metadata.setCreator("test");
		
		Integration integration = new Integration();
		integration.setId("test");
		integration.setName("name");
		integration.setDescription("description");
		integration.setMetadata(metadata);
		
		NamedModelWrapper wrapper = NamedModelWrapper.wrap(integration);
		
		assertEquals( wrapper.getId(), integration.getId());
		assertEquals( wrapper.getName(), integration.getName());
		assertEquals( wrapper.getDescription(), integration.getDescription());
	}

	@Test
	public void testWrapList() {
		Metadata metadata = new Metadata();
		metadata.setCreated(new Date());
		metadata.setCreator("test");
		
		Integration integration = new Integration();
		integration.setId("test");
		integration.setName("name");
		integration.setDescription("description");
		integration.setMetadata(metadata);
		
		List<Integration> list = new ArrayList<Integration>();
		list.add(integration);
		
		List<NamedModelWrapper> wrapperList = NamedModelWrapper.wrapList(list);
		
		assertEquals(1, wrapperList.size());
		
		NamedModelWrapper wrapper = wrapperList.get(0);
		
		assertEquals( wrapper.getId(), integration.getId());
		assertEquals( wrapper.getName(), integration.getName());
		assertEquals( wrapper.getDescription(), integration.getDescription());
		assertEquals( wrapper.getId(), integration.getId());
		assertEquals( wrapper.getName(), integration.getName());
		assertEquals( wrapper.getDescription(), integration.getDescription());
	}

}
