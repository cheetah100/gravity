/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class DatabaseDriverTest {

	@Test
	public void getTeradataDatabaseDriverByUrl() {
		DatabaseDriver dd = DatabaseDriver.fromUrl("jdbc:teradata://MyDatabaseServer/database=MyDatabaseName,tmode=ANSI,charset=UTF8");
		assertEquals( DatabaseDriver.TERADATA, dd);
	}
	
	@Test
	public void getMsSqlDatabaseDriverByUrl() {
		DatabaseDriver dd = DatabaseDriver.fromUrl("jdbc:sqlserver://localhost;user=MyUserName;password=*****;");
		assertEquals( DatabaseDriver.MSSQL, dd);
	}
	
	@Test
	public void getMongoDatabaseDriverByUrl() {
		DatabaseDriver dd = DatabaseDriver.fromUrl("mongodb://mongodb0.example.com:27017");
		assertEquals( DatabaseDriver.MONGO, dd);
	}

}
