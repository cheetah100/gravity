package nz.devcentre.gravity.model.importer;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.controllers.ImportController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:test-controllers.xml" })
public class UploadServiceTest {

	private MockMvc mockMvc;

	@Autowired
	WebApplicationContext wContext;

	@Autowired
	private ImportController importController;
	
	@Autowired
	private UploadService uploadService;
	
	@Autowired
	TestBoardTool tool;
	
	@Autowired
	SecurityTool securityTool;
	
	@Configuration
	@Import({ FongoConfiguration.class, RabbitConfiguration.class })
	@ImportResource("classpath:/test-controllers.xml")
	public static class ContextConfig {

	}

	@BeforeEach
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wContext).alwaysDo(MockMvcResultHandlers.print()).build();
		this.tool.initTestBoard();
		this.securityTool.iAmSystem();
	}

	@Test
	public void createUploadTest() throws IOException, Exception {
		try {
			this.mockMvc.perform(
					MockMvcRequestBuilders.fileUpload("/import/submit")
					.file("file", null)
					.characterEncoding("UTF-8"))
					.andDo(MockMvcResultHandlers.print());

		} catch (Exception e) {
			assertTrue("Request processing failed; nested exception is java.lang.Exception: No Content"
					.equals(e.getMessage()));
		}

	}

	@Test
	public void createUploadTest2() throws IOException, Exception {

		Workbook dummyWb = new XSSFWorkbook();

		Row mockRow = dummyWb.createSheet().createRow(1);

		mockRow.createCell(0).setCellValue("dummy1");
		mockRow.createCell(2).setCellValue("current");
		mockRow.createCell(2).setCellValue("Actor1");
		mockRow.createCell(3).setCellValue("Movie1|Movie2|Movie3|Movie4|Movie5");

		try {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				dummyWb.write(outStream);
				byte[] byteArray = outStream.toByteArray();
				outStream.close();
				dummyWb.close();
				this.mockMvc
						.perform(MockMvcRequestBuilders.fileUpload("/import/submit")
								.file("file", byteArray)
								.characterEncoding("UTF-8"))
						.andDo(MockMvcResultHandlers.print())
						.andExpect(MockMvcResultMatchers.status().isOk());
				
				this.mockMvc
				.perform(MockMvcRequestBuilders.fileUpload("/import")
						.file("file", byteArray)
						.characterEncoding("UTF-8"))
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());


		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	@Test
	public void createUploadTest3() throws Exception {
		Workbook dummyWb = new XSSFWorkbook();
		
		Sheet dummySheet = dummyWb.createSheet("test_board");
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("Id");
		mockRow.createCell(1).setCellValue("Phase");
		mockRow.createCell(2).setCellValue("Card Title");
		mockRow.createCell(3).setCellValue("Name");
		mockRow.createCell(4).setCellValue("Phone");

		
		Row mockRow2 =dummySheet.createRow(1);
		mockRow2.createCell(0).setCellValue("TE001010");
		mockRow2.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(3).setCellValue("dummy1");
		mockRow2.createCell(4).setCellValue("10001");
		
		
		TransferStatus transferStatus = new TransferStatus();
		transferStatus.newId();
		transferStatus.setFileName("test_board.xlsx");
		
		//Create New Card
		this.uploadService.processUpload(dummyWb, null, transferStatus);
		
		mockRow2.createCell(4).setCellValue("10002");
		
		//Update Existing Card
		this.uploadService.processUpload(dummyWb, null, transferStatus);
		
		Sheet unKnownSheet = dummyWb.createSheet("missing_board");
		
		Row mockRow3 = unKnownSheet.createRow(0);

		mockRow3.createCell(0).setCellValue("Id");
		mockRow3.createCell(1).setCellValue("Phase");
		mockRow3.createCell(2).setCellValue("Card Title");
		mockRow3.createCell(3).setCellValue("Name");
		mockRow3.createCell(4).setCellValue("Phone");

		
		Row mockRow4 =unKnownSheet.createRow(1);
		mockRow4.createCell(0).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(3).setCellValue("dummy1");
		mockRow4.createCell(4).setCellValue("10001");
		
		//Create New Card-Unknown Board
		this.uploadService.processUpload(dummyWb, null, transferStatus);
		
		this.importController.processSheets(dummyWb, null);
		dummyWb.close();
		
	}
	
	@Test
	public void createUploadTest4() throws Exception {
		Workbook dummyWb = new XSSFWorkbook();
		
		Sheet dummySheet = dummyWb.createSheet("test_board");
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("Id");
		mockRow.createCell(1).setCellValue("Phase");
		mockRow.createCell(2).setCellValue("Card Title");
		mockRow.createCell(3).setCellValue("Name");
		mockRow.createCell(4).setCellValue("Phone");

		
		Row mockRow2 =dummySheet.createRow(1);
		mockRow2.createCell(0).setCellValue("TE001007");
		mockRow2.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(3).setCellValue("dummy1");
		mockRow2.createCell(4).setCellValue("10001");
		
		
		TransferStatus transferStatus = new TransferStatus();
		transferStatus.newId();
		transferStatus.setFileName("test_board.xlsx");
		
		//Create New Card
		this.importController.processSheets(dummyWb, null);
		
		mockRow2.createCell(4).setCellValue("10002");
		
		//Update Existing Card
		this.importController.processSheets(dummyWb, null);
		
		Sheet unKnownSheet = dummyWb.createSheet("missing_board");
		
		Row mockRow3 = unKnownSheet.createRow(0);

		mockRow3.createCell(0).setCellValue("Id");
		mockRow3.createCell(1).setCellValue("Phase");
		mockRow3.createCell(2).setCellValue("Card Title");
		mockRow3.createCell(3).setCellValue("Name");
		mockRow3.createCell(4).setCellValue("Phone");

		
		Row mockRow4 =unKnownSheet.createRow(1);
		mockRow4.createCell(0).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow4.createCell(3).setCellValue("dummy1");
		mockRow4.createCell(4).setCellValue("10001");
		
		this.importController.processSheets(dummyWb, null);
		
		dummyWb.close();
	}
	@Test
	public void createUploadTest5() throws IOException {
		
		Workbook dummyWb = new XSSFWorkbook();
		Sheet dummySheet = dummyWb.createSheet("test_board");
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("Id");
		mockRow.createCell(1).setCellValue("Phase");
		mockRow.createCell(2).setCellValue("Card Title");
		mockRow.createCell(3).setCellValue("Name");
		mockRow.createCell(4).setCellValue("Phone");

		
		Row mockRow2 =dummySheet.createRow(1);
		mockRow2.createCell(0).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow2.createCell(3).setCellValue("dummy1");
		mockRow2.createCell(4).setCellValue("10001");
		
		Row mockRow3 =dummySheet.createRow(1);
		mockRow3.createCell(0).setCellValue("TE001007");
		mockRow3.createCell(1).setCellValue("test_phase");
		mockRow3.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow3.createCell(3).setCellValue("dummy1");
		mockRow3.createCell(4).setCellValue("10001");
		
		Sheet unKnownSheet = dummyWb.createSheet("missing_board");
		
		Row mockRow4 = unKnownSheet.createRow(0);

		mockRow4.createCell(0).setCellValue("Id");
		mockRow4.createCell(1).setCellValue("Phase");
		mockRow4.createCell(2).setCellValue("Card Title");
		mockRow4.createCell(3).setCellValue("Name");
		mockRow4.createCell(4).setCellValue("Phone");

		
		Row mockRow5 =unKnownSheet.createRow(1);
		mockRow5.createCell(0).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow5.createCell(1).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow5.createCell(2).setCellType(Cell.CELL_TYPE_BLANK);
		mockRow5.createCell(3).setCellValue("dummy1");
		mockRow5.createCell(4).setCellValue("10001");
		
		try {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				dummyWb.write(outStream);
				byte[] byteArray = outStream.toByteArray();
				MultipartFile multipartFile = new MockMultipartFile("file","test_board.xlsx", "text/plain", byteArray);
				this.importController.uploadXcelFile(multipartFile,Mockito.mock(HttpServletRequest.class), "test_board", "test_board", null);
				outStream.close();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		dummyWb.close();
	}

	@Test
	public void createUploadPorcessTest() throws Exception {
	Workbook dummyWb = new XSSFWorkbook();
		
		Sheet dummySheet = dummyWb.createSheet("test_board");
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("Id");
		mockRow.createCell(1).setCellValue("Phase");
		mockRow.createCell(2).setCellValue("Card Title");
		mockRow.createCell(3).setCellValue("Name");
		mockRow.createCell(4).setCellValue("Phone");

		
		
		TransferStatus transferStatus = new TransferStatus();
		transferStatus.newId();
		transferStatus.setFileName("test_board.xlsx");
		this.securityTool.iAmSystem();
		UploadProcess uploadProcess=new UploadProcess(dummyWb, null, SecurityContextHolder.getContext().getAuthentication(), transferStatus, this.importController);
		try {
			uploadProcess.run();
			assertTrue(SecurityContextHolder.getContext().getAuthentication().getName().equalsIgnoreCase("system"));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
	@Test
	public void createImportProcessTest() throws Exception {
	Workbook dummyWb = new XSSFWorkbook();
		
		Sheet dummySheet = dummyWb.createSheet("test_board");
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("Id");
		mockRow.createCell(1).setCellValue("Phase");
		mockRow.createCell(2).setCellValue("Card Title");
		mockRow.createCell(3).setCellValue("Name");
		mockRow.createCell(4).setCellValue("Phone");

		
		
		TransferStatus transferStatus = new TransferStatus();
		transferStatus.newId();
		transferStatus.setFileName("test_board.xlsx");
		this.securityTool.iAmSystem();
		ImportProcess importProcess =new ImportProcess(this.importController,dummyWb, SecurityContextHolder.getContext().getAuthentication(), null);
		try {
			importProcess.run();
			assertTrue(SecurityContextHolder.getContext().getAuthentication().getName().equalsIgnoreCase("system"));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}
	

}
