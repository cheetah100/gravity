package nz.devcentre.gravity.model.importer;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.TemplateField;


@ExtendWith(MockitoExtension.class)
public class SheetUtilTest {

	private SheetUtil sheetUtil = new SheetUtil();
	
	TestBoardTool tool;

	@Mock
	BoardController boardController;
	
	@Mock
	CardController cardController;
	
	private Map<Integer, TemplateField> headerMap = new LinkedHashMap<Integer, TemplateField>();

	@BeforeEach
	public void setUp() throws Exception {
		
		this.tool = new TestBoardTool();
		//this.tool.initTestBoard();

		TemplateField fieldId = new TemplateField();
		fieldId.setId("id");
		fieldId.setName("id");
		fieldId.setType(FieldType.STRING);

		this.headerMap.put(0, fieldId);

		TemplateField fieldPhase = new TemplateField();
		fieldPhase.setId("phase");
		fieldPhase.setName("phase");
		fieldPhase.setLabel("phase");
		fieldPhase.setType(FieldType.STRING);

		this.headerMap.put(1, fieldPhase);

		TemplateField fieldName = new TemplateField();
		fieldName.setId("name");
		fieldName.setName("name");
		fieldName.setType(FieldType.STRING);

		this.headerMap.put(2, fieldName);

		TemplateField fieldMovies = new TemplateField();
		fieldMovies.setId("movies");
		fieldMovies.setName("movies");
		fieldMovies.setType(FieldType.LIST);

		this.headerMap.put(3, fieldMovies);

	}

	@Test
	public void readCardRowTest() throws IOException {
		System.out.println("Start Test");

		Workbook dummyWb = new HSSFWorkbook();

		Row mockRow = dummyWb.createSheet().createRow(1);

		mockRow.createCell(0).setCellValue("dummy1");
		mockRow.createCell(2).setCellValue("current");
		mockRow.createCell(2).setCellValue("Actor1");
		mockRow.createCell(3).setCellValue("Movie1|Movie2|Movie3|Movie4|Movie5");

		//Template template = new Template();
		//template.setBoardId("dummy");
		//template.setId("dummy");

		Card readCardRow = this.sheetUtil.readCardRow(mockRow, this.headerMap);
		dummyWb.close();

		Object field = readCardRow.getField("movies");

		assertTrue(field instanceof List && ((List<?>) field).size() == 5);
	}

	@Test
	public void readCardRowTest2() throws IOException {

		Workbook dummyWb = new HSSFWorkbook();

		Row mockRow = dummyWb.createSheet().createRow(1);

		mockRow.createCell(0).setCellValue("dummy1");
		mockRow.createCell(1).setCellValue("current");
		mockRow.createCell(2).setCellValue("Actor1");
		mockRow.createCell(3).setCellValue("Movie1,Movie2");
		mockRow.createCell(4).setCellType(Cell.CELL_TYPE_BLANK);

		//Template template = new Template();
		//template.setBoardId("dummy");
		//template.setId("dummy");

		Card readCardRow = this.sheetUtil.readCardRow(mockRow, this.headerMap);
		dummyWb.close();

		Object field = readCardRow.getField("movies");
		assertTrue(field instanceof List && ((List<?>) field).size() == 1);
	}

	@Test
	public void readCardRowTest3() throws IOException {

		// Test NULL field
		Workbook dummyWb = new HSSFWorkbook();

		Row mockRow = dummyWb.createSheet().createRow(1);

		mockRow.createCell(0).setCellValue(StringUtils.EMPTY);
		mockRow.createCell(1).setCellValue("Actor1");
		mockRow.createCell(2).setCellValue(StringUtils.EMPTY);

		//Template template = new Template();
		//template.setBoardId("dummy");
		//template.setId("dummy");

		Map<Integer, TemplateField> header = new HashMap<>(this.headerMap);
		header.put(3, null);

		Card readCardRow = this.sheetUtil.readCardRow(mockRow, header);
		dummyWb.close();

		assertTrue(readCardRow.getField("movies") == null);
	}

	@Test
	public void getStringValueTest() {
		String stringValue = this.sheetUtil.getStringValue("Test");

		assertTrue("Test".equals(stringValue));

		stringValue = this.sheetUtil.getStringValue(Arrays.asList(new String[] { "Test1", "Test2" }));

		assertTrue("Test1|Test2".equals(stringValue));

		stringValue = this.sheetUtil.getStringValue(new Date());

		stringValue = this.sheetUtil.getStringValue(100);

	}

	@Test
	@Disabled
	public void readHeadersFromSheetTest() throws IOException {
		Workbook dummyWb = new HSSFWorkbook();
		Sheet dummySheet = dummyWb.createSheet();
		Row mockRow = dummySheet.createRow(0);

		mockRow.createCell(0).setCellValue("id");
		mockRow.createCell(1).setCellValue("phase");
		mockRow.createCell(2).setCellValue("name");
		mockRow.createCell(3).setCellValue("movies");

		Map<String, TemplateField> header = new HashMap<String, TemplateField>();

		TemplateField fieldName = new TemplateField();
		fieldName.setId("name");
		fieldName.setName("name");
		fieldName.setLabel("name");
		fieldName.setType(FieldType.STRING);

		header.put("name", fieldName);

		TemplateField fieldMovies = new TemplateField();
		fieldMovies.setId("movies");
		fieldMovies.setName("movies");
		fieldMovies.setLabel("movies");
		fieldMovies.setType(FieldType.LIST);
		header.put("movies", fieldName);

		//Template template = new Template();
		//template.setBoardId("dummy");
		//template.setId("dummy");
		
		//TemplateGroup templateGroup = new TemplateGroup("dummy");
		//templateGroup.setFields(header);
		
		//template.setGroups(ImmutableMap.of("dummy", templateGroup));

		//Map<Integer, TemplateField> readHeadersFromSheet = this.sheetUtil.readHeadersFromSheet(dummySheet,
		//		template.getBoardId(), template);
		
		//assertTrue(readHeadersFromSheet.size() == 3);
		
		Row mockRow1 = dummySheet.createRow(1);
		mockRow1 = mockRow;
		
		//Test Size column
		this.sheetUtil.sizeColumns(dummySheet, 4);
		
		//Create Style Test
		CellStyle createStyle = this.sheetUtil.createStyle("Arial", 15, new Short("1"), dummyWb);
		assertNotNull(createStyle);
	
		Map<String, CellStyle> createStyles = this.sheetUtil.createStyles(dummyWb);
		assertTrue(createStyles.size()==4);
		
		this.sheetUtil.makeCell(mockRow1, 1, "test",createStyle);
		
		dummyWb.close();

	}

	@Test
	public void getDateFromStringTest() {
		Date dateFromString = this.sheetUtil.getDateFromString("01/01/2050");
		assertTrue(dateFromString instanceof Date);

		dateFromString = this.sheetUtil.getDateFromString(null);
		assertTrue(dateFromString == null);

		dateFromString = this.sheetUtil.getDateFromString("01/01/2050 00:00:00");
		assertTrue(dateFromString == null);

		dateFromString = this.sheetUtil.getDateFromString("32/bb/2050");
		assertTrue(dateFromString == null);
	}

	@Test
	public void getStringsTest() {
		Object[] obj = new Object[] { "Test", "Test1", "Test2" };
		String[] strings = this.sheetUtil.getStrings(obj);
		assertTrue(strings.length == 3);
	}
	@Test
	public void getHeaderMapTest() {
		Map<String,String> headerMap=ImmutableMap.of("key","value");
		Map<String, String> headerMap2 = this.sheetUtil.getHeaderMap(headerMap);
		assertTrue(headerMap2.get("value").equals("key"));
	}
	@Test
	public void writeCardRowTest() throws Exception {
		Workbook dummyWb = new HSSFWorkbook();
		Sheet createSheet = dummyWb.createSheet();
		
		//Template template = TestBoardTool.getTestTemplate();		
		Card card = TestBoardTool.getTestCard("test", "test_board", 100);		
		card.addField("balance", "null");
		card.addField(UploadService.ERRORMESSAGEFIELD, "No Error");
		
		//this.sheetUtil.writeCardRow(createSheet, card,  , 0);
		dummyWb.close();
	}
}
