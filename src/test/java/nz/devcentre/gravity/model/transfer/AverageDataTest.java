package nz.devcentre.gravity.model.transfer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;

public class AverageDataTest {

	@Test
	public void testGetData() {
		PivotData data = new AverageData();
		
		data.addData("B", "A", 2);
		data.addData("B", "B", 2);
		data.addData("B", "B", 2);
		data.addData("B", "C", 2);
		
		data.addData("A", "A", 5.5);
		data.addData("A", "A", 5.5);
		data.addData("A", "B", 5.5);
		data.addData("A", "C", 5.5);
		
		data.addData("C", "A", 5);
		
		List<List<Object>> pivot = data.getData();
		System.out.println( pivot.toString());
		
		assertEquals( pivot.size(),3);

		List<Object> p0 = pivot.get(0);		
		assertEquals( p0.size(),3);
		assertEquals( p0.get(0), 5.5);
	}

	@Test
	public void testGetxAxis() {
		CountData data = new CountData();
		
		data.addData("C", "X", 1);
		
		data.addData("A", "X", 1);
		data.addData("A", "X", 1);
		data.addData("A", "Y", 1);
		data.addData("A", "Z", 1);
		
		data.addData("B", "X", 1);
		data.addData("B", "Y", 1);
		data.addData("B", "Y", 1);
		data.addData("B", "Z", 1);

		Collection<String> xAxis = data.getxAxis();
		
		assert( xAxis.contains("A"));
		
		System.out.println( "X " + xAxis.toString());
	}

	@Test
	public void testGetyAxis() {
		CountData data = new CountData();
		data.addData("A", "X", 1);
		data.addData("A", "X", 1);
		data.addData("A", "Y", 1);
		data.addData("A", "Z", 1);
		
		data.addData("B", "X", 1);
		data.addData("B", "Y", 1);
		data.addData("B", "Y", 1);
		data.addData("B", "Z", 1);
		
		data.addData("C", "X", 1);
		
		Collection<String> yAxis = data.getyAxis();
		
		assert( yAxis.contains("X"));
		
		System.out.println( "Y " + yAxis.toString());
	}

}
