package nz.devcentre.gravity.model.transfer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.Card;

public class PercentDataTest {

	@Test
	public void getDataTest() {
		Card card=new Card();
		card.addField("name", "dummy");
		card.addField("amount", 1000);
		card.setPhase("current");
		
		PercentData percentData=new PercentData();
		percentData.addData("name", "amount", card);

		assertTrue(percentData.getData().size()==1);
		
		Card card1=new Card();
		card.addField("name", "dummy1");
		card.addField("amount", 2000);
		card.setPhase("current");
		
		percentData.addData("name", "amount", card1);
		
		assertTrue(percentData.getData().size()==1);
	}
}
