package nz.devcentre.gravity.model.transfer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;

public class CountDataTest {

	@Test
	public void testGetData() {
		CountData data = new CountData();
		
		data.addData("B", "A", null);
		data.addData("B", "B", null);
		data.addData("B", "B", null);
		data.addData("B", "C", null);
		
		data.addData("A", "A", null);
		data.addData("A", "A", null);
		data.addData("A", "B", null);
		data.addData("A", "C", null);
		
		data.addData("C", "A", null);
		
		List<List<Object>> pivot = data.getData();
		assertEquals( pivot.size(),3);

		List<Object> p0 = pivot.get(0);		
		assertEquals( p0.size(), 3);
		assertEquals( p0.get(0), 2);
		System.out.println( pivot.toString());
	}

	@Test
	public void testGetxAxis() {
		CountData data = new CountData();
		
		data.addData("C", "X", null);
		
		data.addData("A", "X", null);
		data.addData("A", "X", null);
		data.addData("A", "Y", null);
		data.addData("A", "Z", null);
		
		data.addData("B", "X", null);
		data.addData("B", "Y", null);
		data.addData("B", "Y", null);
		data.addData("B", "Z", null);
		
		
		
		Collection<String> xAxis = data.getxAxis();
		
		assert( xAxis.contains("A"));
		
		System.out.println( "X " + xAxis.toString());
		
	}

	@Test
	public void testGetyAxis() {
		CountData data = new CountData();
		data.addData("A", "X", null);
		data.addData("A", "X", null);
		data.addData("A", "Y", null);
		data.addData("A", "Z", null);
		
		data.addData("B", "X", null);
		data.addData("B", "Y", null);
		data.addData("B", "Y", null);
		data.addData("B", "Z", null);
		
		data.addData("C", "X", null);
		
		Collection<String> yAxis = data.getyAxis();
		
		assert( yAxis.contains("X"));
		
		System.out.println( "Y " + yAxis.toString());
	}

}
