package nz.devcentre.gravity.model.transfer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.Card;

public class ListDataTest {
	
	@Test
	public void getListTest() {
		Card card=new Card();
		card.addField("name", "dummy");
		card.setPhase("current");
		ListData listData=new ListData();
		listData.addData("name", "phase", card);
		Map<String, List<Card>> map = listData.getList();
		List<Card> list = map.get("name:phase");
		
		assertTrue(list.size()==1);
		
		Card card2=new Card();
		card2.addField("name", "dummy2");
		card2.setPhase("current");
		listData.addData("name", "phase", card2);
		
		map = listData.getList();
		list = map.get("name:phase");
		
		assertTrue(list.size()==2);
		
		//send List object
		listData.addData("name", "phase", list);
		
		//Change this implementation changes
		assertNull(listData.getData());
	}
}
