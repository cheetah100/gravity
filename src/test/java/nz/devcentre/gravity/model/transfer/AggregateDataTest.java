package nz.devcentre.gravity.model.transfer;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AggregateDataTest {

	@Test
	public void getDataTest() {
		AggregateData aggeAggregateData=new AggregateData();
		
		aggeAggregateData.addData("name", "phase", 1000);
		
		assertFalse(aggeAggregateData.getData().isEmpty());
	}
}
