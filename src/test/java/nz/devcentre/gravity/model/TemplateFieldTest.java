package nz.devcentre.gravity.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TemplateFieldTest {

	@Test
	public void testSetOptionlist() {
		TemplateField tf = new TemplateField();
		tf.setOptionlist("test-value");
		
		String optionlist = tf.getOptionlist();
		assertEquals("test_value", optionlist);
	}
	
	@Test
	public void testSetOptionlistWhenNull() {
		TemplateField tf = new TemplateField();
		tf.setOptionlist(null);
		
		String optionlist = tf.getOptionlist();
		assertNull(optionlist);
	}

}
