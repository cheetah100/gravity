/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Operation;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "/test-controllers.xml" })
class EvaluationServiceTest {

	@Autowired
	private EvaluationService evaluationService;
	
	private Map<String, Object> cardFields;
	
	@Test
	public void testEvalProperty_stringEquality() {
		cardFields.put("fieldName1", "myString");
		assertTrue(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "myString")));
		assertFalse(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "myString1")));
	}
	
	@Test
	public void testEvalProperty_integerEquality() {
		cardFields.put("fieldName1", 2);
		assertTrue(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "2")));
		assertFalse(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "3")));
	}
	
	@Test
	public void testEvalProperty_boolEquality() {
		cardFields.put("fieldName1", true);
		assertTrue(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "true")));
		assertFalse(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "false")));
	}
	
	@Test
	public void testEvalProperty_dateEquality() throws ParseException {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = df.format(cal.getTime());
		
		cardFields.put("fieldName1", df.parse(df.format(cal.getTime())));
		assertTrue(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, dateString)));
		
		cardFields.put("fieldName1", df.parse(df.format(new Date(43413434L))));
		assertFalse(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, dateString)));
	}
	
	@Test
	public void testEvalProperty_expressionString() {
		cardFields.put("fieldName1", "value1");
		cardFields.put("fieldName2", "value1");
		assertTrue(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "#fieldName2")));
		cardFields.put("fieldName2", "value2");
		assertFalse(evaluationService.evalProperty(getNewCard().getFields(), new Condition("fieldName1", Operation.EQUALTO, "#fieldName2")));
	}

	private Card getNewCard() {
		cardFields = new HashMap<>();
		Card newCard = new Card();
		newCard.setFields(cardFields);
		return newCard;
	}

}
