/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.services;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.tools.CardTools;

public class CardToolsTest {
	
	@Test
	public void testOrderCardsLongs() {
		
		List<Card> cardList = new ArrayList<Card>();
		cardList.add(getTestCard("10","Ten",10l));
		cardList.add(getTestCard("5","Five",5l));
		cardList.add(getTestCard("20","Twenty",20l));
		cardList.add(getTestCard("8","Eight",8l));
		cardList.add(getTestCard("16","Sixteen",16l));
		cardList.add(getTestCard("3","Three",3l));
		
		List<Card> orderedCards = CardTools.orderCards(cardList, "order", false, null, null);
		
		for( Card card : cardList){
			System.out.println(card.getId());
		}
		
		assertEquals( cardList.get(0).getId(), "3");
		assertEquals( orderedCards.get(0).getId(), "3");
	}
	
	@Test
	public void testOrderCardsDoubles() {
		
		List<Card> cardList = new ArrayList<Card>();
		cardList.add(getTestCard("10","Ten",10d));
		cardList.add(getTestCard("5","Five",5d));
		cardList.add(getTestCard("20","Twenty",20d));
		cardList.add(getTestCard("8","Eight",8d));
		cardList.add(getTestCard("16","Sixteen",16d));
		cardList.add(getTestCard("3","Three",3d));
		
		List<Card> orderedCards = CardTools.orderCards(cardList, "order", false, null, null);
		
		for( Card card : cardList){
			System.out.println(card.getId());
		}
		
		assertEquals( cardList.get(0).getId(), "3");
		assertEquals( orderedCards.get(0).getId(), "3");
	}
	
	private Card getTestCard(String id, String name, Number order){
		Card card = new Card();
		card.setId(id);
		Map<String,Object> fields = new HashMap<String,Object>();
		fields.put("name", name);
		fields.put("order", order);
		card.setFields(fields);
		return card;
	}
}
