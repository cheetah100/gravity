/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.HttpCallService.AccessToken;
import nz.devcentre.gravity.tools.SAMLTokenTool;

@ExtendWith(MockitoExtension.class)
public class HttpCallToolTest {
	
	@Mock
	SecurityTool securityTool;
	
	@InjectMocks
	HttpCallService tool;
	
	@Mock
	private SAMLTokenTool samlTokentool;
	
	protected static final Logger logger = LoggerFactory.getLogger(HttpCallService.class);

	@Test
	public void testExecuteNoAuthGet() throws Exception {
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("type", "software");
		parameters.put("area", "security");
		
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("test", "true");
		
		String response = this.tool.execute(null, 
				parameters, 
				"https://www.cisco.com/c/en/us/products/[type]/index.html#[area]", 
				"GET", headers, null, 0, 0).getBody();
		
		Assertions.assertNotNull(response);
		logger.info(response);
	}
	
	@Test
	public void testExecuteGetException() throws Exception {
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("type", "software");
		parameters.put("area", "security");
		
		String response = this.tool.execute(null, 
				parameters, 
				"http://nosuchserver.com", 
				"GET", null, null, 0, 0).getBody();
		
		Assertions.assertNull(response);
	}
	
	@Test
	@Disabled
	public void testExecuteOAuthGet() throws Exception {
		
		when(this.securityTool.getSecureCredential("test_credential")).thenReturn(getTestCredential());
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("board1", "newsfeed");
		parameters.put("board2", "lakelevels");
		parameters.put("usImportFlag",true);
		
		String response=null;
		try {
		      response = this.tool.execute(null, 
				parameters, 
				"https://bmi-dev-alln.cisco.com/gravity/spring/board/[board1]", 
				"GET", null, "test_credential", 0, 0).getBody();
		      
		      response = this.tool.execute(null, 
				parameters, 
				"https://bmi-dev-alln.cisco.com/gravity/spring/board/[board2]", 
				"GET", null, "test_credential", 0, 0).getBody();

		} catch(IOException e) {
			 logger.info("IO Exception");
			 e.printStackTrace();
		 }
		
		Assertions.assertNotNull(response);
		logger.info(response);
	}
	
	
	@Test
	@Disabled
	public void testExecuteOAuthGetBasic() throws Exception {
		
		Credential testCredential = getTestCredential();
		testCredential.setMethod("oauth_basic");
		testCredential.setRequestMethod("POST");
		when(this.securityTool.getSecureCredential("test_oauthbasic_credential")).thenReturn(testCredential);
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("board1", "newsfeed");
		parameters.put("board2", "lakelevels");
		parameters.put("usImportFlag",true);
		
		String response = this.tool.execute(null, 
				parameters, 
				"https://bmi-dev-alln.cisco.com/gravity/spring/board/[board1]", 
				"GET", null, "test_oauthbasic_credential", 0, 0).getBody();
		
		Assertions.assertNotNull(response);
		logger.info(response);

	}
	
	@Test
	public void testExecuteOAuthGetJwt() throws Exception {
		
		Credential testCredential = getTestCredential();
		testCredential.setMethod("oauth_jwt");
		testCredential.getResponseFields().put("jwtExpireTime", "5000");
		testCredential.getResponseFields().put("jwtSubject", "test");
		testCredential.getResponseFields().put("jwtName", "test");
		testCredential.getResponseFields().put("accessTokenField", "my_access_token");
		testCredential.getResponseFields().put("expirationField", "my_expiry");
		when(this.securityTool.getSecureCredential("test_oauthjwt_credential")).thenReturn(testCredential);
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("board1", "newsfeed");
		parameters.put("board2", "lakelevels");
		parameters.put("usImportFlag",true);
		
		Assertions.assertThrows(Exception.class, () -> {		
			String response = this.tool.execute(null, 
					parameters, 
					"https://bmi-dev-alln.cisco.com/gravity/spring/board/[board1]", 
					"GET", null, "test_oauthjwt_credential", 0, 0).getBody();
			Assertions.assertNotNull(response);
			logger.info(response);		
		}, "Exception was expected");
		
	}
	
	@Test
	public void testExecuteBasicAuthGet() throws Exception {
		
		when(this.securityTool.getSecureCredential("test_credential")).thenReturn(getBasicTestCredential());
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String response = this.tool.execute(null, 
				parameters, 
				"https://jigsaw.w3.org/HTTP/Basic", 
				"GET", null, "test_credential", 0, 0).getBody();
		
		Assertions.assertNotNull(response);
		
		Map<String, AccessToken> tokens = this.tool.getTokens();
		Assertions.assertNotNull(tokens);
		AccessToken token = tokens.get("test_credential");
		Assertions.assertNotNull( token );
	}
	
	@Test
	public void testExecuteBadBasicAuthGet() throws Exception {
		
		when(this.securityTool.getSecureCredential("test_credential")).thenReturn(getBadTestCredential());
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String response = this.tool.execute(null, 
				parameters, 
				"https://jigsaw.w3.org/HTTP/Basic", 
				"GET", null, "test_credential", 0, 0).getBody();
		
		Assertions.assertNull(response);		
	}

	@Test
	@Disabled
	public void testGetSsoTokenWithSecretAuth() throws Exception {
		Credential credential = getTestCredential();
		String ssoToken = this.tool.getSsoToken(credential);
		Assertions.assertNotNull(ssoToken);
	}

	@Test
	public void testSsoTokenNoCredential() throws Exception {
		Credential credential = getTestCredential();
		credential.setMethod(null);
		
		Assertions.assertThrows(InvalidParameterException.class, () -> {
			this.tool.ssoLogon(credential);
		}, "InvalidParameterException was expected");
	}
	
	@Test
	public void testGetSsoTokenWithBasicCreds() throws Exception {
		Credential credential = getBasicTestCredential();
		String ssoToken = this.tool.getSsoToken(credential);
		System.out.println(ssoToken);
		Assertions.assertNotNull(ssoToken);
		Assertions.assertEquals("Basic Z3Vlc3Q6Z3Vlc3Q=", ssoToken);
	}

	@Test
	public void testStoreAndGetCookie() {
		this.tool.storeCookie("test", "cookie");
		String cookie = this.tool.getCookie("test");
		assertEquals( cookie, "cookie");
	}
	
	@Test
	public void testReleaseToken() throws Exception {
		
		when(this.securityTool.getSecureCredential("test_credential")).thenReturn(getBasicTestCredential());
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String response = this.tool.execute(null, 
				parameters, 
				"https://jigsaw.w3.org/HTTP/Basic", 
				"GET", null, "test_credential", 0, 0).getBody();
		
		Map<String, AccessToken> tokens = this.tool.getTokens();
		Assertions.assertNotNull(tokens);
				
		AccessToken token = tokens.get("test_credential");
		Assertions.assertNotNull( token );
		
		this.tool.releaseToken("test_credential");
		
		AccessToken token2 = tokens.get("test_credential");
		Assertions.assertNull( token2 );
	}
	
	@Test
	public void testExecuteOAuthGetSaml() throws Exception {
		
		Credential testCredential = getTestCredential();
		testCredential.setMethod("oauth_saml");
		testCredential.setRequestMethod("GET");
		when(this.securityTool.getSecureCredential("test_oauthsaml_credential")).thenReturn(testCredential);
		when(samlTokentool.getSamlToken(testCredential)).thenReturn("testToken");
		Map<String,Object> parameters = new HashMap<String,Object>();
	
		Assertions.assertThrows(Exception.class, () -> {
			String response = this.tool.execute(null, 
					parameters, 
					"https://bmi-dev-alln.cisco.com/gravity/spring/board/[board1]", 
					"GET", null, "test_oauthsaml_credential", 0, 0).getBody();
			
			Assertions.assertNotNull(response);
			logger.info(response);
		}, "Exception was expected");

	}
	
	private Credential getTestCredential() {
		Credential credential = new Credential();
		credential.setId("test_credential");
		credential.setMethod("oauth");
		credential.setIdentifier("");
		credential.setSecret("");
		credential.setResource("");

		return credential;
	}
	
	private Credential getBasicTestCredential() {
		Credential credential = new Credential();
		credential.setId("test_credential");
		credential.setMethod("basic");
		credential.setIdentifier("guest");
		credential.setSecret("guest");
		credential.setResource("");
		return credential;
	}

	private Credential getBadTestCredential() {
		Credential credential = new Credential();
		credential.setId("test_credential");
		credential.setMethod("basic");
		credential.setIdentifier("badguy");
		credential.setSecret("noaccess");
		credential.setResource("");
		return credential;
	}
}
