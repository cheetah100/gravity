/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.controllers.CardTaskController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.repository.CardTaskRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.ComplexDateConverter;
import nz.devcentre.gravity.tools.DateInterpreter;
import nz.devcentre.gravity.tools.NowDateConverter;
import nz.devcentre.gravity.tools.PlusDateConverter;
import nz.devcentre.gravity.tools.SubtractDateConverter;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "/test-controllers.xml" })
public class CardServiceTest {
	
	private String TEST_CATEGORY = "test_category";
	
	private CardService cardService;
	
	@Autowired
	private TestBoardTool testBoardTool;
	
	@Autowired
	private CardTaskController cardTaskController;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Mock
	CardTaskRepository cardTaskRepository;

	private static Card card;
	
	public static Boolean initialized = false;
	
	@BeforeEach
	public void setUp() throws Exception {
		
		securityTool.iAmSystem();
		
		List<ComplexDateConverter> complexDateConverters = new ArrayList<ComplexDateConverter>();
		complexDateConverters.add(new PlusDateConverter());
		complexDateConverters.add(new SubtractDateConverter());
		complexDateConverters.add(new NowDateConverter());
		
		DateInterpreter di = new DateInterpreter();
		di.setComplexDateConverters(complexDateConverters);
		
		// TODO Resolve This
		//cardService.setDateInterpreter(di);
		//cardService.setVariableInterpreter(vi);
		
		synchronized(initialized) {
			if (!initialized) {
				initialized = true;
				// Do this only once as all test instances will be sharing the board, cards, rules etc.
				testBoardTool.initTestBoard();
				testBoardTool.initTestUpdBoard();
				testBoardTool.initTestOptionlistBoard();
				card = testBoardTool.getTestCard();
				//testTool.generateRule();
			}
		}
	}

	@Test
	public void testCreateCard() throws Exception {
		
		Map<String,Object> fields = new HashMap<>();
		fields.put("name","Test Card");
		Card card = new Card();
		card.setFields(fields);
		Card createdCard = cardService.createCard(TestBoardTool.BOARD_ID_UPD, card, false);
		
		assertNotNull(createdCard);
		assertNotNull(createdCard.getId());
	}
	
	@Test
	public void testCreateOptionlistCard() throws Exception {
		
		Map<String,Object> fields = new HashMap<>();
		fields.put("name","Test Card");
		Card card = new Card();
		card.setFields(fields);
		Card createdCard = cardService.createCard(TestBoardTool.BOARD_ID_OPTIONLIST, card, false);
		
		assertNotNull(createdCard);
		assertNotNull(createdCard.getId());
		assertEquals("test_card", createdCard.getId());
	}

	@Test
	public void testOrderCardsLongs() {
		
		List<Card> cardList = new ArrayList<Card>();
		cardList.add(getTestCard("10","Ten",10l));
		cardList.add(getTestCard("5","Five",5l));
		cardList.add(getTestCard("20","Twenty",20l));
		cardList.add(getTestCard("8","Eight",8l));
		cardList.add(getTestCard("16","Sixteen",16l));
		cardList.add(getTestCard("3","Three",3l));
		
		List<Card> orderedCards = cardService.orderCards(cardList, "order", false, null, null);
		
		for( Card card : cardList){
			System.out.println(card.getId());
		}
		
		assertEquals( cardList.get(0).getId(), "3");
		assertEquals( orderedCards.get(0).getId(), "3");
	}
	
	@Test
	public void testOrderCardsDoubles() {
		
		List<Card> cardList = new ArrayList<Card>();
		cardList.add(getTestCard("10","Ten",10d));
		cardList.add(getTestCard("5","Five",5d));
		cardList.add(getTestCard("20","Twenty",20d));
		cardList.add(getTestCard("8","Eight",8d));
		cardList.add(getTestCard("16","Sixteen",16d));
		cardList.add(getTestCard("3","Three",3d));
		
		List<Card> orderedCards = cardService.orderCards(cardList, "order", false, null, null);
		
		for( Card card : cardList){
			System.out.println(card.getId());
		}
		
		assertEquals( cardList.get(0).getId(), "3");
		assertEquals( orderedCards.get(0).getId(), "3");
	}
	
	
	@Test
	public void testGetCardBoardAndCardID() throws Exception{
		Card card = cardService.getCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID);
		assertNotNull(card);
	}
	
	@Test
	public void testGetCardBoardCardIDAndView() throws Exception{
		Card card = cardService.getCard(TestBoardTool.BOARD_ID, TestBoardTool.CARD_ID, "testview");
		assertNotNull(card);
		assertEquals("Tim", card.getFields().get("name"));
	}
	
	protected CardTask addTask(String taskId) throws Exception {
		CardTask task = new CardTask();
		task.setComplete(false);
		task.setTaskid(taskId);
		task.setCategory(TEST_CATEGORY);
		task.setDetail("Test Task");
		task.setUser("system");
		task.setBoard(card.getBoard());
		task.setCard(card.getId());
		
		cardTaskRepository.save(task);
		
		Collection<CardTask> tasks = cardTaskController.getTasks(TestBoardTool.BOARD_ID, card.getId());
		assertNotNull(tasks);
		assertEquals(tasks.size(),1);
		
		return tasks.iterator().next();
		
	}
	
	@Test
	@Disabled
	public void testResolveReferenceField() {
		//String resolveReferenceField = this.tools.resolveReferenceField(new Template(), "test_field", null);
		//assertEquals("", resolveReferenceField);
		TemplateField field = new TemplateField();
		field.setName("test_owner");
		field.setType(FieldType.STRING);
		field.setOptionlist("owner_board");
		TemplateField field2 = new TemplateField();
		field2.setName("test_owner1");
		field.setType(FieldType.STRING);
		Map<String,TemplateField> fields = new HashMap<String, TemplateField>();
		fields.put("test_owner", field);
		fields.put("test_owner1", field2);
		//TemplateGroup group = new TemplateGroup();
		//group.setFields(fields);
		//group.setIndex(1);
		//Map<String, TemplateGroup> groups = new HashMap<String,TemplateGroup>();
		//groups.put("a", group);
		//Template template = new Template();
		//template.setGroups(groups);
		//resolveReferenceField = this.tools.resolveReferenceField(template, "test_owner1", "007");
		//assertEquals("", resolveReferenceField);
		//resolveReferenceField = this.tools.resolveReferenceField(template, "test_owner", "007");
		//assertEquals("", resolveReferenceField);
		//Card card=new Card();
		
		//CardTools mockCardTool=Mockito.mock(CardTools.class);
		//Mockito.when(mockCardTool.getCard("owner_board", "007")).thenReturn(card);
		//Mockito.when(mockCardTool.resolveReferenceField(template, "test_owner", "007")).thenCallRealMethod();
		//resolveReferenceField = mockCardTool.resolveReferenceField(template, "test_owner", "007");
		//assertEquals("James Bond", resolveReferenceField);
		
	}
	
	@Test
	public void testReferenceCheck() throws Exception {
		CardService mockCardService = Mockito.mock(CardService.class);
		
		String referenceBoard="owner_board";
		TemplateField field = new TemplateField();
		field.setName("test_owner");
		field.setType(FieldType.LIST);
		field.setOptionlist(referenceBoard);
		
		Card card1=new Card();
		card1.setBoard(referenceBoard);
		card1.setId("1001");

		Card card2=new Card();
		card2.setBoard(referenceBoard);
		card2.setId("1002");
		
		Mockito.when(mockCardService.referenceCheck(field, null)).thenCallRealMethod();
		String referenceCheck = mockCardService.referenceCheck(field, null);
		assertNull(referenceCheck);

		
		Mockito.when(mockCardService.getCard(referenceBoard, card1.getId())).thenReturn(card1);
		Mockito.when(mockCardService.getCard(referenceBoard, card2.getId())).thenReturn(card2);
		
		String[] validArray=new String[] {"1001","1002"};
		String[] inValidArray=new String[] {"1001","1002","1003"};
		List<String> fieldValue=Arrays.asList(validArray);
		
		Mockito.when(mockCardService.referenceCheck(field, fieldValue)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, fieldValue);
		assertNull(referenceCheck);
		
		fieldValue=Arrays.asList(inValidArray);
		Mockito.when(mockCardService.referenceCheck(field, fieldValue)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, fieldValue);
		assertEquals("Reference Error on Field List: test_owner: Id 1003 does not exist on board owner_board", referenceCheck);
		
		Mockito.when(mockCardService.referenceCheck(field, validArray)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, validArray);
		assertNull(referenceCheck);
		
		
		Mockito.when(mockCardService.referenceCheck(field, inValidArray)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, inValidArray);
		assertEquals("Reference Error on Field Array: test_owner: Id 1003 does not exist on board owner_board", referenceCheck);

		Mockito.when(mockCardService.referenceCheck(field, "1003")).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, "1003");
		assertEquals("Reference Error on Field: test_owner: Id 1003 does not exist on board owner_board", referenceCheck);
		
		Mockito.when(mockCardService.referenceCheck(field, "1002")).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, "1002");
		assertNull(referenceCheck);
		
		Object arr1[]=new Object[] {"1002"};
		Object arr2[]=new Object[] {"1003"};
		
		Mockito.when(mockCardService.referenceCheck(field, arr1)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, arr1);
		assertNull(referenceCheck);
		
		Mockito.when(mockCardService.referenceCheck(field, arr2)).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, arr2);
		assertEquals("Reference Error on Field Array: test_owner: Id 1003 does not exist on board owner_board", referenceCheck);
		
		field.setOptionlist("alien");
		Mockito.when(mockCardService.referenceCheck(field, "alien_id")).thenCallRealMethod();
		referenceCheck = mockCardService.referenceCheck(field, "alien_id");
		assertEquals("Reference Error on Field: test_owner: Id alien_id does not exist on board alien", referenceCheck);
		
	}
	
	@Test
	@Disabled 
	/*
	 * This test is mixing mocks with Spring.
	 * Needs investigation
	 */
	public void testValidateCardReferenceFields() throws Exception {
		
		String referenceBoard="owner_board";
		String newBoard="new_board";
		
		TemplateField field = new TemplateField();
		field.setName("test_owner");
		field.setType(FieldType.STRING);
		field.setOptionlist(referenceBoard);
		field.setRequired(true);
		
		Map<String,TemplateField> fields = new HashMap<String, TemplateField>();
		fields.put("test_owner", field);
		
		Card card1=new Card();
		Map<String,Object> cardField = new HashMap<String, Object>();
		cardField.put("test_owner","1003");
		card1.setBoard(referenceBoard);
		card1.setId("1001");
		card1.setBoard(newBoard);
		card1.setFields(cardField);
		
		Mockito.when(cardService.getCard(referenceBoard, card1.getId())).thenReturn(card1);
		Mockito.when(cardService.referenceCheck(field, "1003")).thenCallRealMethod();
		Mockito.when(cardService.validateCardReferenceFields("new_board", card1, false)).thenCallRealMethod();
		
		Map<String, Object> messages = cardService.validateCardReferenceFields("new_board", card1, false);
		assertEquals("Reference Error on Field: test_owner: Id 1003 does not exist on board owner_board", messages.get("test_owner"));
	
		Mockito.when(cardService.validateCardReferenceFields("new_board", card1, true)).thenCallRealMethod();
		
		messages = cardService.validateCardReferenceFields("new_board", card1, true);
		assertEquals("Reference Error on Field: test_owner: Id 1003 does not exist on board owner_board", messages.get("test_owner"));
		assertFalse(card1.getFields().containsKey("test_owner"));
		
		Mockito.when(cardService.validateCardRequiredFields(newBoard, card1)).thenCallRealMethod();
		
		messages = cardService.validateCardRequiredFields(newBoard, card1);
		assertEquals(messages.get("test_owner"),  "Field is required: test_owner");
		
		cardField=new HashMap<String, Object>();
		cardField.put("test_owner","");
		card1.setFields(cardField);
		messages = cardService.validateCardRequiredFields(newBoard, card1);
		assertEquals(messages.get("test_owner"), "Field is required. Cannot be blank or null: test_owner");
	}

	@Test
	public void testValidateCardUniqueFields() throws Exception {
		
		Card createCard1 = cardService.createCard(TestBoardTool.BOARD_ID, TestBoardTool.getTestCard("James", TestBoardTool.BOARD_ID, 1000),false);
		cardService.createCard(TestBoardTool.BOARD_ID, TestBoardTool.getTestCard("Bond", TestBoardTool.BOARD_ID, 1000),false);
		Map<String,Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put("balance",1000);
		Map<String, Object> messages = cardService.validateCardUniqueFields(TestBoardTool.BOARD_ID, fieldValues, createCard1.getId());
		assertTrue(messages.containsKey("Validation"));
		assertEquals("{balance=1000} Combination Already Exists", messages.get("Validation"));
		fieldValues.clear();
		fieldValues.put("unknown",1000);
		messages = cardService.validateCardUniqueFields(TestBoardTool.BOARD_ID, fieldValues, createCard1.getId());
		assertTrue(messages.isEmpty());
	}
	
	@Test
	public void testGetCardFieldChanges() throws Exception {
		Map<String, Object> before = getFields();		
		Map<String, Object> updated = getFields();		
		Map<String, CardEventField> cardFieldChanges = CardTools.getCardFieldChanges(before, updated);
		assertEquals( 0, cardFieldChanges.size());
		
		updated.put("new", "mynewvalue");
		cardFieldChanges = CardTools.getCardFieldChanges(before, updated);
		assertEquals( 1, cardFieldChanges.size());
		
		CardEventField cardEventField = cardFieldChanges.get("new");
		assertNotNull(cardEventField);
		assertEquals( "mynewvalue", cardEventField.getAfter());
		assertEquals( "", cardEventField.getBefore());
	}
	
	@Test
	public void testGetCardFieldChangesWithMaps() throws Exception {
		Map<String, Object> before = getFields();
		
		Map<String, Object> updated = getFields();
		Map<String, Object> map = getFields();
		map.put("newthing", "newvalue2");
		updated.put( "new", map);
		
		Map<String, CardEventField> cardFieldChanges = CardTools.getCardFieldChanges(before, updated);
		assertEquals( 1, cardFieldChanges.size());		
		
		map = getFields();
		map.put("newthing", "newvalue1");
		updated.put( "new", map);

		cardFieldChanges = CardTools.getCardFieldChanges(before, updated);
		assertEquals( 1, cardFieldChanges.size());		

		map = getFields();
		map.put("newthing", "newvalue2");
		updated.put( "new", map);
		 
		Map<String, Object> map2 = getFields();
		map2.put("newthing", "newvalue2");
		before.put( "new", map2);
		
		cardFieldChanges = CardTools.getCardFieldChanges(before, updated);
		assertEquals( 0, cardFieldChanges.size());		
	}
	
	private Map<String,Object> getFields() {
		Map<String, Object> newMap = new HashMap<String,Object>();
		newMap.put("f1", "v1");
		newMap.put("f2", "v2");
		newMap.put("f3", "v3");	
		return newMap;
	}

	private Card getTestCard(String id, String name, Number order){
		Card card = new Card();
		card.setId(id);
		Map<String,Object> fields = new HashMap<String,Object>();
		fields.put("name", name);
		fields.put("order", order);
		card.setFields(fields);
		return card;
	}

	@Test
	public void testResolveValues() {
		Map<String, String> fields = new HashMap<String,String>();
		fields.put("name","#surname");
		fields.put("type","employee");
		fields.put("gender","#sex");
		
		Map<String, Object> context = new HashMap<String,Object>();
		context.put("surname","harrison");
		context.put("dont","includeme");
		context.put("sex","female");
					
		Map<String, Object> resolveValues = CardService.resolveValues(fields, context);
		assertEquals(resolveValues.size(),3);
		assertEquals(resolveValues.get("name"),"harrison");
		assertEquals(resolveValues.get("type"),"employee");
		assertEquals(resolveValues.get("gender"),"female");
		assertNull(resolveValues.get("dont"));
	}

	@Test
	public void testGetValueByReference() {
		
		Map<String,Object> l1 = generateMapWithObject( "name", "joe");
		l1.put("gender", "female");
		Map<String,Object> l2 = generateMapWithObject( "l1", l1);
		Map<String,Object> l3 = generateMapWithObject( "l2", l2);
		Map<String,Object> fin = generateMapWithObject( "l3", l3);
		
		Object name = CardService.getValueByReference("l3.l2.l1.name", fin);
		assertEquals( name, "joe");
		
		Object gender = CardService.getValueByReference("l3.l2.l1.gender", fin);
		assertEquals( gender, "female");
		
		Object newl2 = CardService.getValueByReference("l3.l2", fin);
		assertEquals( newl2, l2);
		
	}
	
	private Map<String,Object> generateMapWithObject( String name, Object obj){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put(name, obj);
		return map;
	}
	
}
