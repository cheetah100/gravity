/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.services;

import static org.mockito.Mockito.when;

import javax.mail.internet.MimeMessage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import nz.devcentre.gravity.config.BeanConfigurtion;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {

	@InjectMocks
	EmailService emailSenderAction;
	
	JavaMailSenderImpl mailSender;
	
	@Mock
	MimeMessage mailMessage;
	
	@Test
	public void testSendEmail() throws Exception{
		
		when( mailSender.createMimeMessage()).thenReturn(this.mailMessage);
		
		emailSenderAction.sendEmail("Unit Test Message", 
				"<html><body><h1>Unit Test Email</h1></body></html>", 
				"gsdora@cisco.com", 
				"gsdora@cisco.com",
				"test@cisco.com",
				"test@cisco.com",
				 true, null, null);	
	}
	
	@Test
	public void testReal() throws Exception {
		
		BeanConfigurtion config = new BeanConfigurtion();
		config.setHostname("mail.networksavvy.org");
		config.setPort(587);
		config.setSmtpStartTlsEnable(true);
		config.setUsername("security@devcentre.nz");
		config.setPassword("56%*fgRTY");
		
		EmailService service = new EmailService( config.createJavaMailSenderImpl() );
		service.sendEmail("Test Email", "Test Body", "cheetah100@gmail.com", 
				null, "security@devcentre.nz", "security@devcentre.nz" , false, null, null);

	}

}
