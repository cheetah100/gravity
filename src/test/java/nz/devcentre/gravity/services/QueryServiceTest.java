/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;

import nz.devcentre.gravity.config.FongoConfiguration;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Option;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
public class QueryServiceTest {
	
	@Autowired
	private QueryService queryService;
	
	@Autowired
	private CardController cardController;
	
	@Autowired
	private TestBoardTool tool;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private SecurityTool securityTool;
		
    @Configuration
    @Import({FongoConfiguration.class})
    @ImportResource("classpath:/test-controllers.xml")
    public static class ContextConfig {
       
    }             
	
	@BeforeEach
	public void setUp() throws Exception {
		Map<String,String> teams = new HashMap<String,String>();
		teams.put("test_team", "Test Team");
		
		Map<String,String> members = new HashMap<String,String>();
		members.put("system", "");
		
		Team team = new Team();
		team.setId("test_team");
		team.setMembers(members);
		
		tool.initTestBoard();
		tool.generateFilters();
		
		securityTool.iAmSystem();
	}

	@Test
	public void testDynamicQuery() throws Exception {
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", "deepak");
		fields.put("city", "pune");
		
		Card card = new Card();
		card.setFields(fields);
		card.setPhase(TestBoardTool.PHASE_ID);
		card.setCreator("admin");
		card.setCreated(new Date());
		card.setModifiedby("admin");
		card.setModified(new Date());	
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);

		Board board = boardsCache.getItem(TestBoardTool.BOARD_ID);
		
		Filter testFilter = this.tool.getTestFilter("test_filter", null, "metadata.creator", "system");
		
		Collection<Card> search = queryService.dynamicQuery(TestBoardTool.BOARD_ID, 
				TestBoardTool.PHASE_ID, 
				testFilter,
				board.getView("testview"), 
				false);
		
		assertNotNull(search);
		assertTrue(search.size() > 0);
	}
	
	@Test
	public void testQueryStringStringStringBooleanObjectContentManager() throws Exception {
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", "deepak");
		fields.put("city", "pune");
		
		Card card = new Card();
		card.setFields(fields);
		card.setPhase(TestBoardTool.PHASE_ID);
		card.setCreator("admin");
		card.setCreated(new Date());
		card.setModifiedby("admin");
		card.setModified(new Date());		
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);
		
		Collection<Card> search = queryService.query(TestBoardTool.BOARD_ID, TestBoardTool.PHASE_ID, "test_filter", "testview", false);
		assertNotNull(search);
		assertTrue(search.size() > 0);
	}

	/*
	@Test
	public void testPhaseCondition() throws Exception {
		
		Filter phasefilter = this.tool.getTestFilter("test_phase_filter", "test_phase|next_phase", "name", "Bob");
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", "Bob");
		
		Card card = new Card();
		card.setFields(fields);
		card.setPhase(TestBoardTool.PHASE_ID);
		card.setCreator("system");
		card.setCreated(new Date());
		card.setModifiedby("system");
		card.setModified(new Date());		
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);
		
		card = new Card();
		card.setFields(fields);
		card.setPhase("next_phase");
		card.setCreator("system");
		card.setCreated(new Date());
		card.setModifiedby("system");
		card.setModified(new Date());		
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);
		
		card = new Card();
		card.setFields(fields);
		card.setPhase("archive");
		card.setCreator("system");
		card.setCreated(new Date());
		card.setModifiedby("system");
		card.setModified(new Date());		
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);
		
		Board board = boardsCache.getItem(TestBoardTool.BOARD_ID);
		Condition cond = queryService.buildPhaseCondition(board, null, false, board.getFilter("test_phase_filter"));
		System.out.println(cond);
		Map<String, String> basicQuery = queryService.basicQuery(TestBoardTool.BOARD_ID, null, board.getFilter("test_phase_filter"), "_id", false);
		
		System.out.println("T SIZE: " + basicQuery.size());
		assertNotNull(basicQuery);
		assertTrue((basicQuery.size() == 1));
		
		basicQuery = queryService.basicQuery(TestBoardTool.BOARD_ID, null, board.getFilter("next_phase_filter"), "_id", false);
		
		System.out.println("N SIZE: " + basicQuery.size());
		assertNotNull(basicQuery);
		//assertTrue(basicQuery.size() == 1);
		
		basicQuery = queryService.basicQuery(TestBoardTool.BOARD_ID, null, board.getFilter("test_phase_next_phase_filter"), "_id", false);
		
		System.out.println("TN SIZE: " + basicQuery.size());
		assertNotNull(basicQuery);
		//assertTrue(basicQuery.size() == 2);
	}
	*/

	/**
	 * This method is only used in notifications, and is questionable.
	 */
	@Test
	@Disabled
	public void testRetrieveObjects() {
		
	}

	/**
	 * This test depends on the timezone, thus tests for a range.
	 * @throws Exception
	 */
	@Test
	public void testDecodeShortDate() throws Exception {
		Date decodeShortDate = queryService.decodeShortDate("01-01-1970");
		assertNotNull(decodeShortDate);
		assertTrue(decodeShortDate.getTime() <= 43200000);
		assertTrue(decodeShortDate.getTime() >= -43200000);
	}

	@Test
	public void testSearch() throws Exception {
		
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("name", "smith");
		
		Card card = new Card();
		card.setFields(fields);
		card.setPhase(TestBoardTool.PHASE_ID);
		card.setCreator("system");
		card.setCreated(new Date());
		card.setModifiedby("system");
		card.setModified(new Date());		
		
		cardController.createCard(TestBoardTool.BOARD_ID, card,false);
		
		Map<String, String> search = queryService.search(TestBoardTool.BOARD_ID, "creator", Operation.EQUALTO, "system", "path", false);
		assertNotNull(search);
	}

	@Test
	public void testOrder() throws Exception {
		
		List<Option> list = new ArrayList<Option>();
		list.add(getOption("a",3));
		list.add(getOption("b",5));
		list.add(getOption("c",2));
		list.add(getOption("d",1));
		
		List<Option> order = QueryService.order(list);
		int max = 0;
		for( Option option : order){
			if(option.getOrder()<max){
				fail("Out of Order");
			} else {
				max = option.getOrder();
			}
		}
	}
	
	private Option getOption(String name, int order) throws Exception{
		Option option = new Option();
		option.setId(name);
		option.setName(name);
		option.setOrder(order);
		return option;
	}
	
	/*
	private Filter getTestFilter() throws Exception{
		Filter filter = new Filter();
		filter.setName("test_filter");
		filter.setId("test_filter");
		
		Condition condition = new Condition("metadata.creator",Operation.EQUALTO,"system");
		condition.setConditionType(null);
		
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		conditions.put("test_condition1", condition);
		filter.setConditions(conditions);
		
		return filter;
	}
	*/
	

}