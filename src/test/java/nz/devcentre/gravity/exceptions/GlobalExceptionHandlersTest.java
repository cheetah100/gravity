package nz.devcentre.gravity.exceptions;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;

import nz.devcentre.gravity.managers.ExceptionManager;

@ExtendWith(MockitoExtension.class)
public class GlobalExceptionHandlersTest {
	
	@Mock
	ExceptionManager exceptionManager;

	@InjectMocks
	GlobalExceptionHandlers handler = new GlobalExceptionHandlers();

	private HttpServletRequest request = mock(HttpServletRequest.class);

	@BeforeEach
	public void init() {
		when(request.getRemoteUser()).thenReturn("gsdora");
		when(request.getRequestURL()).thenReturn(new StringBuffer("testUrl"));
	}

	@Test
	public void testHandleDataValidationException() {				
		Map<String,Object> messages = new HashMap<>();
		messages.put("test-message", "Test Message");
		CardValidationException cardValidationException = new CardValidationException(messages);
		Object exception = handler.handleDataValidationException(request, cardValidationException);

		assertTrue( exception instanceof Map);
		Map<String,Object> output = (Map<String,Object>)exception;
		assertTrue(output.containsKey("test-message"));
	}

	@Test
	public void testHandleResourceNotFoundException() {
		ResourceNotFoundException exception = new ResourceNotFoundException("Test Exception"); 
		Object output = handler.handleResourceNotFoundException(request, exception);		
		assertEquals( "Resource Not Found", output);
	}

	@Test
	public void testHandleAutomationExecutionException() {
		AutomationExecutionException exception = new AutomationExecutionException("Test Exception");
		Object output = handler.handleAutomationExecutionException(request, exception);		
		assertEquals( "Automation Execution Exception: Test Exception", output);
	}
	
	@Test
	public void testHandleAutomationExecutionExceptionWithNull() {
		AutomationExecutionException exception = new AutomationExecutionException("Test Exception");
		Object output = handler.handleAutomationExecutionException(null, exception);		
		assertEquals( "Automation Execution Exception: Test Exception", output);
	}

	@Test
	public void testHandleAccessDeniedException() {
		AccessDeniedException exception = new AccessDeniedException("Test Exception"); 
		Object output = handler.handleAccessDeniedException(request, exception);	
		assertEquals( "Access Denied", output);
		verify(exceptionManager, times(1)).raiseException(anyString(), anyString(), anyString(), anyBoolean());
	}

	@Test
	public void testHandleHttpMessageNotReadableException() {
		HttpMessageNotReadableException exception = new HttpMessageNotReadableException("Test Exception");
		Object output = handler.handleHttpMessageNotReadableException(request, exception);
		assertEquals( "Validation Error", output);
	}

	@Test
	public void testHandleException() {
		Exception exception = new Exception("Test Exception");
		Object output = handler.handleException(request, exception);
		assertEquals( "Server Error", output);
	}

}
