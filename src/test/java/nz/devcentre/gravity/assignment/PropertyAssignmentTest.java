/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.assignment;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;


import nz.devcentre.gravity.model.Card;

public class PropertyAssignmentTest {
	
	private PropertyAssignment pa = new PropertyAssignment();

	@Test
	public void testGetName() {
		String name = pa.getName();
		Assertions.assertEquals("PROPERTY", name);
	}

	@Test
	public void testGetAssignment() throws Exception {
		Map<String,Object> fields = new HashMap<>();
		fields.put("user", "smith");
		Card card = new Card();
		card.setFields(fields);
		String assignment = pa.getAssignment("user", card);
		Assertions.assertEquals("smith", assignment);
	}

}
