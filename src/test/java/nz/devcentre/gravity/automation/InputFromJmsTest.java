/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.ExceptionManager;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@ExtendWith(MockitoExtension.class)
public class InputFromJmsTest {
	
	@InjectMocks
	InputFromJms inputFromJms;

	@Mock
	Message message;
	
	@Mock
	MessageProperties messageProperties;
	
	@Mock
	ExceptionManager exceptionManager;
	
	@Mock
	CardService cardService;
	
	@Mock
	SecurityTool securityTool;
	
	@Test
	public void testOnMessage() throws Exception {
		
		Card card = new Card();
		card.setBoard("test-board");
		card.setId("test");
		
		String body = "{ \"board\":\"test-board\", \"fields\": { \"field\":\"value\" } }";
		Map<String,Object> values = new HashMap<String,Object>();
		values.put("userid", "admin");
		
		byte[] bodyArray = body.getBytes();
		when(message.getBody()).thenReturn(bodyArray);
		when(message.getMessageProperties()).thenReturn(messageProperties);
		when(messageProperties.getHeaders()).thenReturn(values);
		when(cardService.createCard(eq("test-board"), any(),eq(false))).thenReturn(card);
		
		inputFromJms.onMessage(message);
		
		verify(securityTool, times(1)).iAm("admin");
		verify(cardService, times(1) ).createCard(eq("test-board"),any(),eq(false));
	}

	@Test
	public void testAnonymousOnMessage() throws Exception {
		
		Card card = new Card();
		card.setBoard("test-board");
		card.setId("test");
		
		String body = "{ \"board\":\"test-board\", \"fields\": { \"field\":\"value\" } }";
		Map<String,Object> values = new HashMap<String,Object>();
		
		byte[] bodyArray = body.getBytes();
		when(message.getBody()).thenReturn(bodyArray);
		when(message.getMessageProperties()).thenReturn(messageProperties);
		when(messageProperties.getHeaders()).thenReturn(values);
		when(cardService.createCard(eq("test-board"), any(),eq(false))).thenReturn(card);
		
		inputFromJms.onMessage(message);
		
		verify(securityTool, times(1)).iAmSystem();
		verify(cardService, times(1) ).createCard(eq("test-board"),any(),eq(false));
	}

	
	
	@Test
	public void testOnMessageException() throws Exception {
		
		Card card = new Card();
		card.setBoard("test-board");
		card.setId("test");
		
		String body = "{ \"board\":\"test-board\", \"fields\": { \"field\":\"value\" } }";
		Map<String,Object> values = new HashMap<String,Object>();
		values.put("userid", "admin");
		
		byte[] bodyArray = body.getBytes();
		when(message.getBody()).thenReturn(bodyArray);
		when(message.getMessageProperties()).thenReturn(messageProperties);
		when(messageProperties.getHeaders()).thenReturn(values);
		when(cardService.createCard(eq("test-board"), any(),eq(false))).thenThrow(new ResourceNotFoundException());
		
		inputFromJms.onMessage(message);
		
		verify(securityTool, times(1)).iAm("admin");
		verify(cardService, times(1) ).createCard(eq("test-board"),any(),eq(false));
		verify(exceptionManager, times(1)).raiseException(anyString(), any(), anyString(), anyBoolean());
	}

}
