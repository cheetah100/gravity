/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.automation.plugin.ExecutionPlugin;
import nz.devcentre.gravity.automation.plugin.Plugin;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.managers.PluginManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardRule;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.ObjectMapping;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.services.TaskService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.ComplexDateConverter;
import nz.devcentre.gravity.tools.DateInterpreter;
import nz.devcentre.gravity.tools.NowDateConverter;
import nz.devcentre.gravity.tools.PlusDateConverter;
import nz.devcentre.gravity.tools.SubtractDateConverter;

@ExtendWith(SpringExtension.class)
public class AutomationEngineTest {

	AutomationEngine engine;
	
	@Mock
	SecurityTool securityTool;
	
	@Mock 
	CardController cardController;
	
	@Mock 
	BoardsCache boardsCache;
	
	@Mock
	RuleCache ruleCache;
	
	@Mock
	ApplicationContext appContext;
	
	@Mock
	TaskService taskService;
	
	@Mock
	IntegrationService integrationService;
	
	AutomationPluginImpl plugin;
	
	@BeforeEach
	public void init(){
		
		this.plugin = new AutomationPluginImpl();
		when( appContext.getBean("test_plugin")).thenReturn(this.plugin);
		
		List<ComplexDateConverter> complexDateConverters = new ArrayList<ComplexDateConverter>();
		complexDateConverters.add(new PlusDateConverter());
		complexDateConverters.add(new SubtractDateConverter());
		complexDateConverters.add(new NowDateConverter());
		
		DateInterpreter di = new DateInterpreter();
		di.setComplexDateConverters(complexDateConverters);
		VariableInterpreter vi = new VariableInterpreter();
		
		engine = new AutomationEngine(getPluginManager(), securityTool, ruleCache, taskService);
	}	

	@Test
	@Disabled
	public void testOnCardHolderEvent() throws Exception {
		
		when(cardController.getCard(TestBoardTool.BOARD_ID,"test_card","all", false,false)).thenReturn(getTestCard("test_card"));
		when(boardsCache.getItem(TestBoardTool.BOARD_ID)).thenReturn(getTestBoard());
		when(ruleCache.getItem(TestBoardTool.BOARD_ID)).thenReturn(getTestRule());
		
		CardHolder cardHolder = new CardHolder(TestBoardTool.BOARD_ID, "test_card");
		
		//engine.onCardHolderEvent(cardHolder);
	}
	
	@Test 
	public void testAddNewTask() {
		
	}
	
	@Test
	public void testBooleanPropertyConditions() { 

		Card card = getTestCard("test_card");		
		
		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions;
		boolean conditionsMet;
		
		conditions = getTestConditions( "blocked", "true", Operation.EQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(),null,"Automation");
		Assertions.assertTrue( conditionsMet );	

		conditions = getTestConditions( "special", "true", Operation.EQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(),null,"Automation");
		Assertions.assertFalse( conditionsMet );		
	}
	
	@Test
	public void testNumberPropertyConditions() { 

		Card card = getTestCard("test_card");		
		
		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions;
		boolean conditionsMet;
		
		conditions = getTestConditions( "balance", "600", Operation.LESSTHAN, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );
		
		conditions = getTestConditions( "balance", "400", Operation.LESSTHAN, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(),null,"Automation");
		Assertions.assertFalse( conditionsMet );	
		
	}

	@Test
	public void testStringPropertyConditions() { 
		
		Card card = getTestCard("test_card");		
		
		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions = getTestConditions( "name", "timmy", Operation.EQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		
		boolean conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );
		
		conditions = getTestConditions( "name", "slim", Operation.NOTEQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );		
	}

	
	@Test
	public void testDatePropertyConditions() { 

		Card card = getTestCard("test_card");		
		
		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions;
		boolean conditionsMet;
		
		conditions = getTestConditions( "rfsyesterday", "today+1days", Operation.BEFORE, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(),null,"Automation");
		Assertions.assertTrue( conditionsMet );
		
		conditions = getTestConditions( "rfstomorrow", "today+1days", Operation.BEFORE, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(),null,"Automation");
		Assertions.assertFalse( conditionsMet );		
	}

	
	@Test
	public void testOrPropertyConditions() { 
		
		Card card = getTestCard("test_card");		
		
		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions;
		boolean conditionsMet;
		
		conditions = getTestConditions( "name", "slim|jim", Operation.NOTEQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );

		conditions = getTestConditions( "name", "slim|timmy", Operation.NOTEQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );

		conditions = getTestConditions( "name", "slim|jim", Operation.EQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertFalse( conditionsMet );

		conditions = getTestConditions( "name", "slim|timmy", Operation.EQUALTO, ConditionType.PROPERTY);
		rule.setAutomationConditions(conditions);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );
	}
	
	
	@Test
	public void testPhaseConditionsMet() { 
		
		Card card = getTestCard("A12");
		
		Assertions.assertEquals(TestBoardTool.BOARD_ID,card.getBoard());
		Assertions.assertEquals("test_phase",card.getPhase());
		Assertions.assertEquals("A12",card.getId());

		Rule rule = new Rule();
		rule.setName("test_rule");
		Map<String,Condition> conditions = getTestConditions( "phase", "test_phase", Operation.EQUALTO, ConditionType.PHASE);
		rule.setAutomationConditions(conditions);
		
		boolean conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );
		
		conditions = getTestConditions( "phase", "wrong-phase", Operation.NOTEQUALTO, ConditionType.PHASE);
		conditionsMet = engine.conditionsMet(card, rule.getAutomationConditions(), null,"Automation");
		Assertions.assertTrue( conditionsMet );		
	}	
	
	@Test
	public void testExecuteActions() throws Exception {		
		Rule rule = new Rule();
		rule.setActions(getActions());
		rule.setName("myrule");
		engine.executeActions(getTestCard("timmy"), rule);
	}
	
	@Test
	public void testExecuteActionsBoardRule() throws Exception {
		
		AutomationPluginImpl exec = new AutomationPluginImpl();
		when( appContext.getBean("test")).thenReturn(exec);
		
		  ExecutionPlugin plugin = new ExecutionPlugin();
		  plugin.setApplicationContext(appContext);
		 
		List<String> paramList= new ArrayList<String>();
		paramList.add("param1");
		
		Action action = new Action();
		action.setName("execute-test-action");
		
		Map<String,Object> config = new HashMap<>();
		config.put("resource", "test");
		config.put("response", "result");
		config.put("method", "action2");
		config.put("parameters", paramList);
		
		action.setConfig(config);
		action.setType("execute");

		Map<String, Action> actionMap = new HashMap<String, Action>();
		actionMap.put("execute", action);
		Board testBoard = getTestBoard();
		Rule rule = new Rule();
		rule.setBoardId(testBoard.getId());
		rule.setName("test_rule");
		rule.setActions(actionMap);
		BoardRule boardRule = new BoardRule(testBoard.getId(),rule.getId());
		when(ruleCache.getItem(testBoard.getId(), rule.getId())).thenReturn(rule);
		engine.executeActions(boardRule);
	}
	
	@Test
	public void testExecuteInntegrationActionsPluginNull() throws Exception {
		
		Map<String, Action> actions =  new HashMap<String,Action>();
		Action action = new Action();
		actions.put("first", action);
		Integration integration = new Integration();
		integration.setActions(actions);

		Map<String, Object> context = new HashMap<String, Object>();
		
		Assertions.assertThrows(Exception.class, () -> {
			Batch batch = this.integrationService.createBatch(integration, 1, new Date());
			engine.executeIntegrationActions(context, integration, batch);			
		}, "Exception was expected");
	}

	@Test
	public void testExecuteInntegrationActionsProcessFail() throws Exception {
		
		Map<String, Action> actions =  getActions();
		Integration integration = new Integration();
		integration.setActions(actions);

		Map<String, Object> context = new HashMap<String, Object>();
		
		Assertions.assertThrows(Exception.class, () -> {
			Batch batch = this.integrationService.createBatch(integration, 1, new Date());
			engine.executeIntegrationActions(context, integration, batch);
		}, "Exception was expected");
			
	}

	
	@Test
	public void testExecuteInntegrationActions() throws Exception {
		
		AutomationPluginImpl exec = new AutomationPluginImpl();
		when( appContext.getBean("test")).thenReturn(exec);
		
		ExecutionPlugin plugin = new ExecutionPlugin();
		plugin.setApplicationContext(appContext);
		
		Credential cred = new Credential();
		cred.setId("test_cred");
		cred.setIdentifier("guest");
		cred.setSecret("guest");
		cred.setResource("amqp://localhost:5672");
		when(securityTool.getSecureCredential("test_cred")).thenReturn(cred);

		Map<String, Action> actions =  new HashMap<String,Action>();
		List<String> paramList= new ArrayList<String>();
		paramList.add("param1");

		
		Map<String, Object> config = new HashMap<>();
		config.put("credential", "test_cred");
		config.put("queue", "test_queue");
		config.put("resource", "test");
		config.put("response", "result");
		config.put("method", "action2");
		config.put("parameters", paramList);
				
		Action action = new Action();
		action.setName("execute-test-action");
		action.setType("execute");
		actions.put("execute", action);
		Integration integration = new Integration();
		integration.setId("test");
		integration.setConfig(config);
		integration.setActions(actions);

		Map<String, Object> context = new HashMap<String, Object>();
		context.put("param1", "suresh");
		Batch batch = new Batch();
		batch.setNotificationId("testId");
		batch.setMaxProcessDate(new Date());
		batch.setRowsImported(1);
		Object resultingObject = engine.executeIntegrationActions(context, integration, batch);
		Assertions.assertEquals("samId", resultingObject);
	}

	@Test
	public void testEvaluateTaskRulesException() throws Exception {
		Map<String, Rule> rules = new HashMap<String, Rule>();
		Rule rule1 = getTestRule();
		rule1.setRuleType(RuleType.TASK);
		rules.put("rule1", rule1);
		Card card = getTestCard("test_card");
		CardTask cardTask1 =  Mockito.mock(CardTask.class);
		Collection<CardTask> cardTasks = new ArrayList<CardTask>();
		cardTasks.add(cardTask1);
		when(taskService.getTasks(card.getBoard(), card.getId())).thenReturn(cardTasks);	
		when(cardTask1.getUuid()).thenReturn("A[TEST");
		when(cardTask1.getTaskid()).thenReturn("testID");
		
		Assertions.assertThrows(Exception.class, () -> {
			engine.evaluateTaskRules(rules, card);
		}, "Exception was expected");
			
	}

	@Test
	public void testEvaluateTaskRules() throws Exception {
		Map<String, Rule> rules = new HashMap<String, Rule>();
		Rule rule1 = getTestRule();
		rule1.setRuleType(RuleType.TASK);
		rule1.setId("test_rule1");
		Rule rule2 = getTestRule();
		Condition condition = new Condition();
		condition.setConditionType(ConditionType.PROPERTY);
		condition.setFieldName("color");
		condition.setOperation(Operation.ISNULL);
		Map<String, Condition> taskConditions = new HashMap<String, Condition>();
		taskConditions.put("testCondition", condition);
		rule2.setTaskConditions(taskConditions);
		rule2.setRuleType(RuleType.TASK);
		rule2.setId("test_rule2");
		rules.put("rule1", rule1);
		Rule rule3 = getTestRule();
		rules.put("rule2", rule2);
		rules.put("rule3", rule3);
		
		  Rule rule4 = getTestRule(); rule4.setRuleType(RuleType.TASK); Condition
		  condition1 = new Condition();
		  condition1.setConditionType(ConditionType.PROPERTY);
		  condition1.setFieldName("color"); condition1.setOperation(Operation.NOTNULL);
		  Map<String, Condition> taskConditions1 = new HashMap<String, Condition>();
		  taskConditions1.put("testCondition", condition1);
		  rule4.setTaskConditions(taskConditions1); rules.put("rule4", rule4);
		 
		Card card = getTestCard("test_card");
		CardTask cardTask1 =  Mockito.mock(CardTask.class);	
		  Collection<CardTask> cardTasks = new ArrayList<CardTask>();
		  cardTasks.add(cardTask1);
		  when(taskService.getTasks(card.getBoard(), card.getId())).thenReturn(cardTasks);
		  when(cardTask1.getUuid()).thenReturn("ATEST"); 
		  when(cardTask1.getTaskid()).thenReturn("test_rule");
		engine.evaluateTaskRules(rules, card);
		verify(taskService, times(2)).createTaskFromRule(any(Rule.class), any(Card.class));
		verify(taskService, times(2)).deleteTask(any(CardTask.class));
	}
	


	@Test
	@Disabled
	public void testCheckTaskRuleAssignments() throws Exception{
		CardTools cardTools1 = Mockito.mock(CardTools.class );
		//engine.cardTools =cardTools1;
		Map<String, Rule> rules = new HashMap<String, Rule>();
		Rule rule = getTestRule();
		Map <String, Condition> conditions = getTestConditions("test_cond", "pass or fail", Operation.EQUALTO, ConditionType.PROPERTY);	
		
		rule.setCompletionConditions(conditions);
		rules.put("test_rule", rule);
		Card card = getTestCard("test_card");
		CardTask cardTask1 = Mockito.mock(CardTask.class);
		  Collection<CardTask> cardTasks = new ArrayList<CardTask>();
		  cardTasks.add(cardTask1); 
		  when(taskService.getTasks(card.getBoard(), card.getId())).thenReturn(cardTasks);
		  when(cardTask1.getTaskid()).thenReturn("test_rule");
		  engine.checkTaskRuleAssignments(rules, card);
		  //when(engine.cardTools.evalProperty(card, conditions.get("test_cond"))).thenReturn(true);
		engine.checkTaskRuleAssignments(rules, card);
		verify(taskService, times(2)).checkAssignment(any(Rule.class), any(Card.class), any(CardTask.class)); 
		//verify(taskService, times(1)).taskAction(any(CardTask.class), eq(TaskAction.COMPLETE), any(String.class)); 
	}
	
	@Test
	public void testGetRuleExplanationStr()throws Exception{
		Map<String, Boolean> ruleExplanation =new HashMap<String,Boolean>();
		ruleExplanation.put("test1", true);
		ruleExplanation.put("test2", false);
		Assertions.assertEquals("test2: false, test1: true", engine.getRuleExplanationStr(ruleExplanation));
	}
	
	private Map<String, Condition> getTestConditions(String field, String value, Operation operation, ConditionType type) {
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		Condition condition = new Condition(field,operation,value);
		condition.setConditionType(type);
		conditions.put("test", condition);
		return conditions;
	}
	
	public Card getTestCard(String id){
		
		ExampleBean bean = new ExampleBean();
		bean.setName(id);
		bean.setNumber("4");
		
		Map<String,Object> fields = new HashMap<String,Object>();
		fields.put("name", "timmy");
		fields.put("street", "Symonds Street");
		fields.put("balance", 500);
		fields.put("blocked", true);
		fields.put("special", false);
		fields.put("testbean", bean);
		fields.put("rfstoday", getTestDate(0));
		fields.put("rfsyesterday", getTestDate(-2));
		fields.put("rfstomorrow", getTestDate(2));
		
		Card card = new Card();
		card.setFields(fields);
		card.setId(id);
		card.setBoard("test_board");
		card.setPhase("test_phase");
		card.setId(id);
		return card;
	}
	
	public Board getTestBoard() throws Exception{
		Board board = new Board();
		board.setId("test_board");
		board.setName("Test Board");
		Map<String,Phase> phases = new HashMap<String,Phase>();
		phases.put("test_phase", getTestPhase("test_phase"));
		phases.put("archive", getTestPhase("archive"));
		board.setPhases(phases);
		return board;
	}
	
	private Phase getTestPhase(String id) throws Exception{
		Phase phase = new Phase();
		phase.setId(id);
		phase.setName(id);
		return phase;
	}
	
	private Date getTestDate(int i){
		long dateMillis = System.currentTimeMillis() + (i * (1000*60*60*24));
		Date date = new Date(dateMillis);
		return date;
	}
	
	public Rule getTestRule() throws Exception{
		Rule rule = new Rule();
		rule.setActions(getActions());
		rule.setId("test_rule");
		rule.setName("Test Rule");
		rule.setRuleType(RuleType.COMPULSORY);
		rule.setAutomationConditions(getTestConditions("blah", "bleh",Operation.EQUALTO, ConditionType.PROPERTY));
		return rule;
	}
	
	public Map<String,Action> getActions(){
		Map<String, Action> actionMap = new HashMap<String,Action>();
		
		LinkedHashMap<String, String> properties = new LinkedHashMap<String,String>();
		properties.put("name","not used");
		properties.put("street", "not used");
		properties.put("balance", "not used");
		properties.put("blocked", "not used");
		properties.put("special", "not used");
		properties.put("note", "This-is-a-note");
		
		List<String> parameterList = new ArrayList<String>();
		parameterList.add("name");
		parameterList.add("street");
		parameterList.add("balance");
		parameterList.add("blocked");
		parameterList.add("special");
		parameterList.add("note");

		Action testAction = ActionTestTool.newTestActionPP("testAction", 1, "execute", 
				"test_plugin", "samId", "execute", properties, parameterList);
		
		
		LinkedHashMap<String, String> secondProperties = new LinkedHashMap<String,String>();
		secondProperties.put("samId","dummySamID");
		List<String> parameterList2 = new ArrayList<String>();
		parameterList2.add("samId");
		Action secondAction = ActionTestTool.newTestActionPP("secondAction", 2, "execute", 
				"test_plugin", "secondResponse", "action2", secondProperties, parameterList2 );
		
		LinkedHashMap<String, String> thirdProperties = new LinkedHashMap<String,String>();
		List<String> parameterList3 = new ArrayList<String>();
		parameterList3.add("testbean");
		Action thirdAction = ActionTestTool.newTestActionPP("thirdAction", 3, "execute", 
				"test_plugin", "timmy", "action3", thirdProperties, parameterList3);
		
		actionMap.put("testAction", testAction);
		actionMap.put("secondAction", secondAction);
		actionMap.put("thirdAction", thirdAction);
		return actionMap;
	}
	
	public Map<String,ObjectMapping> getMappings(){
		Map<String, ObjectMapping> objectMappings = new HashMap<String,ObjectMapping>();
		ObjectMapping mapping = new ObjectMapping();
		mapping.setClassname("nz.devcentre.gravity.automation.TestBean");
		mapping.setName("testmap");
		
		Map<String,String> mappings = new HashMap<String,String>();
		mappings.put("name", "setName");
		mappings.put("", "setNumber");
		mapping.setMappings(mappings);
		
		objectMappings.put("testmap", mapping);
		return objectMappings;
	}
	
	public PluginManager getPluginManager(){		
		Map<String,Plugin> plugins = new HashMap<String,Plugin>();
		ExecutionPlugin executionPlugin = new ExecutionPlugin();
		executionPlugin.setApplicationContext(this.appContext);
		plugins.put("execute", executionPlugin);
		plugins.put("test", new ExamplePlugin());
		return new PluginManager(plugins);
	}
	
	
}
