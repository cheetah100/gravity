/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class VariableInterpreterTest {
	
	@InjectMocks
	VariableInterpreter vi;
	
	Map<String, Object> context;
	
	@BeforeEach
	public void init(){
		this.context = new HashMap<>();
		context.put("name", "john");
		context.put("street", "sunnydale");
		context.put("reg", "xxxx,yyyy");
	}

	@Test
	public void testIsVariableExpression() {
		boolean v1 = vi.isVariableExpression("#foo");
		Assertions.assertTrue(v1);
		boolean v2 = vi.isVariableExpression("foo");
		Assertions.assertFalse(v2);
	}

	@Test
	public void testResolveRegex() {
		Object resolve = vi.resolve(context, "#reg->(^.*?(?=,),)");
		System.out.println( resolve );
		Assertions.assertEquals( "yyyy", resolve);
	}
	
	@Test
	public void testResolveBasic() {
		Object resolve = vi.resolve(context, "basic");
		Assertions.assertEquals( "basic", resolve);
	}

	@Test
	public void testResolveReference() {
		Object resolve = vi.resolve(context, "#name");
		Assertions.assertEquals( "john", resolve);
	}

	@Test
	public void testResolveNow() {
		Object resolve = vi.resolve(context, "#now");
		Assertions.assertTrue( resolve instanceof Date);
	}
	
	@Test
	public void testResolveEmpty() {
		Object resolve = vi.resolve(context, "");
		Assertions.assertEquals( "", resolve);
	}
	
	@Test
	public void testResolveNowPlusFour() {
		Object resolve = vi.resolve(context, "#now+4");
		Assertions.assertTrue( resolve instanceof Date);
	}

	@Test
	public void testResolveValues() {
		List<Object> resolveValues = vi.resolveValues(context, "test|buffer|#name");
		Assertions.assertTrue( resolveValues.contains("test"));
		Assertions.assertTrue( resolveValues.contains("john"));
	}

	@Test
	public void testCompleteConfig() {
		
		Map<String, Object> config = new HashMap<>();
		config.put("class", "person");
		config.put("species", "human");
		config.put("identifier", "#name");
		config.put("address", "#street");
		
		Map<String, Object> completeConfig = vi.completeConfig(config, context);
		
		Assertions.assertEquals( "person", completeConfig.get("class"));
		Assertions.assertEquals( "john", completeConfig.get("identifier"));
	}

}
