/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.anyString;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.ClusterManager;
import nz.devcentre.gravity.model.Coup;
import nz.devcentre.gravity.model.IdRequest;
import nz.devcentre.gravity.tools.IdentifierTools;

@ExtendWith(MockitoExtension.class)
public class IdGeneratorReceiverTest {
	
	@InjectMocks
	private  IdGeneratorReceiver idg;
	
	@Mock
	private RabbitTemplate rabbitTemplate;
	
	@Mock 
	ClusterManager clusterManager;
	
	@Mock
	IdentifierTools identifierTools;
	
	@Mock
	FanoutExchange gravityIdGeneratorExchange;
	
	@Mock 
	Message message;

	@Test
	public void testCoupMessageFromSelf() throws IOException {
		
		Coup coup = new Coup();
		coup.setRequestId("server");
		coup.setCoupTime(System.currentTimeMillis());
		
		byte[] a = MessageTestTool.serialObject(coup);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.getServerId()).thenReturn("server");
		
		idg.onMessage(message);
	}

	@Test
	public void testCoupMessageFromPrimary() throws IOException {
		
		Coup coup = new Coup();
		coup.setRequestId("primary");
		coup.setCoupTime(1000l);
		
		byte[] a = MessageTestTool.serialObject(coup);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.getServerId()).thenReturn("server");
		when(clusterManager.getStartTime()).thenReturn(500l);
		
		idg.onMessage(message);
	}
	
	@Test
	public void testCoupMessageFromNode() throws IOException {
		
		Coup coup = new Coup();
		coup.setRequestId("node");
		coup.setCoupTime(1000l);
		
		byte[] a = MessageTestTool.serialObject(coup);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.getServerId()).thenReturn("server");
		when(clusterManager.getStartTime()).thenReturn(2500l);
		
		idg.onMessage(message);
		
		verify( this.clusterManager).setNewLeader("node", 1000l);
		
	}
	
	@Test
	public void testIDGenerator() throws Exception {
		
		IdRequest r = new IdRequest();
		r.setBoardId("board");
		r.setName("no");
		r.setRequestId("r1");
		
		byte[] a = MessageTestTool.serialObject(r);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.isLeader()).thenReturn(true);
		when(identifierTools.getNextSequence("board", "no")).thenReturn(5l);
		
		idg.onMessage(message);
		
		verify(identifierTools).getNextSequence(anyString(), anyString());
	}
	
	@Test
	public void testMessageException() throws Exception {
		
		IdRequest r = new IdRequest();
		r.setBoardId("board");
		r.setName("no");
		r.setRequestId("r1");
		
		byte[] a = MessageTestTool.serialObject(r);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.isLeader()).thenReturn(true);
		when(identifierTools.getNextSequence("board", "no")).thenThrow( new Exception("Blah"));
		
		idg.onMessage(message);
	}
	
	@Test
	public void testMessageResourceNotFoundException() throws Exception {
		
		IdRequest r = new IdRequest();
		r.setBoardId("board");
		r.setName("no");
		r.setRequestId("r1");
		
		byte[] a = MessageTestTool.serialObject(r);
		when(message.getBody()).thenReturn(a);
		when(clusterManager.isLeader()).thenReturn(true);
		when(identifierTools.getNextSequence("board", "no")).thenThrow( new ResourceNotFoundException("Blah"));
		
		idg.onMessage(message);
	}

	
	@Test
	public void testRegister() {
		SyncMessage register = idg.register("thing");
		Assertions.assertNotNull(register);
	}

	@Test
	public void testDelete() {
		idg.delete("thing");
	}
	
	

}
