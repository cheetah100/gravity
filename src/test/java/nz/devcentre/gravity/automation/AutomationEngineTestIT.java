/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import nz.devcentre.gravity.automation.plugin.ExecutionPlugin;
import nz.devcentre.gravity.automation.plugin.Plugin;
import nz.devcentre.gravity.tools.ComplexDateConverter;
import nz.devcentre.gravity.tools.DateInterpreter;
import nz.devcentre.gravity.tools.NowDateConverter;
import nz.devcentre.gravity.tools.PlusDateConverter;
import nz.devcentre.gravity.tools.SubtractDateConverter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class AutomationEngineTestIT {
	
	@InjectMocks
	private AutomationEngine engine;
	
	@BeforeEach
	public void init(){
		
		List<ComplexDateConverter> complexDateConverters = new ArrayList<ComplexDateConverter>();
		complexDateConverters.add(new PlusDateConverter());
		complexDateConverters.add(new SubtractDateConverter());
		complexDateConverters.add(new NowDateConverter());
		
		DateInterpreter di = new DateInterpreter();
		di.setComplexDateConverters(complexDateConverters);
		
		//engine = new AutomationEngine();
		//engine.setPlugins(getPlugins());
	}
	
	@Test
	public void testPropertyConditionsMet() {
		Assertions.assertNotNull(engine);
	}
	
	@Test
	public void testEvalProperty_time() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.applyLocalizedPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
		
		Date date = new Date();
		
		String time1 = "xs:dateTime('" + df.format(date) + "+00:00')";
		System.out.println(time1);
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String time = "xs:dateTime('" + df.format(date) + "+00:00')";
		System.out.println(time);
	}
	
	public Map<String,Object>getServices(){
		AutomationPluginImpl plugin = new AutomationPluginImpl();
		Map<String,Object> plugins = new HashMap<String,Object>();
		plugins.put("test_plugin", plugin);
		return plugins;
	}

	public Map<String,Plugin>getPlugins(){		
		Map<String,Plugin> plugins = new HashMap<String,Plugin>();
		ExecutionPlugin executionPlugin = new ExecutionPlugin();
		plugins.put("execute", executionPlugin);
		plugins.put("test", new ExamplePlugin());
		return plugins;
	}

}
