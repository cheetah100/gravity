/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.Message;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.BoardRule;
import nz.devcentre.gravity.model.CardHolder;

@ExtendWith(MockitoExtension.class)
public class JmsEventListenerTest {
	
	@InjectMocks
	JmsEventListener jel;
	
	@Mock
	AutomationEngine automationEngine;
	
	@Mock
	BoardListener listener;
	
	@Mock
	Message message;

	@Test
	public void testCardHolderOnMessage() throws Exception {
		
		CardHolder ch = new CardHolder("board", "card");
		byte[] a = MessageTestTool.serialObject(ch);
		when(message.getBody()).thenReturn(a);
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
		
		verify(listener).onCardHolderEvent(any());
	}
	
	@Test
	public void testCardHolderOnMessageListenerException() throws Exception {
		
		CardHolder ch = new CardHolder("board", "card");
		byte[] a = MessageTestTool.serialObject(ch);
		when(message.getBody()).thenReturn(a);
		doThrow(Exception.class).when(listener).onCardHolderEvent(any());
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
	}

	@Test
	public void testBoardRuleOnMessage() throws Exception {
		
		BoardRule br = new BoardRule("board","rule");
		byte[] a = MessageTestTool.serialObject(br);
		when(message.getBody()).thenReturn(a);
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
	}
	
	@Test
	public void testBoardRuleOnMessageException() throws Exception {
		
		BoardRule br = new BoardRule("board","rule");
		byte[] a = MessageTestTool.serialObject(br);
		when(message.getBody()).thenReturn(a);
		doThrow(Exception.class).when(automationEngine).executeActions(any());
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
	}
	
	@Test
	public void testBoardRuleOnMessageResourceNotFoundException() throws Exception {
		
		BoardRule br = new BoardRule("board","rule");
		byte[] a = MessageTestTool.serialObject(br);
		when(message.getBody()).thenReturn(a);
		doThrow(ResourceNotFoundException.class).when(automationEngine).executeActions(any());
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
	}
	
	@Test
	public void testBadOnMessage() throws Exception {
		
		byte[] a = MessageTestTool.serialObject("Garbage");
		when(message.getBody()).thenReturn(a);
		
		List<BoardListener> listeners = new ArrayList<>();
		listeners.add(listener);
		jel.setListeners(listeners);
		jel.onMessage(message);
	}
	
	@Test
	public void testNullListeners() throws Exception {
		jel.setListeners(null);
	}

}
