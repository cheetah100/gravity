package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;

class CsvGeneratorTest {
	
	@Test
	void testProcess() throws Exception {
		
		CsvGenerator plugin = new CsvGenerator(); 
		
		Map<String,String> properties = new HashMap<>();
		properties.put("name", "name");
		properties.put("phone", "phone");
		properties.put("address", "address");
		properties.put("balance", "balance");
		
		Action action = ActionTestTool.newTestActionPP("csv_generator-action", 0, "csvgenerator", 
				"data", "result", null, properties, null);
				
		Map<String,Object> context = new HashMap<String,Object>();
		
		List<Card> data = new ArrayList<>();
		data.add(TestBoardTool.getTestCard("Joe Smith!", "", 50));
		data.add(TestBoardTool.getTestCard("Tim Taylor", "", 34));
		data.add(TestBoardTool.getTestCard("Penny Hendrix", "", 76));
		data.add(TestBoardTool.getTestCard("Linda, Mc'Donald", "", 23));
		context.put("data", data);
		
		assertNotNull(plugin);
		
		Map<String, Object> process = plugin.process(action, context);
		
		Object object = process.get("result");
		
		assertNotNull(object);
		
		assertTrue( object instanceof String);
		
		System.out.println( object);
	}

}
