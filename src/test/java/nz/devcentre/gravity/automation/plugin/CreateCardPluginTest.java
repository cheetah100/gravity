/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@ExtendWith(MockitoExtension.class)
public class CreateCardPluginTest {
	
	@Mock
	private VariableInterpreter variableInterpreter;
		
	@Mock
	private CardService cardService;
	
	@Mock
	private SecurityTool securityTool;
	
	@InjectMocks
	private CreateCardPlugin plugin;
	
	@BeforeEach
	public void setUp() throws Exception {
		/*
		when( variableInterpreter.resolve(any(Map.class), eq("name"))).thenReturn("Peter");
		when( variableInterpreter.resolve(any(Map.class), eq("number"))).thenReturn("47");
		when( this.variableInterpreter.resolve(any(Map.class), eq("test-board"))).thenReturn("test-board");
		*/
	}

	@Test
	public void TestCreateSingleCard() throws Exception {
		Card card = new Card();
		card.setId("test_card");
		when( cardService.createCard(any(String.class), any(Card.class), eq(false))).thenReturn(card);
		
		Action action = new Action();
		action.setName("create-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");
		
		Map<String, String> properties = new HashMap<String, String>();
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("name", "Peter");
		context.put("number", "47");
			
		Map<String, Object> process = this.plugin.process(action, context);
		
		String result = (String) process.get("result");
		Assertions.assertEquals( "test_card", result);
		
		verify(cardService, times(1)).createCard(anyString(), any(), anyBoolean());
	}
	
	@Test
	public void TestListToCard() throws Exception {
		Card card = new Card();
		card.setId("test_card");
		when( cardService.createCard(any(String.class), any(Card.class), eq(false))).thenReturn(card);
		
		Action action = new Action();
		action.setName("create-multi-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");		
		action.setType("createcard");
		
		Map<String, String> properties = new HashMap<String, String>();
		action.setConfigField("properties",properties);
		
		List<String> parameters = new ArrayList<String>();
		parameters.add("records");
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		List<Map<String,Object>> records = new ArrayList<Map<String,Object>>();
		for( int f=0; f < 4; f++) {
			Map<String,Object> record = new HashMap<String,Object>();
			record.put("name", "User " + f);
			record.put("number", "47");
			records.add(record);
		}
		context.put("records", records);
		
		this.plugin.process(action, context);
	
		verify(this.cardService, times(4)).createCard(anyString(), any(), anyBoolean());
	}
	
	@Test
	public void TestMapToCard() throws Exception {
		Card card = new Card();
		card.setId("test_card");
		when( cardService.createCard(any(String.class), any(Card.class), eq(false))).thenReturn(card);
		
		Action action = new Action();
		action.setName("create-multi-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");
		
		Map<String, String> properties = new HashMap<String, String>();
		action.setConfigField("properties",properties);
		
		List<String> parameters = new ArrayList<String>();
		parameters.add("records");
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		Map<String, Map<String,Object>> records = new HashMap<String,Map<String,Object>>();
		for( int f=0; f < 4; f++) {
			Map<String,Object> record = new HashMap<String,Object>();
			record.put("name", "User " + f);
			record.put("number", "47");
			records.put("User " + f,record);
		}
		context.put("records", records);
		
		this.plugin.process(action, context);
	
		verify(this.cardService, times(4)).createCard(anyString(), any(), anyBoolean());
	}
	
	@Test
	public void TestSaveCardFieldMappingString() throws Exception {		
		// Have createCard return the card it was passed when it's called
		when(cardService.createCard(any(String.class), any(Card.class), eq(false))).thenAnswer(
				i -> i.getArguments()[1]);
		
		Action action = new Action();
		action.setName("create-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("testKey", "testVal");
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		Card savedCard = this.plugin.saveCard("test-board", context, context, properties);
		String savedFieldVal = (String)savedCard.getFields().get("testKey");
		Assertions.assertEquals("testVal", savedFieldVal);
	}
	
	@Test
	public void TestSaveCardFieldMappingHashReference() throws Exception {		
		// Have createCard return the card it was passed when it's called
		when(cardService.createCard(any(String.class), any(Card.class), eq(false))).thenAnswer(
				i -> i.getArguments()[1]);
		
		Action action = new Action();
		action.setName("create-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("testKey", "#testContextVariable");
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("testContextVariable", "testVal");
		
		Card savedCard = this.plugin.saveCard("test-board", context, context, properties);
		String savedFieldVal = (String)savedCard.getFields().get("testKey");
		Assertions.assertEquals("testVal", savedFieldVal);
	}
	
	@Test
	public void TestSaveCardFieldMappingHashReferenceWithBrackets() throws Exception {		
		// Have createCard return the card it was passed when it's called
		when(cardService.createCard(any(String.class), any(Card.class), eq(false))).thenAnswer(
				i -> i.getArguments()[1]);
		
		Action action = new Action();
		action.setName("create-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("testKey", "#testContextVariable[t]");
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("testContextVariable[t]", "testVal");
		
		Card savedCard = this.plugin.saveCard("test-board", context, context, properties);
		String savedFieldVal = (String)savedCard.getFields().get("testKey");
		Assertions.assertEquals("testVal", savedFieldVal);
	}
	
	@Test
	public void TestSaveCardFieldMappingBracketReference() throws Exception {		
		// Have createCard return the card it was passed when it's called
		when(cardService.createCard(any(String.class), any(Card.class), eq(false))).thenAnswer(
				i -> i.getArguments()[1]);
		
		Action action = new Action();
		action.setName("create-card-action");
		action.setConfigField("resource", "test-board");
		action.setConfigField("response", "result");
		action.setType("createcard");

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("testKey", "[testContextVariable]");
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("testContextVariable", "testVal");
		
		Card savedCard = this.plugin.saveCard("test-board", context, context, properties);
		String savedFieldVal = (String)savedCard.getFields().get("testKey");
		Assertions.assertEquals("testVal", savedFieldVal);
	}
}
