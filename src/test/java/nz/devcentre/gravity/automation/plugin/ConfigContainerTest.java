/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ConfigContainerTest {
	
	ConfigContainer container;
	
	@BeforeEach
	public void setUp() throws Exception {
		Map<String, Object> config = new HashMap<>();
		config.put("staticstring", "StaticString");
		config.put("dynamicstring", "#dynamic");
		
		config.put("staticboolean", true);
		config.put("dynamicboolean", "#bool");
		config.put("stringmap", "#stringmap");
		config.put("objectmap", "#objectmap");
		config.put("parameters", "#list");
		
		Map<String, Object> context = new HashMap<>();
		context.put("dynamic", "DynamicString");
		context.put("bool", true);
		
		Map<String,String> stringMap = new HashMap<>();
		stringMap.put("value", "Value");
		
		Map<String,Object> objectMap = new HashMap<>();
		objectMap.put("object", new Object());
		
		List<String> list = new ArrayList<>();
		list.add("element");
		
		context.put("stringmap",  stringMap);
		context.put("objectmap", objectMap);
		context.put("list", list);
		
		this.container = new ConfigContainer(config, context);
	}

	@Test
	public void testConfigContainerMapOfStringObject() {
		Map<String, Object> config = new HashMap<>();
		config.put("string", "String");
		this.container = new ConfigContainer(config);
		Assertions.assertNotNull(this.container);
		Map<String, Object> configOut = container.getConfig();
		Assertions.assertTrue( configOut.containsKey("string"));
	}

	@Test
	public void testGetConfig() {
		Map<String, Object> configOut = this.container.getConfig();
		Assertions.assertTrue( configOut.containsKey("staticstring"));
	}

	@Test
	public void testGetStringValue() {
		Assertions.assertEquals( "StaticString", this.container.getStringValue("staticstring"));
		Assertions.assertEquals( "DynamicString", this.container.getStringValue("dynamicstring"));
	}

	@Test
	public void testGetStringValueNotFound() {
		Assertions.assertNull(this.container.getStringValue("notfound"));
	}
	
	@Test
	public void testGetBooleanValue() {
		Assertions.assertEquals( true, this.container.getBooleanValue("staticboolean"));
		Assertions.assertEquals( true, this.container.getBooleanValue("dynamicboolean"));
	}
	
	@Test
	public void testGetBooleanValueNotFound() {
		Assertions.assertEquals( false, this.container.getBooleanValue("nothere"));
	}
	
	@Test
	public void testGetBooleanValueWrongType() {
		Assertions.assertEquals( false, this.container.getBooleanValue("parameters"));
	}

	@Test
	public void testGetStringMapValue() {
		Map<String, String> stringMapValue = this.container.getStringMapValue("stringmap");
		Assertions.assertNotNull(stringMapValue);
		Assertions.assertTrue(stringMapValue.containsKey("value"));
		Assertions.assertEquals("Value", stringMapValue.get("value"));
	}

	@Test
	public void testGetStringMapValueNotFound() {
		Map<String, String> stringMapValue = this.container.getStringMapValue("notfound");
		Assertions.assertNull(stringMapValue);
	}
	
	@Test
	public void testGetStringMapValueWrongType() {
		Map<String, String> stringMapValue = this.container.getStringMapValue("parameters");
		Assertions.assertNull(stringMapValue);
	}

	@Test
	public void testGetObjectMapValue() {
		Map<String, Object> objectMapValue = this.container.getObjectMapValue("objectmap");
		Assertions.assertNotNull(objectMapValue);
		Assertions.assertTrue(objectMapValue.containsKey("object"));
	}

	@Test
	public void testGetObjectMapValueNotFound() {
		Map<String, Object> objectMapValue = this.container.getObjectMapValue("notfound");
		Assertions.assertNull(objectMapValue);
	}
	
	@Test
	public void testGetObjectMapValueWrongType() {
		Map<String, Object> objectMapValue = this.container.getObjectMapValue("staticboolean");
		Assertions.assertNull(objectMapValue);
	}

	@Test
	public void testGetListValue() {
		List<String> listValue = this.container.getListValue("parameters");
		Assertions.assertNotNull(listValue);
		Assertions.assertTrue(listValue.contains("element"));
	}

	@Test
	public void testGetListValueNotFound() {
		List<String> listValue = this.container.getListValue("notfound");
		Assertions.assertNull(listValue);
	}
	
	@Test
	public void testGetListValueWrongType() {
		List<String> listValue = this.container.getListValue("objectmap");
		Assertions.assertNull(listValue);
	}
}
