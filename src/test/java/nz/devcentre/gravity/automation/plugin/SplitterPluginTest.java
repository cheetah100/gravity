/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.tools.CardTools;

@ExtendWith(MockitoExtension.class)
public class SplitterPluginTest {
	
	@InjectMocks
	SplitterPlugin plugin;
	
	@Mock
	private AutomationEngine automationEngine;
		
	@Mock
	private RuleCache ruleCache;
	
	@Mock 
	private CardTools cardTools;
	
	@Mock
	private Rule rule;

	@Test
	public void testProcess() throws Exception {
		
		Action action = new Action();
		action.setName("splitter_action");
		action.setType("splitter");
		action.setConfigField("target_board", "test-board");
		action.setConfigField("target_rule", "test-rule");
		action.setConfigField("field_to_split", "records");
		action.setConfigField("field_map", getFieldMap());
		
		Map<String,Object> testRecords = new HashMap<>();
		testRecords.put("1", getTestFields());
		
		Map<String,Object> context = new HashMap<>();
		context.put("records", testRecords);
		
		Mockito.when(this.ruleCache.getItem("test-board","test-rule")).thenReturn( this.rule);
		Mockito.when(this.rule.getAutomationConditions()).thenReturn(new HashMap<String,Condition>());
		
		plugin.process(action, context);
		
		verify(this.automationEngine, times(1)).executeActions(any(Rule.class), any(Map.class));
	}

	@Test
	public void testMapNewContext() {
		
		Map<String, Object> out = SplitterPlugin.mapNewContext(getFieldMap(), getTestFields());
		
		assertEquals( "john", out.get("name"));
		assertEquals( "john@test.com", out.get("email"));
		assertEquals( "Hillcroft", out.get("suburb"));
		assertEquals( "Hamilton", out.get("city"));
	}
	
	@Test
	public void testListToMap() {
		List<String> list = new ArrayList<>();
		list.add("first");
		list.add("second");
		list.add("third");
		list.add("fourth");
		list.add("fifth");
		
		Map<String, Object> map = SplitterPlugin.listToMap(list);
		assertEquals( "first", map.get("field_1"));
		assertEquals( "fifth", map.get("field_5"));
	}
	
	private static Map<String,String> getFieldMap(){
		Map<String,String> map = new HashMap<>();
		map.put("name", "first_name");
		map.put("email", "email_address");
		map.put("street", "address_1");
		map.put("suburb", "address_2");
		map.put("city", "address_3");
		return map;
	}
	
	private static Map<String,Object> getTestFields(){
		Map<String,Object> map = new HashMap<>();
		map.put("first_name", "john");
		map.put("email_address", "john@test.com");
		map.put("address_1", "89 Sunnyhook Road");
		map.put("address_2", "Hillcroft");
		map.put("address_3", "Hamilton");
		return map;
	}
}
