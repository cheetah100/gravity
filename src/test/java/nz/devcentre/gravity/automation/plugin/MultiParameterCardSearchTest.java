/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MultiParameterCardSearchTest {
	
	@Mock
	private Board board;
	
	@Mock
	private BoardsCache boardsCache;

	@Mock
	private QueryService listTools;
	
	@Mock
	private MultiParameterCardSearch cardSearch; 
	
	@Mock
	private CardSearchPlugin cardSearchPlugin;
	
	@Mock 
	private CardService cardService;
	
	@Mock
	private GetCardPlugin getCardPlugin;
	
	private Collection<Card> results=new ArrayList<Card>();
	
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		ReflectionTestUtils.setField(this.getCardPlugin, "cardTools", this.cardService);
		ReflectionTestUtils.setField(this.cardSearch, "listTools", this.listTools);
		ReflectionTestUtils.setField(this.cardSearchPlugin, "listTools", this.listTools);
		ReflectionTestUtils.setField(this.cardSearch, "boardsCache", this.boardsCache);
		// ReflectionTestUtils.setField(this.getCardPlugin,"cardController", this.cardController);
		
		Mockito.when(this.cardSearch.process(Mockito.any(Action.class), Mockito.anyMap())).thenCallRealMethod();
		Mockito.when(this.cardSearchPlugin.process(Mockito.any(Action.class), Mockito.anyMap())).thenCallRealMethod();
		
		Mockito.when(this.listTools.dynamicQuery(Mockito.anyString(), Mockito.anyString(), Mockito.any(Filter.class), Mockito.any(View.class), Mockito.anyBoolean())).thenReturn(this.results);
		
		Mockito.when(this.listTools.search(Mockito.anyString(), Mockito.any(Filter.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(this.results);
		
		Mockito.when(this.boardsCache.getItem(Mockito.anyString())).thenReturn(board);
		
		// Mockito.when(this.board.getTemplates()).thenReturn(templates);
		
		// Mockito.when(this.templates.get(Mockito.any())).thenReturn(template);
		
		//Mockito.when(this.boardController.searchWithMap(Mockito.anyString(), Mockito.anyMap())).thenReturn(this.results);
		
		Mockito.when(this.getCardPlugin.process(Mockito.any(Action.class), Mockito.anyMap())).thenCallRealMethod();
	}

	@Test
	public void testProcess() throws Exception {
		
		this.results.clear();
		
		Map<String,String> properties=new LinkedHashMap<String, String>();
		properties.put("firstName","John");
		properties.put("lastName","Snow");
		
		Action action = ActionTestTool.newTestActionPP("Test", 0, "move", 
				"actors", "cards", "", properties, null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		Map<String, Object> process = this.cardSearch.process(action, context);
		assertTrue(process.isEmpty());
		
		Card card=new Card();
		card.setId("1001");
		card.setBoard("actors");
		this.results.add(card);
		
		process = this.cardSearch.process(action, context);
		assertEquals("{cards=1001}", process.toString()); 
		
		action.setConfigField("method", "all");
		process = this.cardSearch.process(action, context);
		assertEquals("{cards=[actors.1001]}", process.toString()); 
		
		action.setConfigField("method", "one");
		process = this.cardSearch.process(action, context);
		assertEquals("{cards=actors.1001}", process.toString());
	}
	
	@Test
	public void testProcessCardSearchPlugin() throws Exception {
		
		//List<Card> results=new ArrayList<Card>();
		
		//Mockito.when(this.boardController.searchCards("actors", "fullName", Operation.EQUALTO,"John Snow",true)).thenReturn(results);
		this.results.clear();
			
		Map<String,String> properties=new LinkedHashMap<String, String>();
		properties.put("localfield","fullName");
		properties.put("remotefield","fullName");
		
		Action action = ActionTestTool.newTestActionPP("Test", 0, "cardsearch", 
				"actors", "cards", "", properties, null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("fullName","John Snow");
		Map<String, Object> process = this.cardSearchPlugin.process(action, context);
		assertFalse(process.containsKey("cards"));
		
		Card card=new Card();
		card.setId("1001");
		card.setBoard("actors");
		this.results.add(card);
		
		Mockito.when(this.listTools.search(Mockito.anyString(), Mockito.any(Filter.class), Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyBoolean())).thenReturn(this.results);
		
		process = this.cardSearchPlugin.process(action, context);
		assertEquals("{cards=1001, fullName=John Snow}", process.toString());
		
		action.setConfigField("method","all");
		process = this.cardSearchPlugin.process(action, context);
		assertEquals("{cards=[actors.1001], fullName=John Snow}", process.toString());
		
		action.setConfigField("method","one");
		process = this.cardSearchPlugin.process(action, context);
		assertEquals("{cards=actors.1001, fullName=John Snow}", process.toString());
		
	}
	@Test
	public void testProcessGetCardPlugin() throws Exception {
		
		Card card=new Card();
		card.setId("1001");
		card.setBoard("actors");
		Map<String,Object> fields=new LinkedHashMap<String, Object>();
		fields.put("name", "John Snow");
		card.setFields(fields);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("id", "1001");
		context.put("name", "John Snow");
		
		// Mockito.when(this.cardController.getCard2(Mockito.anyString(),Mockito.anyString())).thenReturn(card);
		Mockito.when(cardService.getCard(Mockito.anyString(), Mockito.anyString())).thenReturn(card);
			
		Map<String,String> properties=new LinkedHashMap<String, String>();
		properties.put("localfield","id");
				
		Action action = ActionTestTool.newTestActionPP("Test", 0, "getcard", 
				"actors", "result", "id", properties, null);
		
		Map<String, Object> process = this.getCardPlugin.process(action, context);
		
		assertEquals("1001", process.get("result"));
		action.setConfigField("method","name");
		
		process = this.getCardPlugin.process(action, context);
		assertEquals("John Snow", process.get("result"));
	}
}
