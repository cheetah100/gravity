/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */


package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;

@ExtendWith(MockitoExtension.class)
public class XmlDocumentParserPluginTest {

	@InjectMocks
	XmlDocumentParserPlugin plugin;
	
	@Mock
	DocumentBuilderFactory documentBuilderFactory;
	
	@Test
	public void TestPlugin() throws Exception {
		
		List<String> paramList= new ArrayList<String>();
		paramList.add("magic");
				
		Map<String, String> properties = new HashMap<String, String>();

		
		Action action = ActionTestTool.newTestActionPP("xml-test-action", 0, "xmlparser", 
				"param1", "result", null, properties, paramList);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("param1", "<test><name><magic>Magic</magic>Peter</name><number>42</number></test>");
		
		
		when(documentBuilderFactory.newDocumentBuilder()).thenReturn(DocumentBuilderFactory.newInstance().newDocumentBuilder());
		
		Map<String, Object> process = plugin.process(action, context);
		
		String number = (String) process.get("number");
		String magic = (String) process.get("magic");
		
		assertEquals( "42", number);
		assertEquals( "Magic", magic);
	}
}
