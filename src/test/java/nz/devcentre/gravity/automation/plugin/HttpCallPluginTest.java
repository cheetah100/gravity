/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.HttpCallService;
import nz.devcentre.gravity.tools.CallResponse;

import org.mockito.InjectMocks;
import org.mockito.Mock;

@ExtendWith(MockitoExtension.class)
public class HttpCallPluginTest {
	
	@Mock
	HttpCallService tool;
	
	@InjectMocks
	HttpCallPlugin plugin;

	@Test
	public void testProcess() throws Exception {
		
		CallResponse callResponse = new CallResponse("{ data: [ { 'name':'john' },{ 'name':'tim' },{ 'name':'peter' } ] }");
		when( tool.execute(any(),any(), any(), any(), any(), any(), any(), any())).thenReturn(callResponse);
		
		Map<String,String> properties = new HashMap<String,String>();
		properties.put("credential","test_credential");
		
		Action action = ActionTestTool.newTestActionPP("test", 0, "http", 
				"http://echo.jsontest.com/key/value/one/two", "test_body", "GET", properties, null);
		
		Map<String, Object> context = new HashMap<String, Object>();
		
		Map<String, Object> process = plugin.process(action, context);
		
		Assertions.assertNotNull(process);
		
		Object response = process.get("test_body");
		
		Assertions.assertNotNull(response);
		Assertions.assertEquals(response, "{ data: [ { 'name':'john' },{ 'name':'tim' },{ 'name':'peter' } ] }");
	}
}
