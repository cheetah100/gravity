/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.model.Action;

@ExtendWith(MockitoExtension.class)
public class CsvParserTest {
	
	@InjectMocks
	CsvParser parser;

	@Test
	public void testProcess() throws Exception {
		
		Action action = new Action();
		action.setId("test_action");
		action.setName("Test");
		action.setConfigField("resource","body");
		
		List<String> parameters = new ArrayList<>();
		parameters.add("name");
		parameters.add("phone");
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("body", "john,093450697");
		
		Map<String, Object> process = parser.process(action, context);
		
		Assertions.assertEquals( "john", process.get("name"));
		Assertions.assertEquals( "093450697", process.get("phone"));
	}
	
	@Test
	public void testProcessNoBody() throws Exception {
		
		Action action = new Action();
		action.setId("test_action");
		action.setName("Test");
		action.setConfigField("resource","body");
		
		List<String> parameters = new ArrayList<>();
		parameters.add("name");
		parameters.add("phone");
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		
		Map<String, Object> process = parser.process(action, context);
		Assertions.assertFalse( process.containsKey("name"));
	}
	
	@Test
	public void testProcessBadBody() throws Exception {
		
		Action action = new Action();
		action.setId("test_action");
		action.setName("Test");
		action.setConfigField("resource","body");
		
		List<String> parameters = new ArrayList<>();
		parameters.add("name");
		parameters.add("phone");
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("body", "john");
		
		Map<String, Object> process = parser.process(action, context);
		
		Assertions.assertEquals( "john", process.get("name"));
		Assertions.assertFalse( process.containsKey("phone"));
	}

}
