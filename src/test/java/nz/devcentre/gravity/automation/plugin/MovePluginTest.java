/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.CardService;

@ExtendWith(MockitoExtension.class)
public class MovePluginTest {
	
	@InjectMocks
	MovePlugin plugin;
	
	@Mock
	CardService cardService;
	
	@Mock
	VariableInterpreter vi;

	@Test
	public void testMove() throws Exception {
		
		when( vi.resolve(any(), any())).thenReturn("next_phase");
				
		Action action = ActionTestTool.newTestAction("Test", 0, "move", 
				"test", null, null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("boardid", "test_board");
		context.put("phaseid", "test_phase");
		context.put("cardid", "card_id");
		
		Map<String, Object> process = this.plugin.process(action, context);
		
		verify(cardService).moveCard("test_board", "card_id", "next_phase", null);
		
		assertEquals( "next_phase", process.get("phaseid"));
		
	}

	@Test
	public void testMoveNotNeeded() throws Exception {
		
		when( vi.resolve(any(), any())).thenReturn("test_phase");
				
		Action action = ActionTestTool.newTestAction("Test", 0, "move", 
				"test", "result", null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("boardid", "test_board");
		context.put("phaseid", "test_phase");
		context.put("cardid", "card_id");
		
		Map<String, Object> process = this.plugin.process(action, context);
		
		assertEquals( "test_phase", process.get("phaseid"));
		
	}
	
}
