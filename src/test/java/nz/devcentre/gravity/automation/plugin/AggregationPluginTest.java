/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AggregationBuffer;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.model.Action;

@ExtendWith(MockitoExtension.class)
public class AggregationPluginTest {
	
	@Mock
	AggregationBuffer buffer;
	
	@Mock
	CardController cardController;
	
	@Mock
	VariableInterpreter variableInterpreter;
	
	@InjectMocks
	AggregationPlugin plugin;
	
	@BeforeEach
	public void setup() {
		System.out.println("Hello");
		//this.plugin.variableInterpreter =  new VariableInterpreter();
	}

	@Test
	public void testProcessNewCard() throws Exception {
		
		List<String> parameters = new ArrayList<String>();
		parameters.add("axisx");
		
		Map<String,String> properties = new HashMap<String,String>();
		properties.put("amount", "blag");
		
		Action action = new Action();
		action.setName("aggregation-action");
		action.setType("aggregation");
		action.setConfigField("resource","test_totals_board");
		action.setConfigField("properties",properties);
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("axisx", "aaaa");
		context.put("amount", "55");
		this.plugin.process(action, context);
		
		verify(cardController, times(1)).updateCard(anyString(), anyString(), any(), anyString());
		verify(buffer, times(1)).store(any());
	}

	@Test
	public void testProcessValueChanged() throws Exception {
		
		List<String> parameters = new ArrayList<String>();
		parameters.add("axisx");
		
		Map<String,String> properties = new HashMap<String,String>();
		properties.put("amount", "blag");
		
		Action action = new Action();
		action.setName("aggregation");
		action.setType("aggregation");
		action.setConfigField("resource","test_totals_board");
		action.setConfigField("properties",properties);
		action.setConfigField("parameters",parameters);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("axisx", "aaaa");
		context.put("amount", 55d);
		context.put("aggregation_axis", "aaaa_");
		context.put("aggregation_amount", 50d );
		
		this.plugin.process(action, context);
		
		verify(cardController, times(1)).updateCard(anyString(), anyString(), any(), anyString());
		verify(buffer, times(1)).store(any());
	}

	@Test
	public void testProcessAxisChanged() throws Exception {
		
		List<String> parameters = new ArrayList<String>();
		parameters.add("axisx");
		
		Map<String,String> properties = new HashMap<String,String>();
		properties.put("amount", "blag");
		
		Action action = new Action();
		action.setName("aggregation");
		action.setType("aggregation");
		action.setConfigField("resource","test_totals_board");
		action.setConfigField("properties",properties);
		action.setConfigField("parameters",parameters);

		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("axisx", "aaaa");
		context.put("amount", "55");
		context.put("aggregation_axis", "bbbb");
		context.put("aggregation_amount", 50d );
		this.plugin.process(action, context);
		
		verify(cardController, times(1)).updateCard(anyString(), anyString(), any(), anyString());
		verify(buffer, times(2)).store(any());
	}
}
