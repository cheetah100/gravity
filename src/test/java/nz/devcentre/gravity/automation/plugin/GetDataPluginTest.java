/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.services.QueryService;

@ExtendWith(MockitoExtension.class)
public class GetDataPluginTest {
	
	@InjectMocks
	private GetDataPlugin plugin;
	
	@Mock
	private QueryService queryService;
	
	@BeforeEach
	public void setUp() throws Exception {
		Card card = TestBoardTool.getTestCard("Timmy", "", 34);
		List<Card> data = new ArrayList<>();
		data.add(card);
		PaginatedCards pCards = new PaginatedCards();
		pCards.setData(data);
		when( queryService.query(any())).thenReturn(pCards);
	}

	@Test
	public void testProcess() throws Exception {
		
		Map<String,String> properties = new HashMap<>();
		Action action = ActionTestTool.newTestActionPP("get-data-action", 0, "getdata", 
				"test-board", "result", null, properties, null);
		
		Map<String,String> fields = new HashMap<>();
		fields.put("field", "field");
		action.setConfigField("fields", fields);
				
		Card testCard = TestBoardTool.getTestCard("Timmy", "", 34);
		List<Card> data = new ArrayList<>();
		data.add(testCard);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("data", data);
		Map<String, Object> process = this.plugin.process(action, context);		
		List result = (List) process.get("result");
		Assertions.assertNotNull( result );
		Assertions.assertTrue( result instanceof List);
		List<Card> cardList = (List<Card>)result;
		Assertions.assertEquals( 1, cardList.size());
	}	
}
