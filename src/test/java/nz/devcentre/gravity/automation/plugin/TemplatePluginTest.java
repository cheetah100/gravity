/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*; 

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
 
public class TemplatePluginTest {

	@Mock
	private ResourceController resourceController;

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		when(resourceController.getResource("test","test")).thenReturn("This is a test string that inserts here ->${replaceme}<- So do it!");
		when(resourceController.getResource("test1","test1")).thenReturn("Username->${credential_identifier},Password->${credential_secret},Location->${location}");
	}

	@Test
	public void TestPlugin() throws Exception {
		TemplatePlugin plugin = new TemplatePlugin();
		plugin.setResourceController(resourceController);
		
		Action action = ActionTestTool.newTestAction("template-action", 0, "template", 
				"test", "result", null);
				
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("replaceme", "Hello World");
		context.put("boardid", "test");
		
		plugin.process(action, context);
		Object result = context.get("result");
		
		assertNotNull(result);
		assertTrue( result instanceof String);
		String resultString = (String) result;
		assertEquals( "This is a test string that inserts here ->Hello World<- So do it!", resultString);
	}
	@Test
	public void TestCredentialAndProperty() throws Exception {
		TemplatePlugin plugin = new TemplatePlugin();
		plugin.setResourceController(resourceController);
		SecurityTool securityTool=Mockito.mock(SecurityTool.class);
		ReflectionTestUtils.setField(plugin, "securityTool", securityTool);
		
		Credential credential=new Credential();
		credential.setId("test_secure");
		credential.setIdentifier("James");
		credential.setSecret("Bond007");
		when(securityTool.getSecureCredential("test_secure")).thenReturn(credential);
		Map<String,String> properties=new LinkedHashMap<String, String>();
		properties.put("location","United Kingdom");
		properties.put("credential","test_secure");
		
		Action action = new Action();
		action.setConfigField("resource","test1");
		action.setConfigField("response","exampleResult");
		action.setConfigField("properties",properties);
		
		Map<String,Object> context = new HashMap<String,Object>();
		
		context.put("boardid", "test1");
		plugin.process(action, context);
		Object result = context.get("exampleResult");
		
		assertNotNull(result);
		assertTrue( result instanceof String);
		String resultString = (String) result;
		assertEquals( "Username->James,Password->Bond007,Location->United Kingdom", resultString);
	}
}
