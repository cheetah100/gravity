/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.CardService;

public class UniqueValidationPluginTest {

	@Mock
	private CardService cardService;
	
	@Mock
	UniqueValidationPlugin uniqueValidationPlugin;

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		ReflectionTestUtils.setField(this.uniqueValidationPlugin, "cardService",this.cardService);
		Mockito.when(this.uniqueValidationPlugin.process(Mockito.any(Action.class),Mockito.anyMap())).thenCallRealMethod();
	}

	@Test
	public void testProcess() throws Exception {
		Map<String, Object> context=new LinkedHashMap<>();
		context.put("cardid","101");
		context.put("boardid","test_board");
		List<String> parameters=new ArrayList<>();
		
		
		Action action=new Action();
		action.setType("unique");
		action.setConfigField("parameters",parameters);
		
		Map<String, Object> messages = new LinkedHashMap<String, Object>();
		Mockito.when(this.cardService.validateCardUniqueFields(Mockito.anyString(), Mockito.anyMap(), Mockito.anyString())).thenReturn(messages);
		
		Map<String, Object> process = this.uniqueValidationPlugin.process(action, context);
		
		assertEquals(context, process);
		
		Map<String, Object> fieldValues=new LinkedHashMap<>();
		fieldValues.put("service", "Security");
		fieldValues.put("quarter", "Q4");
		
		messages.put("Validation", fieldValues + " Combination Already Exists");
		
		parameters.addAll(fieldValues.keySet());
		context.putAll(fieldValues);
		process = this.uniqueValidationPlugin.process(action, context);
		
		assertTrue(context.containsKey("result"));
		assertEquals("{Validation={service=Security, quarter=Q4} Combination Already Exists}", process.get("result").toString());
		
	}

}
