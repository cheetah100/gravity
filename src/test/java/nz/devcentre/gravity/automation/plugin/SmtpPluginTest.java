/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.EmailService;

@ExtendWith(MockitoExtension.class)
public class SmtpPluginTest {
	
	@InjectMocks
	SmtpPlugin plugin;
	
	@Mock
	EmailService emailSender;
	
	@Mock
	SecurityTool securityTool;
	
	@Mock
	Credential credential;

	@Test
	public void testSmtpProcess() throws Exception {
				
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("subject", "subject");
		properties.put("from", "from@test.com");
		properties.put("to", "#to");
		
		Action action = ActionTestTool.newTestActionPP("smtp-action", 0, "smtp", 
				"body", "result", null, properties, null);
		
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("to", "jack@test.com");
		context.put("body", "This is a test email");
		
		plugin.process(action, context);
				
		verify(this.emailSender).sendEmail("subject", "This is a test email", "jack@test.com", null, 
				"from@test.com", "from@test.com", false, null, null);
	}
}
