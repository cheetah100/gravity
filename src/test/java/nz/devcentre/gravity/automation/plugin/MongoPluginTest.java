/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.DBCollection;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.managers.MongoTemplateManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class MongoPluginTest {
	
	@InjectMocks
	MongoPlugin mongoPlugin;
	
	@Mock
	private SecurityTool securityTool;
	
	@Mock
	private MongoTemplateManager mongoTemplateManager;
	
	@Mock
	private MongoTemplate template;
	
	@Mock
	private DBCollection collection;

	@Test
	public void testProcess() throws Exception {
		
		when(this.mongoTemplateManager.getTemplate(any())).thenReturn(this.template);
		when(this.template.getCollection("test")).thenReturn(this.collection);
		when(this.collection.getCount()).thenReturn(5l);
				
		Action action = ActionTestTool.newTestAction("Test", 0, "map", 
				"test", "result", null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		this.mongoPlugin.process(action, context);
		long count = (Long) context.get("count");
		assertEquals(5l, count);
		
	}
}
