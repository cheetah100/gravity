/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;
public class PropertyMapperPluginTest {

	@Mock
	VariableInterpreter interpreter;
	
	@Mock
	PropertyMapperPlugin propertyMapperPlugin;
	
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		ReflectionTestUtils.setField(this.propertyMapperPlugin, "interpreter", this.interpreter);
		Mockito.when(this.interpreter.resolve(Mockito.anyMap(), Mockito.anyString())).thenCallRealMethod();
		Mockito.when(this.propertyMapperPlugin.process(Mockito.any(Action.class), Mockito.anyMap())).thenCallRealMethod();
	}
	
	@Test
	public void test() throws Exception {
		
		Action action = ActionTestTool.newTestAction("property_map_test", 0, "property", 
			null, null, null);
		
		//Call Method withNull Context
		Map<String, Object> process = this.propertyMapperPlugin.process(action, null);
		Assertions.assertNull(process);
				
		Map<String,Object> context=new LinkedHashMap<String,Object>();
		//Call Method with empty Context
		process = this.propertyMapperPlugin.process(action, context);
		Assertions.assertEquals(context, process);
		
		context.put("name", "James Bond(bond007@gmail.com)");
		context.put("email", null);
		context.put("workLocation", "United Kingdom");
		context.put("baseLocation", null);
		
		//Call Method without Setting the Properties
		process = this.propertyMapperPlugin.process(action, context);
		Assertions.assertEquals(context, process);
		
		Map<String,String> properties=new LinkedHashMap<String,String>();
		action.setConfigField("properties",properties);
		
		//Call Method with empty Properties
		process = this.propertyMapperPlugin.process(action, context);
		Assertions.assertEquals(context, process);
		
		properties.put("email", "#name->.*\\(|\\).*");
		//field/key not present in context
		properties.put("unknown", "#unknown->.*\\(|\\).*");
		properties.put("baseLocation", "#workLocation");
		properties.put("invalidExp", "#workLocation->[");
		process = this.propertyMapperPlugin.process(action, context);
		Assertions.assertEquals("bond007@gmail.com", process.get("email"));
		Assertions.assertNull(process.get("unknown"));
		Assertions.assertEquals("United Kingdom", process.get("baseLocation"));
		Assertions.assertNull(process.get("invalidExp"));
	}

}
