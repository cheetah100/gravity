/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.controllers.TestBoardTool;
import nz.devcentre.gravity.model.Action;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import org.springframework.web.bind.annotation.ResponseBody;

public class ScriptPluginTest {

	
	@Test
	public void TestPlugin() throws Exception {

		final Action action = ActionTestTool.newTestActionPP("javascript-test-action", 0, "script", 
				"a=5.5;answer=x+y+a;", "", null, new HashMap<String, String>(), new ArrayList<String>());
		
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("boardid", TestBoardTool.BOARD_ID);
		context.put("x", 10);
		context.put("y", 15);

		Map<String, Object> process = getPlugin().process(action, context);
		Object result = process.get("answer");
		Assertions.assertNotNull(result);
		Assertions.assertTrue( result.equals( new Double(30.5)));
	}
	
	@Test
	public void TestMapTypeConversion() throws Exception {
		
		final Action action = ActionTestTool.newTestActionPP("javascript-test-action", 0, "script", 
				"map = {\"f1\":\"v1\",\"f2\":\"v2\"};", "", null, new HashMap<String, String>(), new ArrayList<String>());

		Map<String, Object> context = new HashMap<String, Object>();
		context.put("boardid", TestBoardTool.BOARD_ID);
		
		Map<String, Object> process = getPlugin().process(action, context);
		
		Object result = process.get("map");
		Assertions.assertNotNull(result);
		Assertions.assertTrue( result.equals( getFields() ));
	}
	
	@Test
	public void TestListTypeConversion() throws Exception {
		
		final Action action = ActionTestTool.newTestActionPP("javascript-test-action", 0, "script", 
				"list = [\"val1\",\"val2\"];", "", null, new HashMap<String, String>(), new ArrayList<String>());

		Map<String, Object> context = new HashMap<String, Object>();
		context.put("boardid", TestBoardTool.BOARD_ID);
		
		Map<String, Object> process = getPlugin().process(action, context);
		
		Object result = process.get("list");
		Assertions.assertNotNull(result);
		Assertions.assertTrue( result.equals( getList() ));
	}
	
	private Map<String,Object> getFields() {
		Map<String, Object> newMap = new HashMap<String,Object>();
		newMap.put("f1", "v1");
		newMap.put("f2", "v2");
		return newMap;
	}
	
	private List<String> getList() {
		List<String> list = new ArrayList<String>();
		list.add("val1");
		list.add("val2");
		return list;
	}
	
	
	@Test
	public void TestSetTrackingUrlPlugin() throws Exception {
		
		final Action action = ActionTestTool.newTestActionPP("javascript-test-action", 0, "script", 
				null, "cpe_tracking_url='http://trackandtrace.courierpost.co.nz/search/'+cpe_tracking_id;", null, new HashMap<String, String>(), new ArrayList<String>());
		
		
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("cpe_tracking_id", 15);
		
		Map<String, Object> process = getPlugin().process(action, context);
		Object result = process.get("cpe_tracking_url");
		Assertions.assertEquals("http://trackandtrace.courierpost.co.nz/search/15", ""+result);
	}

	private ScriptPlugin getPlugin() {
		ScriptPlugin plugin = new ScriptPlugin();
		
		ResourceController resourceController = new DummyResourceController();
		plugin.setResourceController(resourceController );
		
		return plugin;
	}
	
	private static class DummyResourceController extends ResourceController{
		
		public @ResponseBody String getResource(String boardId, String resourceId) throws Exception {
			return resourceId;
		}
	}
}
