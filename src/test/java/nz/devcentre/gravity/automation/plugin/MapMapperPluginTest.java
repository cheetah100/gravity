/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;

import org.junit.jupiter.api.Test;

public class MapMapperPluginTest {

	@Test
	public void TestPlugin() throws Exception {
		
		MapMapperPlugin plugin = new MapMapperPlugin();
				
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", "firstname");
		properties.put("account", "number");
		
		
		Action action = ActionTestTool.newTestActionPP("map-test-action", 0, "map", 
				null, "result", null, properties, null);
		
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("firstname", "Peter");
		context.put("number", "42");
		
		Map<String, Object> process = plugin.process(action, context);
		Object result = process.get("result");
		
		assertTrue( result instanceof Map);
		
		Map<String,Object> testMap = (Map<String,Object>) result;
		
		assertEquals("Peter", testMap.get("name"));
		assertEquals("42", testMap.get("account"));

	}

	@Test
	public void TestPluginAndMarshall() throws Exception {
		
		MapMapperPlugin plugin = new MapMapperPlugin();
				
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", "firstname");
		properties.put("account", "number");
		
		Action action = ActionTestTool.newTestActionPP("map-test-action", 0, "map", 
				"data", "result", "xml", properties, null);
		
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("firstname", "Peter");
		context.put("number", "42");
		
		Map<String, Object> process = plugin.process(action, context);
		Object result = process.get("result");
		
		assertTrue( result instanceof String);
		
		String testString = (String) result;
		
		assertEquals("<data><name>Peter</name><account>42</account></data>", testString);
	}

	
}
