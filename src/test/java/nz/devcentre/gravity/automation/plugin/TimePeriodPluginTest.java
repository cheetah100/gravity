/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.security.SecurityTool;

public class TimePeriodPluginTest {
	
	@Mock
	private TimePeriodPlugin plugin;
	
	@Mock
	private BoardController boardController;
	
	@Mock
	private SecurityTool securityTool;
	
	@BeforeEach
	public void setUp() throws Exception {
		this.plugin=Mockito.mock(TimePeriodPlugin.class);
		Mockito.when(this.plugin.process(Mockito.any(Action.class), Mockito.anyMap())).thenCallRealMethod();
		this.boardController=Mockito.mock(BoardController.class);
		this.securityTool=Mockito.mock(SecurityTool.class);
		ReflectionTestUtils.setField(this.plugin, "securityTool", this.securityTool);
		ReflectionTestUtils.setField(this.plugin, "boardController", this.boardController);
		Mockito.doNothing().when(this.securityTool).iAmSystem();
	}
	
	@Test
	public void testProcess() throws Exception {
		
		Action action = ActionTestTool.newTestActionPP("test-action", 0, "timeperiod", 
				"fiscal_quarter", "result", null,ImmutableMap.of("startDate","startDate","endDate","endDate"), 
				Arrays.asList(new String[] {"name"}));
		
		
		Mockito.when(this.boardController.getBoard("fiscal_quarter")).thenReturn(this.dummyBoard());
		
		Card card=new Card();
		card.setId("Q1");
		card.setFields(ImmutableMap.of("startDate","2020-01-01","endDate","2020-03-31","name","FY20Q1"));
		List<Card> cards=new ArrayList<>();
		cards.add(card);
		Mockito.when(this.boardController.search(Mockito.anyString(), Mockito.any(Filter.class), Mockito.anyString(),
				Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyBoolean()))
				.thenReturn(cards);

		Map<String, Object> context = this.plugin.process(action, new HashMap<>());
		
		assertEquals("Q1", context.get("test_id").toString());
		assertEquals("FY20Q1", context.get("test_name").toString());
		
		action.setConfigField("response", "current_quarter");
		context = this.plugin.process(action, new HashMap<>());
		
		assertEquals("Q1", context.get("current_quarter_id").toString());
		assertEquals("FY20Q1", context.get("current_quarter_name").toString());
		
		action.setConfigField("parameters", Arrays.asList(new String[] {}));
		context = this.plugin.process(action, new HashMap<>());
		
		assertEquals("Q1", context.get("current_quarter_id").toString());
		assertFalse(context.containsKey("current_quarter_name"));
	}
	@Test
	public void testProcessException() throws Exception {
		
		Action action = ActionTestTool.newTestActionPP("test-action", 0, "timeperiod", 
				"fiscal_quarter", null, null,ImmutableMap.of("startDate","startDate","endDate","endDate"), 
				Arrays.asList(new String[] {"name"}));
		
		Mockito.when(this.boardController.getBoard("fiscal_quarter")).thenReturn(this.dummyBoard());
		this.plugin.process(action, new HashMap<>());
	}
	
	@Test
	public void testProcessException1() throws Exception {
		
		Action action = ActionTestTool.newTestActionPP("test-action", 0, "timeperiod", 
				"fiscal_quarter", null, null,null, 
				Arrays.asList(new String[] {"name"}));
		
		
		Mockito.when(this.boardController.getBoard("fiscal_quarter")).thenReturn(this.dummyBoard());
		Assertions.assertThrows(ValidationException.class, () -> {
			this.plugin.process(action, new HashMap<>());
		}, "ValidationException was expected");		
	}
	
	@Test
	public void testProcessException2() throws Exception {
		
		Action action = ActionTestTool.newTestActionPP("test-action", 0, "timeperiod", 
				"fiscal_quarter", null, null,ImmutableMap.of("startDate","startDate","name","endDate"), 
				Arrays.asList(new String[] {"name"}));
		
		Mockito.when(this.boardController.getBoard("fiscal_quarter")).thenReturn(this.dummyBoard());
		
		Assertions.assertThrows(ValidationException.class, () -> {
			this.plugin.process(action, new HashMap<>());
		}, "ValidationException was expected");
	}
	
	@Test
	public void testProcessException3() throws Exception {
		
		Action action = ActionTestTool.newTestActionPP("test-action", 0, "timeperiod", 
				"fiscal_quarter", null, null,ImmutableMap.of("startDate","","startDate",""), 
				Arrays.asList(new String[] {"name"}));
		
		Mockito.when(this.boardController.getBoard("fiscal_quarter")).thenReturn(this.dummyBoard());
		Assertions.assertThrows(ValidationException.class, () -> {
			this.plugin.process(action, new HashMap<>());
		}, "ValidationException was expected");		
	}
	
	private Board dummyBoard() {
		String boardId="fiscal_quarter";
		Board board=new Board();
		board.setId(boardId);
		board.setName(boardId);
				
		Map<String,TemplateField> fields=new HashMap<>();
		fields.put("startDate", this.getField("startDate", FieldType.DATE));
		fields.put("endDate", this.getField("endDate", FieldType.DATE));
		fields.put("name", this.getField("name", FieldType.STRING));
		board.setFields(fields);		
		return board;
	}
	
	private TemplateField getField(String id,FieldType type ) {
		TemplateField field=new TemplateField();
		field.setId(id);
		field.setName(id);
		field.setType(type);
		return field;
	}
}
