/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */


package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;

public class JsonParserPluginTest {

	@Test
	public void TestBasicJsonParse() throws Exception {
		
		JsonParserPlugin plugin = new JsonParserPlugin();
				
		Map<String, String> properties = new HashMap<String, String>();
		
		Action action = ActionTestTool.newTestActionPP("json-test-action", 0, "jsonparser", 
				"param1", "result", null, properties, null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("param1", "{ 'name':'Peter', 'number':'42'}");
			
		Map<String, Object> process = plugin.process(action, context);
		
		String name = (String) process.get("name");
		String number = (String) process.get("number");
		
		Assertions.assertEquals( "Peter", name);
		Assertions.assertEquals( "42", number);
		
	}
	
	@Test
	public void TestParameterGetting() throws Exception {
		
		JsonParserPlugin plugin = new JsonParserPlugin();
						
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("road_name", "address.name");

		Action action = ActionTestTool.newTestActionPP("json-test-action", 0, "jsonparser", 
				"param1", "result", null, properties, null);

		Map<String,Object> context = new HashMap<String,Object>();
		context.put("param1", "{ 'name':'Peter', 'number':'42', 'address' : { 'name':'Sunset'}}");
			
		Map<String, Object> process = plugin.process(action, context);
		
		String roadname = (String) process.get("road_name");
		
		Assertions.assertEquals( "Sunset", roadname);
	}
	
	@Test
	public void TestGetObject() throws Exception {
		
		Map<String,Object> map1 = new HashMap<>();
		Map<String,Object> map2 = new HashMap<>();
		Map<String,Object> map3 = new HashMap<>();
		
		map3.put("number", "34");
		map3.put("name", "Sunset");
		map3.put("type", "Road");
		map2.put("address", map3);
		map1.put("customer", map2);
		
		Object object = JsonParserPlugin.getObject("customer.address.name", map1);
		
		Assertions.assertEquals("Sunset", object);
	}
	
	
}
