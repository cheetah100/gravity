package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;

@ExtendWith(MockitoExtension.class)
public class JmsPluginTest {
	
	@Mock
	private RabbitTemplate rabbitTemplate;
	
	@Mock
	FanoutExchange gravityEventsExchange;
	
	@InjectMocks
	JmsPlugin plugin;

	@Test
	public void testProcess() throws Exception {
		Map<String, String> properties = new HashMap<>();
		properties.put("test1", "test");
		properties.put("test2", "test");
		
		Action action = ActionTestTool.newTestActionPP("test", 0, "jms", 
				"queueName", null, "method", properties, null);
		
		Map<String, Object> context = new HashMap<>();
		context.put("method", "testBody");
		
		plugin.process(action, context);
		
		// Verify that the send method was called
		ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);
		verify(rabbitTemplate).send(any(), argument.capture());
		
		// Check that the message that was sent is correct
		Message sentMessage = argument.getValue();
		
		// Check that the headers are correct
		Assertions.assertEquals("test", sentMessage.getMessageProperties().getHeaders().get("test1"));
		Assertions.assertEquals("test", sentMessage.getMessageProperties().getHeaders().get("test2"));
		
		// Check that the body is correct
		Assertions.assertEquals("testBody", new String(sentMessage.getBody()));
	}
}
