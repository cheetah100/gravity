/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.ActionTestTool;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.services.CardService;

@ExtendWith(MockitoExtension.class)
public class GetCardPluginTest {
	
	@InjectMocks
	private GetCardPlugin plugin;
	
	@Mock
	private CardService cardService;
	
	@BeforeEach
	public void setUp() throws Exception {
		Card card = new Card();
		card.setId("test_card");
		Map<String,Object> fields = new HashMap<>();
		fields.put("field", "value");
		card.setFields(fields);
		
		when( cardService.getCard(anyString(), anyString())).thenReturn(card);
	}

	@Test
	public void testProcessOldStyleConfig() throws Exception {
		
		
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("localfield", "test_local_field");

		Action action = ActionTestTool.newTestActionPP("get-card-action", 0, "getcard", 
				"test-board", "result", "field", properties, null);
		
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("test_local_field", "test_card");
			
		Map<String, Object> process = this.plugin.process(action, context);
		
		String result = (String) process.get("result");
		verify(cardService, times(1)).getCard("test-board","test_card");
		Assertions.assertEquals( result,  "value");
	}

	@Test
	public void testProcessNewUIStyleConfig() throws Exception {
		Action action = new Action();
		action.setName("get-card-action");
		action.setType("getcard");
		
		Map<String,Object> config = new HashMap<>();
		config.put("resource", "test-board");
		config.put("response", "result");
		config.put("method", "field");
		config.put("resource", "test-board");
		config.put("localfield", "test_local_field");
		config.put("properties", new HashMap<String,String>());
		action.setConfig(config);
	
		Map<String,Object> context = new HashMap<String,Object>();
		context.put("test_local_field", "test_card");
		
		Map<String, Object> process = this.plugin.process(action, context);
		String result = (String) process.get("result");
		verify(cardService, times(1)).getCard("test-board", "test_card");
		Assertions.assertEquals( result, "value");
	}
}
