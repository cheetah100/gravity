/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(SpringExtension.class)
public class AggregationBufferTest {
	
	@Mock
	CardController cardController;
	
	@Mock
	Card card;
	
	@Mock
	SecurityTool securityTool;
	
	@InjectMocks
	AggregationBuffer buffer;

	@Test
	public void testStore() {
		
		BufferElement testBufferElement = getTestBufferElement( "S01", "Q01", 10);
		Assertions.assertEquals( "test_board_S01_Q01_", testBufferElement.getId());
		
		buffer.store(testBufferElement);
		buffer.store(getTestBufferElement( "S01", "Q02", 20));
		buffer.store(getTestBufferElement( "S01", "Q03", 30));

		buffer.store(getTestBufferElement( "S01", "Q02", 50));
		buffer.store(getTestBufferElement( "S01", "Q03", -10));
		
		BufferElement noChange = buffer.getBuffer().get("test_board_S01_Q01_");
		BufferElement addition = buffer.getBuffer().get("test_board_S01_Q02_");
		BufferElement subtraction = buffer.getBuffer().get("test_board_S01_Q03_");
		
		Assertions.assertEquals( 10, noChange.getValues().get("total").doubleValue());
		Assertions.assertEquals( 70, addition.getValues().get("total").doubleValue());
		Assertions.assertEquals( 20, subtraction.getValues().get("total").doubleValue());
	}
	
	@Test
	public void testProcessNewCard() throws Exception {
		when(cardController.getCard2("test_board", "test_board_S02_Q01_")).thenThrow(ResourceNotFoundException.class);
		buffer.store(getTestBufferElement( "S02", "Q01", 50));
		buffer.process();
		verify(cardController, times(1)).createCard(anyString(), any());
	}
	
	@Test
	public void testProcessExistingCard() throws Exception {
		Map<String, Object> fields = new HashMap<String,Object>();
		fields.put("total", 10);
		when(card.getId()).thenReturn("test_board_S02_Q01_");
		when(card.getFields()).thenReturn(fields);
		when(card.getField("total")).thenReturn(10);
		when(cardController.getCard2("test_board", "test_board_S02_Q01_")).thenReturn( this.card);
		buffer.store(getTestBufferElement( "S02", "Q01", 50));
		buffer.process();
		verify(cardController, times(1)).updateCard(anyString(), anyString(), any(), anyString());
	}
	
	private BufferElement getTestBufferElement( String service, String quarter, Number value) {
		Map<String,String> axisFields = new HashMap<String,String>();
		axisFields.put("service", service);
		axisFields.put("quarter", quarter);		
		Map<String,Number> totalFields = new HashMap<String,Number>();
		totalFields.put("total", value);
		return new BufferElement( "test_board", axisFields, totalFields);
	}
}
