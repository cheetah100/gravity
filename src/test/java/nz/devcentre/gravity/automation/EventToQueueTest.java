/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.model.CardHolder;

@ExtendWith(MockitoExtension.class)
public class EventToQueueTest {
	
	@InjectMocks
	EventToQueue etq;
	
	@Mock
	private CardListener listener;
	
	@Mock
	private RabbitTemplate rabbitTemplate;
	
	@Mock
	FanoutExchange gravityEventsExchange;

	@Test
	public void testCheckEventsPaused() {
		etq.setAction(EngineState.PAUSE);
		etq.checkEvents();
	}

	@Test
	public void testCheckEventsStopped() {
		etq.setAction(EngineState.STOP);
		etq.checkEvents();
	}
	
	@Test
	public void testCheckEventsStarted() {
		
		CardHolder ch = new CardHolder("board","card");
		Set<CardHolder> chs = new HashSet<CardHolder>();
		chs.add(ch);
		when(this.listener.getCardSet()).thenReturn(chs);
		when(this.gravityEventsExchange.getName()).thenReturn("testExchange");
		
		etq.setAction(EngineState.START);
		etq.checkEvents();
		verify(rabbitTemplate).convertAndSend(anyString(),anyString(),isA(CardHolder.class));
	}
	
	@Test
	public void testSetAction() {
		etq.setAction(EngineState.PAUSE);
		EngineState engineState = etq.getEngineState();
		Assertions.assertNotNull(engineState);
		Assertions.assertEquals(EngineState.PAUSE, engineState);
	}

}
