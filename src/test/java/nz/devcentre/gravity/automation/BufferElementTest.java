/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class BufferElementTest {
	
	@Test
	public void testAdd() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38d);
		Assertions.assertEquals( "test_board_blah_", bufferElement.getId());
		
		BufferElement bufferElement2 = newBufferElement("test_board", "blah", 50d);
		bufferElement.add(bufferElement2);
		Assertions.assertEquals(88d, bufferElement.getValues().get("amount"));	
	}

	@Test
	public void testGetId() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38);
		Assertions.assertEquals( "test_board_blah_", bufferElement.getId());
	}

	@Test
	public void testGetAxisId() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38);
		Assertions.assertEquals( "blah_", bufferElement.getAxisId());
	}

	@Test
	public void testGetFields() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38);
		Assertions.assertTrue( bufferElement.getFields().containsKey("xaxis"));
	}

	@Test
	public void testGetValues() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38);
		Assertions.assertTrue( bufferElement.getValues().containsKey("amount"));
	}

	@Test
	public void testGetBoardId() {
		BufferElement bufferElement = newBufferElement("test_board", "blah", 38);
		Assertions.assertEquals( "test_board", bufferElement.getBoardId());
	}
	
	public static  BufferElement newBufferElement(String boardId, String xaxis, Number value) {
		
		Map<String, String> fields = new HashMap<String,String>();
		fields.put("xaxis", xaxis);
		
		Map<String, Number> values = new HashMap<String,Number>();
		values.put("amount", value);
		
		BufferElement bufferElement = new BufferElement(boardId, fields, values);
		
		return bufferElement;
	}

}
