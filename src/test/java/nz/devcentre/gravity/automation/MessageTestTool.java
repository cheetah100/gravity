package nz.devcentre.gravity.automation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class MessageTestTool {
	public static byte[] serialObject(Object o) throws IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bout); 
        out.writeObject(o); 
        out.close();
        bout.close();
        return bout.toByteArray();
	}
}
