/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nz.devcentre.gravity.model.Action;

public class ActionTestTool {
	
	public static  Action newTestAction( String name, int order, String type, String resource, String response, String method) {
		Action action = new Action();
		action.setName(name);
		action.setType(type);
		action.setOrder(order);
		
		Map<String,Object> config = new HashMap<>();
		config.put("resource", resource);
		config.put("response", response);
		config.put("method", method);
		
		action.setConfig(config);
		
		return action;
	}

	public static  Action newTestActionPP( String name, 
			int order, 
			String type, 
			String resource, 
			String response, 
			String method,
			Map<String,String> properties,
			List<String> parameters) {
		
		Action action = newTestAction(name, order, type, resource, response, method);
		
		action.setConfigField("parameters", parameters);
		action.setConfigField("properties", properties);		
		return action;
	}
}
