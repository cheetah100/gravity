package nz.devcentre.gravity.security;

import static org.junit.jupiter.api.Assertions.*;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import org.junit.jupiter.api.Test;

import nz.devcentre.gravity.model.User;

public class UserAttributesMapperTest {

	@Test
	public void test() {
		try {
				UserAttributesMapper attributesMapper = new UserAttributesMapper();
				Attributes attr = new BasicAttributes();
				attr.put("cn", "bond");
				attr.put("name", "bond");
				attr.put("givenName", "James");
				attr.put("sn", "Bond");
				attr.put("mail", "bond@cisco.com");
				attr.put("l", "Aska");
				attr.put("description", "james bond");
				attr.put("whenCreated", "20010817020049.0Z");
				attr.put("whenChanged", "20200930193627.0Z");
				attr.put("manager", "CN=joker,OU=Employees,OU=Cisco Users,DC=cisco,DC=com");
				attr.put("title", "Engineer - IT");
				attr.put("ciscoITDescription", "CAST-UDT-IT Bus Mgmt-OPEX-U");
				attr.put("employeeType", "Vendor");
				attr.put("st", "Odisha");
				attr.put("co", "India");
	
				User mapFromAttributes = attributesMapper.mapFromAttributes(attr);
				assertTrue(mapFromAttributes.getId().equals("bond"));
				
				assertTrue(mapFromAttributes.getFullName().equalsIgnoreCase("James Bond"));
				
				Attributes attr1 = new BasicAttributes();
				User mapFromAttributes1 = attributesMapper.mapFromAttributes(attr1);
				assertFalse(mapFromAttributes1.getId().equals("bond"));
				
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
