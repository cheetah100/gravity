/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.SecretKey;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import nz.devcentre.gravity.controllers.CredentialCache;
import nz.devcentre.gravity.controllers.TeamCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.View;

@ExtendWith(SpringExtension.class)
public class SecurityToolTest {
	
	@InjectMocks
	SecurityTool securityTool;
	
	@Mock
	TeamCache teamCache;
	
	@Mock
	CredentialCache credentialCache;

	@BeforeEach
	public void setUp() throws Exception {
		ReflectionTestUtils.setField(securityTool, "sysKeystore", "./src/main/webapp/WEB-INF/testkeystore.jks");
		ReflectionTestUtils.setField(securityTool, "sysKeystorePass", "password");
	}

	@Test
	public void testGetCurrentUserAndIAmSystem() throws Exception {
		securityTool.iAmSystem();
		String currentUser = SecurityTool.getCurrentUser();
		assertEquals("system", currentUser);
	}

	@Test
	public void testIsAuthorisedIsTrue() throws Exception {
		securityTool.iAmSystem();
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("system", "WRITE");
		assertTrue(securityTool.isAuthorised(permissions, "READ,WRITE"));
	}
	
	@Test
	public void testIsAuthorisedIsFalse() throws Exception {
		securityTool.iAmSystem();
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("admin", "WRITE");
		assertFalse(securityTool.isAuthorised(permissions, "READ,WRITE"));
	}

	@Test
	public void testGetRoles() throws Exception {
		Map<String,String> teamList = new HashMap<String,String>();
		teamList.put("test_team", "Test Team");
		when(teamCache.list()).thenReturn(teamList);
		
		Map<String,String> members = new HashMap<String,String>();
		members.put("system", "test_role");
		
		Team team = new Team();
		team.setId("test_team");
		team.setName("Test Team");
		team.setMembers(members);
		
		when(teamCache.getItem("test_team")).thenReturn(team);
		
		List<GrantedAuthority> roles = securityTool.getPermissions("system");
		GrantedAuthority authority = roles.iterator().next();
		
		assertTrue( authority instanceof TeamAuthority);
		
		TeamAuthority teamAuthority = (TeamAuthority) authority;
		
		assertEquals("test_team", teamAuthority.getTeam());
		assertEquals("test_role", teamAuthority.getRole());
	}

	@Test
	public void testInitRole() {
		
		Map<String,String> permissions = new HashMap<String,String>();
		permissions.put("test_user", "test_role");
		
		Map<String, String> initPermissions = securityTool.initPermissions(permissions);
		
		assertTrue(initPermissions.containsKey("administrators"));
		assertTrue(initPermissions.containsKey("system"));
	}

	@Test
	public void testGetCurrentAuthentication() throws Exception {
		securityTool.iAmSystem();
		Authentication currentAuthentication = SecurityTool.getCurrentAuthentication();
		Collection<? extends GrantedAuthority> authorities = currentAuthentication.getAuthorities();
		assertTrue(authorities.size() == 0);
	}

	@Test
	public void testIsAuthorizedViewAndFilter() throws Exception {
		
		Map<String,String> filterPermissions = new HashMap<String,String>();
		filterPermissions.put("system", "WRITE");
		
		Filter filter = new Filter();
		filter.setId("test_filter");
		filter.setPermissions(filterPermissions);
		
		Map<String,Filter> filters = new HashMap<String,Filter>();
		filters.put("test_filter", filter);

		Map<String,String> viewPermissions = new HashMap<String,String>();
		viewPermissions.put("system", "WRITE");
		
		View view = new View();
		view.setId("test_view");
		view.setPermissions(viewPermissions);
		
		Map<String,View> views = new HashMap<String,View>();
		views.put("test_view", view);
		
		Map<String,String> boardPermissions = new HashMap<String,String>();
		viewPermissions.put("administrators", "ADMIN");

		Board board = new Board();
		board.setFilters(filters);
		board.setViews(views);
		board.setPermissions(boardPermissions);
		
		boolean authorised
			= securityTool.isAuthorizedViewAndFilter("test_view", null, board, "ADMIN,WRITE");
		
		assertTrue(authorised);
	}
	
	@Test
	public void testIsAuthorizedViewWithNoFilter() throws Exception {
		
		Map<String,String> filterPermissions = new HashMap<String,String>();
		filterPermissions.put("system", "WRITE");

		Map<String,String> viewPermissions = new HashMap<String,String>();
		viewPermissions.put("system", "WRITE");
		
		View view = new View();
		view.setId("test_view");
		view.setPermissions(viewPermissions);
		
		Map<String,View> views = new HashMap<String,View>();
		views.put("test_view", view);
		
		Map<String,String> boardPermissions = new HashMap<String,String>();
		viewPermissions.put("administrators", "ADMIN");

		Board board = new Board();
		board.setViews(views);
		board.setPermissions(boardPermissions);
		
		boolean authorised
			= securityTool.isAuthorizedViewAndFilter("test_view", null, board, "ADMIN,WRITE");
		
		assertTrue(authorised);
	}
	
	@Test
	public void testEncryptAndDecrypt() throws Exception {
		String encrypted = securityTool.encrypt("Test Message");
		assertNotEquals(encrypted, "Test Message" );
		String decrypted = securityTool.decrypt(encrypted);
		assertEquals(decrypted, "Test Message" );
	}
	
	@Test
	public void testHash() throws Exception {
		String hash = securityTool.hash("Test Message");
		assertNotNull(hash);
		
		String hash2 = securityTool.hash("Test Message");
		assertNotNull(hash2);
		
		assertEquals( hash, hash2);
	}
	
	@Test
	public void testGetSecureCredential() throws Exception {
		Credential cred = new Credential();
		cred.setId("test");
		cred.setIdentifier(securityTool.encrypt("test_id"));
		cred.setSecret(securityTool.encrypt("test_secret"));
		when(credentialCache.getItem("test")).thenReturn(cred);
		Credential secureCredential = securityTool.getSecureCredential("test");
		assertNotNull(secureCredential);
		assertEquals(secureCredential.getIdentifier(),"test_id");
		assertEquals(secureCredential.getSecret(),"test_secret");
	}

	@Test
	public void testGetSystemKey() throws Exception {
		SecretKey systemKey = this.securityTool.getSystemKey();
		assertNotNull( systemKey);
		
		System.out.println("Alg: " + systemKey.getAlgorithm());
		System.out.println("Format: " + systemKey.getFormat());
		System.out.println("Length: " + systemKey.getEncoded().length * 8);
	}

	@Test
	public void testGetSystemKeyBadPath() throws Exception {
		Assertions.assertThrows(FileNotFoundException.class, () -> {
			ReflectionTestUtils.setField(securityTool, "sysKeystore", "badpath.jks");
			this.securityTool.getSystemKey();
		}, "FileNotFoundException was expected");
	}
	
	@Test
	public void testGetSystemKeyBadPassword() throws Exception {
		Assertions.assertThrows(java.io.IOException.class, () -> {
			ReflectionTestUtils.setField(securityTool, "sysKeystorePass", "bad_password");
			this.securityTool.getSystemKey();
		}, "java.io.IOException was expected");
	}
	
	@Test
	public void testIAm() throws Exception {
		this.securityTool.iAm("jim");
		String currentUser = SecurityTool.getCurrentUser();
		assertEquals( currentUser,"jim");
	}
	
	@Test
	public void testIAmSystem() throws Exception {
		this.securityTool.iAmSystem();
		String currentUser = SecurityTool.getCurrentUser();
		assertEquals( currentUser,"system");
	}
	
	@Test
	public void testAccessToKeystore() throws Exception {
		try(FileInputStream inputStream = new FileInputStream(new File("./src/main/webapp/WEB-INF/testkeystore.jks"))) {
			KeyStore ks = KeyStore.getInstance("JCEKS"); 
			ks.load(inputStream, "password".toCharArray());
			
			Enumeration<String> aliases = ks.aliases();
			
			while(aliases.hasMoreElements()) {
				String alias = aliases.nextElement();
				System.out.println(alias);
			}
		}
	}
}
