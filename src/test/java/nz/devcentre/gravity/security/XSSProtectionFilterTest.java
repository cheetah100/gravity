/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import nz.devcentre.gravity.security.xss.CssValidator;
import nz.devcentre.gravity.security.xss.XSSProtectionFilter;

public class XSSProtectionFilterTest {

	@Test
	public void xssProtectionFilterTest() throws IOException {
		XSSProtectionFilter filter = new XSSProtectionFilter();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message","jsContent","<script></script><h1>testing html tag</h1>"));
		MockHttpServletRequest mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod(HttpMethod.POST.name());
		mockRequest.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mockRequest.setContent(jsonString.getBytes());
		mockRequest.addParameter("test", "<script></script><h1>testing html tag</h1>");
		mockRequest.addParameter("testJs", "document.getElementById('test')");

		HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
		FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
		try {
			filter.doFilter(mockRequest, mockResponse, mockFilterChain);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void xssProtectionFilterTest1() throws IOException {
		XSSProtectionFilter filter = new XSSProtectionFilter();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message","jsContent","<script></script><h1>testing html tag</h1>"));
		MockHttpServletRequest mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod(HttpMethod.POST.name());
		mockRequest.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mockRequest.setContent(jsonString.getBytes());
		
		HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
		FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
		try {
			filter.doFilter(mockRequest, mockResponse, mockFilterChain);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void xssProtectionFilterTest2() throws IOException {
		XSSProtectionFilter filter = new XSSProtectionFilter();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message"));
		MockHttpServletRequest mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod(HttpMethod.POST.name());
		mockRequest.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mockRequest.setContent(jsonString.getBytes());
		
		HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
		FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
		try {
			filter.doFilter(mockRequest, mockResponse, mockFilterChain);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void xssProtectionFilterTest3() throws IOException {
		XSSProtectionFilter filter = new XSSProtectionFilter();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message"));
		MockHttpServletRequest mockRequest = new MockHttpServletRequest();
		mockRequest.setMethod(HttpMethod.POST.name());
		mockRequest.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		mockRequest.setContent(jsonString.getBytes());
		
		HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
		FilterChain mockFilterChain = Mockito.mock(FilterChain.class);
		try {
			filter.doFilter(mockRequest, mockResponse, mockFilterChain);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void isValidCssValidatorTest() throws JsonProcessingException {
		CssValidator validator=new CssValidator();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message","jsContent","<script></script><h1>testing html tag</h1>"));
		boolean valid = validator.isValid(jsonString, Mockito.mock(ConstraintValidatorContext.class));
		assertFalse(valid);
		
		jsonString = mapper.writeValueAsString(ImmutableMap.of("message", "success message"));
		valid = validator.isValid(jsonString, Mockito.mock(ConstraintValidatorContext.class));
		assertTrue(valid);
	}
}
