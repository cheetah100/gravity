/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;

import nz.devcentre.gravity.controllers.Cache;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Board;

public class GravityPermissionEvaluatorTest {
	
	private GravityPermissionEvaluator evaluator;
	
	@Mock
	Authentication authentication;
	
	@Mock
	CacheManager cacheManager;
	
	@Mock
	Cache cache;
	
	@Mock
	Board board;
	
	Map<String,String> permissions;

	@BeforeEach
	public void setUp() throws Exception {
		
		this.permissions = new HashMap<String,String>();
		this.permissions.put("admin", "WRITE");
		
		MockitoAnnotations.initMocks(this);
		when(cacheManager.getRegistered("BOARD")).thenReturn(this.cache);
		when(cache.getItem("test_object")).thenReturn(board);
		when(board.getPermissions()).thenReturn(this.permissions);
		when(authentication.getPrincipal()).thenReturn(
				new DefaultOidcUser(new ArrayList<GrantedAuthority>(), new OidcIdToken(null, null, null, null), "admin"));
		
		this.evaluator = new GravityPermissionEvaluator();
		this.evaluator.cacheManager = this.cacheManager;
		
		iAmAdmin();
	}

	@Test
	public void testHasPermissionAuthenticationObjectObject() {
		boolean hasPermission = evaluator.hasPermission(authentication, "test_object", "ADMIN");
		assertFalse(hasPermission);
	}

	@Test
	public void testHasPermissionAuthenticationSerializableStringObject() {
		boolean hasPermission = evaluator.hasPermission(authentication, "test_object", "BOARD", "WRITE");
		assertTrue(hasPermission);
	}

	@Test
	public void testGetPermissionsById() throws Exception {
		Map<String, String> permissionsById = evaluator.getPermissionsById("BOARD", "test_object");
		assertEquals(1, permissionsById.size());
		assertEquals("WRITE", permissionsById.get("admin"));
	}
	
	public void iAmAdmin() throws Exception {
		SecurityContext context = SecurityContextHolder.getContext();
		Collection<TeamAuthority> authorities = new ArrayList<TeamAuthority>();
		authorities.add(new TeamAuthority("test_team", ""));
		
		UsernamePasswordAuthenticationToken authentication = 
			new UsernamePasswordAuthenticationToken("admin", "", authorities);
		
		context.setAuthentication(authentication);
	}

}
