/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class MongoTemplateManagerTest {

	@InjectMocks
	private MongoTemplateManager mongoTemplateManager;
	
	@Mock
	private Credential credential;
	
	@Mock
	private SecurityTool securityTool;
	
	@Test
	public void testGetTemplate() throws Exception {
		
		Map<String,String> rf = new HashMap<String,String>();
		rf.put("port", "27017");
		rf.put("ssl", "true");
		rf.put("servers", "localhost");
		
		when( securityTool.getSecureCredential("test")).thenReturn( this.credential );
		when( credential.getId()).thenReturn("test");
		when( credential.getIdentifier()).thenReturn("test");
		when( credential.getSecret()).thenReturn("test");
		when( credential.getResource()).thenReturn("test");
		when( credential.getResponseFields()).thenReturn(rf);
		
		MongoTemplate template = this.mongoTemplateManager.getTemplate("test");
		Assertions.assertNotNull( template);
	}
	
	@Test
	public void testNoCredentialGettingTemplate() throws Exception {
		
		Map<String,String> rf = new HashMap<String,String>();
		rf.put("port", "27017");
		rf.put("ssl", "true");
		rf.put("servers", "localhost");
		
		when( securityTool.getSecureCredential("idontexist")).thenReturn( null );
		when( credential.getId()).thenReturn("test");
		when( credential.getIdentifier()).thenReturn("test");
		when( credential.getSecret()).thenReturn("test");
		when( credential.getResource()).thenReturn("test");
		when( credential.getResponseFields()).thenReturn(rf);
		
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			MongoTemplate template = this.mongoTemplateManager.getTemplate("idontexist");
		}, "ResourceNotFoundException was expected");
		
	}

}
