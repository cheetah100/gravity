/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.model.BoardRule;

@ExtendWith(MockitoExtension.class)
public class TimerExecutionJobTest {
	
	@InjectMocks
	TimerExecutionJob job;
	
	@Mock
	JobExecutionContext context;
	
	@Mock 
	JobDetail detail;

	@Mock
	JobDataMap dataMap;
	
	@Mock 
	RabbitTemplate template;
	
	@Test
	public void testExecute() throws JobExecutionException {
		
		when(this.context.getJobDetail()).thenReturn(detail);
		when(this.detail.getJobDataMap()).thenReturn(dataMap);
		when(this.dataMap.get("template")).thenReturn(template);
		when(this.dataMap.get("exchangeId")).thenReturn("test_exhange");
		when(this.dataMap.get("board")).thenReturn("test_board");
		when(this.dataMap.get("rule")).thenReturn("test_rule");

		job.execute(this.context);
		
		verify(this.template).convertAndSend(anyString(), anyString(), any(BoardRule.class));
	}
	
	@Test
	public void testExecuteNoBoard() throws JobExecutionException {
		
		when(this.context.getJobDetail()).thenReturn(detail);
		when(this.detail.getJobDataMap()).thenReturn(dataMap);
		when(this.dataMap.get("template")).thenReturn(template);
		when(this.dataMap.get("exchangeId")).thenReturn("test_exhange");
		when(this.dataMap.get("rule")).thenReturn("test_rule");

		Assertions.assertThrows(JobExecutionException.class, () -> {
			job.execute(this.context);
		}, "JobExecutionException was expected");
		
		verify(this.template).convertAndSend(anyString(), anyString(), any(BoardRule.class));
	}
	
	@Test
	public void testExecuteNoRule() throws JobExecutionException {
		
		when(this.context.getJobDetail()).thenReturn(detail);
		when(this.detail.getJobDataMap()).thenReturn(dataMap);
		when(this.dataMap.get("template")).thenReturn(template);
		when(this.dataMap.get("exchangeId")).thenReturn("test_exhange");
		when(this.dataMap.get("board")).thenReturn("test_board");

		Assertions.assertThrows(JobExecutionException.class, () -> {
			job.execute(this.context);
		}, "JobExecutionException was expected");
		
		verify(this.template).convertAndSend(anyString(), anyString(), any(BoardRule.class));
	}
	
	@Test
	public void testExecuteNoExchange() throws JobExecutionException {
		
		when(this.context.getJobDetail()).thenReturn(detail);
		when(this.detail.getJobDataMap()).thenReturn(dataMap);
		when(this.dataMap.get("template")).thenReturn(template);
		when(this.dataMap.get("board")).thenReturn("test_board");
		when(this.dataMap.get("rule")).thenReturn("test_rule");

		Assertions.assertThrows(JobExecutionException.class, () -> {
			job.execute(this.context);
		}, "JobExecutionException was expected");
		
		verify(this.template).convertAndSend(anyString(), anyString(), any(BoardRule.class));
	}
	
	@Test
	public void testExecuteNoTemplate() throws JobExecutionException {
		
		when(this.context.getJobDetail()).thenReturn(detail);
		when(this.detail.getJobDataMap()).thenReturn(dataMap);
		when(this.dataMap.get("board")).thenReturn("test_board");
		when(this.dataMap.get("rule")).thenReturn("test_rule");
		when(this.dataMap.get("exchangeId")).thenReturn("test_exhange");

		Assertions.assertThrows(JobExecutionException.class, () -> {
			job.execute(this.context);
		}, "JobExecutionException was expected");
		
		verify(this.template).convertAndSend(anyString(), anyString(), any(BoardRule.class));
	}

}
