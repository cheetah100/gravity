package nz.devcentre.gravity.managers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ldap.core.support.LdapContextSource;

import nz.devcentre.gravity.model.Credential;

@ExtendWith(MockitoExtension.class)
public class LdapTemplateManagerTest {
	
	@InjectMocks
	LdapTemplateManager ldapTemplateManager;
	

	@Test
	public void contextSourceTest() {
		Credential credential = new Credential();
		credential.setId("test_ID");
		credential.setResource("ldap://ds.cisco.com:389");
		credential.setRequestMethod("ou=employees,ou=cisco users,dc=cisco,dc=com");
		credential.setIdentifier("test");
		credential.setSecret("test");
		
		LdapContextSource contextSource = ldapTemplateManager.contextSource(credential);
		Assertions.assertEquals("ldap://ds.cisco.com:389", contextSource.getUrls()[0]);
	}
	
	/*
	 * Expecting NullPointerException ?
	 */
	@Test 
	public void contextExceptionSourceTest() {
		
		Assertions.assertThrows(NullPointerException.class, () -> {
			  Credential credential = null;
			  LdapContextSource contextSource =  ldapTemplateManager.contextSource(credential); 
			  String[] urls = contextSource.getUrls();
		}, "NullPointerException was expected");
		
	}
	  
}
