/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.integration.IntegrationPlugin;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.services.BoardService;

@ExtendWith(MockitoExtension.class)
public class TimerManagerTest {
	
	@InjectMocks
	TimerManager manager;
	
	@Mock
	private RabbitTemplate rabbitTemplate;
	
	@Mock
	private BoardsCache boardsCache;
	
	@Mock
	private RuleCache ruleCache;
	
	@Mock
	FanoutExchange gravityEventsExchange;
	
	@Mock
	BoardController boardController;
	
	@Mock
	IntegrationManager integrationManager;
	
	@Mock
	BoardService boardTools;
	
	@Mock
	IntegrationPlugin plugin;
	
	@Mock
	Rule rule;
	
	@BeforeEach
	public void setup() throws Exception {
		Map<String,String> boardList = new HashMap<>();
		boardList.put("test_board", "test");
		when(this.boardsCache.list()).thenReturn(boardList);
		
		Map<String,String> ruleList = new HashMap<>();
		ruleList.put("test", "test");
		when(this.ruleCache.list("test_board")).thenReturn(ruleList);
		when(this.ruleCache.getItem("test_board","test")).thenReturn(this.rule);
		when(this.rule.getRuleType()).thenReturn(RuleType.SCHEDULED);
		when(this.rule.getSchedule()).thenReturn("0 0 6 ? * MON");
		when(this.rule.getId()).thenReturn("test");
		when(this.rule.getName()).thenReturn("Test");
	}

	@Test
	public void testStartup() throws Exception {
		manager.startup();
		Map<String, String> timers = manager.getTimers();
		assertEquals(2, timers.size());
		boolean started = manager.isStarted();
		assertTrue(started);
	}

	@Test
	public void testLoadTimersForBoard() throws Exception {
		manager.loadTimersForBoard("test_board");
	}

	@Test
	public void testDisactivateTimer() throws Exception {
		manager.startup();
		manager.disactivateRule(this.rule);
	}

	@Test
	public void testActivateTimerNullRule() throws Exception {
		manager.startup();
		manager.activateRule(null);
		Map<String, String> timers = manager.getTimers();
		assertEquals(2, timers.size());
	}
	
	@Test
	public void testActivateTimerNoScheduleRule() throws Exception {
		manager.startup();
		when(rule.getSchedule()).thenReturn(null);
		manager.activateRule(rule);
		Map<String, String> timers = manager.getTimers();
		assertEquals(0, timers.size());
	}
	
	@Test
	public void testActivateTimerNewScheduleRule() throws Exception {
		manager.startup();
		when(rule.getSchedule()).thenReturn("0 0 7 ? * MON");
		manager.activateRule(rule);
		Map<String, String> timers = manager.getTimers();
		assertEquals(1, timers.size());
	}

	@Test
	public void testActivateTimerIntegrationIntegrationPlugin() throws Exception {
		Integration integration = new Integration();
		Map<String,Object> config = new HashMap<>();
		config.put("schedule", "0 0 6 ? * MON");
		integration.setConfig(config);
		integration.setId("integration_test");
		integration.setName("Integration Test");
		
		manager.startup();
		manager.activateIntegration(integration, plugin);
		
		Map<String, String> timers = manager.getTimers();
		assertEquals(2, timers.size());
	}
	
	@Test
	public void testActivateTimerIntegrationIntegrationPluginModification() throws Exception {
		Integration integration = new Integration();
		Map<String,Object> config = new HashMap<>();
		config.put("schedule", "0 0 6 ? * MON");
		integration.setConfig(config);
		integration.setId("integration_test");
		integration.setName("Integration Test");
		
		manager.startup();
		manager.activateIntegration(integration, plugin);
		
		config.put("schedule", "0 0 8 ? * MON");
		manager.activateIntegration(integration, plugin);
		
		Map<String, String> timers = manager.getTimers();
		assertEquals(2, timers.size());
		
	}
	
	@Test
	public void testActivateTimerIntegrationIntegrationPluginNull() throws Exception {
		Integration integration = new Integration();
		Map<String,Object> config = new HashMap<>();
		config.put("schedule", "0 0 6 ? * MON");
		integration.setConfig(config);
		integration.setId("integration_test");
		integration.setName("Integration Test");
		
		manager.startup();
		manager.activateIntegration(integration, plugin);
		
		config.put("schedule", null);

		manager.activateIntegration(integration, plugin);
		
		Map<String, String> timers = manager.getTimers();
		assertEquals(1, timers.size());
	}

	@Test
	public void testGetTimers() throws Exception {
		manager.startup();
		Map<String, String> timers = manager.getTimers();
		assertTrue( timers.size()>0);
	}

	@Test
	public void testIsStarted() throws Exception {
		manager.startup();
		boolean started = manager.isStarted();
		assertTrue(started);
	}
	
	@Test
	public void testIsNotStarted() throws Exception {
		boolean started = manager.isStarted();
		assertFalse(started);
	}

	@Test
	public void testStopAll() throws Exception {
		manager.startup();
		manager.stopAll();
		boolean started = manager.isStarted();
		assertFalse(started);		
	}

	@Test
	public void testStopAllWhileStopped() throws Exception {
		manager.stopAll();
		boolean started = manager.isStarted();
		assertFalse(started);		
	}

	
}
