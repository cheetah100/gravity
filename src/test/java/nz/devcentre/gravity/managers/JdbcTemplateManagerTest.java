/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

@ExtendWith(MockitoExtension.class)
public class JdbcTemplateManagerTest {
	
	@InjectMocks
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Mock
	private SecurityTool securityTool;

	@Mock
	private Credential credential;
	
	@Test
	public void testGetTemplate() throws Exception {
		
		when( securityTool.getSecureCredential(any())).thenReturn( this.credential );
		when( credential.getId()).thenReturn("test");
		this.jdbcTemplateManager.getTemplate("test");
	}

}
