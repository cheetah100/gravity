/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.config.SystemConfiguration;
import nz.devcentre.gravity.model.ExceptionEvent;
import nz.devcentre.gravity.repository.ExceptionRepository;
import nz.devcentre.gravity.services.EmailService;

@ExtendWith(MockitoExtension.class)
public class ExeptionManagerTest {
	
	@InjectMocks
	private ExceptionManager exceptionManager;
	
	@Mock
	private ExceptionRepository exceptionRepository;
	
	@Mock
	private EmailService emailSenderAction;
	
	@Mock
	private SystemConfiguration systemConfiguration;

	@Test
	public void testRaiseExceptionNoEmail() throws Exception {
		
		this.exceptionManager.raiseException("test", "test", "test", false);
		verify(systemConfiguration, times(1)).getEnvironment();
		
		ArgumentCaptor<ExceptionEvent> argumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);
		verify(exceptionRepository, times(1)).insert(argumentCaptor.capture());
		ExceptionEvent event = argumentCaptor.<ExceptionEvent> getValue();
		Assertions.assertEquals( "test", event.getName());
		Assertions.assertEquals( "test", event.getDescription());
		Assertions.assertEquals( "test", event.getName());
		
		verify(emailSenderAction,never() ).sendEmail(any(), any(), any(), any(), any(), any(), anyBoolean(), any(), any());
	}

	@Test
	public void testRaiseExceptionWithEmail() throws Exception {
		this.exceptionManager.setEmail_from("test_from@test.com");
		this.exceptionManager.setEmail_to("test_to@test.com");
		this.exceptionManager.raiseException("test2", "test2", "test2", true);
		verify(systemConfiguration, times(1)).getEnvironment();
		
		ArgumentCaptor<ExceptionEvent> argumentCaptor = ArgumentCaptor.forClass(ExceptionEvent.class);
		verify(exceptionRepository).insert(argumentCaptor.capture());
		ExceptionEvent event = argumentCaptor.<ExceptionEvent> getValue();
		Assertions.assertEquals( "test2", event.getName());
		Assertions.assertEquals( "test2", event.getDescription());
		Assertions.assertEquals( "test2", event.getName());
		
		verify(emailSenderAction, times(1) ).sendEmail(any(), any(), any(), any(), any(), any(), anyBoolean(), any(), any());
	}
	
	@Test
	public void testGetActiveExceptions() {
		this.exceptionManager.getActiveExceptions();
		verify(exceptionRepository, times(1)).findByActive(true);
	}

	@Test
	public void testGetAllExceptions() {
		this.exceptionManager.getAllExceptions();
		verify(exceptionRepository, times(1)).findAll();
	}

	/*
	@Test
	public void testDismissExceptionEvents() throws Exception {
		this.exceptionManager.dismissExceptionEvents();
		verify(exceptionRepository, times(1)).save(any(ExceptionEvent.class));
	}
	*/

}
