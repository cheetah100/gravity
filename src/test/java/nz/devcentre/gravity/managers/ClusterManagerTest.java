/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.automation.IdGeneratorReceiver;
import nz.devcentre.gravity.automation.SyncMessage;
import nz.devcentre.gravity.model.IdResponse;

@ExtendWith(MockitoExtension.class)
@Disabled
// Ignoring the below test as there are issues in autowiring. Will need more time to rewrite this test without mocking

public class ClusterManagerTest {
	
	@InjectMocks
	ClusterManager clusterManager;
	
	@Mock
	private RabbitTemplate idTemplate;
	
	@Mock
	private IdGeneratorReceiver idSyncReceiever;
	
	@Mock
	private TimerManager timerManager;
	
	@Mock 
	private SyncMessage sync;
	
	@Mock 
	private Message message;
	
	//@Mock
	IdResponse idResponse = new IdResponse();

	@BeforeEach
	public void setUp() throws Exception {
		when(idSyncReceiever.register(any(String.class))).thenReturn(sync);
		when(sync.receiveMessage(10000l)).thenReturn(message);
	}

	@Test
	public void testGetId() throws Exception {
		idResponse.setId(5L);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(idResponse);
		when(message.getBody()).thenReturn(bos.toByteArray());
		//when(idResponse.getId()).thenReturn(5l);
		
		Long id = clusterManager.getId("test_path", "test_field");
		
		assertEquals(5l, id.longValue());
	}

	@Test
	public void testGetIdString() throws Exception {
		idResponse.setId(5L);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(idResponse);
		when(message.getBody()).thenReturn(bos.toByteArray());
		//when(idResponse.getId()).thenReturn(5l);
		
		String idString = clusterManager.getIdString("test_path", "test_field", "PRE");
		assertEquals( "PRE5", idString);
	}

	@Test
	public void testPollLeader() throws Exception {
		when(sync.receiveMessage(10000l)).thenReturn(null);
		clusterManager.setLeader("blah");
		clusterManager.pollLeader();
		assertTrue(clusterManager.isLeader());
	}

	@Test
	public void testGetStartTime() {
		long startTime = clusterManager.getStartTime();
		long difference = System.currentTimeMillis() - startTime;
		assertTrue(difference<1000);
	}

	@Test
	public void testGetServerId() {
		String serverId = clusterManager.getServerId();
		assertNotNull(serverId);
		assertTrue(serverId.length()>5);
	}

}
