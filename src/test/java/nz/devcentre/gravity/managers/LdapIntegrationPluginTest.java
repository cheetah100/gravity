/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Assertions;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.integration.LdapIntegrationPlugin;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.services.LdapService;

import org.springframework.ldap.core.support.LdapContextSource;

@ExtendWith(MockitoExtension.class)
public class LdapIntegrationPluginTest {
	
	@InjectMocks
	private LdapIntegrationPlugin ldapintegrationPlugin;
	
	@Mock
	private LdapService lDAPService;
	
	@Mock
	private LdapTemplateManager ldapTemplateManager;
	
	@Mock
	Integration integration;
	
	@Mock
	private SecurityTool securityTool;

	@Mock
	private Credential credential;
	
	@Mock
	private AutomationEngine automationEngine;
	
	@Mock
	TimerManager timerManager;
	
	@Mock
	LdapContextSource contextSource;
	
	@Mock
	private IntegrationService integrationService;
	
	@Test
	public void executeTest() throws Exception {
		Map<String,Object> config = new HashMap<>();
		config.put("collection", "test");
		config.put("credential", "credential_id");
		List<Map<String, Object>> userList = new LinkedList<Map<String, Object>>();
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("cn", "test");
		userList.add(map1);
		when(integration.getConfig()).thenReturn(config);
		when(securityTool.getSecureCredential("credential_id")).thenReturn(credential);
	
		when(ldapTemplateManager.contextSource(any(Credential.class))).thenReturn(contextSource);
		when(lDAPService.getAllUsers(any(String.class), any(LdapContextSource.class))).thenReturn(userList);
		Batch batch = new Batch();
		when(integrationService.createBatch(any(Integration.class), any(Integer.class), any(Date.class))).thenReturn(batch);
		ldapintegrationPlugin.execute(integration);
		verify(integrationService).execute(any(Map.class), any(Integration.class), any(Batch.class));
	}
	
	@Test
	public void executeTestException() throws Exception {
		Assertions.assertThrows(Exception.class, () -> {
			ldapintegrationPlugin.execute(integration);
		}, "Exception was expected");
	}
	
	@Test
	public void testStart() throws Exception{
		ldapintegrationPlugin.start(integration);
		verify(timerManager, times(1)).activateIntegration(any(Integration.class), any(LdapIntegrationPlugin.class));
	}
	
	@Test
	public void testShutDown() {
		ldapintegrationPlugin.shutdown();
	}
	
}
