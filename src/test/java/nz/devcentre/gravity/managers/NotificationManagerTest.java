package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import nz.devcentre.gravity.integration.IntegrationPlugin;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.repository.BatchRepository;
import nz.devcentre.gravity.repository.IntegrationRepository;

@ExtendWith(MockitoExtension.class)
public class NotificationManagerTest {
	
	@InjectMocks
	IntegrationManager integrationManager;
	
	@Mock
	Map<String, IntegrationPlugin> plugins;
	
	@Mock
	IntegrationPlugin notificationPlugin;
	
	@Mock 
	Iterator<IntegrationPlugin> pluginIterator;
	
	@Mock
	BatchRepository batchRepository;
	
	@Mock
	IntegrationRepository integrationRepository;
	
	@BeforeEach
	public void init() {
		Map<String,IntegrationPlugin> plugins = new HashMap<String,IntegrationPlugin>();
		plugins.put("mongo", this.notificationPlugin);
		this.integrationManager.plugins = plugins;
	}

	@Test
	public void testStartupAndShutdown() throws Exception {
		
		List<Integration> integrationList = new ArrayList<Integration>();
		integrationList.add(getTestIntegration("test","mongo"));
		when(integrationRepository.findAll()).thenReturn(integrationList);
		
		this.integrationManager.startup();
		
		assertTrue(integrationManager.plugins!= null);
		verify(integrationRepository).findAll();
		
		this.integrationManager.shutdown();
		verify(this.notificationPlugin).shutdown();
		
	}


	@Test
	public void testExecute() throws Exception {
		List<Integration> integrationList = new ArrayList<Integration>();
		integrationList.add(getTestIntegration("test","mongo"));
		
		when(integrationRepository.findAll()).thenReturn(integrationList);
		//when(pluginList.iterator()).thenReturn(pluginIterator);
		//when(pluginIterator.hasNext()).thenReturn(true,false);
		//when(pluginIterator.next()).thenReturn(notificationPlugin);

		this.integrationManager.startup();
		this.integrationManager.execute(getTestIntegration("test","mongo"));
		
		// Wait is required because the execute is called in a separate thread.
		Thread.sleep(50);
		
		verify(this.notificationPlugin).execute(any());
	}
	
	private Integration getTestIntegration( String value, String connector ) {
		Integration integration = new Integration();
		integration.setId(value);
		integration.setDescription(value);
		integration.setConnector(connector);
		return integration; 
	}

}
