package nz.devcentre.gravity.managers;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.controllers.Cache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.CacheInvalidationInstruction;

@ExtendWith(MockitoExtension.class)
public class CacheManagerTest {
	
	@Mock
	private RabbitTemplate rabbitTemplate;
	
	@Mock 
	private TimerManager timerManager;
	
	@Mock
	FanoutExchange gravityCacheInvalidationExchange;
	
	@InjectMocks
	CacheManager cacheManager;
	
	@Mock 
	Message message;
	
	@Mock 
	Cache cache;

	@Test
	public void testOnMessage() throws Exception {
		
		CacheInvalidationInstruction instruction = new CacheInvalidationInstruction();
		instruction.setCacheType("BOARD");
		
		String[] ids = new String[1];
		ids[0] = "test";
		instruction.setIds(ids);
		instruction.setCacheType("BOARD");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);   
		out.writeObject(instruction);
		out.flush();
		byte[] instructionBytes = bos.toByteArray();
		
		when(this.message.getBody()).thenReturn(instructionBytes);
		this.cacheManager.register("BOARD", this.cache);
		
		cacheManager.onMessage(this.message);
		
		verify(this.cache, times(1)).invalidate(any());
	}
	
	@Test
	public void testOnMessageBadResource() throws Exception {
		
		CacheInvalidationInstruction instruction = new CacheInvalidationInstruction();
		instruction.setCacheType("BOARD");
		
		String[] ids = new String[1];
		ids[0] = "badboard";
		instruction.setIds(ids);
		instruction.setCacheType("BOARD");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);   
		out.writeObject(instruction);
		out.flush();
		byte[] instructionBytes = bos.toByteArray();
		
		when(this.message.getBody()).thenReturn(instructionBytes);
		doThrow(new ResourceNotFoundException()).when(this.cache).invalidate(any());
		this.cacheManager.register("BOARD", this.cache);
		
		cacheManager.onMessage(this.message);
		
		verify(this.cache, times(1)).invalidate(any());
	}
	
	@Test
	public void testOnMessageNoCache() throws Exception {
		
		CacheInvalidationInstruction instruction = new CacheInvalidationInstruction();
		
		String[] ids = new String[1];
		ids[0] = "badboard";
		instruction.setIds(ids);
		instruction.setCacheType("BOARD");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);   
		out.writeObject(instruction);
		out.flush();
		byte[] instructionBytes = bos.toByteArray();
		
		when(this.message.getBody()).thenReturn(instructionBytes);
		doThrow(new ResourceNotFoundException()).when(this.cache).invalidate(any());
		this.cacheManager.register("BAD", this.cache);
		
		cacheManager.onMessage(this.message);
		
		verify(this.cache, times(0)).invalidate(any());
	}

	public void testOnMessageGeneralException() throws Exception {
		
		CacheInvalidationInstruction instruction = new CacheInvalidationInstruction();
		
		String[] ids = new String[1];
		ids[0] = "badboard";
		instruction.setIds(ids);
		instruction.setCacheType("BOARD");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);   
		out.writeObject(instruction);
		out.flush();
		byte[] instructionBytes = bos.toByteArray();
		
		when(this.message.getBody()).thenReturn(instructionBytes);
		doThrow(new Exception()).when(this.cache).invalidate(any());
		this.cacheManager.register("BAD", this.cache);
		
		cacheManager.onMessage(this.message);
		
		verify(this.cache, times(0)).invalidate(any());
	}

	
	@Test
	public void testInvalidate() {
		cacheManager.invalidate("BOARD", "test");
	}

}
