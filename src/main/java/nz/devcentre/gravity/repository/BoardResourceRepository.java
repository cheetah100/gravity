/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import nz.devcentre.gravity.model.BoardResource;;



public interface BoardResourceRepository extends MongoRepository<BoardResource, String>{
	
	public List<BoardResource> findByBoardIdAndId(String boardId, String id);
	
	public List<BoardResource> findByBoardId(String boardId);
	
	public BoardResource findByBoardIdAndIdAndActiveTrue(String boardId, String id);
	
	public List<BoardResource> findByBoardIdAndActiveTrue(String boardId);
	
	public BoardResource findByBoardIdAndIdAndVersion(String boardId,String id, String version);

	public List<BoardResource> findByBoardIdAndPublish(String boardId,boolean publish);
	
}
