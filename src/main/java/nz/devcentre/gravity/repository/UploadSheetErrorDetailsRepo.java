package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import nz.devcentre.gravity.model.UploadSheetErrorDetails;

public interface UploadSheetErrorDetailsRepo extends PagingAndSortingRepository<UploadSheetErrorDetails, String>{

	public List<UploadSheetErrorDetails> findByUploadIdAndMetadata_Creator(String uploadId,String creator);
	public List<UploadSheetErrorDetails> findByUploadIdAndUploadSheetIdAndMetadata_Creator(String uploadId,String uploadSheetId,String creator);
}
