/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import nz.devcentre.gravity.model.CardEvent;

public interface CardEventRepository extends MongoRepository<CardEvent, String>{
	
	public List<CardEvent> findByUser(String user);

	public CardEvent findByUuid(String uuid);
	
	public List<CardEvent> findByBoard(String board);
	
	public List<CardEvent> findByBoardAndDetailOrCategory(String board, String detail, String category);
	
	public List<CardEvent> findByBoardAndDetail(String board, String detail);
	
	public List<CardEvent> findByBoardAndCategory(String board, String category);
	
	public List<CardEvent> findByCard(String card);
	
	public List<CardEvent> findByBoardAndCard(String board, String card);
	
	public List<CardEvent> findByOccuredTimeBetween( Date from, Date to);
	
	public Page<CardEvent> findByOccuredTimeBetween( Date from, Date to,Pageable pageable);
	
	public List<CardEvent> findByBoardAndOccuredTimeBetweenAndCategoryOrDetail( String board, Date from, Date to, String category, String detail);
	
	public List<CardEvent> findByBoardAndCardAndCategory(String board, String card, String category);

	public List<CardEvent> findByBoardAndCardAndCategoryAndLevel(String board, String card, String category, String level);

	public List<CardEvent> findByCategoryAndLevelAndUser(String category, String level, String user);
	
	public List<CardEvent> findByBoardAndPhaseAndCardAndCategoryOrDetail(String board, String phase, String card, String category, String detail);
	
	public List<CardEvent> findByOccuredTimeBetweenAndUserNotIgnoreCase( Date from, Date to,String user);
}
