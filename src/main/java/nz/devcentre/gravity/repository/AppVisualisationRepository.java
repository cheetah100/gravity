package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import nz.devcentre.gravity.model.AppVisualisation;


public interface AppVisualisationRepository extends MongoRepository<AppVisualisation, String>{
	
	public AppVisualisation findByApplicationIdAndId(String applicationId, String visualisationId);
	
	public List<AppVisualisation> findByApplicationId(String applicationId);

	

}



