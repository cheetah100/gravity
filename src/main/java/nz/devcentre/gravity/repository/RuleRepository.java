/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import nz.devcentre.gravity.model.Rule;;



public interface RuleRepository extends MongoRepository<Rule, String>{
	
	public List<Rule> findByBoardIdAndId(String boardId, String id);
	
	public List<Rule> findByBoardId(String boardId);
	
	public List<Rule> findByBoardIdAndActiveTrue(String boardId);
	
	public Rule findByBoardIdAndIdAndVersion(String boardId, String id,String version);
	
	public Rule findByBoardIdAndIdAndActiveTrue(String boardId, String id);
	
	
}
