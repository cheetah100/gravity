package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import nz.devcentre.gravity.model.CardWatch;;


public interface CardWatchRepository extends MongoRepository<CardWatch, String>{
	
	public CardWatch findById(String id);
	
	public List<CardWatch> findByBoardAndCard(String board, String card);
	
	public List<CardWatch> findByBoard(String board);
	public List<CardWatch> findByUser(String user);
	//public List<Dashboard> findByDeleted(Boolean deleted);

}
