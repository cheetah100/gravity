package nz.devcentre.gravity.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import nz.devcentre.gravity.model.Upload;
import nz.devcentre.gravity.model.importer.TransferStatusEnum;

public interface UploadRepo extends PagingAndSortingRepository<Upload, String> {

	public List<Upload> findByMetadata_CreatorOrderByMetadata_CreatedDesc(String creator);
	public List<Upload> findByStatusInOrderByMetadata_CreatedDesc(List<TransferStatusEnum> status);
	public List<Upload> findByStatusAndMetadata_CreatorOrderByMetadata_CreatedDesc(TransferStatusEnum status,String creator);
}
