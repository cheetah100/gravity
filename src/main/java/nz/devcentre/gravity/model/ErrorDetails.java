package nz.devcentre.gravity.model;

import java.io.Serializable;

public class ErrorDetails implements Serializable {
	
	private static final long serialVersionUID = 4442021428402240230L;
	
	private String rowNum;
	
	private String rowData;
	
	private String errorMessage;

	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getRowData() {
		return rowData;
	}

	public void setRowData(String rowData) {
		this.rowData = rowData;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorDetails(String rowNum, String rowData, String errorMessage) {
		super();
		this.rowNum = rowNum;
		this.rowData = rowData;
		this.errorMessage = errorMessage;
	}
	
	

}
