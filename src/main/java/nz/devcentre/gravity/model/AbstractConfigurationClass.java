package nz.devcentre.gravity.model;

import org.springframework.data.annotation.Id;

public class AbstractConfigurationClass {
	
	@Id
	private String uniqueid;
	
	private String id;
	
	private String version;
	
	private String name;
	
	private String description;
	
	private String tenantId;
	
	private boolean active;
	
	private boolean publish;
	
	private Metadata metadata;
	
	public void addOrUpdate(){
		if(this.metadata==null){
			this.metadata = new Metadata();
		} else {
			this.metadata.update();
		}
	}

	/**
	 * @return the metadata
	 */
	public Metadata getMetadata() {
		return metadata;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public String getId() {
		if(id!=null) {
			return id;	
		}
		return uniqueid;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
}
