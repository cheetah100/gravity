/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import nz.devcentre.gravity.model.importer.TransferStatusEnum;

@Document(collection="gravity_uploads")
public class Upload extends AbstractBaseModelClass{

	private int totalRows;
	private int successRows;
	private int errorRows;
	private int createRows;
	private int updateRows;
	
	private String message;
	
	private String fileName;
	
	private List<UploadSheet> sheets;
	
	
	@NotNull
	private TransferStatusEnum status;
	/**
	 * @return the totalRows
	 */
	public int getTotalRows() {
		return totalRows;
	}
	/**
	 * @param totalRows the totalRows to set
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}
	/**
	 * @return the successRows
	 */
	public int getSuccessRows() {
		return successRows;
	}
	/**
	 * @param successRows the successRows to set
	 */
	public void setSuccessRows(int successRows) {
		this.successRows = successRows;
	}
	/**
	 * @return the errorRows
	 */
	public int getErrorRows() {
		return errorRows;
	}
	/**
	 * @param errorRows the errorRows to set
	 */
	public void setErrorRows(int errorRows) {
		this.errorRows = errorRows;
	}
	
	/**
	 * @return the status
	 */
	public TransferStatusEnum getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(TransferStatusEnum status) {
		this.status = status;
	}
	/**
	 * @return the createRows
	 */
	public int getCreateRows() {
		return createRows;
	}
	/**
	 * @param createRows the createRows to set
	 */
	public void setCreateRows(int createRows) {
		this.createRows = createRows;
	}
	/**
	 * @return the updateRows
	 */
	public int getUpdateRows() {
		return updateRows;
	}
	/**
	 * @param updateRows the updateRows to set
	 */
	public void setUpdateRows(int updateRows) {
		this.updateRows = updateRows;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the sheets
	 */
	public List<UploadSheet> getSheets() {
		return sheets;
	}
	/**
	 * @param sheets the sheets to set
	 */
	public void setSheets(List<UploadSheet> sheets) {
		this.sheets = sheets;
	}
	
	
	
}
