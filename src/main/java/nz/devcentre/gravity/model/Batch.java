/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

import java.util.Date;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_batches")
@CompoundIndexes({
	  @CompoundIndex(def = "{'notificationId':1, 'maxProcessDate':-1}", name = "latest_batch_1")
})
public class Batch extends AbstractBaseModelClass{
	
	private String notificationId;
 
	private Date maxProcessDate;
	
	private int rowsImported;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public Date getMaxProcessDate() {
		return maxProcessDate;
	}

	public void setMaxProcessDate(Date maxProcessDate) {
		this.maxProcessDate = maxProcessDate;
	}

	public int getRowsImported() {
		return rowsImported;
	}

	public void setRowsImported(int rowsImported) {
		this.rowsImported = rowsImported;
	}
}
