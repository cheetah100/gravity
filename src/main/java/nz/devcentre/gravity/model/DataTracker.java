package nz.devcentre.gravity.model;

import java.io.Serializable;

public class DataTracker implements Serializable {
	private static final long serialVersionUID = 6722021428447640230L;
	
	Integer validCount;
	String fieldName;
	Integer totalNumber;
	Boolean isRequired;
	FieldType dataType;
	String validation;

	public DataTracker(String name, Boolean required, FieldType type, String validation, Integer totalNumbers){
		this.validCount = 0;
		this.fieldName = name;
		this.isRequired = required;
		this.dataType = type;
		this.validation = validation;
		this.totalNumber = totalNumbers;
	}

	public void incrementCounter(){
		Integer currentCount = getValidCount();
		this.validCount = currentCount  + 1;
	}


	public Integer getValidCount(){
	
		return this.validCount;
	
	}

	public String getFieldName(){
	
		return this.fieldName;
	
	}

	public Integer getTotalNumber(){
	
		return this.totalNumber;
	
	}

	public Boolean getIsRequired(){
	
		return this.isRequired;
	
	}

	public FieldType getDataType(){
	
		return this.dataType;
	
	}

	public String getValidation(){
	
		return this.validation;
	
	}


}
