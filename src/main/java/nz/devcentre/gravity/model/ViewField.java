/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.Map;

public class ViewField extends AbstractNamedModelClass {
	
	private static final long serialVersionUID = 5867099109422361866L;
	
	private String label;
		
	private Integer index;
	
	private String childboard;
	
	private String childfield;
	
	private Map<String,Object> lookups;
	
	private Map<String,Object> filters;
	
	private Map<String,Object> ui;

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getIndex() {
		return index;
	}

	public Map<String,Object> getUi() {
		return ui;
	}

	public void setUi(Map<String,Object> ui) {
		this.ui = ui;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getChildboard() {
		return childboard;
	}

	public void setChildboard(String childboard) {
		this.childboard = childboard;
	}

	public String getChildfield() {
		return childfield;
	}

	public void setChildfield(String childfield) {
		this.childfield = childfield;
	}
}
