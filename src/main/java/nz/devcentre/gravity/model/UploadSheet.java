package nz.devcentre.gravity.model;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import nz.devcentre.gravity.model.importer.TransferStatusEnum;


public class UploadSheet extends AbstractBoardClass{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4740651508749392252L;
	
	private int totalRows;
	private int successRows;
	private int errorRows;
	private int createRows;
	private int updateRows;
	
	private String message;
	

	
	@NotNull
	private TransferStatusEnum status;
	/**
	 * @return the totalRows
	 */
	public int getTotalRows() {
		return totalRows;
	}
	/**
	 * @param totalRows the totalRows to set
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}
	/**
	 * @return the successRows
	 */
	public int getSuccessRows() {
		return successRows;
	}
	/**
	 * @param successRows the successRows to set
	 */
	public void setSuccessRows(int successRows) {
		this.successRows = successRows;
	}
	/**
	 * @return the errorRows
	 */
	public int getErrorRows() {
		return errorRows;
	}
	/**
	 * @param errorRows the errorRows to set
	 */
	public void setErrorRows(int errorRows) {
		this.errorRows = errorRows;
	}
	
	/**
	 * @return the status
	 */
	public TransferStatusEnum getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(TransferStatusEnum status) {
		this.status = status;
	}
	/**
	 * @return the createRows
	 */
	public int getCreateRows() {
		return createRows;
	}
	/**
	 * @param createRows the createRows to set
	 */
	public void setCreateRows(int createRows) {
		this.createRows = createRows;
	}
	/**
	 * @return the updateRows
	 */
	public int getUpdateRows() {
		return updateRows;
	}
	/**
	 * @param updateRows the updateRows to set
	 */
	public void setUpdateRows(int updateRows) {
		this.updateRows = updateRows;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	@JsonIgnore
	public Metadata getMetadata() {
		// TODO Auto-generated method stub
		return super.getMetadata();
	}
	@Override
	@JsonIgnore
	public String getUniqueid() {
		// TODO Auto-generated method stub
		return super.getUniqueid();
	}
	@Override
	@JsonIgnore
	public boolean isActive() {
		// TODO Auto-generated method stub
		return super.isActive();
	}
	@Override
	@JsonIgnore
	public boolean isPublish() {
		// TODO Auto-generated method stub
		return super.isPublish();
	}
	@Override
	@JsonIgnore
	public String getVersion() {
		// TODO Auto-generated method stub
		return super.getVersion();
	}
}
