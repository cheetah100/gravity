package nz.devcentre.gravity.model;

public interface Orderable {
	
	public int getOrder();

}
