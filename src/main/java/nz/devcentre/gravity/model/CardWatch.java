/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gravity_watch")
public class CardWatch extends AbstractBaseModelClass implements Serializable{
		
	private static final long serialVersionUID = 12537785056044716L;
	
	private String user;
	
	private Date watchDate;

	private String board;
	
	private String card;
	
	private Boolean active;
	
	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setWatchDate(Date watchDate) {
		this.watchDate = watchDate;
	}

	public Date getWatchDate() {
		return watchDate;
	}

	
	public String getCard() {
		return card;
	}
	
	public String getBoard() {
		return board;
	}

	public void setCard(String cardId) {
		this.card = cardId;
	}
	
	public void setBoard(String boardId) {
		this.board = boardId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
