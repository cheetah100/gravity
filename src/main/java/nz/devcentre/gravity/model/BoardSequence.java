package nz.devcentre.gravity.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_sequences")
public class BoardSequence {
	
	@Id
	private String id;
	
	private String boardid;
	
	private String name;
	
	private Long value;

	public String getId() {
		if(this.id==null && this.boardid!=null && this.name!=null){
			return this.boardid + "_" + this.name;
		}
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBoardid() {
		return boardid;
	}

	public void setBoardid(String boardid) {
		this.boardid = boardid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

}
