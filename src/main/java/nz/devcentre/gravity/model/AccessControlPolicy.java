/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_acp")
public class AccessControlPolicy extends AbstractNamedModelClass{
	
	private static final long serialVersionUID = 7681407905409109728L;
	
	private List<AccessControlResource> resources;
	
	private List<String> owners;
	
	private List<String> teams;

	public List<AccessControlResource> getResources() {
		return resources;
	}

	public void setResources(List<AccessControlResource> resources) {
		this.resources = resources;
	}

	public List<String> getOwners() {
		return owners;
	}

	public void setOwners(List<String> owners) {
		this.owners = owners;
	}

	public List<String> getTeams() {
		return teams;
	}

	public void setTeams(List<String> teams) {
		this.teams = teams;
	}

}
