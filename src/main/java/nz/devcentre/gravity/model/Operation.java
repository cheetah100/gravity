/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

public enum Operation {
	
	CONTAINS("contains", "\"${field}\":/${value}/","" ),
	NOTCONTAINS("contains", "\"${field}\":{$not: /${value}/}","" ),
	EQUALTO("equal to", "\"${field}\":\"${value}\"","${field} = '${value}'"),
	NUMBEREQUALTO("equal to", "\"${field}\":${value}","${field} = ${value}"),
	NOTEQUALTO("not equal to", "\"${field}\" : {$ne: \"${value}\" }",""),
	NUMBERNOTEQUALTO("not equal to", "\"${field}\" : {$ne: ${value} }",""),
	GREATERTHAN("greater than", "\"${field}\" : {$gt: ${value} }","${field} > ${value}"),
	GREATERTHANOREQUALTO("greater than or equal to", "\"${field}\" : {$gte: ${value} }","${field} >= ${value}"),
	LESSTHAN("less than", "\"${field}\" : {$lt: ${value} }","${field} < ${value}"),
	LESSTHANOREQUALTO("less than or equal to", "\"${field}\" : {$lte: ${value} }","${field} <= ${value}"),
	ISNULL("is null", "\"${field}\":null",""),
	NOTNULL("is not null", "\"${field}\" : {$ne: null }",""),
	BEFORE("before", "\"${field}\" : {$lt: new Date(${value})}",""),
	AFTER("after", "\"${field}\" : {$gt: new Date(${value})}",""),
	AT("at", "\"${field}\" : new Date(${value})",""),
	COMPLETE("complete", null, null),
	INCOMPLETE("incomplete", null, null),
	ALERTS("alerts", null, null),
	NOTPHASE("notphase", "\"${metadata.phase}\" : {$nin: \"${field}\" }", ""),
	ISPHASE("isphase", "\"${metadata.phase}\" : {$in: \"${field}\" }",""),
	IN("in", "\"${field}\" : {$in: [\"${value}\"] }",""),
	NIN("nin","\"${field}\" : {$nin: [\"${value}\"] }",""),
	ONORBEFORE("on or before", "\"${field}\" : {$lte: new Date(${value})}",""),
	ONORAFTER("on or after", "\"${field}\" : {$gte: new Date(${value})}","");
	
	private String label;
	private String mongoExpression;
	private String sqlExpression;
	
	private Operation(String label, String mongoExpression, String sqlExpression){
		this.label = label;
		this.mongoExpression = mongoExpression;
		this.sqlExpression = sqlExpression;
	}
	
	public String getLabel(){
		return this.label;
	}

	public String getMongoExpression() {
		return mongoExpression;
	}

	public String getSqlExpression() {
		return sqlExpression;
	}
	
}
