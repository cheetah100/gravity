package nz.devcentre.gravity.model;

import java.util.List;

public class UserDetails extends User {


	private static final long serialVersionUID = 1028488260345152687L;
	private List<Preference> preference;

	public List<Preference> getPreference() {
		return preference;
	}

	public void setPreference(List<Preference> preference) {
		this.preference = preference;
	}

}
