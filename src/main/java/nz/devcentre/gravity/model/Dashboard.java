/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_dashboards")
public class Dashboard extends AbstractConfigurationClass implements Serializable, SecurityPermissionEntity {

	private static final long serialVersionUID = 1645058226599514198L;
	
	private String icon_path;
	
	private Map<String,String> permissions;
	
	private List<WidgetContainer> widgets;
	
	private Map<String, Object> ui;
	
	@Override
	public Map<String, String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String,String> permissions) {
		this.permissions = permissions;
	}

	public List<WidgetContainer> getWidgets() {
		return widgets;
	}

	public void setWidgets(List<WidgetContainer> widgets) {
		this.widgets = widgets;
	}

	public String getIcon_path() {
		return icon_path;
	}

	public void setIcon_path(String icon_path) {
		this.icon_path = icon_path;
	}

	public Map<String, Object> getUi() {
		return ui;
	}

	public void setUi(Map<String, Object> ui) {
		this.ui = ui;
	}

}
