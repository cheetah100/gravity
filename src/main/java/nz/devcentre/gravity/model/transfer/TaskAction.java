package nz.devcentre.gravity.model.transfer;

public enum TaskAction {
	ASSIGN,
	COMPLETE,
	REVERT
}
