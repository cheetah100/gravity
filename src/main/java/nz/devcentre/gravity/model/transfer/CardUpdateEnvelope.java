package nz.devcentre.gravity.model.transfer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CardUpdateEnvelope implements Serializable{

	private static final long serialVersionUID = -6984014585560091871L;
	
	private List<String> ids;
	
	private Map<String,Object> fields;

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	public Map<String,Object> getFields() {
		return fields;
	}

	public void setFields(Map<String,Object> fields) {
		this.fields = fields;
	}

}
