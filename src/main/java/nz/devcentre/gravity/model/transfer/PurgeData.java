package nz.devcentre.gravity.model.transfer;

public class PurgeData {

	private Purge purge;
    private String id;
    
	public Purge getPurge() {
		return purge;
	}
	public void setPurge(Purge purge) {
		this.purge = purge;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
