/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nz.devcentre.gravity.controllers.PivotTableController;

public class PercentData extends AbstractPivotData {

	private Map<String, Number> values = new HashMap<String,Number>();
	
	private Map<String, Number> counts = new HashMap<String,Number>();
	
	private Map<String, Number> grandTotals = new HashMap<String,Number>();
	
	private static final Logger logger = LoggerFactory.getLogger(PercentData.class);
	
	DecimalFormat df = new DecimalFormat("#.##");
	
	@Override
	public void addData(String xAxis, String yAxis, Object value) {
		
		this.xAxisSet.put(xAxis,xAxis);
		this.yAxisSet.put(yAxis,yAxis);
		String elementKey = xAxis + ":" + yAxis;
		String xGTKey = xAxis;
		
		if( this.counts.containsKey(elementKey) ){
			Number newCount = counts.get(elementKey).intValue() + 1;
			this.counts.put(elementKey, newCount);
			
			Number newXGT = grandTotals.get(xGTKey).doubleValue() + 1;
			this.grandTotals.put(xGTKey, newXGT);
								
			logger.debug("elementKey:" +elementKey +" newCount: "+ newCount + " xGTKey:" + xGTKey + " GT:" + newXGT );
		} else {
			
			this.counts.put(elementKey, 1);
			if(this.grandTotals.containsKey(xGTKey) ){
				Number newXGT = grandTotals.get(xGTKey).doubleValue() + 1;
				this.grandTotals.put(xGTKey, newXGT);
				logger.debug("elementKey:" +elementKey +" newCount: 1" + " xGTKey:" + xGTKey + " GT:" + newXGT );
			}else{
				this.grandTotals.put(xGTKey, 1);
				logger.debug("elementKey: 1 GT: 1 newPercent: 100"  + elementKey + xGTKey);
			}
			

			
		}	
	}
	
	@Override
	public List<List<Object>> getData() {
		List<List<Object>> result = new ArrayList<List<Object>>();
		for( String yAxis : yAxisSet.keySet()){
			List<Object> newLine = new ArrayList<Object>();
			for( String xAxis : xAxisSet.keySet() ){
				String elementKey = xAxis + ":" + yAxis;
				String xGTKey = xAxis;
				Number count = counts.get(elementKey);
				Number total = grandTotals.get(xGTKey);
				if( count!=null && total!=null){
					Number number = ((count.doubleValue() / total.doubleValue() ) * 100);
					DecimalFormat df = new DecimalFormat("#.##'%'");
					logger.debug("elementKey:" +elementKey + " xGTKey:" + xGTKey +" newCount: "+ count + " GT:" + total+ " Percent:" + number );
					newLine.add(df.format(number.doubleValue()));
				} else {
					newLine.add("0%");
				}
			}
			result.add(newLine);
		}
		return result;
	}
}
