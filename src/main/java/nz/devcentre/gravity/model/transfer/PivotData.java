/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import nz.devcentre.gravity.model.Option;

/**
 * PivotData
 * 
 * The purpose of the PivotData Class is to act as a container for data injected into a pivot table.
 * 
 * @author peter
 *
 */
public interface PivotData {
	
	public List<List<Object>> getData();
	
	public void addData( String xAxis, String yAxis, Object value );
	
	public Collection<String> getxAxis();
	
	public Collection<String> getyAxis();
	
	public void reorderxAxis(Map<String, Option> set);
	
	public void reorderyAxis(Map<String, Option> set);
	
	public void addDatasource(String datasource);
	
	public Collection<String> getDatasources();
	
}
