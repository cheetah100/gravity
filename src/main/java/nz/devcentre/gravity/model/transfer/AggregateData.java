/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AggregateData extends AbstractPivotData {

	private Map<String, Number> values = new HashMap<String,Number>();
	
	@Override
	public void addData(String xAxis, String yAxis, Object value) {
		if(!(value instanceof Number) || xAxis == null || yAxis == null){
			return;
		} 
		Number numberValue = (Number) value; 
		this.xAxisSet.put(xAxis,xAxis);
		this.yAxisSet.put(yAxis,yAxis);
		String elementKey = xAxis + ":" + yAxis;
		this.values.put(elementKey, numberValue);
			
	}
	
	@Override
	public List<List<Object>> getData() {
		List<List<Object>> result = new ArrayList<List<Object>>();
		for( String yAxis : yAxisSet.keySet()){
			List<Object> newLine = new ArrayList<Object>();
			for( String xAxis : xAxisSet.keySet() ){
				String elementKey = xAxis + ":" + yAxis;
				Number number = values.get(elementKey);
				newLine.add(number);
			}
			result.add(newLine);
		}
		return result;
	}
	
	public void addToMap( Map<String,Object> map ){
		map.put("datasources", this.datasources);
		map.put("data", this.getData());
		map.put("xAxis", this.getxAxis());
		map.put("yAxis", this.getyAxis());
	}
	
	
	public void addToMapForDataServices( Map<String,Object> map ){
		map.put("datasources", this.datasources);
	
	}
	public  Map<String,Object> addDataSeries(String xAxis,String xAxisValue,Number value) {
		
		Map<String,Object> dataSeries = new HashMap<String,Object>();
		
		
		dataSeries.put("x", xAxisValue);
		
	
		dataSeries.put("y", value);
		
		return dataSeries;
	}

}
		
	
	
	

