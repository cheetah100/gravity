/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nz.devcentre.gravity.model.Card;

public class ListData extends AbstractPivotData {

	private Map<String, List<Card>> values = new HashMap<String,List<Card>>();
	
	@Override
	public void addData(String xAxis, String yAxis, Object value) {
		if(!(value instanceof Card)){
			return;
		}
		Card card = (Card) value; 
		this.xAxisSet.put(xAxis,xAxis);
		this.yAxisSet.put(yAxis,yAxis);
		String elementKey = xAxis + ":" + yAxis;
		if( this.values.containsKey(elementKey) ){
			List<Card> list = this.values.get(elementKey);
			list.add(card);
		} else {
			List<Card> list = new ArrayList<Card>();
			list.add(card);
			this.values.put(elementKey, list);
		}	
	}
	
	@Override
	public List<List<Object>> getData() {
		return null;
	}
	
	public Map<String, List<Card>> getList() {
		return this.values;
	}
	
	
}
