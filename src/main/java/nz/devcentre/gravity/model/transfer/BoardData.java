package nz.devcentre.gravity.model.transfer;

public class BoardData {
	String id;
	long  value;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}

}
