package nz.devcentre.gravity.model.transfer;

import java.util.Date;

public class Interval {
	String id;
	Date startDate;
	Date endDate;
	String name;

	long daysElapsed;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDaysElapsed() {
		return daysElapsed;
	}

	public void setDaysElapsed(long daysElapsed) {
		this.daysElapsed = daysElapsed;
	}

}
