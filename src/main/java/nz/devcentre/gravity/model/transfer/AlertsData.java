package nz.devcentre.gravity.model.transfer;

import java.util.List;

public class AlertsData {
	boolean all;
	List<String>  alerts;
	public boolean isAll() {
		return all;
	}
	public void setAll(boolean all) {
		this.all = all;
	}
	public List<String> getAlerts() {
		return alerts;
	}
	public void setAlerts(List<String> alerts) {
		this.alerts = alerts;
	}
	
	

}
