/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model.transfer;

import java.util.List;
import java.util.Map;

import nz.devcentre.gravity.model.Condition;

public class QueryRequest {
	
	private String boardId;
	
	private String filterId;
	
	private String viewId;
	
	private boolean allFields;
	
	private boolean allCards;
	
	private int skip = 0;
	
	private int pageSize = 50;
	
	private Map<String,String> fields;
	
	private List<Condition> conditions;
	
	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getFilterId() {
		return filterId;
	}

	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

	public boolean isAllFields() {
		return allFields;
	}

	public void setAllFields(boolean allFields) {
		this.allFields = allFields;
	}

	public boolean isAllCards() {
		return allCards;
	}

	public void setAllCards(boolean allCards) {
		this.allCards = allCards;
	}

	public Map<String, String> getFields() {
		return fields;
	}

	public void setFields(Map<String, String> fields) {
		this.fields = fields;
	}

	public List<Condition> getConditions() {
		return conditions;
	}

	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}
	
	public int getSkip() {
		return skip;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setSkip(int skip) {
		this.skip = skip;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	
}
