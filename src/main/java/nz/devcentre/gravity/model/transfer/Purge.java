package nz.devcentre.gravity.model.transfer;

public enum Purge {

	ALL,
	BOARD,
	CARD
}
