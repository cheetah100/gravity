/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import nz.devcentre.gravity.model.Option;

import java.util.TreeMap;

public abstract class AbstractPivotData implements PivotData {

	Map<String,String> xAxisSet = new TreeMap<String,String>();
	
	Map<String,String> yAxisSet = new TreeMap<String,String>();
	
	Collection<String> datasources = new HashSet<String>();
	
	public Collection<String> getxAxis() {
		return this.xAxisSet.values();
	}

	public Collection<String> getyAxis() {
		return this.yAxisSet.values();
	}
	
	public Collection<String> getDatasources(){
		return this.datasources;
	}	
	
	public void addDatasource(String datasource){
		this.datasources.add(datasource);
	}	
	
	public void reorderxAxis(Map<String, Option> order){
		this.xAxisSet = reorder(this.xAxisSet, order);
	}

	public void reorderyAxis(Map<String, Option> order){
		this.yAxisSet = reorder(this.yAxisSet, order);
	}
	
	public void retitlexAxis(Map<String, Option> order){
		this.xAxisSet = retitle(this.xAxisSet, order);
	}

	public void retitleyAxis(Map<String, Option> order){
		this.yAxisSet = retitle(this.yAxisSet, order);
	}
	
	private Map<String,String> reorder(Map<String,String> map, Map<String, Option> order  ){
		LinkedHashMap<String,String> newMap  = new LinkedHashMap<String,String>();
		for( String item : order.keySet()){
			if( map.containsKey(item)){
				newMap.put(item,order.get(item).getName());
			}
		}
		return newMap;
	}
	
	private Map<String,String> retitle(Map<String,String> map, Map<String, Option> order  ){
		for( Entry<String,String> entry : map.entrySet()) {
			entry.setValue(order.get(entry.getKey()).getName());
		}
		return map;
	}

	public void setDatasources(Collection<String> datasources) {
		this.datasources = datasources;
	}
}
