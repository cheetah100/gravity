package nz.devcentre.gravity.model.transfer;

import nz.devcentre.gravity.model.FieldType;

public class LookupDetails {
	
	private String element;
	private String board;
	private FieldType fieldType;
	
	public LookupDetails(String element, String board,FieldType fieldType ) {
		this.element = element;
		this.board = board;
		this.fieldType=fieldType;
	}
	
	public String getElement() {
		return element;
	}

	public String getBoard() {
		return board;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	@Override
	public String toString() {
		return " element: " + element + " board: " + board + " "+" fieldType: "+fieldType.name();
	}
}
