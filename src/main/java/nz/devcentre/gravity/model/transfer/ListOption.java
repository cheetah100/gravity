package nz.devcentre.gravity.model.transfer;

public class ListOption {

	private String id;

	private String title;

	private String color;

	private String icon;

	private String tooltip;

	private String phase;

	private String order;

	public ListOption(String id, String title, String color, String icon, String tooltip, String phase, String order) {
		this.id = id;
		this.title = title;
		this.color = color;
		this.icon = icon;
		this.tooltip = tooltip;
		this.phase = phase;
		this.order = order;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getColor() {
		return color;
	}

	public String getIcon() {
		return icon;
	}

	public String getTooltip() {
		return tooltip;
	}

	public String getPhase() {
		return phase;
	}

	public String getOrder() {
		return order;
	}

}
