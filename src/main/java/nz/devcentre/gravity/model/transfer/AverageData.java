/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.transfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AverageData extends AbstractPivotData {

	private Map<String, Number> values = new HashMap<String,Number>();
	
	private Map<String, Number> counts = new HashMap<String,Number>();
	
	@Override
	public void addData(String xAxis, String yAxis, Object value) {
		if(!(value instanceof Number)){
			return;
		}
		Number numberValue = (Number) value;
		this.xAxisSet.put(xAxis,xAxis);
		this.yAxisSet.put(yAxis,yAxis);
		String elementKey = xAxis + ":" + yAxis;
		if( this.values.containsKey(elementKey) ){
			Number newCount = counts.get(elementKey).intValue() + 1;
			this.counts.put(elementKey, newCount);
			Number newValue = values.get(elementKey).doubleValue() + numberValue.doubleValue();
			this.values.put(elementKey, newValue);
		} else {
			this.counts.put(elementKey, 1);
			this.values.put(elementKey, numberValue);
		}	
	}
	
	@Override
	public List<List<Object>> getData() {
		List<List<Object>> result = new ArrayList<List<Object>>();
		for( String yAxis : yAxisSet.keySet()){
			List<Object> newLine = new ArrayList<Object>();
			for( String xAxis : xAxisSet.keySet() ){
				String elementKey = xAxis + ":" + yAxis;
				Number value = values.get(elementKey);
				Number count = counts.get(elementKey);
				if( value!=null && count!=null){
					Number number = value.doubleValue() / count.doubleValue();
					newLine.add(number);
				} else {
					newLine.add(0);
				}
			}
			result.add(newLine);
		}
		return result;
	}
}
