package nz.devcentre.gravity.model.transfer;

import nz.devcentre.gravity.annotations.ConfigurationFieldType;

public class PluginField {
	
	private String field;
	private String label;
	private ConfigurationFieldType type;
	private String description;
	private boolean required;
	private String board;
	
	public String getField() {
		return field;
	}
	
	public void setField(String field) {
		this.field = field;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	public ConfigurationFieldType getType() {
		return type;
	}
	
	public void setType(ConfigurationFieldType type) {
		this.type = type;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isRequired() {
		return required;
	}
	
	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

}
