package nz.devcentre.gravity.model.transfer;

public class TaskActionBody {
	
	private TaskAction taskAction;
	
	private String user;

	public TaskAction getTaskAction() {
		return taskAction;
	}

	public void setTaskAction(TaskAction taskAction) {
		this.taskAction = taskAction;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
