package nz.devcentre.gravity.model.transfer;

public enum AutomationAction {
	START,
	STOP,
	PAUSE

}
