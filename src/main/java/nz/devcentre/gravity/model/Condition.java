/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;

public class Condition implements Serializable, Cloneable  {

	private static final long serialVersionUID = 8687513461317567183L;

	private String fieldName;
	
	private Operation operation;
	
	private String value;
	
	private ConditionType conditionType;
	
	public Condition(){
	}
	
	public Condition( String fieldName, Operation operation, String value){
		this.fieldName = fieldName;
		this.operation = operation;
		this.value = value;
		this.conditionType = ConditionType.PROPERTY;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public Operation getOperation() {
		return operation;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setConditionType(ConditionType conditionType) {
		this.conditionType = conditionType;
	}

	public ConditionType getConditionType() {
		return conditionType;
	}

	@Override
	public String toString() {
		return "Condition [fieldName=" + fieldName + ", operation=" + operation
				+ ", value=" + value + ", conditionType="
				+ conditionType + "]";
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Condition c = new Condition(fieldName,operation,value);
		c.setConditionType(conditionType);
		return c;
	}

}
