/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 DevCentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.Map;

public class TemplateField extends AbstractNamedModelClass{
	
	private static final long serialVersionUID = 5867099109422361866L;
		
	private String label;

	private String validation;
	
	private FieldType type;
	
	private Map<String,Object> ui;
	
	private String optionlist;
	
	private String optionlistfilter;
	
	private boolean required;
	
	private String requiredPhase;
	
	private boolean immutable;
	
	private boolean indexed;
	
	@Override
	public String getName(){
		if( super.getName()!=null){
			return super.getName();
		} else {
			return super.getId();
		}
	}
	
	public void setType(FieldType type) {
		this.type = type;
	}

	public FieldType getType() {
		return type;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public boolean isRequired() {
		return required;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public String getValidation() {
		return validation;
	}

	public void setOptionlist(String optionlist) {
		if(optionlist!=null){
			this.optionlist = optionlist.replace("-", "_");	
		} else {
			this.optionlist = null;
		}
	}

	public String getOptionlist() {
		return optionlist;
	}
	
	public boolean isReferenceField(){
		if(this.getOptionlist() != null && !("").equals(this.getOptionlist()))
			return true;
		return false;
	}
	
	public String getRequiredPhase() {
		return requiredPhase;
	}

	public void setRequiredPhase(String requiredPhase) {
		this.requiredPhase = requiredPhase;
	}

	public Map<String,Object> getUi() {
		return ui;
	}

	public void setUi(Map<String,Object> ui) {
		this.ui = ui;
	}

	public String getOptionlistfilter() {
		return optionlistfilter;
	}

	public void setOptionlistfilter(String optionlistfilter) {
		this.optionlistfilter = optionlistfilter;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(boolean indexed) {
		this.indexed = indexed;
	}

	public boolean isImmutable() {
		return immutable;
	}

	public void setImmutable(boolean immutable) {
		this.immutable = immutable;
	}
	
	public TemplateField copy() throws Exception{
		TemplateField newTf = new TemplateField();
		newTf.setId(this.getId());
		newTf.setLabel(this.getLabel());
		newTf.setName(this.getName());
		newTf.setOptionlist(this.getOptionlist());
		newTf.setOptionlistfilter(this.getOptionlistfilter());
		newTf.setRequired(this.isRequired());
		newTf.setRequiredPhase(this.getRequiredPhase());
		newTf.setType(this.getType());
		newTf.setValidation(this.getValidation());
		newTf.setUi(this.getUi());
		return newTf;
	}
}
