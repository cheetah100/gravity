package nz.devcentre.gravity.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class NamedModelWrapper extends AbstractNamedModelClass {

	private static final long serialVersionUID = -8585962993697410438L;

	public static NamedModelWrapper wrap( AbstractNamedModelClass modelInstance) {
		NamedModelWrapper wrapper = new NamedModelWrapper();
		wrapper.setId(modelInstance.getId());
		wrapper.setName(modelInstance.getName());
		wrapper.setDescription(modelInstance.getDescription());
		wrapper.setMetadata(modelInstance.getMetadata());
		return wrapper;
	}
	
	public static List<NamedModelWrapper> wrapList( List<? extends AbstractNamedModelClass> list){
		List<NamedModelWrapper> newList = new ArrayList<NamedModelWrapper>();
		for(AbstractNamedModelClass item : list ) {
			newList.add( wrap(item));
		}
		return newList;
	}
	
	@Override
	@JsonIgnore
	public Metadata getMetadata() {
		return null;
	}
}
