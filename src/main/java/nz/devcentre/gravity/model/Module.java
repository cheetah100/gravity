/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.Map;

public class Module extends AbstractNamedModelClass {

	private static final long serialVersionUID = 9058252562547700632L;
	
	private Map<String,String> permissions;
	
	private Map<String, Object> ui;

	public Map<String,String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String,String> roles) {
		this.permissions = roles;
	}

	public Map<String, Object> getUi() {
		return ui;
	}

	public void setUi(Map<String, Object> ui) {
		this.ui = ui;
	}

}
