/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
import java.util.Map;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="gravity_credentials")
public class Credential extends AbstractNamedModelClass implements SecurityPermissionEntity, Cloneable {
		
	private static final long serialVersionUID = -787744857252601170L;
	
	private String tenantId;

	private String resource;	// URI of the resource to access
	
	private String method;	// SSO, Basic etc.
	
	private String requestMethod; // GET, POST, etc.
	
	private String identifier;	// User Id, Client Id etc.
	
	private String secret;		// Password, client secret etc.
	
	private Map<String,String> responseFields; // field names for access token and expiration time
	
	private Map<String,String> permissions;
		
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}
	
	public Map<String, String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String, String> permissions) {
		this.permissions = permissions;
	}

	public Map<String, String> getResponseFields() {
		if( this.responseFields==null) {
			Map<String, String> responseFields = new HashMap<>();
			this.setResponseFields(responseFields);
		}
		return this.responseFields;
	}
	
	public void setResponseFields(Map<String, String> responseFields) {
		this.responseFields = responseFields;
	}
	
	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getResource() {
		return resource;
	}

	@JsonIgnore
	public String getResourceWithCreds() {
		String result = this.resource.replace("{secret}", this.secret).replace("{id}", this.identifier);
		return result;
	}
	
	@JsonIgnore
	public String getBasicAuth() {
		return "Basic "
			+ Base64.toBase64String((identifier + ":" + secret).getBytes());
	}
	
	public void setMethod(String method) {
		this.method = method;
	}

	public String getMethod() {
		return method;
	}
	
	public String getRequestMethod() {
		return this.requestMethod;
	}
	
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
