/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;

public class BoardRule implements Serializable {

	private static final long serialVersionUID = 921497483291950847L;

	private String boardId;
	
	private String ruleId;
	
	public BoardRule(String boardId, String ruleId) {
		this.boardId = boardId;
		this.ruleId = ruleId;
	}

	public String getBoardId() {
		return boardId;
	}

	public String getRuleId() {
		return ruleId;
	}
	
	@Override
	public String toString() {
		return "BoardRule [ boardId =" + boardId+ ", ruleId=" + ruleId +  "]";
	}
	
	
}
