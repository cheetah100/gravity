/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

import nz.devcentre.gravity.model.transfer.AverageData;
import nz.devcentre.gravity.model.transfer.CountData;
import nz.devcentre.gravity.model.transfer.PercentData;
import nz.devcentre.gravity.model.transfer.PivotData;
import nz.devcentre.gravity.model.transfer.SumData;

public enum PivotType {
	
	COUNT(CountData.class,",total: {$sum:1}"),
	SUM(SumData.class,",total: {$sum:\"$totalBy\"}"),
	AVERAGE(AverageData.class,",total: {$avg:\"$totalBy\"}"),
	MIN(AverageData.class,",total: {$min:\"$totalBy\"}"),
	MAX(AverageData.class,",total: {$max:\"$totalBy\"}"),
	PERCENT(PercentData.class,null);
	
	
	private Class implClass;
	private String aggregationQuery;
	
	private PivotType( Class implClass,String query){
		this.implClass = implClass;
		this.aggregationQuery=query;
	}
	public String  getAggregationQuery(String totalBy) {
		return aggregationQuery.replaceAll("totalBy",totalBy);
	}
	public PivotData getPivotDataInstance() throws Exception {
		return (PivotData) this.implClass.newInstance();
	}
	
}
