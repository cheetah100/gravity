package nz.devcentre.gravity.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gravity_comments")
public class CardComment extends CardEvent {
	
	private static final long serialVersionUID = 2397716322280258850L;

	public CardComment() {
		setCategory("comment");
	}
}
