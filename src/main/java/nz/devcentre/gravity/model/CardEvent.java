/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "gravity_events")
public class CardEvent implements Serializable{
		
	private static final long serialVersionUID = 125114950269044716L;
	
	@Id
	private String uuid;
	
	@Indexed
	private String user;
	
	@Indexed
	private Date occuredTime;

	private String detail;
	
	private String level;

	private String category;
	
	private String board;
	
	private String phase;
	
	@Indexed
	private String card;
	
	private 
	Map<String,CardEventField> fields;
	
	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setOccuredTime(Date occuredTime) {
		this.occuredTime = occuredTime;
	}

	public Date getOccuredTime() {
		return occuredTime;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDetail() {
		return detail;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}
	
	/*
	 * commented this because it was causing problem while getting task details using spring data
	 */
	
//	public String getStatus() {
//		return IdentifierTools.getFromPath(this.path,7);
//	}

	public String getCard() {
		return card;
	}
	
	public String getPhase() {
		return phase;
	}
	
	public String getBoard() {
		return board;
	}

	@Override
	public String toString() {
		return "CardEvent [user=" + user + ", occuredTime=" + occuredTime
				+ ", detail=" + detail + ", level=" + level + ", category="
				+ category + "]";
	}

	public Map<String,CardEventField> getFields() {
		return fields;
	}

	public void setFields(Map<String,CardEventField> fields) {
		this.fields = fields;
	}
	
	public void setCard(String cardId) {
		this.card = cardId;
	}
	
	public void setPhase(String phaseId) {
		this.phase = phaseId;
	}
	
	public void setBoard(String boardId) {
		this.board = boardId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
}
