/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.data.mongodb.core.mapping.Document;

import nz.devcentre.gravity.services.QueryService;

@Document(collection="gravity_lists")
public class ListResource extends AbstractNamedModelClass {
		
	private static final long serialVersionUID = -1199910314402814094L;
	
	private ListResourceType listResourceType;
	
	private String board;
	
	private String phase;
	
	private String view;
	
	private String filter;

	private String nameField;
	
	private String order;
	
	private Map<String, Option> items;
	
	public ListResourceType getListResourceType() {
		return listResourceType;
	}

	public void setListResourceType(ListResourceType listResourceType) {
		this.listResourceType = listResourceType;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getView() {
		return view;
	}

	public void setView(String view) {
		this.view = view;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	public void setItems(Map<String, Option> items) {
		LinkedHashMap<String, Option> newMap = new LinkedHashMap<String,Option>();
		
		Map<Option,String> lookup = new HashMap<Option,String>();
		for( Entry<String,Option> entry : items.entrySet()){
			lookup.put(entry.getValue(), entry.getKey());
		}
		
		List<Option> newItems = QueryService.order(items.values());
		
		for( Option option : newItems){
			newMap.put(lookup.get(option), option);
		}
		
		this.items = newMap;
	}

	public Map<String, Option> getItems() {
		return items;
	}

	public String getNameField() {
		return nameField;
	}

	public void setNameField(String nameField) {
		this.nameField = nameField;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}
