package nz.devcentre.gravity.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gravity_alerts")
public class CardAlert extends CardEvent {
	
	private static final long serialVersionUID = -1834836293832480788L;

	public CardAlert() {
		setCategory("alert");
	}
}
