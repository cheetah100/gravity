/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_apps")
public class Application extends AbstractConfigurationClass implements Serializable, SecurityPermissionEntity {

	private static final long serialVersionUID = 9058252562547700632L;

	private Map<String,String> permissions;
	
	private Map<String,Module> modules;
	
	private Map<String,ApplicationGroup> groups;
	
	private Map<String,AppVisualisation> visualisation;
	
	@Override
	public Map<String, String> getPermissions() {
		return this.permissions;
	}

	public void setPermissions(Map<String,String> permissions) {
		this.permissions = permissions;
	}

	public Map<String,Module> getModules() {
		return modules;
	}

	public void setModules(Map<String,Module> modules) {
		this.modules = modules;
	}


	public Map<String,ApplicationGroup> getGroups() {
		return groups;
	}

	public void setGroups(Map<String,ApplicationGroup> groups) {
		this.groups = groups;

	}

	public Map<String, AppVisualisation> getVisualisation() {
		return visualisation;
	}

	public void setVisualisation(Map<String, AppVisualisation> visualisation) {
		this.visualisation = visualisation;
	}

}

