package nz.devcentre.gravity.model.importer;

public enum TransferStatusEnum {
	READY,
	INPROGRESS,
	ERROR,
	SUCCESSFUL,
	FAILED,
	PARTIAL;
}
