/**	
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.importer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Upload;
import nz.devcentre.gravity.model.UploadSheet;
import nz.devcentre.gravity.model.UploadSheetErrorDetails;
import nz.devcentre.gravity.repository.UploadRepo;
import nz.devcentre.gravity.repository.UploadSheetErrorDetailsRepo;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@Service
public class UploadService {

	private static final Logger logger = LoggerFactory.getLogger(UploadService.class);

	@Autowired
	private UploadRepo uploadRepo;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private UploadSheetErrorDetailsRepo uploadSheetErrorDetailsRepo;

	@Autowired
	private CardService cardService;

	@Autowired
	private SheetUtil sheetUtil;

	@Autowired
	SecurityTool securityTool;
	
	public static final String ERRORDOWNLOAD="ERROR-DOWNLOAD";
	public static final String ERRORMESSAGEFIELD="errorMessage";

	public Upload createUpload(String uploadId,String fileName) {
		Upload upload = new Upload();
		upload.setId(uploadId);
		upload.setFileName(fileName);
		upload.setStatus(TransferStatusEnum.INPROGRESS);
		upload.setSheets(new LinkedList<>());
		upload.addOrUpdate();
		upload = this.uploadRepo.save(upload);
		logger.info("New Upload:" + upload.getId() + " Submitted By:" + upload.getMetadata().getCreator());
		return upload;
	}
	
	private UploadSheet createUploadSheet(String boardId, String sheetName) {
		UploadSheet uploadSheet = new UploadSheet();
		uploadSheet.setBoardId(boardId);
		uploadSheet.setName(sheetName);
		uploadSheet.setStatus(TransferStatusEnum.INPROGRESS);
		uploadSheet.addOrUpdate();
		return uploadSheet;
	}

	public Upload getUpload(String uploadId) {
		return this.uploadRepo.findOne(uploadId);
	}
	
	public List<Upload> getUploadForUser(){
		SecurityContext context = SecurityContextHolder.getContext();
		return this.uploadRepo.findByMetadata_CreatorOrderByMetadata_CreatedDesc(context.getAuthentication().getName());
	}
	
	public List<Upload> getUploadForUserByStatus(TransferStatusEnum status){
		SecurityContext context = SecurityContextHolder.getContext();
		return this.uploadRepo.findByStatusAndMetadata_CreatorOrderByMetadata_CreatedDesc(status, context.getAuthentication().getName());
	}
	
	
	public void processUpload(Workbook wb, String mode, TransferStatus transferStatus)
			throws Exception {
		int numberOfSheets = wb.getNumberOfSheets();

		String uploadId = transferStatus.getId();
		String fileName = transferStatus.getFileName();
		Upload upload = this.createUpload(uploadId, fileName);
		/**
		 * At Workbook Lebvel
		 */
		int totalSuccessCount = 0;
		int totalFailCount = 0;
		int totalCreateCount = 0;
		int totalUpdateCount = 0;
	
		List<UploadSheet> uploadSheets = new LinkedList<UploadSheet>();
		
		try {
			for (int sheetno = 0; sheetno < numberOfSheets; sheetno++) {
				
				/**
				 * At Sheet level
				 */
				int successCount = 0;
				int failCount = 0;
				int createCount = 0;
				int updateCount = 0;

				Sheet sheet = wb.getSheetAt(sheetno);
				String sheetName = sheet.getSheetName();
				Board board = null;
				UploadSheet uploadSheet = new UploadSheet();
				try {
					board = boardsCache.getItem(sheetName);
					uploadSheet = this.createUploadSheet(board.getId(), sheetName);// Create a New Upload Entry-For Each Sheet
																						 
				} catch (Exception rnf) {
					logger.warn("Board Not Found: " + sheetName);
					uploadSheet = this.createUploadSheet(sheetName, sheetName);// Create a New Upload  Entry-For Each Sheet
					uploadSheet.setMessage("Board Not Found: " + sheetName);
					uploadSheet.setStatus(TransferStatusEnum.ERROR);
					uploadSheets.add(uploadSheet);
					upload.setStatus(TransferStatusEnum.ERROR);
					continue;
				}
				Map<Integer, TemplateField> headerMap = sheetUtil.readHeadersFromSheet(sheet, sheetName, board);
				Iterator<Row> iterator = sheet.rowIterator();
				Card card = null;

				while (iterator.hasNext()) {
					Row dataRow = iterator.next();

					// Skip the Header Row
					if (dataRow.getRowNum() == 0) {
						continue;
					}

					// if the entire row is blank need to Skip the row from processing
					if (dataRow.getPhysicalNumberOfCells() == 0) {
						logger.info("Blank Row found So ignore");
						continue;
					}

					try {
						card = sheetUtil.readCardRow(dataRow, headerMap);
						Card existingCard = null;

						logger.info("Upload Process: " + card.getFields().toString());

						if (card.getId() != null && !card.getId().equals("")) {
							try {
								existingCard = cardService.getCard(sheetName, card.getId());
								logger.info("Upload Process: Found existing card: " + card.getId());
							} catch (ResourceNotFoundException e) {
								logger.info("Upload Process: Card not found: " + card.getId());
							}
						}
						if (existingCard != null) {
							// PUT CALL
							String existingPhase = existingCard.getPhase();
							//Set the BoardId on card
							card.setBoard(sheetName);
							if (!card.getPhase().equalsIgnoreCase(existingPhase)) {
								logger.info("Upload Process: MOVE CARD" + card.getId() + " From phase: " + existingPhase
										+ " : To Phase: " + card.getPhase());
								cardService.moveCard(sheetName, card.getId(), card.getPhase(),null);
							}
							logger.info("Upload Process: EDIT CARD" + card.getId());
							cardService.updateCard(card, null, false);
							++updateCount; // Increment Update Count
							++totalUpdateCount;
						} else if (card != null) {
							// POST CALL
							logger.info("Upload Process: CREATE CARD");
							cardService.createCard(sheetName, card, false);
							++createCount; // Increment Create Count
							++totalCreateCount;
						}
						// Increment Success Count
						++successCount;
						++totalSuccessCount;
					} catch (Exception e) {

						++failCount;// Increment Fail Count
						++totalFailCount;

						String st = ExceptionUtils.getStackTrace(e);
						logger.warn("Upload Process: Error Row: " + dataRow.getRowNum() + e.getMessage()
								+ " StackTrace: \n" + st);
						
						//Prepare the Error Record
						UploadSheetErrorDetails errorDetails=new UploadSheetErrorDetails();
						errorDetails.setBoardId(uploadSheet.getBoardId());
						errorDetails.setUploadId(upload.getId());
						//TODO: Do we need Unique Id instead??
						errorDetails.setUploadSheetId(uploadSheet.getId()); 
						errorDetails.setErrorCard(card);
						
						if(StringUtils.isEmpty(e.getMessage())) {
							errorDetails.setErrorMessage("Generic System Error");
						}else {
							errorDetails.setErrorMessage(e.getMessage());
						}
						errorDetails.addOrUpdate();
						//Save the Error Record
						this.uploadSheetErrorDetailsRepo.save(errorDetails);
						
					}
				}
				
				// update the counts for Upload for Sheet
				uploadSheet.setCreateRows(createCount);
				uploadSheet.setUpdateRows(updateCount);
				uploadSheet.setSuccessRows(successCount);
				uploadSheet.setErrorRows(failCount);
				this.updateUploadSheetStatus(uploadSheet);// Update the Status
				logger.info("Upload Process Complete: for sheet " + sheetName);
				
				uploadSheets.add(uploadSheet);

				
			}
			logger.info("Upload Process Complete: for Workbook");
		} finally {
			wb.close();
			// this.status.setStatus(TransferStatusEnum.READY);
			//Update at Workbook level
			upload.setSuccessRows(totalSuccessCount);
			upload.setErrorRows(totalFailCount);
			upload.setCreateRows(totalCreateCount);
			upload.setUpdateRows(totalUpdateCount);
			upload.setSheets(uploadSheets);
			
			this.updateUpload(upload);
		}
	
	}
	
	private synchronized Upload updateUpload(Upload upload) {
		int successCount=upload.getSuccessRows();
		int failCount=upload.getErrorRows();
		int totalCount=successCount+failCount;
		
		if(upload.getStatus().equals(TransferStatusEnum.ERROR)) {
			logger.info("At least One of The Sheet is invalid");
		}
		else if(totalCount==successCount) {
			logger.info("All the Rows Processed");
			upload.setStatus(TransferStatusEnum.SUCCESSFUL);
		}else if(totalCount==failCount) {
			logger.info("All the Rows failed");
			upload.setStatus(TransferStatusEnum.FAILED);
		}else if(successCount>0 && failCount>0) {
			logger.info("Partial Error");
			upload.setStatus(TransferStatusEnum.PARTIAL);
		}
		upload.setTotalRows(totalCount);
		upload.setMessage(this.getMessage(successCount, failCount, upload.getFileName()));
		upload.addOrUpdate();
		return this.uploadRepo.save(upload);
	}

	private  UploadSheet updateUploadSheetStatus(UploadSheet uploadSheet) {
		
		if(uploadSheet.getStatus().equals(TransferStatusEnum.ERROR)) {
			return uploadSheet;
		}
		
		int successCount=uploadSheet.getSuccessRows();
		int failCount=uploadSheet.getErrorRows();
		int totalCount=successCount+failCount;
		
		if(totalCount==successCount) {
			logger.info("All the Rows Processed");
			uploadSheet.setStatus(TransferStatusEnum.SUCCESSFUL);
		}else if(totalCount==failCount) {
			logger.info("All the Rows failed");
			uploadSheet.setStatus(TransferStatusEnum.FAILED);
		}else if(successCount>0 && failCount>0) {
			logger.info("Partial Error");
			uploadSheet.setStatus(TransferStatusEnum.PARTIAL);
		}
		uploadSheet.setTotalRows(totalCount);
		//Set the message
		uploadSheet.setMessage(this.getMessage(successCount, failCount, uploadSheet.getName()));
		uploadSheet.addOrUpdate();
		return uploadSheet;
	}
	private String getMessage(int successCount,int failCount,String sheetName) {
		StringBuilder sb=new StringBuilder();
		if (successCount > 0) {
			sb.append("Nicely done! " + successCount + " " + sheetName + " were imported successfully.");
		}
		if (failCount > 0) {
			sb.append("Unfortunately a total of " + failCount + " " + sheetName
					+ " had an error.  Please download and review the details , correct the specified issues, and try importing a delta file to factor in the rest of your changes. Be sure you don't attempt to add any records that were already imported successfully or you will create duplicate records.");
		}
		return sb.toString();
	}

	public List<UploadSheetErrorDetails> getUploadForError(String uploadId, String  uploadSheetId) {
		String creator=SecurityContextHolder.getContext().getAuthentication().getName();
		
		if(StringUtils.isEmpty(uploadSheetId) && StringUtils.isEmpty(uploadId)) {
			return new LinkedList<>();
		}else if(!StringUtils.isEmpty(uploadSheetId) && !StringUtils.isEmpty(uploadId)) {
			return this.uploadSheetErrorDetailsRepo.findByUploadIdAndUploadSheetIdAndMetadata_Creator(uploadId, uploadSheetId, creator);
		}else if(StringUtils.isEmpty(uploadSheetId) && !StringUtils.isEmpty(uploadId)) {
			return this.uploadSheetErrorDetailsRepo.findByUploadIdAndMetadata_Creator(uploadId, creator);
		}
		return new LinkedList<>();
	}

}
