package nz.devcentre.gravity.model.importer;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;

public class SheetToMap implements SheetContentsHandler{
			
	protected static final Logger LOG = Logger.getLogger(SheetToMap.class.getName());
	
	/**
	 * Storage of Single Sheet
	 */
	private List<Map<String,String>> sheet;
	
	/**
	 * Current Row
	 */	
	private Map<String,String> row;
	
	public SheetToMap(List<Map<String,String>> sheet){
		this.sheet = sheet;
	}
	
	public void startRow(int rowNum) {
		this.row = new LinkedHashMap<String,String>();
		this.sheet.add(this.row);
	}

	public void endRow(int rowNum) {
		// Do Nothing
	}

	public void cell(String cellReference, String formattedValue, XSSFComment comment) {
		if( this.row==null){
			startRow(1);
		}
		if(!"NULL".equals(formattedValue)){
			this.row.put(getColumn(cellReference), formattedValue);	
		}
	}

	public void headerFooter(String text, boolean isHeader, String tagName) {
		// Skip, no headers or footers in CSV
	}
	
	public static String getColumn(String cellReference){
		
		if(StringUtils.isEmpty(cellReference)){
			return "";
		}
		
		boolean alpha = false;
		for( int a=1; a < cellReference.length(); a++){
			alpha = StringUtils.isAlpha(cellReference.substring(0, a));
			if(!alpha){
				return cellReference.substring(0,a-1);
			}
		}
		if(!StringUtils.isAlpha(cellReference)){
			return cellReference.substring(0,cellReference.length()-1);
			
		}
		
		return cellReference;
	}

}
