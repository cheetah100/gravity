/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model.importer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TransferStatus implements Serializable{

	private static final long serialVersionUID = 5033722259686023776L;
	
	private String id;
	
	private TransferStatusEnum status;
	
	private String errorMessage;
	
	private String user;
	
	private String fileName;
	
	private int totalSuccess;
	
	private int totalFailed;
	
	private List<String> messages;

	public TransferStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TransferStatusEnum status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<String> getMessages() {
		if(messages == null)
			messages = new ArrayList<String>();
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getId() {
		return id;
	}

	public void newId() {
		this.id = UUID.randomUUID().toString();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void resetCounts() {
		this.totalSuccess = 0;
		this.totalFailed = 0;
	}

	public int getTotalSuccess() {
		return totalSuccess;
	}

	public int getTotalFailed() {
		return totalFailed;
	}

	public void incTotalSuccess() {
		this.totalSuccess++;
	}
	
	public void incTotalFailed() {
		this.totalFailed++;
	}

}
