/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model.importer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;

@Component
public class SheetUtil {

	public static final BorderStyle BORDERSTYLE = BorderStyle.THIN; // LW
	
	private static final Logger logger = LoggerFactory.getLogger(SheetUtil.class);

	public static final String FONTNAME = "Arial";
	
	public static final int SCALE_FACTOR = 350;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private QueryService queryService;
	
	//Stores option-list, Map of <displayname, id>
	private final Map<String, Map<String,String>> optionListCache = new HashMap<String, Map<String, String>>();
	
	private final SimpleDateFormat SDF = new SimpleDateFormat("MM/dd/yyyy");
	
	private static final char CHAR_VALUE_SEPARATOR = '|';
	
	private static final Logger log = LoggerFactory.getLogger(SheetUtil.class);

	public Cell makeCell(Row row, int col, String value, CellStyle style) {
		Cell cell = row.createCell(col);
		cell.setCellValue(value);
		if (style != null) {
			cell.setCellStyle(style);
		}
		return cell;
	}

	public Map<String,CellStyle> createStyles(Workbook workbook) {
		Map<String,CellStyle> styles = new HashMap<String,CellStyle>();
		styles.put("header", createStyle(FONTNAME, 14, CellStyle.ALIGN_CENTER, HSSFColor.WHITE.index, HSSFColor.BLUE.index, true, workbook));
		styles.put("subheader", createStyle(FONTNAME, 12, CellStyle.ALIGN_LEFT, HSSFColor.WHITE.index, HSSFColor.BLUE.index, true, workbook));
		styles.put("left", createStyle(FONTNAME, 9, CellStyle.ALIGN_LEFT, HSSFColor.BLACK.index, HSSFColor.WHITE.index, true, workbook));
		styles.put("leftunlocked", createStyle(FONTNAME, 9, CellStyle.ALIGN_LEFT, HSSFColor.BLACK.index, HSSFColor.WHITE.index, false, workbook));
		return styles;
	}

	/**
	 * Create a default style with the values entered. If the sheet is protected, the cells will be locked.
	 * @param fontname
	 * @param fontPointSize
	 * @param alignment
	 * @param wb
	 * @return
	 */
	public CellStyle createStyle(String fontname, int fontPointSize, short alignment, Workbook wb) {
		return createStyle(fontname, fontPointSize, alignment, HSSFColor.BLACK.index, HSSFColor.WHITE.index, true, wb);
	}

	/**
	 * Create a default style with the values entered. If the sheet is protected, the cells will be set to lockState.
	 * @param fontname
	 * @param fontPointSize
	 * @param alignment
	 * @param wb
	 * @return
	 */
	public CellStyle createStyle(String fontname, int fontPointSize, short alignment, short fontColor, short bgColor, boolean lockState, Workbook wb) {

		CellStyle style = wb.createCellStyle();

		Font font = wb.createFont();
		font.setFontHeightInPoints((short) fontPointSize);
		font.setFontName(fontname);
		font.setColor(fontColor);
		
		style.setFont(font);
		style.setAlignment(alignment);
		style.setBorderBottom(BORDERSTYLE);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(BORDERSTYLE);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderRight(BORDERSTYLE);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(BORDERSTYLE);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setFillForegroundColor(bgColor);
		style.setLocked(lockState);
		style.setWrapText(true);

		return style;
	}

	public void sizeColumns(Sheet sheet, int numberOfColumns) {
		Row row0 = sheet.getRow(1);
		Row row1 = sheet.getRow(2);
		
		for (int i = 0; i < numberOfColumns; i++) {
			Cell cell0 = row0.getCell(i);
			Cell cell1 = null;
			if( row1!=null){
				cell1 = row1.getCell(i);
			}
			
			int width = 500;
			
			if(cell0!=null){
				int newwidth = cell0.getStringCellValue().length() * SCALE_FACTOR;
				if(newwidth > width){
					width = newwidth;
				}
			}

			if(cell1!=null){
				int newwidth = cell1.getStringCellValue().length() * SCALE_FACTOR;
				if(newwidth > width){
					width = newwidth;
				}
			}
			
			if( width>(350*50)){
				width = 350*50;
			}
				
			
			sheet.setColumnWidth(i, width);
		}
	}

	public String[] getStrings(Object[] objects) {
		String[] values = new String[objects.length];
		for (int f = 0; f < objects.length; f++) {
			values[f] = objects[f].toString();
		}
		return values;
	}

	public void createConstraint(Sheet sheet, String[] list, int fromRow, int toRow, int column) {
		
		XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet)sheet);
		XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)
		    dvHelper.createExplicitListConstraint(list);
		
		CellRangeAddressList addressList = new CellRangeAddressList();
		CellRangeAddress userReferenceRange = new CellRangeAddress(fromRow, toRow, column, column);
		addressList.addCellRangeAddress(userReferenceRange);
		
		XSSFDataValidation validation = (XSSFDataValidation)dvHelper.createValidation(dvConstraint, addressList);	
		validation.setShowErrorBox(true);
		sheet.addValidationData(validation);	
	}
	
	/**
	 * This method inverts a Map<String,String> so that it is possible to look up a based on the values rather than keys.
	 * This is useful when dealing with headers, which originally come in a Column,Name form. The inversion allows lookup
	 * on the name.
	 * 
	 * @param headers
	 * @return
	 */
	public Map<String,String> getHeaderMap( Map<String,String> headers){
		Map<String,String> resultMap = new HashMap<String,String>();
		for( Entry<String,String> entry: headers.entrySet()){
			resultMap.put(entry.getValue(), entry.getKey());
		}
		return resultMap;
	}
	
	public int getIntFromColumnId( String id) throws Exception{
		int v = 0;
		for( int f=0; f<id.length();f++){
			int i = getIntFromChar(id.charAt(f));
			int m = pow(26,(id.length() - (f + 1)));
			v = v + (i * m);
			
		}
		return v;
	}
	
	public String getColumnIdFromInt( int col){		
		if( (col-1)/26<1 ){
			char[] c = new char[1];
			c[0] = (char)(col + 64);
			return new String(c);
		} else {
			char[] c = new char[2];
			int c1 = ((col-1)/26);
			c[0] = (char)(c1 + 64);
			int c2 = ((col-1) % 26)+1;
			c[1] = (char)(c2 + 64);
			return new String(c);
		}
	}
	
	protected int getIntFromChar( char c ) throws Exception{
		int f = c - 64;;
		if(f<0 || f>26){
			throw new Exception("Character Out of Bounds");
		}
		return f;
	}
	
	public int pow(int a, int b)
    {
		int power = 1;
		for(int c=0;c<b;c++)
		power*=a;
		return power;
    }
	
	/**
	 * Create a secure worksheet. This method is a dropin replacement for the
	 * ordinar createSheet method. If password is not null and not empty, a
	 * secure spreadsheet will be created. This sheet will have all cells locked
	 * to user input (but still available programmatically). These cells must be
	 * explicitely unlocked to make them available to the user for editing. Use
	 * the lockCell method for this.
	 * 
	 * @return
	 */
	static Sheet createSecureSheet(Workbook wb, String title, String password) {
		Sheet aSheet = wb.createSheet(title);
		if (password != null && password.length() > 0) {
			aSheet.protectSheet(password);
		}
		return aSheet;
	}
	
	public Date getDateFromString( String dateString ){
		if( StringUtils.isEmpty(dateString) ){
			return null;
		}
		if( dateString.length() != 10){
			return null;
		}
		Date parse;
		try {
			parse = SDF.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
		return parse; 
	}
	
	public String getStringFromDate( Date dateString ){
		if( dateString == null ){
			return null;
		}
		String parse;
		try {
			parse = SDF.format(dateString);
		} catch (Exception e) {
			return null;
		}
		return parse; 
	}
	
	public void writeHeaderRow(Sheet sheet, Map<String,String> data){
		Row header = sheet.createRow(0);
		int i=0;
		for(Entry<String, String> entry: data.entrySet()){
			this.makeCell(header, i, entry.getValue(), null);
			i++;
		}
	}
	
	public void writeCardRow(Sheet sheet, Card card, Board board, int startRow){
		
		Row dataRow = sheet.createRow(startRow);
		//Add Id Data
		this.makeCell(dataRow, 0, card.getId(), null);
		this.makeCell(dataRow, 1, card.getPhase(), null);
		int i=2;
				
		for(Entry<String, TemplateField> entry: board.getFields().entrySet()){
			try {
				TemplateField field = entry.getValue();
				Object fieldValue = card.getFields().get(field.getName());
				logger.debug("Is reference field: " + field.getId() + ":" + field.isReferenceField());
				if(field.isReferenceField() && fieldValue != null){
					logger.debug("Before  reference field: " + fieldValue);
					fieldValue = resolveReferenceField(field, fieldValue);
					logger.debug("After reference resolution: " + fieldValue);
				}else{
					fieldValue = getStringValue(fieldValue);
				}
				if(fieldValue != null) {
					this.makeCell(dataRow, i, fieldValue.toString(), null);
				}
				else {
					this.makeCell(dataRow, i, "", null);
				}
				i++;
			} catch (Exception e) {
				log.info("writeCardRow Exception", e);
			}
		}
		//Write Error Field If available- Used mostly for Error Download
		 if(card.getFields().containsKey(UploadService.ERRORMESSAGEFIELD)){
				Object fieldValue = card.getFields().get(UploadService.ERRORMESSAGEFIELD);
				this.makeCell(dataRow, i, fieldValue.toString(), null);
		 }
	}

	/*
	 * Maps the first row of the headers to the template's fields.
	 * The Map returned preserves the order in which to read the data
	 * 
	 * */
	public Map<Integer,TemplateField> readHeadersFromSheet(Sheet sheet, String boardId, Board board){
		Map<Integer, TemplateField> colHeaders = new LinkedHashMap<Integer,TemplateField>();
		try {
			Row headerRow = sheet.getRow(0);
			Iterator<Cell> cellIterator = headerRow.iterator();
            while (cellIterator.hasNext()) {

                Cell currentCell = cellIterator.next();
                String headerName = currentCell.getStringCellValue();
                if(headerName.equalsIgnoreCase("id")){
                	TemplateField fieldId = new TemplateField();
                	fieldId.setId("id");
                	fieldId.setName("id");
                	fieldId.setType(FieldType.STRING);
                	colHeaders.put(0, fieldId);
                }else if(headerName.equalsIgnoreCase("phase")){
                	TemplateField fieldId = new TemplateField();
                	fieldId.setId("phase");
                	fieldId.setName("phase");
                	fieldId.setType(FieldType.STRING);
                	colHeaders.put(1, fieldId);
                }else{
                	TemplateField field = board.getFieldFromLabel(headerName);
                	if(field!=null){
                		colHeaders.put(currentCell.getColumnIndex(), field);
                		if(field.isReferenceField()){
                			optionListCache.put(field.getOptionlist(), buildOptionListCache(field.getOptionlist()));
                		}
                	}
                }
            }
			
		} catch (Exception e) {
			log.info("readHeadersFromSheet Exception", e);
		}
		return colHeaders;
	}
	
	private Map<TemplateField, Cell> getCellTemplateMap(Row dataRow, Map<Integer,TemplateField> headerMap){
		int numberOfColumns = dataRow.getLastCellNum();
		log.info("Number of Columns = " + numberOfColumns);
		Map<TemplateField, Cell> cellTemplateMap = new LinkedHashMap<TemplateField, Cell>();
		for(int i = 0; i < numberOfColumns; i++){
			Cell cell = dataRow.getCell(i,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
			TemplateField field = headerMap.get(i);
			if(field!=null){
				cellTemplateMap.put(field, cell);
			}
		}
		return cellTemplateMap;
	}
	
	private Map<String, String> buildOptionListCache(String boardId){
		Map<String,String> cache = new HashMap<String,String>();
		try {
			
			Collection<Card> cards = this.queryService.query(boardId, null, null, "all", false);
			
			log.info("buildOptionListCache: For BOARD : " + boardId + ": Number of cards: " + cards.size());
			for(Card card :cards){
				String title = cardService.getTitle(card);
				if(cache.containsKey(title)){
					log.warn("ALARM: Card Title Not Unique " + card.toString() + ":" + title);
				}else if(title == null || ("").equals(title)){
					log.warn("ALARM: Card Title Is Blank " + card.toString());
				}
				
				if(title != null) {
					cache.put(title,card.getId());
				}
			}
			log.info("buildOptionListCache: Number of cards in cache: " + cache.size());
			
		} catch (Exception e) {
			log.warn("Exception building Map: " + boardId + " : " + e.getMessage(), e);
		}
		
		return cache;
	}
	
	public Card readCardRow(Row dataRow, Map<Integer,TemplateField> headerMap) {
		
		Map<TemplateField, Cell> cellTemplateMap = getCellTemplateMap(dataRow, headerMap);
		Card card2 = new Card();
		for(Entry<TemplateField, Cell> entry: cellTemplateMap.entrySet()){
				
			TemplateField field = entry.getKey();
			Cell cell = entry.getValue();
			
			if( field==null ){
				log.warn("readCardRow: Field Is Null");
				continue;
			}

			if( cell == null){
				log.warn("readCardRow: Cell Is Null");
				continue;
			}
			
			log.info("readCardRow: Start reading " + field.getId() + ": " + field.getType() + " : " + cell.getCellType());
			
			Object xlsFieldValue = null;
			if(field.getType().equals(FieldType.DATE) && cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
				xlsFieldValue = cell.getDateCellValue();
				log.info("readCardRow: DATE-NUMERIC" + xlsFieldValue);
			} else if(field.getType().equals(FieldType.DATE) && cell.getCellType() == Cell.CELL_TYPE_STRING){
				xlsFieldValue = getDateFromString(cell.getStringCellValue());
				log.info("readCardRow: DATE-STRING" + xlsFieldValue);
			} else if(cell.getCellType() == Cell.CELL_TYPE_NUMERIC){
				xlsFieldValue = Double.toString(cell.getNumericCellValue());
			} else if(cell.getCellType() == Cell.CELL_TYPE_BOOLEAN){
				xlsFieldValue = Boolean.toString(cell.getBooleanCellValue());
			} else {
				xlsFieldValue = cell.getStringCellValue();
			}
			
			if(xlsFieldValue==null){
				log.info("Excel Value Null : " + field.getName());
			} else {
				log.info("Excel Value Found : " + field.getName() + " = " + xlsFieldValue.toString());	
			}
			
			if(field.getId().equalsIgnoreCase("id")){
				log.info("readCardRow: Entering(1)");
				String id = cell.getStringCellValue();
				if(id == null || id.isEmpty()){
					card2.setId(null);
				} else {
					card2.setId(cell.getStringCellValue());
				}
			} else if(field.getId().equalsIgnoreCase("phase")){
				log.info("readCardRow: Entering(2)");
				card2.setPhase(cell.getStringCellValue());
			}else {
				log.info("readCardRow: Entering(3)");
				if(field.isReferenceField()) {
					Map<String, String> refCache = optionListCache.get(field.getOptionlist());
					List<String> idList = new ArrayList<String>(); 
					String valueFromFile = cell.getStringCellValue();
					if(FieldType.LIST.equals(field.getType())){
						if(valueFromFile != null && !valueFromFile.isEmpty()){
							String[] strings = StringUtils.split(valueFromFile.trim(), CHAR_VALUE_SEPARATOR);
							for(String each: strings){
								String id = refCache.get(each);
								idList.add(id);
							}
						}
						card2.addField(field.getId(), idList);
						log.info("readCardRow(1): " + field.getId() + ": " + idList);
					} else {
						String id = refCache.get(valueFromFile);
						if(id == null){
							log.warn("WARN: Unable to find related Id for " + field.getId() + ": " + id+ " : " + valueFromFile + ":");
						}
						card2.addField(field.getId(), id);
						log.info("readCardRow(2): " + field.getId() + ": " + id);
					}
				} /*Handle LIST type field ,which doesn't refer to any external board*/
				else if (FieldType.LIST.equals(field.getType())) {
					String valueFromFile = cell.getStringCellValue();
					if (valueFromFile != null && !valueFromFile.isEmpty()) {
						String[] strings = StringUtils.split(valueFromFile.trim(), CHAR_VALUE_SEPARATOR);
						List<String> valueList = Arrays.asList(strings);
						card2.addField(field.getId(), valueList);
						log.info("readCardRow(3): " + field.getId() + ": " + valueList);
					}
				}else {
					log.info("readCardRow: Entering(4)");
					card2.addField(field.getId(),  xlsFieldValue);
					log.info("readCardRow(3): " + field.getId() + ": " + xlsFieldValue);
				}	
			}
		}

		log.info("readCardRow: Card built" );
		return card2;
	}
	
	public Object convertXLSStringToTemplateDataType(String dataType, String cellValue){
		return cellValue;
	}
	
	public String resolveReferenceField(TemplateField fld, Object fieldValue){
		try{
			if(fieldValue != null && fieldValue instanceof List){
				List arrayl = (List)fieldValue;
				List<String> strings = new ArrayList<String>();
				for(Object each: arrayl){
					Card referenceCard = cardService.getCard(fld.getOptionlist(), (String)each);
					if(referenceCard != null){
						strings.add(cardService.getTitle(referenceCard));
					}
				}
				return StringUtils.join(strings, CHAR_VALUE_SEPARATOR);
				
			}else if(fieldValue != null && fieldValue instanceof String[]){
				List<String> strings = new ArrayList<String>();
				String[] array1 = (String[])fieldValue;
				for(Object each: array1){
					Card referenceCard = cardService.getCard(fld.getOptionlist(), (String)each);
					if(referenceCard != null){
						strings.add(cardService.getTitle(referenceCard));
					}
				}
				return StringUtils.join(strings, CHAR_VALUE_SEPARATOR);
				
			}else if(fieldValue != null && fieldValue instanceof Object[]){
				List<String> strings = new ArrayList<String>();
				Object[] array1 = (Object[])fieldValue;
				for(Object each: array1){
					Card referenceCard = cardService.getCard(fld.getOptionlist(), (String)each);
					if(referenceCard != null){
						strings.add(cardService.getTitle(referenceCard));
					}
				}
				return StringUtils.join(strings, CHAR_VALUE_SEPARATOR);
				
			}	
			else{
				if(fieldValue != null && !("").equals(fieldValue.toString()) && !("null").equals(fieldValue.toString())){
					Card referenceCard = cardService.getCard(fld.getOptionlist(), (String)fieldValue);
					if(referenceCard != null){
						return cardService.getTitle(referenceCard);
					}
				}
			}
		}catch(Exception e){
			return ("Reference Error on field: " + fld.getName() + ": Id " + fieldValue + " does not exist on board " + fld.getOptionlist());
		}
		return null;
	}
	
	public String getStringValue(
			Object value) {
		String stringvalue ="";
 		if(value != null){
			if( value instanceof Date){
				Date date = (Date)value;
	    		stringvalue = getStringFromDate(date);
	    	} else if( value instanceof List<?>){
	    			List<String> stringList = (List<String>)value;
	    			stringvalue = StringUtils.join(stringList, CHAR_VALUE_SEPARATOR);
	    	
	    	} else if(value instanceof String){
	    		if(!((String)value).equalsIgnoreCase("null")){
	    			stringvalue = value.toString();
	    		}
	    	} else {
	    		stringvalue = value.toString();
	    	}
 		}
    	return stringvalue;
	}
}
