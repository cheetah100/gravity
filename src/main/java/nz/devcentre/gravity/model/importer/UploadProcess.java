package nz.devcentre.gravity.model.importer;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import nz.devcentre.gravity.controllers.ImportController;

public class UploadProcess implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(UploadProcess.class);
	

	private ImportController controller;

	private Workbook wb;

	private String mode;

	Authentication auth;
	
	TransferStatus transferStatus;

	public UploadProcess( Workbook wb,String mode,Authentication auth,TransferStatus transferStatus,ImportController controller) {
		this.wb=wb;
		this.mode=mode;
		this.auth=auth;
		this.transferStatus=transferStatus;
		this.controller=controller;
	}
	@Override
	public void run() {
		this.setUser();
		try {
			this.controller.uploadService.processUpload(this.wb,this.mode,this.transferStatus);
		} catch (Exception e) {
			logger.warn("Workbook Generation Exception", e);
		}
	}
	
	private void setUser() {
		logger.info("Setting Up Import Process with User " + this.auth.getPrincipal());
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(auth);
	}
}
