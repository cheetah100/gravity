package nz.devcentre.gravity.model.importer;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import nz.devcentre.gravity.controllers.ImportController;

public class ImportProcess implements Runnable {
	
	private static final Logger logger = LoggerFactory.getLogger(ImportProcess.class);

	private ImportController controller;
	
	private Workbook wb;
	
	private String mode;
	
	Authentication auth;
	
	public ImportProcess(ImportController controller, Workbook wb, Authentication auth, String mode) {
		this.controller = controller;
		this.mode = mode;
		this.wb = wb;
		this.auth = auth;
	}
	
	@Override
	public void run() {
		setUser();
		try {
			this.controller.processSheets(this.wb, mode);
		} catch (Exception e) {
			logger.warn("Exception in processing Import file", e);
		}
	}
	
	private void setUser() {
		logger.info("Setting Up Import Process with User " + this.auth.getPrincipal());
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(auth);
	}
}
