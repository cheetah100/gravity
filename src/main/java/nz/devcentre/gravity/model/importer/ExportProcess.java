package nz.devcentre.gravity.model.importer;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import nz.devcentre.gravity.controllers.ExportController;
import nz.devcentre.gravity.model.Filter;

public class ExportProcess implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(ExportProcess.class);

	private ExportController controller;

	private String config;

	private String mode;

	private String filter;

	private String view;

	private Authentication auth;

	private Filter dynamicFilter;

	public ExportProcess(ExportController controller, Authentication auth, String config, String mode, String filter,
			String view) {

		this.controller = controller;
		this.auth = auth;
		this.config = config;
		this.mode = mode;
		this.filter = filter;
		this.view = view;
		this.dynamicFilter = null;

	}

	// Used to Initialize the Dynamic Filter
	public ExportProcess(ExportController controller, Authentication auth, String config, String mode, String filter,
			String view, Filter dynamicFilter) {

		this.controller = controller;
		this.auth = auth;
		this.config = config;
		this.mode = mode;
		this.filter = filter;
		this.view = view;
		this.dynamicFilter = dynamicFilter;
	}

	@Override
	public void run() {
		setUser();
		try {
			controller.getStatus().setStatus(TransferStatusEnum.INPROGRESS);
			// IF Error Download
			
			if (!StringUtils.isEmpty(mode) && mode.equals(UploadService.ERRORDOWNLOAD)) {
				/**
				 * This will not call the cardcontroller, will call the actual error cards from
				 * error details Send the workbook/uploadId as config Send the sheetId as view
				 */
				logger.info("Export Error file");
				controller.setWorkbook(this.controller.generateWorkbook(config, view));
			}else if (this.dynamicFilter != null) {
				// If Dynamic Filter value is provided
				// Run Search On Board With Dynamic Filter
				controller.setWorkbook(this.controller.generateWorkbook(config, mode, filter, view, dynamicFilter));
			} else {
				// Run search on Board without Dynamic Filter
				controller.setWorkbook(this.controller.generateWorkbook(config, mode, filter, view));
			}

			controller.getStatus().setStatus(TransferStatusEnum.READY);
		} catch (Exception e) {
			controller.getStatus().setStatus(TransferStatusEnum.ERROR);
			controller.getStatus().setErrorMessage(e.getMessage());
			logger.warn("Exception in processing Export	 file",e);
		}
	}

	private void setUser() {
		logger.info("Setting Up Import Process with User " + this.auth.getPrincipal());
		SecurityContext context = SecurityContextHolder.getContext();
		context.setAuthentication(auth);
	}
}
