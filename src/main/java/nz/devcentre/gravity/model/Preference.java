package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import nz.devcentre.gravity.tools.IdentifierTools;

@Document(collection="gravity_user_preferences")
public class Preference implements Serializable {

	private static final long serialVersionUID = 3533950586526412321L;
	
	@Id
	private String uniqueid;
	
	@Indexed
	protected String id;
		
	private Map<String,Object> values;
	
	@Indexed
	private String userId;
	
	private String name;
	
	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	
	private void update(){
		if( this.userId!=null && this.id!=null) {
			this.uniqueid = this.userId + "_" + this.id;
		}
	}	
	
	public String getId(){
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
		update();
	}

	public void setName(String name) {
		this.name = name;
		if (id==null) {
			setId(IdentifierTools.getIdFromName(name));
		}
	}

	public String getName() {
		return name;
	}

	public Map<String,Object> getValues() {
		return values;
	}

	public void setValues(Map<String,Object> values) {
		this.values = values;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
		update();
	}
}
