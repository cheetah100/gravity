/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Map;

public class AccessControlResource implements Serializable{
	
	private static final long serialVersionUID = -1004708710895244970L;

	private String resourceType;
	
	private String resourceId;
	
	private AccessType accessType;

	private Map<String, AccessType> fields;
	
	private Map<String,Condition> conditions;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public AccessType getAccessType() {
		return accessType;
	}

	public void setAccessType(AccessType accessType) {
		this.accessType = accessType;
	}

	public Map<String, AccessType> getFields() {
		return fields;
	}

	public void setFields(Map<String, AccessType> fields) {
		this.fields = fields;
	}

	public Map<String,Condition> getConditions() {
		return conditions;
	}

	public void setConditions(Map<String,Condition> conditions) {
		this.conditions = conditions;
	}
}
