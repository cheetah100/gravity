/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_rules")
public class Rule extends AbstractBoardClass {
	
	private static final long serialVersionUID = 5907868381519704015L;
	
	private String taskName;

	private Map<String,Condition> taskConditions;
	
	private Map<String,Condition> automationConditions;
	
	private Map<String,Condition> completionConditions;
	
	private Map<String,Action> actions;
	
	private RuleType ruleType;
	
	private AssignmentStrategyName assignmentStrategy;
	
	private String assignto;
	
	private String schedule;
	
	public Map<String, Condition> getTaskConditions() {
		return taskConditions;
	}

	public void setTaskConditions(Map<String, Condition> taskConditions) {
		this.taskConditions = taskConditions;
	}

	public Map<String, Condition> getAutomationConditions() {
		return automationConditions;
	}

	public void setAutomationConditions(Map<String, Condition> automationConditions) {
		this.automationConditions = automationConditions;
	}

	public void setActions(Map<String,Action> actions) {
		this.actions = actions;
	}

	public Map<String,Action> getActions() {
		return actions;
	}

	public RuleType getRuleType() {
		return ruleType;
	}

	public void setRuleType(RuleType ruleType) {
		this.ruleType = ruleType;
	}

	public AssignmentStrategyName getAssignmentStrategy() {
		return assignmentStrategy;
	}

	public void setAssignmentStrategy(AssignmentStrategyName assignmentStrategy) {
		this.assignmentStrategy = assignmentStrategy;
	}

	public String getAssignto() {
		return assignto;
	}

	public void setAssignto(String assignto) {
		this.assignto = assignto;
	}

	public Map<String,Condition> getCompletionConditions() {
		return completionConditions;
	}

	public void setCompletionConditions(Map<String,Condition> completionConditions) {
		this.completionConditions = completionConditions;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	
}
