/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CardEventHistory extends AbstractBaseModelClass implements Serializable{
		
	private static final long serialVersionUID = 125114950269044716L;
	
	
	private String user;
	private Date occuredTime;
	private String detail;
	private String level;
	private String boardId;
	private String phaseId;
	private String cardId;
	private String category;
	private List<CardEventField> fields;
	
	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}

	public void setOccuredTime(Date occuredTime) {
		this.occuredTime = occuredTime;
	}

	public Date getOccuredTime() {
		return occuredTime;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getDetail() {
		return detail;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public String getCard() {
		return cardId;
	}
	
	public String getPhase()  {
		return phaseId;
	}
	
	public String getBoard() {
		return boardId;
	}

	@Override
	public String toString() {
		return "CardEvent [user=" + user + ", occuredTime=" + occuredTime
				+ ", detail=" + detail + ", level=" + level + ", category="
				+ category + "]";
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getPhaseId() {
		return phaseId;
	}

	public void setPhaseId(String phaseId) {
		this.phaseId = phaseId;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public List<CardEventField> getFields() {
		return fields;
	}

	public void setFields(List<CardEventField> fields) {
		this.fields = fields;
	}
}
