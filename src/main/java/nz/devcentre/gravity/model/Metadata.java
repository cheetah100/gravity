/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Date;

import nz.devcentre.gravity.security.SecurityTool;

public class Metadata implements Serializable, Cloneable{
	
	private static final long serialVersionUID = 6554247119174282873L;

	// private Long version;
	
	private String creator;
	
	private Date created;
	
	private String modifiedby;
	
	private Date modified;
	
	public Metadata() {
		this.created = new Date();
		this.creator = SecurityTool.getCurrentUser();
		//this.version = 1l;
	}
	
	public void update() {
		this.modified = new Date();
		this.modifiedby = SecurityTool.getCurrentUser();
		//this.version++;
	}

	/*
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	*/

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
