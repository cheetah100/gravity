package nz.devcentre.gravity.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "gravity_upload_errors")
public class UploadSheetErrorDetails extends AbstractBoardClass{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String uploadId;
	private String uploadSheetId;
	private Card errorCard;
	private String errorMessage;
	/**
	 * @return the uploadId
	 */
	public String getUploadId() {
		return uploadId;
	}
	/**
	 * @param uploadId the uploadId to set
	 */
	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}
	/**
	 * @return the uploadSheetId
	 */
	public String getUploadSheetId() {
		return uploadSheetId;
	}
	/**
	 * @param uploadSheetId the uploadSheetId to set
	 */
	public void setUploadSheetId(String uploadSheetId) {
		this.uploadSheetId = uploadSheetId;
	}
	/**
	 * @return the errorCard
	 */
	public Card getErrorCard() {
		return errorCard;
	}
	/**
	 * @param errorCard the errorCard to set
	 */
	public void setErrorCard(Card errorCard) {
		this.errorCard = errorCard;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@Override
	@JsonIgnore
	public boolean isActive() {
		// TODO Auto-generated method stub
		return super.isActive();
	}
	@Override
	@JsonIgnore
	public boolean isPublish() {
		// TODO Auto-generated method stub
		return super.isPublish();
	}
	@Override
	@JsonIgnore
	public String getVersion() {
		// TODO Auto-generated method stub
		return super.getVersion();
	}
	
	
}
