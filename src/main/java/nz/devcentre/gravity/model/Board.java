/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.beans.Transient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="gravity_boards")
public class Board extends AbstractConfigurationClass implements Serializable, SecurityPermissionEntity {

	private static final long serialVersionUID = -502272775478053887L;
		
	private String orderField;
	
	private BoardType boardType;
	
	private String tableName;
	
	private boolean history;
	
	private KeyMechanism keyMechanism = KeyMechanism.NONE;
	
	private String invalidCardPhase;
	
	private String prefix = "";
	
	private String titleField = null;
	
	private String keyField = null;
	
	private String credentialId = null;
	
	private String phaseField = null;

	private Map<String,String> permissions = new HashMap<String,String>();
	
	private Map<String,Phase> phases = new HashMap<String,Phase>();
	
	private Map<String,Filter> filters = new HashMap<String,Filter>();

	private Map<String,View> views = new HashMap<String,View>();
	
	private Map<String,TemplateField> fields = new HashMap<String,TemplateField>();

	public void setPhases(Map<String,Phase> phases) {
		this.phases = phases;
	}

	public Map<String,Phase> getPhases() {
		return phases;
	}
	
	public Phase getPhase(String phaseId){
		if(phaseId==null || this.phases==null) return null;
		if(this.phases.containsKey(phaseId)){
			return this.phases.get(phaseId);
		} else {
			return null;
		}
	}
	
	public boolean isPhaseInvisible(String phaseId){
		Phase phase = getPhase(phaseId);
		if(phase!=null){
			return phase.isInvisible();
		} else {
			return true;
		}
	}
	
	@Transient
	public String getDefaultPhase(){
		Phase returnPhase = null;
		int v = 1000;
		for(Phase phase : this.phases.values()){
			//Ignore the Invisible Phases
			if( phase.getIndex()<v && !phase.isInvisible()) {
				returnPhase = phase;
				v = phase.getIndex();
				if(v==1){
					return returnPhase.getId();
				}
			}
		}
		if(returnPhase!=null){
			return returnPhase.getId();
		}
		return null;
	}
	
	public void setFilters(Map<String,Filter> filters) {
		this.filters = filters;
	}

	public Map<String,Filter> getFilters() {
		return filters;
	}
	
	public Filter getFilter(String filterId){
		if(filterId==null || this.filters==null) return null;
		if(this.filters.containsKey(filterId)){
			return this.filters.get(filterId);
		} else {
			return null;
		}
	}

	public void setViews(Map<String,View> views) {
		this.views = views;
	}

	public Map<String,View> getViews() {
		return views;
	}
	
	public View getView(String viewId){
		if(viewId==null || this.views==null) return null;
		if(this.views.containsKey(viewId)){
			return this.views.get(viewId);
		} else {
			return null;
		}
	}
	
	public void setPermissions(Map<String, String> permissions) {
		this.permissions = permissions;
	}
	
	public Map<String,String> getPermissions() {
		Map<String,String> returnPermissions = new HashMap<String,String>();
		if(this.permissions!=null){
			returnPermissions.putAll(this.permissions);
		}
		if(this.views!=null){
			for( View view : this.views.values()){
				if(view.getPermissions()!=null){
					returnPermissions.putAll(view.getPermissions());
				}
			}
		}
		if(this.filters!=null){
			for( Filter filter : this.filters.values()){
				if(filter.getPermissions()!=null){
					returnPermissions.putAll(filter.getPermissions());
				}
			}		
		}
		return returnPermissions;
	}
	
	public Map<String,String> getRootPermissions() {
		return this.permissions;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public BoardType getBoardType() {
		return boardType;
	}

	public void setBoardType(BoardType boardType) {
		this.boardType = boardType;
	}

	public boolean isHistory() {
		return history;
	}

	public void setHistory(boolean history) {
		this.history = history;
	}

	public String getInvalidCardPhase() {
		return invalidCardPhase;
	}

	public void setInvalidCardPhase(String invalidCardPhase) {
		this.invalidCardPhase = invalidCardPhase;
	}

	public Map<String,TemplateField> getFields() {
		return fields;
	}
	
	public TemplateField getField(String name) {
		if( name!=null && this.fields.containsKey(name)) {
			return fields.get(name);
		}
		return null;
	}

	public void setFields(Map<String,TemplateField> fields) {
		this.fields = fields;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@JsonIgnore
	public Map<String,TemplateField> getAllRequiredFields(){
		Map<String,TemplateField> result = new HashMap<String,TemplateField>();
		for( TemplateField field : this.fields.values()){
			if( field.isRequired() || field.getRequiredPhase()!=null){
				result.put(field.getName(), field);
			}
		}
		return result;
	}
	
	@JsonIgnore
	public Map<String,TemplateField> getReferenceFields(){
		Map<String,TemplateField> referenceFields = new HashMap<String, TemplateField>();
		for( TemplateField fld: fields.values()){
			if(fld.isReferenceField())
				referenceFields.put(fld.getId(), fld);
		}
		return referenceFields;
	}
	
	@JsonIgnore
	public TemplateField getFieldFromLabel(String label){		
		for( TemplateField tf : this.fields.values()) {
			if(tf.getLabel().equals(label)) {
				return tf;
			}
		}
		return null;
	}
	
	public boolean isReferenceField(String fieldName){
		TemplateField templateField = this.getField(fieldName);
		if(templateField != null && templateField.isReferenceField())
			return true;
		return false;
	}
	
	@JsonIgnore
	public Map<String,TemplateField> getTeamConstraints(){
		Map<String,TemplateField> result = new HashMap<String,TemplateField>();
		for( TemplateField field : this.getFields().values()){
			if( "team".equals(field.getValidation())){
				result.put(field.getName(), field);
			}
		}
		return result;
	}
	
	public TemplateField getFieldByOptionList( String optionList ){
		for( TemplateField tf : this.fields.values()){
			if( tf.getOptionlist()!= null && tf.getOptionlist().equals(optionList)){
				return tf;
			}
		}
		return null;
	}

	public String getTitleField() {
		return titleField;
	}

	public void setTitleField(String titleField) {
		this.titleField = titleField;
	}

	public String getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(String credentialId) {
		this.credentialId = credentialId;
	}

	public String getKeyField() {
		return keyField;
	}

	public void setKeyField(String keyField) {
		this.keyField = keyField;
	}

	public String getPhaseField() {
		return phaseField;
	}

	public void setPhaseField(String phaseField) {
		this.phaseField = phaseField;
	}

	public KeyMechanism getKeyMechanism() {
		return keyMechanism;
	}

	public void setKeyMechanism(KeyMechanism keyMechanism) {
		this.keyMechanism = keyMechanism;
	}

	public String getTableName() {
		if( tableName!=null) {
			return tableName;
		} else {
			return getId();
		}
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
