/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Filter extends AbstractNamedModelClass implements Cloneable{
		
	private static final long serialVersionUID = -3218537113174226440L;
	
	private String boardId;
	
	private String expression;
	
	private String owner;
	
	private String phase;
	
	private AccessType access;
	
	private Map<String,Condition> conditions;
	
	private Map<String,String> permissions;

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getExpression() {
		return expression;
	}

	public void setConditions(Map<String,Condition> conditions) {
		this.conditions = conditions;
	}

	public Map<String,Condition> getConditions() {
		return conditions;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setAccess(AccessType access) {
		this.access = access;
	}

	public AccessType getAccess() {
		return access;
	}
	
	public List<String> getExcludedPhases(){
		List<String> result = new ArrayList<String>();
		if( conditions!=null){
			for( Condition condition : conditions.values()){
				if( condition.getOperation().equals(Operation.NOTPHASE)){
					result.add(condition.getFieldName());
				}
			}
		}
		return result;
	}

	public Map<String,String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String,String> permissions) {
		this.permissions = permissions;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Filter f = new Filter();
		f.setAccess(access);
		f.setBoardId(boardId);
		Map<String,Condition> copyconditions = new HashMap<String,Condition>();
		for (String key : conditions.keySet()) {
			copyconditions.put(key, (Condition)conditions.get(key).clone());
		}
		f.setConditions(copyconditions);
		f.setExpression(expression);
		f.setId(id);
		f.setMetadata(getMetadata() != null ?(Metadata)getMetadata().clone(): null);
		f.setName(this.getName());
		f.setOwner(owner);
		Map<String,String> copypermissions = new HashMap<String,String>();
		if (permissions!=null) {
			for (String key : permissions.keySet()) {
				copypermissions.put(key, permissions.get(key));
			}
			f.setPermissions(copypermissions);
		}
		f.setPhase(phase);
		return f;
	}

}
