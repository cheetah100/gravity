/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_teams")
public class Team extends AbstractNamedModelClass implements SecurityPermissionEntity {
	
	private static final long serialVersionUID = 167163553039753200L;
	
	private String tenantId;

	private Map<String,String> owners;
	
	private Map<String,String> members;

	private Map<String,String> roles;
	
	private List<String> aliases;

	public void setOwners(Map<String,String> owners) {
		this.owners = owners;
	}

	public Map<String,String> getOwners() {
		return owners;
	}

	public void setMembers(Map<String,String> members) {
		this.members = members;
	}

	public Map<String,String> getMembers() {
		return members;
	}

	public Map<String,String> getRoles() {
		return roles;
	}

	public void setRoles(Map<String,String> roles) {
		this.roles = roles;
	}
	
	@Override
	public Map<String, String> getPermissions() {
		Map<String, String> returnMap = new HashMap<>();
		returnMap.putAll(this.owners);
		returnMap.putAll(this.members);
		return returnMap;
	}

	public List<String> getAliases() {
		return aliases;
	}

	public void setAliases(List<String> aliases) {
		this.aliases = aliases;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenant(String tenantId) {
		this.tenantId = tenantId;
	}
	

}
