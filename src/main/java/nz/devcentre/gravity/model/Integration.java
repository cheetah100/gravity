/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="gravity_integration")
public class Integration extends AbstractNamedModelClass implements SecurityPermissionEntity{

	private static final long serialVersionUID = -3806142471660997688L;
	
	private String tenantId;
	
	private String connector;
	
	private Map<String,Object> config;
	
	private Map<String,Action> actions;
	
	private Map<String,String> permissions = new HashMap<String,String>();
	
	public Map<String,Action> getActions() {
		return actions;
	}

	public void setActions(Map<String,Action> actions) {
		this.actions = actions;
	}

	public Map<String,Object> getConfig() {
		return config;
	}

	public void setConfig(Map<String,Object> config) {
		this.config = config;
	}
	
	@Override
	public String toString() {
		return "IntegrationMapping [name=" + this.getName() +  "]";
	}

	public String getConnector() {
		return connector;
	}

	public void setConnector(String connector) {
		this.connector = connector;
	}

	public void setPermissions( Map<String,String> permissions) {
		this.permissions = permissions;
	}
	
	@Override
	public Map<String, String> getPermissions() {
		return this.permissions;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
}
