/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class View extends AbstractNamedModelClass{
		
	private static final long serialVersionUID = -3663689217569406184L;
	
	private String type;
		
	private Map<String, ViewField> fields;
	
	private Map<String,String> permissions;
	
	private String boardId;
	
	private String orderField;
	
	public void setFields(Map<String, ViewField> fields) {
		this.fields = fields;
	}

	public Map<String,ViewField> getFields() {
		return this.fields;
	}
	
	@JsonIgnore
	public Map<String,ViewField> getChildFields() {
		Map<String,ViewField> result = new HashMap<String,ViewField>();
		for(Entry<String,ViewField> entry : this.fields.entrySet()){
			if( entry.getValue().getChildboard() != null){
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}
	
	public ViewField getField( String fieldName ){
		if(this.fields.containsKey(fieldName)){
			return this.fields.get(fieldName);
		}
		return null;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String,String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String,String> permissions) {
		this.permissions = permissions;
	}

	public String getBoardId() {
		return boardId;
	}

	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

}
