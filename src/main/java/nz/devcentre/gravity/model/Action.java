/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.util.HashMap;
//import java.util.List;
import java.util.Map;

public class Action extends AbstractNamedModelClass implements Orderable{
		
	private static final long serialVersionUID = -787744857412601170L;
	
	private int order;
	
	private String type;
	
	private Map<String,Object> config;

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public Map<String,Object> getConfig() {
		if( this.config==null) {
			this.config = new HashMap<String,Object>();
		}
		return config;
	}

	public void setConfig(Map<String,Object> config) {
		this.config = config;
	}
	
	public void setConfigField( String field, Object value) {
		Map<String, Object> config = this.getConfig();
		if(value!=null) {
			config.put(field, value);
		}
	}
	
	public Object getConfigField( String field ) {
		if(this.config != null) {
			Object value = this.config.get(field);
			if( value!=null) {
				return value;
			}
		}
		return null;
	}
}
