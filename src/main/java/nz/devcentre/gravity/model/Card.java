/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Card implements Serializable {
	
	private static final long serialVersionUID = 4442021428402240230L;
	
	private String id;
	
	private String creator;
	
	private Date created;
	
	private String modifiedby;
	
	private Date modified;
		
	private Map<String, Object> fields;
	
	private String board;
	
	private Boolean lock;
	
	private String phase;
	
	private boolean deleted = false;
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getPhase() {
		return phase;
	}
	
	public void setPhase(String phase) {
		this.phase = phase;
	}
	
	public String getBoard() {
		return this.board;
	}

	public void setBoard(String board) {
		this.board = board;
	}
	
	public String getCreator() {
		return creator;
	}
	
	public void setCreator(String creator) {
		this.creator = creator;
	}

	public void setFields(Map<String, Object> fields) {
		this.fields = fields;
	}
	
	@JsonSerialize
	//(using=MapJsonSerializer.class)
	public Map<String, Object> getFields() {
		return fields;
	}
	
	public void addField(String fieldName,  Object fieldValue) {
		if(this.fields == null){
			this.fields= new HashMap<String,Object>();
		}
		this.fields.put(fieldName, fieldValue);
	}
	
	public Object getField(String fieldName){
		return this.fields.get(fieldName);
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getCreated() {
		return created;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getModified() {
		return modified;
	}

	public void setLock(Boolean lock) {
		this.lock = lock;
	}

	public Boolean getLock() {
		return lock;
	}
	
	public String toString(){
		return this.getBoard()+"."+this.id;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
			
}
