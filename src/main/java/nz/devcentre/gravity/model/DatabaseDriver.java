/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.model;

public enum DatabaseDriver {
	
	MONGO( "mongo_storage", 
			"",
			"show tables;",
			"mongodb",
			"",
			"",
			"",
			"",			
			""),
	
	ORACLE( "oracle_storage", 
			"oracle.jdbc.driver.OracleDriver",
			"SELECT table_name as name FROM user_tables",
			"jdbc:oracle",
			"",
			"",
			"",
			"",			
			""),
	
	MSSQL( "mssql_storage", 
			"com.microsoft.sqlserver.jdbc.SQLServerDriver",
			"select * from sys.tables;",
			"jdbc:sqlserver",
			"select * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='%s';",
			"COLUMN_NAME",
			"DATA_TYPE",
			"IS_NULLABLE",			
			""),

	MYSQL( "mysql_storage", 
			"com.mysql.cj.jdbc.Driver",
			"SELECT concat(table_name,\"\") as name FROM information_schema.tables order by table_schema,table_name;",
			"jdbc:mysql",
			"describe %s",
			"Field",
			"Type",			
			"Null",			
			"Key"),
	
	POSTGRES( "postgresql_storage",
			"org.postgresql.Driver",
			"SELECT tablename as name FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';",
			"jdbc:postgresql",
			"",
			"",			
			"",			
			"",			
			""),
	
	SNOWFLAKE( "snowflake_storage",
			"com.snowflake.client.jdbc.SnowflakeDriver",
			"SHOW TABLES",
			"jdbc:snowflake",
			"",
			"",			
			"",			
			"",			
			""),
	
	TERADATA( "teradata_storage",
			"com.teradata.jdbc.TeraDriver", 
			"SELECT TOP 20 TableName as name FROM DBC.TablesV WHERE tablekind = 'T'",
			"jdbc:teradata",
			"",
			"",			
			"",			
			"",			
			"");
	
	private String storagePlugin;
	private String driver;
	private String schemaQuery;
	private String urlPrefix;
	private String tableQuery;
	private String fieldKey;
	private String typeKey;
	private String nullKey;
	private String keyKey;
	
	/**
	 * @param storagePlugin
	 * @param driver
	 * @param schemaQuery
	 */
	private DatabaseDriver(String storagePlugin, 
			String driver, 
			String schemaQuery,
			String urlPrefix, 
			String tableQuery,
			String fieldKey,
			String typeKey,
			String nullKey,
			String keyKey){
		
		this.storagePlugin = storagePlugin;
		this.driver = driver;
		this.schemaQuery = schemaQuery;
		this.urlPrefix = urlPrefix;
		this.tableQuery = tableQuery;
		this.fieldKey = fieldKey;
		this.typeKey = typeKey;
		this.nullKey = nullKey;
		this.keyKey = keyKey;
		try {
			if( driver!=null) {
				Class.forName(driver);
			}
		} catch (ClassNotFoundException e) {
			// Eat the exception
			// ClassNotFound means that the driver specified isn't found.
			// Can't throw exception or access logger from initializer
		}
	}

	public static DatabaseDriver fromUrl( String url) {
		for( DatabaseDriver driver : values()) {
			if( url.startsWith(driver.getUrlPrefix())){
				return driver;
			}
		}
		return null;
	}

	public String getStoragePlugin() {
		return storagePlugin;
	}

	public String getUrlPrefix() {
		return urlPrefix;
	}

	public String getDriver() {
		return driver;
	}

	public String getSchemaQuery() {
		return schemaQuery;
	}

	public String getTableQuery() {
		return tableQuery;
	}

	public String getFieldKey() {
		return fieldKey;
	}

	public String getTypeKey() {
		return typeKey;
	}

	public String getNullKey() {
		return nullKey;
	}

	public String getKeyKey() {
		return keyKey;
	}

}
