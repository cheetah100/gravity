package nz.devcentre.gravity.model;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class PaginatedCards {
	private Collection<Card> data;
	private List<Map<String,Object>> pageInfo;
	public Collection<Card> getData() {
		return data;
	}
	public void setData(Collection<Card> data) {
		this.data = data;
	}
	public List<Map<String, Object>> getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(List<Map<String, Object>> pageInfo) {
		this.pageInfo = pageInfo;
	}
	
	
}	
