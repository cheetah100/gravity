/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;

public class CardEventField implements Serializable{
		
	private static final long serialVersionUID = -6386696772467972848L;

	private String fieldName;
	
	private String label;
	
	private String before;
	
	private String after;
	
	private String reason;

	public CardEventField() {
	}
	
	public CardEventField( String field, Object currentValue, Object newValue) {
		super();
		this.fieldName = field;
		if(currentValue!=null){
			this.before = currentValue.toString();
		} else {
			this.before = "";
		}
		if(newValue!=null){
			this.after = newValue.toString();
		} else {
			this.after = "";
		}
	}
	
	public CardEventField( String field, String label, Object currentValue, Object newValue) {
		super();
		this.fieldName = field;
		this.label = label;
		if(currentValue!=null){
			this.before = currentValue.toString();
		} else {
			this.before = "";
		}
		if(newValue!=null){
			this.after = newValue.toString();
		} else {
			this.after = "";
		}
	}
	
	@Override
	public String toString() {
		return "CardEventField [fieldName=" + fieldName + ", before=" + before
				+ ", after=" + after + ", reason=" + reason +"]";
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getAfter() {
		return after;
	}

	public void setAfter(String after) {
		this.after = after;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
	

	
	
	