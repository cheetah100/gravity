/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemReport extends AbstractBaseModelClass implements Serializable{
		
	private static final long serialVersionUID = 125114950269044716L;
	
	
	private Map<String,Integer> alertSummary;
	
	private Map<String,List<CardAlert>> alertDetails;
	
	public Map<String, Integer> getAlertSummary() {
		return alertSummary;
	}

	public void setAlertSummary(Map<String, Integer> alertSummary) {
		this.alertSummary = alertSummary;
	}

	public Map<String, List<CardAlert>> getAlertDetails() {
		return alertDetails;
	}

	public void setAlertDetails(Map<String, List<CardAlert>> alertDetails) {
		this.alertDetails = alertDetails;
	}
	
	public void addAlertsPerBoard(String boardName, List<CardAlert> alerts) {
		if(this.alertDetails == null)
			this.alertDetails = new HashMap<String, List<CardAlert>>();
		
		this.alertDetails.put(boardName, alerts);
	}
	
	public void addAlertCountsPerBoard(String boardName, Integer count) {
		if(this.alertSummary == null)
			this.alertSummary = new HashMap<String, Integer>();
		
		this.alertSummary.put(boardName, count);
	}
	
	
}
