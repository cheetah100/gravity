/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.tools.CardTools;


//TODO Retire the old way of getting records. Use Cursors instead.

@Component("resource")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField( field = "resource", name = "Resource containing Mongo Query",type=ConfigurationFieldType.RESOURCE)
public class ResourceAggregator implements Transformer {
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceAggregator.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private ResourceController resourceController;
		
	@Autowired
	private BoardService boardTools;
	
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, 
			Map<String, Object> dynamicConfigs)
			throws Exception {
		
		String boardId = (String)transform.getConfiguration().get("board");

		//Validate Board Access if rollup is false
		if (!rollup && !this.boardTools.isUserAccess(boardId,null)) {
			throw new AccessDeniedException("Access Denied for Board:"+boardId);
		}
		
		String resourceId = (String)transform.getConfiguration().get("resource");
		String query = resourceController.getResource(boardId, resourceId);
		
		logger.info("Starting Resource Transform on " + boardId + ": " + query);
		
		String completedQuery = "{ \"list\" : " + query + "}";
		
		if(dynamicConfigs!=null) {
			for(Entry<String,Object> entry: dynamicConfigs.entrySet()){
				completedQuery = completedQuery.replaceAll("!!" + entry.getKey(), entry.getValue().toString());
				completedQuery = completedQuery.replaceAll("!#" + entry.getKey(), getParameter( entry.getKey(), entry.getValue().toString()));
			}
		}
				
		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);
		BasicDBObject mongoQuery = BasicDBObject.parse(completedQuery);
		BasicDBList list = (BasicDBList) mongoQuery.get("list");
		
		List<DBObject> mongoPipeline = new ArrayList<DBObject>();
		for( Object d : list){
			mongoPipeline.add((DBObject) d);
		}
		
		logger.info("Aggregation: " + mongoPipeline.getClass().getName() + mongoPipeline.toString());
		AggregationOutput aggregate = collection.aggregate(mongoPipeline);
		
		Map<String, Object> cards = new HashMap<String,Object>();
		for( DBObject result : aggregate.results() ){
			Card card = CardTools.mongoDBObjectToCard(result, boardId);
			cards.put(card.getId(), card);
		}
		return cards;
	}
	
	
	// "$or": [{"asg_id": "b0064d76dba3d3c477405868dc9619c9"},
	// {"portfolio_epic_status":"funded"}]
	String getParameter(String field, String value){
		
		StringBuilder sb = new StringBuilder();
		
		if(StringUtils.isEmpty(value)){
			return "{}";
		}
		
		if( value.contains("|")){
			
			String[] valuesArray = StringUtils.split(value,"|");
			List<String> values = Arrays.asList(valuesArray);
			
			sb.append("{\"\\$or\": [");
			for( String v : values ){
				sb.append("{\"");
				sb.append(field);
				sb.append("\":\"");
				sb.append(v);
				sb.append("\"},");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("]}");
			
		} else {
			sb.append("{\"");
			sb.append(field);
			sb.append("\":\"");
			sb.append(value);
			sb.append("\"}");
		}
		return sb.toString();
	}

}
