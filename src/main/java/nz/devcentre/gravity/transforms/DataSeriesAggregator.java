/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.model.transfer.AggregateData;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@Component("dataseries")
@ConfigurationDetail( name="Data Series Aggregator", description = "")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="lookup.field", name="lookup.field",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="dataseries.field", name="dataseries.field",type=ConfigurationFieldType.LIST,required=true)
@ConfigurationField(field="total", name="total",type=ConfigurationFieldType.STRING,required=true)
@ConfigurationField(field="xaxis", name="xaxis",type=ConfigurationFieldType.STRING,required=true)
@ConfigurationField(field="ylabel", name="ylabel",type=ConfigurationFieldType.STRING,required=true)
@ConfigurationField(field="sort", name="sort",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="aggregationType", name="aggregationType",type=ConfigurationFieldType.STRING,required=true)
@ConfigurationField(field="addxtotal", name="addxtotal",type=ConfigurationFieldType.BOOLEAN)
@ConfigurationField(field="addytotal", name="addytotal",type=ConfigurationFieldType.BOOLEAN)


public class DataSeriesAggregator extends PivotAggregator {

	private static final Logger logger = LoggerFactory.getLogger(PivotAggregator.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private BoardsCache boardsCache;

	@Autowired
	private QueryService queryService;

	@Autowired
	private BoardService boardTools;

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup,
			Map<String, Object> dynamicConfigs) throws Exception {

		String boardId = (String) transform.getConfiguration().get("board");
		String filterId = (String) transform.getConfiguration().get("filter");
		List<String> lookupList = TransformUtility.getValueList(transform, "lookup.field");
		List<String> groupByList = new ArrayList<String>(
				(List<String>) TransformUtility.getValue(transform, "dataseries.field"));
		String totalBy = (String) transform.getConfiguration().get("total");
		String xAxis = (String) transform.getConfiguration().get("xaxis"); // Is this required ??
		// String yAxis = (String)transform.getConfiguration().get("yaxis");
		// List<String> yAxisList= new ArrayList<String>((List<String>)
		// TransformUtility.getValue(transform, "dataseries.field"));

		String yLabel = (String) transform.getConfiguration().get("ylabel");
		List<Object> sortList = new ArrayList<>((List<Object>) TransformUtility.getValue(transform, "sort"));
		String aggregationType = (String) transform.getConfiguration().get("aggregationType");
		boolean addXTotal = Boolean.parseBoolean((String) transform.getConfiguration().get("addxtotal"));
		boolean addYTotal = Boolean.parseBoolean((String) transform.getConfiguration().get("addytotal"));

		Board board = boardsCache.getItem(boardId);
		//Template template = board.getTemplates().get(boardId);

		// Validate Board Access if rollup is false
		if (!rollup && !this.boardTools.isUserAccess(board.getId(), null)) {
			throw new AccessDeniedException("Access Denied for Board:" + board.getName());
		}

		Filter filter = null;
		Filter boardFilter = null;
		if (filterId != null && board.getFilters() != null) {
			boardFilter = board.getFilters().get(filterId);
			filter = (Filter) SerializationUtils.clone(boardFilter);
		}

		// Add the dynamic filter to our filtering clause
		Filter dynaFilter = null;
		if (dynamicConfigs != null && dynamicConfigs.containsKey("filter")) {
			dynaFilter = (Filter) dynamicConfigs.get("filter");
			queryService.resolveDynamicFilter(boardId, dynaFilter, filter);
		}

		if (filter != null && dynaFilter != null) {
			filter.getConditions().putAll(dynaFilter.getConditions());
		} else if (filter == null && dynaFilter != null) {
			filter = dynaFilter;
		}
		Condition phaseCondition = QueryService.buildPhaseCondition(board, null, false, filter);
		if (filter != null && phaseCondition != null) {
			filter.getConditions().put("phase", phaseCondition);
		} else if (phaseCondition != null) {
			filter = new Filter();
			Map<String, Condition> conditionsMap = new HashMap<>();
			conditionsMap.put("phase", phaseCondition);
			filter.setConditions(conditionsMap);
		}

		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);

		List<DBObject> pipeline = new ArrayList<DBObject>();

		// Add Match Object
		// The Match Object is the same as a Filter.
		// Match should be first part of the generated aggregation query
		if (filter != null) {
			BasicDBObject match = new BasicDBObject();
			String mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(filter.getConditions().values(), null);
			BasicDBObject q = BasicDBObject.parse(mongoQueryStr);
			match.append("$match", q);
			pipeline.add(match);
		}

		Map<String, String> replace = new HashMap<String, String>();

		if (xAxis.contains(".")) {
			lookupList.add(xAxis);
		}

		// if(StringUtils.isNotEmpty(yaxisList) && yAxis.contains(".")){
		for (String yAxis : groupByList) {
			
			lookupList.add(yAxis);
		}

		if (!lookupList.isEmpty()) {
			for (String each : lookupList) {
				String[] split = each.split("\\.");
				String localField = split[0];
				replace.put(localField, each);
				TemplateField tf = board.getField(localField);
				if (tf != null && tf.getOptionlist() != null) {
					StringBuilder sb = new StringBuilder();
					sb.append("{ $lookup: { from:\"");
					sb.append(tf.getOptionlist());
					sb.append("\", localField:\"");
					sb.append(localField);
					sb.append("\", foreignField:\"_id\", as:\"");
					sb.append(localField);
					sb.append("\"}}");

					BasicDBObject lookup = BasicDBObject.parse(sb.toString());
					pipeline.add(lookup);

					sb = new StringBuilder();
					sb.append("{ $unwind:\"$");
					sb.append(localField);
					sb.append("\" }");
					BasicDBObject unwind = BasicDBObject.parse(sb.toString());
					pipeline.add(unwind);
				}
			}
		}

		// Multiple values in Group By
		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();

		List<String> groups = new ArrayList<String>();
		groups.addAll(groupByList);
		groups.add(xAxis);

		if (!groups.isEmpty()) {

			
			sb.append("{ \"$group\": { \"_id\":{");

			for (String each : groups) {
				sb.append("\"");
				sb.append(each.replace(".", "_"));
				sb.append("\"");

				// Handling conversion of Ids to reference field names

				if ("phase".equals(each)) {
					sb.append(" : \"$metadata.phase\",");
				} else if (replace.containsKey(each)) {
					sb.append(" : \"$");
					sb.append(replace.get(each));
					if( lookupList.contains(each)) {
						sb.append("._id");
					}
					sb.append("\",");
				} else {
					sb.append(" : \"$");
					sb.append( each );
					if( lookupList.contains(each)) {
						sb.append("._id");
					}
					sb.append("\",");
				}
				
				sb2.append("\"");
				sb2.append(each.replace(".", "_"));
				sb2.append("\":\"$_id.");
				sb2.append(each.replace(".", "_"));
				sb2.append("\",");
			}
			// Remove last comma
			sb.deleteCharAt(sb.length() - 1);
			sb2.deleteCharAt(sb2.length() - 1);
			sb.append("}");
			
			System.out.println("sb: " + sb.toString());
			System.out.println("sb2: " + sb2.toString());

			if (StringUtils.isNotEmpty(totalBy)) {

				sb.append(TransformUtility.getAggregationString(aggregationType, totalBy));

			} else {
				sb.append(",total: {$sum:1}");
			}

			sb.append("}}");

			BasicDBObject group = BasicDBObject.parse(sb.toString());
			pipeline.add(group);

			sb = new StringBuilder();
			sb.append("{$addFields:{");
			sb.append(sb2.toString());
			sb.append("}}");

			BasicDBObject addFields = BasicDBObject.parse(sb.toString());
			pipeline.add(addFields);

		}

		// AddFields
		if (!sortList.isEmpty()) {
			sb = new StringBuilder();
			sb.append("{$sort:{");
			if (sortList.get(0) instanceof String) {
				for (Object each : sortList) {
					String[] sortDirectArray = StringUtils.split(each.toString(), ":");
					sb.append("\"");
					sb.append(sortDirectArray[0]);
					sb.append("\"");
					sb.append(": ");
					sb.append(sortDirectArray[1]);
					sb.append(",");
				}
			} else if (sortList.get(0) instanceof Map) {
				for (Object sort : sortList) {
					sb.append("\"");
					sb.append(TransformUtility.getMapValue(sort, "field"));
					sb.append("\"");
					sb.append(": ");
					sb.append(TransformUtility.getMapValue(sort, "order"));
					sb.append( ",");
				}
			}

			sb.deleteCharAt(sb.length() - 1);
			sb.append("}}");
			BasicDBObject addsort = BasicDBObject.parse(sb.toString());
			pipeline.add(addsort);
		} else {
			{
				sb = new StringBuilder();
				sb.append("{$sort:{");

				for (String group : groups) {
					sb.append("\"" + group + "\"");
					sb.append(": " + "1" + ",");
				}
				sb.deleteCharAt(sb.length() - 1);
				sb.append("}}");
				BasicDBObject addsort = BasicDBObject.parse(sb.toString());
				pipeline.add(addsort);
			}

		}
		logger.info("Transpose Aggregation Query: " + pipeline.toString());

		AggregationOptions options = AggregationOptions.builder().build();
		Cursor aggregate = collection.aggregate(pipeline, options);

		Map<String, Object> formatAggregationResult = null;
		formatAggregationResult = formatAggregationResult(aggregate, boardId, xAxis, groupByList, yLabel, addXTotal,
				addYTotal);

		aggregate.close();

		return formatAggregationResult;
	}

	protected String getValueFromResultSet(DBObject map, String field) {
		if (field == null) {
			return null;
		}
		String stripDot = field.replace(".", "_");
		Object obj = map.get(stripDot);
		if (obj != null) {
			if (obj instanceof Date) {
				return Long.toString(((Date) obj).getTime());
			}
			return obj.toString();
		}
		return null;
	}

	public Map<String, Object> formatAggregationResult(Cursor aggregate, String boardId, String xAxis,
			List<String> yAxisList, String yLabel, boolean addXTotal, boolean addYTotal) {

		Map<String, Object> results = new HashMap<String, Object>();

		AggregateData pivotResponse = new AggregateData();

		List<Object> recordList = null;

		// Map<String, Object> completeResult = new HashMap<String, Object>();

		String dataSeriesId = "";
		while (aggregate.hasNext()) {
			DBObject result = aggregate.next();
			String x = getValueFromResultSet(result, xAxis);
			// List<String> yList = new ArrayList<String>();

			// String y = "";
			String newDataSeriesId = "";
			if (yAxisList != null) {

				for (String yAxis : yAxisList) {
					String y = getValueFromResultSet(result, yAxis);
					newDataSeriesId = newDataSeriesId + y;


				}

			}
			if (!newDataSeriesId.equals(dataSeriesId)) {
				recordList = new ArrayList<Object>();
				Map<String, Object> mainObject = new HashMap<String, Object>();
				mainObject.put("id", newDataSeriesId);
				Map<String, Object> fields = new HashMap<String, Object>();
				for (String yAxis : yAxisList) {
					fields.put(yAxis, getValueFromResultSet(result, yAxis));
				}
				mainObject.put("fields", fields);
				mainObject.put("items", recordList);
				// results.put("main",mainObject);

				results.put(newDataSeriesId, mainObject);

				dataSeriesId = newDataSeriesId;
			}

			Object total = result.get("total");
			Number totalNumber = null;
			long totalValue = 0;

			if ((total instanceof Number) || xAxis == null || yAxisList == null) {
				totalNumber = (Number) total;
				totalValue = totalNumber.longValue();
			}

			Map<String, Object> dataObject = pivotResponse.addDataSeries("x", x, totalValue);
			recordList.add(dataObject);

		}

		/*
		 * try { String xDataSource = listTools.getDatasource(boardId, xAxis); if
		 * (xDataSource != null) { Map<String, Option> orderedXList =
		 * getList(xDataSource, xAxis); if (orderedXList != null) {
		 * pivotResponse.retitlexAxis(orderedXList);
		 * pivotResponse.addDatasource(xDataSource); } } else if (xAxis.equals("phase"))
		 * { Map<String, Option> orderedXList = getPhaseList(boardId);
		 * pivotResponse.reorderxAxis(orderedXList); } else {
		 * logger.warn("Datasource Not Found for xAxis : " + xAxis); }
		 * 
		 * } catch (Exception e) { logger.info("No sorting applied"); }
		 * 
		 * pivotResponse.addToMapForDataServices(results);
		 * //pivotResponse.addToMapForDataServices(completeResults);
		 */
		return results;

	}

}
