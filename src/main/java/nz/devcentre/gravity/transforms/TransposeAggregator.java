/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@SuppressWarnings("deprecation")
@Component("transpose")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="lookup.field", name="lookup.field",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="group.field", name="group.field",type=ConfigurationFieldType.LIST,required=true)
@ConfigurationField(field="transpose.field", name="transpose.field",type=ConfigurationFieldType.LIST,required=true)

@ConfigurationField(field="total", name="total",type=ConfigurationFieldType.STRING,required=true)
@ConfigurationField(field="sort", name="sort",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="pipeline", name="pipeline",type=ConfigurationFieldType.LIST,required=true)
@ConfigurationField(field="aggregationType", name="aggregationType",type=ConfigurationFieldType.STRING)

public class TransposeAggregator implements Transformer {
	
	private static final Logger logger = LoggerFactory.getLogger(TransposeAggregator.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired 
	private QueryService queryService;
	
	@Autowired
	private BoardService boardTools;

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, Map<String, Object> dynamicConfigs)
			throws Exception {

		String boardId = (String)transform.getConfiguration().get("board");
		String filterId = (String)transform.getConfiguration().get("filter");
		List<String> lookupList = TransformUtility.getValueList(transform, "lookup.field");
		List<String> groupByList =TransformUtility.getValueList(transform, "group.field");
		List<String> transposeList=TransformUtility.getValueList(transform, "transpose.field");
		String totalBy = (String)transform.getConfiguration().get("total");
		List<Object> sortList=new ArrayList<>((List<Object>) TransformUtility.getValue(transform, "sort"));
		List<String> pipelineStrList =new ArrayList<>(TransformUtility.getValueList(transform, "pipeline"));
		String aggregationType=(String)transform.getConfiguration().get("aggregationType");
		
		Board board = boardsCache.getItem(boardId);
		//Validate Board Access if rollup is false
		if (!rollup && !this.boardTools.isUserAccess(board.getId(),null)) {
			throw new AccessDeniedException("Access Denied for Board:"+board.getName());
		}
		logger.info("Run Transform on Board:-"+board.getId());
		Filter filter = null;
		Filter boardFilter = null;
		if( filterId!=null && board.getFilters()!=null){
			boardFilter = board.getFilters().get(filterId);
			filter = (Filter)SerializationUtils.clone(boardFilter);
		}
		//Add the dynamic filter to our filtering clause
		Filter dynaFilter = null;
		if(dynamicConfigs != null && dynamicConfigs.containsKey("filter")) {
			dynaFilter = (Filter)dynamicConfigs.get("filter");
			queryService.resolveDynamicFilter(boardId, dynaFilter, filter);
		}
		
		if(filter != null && dynaFilter != null) {
			filter.getConditions().putAll(dynaFilter.getConditions());
		}else if(filter == null && dynaFilter != null) {
			filter = dynaFilter;
		}
		Condition phaseCondition =  QueryService.buildPhaseCondition(board, null, false, filter);
		if(filter != null && phaseCondition != null) {
			filter.getConditions().put("phase",phaseCondition);
		}else if (phaseCondition != null) {
			filter = new Filter();
			Map<String, Condition> conditionsMap = new HashMap<>();
			conditionsMap.put("phase",phaseCondition);
			filter.setConditions(conditionsMap);
		}
		
		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);
		
		List<DBObject> pipeline = new ArrayList<DBObject>();
		
		// Add Match Object
		// The Match Object is the same as a Filter.
		if(filter != null) {
			BasicDBObject match = new BasicDBObject();
			String mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(filter.getConditions().values(),null);
			BasicDBObject q = BasicDBObject.parse(mongoQueryStr);
			match.append("$match", q);
			pipeline.add(match);
		}
		
		if(pipelineStrList != null && !pipelineStrList.isEmpty()) {
			for(String dbString: pipelineStrList) {
				BasicDBObject q = BasicDBObject.parse(dbString);
				pipeline.add(q);
			}
			AggregationOutput aggregate = collection.aggregate(pipeline);
			return formatAggregationResult(aggregate);
		}
		
		// Add Group Object
		
		/*
		 * {$group:
     			{_id:{sub_function:"$sub_function", function:"$function", project_id:"$project_id"}, 
	  	        items:{$addToSet:{quarter:"$quarter",investment_amount:"$investment_amount"}}
      			}
  			},
  			{$project:
			     {tmp:
					{$arrayToObject: 
						{$zip:{inputs:["$items.quarter", "$items.investment_amount"]}}
					}
				   ,total:{
					$sum:"$items.investment_amount"
					}
				}
			  }, 
			  {$addFields:
			     {"tmp.sub_function":"$_id.sub_function", 
			      "tmp.function":"$_id.function",
			      "tmp.project_id": "$_id.project_id",
			      "tmp.total":"$total"}
			  }, 
			  {$replaceRoot:{newRoot:"$tmp"}
			  }
  			
		*/
		Map<String, String> replace = new HashMap<String, String>();
		if(!lookupList.isEmpty()) {
			
			for(String each: lookupList) {
				String[] split = each.split("\\.");
				// $lookup: { from:"capabilities", localField:"capability", foreignField:"_id", as:"capability" } }
				
				String localField = split[0];
				String subField = split[1];
				replace.put(localField, each);
				TemplateField tf = board.getField(localField);
				if(tf!=null && tf.getOptionlist()!=null){		
					StringBuilder sb = new StringBuilder();
					sb.append("{ $lookup: { from:\"");
					sb.append( tf.getOptionlist() );
					sb.append("\", localField:\"");
					sb.append(localField);
					sb.append("\", foreignField:\"_id\", as:\"");
					sb.append(localField);
					sb.append("\"}}");
					
					BasicDBObject lookup = BasicDBObject.parse(sb.toString());
					pipeline.add(lookup);
					
					sb = new StringBuilder();
					sb.append("{ $unwind:\"$");
					sb.append( localField ); 
					sb.append("\" }");
					BasicDBObject unwind = BasicDBObject.parse(sb.toString());
					pipeline.add(unwind);
				}
			}
		}
		
		//Multiple values in Group By
		
		if(!groupByList.isEmpty()) {

			
			StringBuilder sb = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();
			sb.append("{ \"$group\": { \"_id\":{");
			
			for(String each : groupByList) {
				sb.append("\"" + each + "\"");
				
				//Handling conversion of Ids to reference field names
				if(replace.containsKey(each)) {
					sb.append(" : \"$" + replace.get(each) + "\",");
				}else if("phase".equals(each)){
					sb.append(" : \"$metadata.phase\",");
				}else {
					sb.append(" : \"$" + each + "\",");
				}
				//"tmp.sub_function":"$_id.sub_function",
				sb2.append("\"tmp."+each+"\":\"$_id." + each +"\",");
			}
			//Remove last comma
			sb.deleteCharAt(sb.length()-1);
			sb2.deleteCharAt(sb2.length()-1);
			sb.append("}");
			
			StringBuilder sb1 = new StringBuilder();
			if(!transposeList.isEmpty()) {
				sb.append(",\"items\":{\"$addToSet\":{");
				for(String each : transposeList) {
					sb.append("\"" + each + "\"");
					if( "phase".equals(each)){
						sb.append(": \"$metadata.phase\",");
					} else { 
						sb.append(": \"$"+ each + "\",");
					}
					sb1.append("\"$items." + each + "\",");
				} 
				//Remove last comma
				sb.deleteCharAt(sb.length()-1);
				sb1.deleteCharAt(sb1.length()-1);
				sb.append("}}");
			}
			
			sb.append("}}");
			
			BasicDBObject group = BasicDBObject.parse(sb.toString());
			pipeline.add(group);
			
			sb = new StringBuilder();
			sb.append("{$project: {tmp: {$arrayToObject: {$zip:{inputs:[");
			sb.append(sb1.toString());
			sb.append("]}}}");
			
			if( totalBy!=null){
				sb.append(TransformUtility.getAggregationString(aggregationType, "items."+totalBy));
			} else {
				sb.append(",total: {$sum:1}");
			}
			sb.append("}}");
			
			BasicDBObject project = BasicDBObject.parse(sb.toString());
			pipeline.add(project);
			
			sb = new StringBuilder();
			sb.append("{$addFields:{");
			sb.append(sb2.toString());
			if(totalBy != null) {
				sb.append(",\"tmp.total\":\"$total\"");
			}
			sb.append("}}");
			
			BasicDBObject addfields = BasicDBObject.parse(sb.toString());
			pipeline.add(addfields);
			
			if(!sortList.isEmpty()) {
				sb = new StringBuilder();
				sb.append("{$sort:{");
				if (sortList.get(0) instanceof String) {
					for(Object each : sortList) {
						String[] sortDirectArray = StringUtils.split(each.toString(), ":");
						sb.append("\"tmp." + sortDirectArray[0] + "\"");
						sb.append(": " + sortDirectArray[1] +",");
					}
				}else if(sortList.get(0) instanceof Map) {
					for(Object sort:sortList) {
						sb.append("\"" + TransformUtility.getMapValue(sort, "field")+ "\"");
						sb.append(": " + TransformUtility.getMapValue(sort, "order") +",");
					}
				}	
					sb.deleteCharAt(sb.length()-1);
	
				sb.append("}}");
				BasicDBObject addsort = BasicDBObject.parse(sb.toString());
				pipeline.add(addsort);
			}
			
			BasicDBObject replaceRoot = BasicDBObject.parse("{$replaceRoot:{newRoot:\"$tmp\"}}");
			pipeline.add(replaceRoot);
			
		}
		logger.info("Transpose Aggregation Query: " + pipeline.toString());
		
		AggregationOutput aggregate = collection.aggregate(pipeline);
		return formatAggregationResult(aggregate);
	}
	
	private Map<String, Object> formatAggregationResult(AggregationOutput aggregate){
		Map<String, Object> results = new HashMap<String,Object>();
		Set<String> columns = new HashSet<String>();
		List<DBObject> data = new ArrayList<DBObject>();
		
		for( DBObject result : aggregate.results() ){
			data.add(result);
			columns.addAll(result.keySet());
		}
		results.put("data", data);
		results.put("columns", columns);
		return results;
	}

}
