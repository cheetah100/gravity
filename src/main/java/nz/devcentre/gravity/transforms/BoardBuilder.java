/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Transform;

@Component("builder")
@ConfigurationDetail( name="Builder Aggregation", 
	description = "Gets raw data from a specific board.")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="phase", name="phase",type=ConfigurationFieldType.PHASE)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="view", name="view",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="allcards", name="allCards",type=ConfigurationFieldType.BOOLEAN)
@ConfigurationField(field="descending", name="descending",type=ConfigurationFieldType.BOOLEAN)

public class BoardBuilder implements Transformer {
	
	@Autowired
	CardController cardController;

	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, Map<String, Object> dynamicConfigs) throws Exception{
		
		String board = (String)transform.getConfiguration().get("board");
		String phase = (String)transform.getConfiguration().get("phase");
		String filter = (String)transform.getConfiguration().get("filter");
		String view = (String)transform.getConfiguration().get("view");
		boolean allcards = "true".equals(transform.getConfiguration().get("allcards"));
		boolean descending = "true".equals(transform.getConfiguration().get("descending"));
		
		Collection<Card> cardsDetail = 
				this.cardController.getCards(board, phase, filter, view, null, false, allcards, descending);
		
		input.put(board, cardsDetail);
		return input;
	}

}
