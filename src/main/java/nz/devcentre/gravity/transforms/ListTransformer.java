/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@Component("list")
@ConfigurationDetail( name="List Transformer", description = "")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="lookup", name="lookup",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="sort", name="sort",type=ConfigurationFieldType.STRING)

public class ListTransformer implements Transformer {

	private static final Logger logger = LoggerFactory.getLogger(ListTransformer.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired 
	private QueryService queryService;
	
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, Map<String, Object> dynamicConfigs)
			throws Exception {
		
		String boardId = (String)transform.getConfiguration().get("board");
		String filterId = (String)transform.getConfiguration().get("filter");
		String lookupStr = (String)transform.getConfiguration().get("lookup");
		String sort = (String)transform.getConfiguration().get("sort");
		
		Board board = boardsCache.getItem(boardId);
		
		Filter filter = null;
		Filter boardFilter = null;
		if( filterId!=null && board.getFilters()!=null){
			boardFilter = board.getFilters().get(filterId);
			filter = (Filter)SerializationUtils.clone(boardFilter);
		}
		
		//Add the dynamic filter to our filtering clause
		Filter dynaFilter = null;
		if(dynamicConfigs != null && dynamicConfigs.containsKey("filter")) {
			dynaFilter = (Filter)dynamicConfigs.get("filter");
			queryService.resolveDynamicFilter(boardId, dynaFilter, filter);
		}
		
		if(filter != null && dynaFilter != null) {
			filter.getConditions().putAll(dynaFilter.getConditions());
		}else if(filter == null && dynaFilter != null) {
			filter = dynaFilter;
		}
		Condition phaseCondition =  QueryService.buildPhaseCondition(board, null, false, filter);
		if(filter != null && phaseCondition != null) {
			filter.getConditions().put("phase",phaseCondition);
		}else if (phaseCondition != null) {
			filter = new Filter();
			Map<String, Condition> conditionsMap = new HashMap<>();
			conditionsMap.put("phase",phaseCondition);
			filter.setConditions(conditionsMap);
		}
		
		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);
		
		List<DBObject> pipeline = new ArrayList<DBObject>();
		
		// Add Match Object
		// The Match Object is the same as a Filter.
		if(filter != null) {
			BasicDBObject match = new BasicDBObject();
			String mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(filter.getConditions().values(),null);
			BasicDBObject q = BasicDBObject.parse(mongoQueryStr);
			match.append("$match", q);
			pipeline.add(match);
		}
		
		Map<String, String> replace = new HashMap<String, String>();
		if(lookupStr!= null) {
			String[] lookupArray = StringUtils.split(lookupStr, "|");
			List<String> lookupList = Arrays.asList(lookupArray);
			
			for(String each: lookupList) {
				String[] split = each.split("\\.");
				// $lookup: { from:"capabilities", localField:"capability", foreignField:"_id", as:"capability" } }
				
				String localField = split[0];
				replace.put(localField, each);
				TemplateField tf = board.getField(localField);
				if(tf!=null && tf.getOptionlist()!=null){		
					StringBuilder sb = new StringBuilder();
					sb.append("{ $lookup: { from:\"");
					sb.append( tf.getOptionlist() );
					sb.append("\", localField:\"");
					sb.append(localField);
					sb.append("\", foreignField:\"_id\", as:\"");
					sb.append(localField);
					sb.append("\"}}");
					
					BasicDBObject lookup = BasicDBObject.parse(sb.toString());
					pipeline.add(lookup);
					
					sb = new StringBuilder();
					sb.append("{ $unwind:\"$");
					sb.append( localField ); 
					sb.append("\" }");
					BasicDBObject unwind = BasicDBObject.parse(sb.toString());
					pipeline.add(unwind);
				}
			}
		}
		
		
		//Multiple values in Group By
		StringBuilder sb = new StringBuilder();
				
		//AddFields
		if(sort != null) {
			sb = new StringBuilder();
			sb.append("{$sort:{");
			if(sort.contains("|")) {
				String[] sortArray = StringUtils.split(sort, "|");
				List<String> sortList = Arrays.asList(sortArray);
				for(String each : sortList) {
					String[] sortDirectArray = StringUtils.split(each, ":");
					sb.append("\"" + sortDirectArray[0] + "\"");
					sb.append(": " + sortDirectArray[1] +",");
				}
				sb.deleteCharAt(sb.length()-1);
			}else {
				String[] sortDirectArray = StringUtils.split(sort, ":");
				sb.append("\"" + sortDirectArray[0] + "\"");
				sb.append(": " + sortDirectArray[1] );
			}
			sb.append("}}");
			BasicDBObject addsort = BasicDBObject.parse(sb.toString());
			pipeline.add(addsort);
		}
		logger.info(" Aggregation Query: " + pipeline.toString());
		
		AggregationOptions options = AggregationOptions.builder().build();
		Cursor aggregate = collection.aggregate(pipeline,options);
		List<DBObject> recordList = new ArrayList<DBObject>();
		while( aggregate.hasNext()) {
			recordList.add(aggregate.next());
		}
		aggregate.close();
		
		Map<String, Object> results = new HashMap<String,Object>();
		results.put("list", recordList);
		return results;
	}
		
}
