/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@Component("aggregate")
@ConfigurationDetail( name="Aggregation Transformer", 
	description = "")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="group", name="Group by Field",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="total", name="Total Field",type=ConfigurationFieldType.STRING)
public class Aggregator implements Transformer {
	
	private static final Logger logger = LoggerFactory.getLogger(Aggregator.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private BoardsCache boardsCache;

	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, 
			Map<String, Object> dynamicConfigs)
			throws Exception {
		
		
		String boardId = (String)transform.getConfiguration().get("board");
		String filterId = (String)transform.getConfiguration().get("filter");
		String groupBy = (String)transform.getConfiguration().get("group");
		String totalBy = (String)transform.getConfiguration().get("total");
		
		Board board = boardsCache.getItem(boardId);
		Filter filter = null;
		if( filterId!=null && board.getFilters()!=null){
			filter = board.getFilters().get(filterId);
		}
		
		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);
		
		List<DBObject> pipeline = new ArrayList<DBObject>();
		
		// Add Match Object
		// The Match Object is the same as a Filter.
		BasicDBObject match = new BasicDBObject();
		Map<String, Condition> conditions = new HashMap<>();
		if (filter != null)
			conditions = filter.getConditions();
		String mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions.values(),null);
		BasicDBObject q = BasicDBObject.parse(mongoQueryStr);
		match.append("$match", q);
		pipeline.add(match);
		
		// Add Group Object
		// group = the field name to group by
		// total = the field name to total up
		
		BasicDBObject group = new BasicDBObject();
		
		BasicDBObject g1 = new BasicDBObject();
		g1.append("_id", "$" + groupBy);
		
		BasicDBObject total = new BasicDBObject();
		if( totalBy!=null){
			total.append("$sum", "$"+ totalBy);
		} else {
			total.append("$sum", 1);
		}
		g1.append("total", total);
		group.append("$group", g1);
		pipeline.add(group);
		
		logger.info("Aggregation: " + pipeline.toString());
		
		AggregationOptions options = AggregationOptions.builder().build();
		Cursor aggregate = collection.aggregate(pipeline,options);
		
		Map<String, Object> cards = new HashMap<String,Object>();
		while( aggregate.hasNext()) {
			DBObject result = aggregate.next();
			Card card = CardTools.mongoDBObjectToCard(result, boardId);
			cards.put(card.getId(), card);
		}
		aggregate.close();
		return cards;
	}

}
