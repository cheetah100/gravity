/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.QueryService;

@Component("concat")
@ConfigurationDetail( name="Concatinate Data Together", 
	description = "")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="view", name="view",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="allcards", name="allCards",type=ConfigurationFieldType.BOOLEAN)
public class Concatenator implements Transformer {
	
	private static final Logger logger = LoggerFactory.getLogger(Concatenator.class);
		
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired 
	private QueryService queryService;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, Map<String, Object> dynamicConfigs) throws Exception{
		String boardId = (String)transform.getConfiguration().get("board");
		String filterId = (String)transform.getConfiguration().get("filter");
		String viewId = (String)transform.getConfiguration().get("view");
		boolean allCards = "true".equals(transform.getConfiguration().get("allcards"));
		
		String securityFilter = "READ,WRITE,ADMIN";
		if(rollup){
			securityFilter = "READ,WRITE,ADMIN,ROLLUP";
		}
		
		Board board = this.boardsCache.getItem(boardId);
		
		boolean authorised = this.securityTool.isAuthorised(board.getPermissions(), securityFilter);
		if(!authorised){
			logger.info("Skipping Board in Transform: " + boardId);
			return input;
		}

		Collection<Card> cardsDetail = queryService.dynamicQuery(boardId, null, board.getFilter(filterId), board.getView(viewId), allCards);
		for( Card card : cardsDetail){
			input.put(board + card.getId(), card);
		}
		
		return input;
	}
}
