package nz.devcentre.gravity.transforms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import nz.devcentre.gravity.model.PivotType;
import nz.devcentre.gravity.model.Transform;

public class TransformUtility {
	
	//Utility Method to build the Dynamic Aggregation Text
	public static String getAggregationString(String aggregationType,String totalBy) {
		try {
			PivotType pivot=PivotType.valueOf(PivotType.class,aggregationType.toUpperCase());
			return pivot.getAggregationQuery(totalBy);
		}catch (Exception e) {
			PivotType pivot=PivotType.SUM;
			return pivot.getAggregationQuery(totalBy);
		}
	}
	@SuppressWarnings("unchecked")
	public static List<String> objectToList(Object obj) {
		if(obj==null) {
			return new ArrayList<String>();
		}
		if (obj instanceof String) {
			String str = (String) obj;
			String[] lookupArray = StringUtils.split(str, "|");
			return new ArrayList<String>(Arrays.asList(lookupArray));
		}else if(obj instanceof List){
			return (List<String>) (obj);
		}
		return new ArrayList<String>();
	}
	
	//Utility Method to Fetch Value
	@SuppressWarnings("unchecked")
	public static Object getValue(Transform transForm, String path) {
		String[] paths = path.split("\\.");
		Object obj = transForm.getConfiguration().get(paths[0]);
		//If path not present in Transform
		if(obj==null) {
			return new ArrayList<String>();
		}
		//If path is LIST and empty
		if(obj instanceof List && ((List) obj).isEmpty()) { //sort:[]
			return obj;
		}
		//lookup.field - paths size=2
		if (obj instanceof List && ((List<Object>) obj).get(0) instanceof Map && paths.length>1) {
			List<Map<String, Object>> objects = (List<Map<String, Object>>) obj;
			List<String> values = objects.stream().map(m -> m.get(paths[1]).toString()).collect(Collectors.toList());
			return values;
		} else if (obj instanceof List && ((List) obj).get(0) instanceof String) {//Old Configurations group:[phase,funtion]
			return (List<String>) (obj);
		} else if (obj instanceof String) {//Old Configuration with |
			String str = (String) obj;
			String[] lookupArray = StringUtils.split(str, "|");
			return new ArrayList<String>(Arrays.asList(lookupArray));
		} else if(obj instanceof List && ((List) obj).get(0) instanceof Map) {
			return (List<Map<String,Object>>)obj;
		} else if(obj instanceof List) {
			return (List)obj;
		} else {
			return new Object();
		}
	}
	public static List<String> getValueList(Transform transForm, String path) {
		String[] paths = path.split("\\.");
		Object obj = transForm.getConfiguration().get(paths[0]);
		//If path not present in Transform
		if(obj==null) {
			return new ArrayList<String>();
		}
		//If path is LIST and empty
		if(obj instanceof List && ((List) obj).isEmpty()) { //sort:[]
			return new ArrayList<String>();
		}
		//lookup.field - paths size=2
		if (obj instanceof List && ((List<Object>) obj).get(0) instanceof Map && paths.length>1) {
			List<Map<String, Object>> objects = (List<Map<String, Object>>) obj;
			List<String> values = objects.stream().map(m -> m.get(paths[1]).toString()).collect(Collectors.toList());
			return values;
		} else if (obj instanceof List && ((List) obj).get(0) instanceof String) {//Old Configurations group:[phase,funtion]
			return new ArrayList<>((List<String>) (obj));
		} else if (obj instanceof String) {//Old Configuration with |
			String str = (String) obj;
			String[] lookupArray = StringUtils.split(str, "|");
			return new ArrayList<String>(Arrays.asList(lookupArray));
		} else if(obj instanceof List) {
			return new ArrayList<String>((List)obj);
		} else {
			return new ArrayList<String>();
		}
	}

	public static Object getMapValue(Object obj, String key) {
		if(obj instanceof Map) {
			return ((Map) obj).get(key);
		}else {
			return null;
		}
	}

}

