/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.transforms;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.DataTracker;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.FilterTools;

//extend the write method 
@Component("dataanalytics")
@ConfigurationDetail( name="Data Analytics Transformer", 
	description = "")
@ConfigurationField(field="board", name="Board",type=ConfigurationFieldType.BOARD, required=true)
@ConfigurationField(field="filter", name="Filter",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="group", name="Group by Field",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="total", name="Total Field",type=ConfigurationFieldType.STRING)
public class DataAnalytics implements Transformer {

	private static final Logger logger = LoggerFactory.getLogger(DataAnalytics.class);

	//using transformer for pivot capabilities. 

	@Autowired
	private BoardsCache boardsCache; //easy way of retrieving board.

	@Autowired
	private SecurityTool securityTool;

	@Autowired 
	private QueryService queryService;

	//need to add transform override method
	@Override
	public Map<String, Object> transform(Map<String, Object> input, Transform transform, boolean rollup, Map<String, Object> dynamicConfigs) throws Exception{

		String boardId = (String)transform.getConfiguration().get("board");
		String filterId = (String)transform.getConfiguration().get("filter");
		String viewId = (String)transform.getConfiguration().get("view");
		boolean allCards = "true".equals(transform.getConfiguration().get("allcards"));

		//String batchId = (String)transform.getConfiguration().get("batchId");
		String batchId = null;
		if (dynamicConfigs != null) {
			batchId = (String)dynamicConfigs.get("batchId"); 
		}
		
		logger.info("Started Data Analytics Transform");

		String securityFilter = "READ,WRITE,ADMIN";

		//helper to abstract the fields (keys that we want to count)
		Map <String,TemplateField> fieldsToTrack = new HashMap <String,TemplateField>(); 

		//my custom Map Tracker
		Map <String, Object> dynamicDataTracker = new HashMap <String, Object>(); 

		if(rollup){
			securityFilter = "READ,WRITE,ADMIN,ROLLUP";
		}

		//get board
		Board board = this.boardsCache.getItem(boardId);
		boolean authorised = this.securityTool.isAuthorised(board.getPermissions(), securityFilter);
		if(!authorised){
			logger.info("Skipping Board in Transform: " + boardId);
			return input; //need some form of return to prevent from going further.
		}

		//get cards. **NEED TO CONFIRM THIS IS RETRIEVING THE CARDS.
		Collection<Card> cardsDetail;

		if(batchId == null){
			cardsDetail = queryService.dynamicQuery(boardId, null, board.getFilter(filterId), board.getView(viewId), allCards);
		} else {
			Filter filter = board.getFilter(filterId);
			if(filter == null){

				filter = FilterTools.createSimpleFilter("batch_id", Operation.EQUALTO, batchId);

			} else {
				//filter exists
				Map <String, Condition> conditionMap = filter.getConditions();
				Condition condition = new Condition( "batch_id",  Operation.EQUALTO, batchId);
				conditionMap.put("batchCondition", condition);
				filter.setConditions(conditionMap);
			}
			cardsDetail = queryService.dynamicQuery(boardId, null, filter, board.getView(viewId), allCards);	
		}


		Integer cardSize = cardsDetail.size();

		//everything ABOVE is boilerplate. 
		//Map<String,Template> templates = board.getTemplates();

		//need to iterate through the keys of templates & assign to fieldsToTrack **NEED SECOND LOOK @ THIS LOGIC.
		//templates.forEach((k, v) -> {
		//	Map <String,TemplateField> v1 = v.getFields();
		//	v1.forEach((k2,v2) -> fieldsToTrack.put(k2, v2));
		//});

		//modify said Map < key , DataTracker > 
		fieldsToTrack.forEach((key, value) -> dynamicDataTracker.put(key, new DataTracker(key, value.isRequired(), value.getType(), value.getValidation(), cardSize)));

		//iterate through cards & do count. 
		cardsDetail.forEach(card -> {
			logger.info(card.getClass().getName());
			Map<String, Object> fields = card.getFields();
			fields.forEach((key, value) -> {
				if(value != null && !value.equals("")){
					DataTracker fieldExistCounter = (DataTracker)dynamicDataTracker.get(key);
					if(fieldExistCounter != null && fieldExistCounter.toString() != null){
						fieldExistCounter.incrementCounter();
						dynamicDataTracker.put(key, fieldExistCounter);
					}
					
				}
			});
		});

		return dynamicDataTracker;
	}

}


