/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import java.util.Map;
import java.util.Map.Entry;

public class BufferElement {
	
	private String id;
	private String boardId;
	private Map<String, String> fields;
	private Map<String, Number> values;
	private String axisId;
	
	public BufferElement(String boardId, Map<String, String> fields, Map<String, Number> values) {
		this.boardId = boardId;
		this.fields = fields;
		this.values = values;
		StringBuffer sb = new StringBuffer();
		for( String value : fields.values()) {
			sb.append(value);
			sb.append("_");
		}
		this.id = boardId + "_" + sb.toString();
		this.axisId = sb.toString();
	}
	
	public BufferElement(String boardId, String axis, Map<String, Number> values) {
		this.boardId = boardId;
		this.values = values;
		this.id = boardId + "_" + axis;
		this.axisId = axis;
	}
	
	public void add(BufferElement be) {
		for( Entry<String,Number> entry : be.values.entrySet()) {
			Number existingValue = this.values.get(entry.getKey());
			Number newValue = existingValue.doubleValue() + entry.getValue().doubleValue();
			this.values.put(entry.getKey(), newValue);
		}
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getAxisId() {
		return this.axisId;
	}

	
	public Map<String, String> getFields() {
		return this.fields;
	}
		
	public Map<String, Number> getValues() {
		return this.values;
	}

	public String getBoardId() {
		return boardId;
	}
}
