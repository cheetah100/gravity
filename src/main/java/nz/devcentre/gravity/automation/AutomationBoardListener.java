/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.AlertService;
import nz.devcentre.gravity.services.CardService;

@Component
public class AutomationBoardListener implements BoardListener{
	
	private static final Logger logger = LoggerFactory.getLogger(AutomationBoardListener.class);
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	SecurityTool securityTool;
	
	@Autowired
	BoardsCache boardCache;
	
	@Autowired
	AutomationEngine automationEngine;
	
	@Autowired
	AlertService alertService;

	@Override
	public void onCardHolderEvent(CardHolder cardHolder) throws Exception {
		logger.info("OnCardHolderEvent: " + cardHolder.toString());
		this.securityTool.iAmSystem();

		Card card = null;
		try {
			card = cardService.getCard(cardHolder.getBoardId(), cardHolder.getCardId());
		} catch (ResourceNotFoundException e) {
			logger.info("Resource no longer exists: " + cardHolder.toString());
			return;
		}

		Board board = boardCache.getItem(card.getBoard());
		Phase phase = board.getPhases().get(card.getPhase());

		if (phase.isInvisible()) {
			logger.info("Card Phase is Invisible: Dont run rules " + cardHolder.toString());
			return;
		}

		Map<String, Rule> rules = this.automationEngine.getRulesFromBoard(cardHolder.getBoardId());

		logger.info("Automation Examining :" + card.getId());

		this.automationEngine.checkTaskRuleAssignments(rules, card);
		this.automationEngine.evaluateTaskRules(rules, card);

		if (this.alertService.isAlerted(card) || card.isDeleted()) {
			logger.warn("Alert/Deleted Blocking Rule Execution: " + card.toString());
			return;
		}

		this.automationEngine.executeCompulsoryRules(rules, card);
		this.automationEngine.executeTaskRules(rules, card);
		logger.info("Rule Execution Complete: " + card.toString());
	}
}
