/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2016 Cisco 
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class VariableInterpreter {
	
	private static final Logger logger = LoggerFactory.getLogger(VariableInterpreter.class);
	
	private static final String VARIABLE_TEMPLATE_TOKEN = "#";
	private static final String REGEX_DELIMITER="->";
	
	public boolean isVariableExpression(String variableExpression){
		return StringUtils.startsWith(variableExpression, VARIABLE_TEMPLATE_TOKEN);
	}
	
	public Object resolve(Map<String,Object> context, String variableExpression){
		
		if(StringUtils.isBlank(variableExpression)) {
			return variableExpression;
		}
		
		if ( variableExpression.startsWith("#now")) {
			if (variableExpression.length() > 4) {
				int modifier = Integer.parseInt(variableExpression.substring(5)) * 86400000;
				return new Date(System.currentTimeMillis() + modifier);
			} else {
				return new Date(System.currentTimeMillis());
			}
		}
		/**
		 * Check if regex expression and resolve
		 */
		if (StringUtils.isNotBlank(variableExpression)
				&& StringUtils.startsWith(variableExpression, VARIABLE_TEMPLATE_TOKEN)
				&& StringUtils.contains(variableExpression, REGEX_DELIMITER)) {
			return this.resolveRegEx(context, variableExpression);
		}
		
		if (StringUtils.startsWith(variableExpression, VARIABLE_TEMPLATE_TOKEN)) {
			return context.get(variableExpression.substring(1));
		}
		return variableExpression;
	}
	/**
	 * Used to resolve regular expression, replaceAll based on the pattern
	 * @param context
	 * @param variableExpression
	 * @return
	 */
	private Object resolveRegEx(Map<String, Object> context, String variableExpression) {
		// Split the string into 2 parts based on the first occurrence of the delimiter(->)
		final String[] tokens = variableExpression.split(REGEX_DELIMITER, 2);
		// Source field on which regex will be applied
		String sourceField = tokens[0].substring(1);
		String fieldValue = context.containsKey(sourceField) ? context.get(sourceField).toString() : null;
		if (StringUtils.isBlank(fieldValue)) {
			return fieldValue;
		}
		try {
			String pattern = tokens[1];
			return fieldValue.replaceAll(pattern, "");
		} catch (PatternSyntaxException e) {
			logger.warn("Regular Expression Errror", e);
			return null;
		}
	}
	
	public List<Object> resolveValues(Map<String,Object> context, String variableExpression){
		List<Object> returnValues = new ArrayList<Object>();		
		if (StringUtils.isNotBlank(variableExpression)){
			String[] split = variableExpression.split("\\|");
			List<String> values = Arrays.asList(split);			
			for( String value : values){
				if(value.startsWith(VARIABLE_TEMPLATE_TOKEN)){
					returnValues.add(context.get(value.substring(1)));	
				} else {
					returnValues.add(value);
				}
			}
		}
		return returnValues;
	}
	
	
	/**
	 * This purpose of this method is to take a configuration and context and fill in the values of the
	 * configuration from the context where there are references to the context, aka a '#'
	 * @param config
	 * @param context
	 * @return
	 */
	public Map<String,Object> completeConfig( Map<String,Object> config, Map<String,Object> context) {
		HashMap<String,Object> returnMap = new HashMap<String, Object>( config );
		for( Entry<String,Object> entry : returnMap.entrySet() ) {
			if( entry.getValue() instanceof String) {
				String value = (String) entry.getValue();
				if( value.startsWith(VARIABLE_TEMPLATE_TOKEN)) {
					String ref = value.substring(1);
					if(context.get(ref)!=null) {
						entry.setValue(context.get(ref));
					}
				}
			}
		}	
		return returnMap;
	}
}
