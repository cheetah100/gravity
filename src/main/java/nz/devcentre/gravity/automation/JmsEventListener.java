/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.BoardRule;
import nz.devcentre.gravity.model.CardHolder;

@Service
public class JmsEventListener implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(JmsEventListener.class);
	
	@Autowired
	AutomationEngine automationEngine;
	
	private List<BoardListener> listeners;
	
	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{gravityEventsQueue.name}", concurrency = "3")
	@Override
	public void onMessage(Message message) {		
		try {
			
			if( logger.isDebugEnabled()) {
				logger.debug("CardHolder message received");
			}
			
			ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
			ObjectInputStream ois = new ObjectInputStream(bis);
			Serializable body = (Serializable) ois.readObject(); 
			if( body instanceof CardHolder){
				CardHolder cardHolder  = (CardHolder) body; 
				
				for( BoardListener bl : this.listeners){
					try{
						if( logger.isDebugEnabled()) {
							logger.debug("Forwarding Received CardHolder Event to :" + bl.getClass().getCanonicalName());
						}
						bl.onCardHolderEvent(cardHolder);
					} catch (Exception e) {
						logger.warn("Exception Processing BoardListener: " + bl.getClass().getName(), e);
					}
				}
			} else if(body instanceof BoardRule){
				BoardRule timerNotification  = (BoardRule) body; 
				automationEngine.executeActions(timerNotification);
			}else {
				logger.warn("JMS Message Contained Invalid Body");
			}
		} 
		catch (ResourceNotFoundException ignore) {
			// This is required since, during Test executions at the time of builds some stale messages are received from the queues 
			logger.warn("ResourceNotFoundException in JMSEventListener ignored");
		}
		catch (Exception e) {
			logger.error("JMS Exception on Event Reception",e);
		}
	}

	@Autowired
	public void setListeners(List<BoardListener> listeners) {
		if(listeners == null) {
			logger.error("Set Listeners Called with null listeners");
			return;
		}
		
		logger.info("Set Listeners: " + listeners.size());	
		this.listeners = listeners;
		for( BoardListener l : this.listeners){
			logger.info("Registered Board Listener " + l.getClass().getName());
		}
	}
}
