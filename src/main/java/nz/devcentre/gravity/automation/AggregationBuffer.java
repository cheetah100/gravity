/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@Controller
public class AggregationBuffer {
	
	private static final Logger logger = LoggerFactory.getLogger(AggregationBuffer.class);
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private SecurityTool securityTool;
	
	private Map<String, BufferElement> buffer = new ConcurrentHashMap<String, BufferElement>();
	
	/**
	 * 
	 * @param boardId
	 * @param axisFields
	 * @param totalFields
	 */
	public synchronized void store( BufferElement be ) {
		logger.info("New Buffer Element: " + be.getId());
		BufferElement existing = buffer.get(be.getId());
		if( existing!=null) {
			existing.add(be);
		} else {
			buffer.put(be.getId(), be);
		}
	}
	
	@Scheduled( fixedDelayString = "${aggregation.frequency:60000}" )
	public void process() throws Exception {
		
		if( buffer.size()>0) {
			logger.info("Aggregation Buffer Running: " + buffer.size());
		}
		
		this.securityTool.iAmSystem();
		
		// Loop over the  buffer
		for( Entry<String, BufferElement> entry : buffer.entrySet()) {
			
			BufferElement bufferElement = entry.getValue();
			
			// Get the existing card.
			Card card = null;
			try {
				card = cardService.getCard(bufferElement.getBoardId(), entry.getValue().getId());
				
				// Increment the values
				for( Entry<String, Number> value : bufferElement.getValues().entrySet()) {
					Number modification = value.getValue();
					Object existingField = card.getField(value.getKey());
					Number existingValue = 0;
					if( existingField instanceof Number) {
						existingValue = (Number) existingField;
					}
					Number newValue = existingValue.doubleValue() + modification.doubleValue();
					card.getFields().put(value.getKey(), newValue);
				}
				
				// Save the card
				cardService.updateCard(card, null, false);
			
			} catch( ResourceNotFoundException e) {
				card = new Card();
				card.setId(entry.getValue().getId());
				Map<String,Object> fields = new HashMap<String,Object>();
				fields.putAll(entry.getValue().getFields());
				fields.putAll(entry.getValue().getValues());
				card.setFields(fields);
				cardService.createCard(entry.getValue().getBoardId(), card, false);
			}	
		}
		
		buffer.clear();
	}

	public Map<String, BufferElement> getBuffer() {
		return buffer;
	}
}
