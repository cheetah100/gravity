/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.plugin.Plugin;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.exceptions.AutomationExecutionException;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.PluginManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.BoardRule;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.model.transfer.TaskAction;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.AlertService;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.EvaluationService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.services.TaskService;

@Component
public class AutomationEngine {
	private static final Logger logger = LoggerFactory.getLogger(AutomationEngine.class);
	
	@Autowired
	private TaskService taskService;

	@Autowired
	private RuleCache ruleCache;

	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private AlertService alertService;
	
	@Autowired
	private PluginManager pluginManager;
	
	@Autowired
	private EvaluationService evaluationService;
	
	public static Set<String> lockedBoardRules=new HashSet<String>();
	
	public AutomationEngine() {
		super();
		logger.info("Automation Engine Starting");
	}

	public AutomationEngine( PluginManager pluginManager,
			SecurityTool securityTool, 
			RuleCache ruleCache, 
			TaskService taskService) {
		
		super();
		this.pluginManager = pluginManager;
		this.securityTool = securityTool;
		this.ruleCache = ruleCache;
		this.taskService = taskService;
		logger.info("Automation Engine Starting");
	}

	public Map<String, Rule> getRulesFromBoard(String boardId) throws Exception {
		Map<String, String> ruleList = ruleCache.list(boardId);
		logger.info("getRulesFromBoard: " + boardId + " " + ruleList);
		Map<String, Rule> rules = new HashMap<>();

		for (String ruleId : ruleList.keySet()) {
			Rule rule = ruleCache.getItem(boardId, ruleId);
			if (rule != null) {
				rules.put(ruleId, rule);
			}
		}
		return rules;
	}

	public String validate(String boardId, Card card) {
		try {
			Map<String, Rule> rules = getRulesFromBoard(boardId);
			for (Rule rule : rules.values()) {
				if (rule.getRuleType().equals(RuleType.VALIDATION)) {
					if (rule.getAutomationConditions() != null
							&& conditionsMet(card, rule.getAutomationConditions(), null, null)) {
						logger.info("Executing validation rule " + rule.getId() + " for card :" + card.toString());
						Object result = executeActions(card, rule);
						if (result != null) {
							return result.toString();
						}

					}
				}
			}
		} catch (Exception e) {
			logger.warn("Exception in Validation: " + e.getMessage());
			return e.getMessage();
		}
		logger.info("Rule Validation Success: " + card.getId());
		return null;
	}

	public void executeCompulsoryRules(Map<String, Rule> rules, Card card) throws Exception {
		for (Rule rule : rules.values()) {
			if (rule.getRuleType().equals(RuleType.COMPULSORY) && rule.getAutomationConditions() != null
					&& conditionsMet(card, rule.getAutomationConditions(), null, null)) {

				logger.info("Executing compulsory rule " + rule.getId() + " for card :" + card.getId());
				executeActions(card, rule);
			}
		}
	}

	public void executeTaskRules(Map<String, Rule> rules, Card card) throws Exception {
		Collection<CardTask> tasks = taskService.getTasks(card.getBoard(), card.getId());
		for (CardTask task : tasks) {
			if (!task.getComplete()) {
				Rule rule = rules.get(task.getTaskid());
				if (rule.getAutomationConditions() == null) {
					logger.info("Executing task rule " + rule.getId() + " for card :" + card.getBoard() + "." + card.getId());
					executeActions(card, rule);
					this.taskService.taskAction(task, TaskAction.COMPLETE, null);
				} else if (conditionsMet(card, rule.getAutomationConditions(), null, null)) {
					logger.info("Executing task rule " + rule.getId() + " for card :" + card.getBoard() + "." + card.getId());
					executeActions(card, rule);
					this.taskService.taskAction(task, TaskAction.COMPLETE, null);
				}
			}
		}
	}

	/**
	 * Checks Task Assignments and Completion Conditions of Card
	 * 
	 * @param rules
	 * @param card
	 * @throws Exception
	 */
	public void checkTaskRuleAssignments(Map<String, Rule> rules, Card card) throws Exception {
		
		logger.info("Checking Task Assignments and Completion Conditions of Card " + card.getId());
		
		Collection<CardTask> tasks = this.taskService.getTasks(card.getBoard(), card.getId());
		
		for (CardTask task : tasks) {
			if (!task.getComplete()) {
				Rule rule = rules.get(task.getTaskid());
				if (rule != null) {
					taskService.checkAssignment(rule, card, task);
					
					if (rule.getCompletionConditions() != null && rule.getCompletionConditions().size() > 0
							&& conditionsMet(card, rule.getCompletionConditions(), null, null)) {
						
						this.taskService.taskAction(task,TaskAction.COMPLETE,null);
						logger.info("Task COMPLETE " + card.getId() + "." + task.getUuid());
					} else {
						logger.info("task incomplete " + card.getId() + "." + task.getUuid());
					}
				}
			}
		}
	}

	/**
	 * Evaluates task rules by cleaning up duplicate tasks and adding new tasks to cards.
	 * 
	 * @param rules
	 * @param card
	 * @throws Exception
	 */
	public void evaluateTaskRules(Map<String, Rule> rules, Card card) throws Exception {
		Collection<CardTask> tasks = this.taskService.getTasks(card.getBoard(), card.getId());

		Map<String, CardTask> it = new HashMap<String, CardTask>();

		Set<String> tasksToCleanUp = new HashSet<String>();

		for (CardTask task : tasks) {
			if (task.getUuid().contains("[")) {
				tasksToCleanUp.add(task.getTaskid());
			} else {
				it.put(task.getTaskid(), task);
			}
		}

		for (String taskId : tasksToCleanUp) {
			logger.error("Duplicate Task Found: " + taskId + " on card " + card.getId());
			throw new Exception("Duplicate Task Found: " + Encode.forHtmlContent(taskId) + " on card " + Encode.forHtmlContent(card.getId()));
		}

		for (Rule rule : rules.values()) {
			if (rule.getRuleType().equals(RuleType.TASK)) {
				if (rule.getTaskConditions() == null) {
					CardTask existingCardTask = it.get(rule.getId());
					if (existingCardTask == null) {
						this.taskService.createTaskFromRule(rule, card);
					}
				} else {
					if (conditionsMet(card, rule.getTaskConditions(), null, null)) {
						CardTask existingCardTask = it.get(rule.getId());
						if (existingCardTask == null) {
							this.taskService.createTaskFromRule(rule, card);
						}
					} else {
						CardTask existingCardTask = it.get(rule.getId());
						if (existingCardTask != null) {
							this.taskService.deleteTask(existingCardTask);
						}
					}
				}
			} else {
				CardTask existingCardTask = it.get(rule.getId());
				if (existingCardTask != null) {
					this.taskService.deleteTask(existingCardTask);
				}
			}
		}
	}

	public Map<String, Map<String, Boolean>> explain(String boardId, String cardId) throws Exception {

		Map<String, Map<String, Boolean>> result = new HashMap<String, Map<String, Boolean>>();
		Card card = null;

		try {
			card = cardService.getCard(boardId, cardId);

		} catch (ResourceNotFoundException e) {
			throw new Exception("Resource Not Found: " + Encode.forHtmlContent(boardId));
		}

		Map<String, Rule> rules = getRulesFromBoard(boardId);
		Set<Entry<String, Rule>> entrySet = rules.entrySet();

		logger.info("Explaining :" + card.getId());

		for (Entry<String, Rule> entry : entrySet) {
			Rule rule = entry.getValue();
			Map<String, Boolean> conditionMap = new HashMap<String, Boolean>();
			result.put(rule.getName(), conditionMap);
			conditionsMet(card, rule.getTaskConditions(), conditionMap, "Task");
			conditionsMet(card, rule.getAutomationConditions(), conditionMap, "Automation");
			conditionsMet(card, rule.getCompletionConditions(), conditionMap, "Completion");
		}

		return result;
	}
	
	/**
	 * Creates a String representation of a rule explanation
	 * 
	 * @param ruleExplanation
	 */
	public String getRuleExplanationStr(Map<String, Boolean> ruleExplanation) {
		StringBuilder ruleExplanationStr = new StringBuilder();
		boolean first = true;
		for (String key: ruleExplanation.keySet()) {
			if (first) {
				first = false;
			}
			else {
				ruleExplanationStr.append(", ");
			}
			ruleExplanationStr.append((key + ": " + ruleExplanation.get(key)));
		}
		return ruleExplanationStr.toString();
	}

	/**
	 * Checks if the rule conditions are met on a card for automation.
	 * 
	 * @param card
	 * @param rule
	 * @param condition
	 * @param explain
	 * @param prefix
	 */
	public boolean conditionsMet(Card card, Map<String, Condition> conditions,
			Map<String, Boolean> explain, String prefix) {

		if (null == conditions) {
			return true;
		}
		for (Entry<String, Condition> entry : conditions.entrySet()) {
			Condition condition = entry.getValue();

			if (condition.getConditionType() == null) {
				logger.warn("Evaluation Condition Type not set");
				return false;
			}

			boolean result = false;

			switch (condition.getConditionType()) {
			case PROPERTY:
				result = evaluationService.evalProperty(card.getFields(), condition);
				break;
			case TASK:
				try {
					CardTask task = this.taskService.getTask(card.getBoard(), card.getId(), condition.getFieldName());

					result = evalTask(task, condition);
				} catch (Exception e) {
					logger.warn("Error in Task Evaluation: ", e);
					result = false;
				}
				break;
			case PHASE:
				result = evalPhase(card, condition);
				break;
			case NOTIFICATION:
				// We don't execute Notifications here.
				result = false;
				break;
			case TIMER:
				// We don't execute Timers here.
				result = false;
				break;
			case DYNAMIC:
				// We don't execute Timers here.
				result = false;
				break;				
			}

			if (!result) {
				if (logger.isDebugEnabled()) {
					logger.debug("Condition FAILED - " + condition.getFieldName() + " - " + condition.getConditionType());
				}
				if (explain == null) {
					return false;
				}
			}

			if (explain != null) {
				explain.put(prefix + " " + condition.getConditionType() + " " + condition.getFieldName() + " "
						+ condition.getOperation() + " " + condition.getValue(), result);
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Condition Met :" + condition.getFieldName()+ " - " + condition.getConditionType());
			}

		}
		return true;
	}

	private boolean evalTask(CardTask task, Condition condition) {
		Operation operation = condition.getOperation();

		if (task == null) {
			logger.warn("TASK Condition - Null Task - condition:" + condition.getFieldName());
			return false;
		}

		if (operation == null) {
			logger.warn("TASK Condition - Null Operation - condition:" + condition.getFieldName());
			return false;
		}

		logger.debug("TASK Condition - operation:" + condition.getOperation() + " Value:" + task.getComplete());

		switch (operation) {
		case EQUALTO:
			return task.getComplete();
		case NOTEQUALTO:
			return !task.getComplete();
		case COMPLETE:
			return task.getComplete();
		case INCOMPLETE:
			return !task.getComplete();
		default:
			return false;
		}

	}

	private boolean evalPhase(Card card, Condition condition) {
		Operation operation = condition.getOperation();
		List<String> values = Arrays.asList(condition.getValue().split("\\|"));

		switch (operation) {
		case EQUALTO:
			return values.contains(card.getPhase());
		case NOTEQUALTO:
			return !values.contains(card.getPhase());
		}

		return false;
	}

	public void executeActions(BoardRule timerNotification) throws Exception {
		Map<String, Object> context = new HashMap<String, Object>();
		Rule rule = this.ruleCache.getItem(timerNotification.getBoardId(), timerNotification.getRuleId());
		executeActions(rule, context);
	}

	public void executeActions(Rule rule, Map<String, Object> context) throws Exception {

		logger.info("Executing Actions on board:" + rule.getBoardId() + " Rule " + rule.getId());
		
		// Execution of Actions occurs under System.
		this.securityTool.iAmSystem();
		context.put("boardid", rule.getBoardId());
		context.put("ruleid", rule.getId());
		
		List<Action> sortedActionList = QueryService.order(rule.getActions().values());

		for (Action action : sortedActionList) {
			Plugin plugin = this.pluginManager.getPlugins().get(action.getType());
			logger.info( String.format("Action %s.%s",action.getId(), action.getType()));

			try {
				plugin.process(action, context);
			} catch (Exception e) {
				logger.warn("Exception", e);
				raiseAlertOnError(e, rule, action, null);
				throw new Exception("Automation Failed: " + e.getMessage(), e);
			}

			if (logger.isDebugEnabled()) {
				outputContext(context);
			}
		}
	}

	public Object executeActions(Card card, Rule rule) throws Exception {

		logger.info("Executing ActionChain on Card :" + card.getId() + " rule " + rule.getId());
		Map<String, Object> context = new HashMap<String, Object>();
		context.putAll(card.getFields());

		context.put("boardid", card.getBoard());
		context.put("phaseid", card.getPhase());
		context.put("cardid", card.getId());
		context.put("creation-date", card.getCreated());
		context.put("creator", card.getCreator());
		context.put("modified-date", card.getModified());
		context.put("modified-by", card.getModifiedby());

		List<Action> sortedActionList = QueryService.order(rule.getActions().values());

		logger.info("Rule Actions: " + sortedActionList.size());

		for (Action action : sortedActionList) {
			Plugin plugin = this.pluginManager.getPlugins().get(action.getType());
			logger.info( String.format("Card %s Action %s.%s", card.getId(), action.getId(), action.getType()));

			try {
				plugin.process(action, context);
			} catch (Exception e) {
				String message = e.getMessage();
				if (card.getCreated() != null) {
					raiseAlertOnError(e, rule, action, card);
					message = getAlertOnError(e, rule, action);
				}
				logger.info("Exception Running Action: " + message);
				throw new AutomationExecutionException(message);
			}

			if (logger.isDebugEnabled()) {
				outputContext(context);
			}
		}

		Object result = context.get("result");
		return result;
	}

	public Object executeIntegrationActions(Map<String,Object> incomingContext, Integration integration, Batch batch) throws Exception {

		logger.info("Executing ActionChain on Integration :" + integration.getName());
		
		this.securityTool.iAmSystem();
		Map<String, Object> context = new HashMap<String, Object>();
		context.putAll(incomingContext);
		context.put("batchId", batch.getId());

		List<Action> sortedActionList = QueryService.order(integration.getActions().values());
		logger.info("Integration Actions: " + sortedActionList.size());

		for (Action action : sortedActionList) {
			Plugin plugin = this.pluginManager.getPlugins().get(action.getType());
			if( plugin==null) {
				throw new Exception("Plugin Not found: " + action.getType());
			}

			logger.info( String.format("Action %s.%s", action.getId(), action.getType()));
			
			try {
				plugin.process(action, context);
			} catch (Exception e) {
				logger.info("Exception Running Action: " + e.getMessage());
				throw new Exception("Automation Failed", e);
			}

			if (logger.isDebugEnabled()) {
				outputContext(context);
			}
		}

		Object result = context.get("result");
		return result;
	}

	public Map<String, Object> validateCardFields(String boardId, Card card) {
		Map<String, Object> messages = new HashMap<String, Object>();
		messages.putAll(cardService.validateCardRequiredFields(boardId, card));
		messages.putAll(cardService.validateCardReferenceFields(boardId, card, false));
		String validate = validate(boardId, card);
		if (validate != null) {
			messages.put("rules", validate);
		}
		return messages;
	}

	public Map<String, Object> validateCardUpdate(String boardId, Card card) {
		Map<String, Object> messages = new HashMap<String, Object>();
		messages.putAll(cardService.validateCardRequiredFields(boardId, card));
		messages.putAll(cardService.validateCardReferenceFields(boardId, card, false));
		String validate = validate(boardId, card);
		if (validate != null) {
			messages.put("rules", validate);
		}
		return messages;
	}

	private static String getAlertOnError(Throwable e, Rule rule, Action action) throws Exception {
		StringBuilder message = new StringBuilder();
		message.append("Rule ");
		message.append(rule.getId());
		message.append(".");
		message.append(action.getId());
		message.append(".");
		message.append(action.getType());
		message.append(" caused ");

		Throwable cause = e;

		while (cause != null) {
			if (cause.getMessage() != null) {
				message.append("(");
				message.append(cause.getMessage());
				message.append(") ");
			}
			cause = cause.getCause();
		}

		return message.toString();
	}
	
	private void raiseAlertOnError(Throwable e, Rule rule, Action action, Card card) throws Exception {

		StringBuilder message = new StringBuilder();
		message.append("Card: ");
		message.append(card.getId());
		message.append(" Rule ");
		message.append(rule.getId());
		message.append(".");
		message.append(action.getId());
		message.append(".");
		message.append(action.getType());
		message.append(" caused ");

		Throwable cause = e;

		while (cause != null) {
			if (cause.getMessage() != null) {
				message.append("(");
				message.append(cause.getMessage());
				message.append(") ");
			}
			cause = cause.getCause();
		}

		this.alertService.storeAlert(message.toString(), card, "alert", SecurityTool.getCurrentUser(), null, null);
		logger.warn("Automation Exception - Card " + card.toString() + " " + message.toString(), e);
	}

	private static void outputContext(Map<String, Object> context) {

		StringBuilder sb = new StringBuilder();
		for (Entry<String, Object> entry : context.entrySet()) {
			sb.append(entry.getKey());
			sb.append(" = ");

			if (entry.getValue() != null) {
				String value = entry.getValue().toString();
				if( value.length()>500 ) {
					value = value.substring(0, 500);
				}
				sb.append(value);
				sb.append(" (");
				sb.append(entry.getValue().getClass().getName());
				sb.append(")\n");
			} else {
				sb.append(" null\n");
			}
		}

		logger.debug("Context Dump: " + sb.toString());
	}
	
	/**
	 * 
	 * @param boardId
	 * @param rule
	 * @return
	 */
	@Async
	public Map<String, Object> executeOnDemandScriptAction(Rule rule, Map<String, Object> context) throws Exception {
		String key = rule.getBoardId() + "_" + rule.getId() + "_lock";
		lock(key);
		logger.info("Using "+Thread.currentThread().getName()+":Starting Rule Execution:" + key);
		try {
			this.executeActions(rule, context);
			logger.info("Using "+Thread.currentThread().getName()+":Completed Rule Execution:" + key);

		} catch (Exception e) {
			logger.warn("Using "+Thread.currentThread().getName()+":Exception While Rule Execution:" + key, e);
		}
		unlock(key);
		return context;
	}
	
	private static void lock(String key) throws InterruptedException {
		synchronized (lockedBoardRules) {
			while (!lockedBoardRules.add(key)) {
				lockedBoardRules.wait();
			}
		}
	}

	private static void unlock(String key) {
		synchronized (lockedBoardRules) {
			lockedBoardRules.remove(key);
			lockedBoardRules.notifyAll();
		}
	}
	 
}