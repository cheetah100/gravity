/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.transfer.Purge;
import nz.devcentre.gravity.model.transfer.PurgeData;

@Service
public class CardListener {

	private static final Logger logger = LoggerFactory.getLogger(CardListener.class);
	
	private long automationDelay;
	
	private long maxCardsPerBatch;
	
	private List<CardHolder> cardSet = Collections.synchronizedList(new ArrayList<CardHolder>());
	
	/*
	 * The syncronized methods allow the onEvent to add cards while another thread can
	 * pick them up.
	 * 
	 * The reason we use a set is because we will receive multiple events for the same
	 * card. By adding them to a set we remove the duplicates. By having a timer process
	 * the event set every few seconds we ensure that we are not triggering mutiple 
	 * card event messages unnessasarily.
	 */
	public synchronized void addCardHolder( CardHolder cardHolder ){
		if( cardHolder.isValid()){
			int cardIndex = this.cardSet.indexOf(cardHolder);
			
			if(cardIndex > -1){
				CardHolder cardToModify = this.cardSet.get(cardIndex);
				cardToModify.setReceived(System.currentTimeMillis());
			} else {
				cardHolder.setReceived(System.currentTimeMillis());
				this.cardSet.add(cardHolder);
			}
			if( logger.isDebugEnabled()) {
				logger.debug("CardHolder " + cardHolder.getBoardId() + 
						"." + cardHolder.getCardId() + " " 
						+ cardHolder.getReceived() + " Size: " 
						+ this.cardSet.size());
			}
		}
	}
	
	public synchronized Set<CardHolder> getCardSet() {
		
		Set<CardHolder> toSend = new HashSet<CardHolder>();
		Iterator<CardHolder> i = this.cardSet.iterator();
		long count = 0;
		
		while (i.hasNext() && count < this.getMaxCardsPerBatch() ) {
		   CardHolder card = i.next();
		   if( (System.currentTimeMillis() - card.getReceived()) > this.getAutomationDelay() ){
			   toSend.add(card);
			   i.remove();
			   count++;
		   }
		}
		
		return toSend;
	}
	
	public synchronized Set<CardHolder> getReadOnlyCardSet() {
		Set<CardHolder> toSend = new HashSet<CardHolder>(this.cardSet);		
		return toSend;
	}
	
	@Value("${automation_delay:60}")
	public void setAutomationDelay(String automationDelay) {
		this.automationDelay = Long.parseLong(automationDelay);
	}
	
	public void setAutomationDelay(long automationDelay) {
		this.automationDelay = automationDelay;
	}

	public long getAutomationDelay() {
		return automationDelay;
	}
	
	public int getAutomationBacklog(){
		return this.cardSet.size();
	}

	public long getMaxCardsPerBatch() {
		return maxCardsPerBatch;
	}
	
	
	
	public void purgeBacklog(PurgeData purgedata) {
		
		if(purgedata.getPurge().equals(Purge.ALL)) {
			purgeBacklog();
		}
		
		else if(purgedata.getPurge().equals(Purge.BOARD)) {
			purgeBacklogBoard(purgedata.getId());
		}
		
		else {
			purgeBacklogCard( purgedata.getId());
		}
		
	}
	
	private void purgeBacklog(){
		this.cardSet.clear();
	}
	

	private void purgeBacklogBoard(String board){
		Iterator<CardHolder> it = this.cardSet.iterator();
		while( it.hasNext() ){
			CardHolder next = it.next();
			
			if(board!=null && board.equals(next.getBoardId()) ){
				it.remove();
				continue;
			}
		}
	}
	
	private void purgeBacklogCard(String card){
		Iterator<CardHolder> it = this.cardSet.iterator();
		while( it.hasNext() ){
			CardHolder next = it.next();
			
			if(card!=null && card.equals(next.getCardId()) ){
				it.remove();
				continue;
			}
		}
	}
	
	@Value("${automation_maxcardsperbatch:10}")
	public void setMaxCardsPerBatch(long maxCardsPerBatch) {
		this.maxCardsPerBatch = maxCardsPerBatch;
	}
}
