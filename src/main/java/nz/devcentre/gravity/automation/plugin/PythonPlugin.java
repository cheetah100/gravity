/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;
import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.CardHistoryController;
import nz.devcentre.gravity.controllers.CardTaskController;
import nz.devcentre.gravity.controllers.ListController;
import nz.devcentre.gravity.controllers.PivotTableController;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.controllers.UserController;
import nz.devcentre.gravity.model.Action;

import org.python.core.Options;

@Component("python")
@ConfigurationDetail( name="Python Script Runner", 
description = "Runs the specified Python Script found in the Resource.")
@ConfigurationField(field="resource",name="Resource for Python Script", type=ConfigurationFieldType.RESOURCE)
@ConfigurationField(field="properties",name="List of Key Values",type=ConfigurationFieldType.MAP)
public class PythonPlugin implements Plugin{
	
	private static final Logger log = LoggerFactory.getLogger(PythonPlugin.class);
	
	@Autowired
	ResourceController resourceController;
	
	@Autowired
	ListController listController;
	
	@Autowired
	CardController cardController;
	
	@Autowired
	BoardController boardController;
	
	@Autowired
	PivotTableController pivotController;
	
	@Autowired
	CardHistoryController cardHistoryController;
	
	@Autowired
	CardTaskController cardTaskController;
	
	@Autowired
	UserController userController;
	
	@Autowired
	TransformController transformController;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		Options.importSite = false;
		ScriptEngine engine = 
            new ScriptEngineManager().getEngineByName("python");
		
		for( Entry<String,Object> entry : context.entrySet()){
			if(StringUtils.isNotBlank(entry.getKey())){
				Object value = entry.getValue();
				if( value instanceof String){
					String strValue = (String) value;
					value = strValue.replace("$", "\\$");
				}
				engine.put(entry.getKey(), value);
			}
		}

		Map<String,String> properties = config.getProperties();
		if( properties!=null){
			for( Entry<String,String> entry : properties.entrySet()){
				if(StringUtils.isNotBlank(entry.getKey())){
					engine.put(entry.getKey(), entry.getValue());
				}
			}
		}
		
		engine.put("lists", listController);
		engine.put("cards", cardController);
		engine.put("boards", boardController);
		engine.put("pivots", pivotController);
		engine.put("cardhistory", cardHistoryController);
		engine.put("cardtasks", cardTaskController);
		engine.put("users", userController);
		engine.put("transforms", transformController);
		engine.put("log", log);
		
		String script = null;
		if(StringUtils.isNotBlank(config.getResource()) ){
			script = resourceController.getResource((String)context.get("boardid"),config.getResource());
		} else {
			script = config.getMethod();
		}
		
		engine.eval(script);
		Bindings resultBindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		for( Entry<String, Object> entry : resultBindings.entrySet() ){
			if( entry.getKey().startsWith("_") 
					|| entry.getKey().equals("context") 
					|| entry.getKey().equals("print") 
					|| entry.getKey().equals("println")){
				
				continue;
			}
			context.put(entry.getKey(), entry.getValue());
		}
		return context;
	}

	/**
	 * @param resourceController the resourceController to set
	 */
	protected void setResourceController(ResourceController resourceController) {
		this.resourceController = resourceController;
	}	
}
