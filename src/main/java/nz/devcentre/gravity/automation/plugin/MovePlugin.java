/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.services.CardService;

/**
 * This Plugin moves a card from the existing phase to the phase specified in the resource.
 * 
 * @author peter
 */

@Component("move")
@ConfigurationDetail( name="Change Phase", 
description = "Change Phase of current Card")
@ConfigurationField(field="resource",name="Phase to move to", type=ConfigurationFieldType.PHASE)
@ConfigurationField(field="cardlist",name="Field Name of List of Card Objects to move", type=ConfigurationFieldType.FIELD)
public class MovePlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(MovePlugin.class);

	@Autowired
	CardService cardService;
	
	@Autowired
	VariableInterpreter interpreter;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		String targetPhase = config.getResource();
		String cardList = config.getStringValue("cardlist");
		
		if( cardList==null) {
			String boardId = (String) context.get("boardid");
			String phaseId = (String) context.get("phaseid");
			String cardId = (String) context.get("cardid");
			if(!phaseId.equals(targetPhase)) {
				cardService.moveCard(boardId, cardId, targetPhase, "Move");
				context.put("phaseid", targetPhase);
				logger.info("Move of Card: " + cardId + " to phase " + targetPhase);
			}
		} else if( context.containsKey(cardList)) {
			
			logger.info("Move of Cards in: " + cardList + " to phase " + targetPhase);
			List<Card> cards = (List<Card>) context.get(cardList);
			for( Card card : cards) {
				if(!targetPhase.equals(card.getPhase())) {
					cardService.moveCard(card.getBoard(), card.getId(), targetPhase, "Move");
				}
			}
		}
		return context;
	}	
}
