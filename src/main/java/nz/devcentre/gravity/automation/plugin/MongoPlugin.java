/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.DBCollection;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.managers.MongoTemplateManager;
import nz.devcentre.gravity.model.Action;

// TODO Actually complete this plugin!!!

//@Component("mongo")
@ConfigurationDetail( name="Mongo Query", 
description = "Performs Mongo Query on Target Database")
@ConfigurationField(field="resource",name="Collection Name", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="credential",name="Credential", type=ConfigurationFieldType.CREDENTIAL)
@ConfigurationField(field="server",name="Server Name", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="port",name="Port", type=ConfigurationFieldType.STRING)
public class MongoPlugin implements Plugin {
	
	protected static final Logger logger = LoggerFactory.getLogger(MongoPlugin.class);

	Map<String,MongoTemplate> templates = new ConcurrentHashMap<String, MongoTemplate>();
	
	@Autowired
	private MongoTemplateManager mongoTemplateManager;
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		MongoTemplate template = this.mongoTemplateManager.getTemplate(config.getProperties().get("credential"));
		DBCollection collection = template.getCollection(config.getResource());
		long count = collection.getCount();
		
		logger.info("Count for " + collection.getName() + " is " + count);
		context.put("count", count);
		return context;
	}
}
