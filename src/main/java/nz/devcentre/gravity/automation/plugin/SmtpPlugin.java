/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.EmailService;

@Component("smtp")
@ConfigurationDetail( name="SMTP Email Sender", description = "Sends Emails via SMTP Protocol.")
@ConfigurationField(field="subject",name="Subject of the Email", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="to",name="Destination Email", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="from",name="Originator of the Email", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="resource",name="Field containing Message Body", type=ConfigurationFieldType.FIELD)
public class SmtpPlugin implements Plugin{
	
	private static final Logger logger = LoggerFactory.getLogger(SmtpPlugin.class);
	
	@Autowired
	EmailService emailSender;
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String body = (String) context.get(config.getResource());
		String subject = config.getStringValue("subject");
		String to = config.getStringValue("to");
		String from = config.getStringValue("from");
		boolean html = body.toLowerCase().contains("<html");
		String attachment = config.getStringValue("attachment");
		
		logger.info("Email Outgoing - Subject: " + subject + " To: " + to + " from " + from);
	
		emailSender.sendEmail(subject, body, to, null, from, from, html, attachment, (String) context.get("boardid"));
	
		return context;
	}
	
	
	
}
