/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.opencsv.CSVReader;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("csvparser")
@ConfigurationDetail( name="CSV Parser", 
description = "Parse a field containing CSV content into list of records")
@ConfigurationField(field="resource",name="Field containing CSV content", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="response",name="Field to store resulting records", type=ConfigurationFieldType.FIELD)
public class CsvParser implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(CsvParser.class);

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		Object body = context.get(config.getResource());
		
		if( body==null || !(body instanceof String)) {
			logger.info("No Valid Body in message");
			return context;
		}
		
		String cvsString = (String) body;
		CSVReader reader = null;
		try {
			reader = new CSVReader( new StringReader(cvsString));
			List<String[]> allLines = reader.readAll();
			List<Map<String,Object>> records = new ArrayList<>();
			String[] headers = allLines.get(0);
			for( int f=1; f < allLines.size(); f++) {
				Map<String,Object> record = new HashMap<>();
				String[] line = allLines.get(f);
				for(int n=0; n < line.length; n++) {
					record.put(headers[n], line[n]);
				}
				records.add(record);
			}
			context.put(config.getResponse(), records);
			
		} catch (Exception e) {
            logger.warn("Exception parsing csv", e);
        } finally {
        	if( reader!=null) {
        		reader.close();
        	}
        }		
		return context;
	}
}
