/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright Peter Harrison 2016
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("extract")
@ConfigurationDetail( name="Extract Data from Java Object", 
description = "Extracts data from fields on a Java Object")
@ConfigurationField(field="resource",name="Field containing the Java Object to extract from", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="method",name="Method name to call", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="response",name="Field to put result in",type=ConfigurationFieldType.FIELD)

public class ExtractPlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(ExtractPlugin.class);
		
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{

		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		Object target = context.get(config.getResource());
				
		List<Object> parameterValueList = new ArrayList<Object>();
		Object[] parameterValueArray = parameterValueList.toArray();
		
		Method method = null;
		
		try {
			// Find the method to call
			method = target.getClass().getMethod(
					config.getMethod());
		} catch (NoSuchMethodException e ){
			Method[] methods = target.getClass().getMethods();
			List<Method> methodList = Arrays.asList(methods);
			for( Method m: methodList){
				if( m.getName().equals(config.getMethod())){
					method = m;
					break;
				}
			}
		}
		
		if(method==null){
			throw new Exception("Method Not Found: " + Encode.forHtmlContent(target.toString()));
		}
	
		Object response = method.invoke(target,parameterValueArray);
		
		if (response != null) {
			context.put(config.getResponse(), response);
			logger.info("Recevied response is " + response);
		}
		
		return context;
	}
}
