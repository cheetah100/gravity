/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.FilterTools;

/**
 * 
 * @author peter
 *
 */
@Component("cardsearch")
@ConfigurationDetail( name="Card Search", 
description = "Search for a card in another target board")
@ConfigurationField(field="resource",name="Target Board to Search", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="localfield",name="Field in Local Card to use for search",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="remotefield",name="Field in the Target Board to search in",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="view",name="View to apply",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="order",name="Field name to order results by",type=ConfigurationFieldType.STRING)
@ConfigurationField(field="descending",name="Sort descending",type=ConfigurationFieldType.BOOLEAN)

public class CardSearchPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(CardSearchPlugin.class);
	
	@Autowired
	private QueryService queryService;
		
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String boardId = config.getResource();
		String localField = config.getStringValue("localfield");
		String remoteField = config.getStringValue("remotefield");
		String view = config.getStringValue("view");
		String order = config.getStringValue("order");
		String descending_str = config.getStringValue("descending");
		boolean descending = false;
		if(descending_str!=null && descending_str.equals("true")) {
			descending = true;
		}
		String value = context.get(localField).toString();
		
		if( value==null) {
			logger.info("Value not found for search. Looking in " + localField);
			return context;
		}
				
		Filter filter = FilterTools.createSimpleFilter(remoteField, Operation.EQUALTO, value);
		Collection<Card> result = queryService.search(boardId, filter, view, null, order, true, descending);
		
		String method = config.getMethod();
		if( method.equals("all")) {
			context.put(config.getResponse(), result);
		} else if( method.equals("one")){
			if(!result.isEmpty()){
				context.put(config.getResponse(), result.iterator().next());
			}
		} else {
			if(!result.isEmpty()){
				context.put(config.getResponse(), result.iterator().next().getId());
			}
		}
		return context;
	}
}
