/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("object")
@ConfigurationDetail( name="Object Creator", 
description = "Create a Java Object and set its parameters")
@ConfigurationField(field="resource",name="Target class name for object to create", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="properties",name="Mapping of values", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="response",name="Field to place new object", type=ConfigurationFieldType.FIELD)
public class ObjectMapperPlugin implements Plugin {

	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{

		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		// Create and instance of the class
		Class<?> targetClass = Class.forName(config.getResource());
		Object targetObject = targetClass.newInstance();
		
		// Loop over the mappings to set the primitive values
		for( Entry<String,String> entry: config.getProperties().entrySet()){
			String key = entry.getKey();
			Object contextObject = context.get(key);
			if (contextObject !=null) {
				Method methodToCall = targetClass.getDeclaredMethod(entry.getValue(), contextObject.getClass());
				Object value = context.get(key);
				methodToCall.invoke(targetObject, value);
			}			
		}
		
		// Inject the result into the context
		context.put(config.getResponse(), targetObject);
		return context;
	}
}
