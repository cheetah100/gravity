/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.CardCommentController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.services.CardService;

/**
 * The purpose of the Spawn Plugin is to be able to create new cards 
 * on a different board, or even the same board from a parent board.
 * 
 * action.resource = target board
 * action.method = target template
 * action.
 * properties = mapping between target card properties and current card
 * 
 * Target phase will be the first phase.
 * 
 * @author peter
 *
 */

@Component("spawn")
@ConfigurationDetail( name="Spawn new Card", 
description = "Creates a new Card in the Target Board")
@ConfigurationField(field="resource",name="Target Board", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="id_field",name="ID Field", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="parent_board_field",name="Parent board field", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="parent_card_field",name="Parent card field", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="Properties to include in new Card", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="response",name="Field to set new cards id", type=ConfigurationFieldType.STRING)
public class SpawnPlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(SpawnPlugin.class);
	
	@Autowired
	private CardCommentController cardCommentController;
	
	@Autowired
	CardService cardService;
	
	@Autowired
	private BoardsCache boardCache;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String targetBoardId = config.getResource();
		Board targetBoard = boardCache.getItem(targetBoardId);
		
		String targetPhase = null;
		for( Phase phase : targetBoard.getPhases().values()){
			if( phase.getIndex()==1){
				targetPhase = phase.getId();
				break;
			}
		}
		
		if( targetPhase==null){
			logger.warn("First Phase Not Found when Spawning Onto Board " + targetBoard.getName());
			throw new ResourceNotFoundException();
		}
		
		Card card = new Card();
		
		Object idField = config.getStringValue("id_field");
		if( idField!=null) {		
			card.setId((String) context.get(idField));
		}
				
		Object parentBoardField = context.get("parent_board_field");
		if(parentBoardField==null) {
			parentBoardField = config.getStringValue("parent_board_field");
			if(parentBoardField==null) {
				parentBoardField = "parent_board";
			}
		}
		
		Object parentCardField = context.get("parent_card_field");
		if(parentCardField==null) {
			parentCardField = config.getStringValue("parent_card_field");
			if(parentCardField==null) {
				parentCardField = "parent_card";
			}
		}
		
		Map<String,Object> fields = new HashMap<String,Object>();
		fields.put(parentBoardField.toString(), context.get("boardid"));
		fields.put(parentCardField.toString(), context.get("cardid"));
		
		for( Entry<String,String> entry : config.getProperties().entrySet()){
			Object value = entry.getValue();
			if(((String)value).startsWith("#")){
				value = context.get(((String)value).substring(1));
			}
			if( value!=null && !entry.getKey().equals("id")){
				fields.put(entry.getKey(), value);
			}
		}
		
		String comment = "Spawning new card on board " + targetBoardId + " into phase " +  targetPhase; 
		cardCommentController.saveComment(context.get("boardid").toString(),  
				context.get("cardid").toString(), 
				comment);
		
		card.setFields(fields);
		card.setPhase(targetPhase);
		Card createCard = cardService.createCard(targetBoardId, card,false);
		context.put(config.getResponse(), createCard.getId());
		return context;
	}
}