/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("jmssender")
@ConfigurationDetail( name="JMS Sender", 
description = "Send a Message via JMS")
@ConfigurationField(field="resource",name="Queue Name", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="bodyfield",name="Body Field", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="properties",name="Message Headers", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="method",name="Field that contains body of message", type=ConfigurationFieldType.STRING)
public class JmsPlugin implements Plugin {
	
	protected static final Logger LOG = LoggerFactory.getLogger(JmsPlugin.class);	
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		Map<String, String> properties = config.getProperties();
		String queueName = config.getResource();
		String bodyField = config.getStringValue("bodyfield","body");
		String body = (String) context.get(bodyField);
				
		MessageProperties messageProperties = new MessageProperties();
		if (properties != null) {
			for (String key: properties.keySet()) {
				messageProperties.setHeader(key, properties.get(key));
			}
		}
		
		Message message = new Message(body.getBytes(), messageProperties);		
		rabbitTemplate.send(queueName, message);

		return context;
	}
}
