/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("xmlparser")
@ConfigurationDetail( name="XML Document Parser", 
description = "Parses a XML Document to extract fields.")
@ConfigurationField(field="resource",name="XML Document Field Name", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="parameters",name="Fields to extract from XML", type=ConfigurationFieldType.LIST)
public class XmlDocumentParserPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(XmlDocumentParserPlugin.class);
	

	@Autowired
	DocumentBuilderFactory documentBuilderFactory;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		Object body = context.get(config.getResource());
		
		if( body==null || !(body instanceof String)) {
			logger.info("No Valid Body in message");
			return context;
		}
		
		List<String> parameters = config.getParameters();
		if( parameters.isEmpty()) {
			logger.info("No Valid Parameters in Configuration");
			return context;
		}
		
		String xmlString = (String) body;
		
		InputSource inputSource = new InputSource(new StringReader( xmlString));
		
		DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputSource);
		
		for( String parameter : parameters) {
			NodeList nodes = doc.getElementsByTagName(parameter);
			if(nodes.getLength()==0) {
				logger.warn("No nodes for " + parameter);
			}
			for( int f = 0; f<nodes.getLength();f++) {
				Node item = nodes.item(f);
				Node value = item.getFirstChild();
				String nodeValue = value.getNodeValue();
				context.put( item.getNodeName(), nodeValue);
				logger.warn("Node " + parameter + " = " + nodeValue);
			}				
		}
		
		Node root = doc.getFirstChild();
		NodeList childNodes = root.getChildNodes();
		
		for( int f = 0; f<childNodes.getLength();f++) {
			Node item = childNodes.item(f);
			Node value = item.getFirstChild();
			String nodeValue = value.getNodeValue();
			context.put( item.getNodeName(), nodeValue);
		}
		
		return context;
	}
}
