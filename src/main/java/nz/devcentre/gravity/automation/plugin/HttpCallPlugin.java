/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.HttpCallService;
import nz.devcentre.gravity.tools.CallResponse;

@Component("http")
@ConfigurationDetail( name="Http Call", 
description = "Make a HTTP Call")
@ConfigurationField(field="resource",name="End Point URL", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="method",name="HTTP Method", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="credential",name="Crendentials", type=ConfigurationFieldType.CREDENTIAL)
@ConfigurationField(field="response",name="Field to place HTTP response body in", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="Headers", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="retries",name="Number of retries", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="waitperiod",name="Wait Period in seconds between retries", type=ConfigurationFieldType.STRING)
public class HttpCallPlugin implements Plugin {
	
	protected static final Logger LOG = LoggerFactory.getLogger(HttpCallPlugin.class);	
	
	@Autowired
	private HttpCallService httpCallService;

	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);

		String endPoint = config.getResource();
		String method = config.getMethod();
		String body = (String) context.get("body");
		String credentialId = config.getStringValue("credential");
		int retries = config.getIntValue("retries");
		int waitPeriod = config.getIntValue("waitperiod");
				
		CallResponse response = httpCallService.execute( body, context, endPoint, method, config.getProperties(), credentialId, retries, waitPeriod);
		context.put(config.getResponse(), response.getBody());
		if(response.getHeaders()!=null ) {
			context.put("responseHeaders", response.getHeaders());
		}
		return context;
	}
}
