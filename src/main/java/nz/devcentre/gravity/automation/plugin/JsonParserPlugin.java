package nz.devcentre.gravity.automation.plugin;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component("jsonparser")
@ConfigurationDetail( name="JSON Parser", 
description = "Parse JSON Messages")
@ConfigurationField(field="resource",name="Field containing JSON Data", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="Map of properties to include", type=ConfigurationFieldType.MAP)

public class JsonParserPlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(JsonParserPlugin.class);
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		Object body = context.get(config.getResource());
		
		if( body==null || !(body instanceof String)) {
			logger.info("No Valid Body in message");
			return context;
		}
		
		String jsonString = (String) body;
		
		Gson gson = new Gson();
		Object jsonObject = gson.fromJson(jsonString, Object.class);
		
		Map<String, String> properties = config.getProperties();
		if(properties!=null && properties.size()>0) {
			for( Entry<String,String> property :properties.entrySet()) {
				Object o = getObject( property.getValue(), jsonObject);
				context.put(property.getKey(), o);
			}
		} else { 
			if( jsonObject instanceof Map) {
				Map map = (Map) jsonObject;
				for( Object key : map.keySet()  ) {
					if( key instanceof String) {
						Object object = map.get(key);
						context.put((String)key, object);
					}
				}
			} else {
				context.put(config.getResponse(), jsonObject);	
			}			
		}
		return context;
	}
	
	public static Object getObject( String field, Object object) {
		String[] split = field.split("\\.");
		List<String> fields = Arrays.asList(split);
		Object o = object;
		for( String f : fields) {
			if( o !=null && o instanceof Map ) {
				Map map = (Map) o;
				o = map.get(f);
			} else {
				return o;
			}
		}
		return o;
	}

}
