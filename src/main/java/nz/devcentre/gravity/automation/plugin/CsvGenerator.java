/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.FmtNumber;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.TemplateField;

@Component("csvgenerator")
@ConfigurationDetail( name="CSV Content Generator", 
description = "Generate CSV data from data")
@ConfigurationField(field="resource",name="Data field generate from", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="properties",name="Field name mappings",type=ConfigurationFieldType.MAP)
@ConfigurationField(field="response",name="Local field to store result in",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="board",name="Board to use for Template",type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="escape",name="Apply Escaping",type=ConfigurationFieldType.BOOLEAN)
public class CsvGenerator implements Plugin {
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {

		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		List<Card> data = (List<Card>) context.get(config.getResource());
		Map<String,String> fields = config.getProperties();
		String response = config.getStringValue("response");
		String escape = config.getStringValue("escape");
		
		String boardId = config.getStringValue("board");
		if(boardId==null) {
			boardId = (String) context.get("boardid");
		}
		
		Board board = this.boardsCache.getItem( boardId );
		
		if( data==null || data.size()==0) {
			context.put(response, "");
			return context;
		}
		
		String[] header = getHeadersFromMap(data.get(0).getFields());		
		CellProcessor[] processors = getProcessorsFromMap(data.get(0).getFields(), board.getFields());
		StringWriter sw = new StringWriter(); 
		CsvMapWriter mapWriter = new CsvMapWriter( sw, CsvPreference.STANDARD_PREFERENCE );
		
		mapWriter.writeHeader(header);
		for( Card card : data) {
			mapWriter.write(card.getFields(), header, processors);
		}
		mapWriter.close();
		
		String output = sw.toString();
		if("xml".equals(escape)) {
			output = StringEscapeUtils.escapeXml11(output);
		}
		
		context.put(response, output);
		
		return context;
	}
	
	private String[] getHeadersFromMap( Map<String,Object> map) {
		Collection<String> values = map.keySet();
		String[] array = values.toArray(new String[0]);
		return array;
	}
	
	private CellProcessor[] getProcessorsFromMap( Map<String,Object> map, Map<String,TemplateField> fields) {
		Collection<CellProcessor> values = new ArrayList<CellProcessor>();		
		for( Entry<String,Object> entry: map.entrySet()) {
			if( entry.getValue() instanceof Date) {
				String format = getFormat( entry.getKey(), "yyyy-MM-dd HH:mm:ss", fields);
				values.add(new FmtDate(format));
			} else if ( entry.getValue() instanceof Number) {
				String format = getFormat( entry.getKey(), "#0", fields);
				values.add(new FmtNumber(format));
			} else {
				values.add(new Optional());
			}
		}
		return values.toArray(new CellProcessor[0]);
	}
	
	private static String getFormat( String field,  String def, Map<String,TemplateField> fields) {
		String format = def;
		if(fields.containsKey(field)) {
			String fieldFormat = fields.get(field).getValidation();
			if( StringUtils.isNotEmpty(fieldFormat)) {
				format = fieldFormat;
			} 
		}
		return format;
	}
}
