/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.AggregationBuffer;
import nz.devcentre.gravity.automation.BufferElement;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.model.Action;

/**
 * Aggregation Plugin
 * 
 * The Aggregation Plugin is designed to be triggered by rules on a source board
 * to update an aggregation board. It achieves this through the use of an AggregationBuffer
 * to reduce the number of individual updates to the aggregation board.
 * 
 * When the plugin is triggered there are a number of potential conditions.
 * 
 * a) The source card has just been created. 
 *    We can simply send a BufferElement with the axis fields and values.
 *    
 * b) The source card has been updated, the values have changed, but not the axis values.
 *    In this case we need to work out what the values were previously for the source card
 *    and send a single BufferElement with the difference.
 *    
 * c) The source card has been updated, both the axis and values are different.
 *    In this case we need two BufferElements, one reversing the old values and
 *    the other adding to the new values.
 *    
 * d) The source card phase is updated to one that is invisible / card is removed.
 *    In this case we need one BufferElement to reverse the old values.
 *    
 *    
 * To compare new values to old we need to store the old values.
 *
 * @author peterjha
 *
 */

@Component("aggregation")
@ConfigurationDetail( name="Aggregation Update", 
description = "When this action is executed the linked Aggregate board is updated.")
@ConfigurationField(field="resource",name="Aggregate Board", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="parameters",name="List of fields group by",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="values",name="Map of Values", type=ConfigurationFieldType.MAP)
public class AggregationPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(AggregationPlugin.class);
	
	@Autowired
	AggregationBuffer buffer;
	
	@Autowired
	CardController cardController;
	
	@Autowired
	VariableInterpreter variableInterpreter;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		// Get Configuration Details
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		String boardId = config.getResource();
		List<String> axisList = config.getParameters();
		Map<String, String> values = config.getProperties();
		
		logger.info("Aggregation Plugin: " + action.getId());
		
		Map<String,String> aggregationAxis = new HashMap<String,String>();
		for( String axis : axisList) {
			Object value = context.get(axis);
			if( value != null && value instanceof String) {
				aggregationAxis.put(axis, (String)value);
			}
		}
		
		Map<String,Number> aggregationValues = new HashMap<String,Number>();
		for( String field :values.keySet()) {
			Object value = context.get(field);
			if( value != null && value instanceof Number) {
				aggregationValues.put(field, (Number)value);
			}
		}
		
		BufferElement be = new BufferElement(boardId, aggregationAxis, aggregationValues);
		
		
		logger.info("Just About There");
		
		// Has this card been aggregated?
		String oldAxisField = action.getId()+"_axis";
		if( context.containsKey(oldAxisField)) {
			// Get the old Axis and compare
			String oldAxis = (String) context.get(oldAxisField);
			if( oldAxis.equals(be.getAxisId())) {
				// Axis has not changed - modify existing BufferElement
				for( String field :values.keySet()) {
					Object oldObject = context.get(action.getId()+ "_" + field);
					if( oldObject != null && oldObject instanceof Number) {
						Number oldValue = (Number) oldObject;
						Number newValue = be.getValues().get(field);
						Number toStore = newValue.doubleValue() - oldValue.doubleValue();
						be.getValues().put(field, toStore);
					}
				}
			} else {
				// Axis has changed. Create new buffer element to remove old value		
				Map<String,Number> oldAggregationValues = new HashMap<String,Number>();
				for( String field :values.keySet()) {
					Object oldObject = context.get(action.getId()+ "_" + field);
					if( oldObject != null && oldObject instanceof Number) {
						Number oldValue = (Number) oldObject;
						Number toStore = 0 - oldValue.doubleValue();
						oldAggregationValues.put(field, toStore);
					}
				}
				BufferElement oldBuffer = new BufferElement(boardId, oldAxis, oldAggregationValues);
				this.buffer.store(oldBuffer);
			}	
		}
		this.buffer.store(be);
		
		// Store values for this aggregation
		logger.info("Storing current condition");
		
		Map<String,Object> body = new HashMap<String,Object>();
		body.put(action.getId()+"_axis", be.getAxisId());
		for( Entry<String, Number> entry : aggregationValues.entrySet() ) {
			body.put(action.getId() + "_" + entry.getKey(), context.get(entry.getKey()));
		}
		String sourceBoardId = (String) context.get("boardid");
		String cardId = (String) context.get("cardid");
		cardController.updateCard(sourceBoardId, cardId, body, null);
		
		context.putAll(body);
		return context;
	}
}
