/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.services.QueryService;

@Component("examine")
@ConfigurationDetail( name="Examine Board", 
description = "Examine all the card in a Target Board")
@ConfigurationField(field="resource",name="Target Board to Examine", type=ConfigurationFieldType.BOARD)
public class ExaminePlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(ExaminePlugin.class);

	@Autowired
	private CardListener cardListener;
	
	@Autowired 
	private QueryService queryService;
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		String boardId = config.getResource();
		for( Card card : queryService.query(boardId, null, null, null, true)){
			CardHolder cardHolder = new CardHolder(boardId, card.getId());
			cardListener.addCardHolder(cardHolder);
		}
		if( logger.isDebugEnabled()) {
			logger.debug("Examine called on " + boardId);
		}
		return context;
	}
}
