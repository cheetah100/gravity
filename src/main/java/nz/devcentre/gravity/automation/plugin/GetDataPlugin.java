/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.transfer.QueryRequest;
import nz.devcentre.gravity.services.QueryService;

@Component("getdata")
@ConfigurationDetail( name="Get Data", 
description = "Get Data from another Board")
@ConfigurationField(field="resource",name="Target Board to get card from", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="response",name="Local field to store data in",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="Filtering Conditions",type=ConfigurationFieldType.MAP)
@ConfigurationField(field="fields",name="Fields to include",type=ConfigurationFieldType.MAP)
@ConfigurationField(field="maxrecords",name="Maximum Records",type=ConfigurationFieldType.NUMBER)
public class GetDataPlugin implements Plugin {
	
	@Autowired 
	private QueryService queryService;
	
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String boardId = config.getResource();
		String response = config.getResponse();
		Map<String,String> filterProperties = config.getProperties();
		Map<String,String> fields = config.getStringMapValue("fields");
		int maxRecords = config.getIntValue("maxrecords");
		
		List<Condition> conditions = new ArrayList<>();
		if( filterProperties!=null) {
			for( Entry<String,String> entry : filterProperties.entrySet()) {
				if( "phase".equals(entry.getKey())) {
					conditions.add( new Condition( "metadata.phase", Operation.EQUALTO, entry.getValue() ));
				} else {
					conditions.add( new Condition( entry.getKey(), Operation.EQUALTO, entry.getValue() ));
				}
			}
		}
		
		QueryRequest qr = new QueryRequest();
		qr.setBoardId(boardId);
		qr.setConditions(conditions);
		if(maxRecords>0) {
			qr.setPageSize(maxRecords);
		}
		
		if(fields!=null && fields.size()>0) {
			qr.setFields(fields);
		}
		
		PaginatedCards result = this.queryService.query(qr);
		
		if(result!=null){
			context.put(response, result.getData());
		}
		
		return context;
	}
}
