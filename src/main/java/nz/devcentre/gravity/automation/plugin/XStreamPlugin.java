/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;

@Component(value="xml")
@ConfigurationDetail( name="XML Document Generator", 
description = "Generates a XML Document from a Java Object.")
@ConfigurationField(field="resource",name="Field to convert into XML",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="XStream Aliases",type=ConfigurationFieldType.MAP)
@ConfigurationField(field="response",name="Field to inject response into",type=ConfigurationFieldType.FIELD)
public class XStreamPlugin implements Plugin {
	
	private Map<Action,XStream> xstreamCache = new HashMap<Action,XStream>();
	
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		Object objectToInject = context.get(config.getResource());
		XStream xstream = getXStream(action, config.getProperties());
		String response = xstream.toXML(objectToInject);
		context.put(config.getResponse(), response);
		return context;
	}

	/**
	 * This is a caching XStream system.
	 * The XStream is configured once for every action and cached for later use.
	 * 
	 * @param action
	 * @return
	 * @throws ClassNotFoundException 
	 */
	public XStream getXStream(Action action, Map<String,String> properties) throws ClassNotFoundException {
		XStream xStream = this.xstreamCache.get(action);
		if(xStream==null){
			xStream = new XStream();
			for( Entry<String,String> entry : properties.entrySet()){
				xStream.alias(entry.getKey(), Class.forName(entry.getValue()));
			}
			this.xstreamCache.put(action, xStream);
		}
		return xStream;
	}
}
