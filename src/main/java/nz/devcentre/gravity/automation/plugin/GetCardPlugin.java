/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.services.CardService;

/**
 * 
 * @author peter
 *
 */
@Component("getcard")
@ConfigurationDetail( name="Get Card", 
description = "Get Card from another Board")
@ConfigurationField(field="resource",name="Target Board to get card from", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="localfield",name="Local Field to specify Card ID",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="method",name="Field from Target Card to return (optional)",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="response",name="Local field to store result in",type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="properties",name="Map of fields to copy into context",type=ConfigurationFieldType.MAP)
public class GetCardPlugin implements Plugin {
	
	@Autowired 
	private CardService cardService;
		
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);

		String boardId = config.getResource();
		String localField = config.getStringValue("localfield");
		String value = (String) context.get(localField);
		Card result = cardService.getCard(boardId, value);
		
		if(result!=null){

			if( config.getProperties()!=null){
				for( Entry<String,String> property : config.getProperties().entrySet()){
					Object v = result.getField(property.getValue());
					if(v!=null){
						context.put(property.getKey(), v);
					}
				}

			} else {
				if( config.getMethod()!=null) {
					if( config.getMethod().equals("id")) {
						context.put(config.getResponse(), result.getId());
					} else {
						context.put(config.getResponse(), result.getField(config.getMethod()));
					}
				} else {
					context.put(config.getResponse(), result.getField(config.getMethod()));
				}
			}
		}
		return context;
	}
}
