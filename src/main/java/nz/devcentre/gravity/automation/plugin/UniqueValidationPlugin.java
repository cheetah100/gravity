/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.CardService;

@Component("unique")
@ConfigurationDetail( name="Unique Card Check", 
description = "Used in Validation Rules to Validate whether incoming card in unqiue.")
@ConfigurationField(field="parameters",name="Fields which must be unique", type=ConfigurationFieldType.LIST)

public class UniqueValidationPlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(UniqueValidationPlugin.class);

	@Autowired
	private CardService cardService;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String cardId = context.get("cardid").toString();
		String boardId = context.get("boardid").toString();
		
		List<String> parameters = config.getParameters();
		if(parameters.isEmpty()) {
			logger.info("Unique Field List is Empty");
			return context;
		}
		
		logger.info("Unique Field Combination:-" + parameters.toString());
		
		Map<String, Object> fieldValues = new HashMap<>();
		for (String field : parameters) {
			fieldValues.put(field, context.get(field));
			logger.info(context.get(field).toString());
		}
		
		//Method Call to Execute MongoDB Query to check the Entry
		Map<String,Object> result = 
				cardService.validateCardUniqueFields(boardId, fieldValues,cardId);
		if(!result.isEmpty()) {
			context.put("result", result);
		}
		return context;

	}
}
