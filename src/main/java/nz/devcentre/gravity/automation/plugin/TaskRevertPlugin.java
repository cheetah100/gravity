/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.services.TaskService;

@Component("taskrevert")
@ConfigurationDetail( name="Task Revert", 
description = "Revert a Task on this card or all cards.")
@ConfigurationField(field = "resource", name = "Rule to Revert", type = ConfigurationFieldType.STRING)
@ConfigurationField(field = "method", name = "Revert All Cards (all)", type = ConfigurationFieldType.STRING)

public class TaskRevertPlugin implements Plugin {

	private static final Logger logger = LoggerFactory.getLogger(TaskRevertPlugin.class);

	@Autowired
	TaskService taskService;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {

		// resource -- task
		// method -- all , any thing else
		// board id -- from context
		// card id --- from context
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String method = config.getMethod();
		if (method.equals("all")) {
			this.taskService.revertTasks((String) context.get("boardid"), config.getResource());
			if (logger.isDebugEnabled()) {
				logger.debug("Revert of tasks on board " + context.get("boardid") + " task " + config.getResponse());
			}

		} else {
			this.taskService.revertTask((String) context.get("boardid"), (String) context.get("cardid"),
					config.getResource());
			if (logger.isDebugEnabled()) {
				logger.debug("Revert of task on card -- Task :" + config.getResource() + " card :"
						+ (String) context.get("cardid") + "board id :" + (String) context.get("boardid"));
			}
		}

		return context;
	}
}
