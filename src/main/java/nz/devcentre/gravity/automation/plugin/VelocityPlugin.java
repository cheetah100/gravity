/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.model.Action;

@Component("velocity")
@ConfigurationDetail( name="Velocity Templating", 
description = "Creates a resulting document from a Template using Velocity.")
@ConfigurationField(field="resource",name="Template Resource", type=ConfigurationFieldType.RESOURCE)
@ConfigurationField(field="properties",name="Properties to use include in Velocity Template", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="response",name="", type=ConfigurationFieldType.FIELD)
public class VelocityPlugin implements Plugin {
	
	@Autowired
	private ResourceController resourceController;
	
	private VelocityEngine velocityEngine;
	
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String resource = getResourceController().getResource((String)context.get("boardid"),config.getResource());
        VelocityContext velocityContext = new VelocityContext(context);
        
        Map<String,String> properties = config.getProperties();
    	for( Entry<String,String> entry: properties.entrySet()) {
    		velocityContext.put(entry.getKey(), entry.getValue());		
    	}
        
        StringWriter resultWriter = new StringWriter();
        getVelocityEngine().evaluate(velocityContext, resultWriter, config.getResource(), resource);
        String result = resultWriter.toString();
        context.put(config.getResponse(), result);
		return context;
	}

	public ResourceController getResourceController() {
		return resourceController;
	}

	public void setResourceController(ResourceController resourceController) {
		this.resourceController = resourceController;
	}

	@Autowired
	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
		Properties props = new Properties();
		props.put("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.SimpleLog4JLogSystem");
		props.put("runtime.log.logsystem.log4j.category", "velocity");
		props.put("runtime.log.logsystem.log4j.logger", "velocity");
		this.velocityEngine.init(props);
	}

	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}
}
