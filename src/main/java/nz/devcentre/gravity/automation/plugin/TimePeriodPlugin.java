package nz.devcentre.gravity.automation.plugin;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.python.jline.internal.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.security.SecurityTool;

@Component("timeperiod")
@ConfigurationDetail( name="Time Period Resolver", 
description = "Resolves the time peiod by reference to another board.")
@ConfigurationField(field="resource",name="Board", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="method",name="Board Field to use", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="startDate",name="Start Date", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="endDate",name="End Date", type=ConfigurationFieldType.STRING)

public class TimePeriodPlugin implements Plugin {

	private static final Logger LOG = LoggerFactory.getLogger(TimePeriodPlugin.class);

	@Autowired
	private BoardController boardController;
	
	@Autowired
	private SecurityTool securityTool;
	
	private static final String STARTDATE = "startDate";
	private static final String ENDDATE = "endDate";
	
	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		this.securityTool.iAmSystem();

		String boardId = config.getResource();
		Board board = this.boardController.getBoard(boardId);

		LOG.info("Use Board:" + board.getId());

		String boardFieldToUse = config.getMethod();
		String value = "" + (System.currentTimeMillis());

		if (!StringUtils.isBlank(boardFieldToUse)) {
			LOG.info("Use Field:" + boardFieldToUse);

			// If field value is NULL or Blank use current Time
			Object object = context.get(boardFieldToUse);
			if( object != null) {
				if( object instanceof Date) {
					Date theDate = (Date) object;
					value = "" + theDate.getTime();
				} else {
					value = object.toString();
				}
			}
			LOG.info("Use Field:" + boardFieldToUse+" ="+value);

		} else {
			LOG.info("Use current date");
		}

		Map<String, String> properties = config.getProperties();
		if (CollectionUtils.isEmpty(properties)) {
			throw new ValidationException( "Property is Empty");
		}
		if (!(properties.containsKey(STARTDATE) && properties.containsKey(ENDDATE))) {
			throw new ValidationException("Required Property Not Provided");
		}
		if (StringUtils.isBlank(properties.get(STARTDATE).toString())
				|| StringUtils.isBlank(properties.get(ENDDATE).toString())) {
			throw new ValidationException( "Required Property Not Provided");
		}
		
		validateField(board, properties.get(STARTDATE).toString(),true);
		validateField(board, properties.get(ENDDATE).toString(),true);
		
		/**
		 * Fields to Return from Target Board
		 */
		List<String> parameters = config.getParameters();
		if(!CollectionUtils.isEmpty(parameters)) {
			for(String fieldId:parameters) {
				validateField(board, fieldId.toString(),false);
			}
		}
		
		/*
		 * Build the Dynamic Filter based start and end dates
		 */
		Filter filter = new Filter();
		Map<String, Condition> conditions = new LinkedHashMap<String, Condition>();
		Condition startCondition = new Condition(properties.get(STARTDATE).toString(), Operation.ONORBEFORE,value);
		Condition endCondition = new Condition(properties.get(ENDDATE).toString(), Operation.ONORAFTER,value);
		conditions.put("startDate", startCondition);
		conditions.put("endDate", endCondition);
		filter.setConditions(conditions);
		
		Collection<Card> cards = this.boardController.search(boardId, filter, null, null, "id", false, true);
		if(CollectionUtils.isEmpty(cards)) {
			return context;
		}
		Stack<Card> result = new Stack<Card>();
		result.addAll(cards);
		Card timeCard = result.pop();
		
		//if response is not available use action id as prefix
		String responsePrefix=StringUtils.isBlank(config.getResponse())?action.getId():config.getResponse();
		
		if(!CollectionUtils.isEmpty(parameters)) {
			Map<String,Object> pluginResult=timeCard.getFields().entrySet().stream()
			.filter(e->parameters.contains(e.getKey()))
			.collect(Collectors.toMap(e -> responsePrefix+"_"+e.getKey(), e -> e.getValue()));
			context.putAll(pluginResult);
		}
		Log.info("Time Card:-"+timeCard.getFields());
		context.put(responsePrefix+"_id", timeCard.getId());
		return context;
	}

	/**
	 * 
	 * @param board
	 * @param fieldId
	 * @throws ValidationException Check if the provided fields are valid field on
	 *                             the board to Query
	 */
	private void validateField(Board board, String fieldId, boolean ensureDate) throws ValidationException {

		String boardId = board.getId();
		TemplateField field = board.getField(fieldId);
		if (field == null) {
			throw new ValidationException( fieldId + " Not a valid field on Board:" + boardId);
		}
		if (ensureDate && !field.getType().equals(FieldType.DATE)) {
			throw new ValidationException( fieldId + " Not of type Date");
		}
	}
}
