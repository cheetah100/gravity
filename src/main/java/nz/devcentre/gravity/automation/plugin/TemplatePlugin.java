/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

/**
 * This plugin is a simple text replacement plugin which replaces text in a resource
 * with Strings or Objects from the Context. It is similar to velocity and freemarker, 
 * only it actually works ;-)
 * 
 * @author peter
 *
 */
@Component("template")
@ConfigurationDetail( name="Basic Templating", 
description = "Use Template to create a Document. Can be used to create documents with credentials.")
@ConfigurationField(field="resource",name="Template Resource", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="credential",name="Credential", type=ConfigurationFieldType.CREDENTIAL)
public class TemplatePlugin implements Plugin {
	
	@Autowired
	private ResourceController resourceController;
	
	@Autowired
	private SecurityTool securityTool;
		
	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{        

		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String resource = getResourceController().getResource((String)context.get("boardid"),config.getResource());

		StringBuilder builder = new StringBuilder(resource);
		
		for( Entry<String,Object> entry : context.entrySet()){ 
			if( entry.getValue()!=null){
				String valueString = entry.getValue().toString();
				replaceAll( entry.getKey(), valueString, builder);
			}
		}
		
		Map<String,String> properties = config.getProperties();
		if( properties!=null) {
			for( Entry<String,String> entry : properties.entrySet()){
				if("credential".equals(entry.getKey()) && !StringUtils.isEmpty(entry.getValue())){
					this.resolveCredential(entry.getValue(), builder);
				}
				else if( entry.getValue()!=null){
					String valueString = entry.getValue();
					replaceAll( entry.getKey(), valueString, builder);
				}
			}
		}
		
        context.put(config.getResponse(), builder.toString());
		return context;
	}

	/**
	 * Resolve All the Credentials based on the Map In resource property should be
	 * marked as credential.identifier and credential.secret
	 * 
	 * @throws Exception
	 */
	private void resolveCredential(String credentialId, StringBuilder builder) throws Exception {

		Credential secureCredential = this.securityTool.getSecureCredential(credentialId);
		String identifier = secureCredential.getIdentifier();
		String secret = secureCredential.getSecret();
		// Replace identifier
		this.replaceAll("credential_identifier", identifier, builder);
		// Replace secret
		this.replaceAll("credential_secret", secret, builder);

	}
	private void replaceAll( String field, String value, StringBuilder builder){
		
		int x = 0;
		String toFind = "${" + field + "}"; 
		while(x>=0){
			x = builder.indexOf(toFind);
			if(x>=0){
				builder.replace(x, x+toFind.length(), value);
			}
		}
	}
	
	public ResourceController getResourceController() {
		return resourceController;
	}

	public void setResourceController(ResourceController resourceController) {
		this.resourceController = resourceController;
	}
}
