/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;

@Component("property")
@ConfigurationDetail( name="Property Mapper", 
description = "Copies data from one field to others, or creates new values.")
@ConfigurationField(field="properties",name="Field Mappings", type=ConfigurationFieldType.MAP)
public class PropertyMapperPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(PropertyMapperPlugin.class);
	
	@Autowired
	VariableInterpreter interpreter;

	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		Map<String,String> properties = config.getProperties();
		
		if(CollectionUtils.isEmpty(context) || CollectionUtils.isEmpty(properties)) {
			logger.warn("No Properties to Map with action " + action.getId());
			return context;
		}
		
		logger.info("Property Mapper: properties = " + properties.size());
		
		for (Map.Entry<String, String> entry : properties.entrySet()) {
			Object resolve = interpreter.resolve(context, entry.getValue());
			context.put(entry.getKey(), resolve);
			if(logger.isDebugEnabled()) {
				if(resolve!=null) { 
					logger.debug("Property Mapper setting field " + entry.getKey() + " = " + resolve.toString());
				} else {
					logger.debug("Property Mapper setting field " + entry.getKey() + " = NULL");
				}
			}
		}
		return context;
	}
}
