/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.model.Action;

/**
 * This Plugin allows the rule designer to store information from the context back into the card.
 * The reason this does not happen by default is essentially scope; that a scope of change in a 
 * context is limited to the rule under exection. This plugin allows the rule designer to push
 * information back out to the Java Content Repository for use later.
 * 
 * @author peter
 */

@Component("persist")
@ConfigurationDetail( name="Persist Data", 
description = "Persist the data in the Context into the Card.")
@ConfigurationField(field="parameters",name="Fields to persist", type=ConfigurationFieldType.LIST)
@ConfigurationField(field="properties",name="Field Mapping", type=ConfigurationFieldType.MAP)
public class PersistPlugin implements Plugin {

	@Autowired
	CardController controller;
	
	@Autowired
	VariableInterpreter interpreter;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String boardId = (String) context.get("boardid");
		String cardId = (String) context.get("cardid");
		Map<String,Object> body = new HashMap<String,Object>();
		
		List<String> parameters = config.getParameters();
		
		Map<String,String> properties = config.getProperties();
		
		if( parameters != null) {
			for( String field : parameters ){
				body.put(field, context.get(field));
			}
		}
		
		if( properties != null) {
			for (Entry<String, String> entry : properties.entrySet()) {
				Object resolve = interpreter.resolve(context, entry.getValue());
				body.put(entry.getKey(), resolve);
			}	
		}
		
		controller.updateCard(boardId, cardId, body, null,false);
		return context;
	}
}
