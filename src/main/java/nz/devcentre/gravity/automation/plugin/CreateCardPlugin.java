/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.services.CardService;

@Component("createcard")
@ConfigurationDetail( name="Create Card", 
description = "Create a Card in a Target Board")
@ConfigurationField(field="resource",name="Target Board to create card on", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="parameters",name="Fields with map or collection of data to store.",type=ConfigurationFieldType.LIST)
@ConfigurationField(field="properties",name="Fields mappings. Leave empty to store all.",type=ConfigurationFieldType.MAP)
public class CreateCardPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(CreateCardPlugin.class);
	
	@Autowired
	private VariableInterpreter variableInterpreter;
		
	@Autowired
	private CardService cardService;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		String boardId = config.getResource();
		List<String> parameters = config.getParameters();
		
		if(parameters!=null && parameters.size()>0) {
			
			Object field = config.getParameters().get(0);
			Object source = context.get(field);
			
			if( source instanceof Map) {
				Map map = (Map) source;
				for( Object record  : map.values()) {
					if( record instanceof Map ) {
						this.saveCard( boardId, (Map)record, context, config.getProperties());
					}
				}
			} else if( source instanceof Collection) {
				Collection collection = (Collection) source;
				for( Object record  : collection) {
					if( record instanceof Map ) {
						this.saveCard( boardId, (Map)record, context, config.getProperties());
					}
				}
			} else {
				logger.warn("Source Not Map or Collection");
			}
			return context;
			
		} else {
			Card createCard = this.saveCard(boardId, context, context, config.getProperties());
			context.put(config.getResponse(), createCard.getId());
		}		
		return context;
	}
	
	public Card saveCard( String boardId, Map<String,Object> context, Map<String,Object> parentContext, Map<String,String> properties) throws Exception {
		
		logger.info("Creating Card on : " + boardId);
		
		Card card = new Card();
		card.setBoard(boardId);
		
		Map<String,Object> fields = new HashMap<String,Object>();		
//		if( "all".equals(action.getMethod())) {
//			fields.putAll(context);	
//		}
		
		if( properties.size()>0) {
			for( Entry<String,String> entry : properties.entrySet()){
				Object value = entry.getValue();
				if (value != null) {
					if(((String)value).startsWith("#")){
						String fieldName = ((String)value).substring(1);
						value = CardService.getValueByReference(fieldName, context);
						if( value==null) {
							value = CardService.getValueByReference(fieldName, parentContext);
						}
						
						fields.remove(fieldName);
						
						if (value != null) {
							// Value was found in the context or parentContext
							fields.put(entry.getKey(), value);
						}
					}
					else {
						if( value instanceof String) {
							fields.put(entry.getKey(), CardService.parseField((String)value, context, parentContext));
						} else {
							fields.put(entry.getKey(), value);
						}
					}
				}
			}
		} else {
			fields.putAll(context);
		}
		
		card.setFields(fields);
		Card createCard = cardService.createCard(boardId, card, false);
		
		if( createCard!=null) {
			logger.info("Card Created on : " + boardId + " id:" + createCard.getId());
		} else {
			logger.warn("Created Card is Null");
		}
		
		return createCard;
	}
}
