/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.Template;
import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.model.Action;

@Component("freemarker")
@ConfigurationDetail( name="Freemarker Templating", 
description = "Creates a resulting document from a Template using FreeMarker")
@ConfigurationField(field="resource",name="Resource of the template", type=ConfigurationFieldType.RESOURCE)
@ConfigurationField(field="response",name="Field to place resulting completed document", type=ConfigurationFieldType.RESOURCE)
public class FreemarkerPlugin implements Plugin {
	
	@Autowired
	private ResourceController resourceController;

	@Override
	public Map<String,Object> process( Action action, Map<String,Object> context ) throws Exception{    
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
        
		String resource = getResourceController().getResource((String)context.get("boardid"),config.getResource());
        
        Configuration configuration = new Configuration();
        configuration.setEncoding(Locale.ENGLISH, "UTF-8");
        
        Template template = new Template(config.getResource(), new StringReader(resource), configuration);
        Writer output = new StringWriter();
        template.process(context, output);
        String result = output.toString();
        context.put(config.getResponse(), result);
		return context;
	}
	
	public ResourceController getResourceController() {
		return resourceController;
	}

	public void setResourceController(ResourceController resourceController) {
		this.resourceController = resourceController;
	}
}
