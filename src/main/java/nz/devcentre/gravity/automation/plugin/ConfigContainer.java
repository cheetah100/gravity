/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;

public class ConfigContainer {
	
	private static final String VARIABLE_TEMPLATE_TOKEN = "#";
	
	private Map<String,Object> config;
	
	private Map<String,Object> context;
	
	public ConfigContainer(Map<String,Object> config) {
		this.config = config;
	}
	
	public ConfigContainer(Map<String,Object> initial, Map<String,Object> context) {
		this.config = new HashMap<String, Object>( initial );
		this.context = context;
	}
	
	public Map<String,Object> getConfig() {		
		return ImmutableMap.copyOf(this.config);
	}
	
	public String getStringValue( String ref ) {
		Object obj = resolveField(ref);
		if(obj==null) {
			return null;
		}
		return obj.toString();
	}

	public String getStringValue( String ref, String def ) {
		Object obj = resolveField(ref);
		if(obj==null) {
			return def;
		}
		return obj.toString();
	}

	
	public int getIntValue( String ref ) {
		Object obj = resolveField(ref);
		if( obj==null) {
			return 0;
		}
		
		if(obj instanceof Integer) {
			return (Integer) obj;
		}
		
		if(obj instanceof Number) {
			Number num = (Number) obj;
			return num.intValue();
		}
		
		return Integer.parseInt(obj.toString());
	}
	
	public boolean getBooleanValue( String ref ) {
		Object obj = resolveField(ref);
		if( obj instanceof Boolean ) {
			Boolean bool = (Boolean) obj;
			return bool.booleanValue();
		}
		
		if( obj!=null) {
			String str = obj.toString();
			return str.equals("true");
		}
		
		return false;
	}
	
	public List<String> getStringList(String ref) {
		Object obj = resolveField(ref);
		if( obj instanceof List ) {
			List<String> list = (List<String>) obj;
			List<String> newList = new ArrayList<>();
			for( String item: list) {
				newList.add(resolveValue(item).toString());
			}
			return newList;
		} else {
			return null;
		}
	}
	
	public Map<String,String> getStringMapValue(String ref) {
		Object obj = resolveField(ref);
		if( obj instanceof Map ) {
			Map<String,Object> map = (Map<String, Object>) obj;
			Map<String,String> newMap = new HashMap<>();
			for( Entry<String,Object> entry: map.entrySet()) {
				Object value = resolveValue(entry.getValue());
				if(value!=null) {
					newMap.put(entry.getKey(), value.toString());
				}
			}
			return newMap;
		} else {
			return new HashMap<String,String>();
		}
	}

	public Map<String,Object> getObjectMapValue(String ref) {
		Object obj = resolveField(ref);
		if( obj instanceof Map ) {
			Map<String,Object> map = (Map<String, Object>) obj;
			Map<String,Object> newMap = new HashMap<>();
			for( Entry<String,Object> entry: map.entrySet()) {
				Object value = resolveValue(entry.getValue());
				if(value!=null) {
					newMap.put(entry.getKey(), value);
				}
			}
			return newMap;
		} else {
			return null;
		}
	}

	public List<String> getListValue(String ref) {
		Object obj = resolveField(ref);
		if( obj!=null && obj instanceof List ) {
			List<String> newList= new ArrayList<>();
			for( Object o : (List) obj){
				Object value = resolveValue(o);
				if( value!=null) {
					newList.add(value.toString());
				}
			}
			return newList;
		}
		return null;
	}
	
	public boolean contains(String key) {
		return this.config.containsKey(key);
	}
	
	public Object resolveField(String ref) {
		Object value = this.config.get(ref);
		return resolveValue(value);
	}
	
	public Object resolveValue( Object value) {
		if(value!=null && value instanceof String) {
			String valueString = (String) value;
			if( valueString.startsWith(VARIABLE_TEMPLATE_TOKEN)) {
				String contextRef = valueString.substring(1);
				if(context!=null && context.containsKey(contextRef)) {
					return context.get(contextRef);
				} else {
					return null;
				}
			}
		}
		return value;
	}
	
	public String getResource() {
		return this.getStringValue("resource");
	}
	
	public String getMethod() {
		return this.getStringValue("method");
	}
	
	public String getResponse() {
		return this.getStringValue("response");
	}
	
	public List<String> getParameters(){
		return this.getStringList("parameters");
	}
	
	public Map<String,String> getProperties(){ 
		return this.getStringMapValue("properties");
	}
	
	/* Possibly not needed.
	public Object getProperty(String field) {
		Object obj = resolveField("properties");
		 
		if( obj instanceof Map ) {
			Map<?,?> map = (Map) obj;
			Object fieldName = resolveValue(field);
			return map.get(fieldName);
			
		} else {
			return null;
		}
	}
	*/
	
}
