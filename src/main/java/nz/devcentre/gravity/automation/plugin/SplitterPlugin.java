/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.services.EvaluationService;

/**
 * The Splitter will loop over the specified list calling the 
 * specified rule. The input context is either the original
 * context if there are no elements in the input map, or 
 * uses the input map to create a new input context. On the way
 * out if the original context was input the output is also
 * the original context. If not the output map will map the
 * output fields to fields in the original context. This enables
 * looping context isolation.
 * 
 * target_board : The board containing the rule.
 * target_rule : The rule to use in the split loop.
 * field_to_split: The field name containing the array or map to split.
 * inward_map: The map that translates elements in the input to the context.
 * outward_map: The man that translates from the context within the loop
 * to the outer context.
 * 
 * @author peter
 *
 */
@Component("splitter")
@ConfigurationDetail( name="Splitter", description = "Splits lists and calls a Rule")
@ConfigurationField(field="target_board",name="Target Board", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="target_rule",name="Target Rule", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="field_to_split",name="Field to Split", type=ConfigurationFieldType.FIELD)
@ConfigurationField(field="field_map",name="Field Maping", type=ConfigurationFieldType.MAP)
@ConfigurationField(field="parent_field_map",name="Parent Field Maping", type=ConfigurationFieldType.MAP)
public class SplitterPlugin implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(SplitterPlugin.class);
	
	@Autowired
	@Lazy
	private AutomationEngine automationEngine;
		
	@Autowired
	private RuleCache ruleCache;
	
	@Autowired 
	private EvaluationService evaluationService;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		String targetBoard = config.getStringValue("target_board");
		String targetRule = config.getStringValue("target_rule");
		String fieldToSplit = config.getStringValue("field_to_split");
		Map<String, String> fieldMap = config.getStringMapValue("field_map");
		Rule rule = ruleCache.getItem(targetBoard, targetRule);
		
		logger.info("Splitting Card: " + context.get("boardid") + "." + context.get("cardid"));
		
		// Find Object within context to loop over
		Object objectToSplit = context.get(fieldToSplit);
		
		if( objectToSplit instanceof List) {
			processList( context, rule, (List<?>) objectToSplit, fieldMap);
		} else if ( objectToSplit instanceof Map) {
			processList( context, rule, ((Map<?,?>) objectToSplit).values(), fieldMap);
		}
		return context;
	}
	
	private void processList( Map<String,Object> parentContext, 
			Rule rule, Collection<?> list, Map<String,String> fieldMap) throws Exception{

		for( Object item : list) {
			
			Map<String, Object> itemContext = null;
			if(item instanceof Map) {
				logger.info("Splitting Map");
				itemContext = mapNewContext(fieldMap, ((Map<?,?>) item)); 				
			} else if( item instanceof List) {
				logger.info("Splitting List");
				Map<String,Object> map = listToMap((List<?>)item); 
				itemContext = mapNewContext(fieldMap, map);
			} else if( item instanceof Card) {
				logger.info("Splitting Card");
				Card card = (Card) item;
				itemContext = new HashMap<>();
				itemContext.putAll(card.getFields());
				itemContext.put("cardid", card.getId());
				itemContext.put("boardid", card.getBoard());
			} else {
				logger.info("No Split");
			}
			
			if( itemContext!=null) {
				itemContext.put("parent_board", parentContext.get("boardid"));
				itemContext.put("parent_card", parentContext.get("cardid"));
				
				boolean pass = true;
				if(rule.getAutomationConditions()!=null) {
					for( Condition condition: rule.getAutomationConditions().values()) {
						if( condition.getConditionType().equals(ConditionType.PROPERTY)) {
							if(this.evaluationService.evalProperty(itemContext, condition)) {
								pass = false;
								break;
							}
						}
					}
				}
				if(pass) {
					this.automationEngine.executeActions(rule, itemContext);
				}
			}
		}		
	}
	
	static Map<String, Object> mapNewContext(Map<String,String> mapping, Map<?,?> origin) {
		Map<String, Object> context = new HashMap<>();
		if( mapping!=null && mapping.size()>0) {
			for( Entry<String,String> entry: mapping.entrySet()) {
				Object resolve = origin.get(entry.getValue());
				context.put(entry.getKey(), resolve);
			}
		} else {
			for( Entry<?,?> entry : origin.entrySet()) {
				context.put(entry.getKey().toString(), entry.getValue());
			}
		}
		return context;
	}
	
	static Map<String, Object> listToMap( List<?> list){
		Map<String, Object> map = new HashMap<>();
		int f = 1;
		for( Object o : list) {
			map.put("field_"+f, o);
			f++;
		}
		return map;
	}

}
