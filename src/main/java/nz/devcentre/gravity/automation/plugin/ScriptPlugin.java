/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation.plugin;

import java.util.Map;
import java.util.Map.Entry;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardController;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.CardHistoryController;
import nz.devcentre.gravity.controllers.CardTaskController;
import nz.devcentre.gravity.controllers.ListController;
import nz.devcentre.gravity.controllers.PivotTableController;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.controllers.UserController;
import nz.devcentre.gravity.model.Action;

@Component("script")
@ConfigurationDetail( name="JavaScript Runner", 
description = "Runs the specified JavaScript found in the Resource.")
@ConfigurationField(field="resource",name="Javascript Resource", type=ConfigurationFieldType.RESOURCE)
@ConfigurationField(field="properties",name="Properties to add to Engine",type=ConfigurationFieldType.MAP)

public class ScriptPlugin implements Plugin{
	
	private static final Logger log = LoggerFactory.getLogger(ScriptPlugin.class);
	
	@Autowired
	ResourceController resourceController;
	
	@Autowired
	ListController listController;
	
	@Autowired
	CardController cardController;
	
	@Autowired
	BoardController boardController;
	
	@Autowired
	PivotTableController pivotController;
	
	@Autowired
	CardHistoryController cardHistoryController;
	
	@Autowired
	CardTaskController cardTaskController;
	
	@Autowired
	UserController userController;
	
	@Autowired
	TransformController transformController;
	
	@Override
	public Map<String, Object> process(Action action,
			Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		ScriptEngine engine = 
            new ScriptEngineManager().getEngineByName("javascript");
		
		for( Entry<String,Object> entry : context.entrySet()){
			if(StringUtils.isNotBlank(entry.getKey())){
				Object value = entry.getValue();
				if( value instanceof String){
					String strValue = (String) value;
					value = strValue.replace("$", "\\$");
				}
				engine.put(entry.getKey(), value);
			}
		}

		Map<String,String> properties = config.getProperties();
		if( properties!=null){
			for( Entry<String,String> entry : properties.entrySet()){
				if(StringUtils.isNotBlank(entry.getKey())){
					engine.put(entry.getKey(), entry.getValue());
				}
			}
		}
		
		engine.put("lists", listController);
		engine.put("cards", cardController);
		engine.put("boards", boardController);
		engine.put("pivots", pivotController);
		engine.put("cardhistory", cardHistoryController);
		engine.put("cardtasks", cardTaskController);
		engine.put("users", userController);
		engine.put("transforms", transformController);
		engine.put("log", log);
		
		String script = null;
		if(StringUtils.isNotBlank(config.getResource()) ){
			Object boardId = context.get("boardid");
			if( boardId!=null && boardId instanceof String) {	
				script = resourceController.getResource((String)boardId,config.getResource());
			} else {
				log.warn("Board ID not found running script");
				script = config.getMethod();
			}
		} else {
			script = config.getMethod();
		}
		
		engine.eval(script);
		Bindings resultBindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		for( Entry<String, Object> entry : resultBindings.entrySet() ){
			if( entry.getKey().equals("context") || entry.getKey().equals("print") || entry.getKey().equals("println")){
				continue;
			}
			context.put(entry.getKey(), entry.getValue());
		}
		return context;
	}

	/**
	 * @param resourceController the resourceController to set
	 */
	protected void setResourceController(ResourceController resourceController) {
		this.resourceController = resourceController;
	}
	
	/*
	protected Object javascriptTranslate( Object o) {
		
		if(o==null) {
			return null;
		}
		
		if( o instanceof ScriptObjectMirror) {
			ScriptObjectMirror so = (ScriptObjectMirror) o;
			if( so.isArray()) {
				return new ArrayList(so.values());
			} else {
				return new HashMap<String,Object>((ScriptObjectMirror)o);
			}
		}
		
		return o;
	}
	*/
}
