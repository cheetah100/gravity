/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation.plugin;

import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.FilterTools;

@Component("multiparamsearch")
@ConfigurationDetail( name="Multiple Parameter Card Search", 
description = "Search for Card in Target Board using Multiple Parameters")
@ConfigurationField(field="resource",name="Target Board to search", type=ConfigurationFieldType.BOARD)
@ConfigurationField(field="properties",name="Search parameters", type=ConfigurationFieldType.MAP, board="resource")
@ConfigurationField(field="method",name="Either 'all' or 'one'", type=ConfigurationFieldType.STRING)
@ConfigurationField(field="response",name="Field to place query result", type=ConfigurationFieldType.FIELD)

public class MultiParameterCardSearch implements Plugin {
	
	private static final Logger logger = LoggerFactory.getLogger(MultiParameterCardSearch.class);
	
	@Autowired
	private QueryService queryService;
	
	@Autowired
	private BoardsCache boardsCache;

	@Override
	public Map<String, Object> process(Action action, Map<String, Object> context) throws Exception {
		
		ConfigContainer config = new ConfigContainer( action.getConfig(), context);
		
		Map<String, String> values = CardService.resolveStringValues(config.getProperties(), context);
		Board board = boardsCache.getItem(config.getResource());
		Filter filter = FilterTools.createMapFilter(values, board);
		
		Collection<Card> result = queryService.dynamicQuery(config.getResource(), null, filter, null, true);
		
		logger.info("Search results: " + result.size());
		
		if( config.getMethod().equals("all")) {
			context.put(config.getResponse(), result);
		} else if( config.getMethod().equals("one")){
			if(!result.isEmpty()){
				context.put(config.getResponse(), result.iterator().next());
			}
		} else {
			if(!result.isEmpty()){
				String resultId = result.iterator().next().getId();
				context.put(config.getResponse(), resultId);
				logger.info("Setting " + config.getResponse() + " to " + resultId);
			} else {
				logger.info("No Record found");
			}
		}
		return context;
	}
}
