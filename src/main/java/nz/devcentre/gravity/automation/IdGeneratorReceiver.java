/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.ClusterManager;
import nz.devcentre.gravity.model.Coup;
import nz.devcentre.gravity.model.IdRequest;
import nz.devcentre.gravity.model.IdResponse;
import nz.devcentre.gravity.tools.IdentifierTools;

@Component
public class IdGeneratorReceiver extends SyncReceive {
	
	private static final Logger LOG = LoggerFactory.getLogger(IdGeneratorReceiver.class);
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired 
	ClusterManager clusterManager;
	
	@Autowired
	IdentifierTools identifierTools;
	
	@Autowired
	FanoutExchange gravityIdGeneratorExchange;
	
	/**
	 * This onMessage overrides the SyncReceive method which notifies the registered SyncMessage Object.
	 * If this is the Primary Server it will send a IdResponse with a new ID.
	 */
	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{gravityIdGeneratorQueue.name}")
	@Override
	public void onMessage(Message message) {

		// See if this is a IdRequest - If So Process it.
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
			ObjectInputStream ois = new ObjectInputStream(bis);
			Serializable body = (Serializable)ois.readObject();
			if(body instanceof Coup){
				Coup coup = (Coup)body;
				if( !coup.getRequestId().equals(clusterManager.getServerId()) ){					
					if( (coup.getCoupTime() < clusterManager.getStartTime()) ) {
						clusterManager.setNewLeader(coup.getRequestId(), coup.getCoupTime());
					} else {
						LOG.debug("Leadership Challenge Message Received from Younger Node: " + coup.getRequestId() );
					}
				}
			}
			if(clusterManager.isLeader() && body instanceof IdRequest){
				sendNewId((IdRequest)body);
			}
		} 
		catch (ResourceNotFoundException ignore) {
			// This is required since, during Test executions at the time of builds some stale messages are received from the queues 
			LOG.warn("ResourceNotFoundException on Idgeneration ignored");
		}
		catch (Exception e) {
			// Do Something
			LOG.error("Exception on Idgeneration", e);
		} finally {
			super.onMessage(message);	
		}
	}
	
	private void sendNewId(IdRequest request) throws Exception {
		Long id = identifierTools.getNextSequence(request.getBoardId(),request.getName());
		if( LOG.isDebugEnabled()){
			LOG.debug("Generate ID - board: " + request.getBoardId() + " name: " + request.getName() + " ID: " + id);
		}
		IdResponse response = new IdResponse();
		response.setId(id);
		rabbitTemplate.convertAndSend(gravityIdGeneratorExchange.getName(),"", response, new CorrelationIdPostProcessor(request.getRequestId()));
	}

}
