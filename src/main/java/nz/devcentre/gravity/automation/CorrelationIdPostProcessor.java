/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;


import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;


public class CorrelationIdPostProcessor implements MessagePostProcessor {

	private String correlationId;

	public CorrelationIdPostProcessor( String correlationId ){
		this.correlationId = correlationId;
	}
	
	@Override
	public Message postProcessMessage(Message message) throws AmqpException {
		// RabbitMQ API does not support sending user defined Correlation ids across to the other side.. hence sending it in the header
		
		message.getMessageProperties().setCorrelationId(correlationId);
		
		// message.getMessageProperties().setCorrelationIdString(correlationId);
		
		message.getMessageProperties().setHeader("corid", correlationId);
		return message;
	}

}
