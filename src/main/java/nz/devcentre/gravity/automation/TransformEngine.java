/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.model.TransformChain;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.AnnotationTool;
import nz.devcentre.gravity.transforms.Transformer;

@Component
public class TransformEngine {
	
	@Autowired
	private Map<String,Transformer> transformers;
	
	Map<String, PluginMetadata> metadata;
		
	@PostConstruct
	private void init() {
		if(this.metadata==null) {
			this.metadata = AnnotationTool.getMetadata(this.transformers);
		}
	}
	
	public Map<String,Object> execute(TransformChain chain, Map<String,Object> dynamicConfigs, boolean rollup) throws Exception{
		
		List<Transform> transforms = QueryService.order(chain.getTransforms().values());
		Map<String,Object> dataset = new HashMap<String,Object>();
		for( Transform transform : transforms){
			Transformer transformer = getTransformer( transform.getTransformer());
			dataset = transformer.transform(dataset, transform, rollup, dynamicConfigs);
		}
		return dataset;
	}
	
	public Transformer getTransformer(String transformer) {
		return transformers.get(transformer);
	}

	public Map<String,Transformer> getTransformers() {
		return transformers;
	}
	
	public Map<String, PluginMetadata> getMetadata() {
		return this.metadata;
	}

	public void setTransformers(Map<String,Transformer> transformers) {
		this.transformers = transformers;
	}

}
