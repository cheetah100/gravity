/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.automation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import nz.devcentre.gravity.managers.ExceptionManager;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@Service
public class InputFromJms implements MessageListener{
	
	private static final Logger logger = LoggerFactory.getLogger(InputFromJms.class);
		
	@Autowired 
	private CardService cardService;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private ExceptionManager exceptionManager;
	
	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{inputCardQueue.name}")
	@Override
	public void onMessage(Message message) {
			
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			
			String userId = (String) message.getMessageProperties().getHeaders().get("userid");
			if( userId!=null){
				logger.info("JMS System receiving message from " + userId);
				securityTool.iAm(userId);
			} else {
				logger.info("JMS System receiving anonymous message");
				securityTool.iAmSystem();
			}
			
			Card card = objectMapper.readValue(message.getBody(), Card.class);
			Card createCard = cardService.createCard(card.getBoard(), card, false);
			logger.info("Card Created/Updated on " + card.getBoard() + " ID:" + createCard.getId() );
		} catch (Exception e) {			
			this.exceptionManager.raiseException("JMS Input Processing", e.getMessage(), new String(message.getBody()), false);
		}
	}
}
