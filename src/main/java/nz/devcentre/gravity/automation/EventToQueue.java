/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.model.CardHolder;

@Service
public class EventToQueue {

	private static final Logger logger = LoggerFactory.getLogger(EventToQueue.class);
	
	private EngineState state = EngineState.START;
	
	@Autowired
	private CardListener listener;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private FanoutExchange gravityEventsExchange;
	
	@Scheduled( fixedDelayString = "${automation.frequency:3000}" )
	public void checkEvents() {
				
		// If Paused we don't want to call getCardSet() which clears the cache of cards to examine
		if( this.state.equals(EngineState.PAUSE)){
			return;
		}
		
		Set<CardHolder> cardSet = this.listener.getCardSet();
		if (cardSet.size() > 0)
			logger.info("Events to Send: " + cardSet.size());
		
		// If Started we will send messages to the queue to examine cards.
		if(this.state.equals(EngineState.START)){
			for( CardHolder cardHolder : cardSet ){
				logger.info("Sending CardHolder Event: " 
						+ cardHolder.getBoardId() 
						+ "/" 
						+ cardHolder.getCardId());
				
				rabbitTemplate.convertAndSend(gravityEventsExchange.getName(),"", cardHolder);
			}
		}
	}

	public EngineState getEngineState() {
		return this.state;
	}

	public void setAction(EngineState engineState ) {
		this.state = engineState;
	}
}
