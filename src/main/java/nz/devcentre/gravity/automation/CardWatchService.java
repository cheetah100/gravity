/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.automation;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import nz.devcentre.gravity.controllers.CardWatchController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.CardWatch;
import nz.devcentre.gravity.repository.CardWatchRepository;
import nz.devcentre.gravity.services.AuditService;

/**
 * Card Watch Service
 * 
 * This is an inheriently flawed implementation, depending on a separate queue.
 * Instead it should be a BoardListener and intercept messages for cards.
 * 
 * @author peter
 *
 */
//@Service
public class CardWatchService implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(CardWatchService.class);
	
	@Autowired
	private AuditService auditService;
	
	@Autowired
	private CardWatchRepository cardWatchRepository;
	
	// For now not invalidating the watchList, a better way perhaps is implementing this as a cache so it can be invalidated periodically?
	Map<String,List<CardWatch>> watchList = new HashMap<String,List<CardWatch>>();
	
	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{gravityEventsWatchQueue.name}")
	@Override
	public void onMessage(Message message) {		
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
			ObjectInputStream ois = new ObjectInputStream(bis);
			Serializable body = (Serializable) ois.readObject(); 
			if( body instanceof CardHolder){
				CardHolder cardHolder  = (CardHolder) body; 
				processEvent(cardHolder);
			}
		} 
		catch (ResourceNotFoundException ignore) {
			// This is required since, during Test executions at the time of builds some stale messages are received from the queues 
			logger.warn("ResourceNotFoundException in CardWatchService ignored");
		}
		catch (Exception e) {
			logger.error("Exception on Event Reception",e);
		}
	}
	
	/**
	 * Load the watchlist for a board and cache it in the watchList map
	 * @param boardId
	 * @return
	 * @throws Exception
	 */
	protected List<CardWatch> getWatchList(String boardId) throws Exception {
		List<CardWatch> list = watchList.get(boardId);
		if (list == null) {
			try {
				list = cardWatchRepository.findByBoard(boardId);
				watchList.put(boardId, list);
			} catch (ResourceNotFoundException ignore) {
				
			}
		}
		return list;
	}
	
	protected void processEvent(CardHolder cardholder) throws Exception {
		String boardId = cardholder.getBoardId();
		String cardId = cardholder.getCardId();
		logger.info("Processing cardHolder event for boardId:" + boardId + " cardId:" + cardId);
		
		Map<String,CardWatch> watches = getActiveWatchesByUser(boardId,cardId);
		if (watches==null || watches.isEmpty())
			return;
		
		// For each active watch of a user generate a notification if not already present
		Map<String,CardEvent> events = getNotificationsByUser(boardId, cardId, CardWatchController.EVENT_CATEGORY, "New");
		for (String user: watches.keySet()) {
			CardWatch watch = watches.get(user);
			CardEvent event = events.get(user);
			if (event==null) { // add new event, if not already present
				logger.info("Generating notification for the user:" + watch.getUser());
				saveEvent(boardId, cardId, "There are updates on the card you are watching", 
						"New", watch.getUser());
			}
		}
		
	}
	
	/**
	 * Get Notifications already present for a given Board and a given card
	 */
	public Map<String,CardEvent> getNotificationsByUser(String board,
			String card,
			String category, 
			String level ) throws Exception {
		/*
		List<CardEvent> events = eventRepository.findByBoardAndCardAndCategoryAndLevel(board, card, category, level);
		
		Map<String,CardEvent> result = new HashMap<String,CardEvent>();
		
		for (CardEvent event: events) {
			result.put(event.getUser(), event);
		}
		return result;
		*/
		return null;
	}
	/**
	 * Get the watches for all the users who have an active watch on a given card (of a given board)
	 * @param boardId
	 * @param cardId
	 * @return
	 * @throws Exception
	 */
	public Map<String,CardWatch> getActiveWatchesByUser(String boardId, String cardId) throws Exception {
		List<CardWatch> list = getWatchList(boardId);
		Map<String,CardWatch> result = new HashMap<String,CardWatch>();
		if (list == null || list.isEmpty())
			return null;
		for (CardWatch watch: list) {
			if (watch.getCard().equals(cardId) && watch.getActive()== true)
				result.put(watch.getUser(), watch);
		}
		return result;
	}
	
	public CardEvent saveEvent(String boardId, 
		  	 String cardId,
		  	 String message,
		  	 String level,
		  	 String user) throws Exception {

		CardEvent event;
		event = auditService.storeCardEvent(message, boardId, cardId, "New", CardWatchController.EVENT_CATEGORY, user, new Date(), null);
		return event;
	}
	
}
