/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;


import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.services.HttpCallService;

@Component
public class SAMLTokenTool {

	protected static final Logger logger = LoggerFactory.getLogger(SAMLTokenTool.class);
	
	@Autowired
	private HttpCallService httpCallService;
	
	@Autowired
	DocumentBuilderFactory documentBuilderFactory;
	
	public String  getSamlToken(Credential credential) throws Exception {
		String extrctedsamlToken ="";
		String requestBody = credential.getResponseFields().get("samlTokenRequestBody");
		requestBody= requestBody.replace("{samlUserId}", credential.getIdentifier()).replace("{samlUserPassword}", credential.getSecret());
		String decodedRequestBody = StringEscapeUtils.unescapeXml(requestBody);
		String endPoint = credential.getResponseFields().get("samlTokenUrl");
		Map<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/xml");
		
		CallResponse response = httpCallService.execute(decodedRequestBody, null, endPoint, 
				credential.getResponseFields().get("samlTokenUrlMethodType"), headers, null, 1, 1);
		String xmlResponseString = response.getBody();

		StringReader stringReader = new StringReader(xmlResponseString); 
		InputSource is = new InputSource(stringReader);
		DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
		Document doc = builder.parse(is);
		NodeList list = doc.getElementsByTagName(credential.getResponseFields().get("samlTokenTag"));
		extrctedsamlToken = list.item(0).getTextContent();


		return extrctedsamlToken;
		
	}
	
	
}
