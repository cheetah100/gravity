/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;

public class FilterFactory {
	
	public static Filter singleConditionFilter( String field, Operation operation, String value ) {
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		conditions.put("a", new Condition(field, operation,value));
		Filter filter = new Filter();
		filter.setConditions(conditions);
		return filter;
	}

	public static Filter stringMapFilter(Map<String,String> map) {
		Filter filter = new Filter();
		Map<String, Condition> conditions = new HashMap<String,Condition>();
		for( Entry<String,String> entry : map.entrySet()){
			conditions.put(entry.getKey(), new Condition(entry.getKey(),Operation.EQUALTO,entry.getValue()));
		}
		filter.setConditions(conditions);
		return filter;
	}
	
}
