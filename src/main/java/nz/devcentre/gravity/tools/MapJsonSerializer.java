/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class MapJsonSerializer extends JsonSerializer<Map<String, Object>> {

	@Override
	public void serialize(Map<String, Object> fields, JsonGenerator jgen,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		
		jgen.writeStartObject();
		for (Entry<String, Object> entry : fields.entrySet()) {
			Object objectValue = entry.getValue();
			if (objectValue instanceof Date) {
				Date date = (Date) objectValue;
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Pacific/Auckland"));
				String formattedDate = simpleDateFormat.format(date);
				jgen.writeObjectField(entry.getKey().toString(), formattedDate);
			}/*else if(objectValue instanceof List<?>){
				List<String> list = (List<String>) objectValue;
				jgen.writeArrayFieldStart(entry.getKey().toString());
				for(String thing : list){
					jgen.writeString(thing);
				}
				jgen.writeEndArray();
			}*/else{
				jgen.writeObjectField(entry.getKey().toString(), objectValue);
			}
		}
		jgen.writeEndObject();
	}

}