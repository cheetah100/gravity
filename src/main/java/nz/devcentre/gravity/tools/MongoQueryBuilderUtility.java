/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.BsonDocument;
import org.bson.conversions.Bson;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Facet;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UnwindOptions;

import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.PivotType;
import nz.devcentre.gravity.model.transfer.LookupDetails;

/**
 * Used to generate Mongo specific Query Strings for different aggregation
 */
public class MongoQueryBuilderUtility {

	private static final Log logger = LogFactory.getLog(MongoQueryBuilderUtility.class);

	/**
	 * Build Sort Aggregation Query String
	 * @param fields
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String buildSortQuery(Map<String, String> fields) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(fields);
		Bson sort = BsonDocument.parse(jsonString);
		String json = Aggregates.sort(sort).toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry())
				.toJson();
		logger.info("Sort Query String:-" + json);
		return json;
	}
	/**
	 * Build Lookup Aggregation Query String
	 * @param boardId
	 * @param localField
	 * @param foreignField
	 * @param alias
	 * @return
	 */
	public static String buildLookUpQuery(String boardId, String localField, String foreignField, String alias) {
		LookupOperation operation = LookupOperation.newLookup().from(boardId).localField(localField)
				.foreignField(foreignField).as(alias);
		String lookupString = operation.toDBObject(Aggregation.DEFAULT_CONTEXT).toString();
		logger.info("Lookup Query String:-" + lookupString);
		return lookupString;
	}
	
	/**
	 * Build Unwind Aggregation Query String (keep NULL values)
	 * @param unWindObject
	 * @return
	 */
	public static String buildUnWindString(String unWindObject) {

		String string = Aggregates.unwind("$" + unWindObject, new UnwindOptions().preserveNullAndEmptyArrays(true))
				.toString();
		logger.info("Unwind Query String:-" + string);
		return string;
	}
	
	/**
	 * Build AddFields Aggregation Query String
	 * @param fields
	 * @return
	 */
	public static String buildAddFieldString(Map<String, String> fields) {
		List<Field<?>> fieldList = new ArrayList<>();
		for (Entry<String, String> f : fields.entrySet()) {
			fieldList.add(new Field<>(f.getKey(), "$".concat(f.getValue())));
		}
		BsonDocument addField = Aggregates.addFields(fieldList).toBsonDocument(BsonDocument.class,
				MongoClient.getDefaultCodecRegistry());
		logger.info("Add Field Query String:-" + addField.toJson());
		return addField.toJson();
	}
	
	/**
	 * Build Projection Aggregate Query String
	 * @param fields
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String buildProjectionString(Map<String, Integer> fields) throws JsonProcessingException {

		Map<String, String> projectionMap = new LinkedHashMap<>();
		for (Entry<String, Integer> f : fields.entrySet()) {
			projectionMap.put(f.getKey(), f.getValue().toString());
		}
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(projectionMap);
		Bson projection = BsonDocument.parse(jsonString);
		String json = Aggregates.project(projection)
				.toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry()).toJson();
		logger.info("Projection Query String:-" + json);
		return json;
		
	}
	
	/**
	 * Build a facet aggregation Query String- Used for Paginated Result
	 * @param skip
	 * @param pageSize
	 * @param ignorePage
	 * @return
	 */
	public static String buildFacetQuery(long skip, long pageSize, boolean ignorePage) {
		List<Bson> data = new ArrayList<>();
		Aggregation.sort(new Sort(Sort.Direction.DESC, "_id")).toDBObject(Aggregation.DEFAULT_CONTEXT);
		Bson sortBson=BsonDocument.parse(Aggregation.sort(new Sort(Sort.Direction.DESC, "_id")).toDBObject(Aggregation.DEFAULT_CONTEXT).toString());
		data.add(sortBson);
		// Ignore Pagination
		if (!ignorePage) {
			data.add(BsonDocument.parse(Aggregation.skip(Math.toIntExact(skip)).toDBObject(Aggregation.DEFAULT_CONTEXT).toString()));
			data.add(BsonDocument.parse(Aggregation.limit(pageSize).toDBObject(Aggregation.DEFAULT_CONTEXT).toString()));
		}
		
		BsonDocument facetQuery = Aggregates
				.facet(Arrays.asList(new Facet("data", data),
						new Facet("pageInfo",
								Aggregates.count("totalRecords").toBsonDocument(BsonDocument.class,
										MongoClient.getDefaultCodecRegistry()))))
				.toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry());
		logger.info("Facet Query for Pagination:-"+facetQuery.toJson());
		return facetQuery.toJson();

	}
	
	/**
	 * Build In Query String
	 * @param fieldName
	 * @param values
	 * @return
	 */
	public static String buildInQuery(String fieldName,List<?>values) {
		
		BsonDocument bsonDocument = Filters.in(fieldName, values).toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry());
		logger.info("In Query String:-"+bsonDocument.toJson());
		return bsonDocument.toJson();
	}
	
	public static DBObject buildGroupByQuery(List<String> fields,PivotType  pivotType,String aggregationField,String alias) {
		String[] fieldsArray=new String[] {};
		fieldsArray=fields.toArray(fieldsArray);
		if(StringUtils.isEmpty(alias)) {
			alias=(aggregationField.concat("_").concat(pivotType.name())).toLowerCase();
		}
		//Default is Count
		GroupOperation group = Aggregation.group(fieldsArray).count().as(alias);
		switch (pivotType) {
		case SUM:
			group = Aggregation.group(fieldsArray).sum(aggregationField).as(alias);
			break;
		case AVERAGE:
			group = Aggregation.group(fieldsArray).avg(aggregationField).as(alias);
			break;
		case MAX:
			group = Aggregation.group(fieldsArray).max(aggregationField).as(alias);
			break;
		case MIN:
			group = Aggregation.group(fieldsArray).min(aggregationField).as(alias);
			break;
		default:
			break;
		}
		
		return group.toDBObject(Aggregation.DEFAULT_CONTEXT);
	}
	
	public static void addLookup(List<LookupDetails> list, Set<String> context, List<DBObject> pipeline, String key) throws Exception {
		String localField = "";
		int level = 0;
		boolean dirty = false;
		for (LookupDetails ld : list) {
			StringBuffer sb = new StringBuffer();

			if (level == 0) {
				localField += ld.getElement();
			} else {
				localField = localField + "." + ld.getElement();
			}
			
			if (context.contains(localField)) {
				//logger.info("Current Key:-"+key);
				// If this was already processed, don't overwrite
				level++;
				continue;
			}
		
			sb.append("{$lookup:{");
			sb.append("'from':'" + ld.getBoard() + "',");
			sb.append("'localField':'" + localField + "',");
			sb.append("'foreignField':'_id',");
			sb.append("'as':'temp123'}}");
			BasicDBObject lookup = BasicDBObject.parse(sb.toString());
			pipeline.add(lookup);
			sb = new StringBuffer();
			// sb.append("{$unwind:'$temp123'}");
			sb.append("{$unwind:{path:'$temp123',preserveNullAndEmptyArrays: true}}");// Allow Empty Arrays
			BasicDBObject unwind = BasicDBObject.parse(sb.toString());
			if (!ld.getFieldType().equals(FieldType.LIST))// Exclude List Type
				pipeline.add(unwind);
		
			sb = new StringBuffer();
			sb.append("{$addFields:{'");
			sb.append(localField + "':'$temp123'}}");
			/*if (!ld.fieldType.equals(FieldType.LIST))
				sb.append(localField + "':'$temp123'}}");
			else
				sb.append(key + "':'$temp123"+key.substring(localField.length())+"'}}");*/
			BasicDBObject addFields = BasicDBObject.parse(sb.toString());
			pipeline.add(addFields);
			context.add(localField);
			level++;
			dirty = true;
		}

		if (dirty) {
			StringBuffer sb = new StringBuffer();
			sb.append("{$project:{'temp123':0}}");
			BasicDBObject project = BasicDBObject.parse(sb.toString());
			pipeline.add(project);
		}
	}

	public static List<String> getVisiblePhasesForBoard(Board board, String phaseId, List<String> toExclude,
			boolean allPhases) throws Exception {
		List<String> returnList = new ArrayList<String>();
		if (StringUtils.isEmpty(phaseId)) {
			Map<String, Phase> phases = board.getPhases();
			for (Phase phase : phases.values()) {
				if ((!phase.isInvisible() || allPhases) && (toExclude == null || !toExclude.contains(phase.getId()))) {
					if (phase.getId() != null)
						returnList.add(phase.getId());
					else
						logger.error("Error: getVisiblePhasesForBoard - PhaseId is null, boardId: " + board.getId());
				}
			}
		} else if (StringUtils.contains(phaseId, "|")) {
			String[] phaseFilterArray = StringUtils.split(phaseId, "|");
			returnList = Arrays.asList(phaseFilterArray);
		} else {
			returnList.add(phaseId);
		}
		return returnList;
	}
	
	public static String mongoQueryFromConditions(Collection<Condition> conditions, Map<String, String> mappings) {
		StringBuilder sb = new StringBuilder();
		sb.append("{$and:[{");

		boolean first = true;

		for (Condition condition : conditions) {
			if (condition.getConditionType() != null && ConditionType.DYNAMIC.equals(condition.getConditionType()))
				continue;
			if (!((Operation.NOTPHASE).equals(condition.getOperation()))) {
				if (first) {
					first = false;
				} else {
					sb.append("},{");
				}

				String fieldName = condition.getFieldName();
				if (mappings != null && mappings.containsKey(fieldName)) {
					fieldName = mappings.get(fieldName);
				}

				genEx(condition.getOperation().getMongoExpression(), fieldName, condition.getValue(), sb);
			}
		}

		sb.append("}]}");

		return sb.toString();
	}
	
	private static void genEx(String expression, String field, String value, StringBuilder sb) {
		if (value.contains("|")) {
			List<String> values = Arrays.asList(value.split("\\|"));
			sb.append("$or:");
			sb.append("[{");
			boolean first = true;
			for (String v : values) {
				String rf = expression.replaceAll("\\$\\{field\\}", field);
				String rv = rf.replaceAll("\\$\\{value\\}", v);
				if (first) {
					first = false;
				} else {
					sb.append("},{");
				}
				sb.append(rv);
			}
			sb.append("}]");
		} else {
			String rf = expression.replaceAll("\\$\\{field\\}", field);
			String rv = rf.replaceAll("\\$\\{value\\}", value);
			sb.append(rv);
		}
	}


}
