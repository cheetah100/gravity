/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Security
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.PivotType;

@Service
public class MongoQueryTool {
	
	private static final Logger logger = LoggerFactory.getLogger(MongoQueryTool.class);

	@Autowired
	MongoTemplate mongoTemplate;
	
	public static DBObject matchInterval(String field, Interval interval) {
		Date start = interval.getStart().toDate();
		Date end = interval.getEnd().toDate();
		
		StringBuilder sb = new StringBuilder();
		sb.append("{ $match: {\"");
		sb.append(field);
		sb.append("\": {$gte: new Date(");
		sb.append(start.getTime());
		sb.append("), $lt: new Date(");
		sb.append(end.getTime());
		sb.append( ")}}}");
		String returnString = sb.toString();
		return BasicDBObject.parse(returnString);
	}
	
	public static DBObject match(Map<String,Condition> conditions) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ $match: {");
		
		if (conditions.size() == 1) {
			for( Condition condition : conditions.values()) {
				expression( condition.getOperation().getMongoExpression(), condition.getFieldName(), condition.getValue(), sb);
			}
		}
		else {
			sb.append("$and:[ {");
			for( Condition condition : conditions.values()) {
				expression( condition.getOperation().getMongoExpression(), condition.getFieldName(), condition.getValue(), sb);
				sb.append("},{");
			}
		
			int lastDelimeterIdx = sb.lastIndexOf(",{");
			if (lastDelimeterIdx != -1) {
				// Remove last comma 
				//sb.deleteCharAt(sb.lastIndexOf(","));
				sb.delete(lastDelimeterIdx, lastDelimeterIdx + 2);
			}
			sb.append("]");
		}
		
		sb.append("}}");
		
		logger.debug(sb.toString());
		
		return BasicDBObject.parse(sb.toString());
	}


	// { $addFields: { <key>: <expression, concatanated by file seperator> } } //all expressions into a single key
	public static DBObject addFields(String key , List<String> fields, String fileSeperator) {
		
		StringBuilder sb = new StringBuilder();	   	
		boolean first=true;		    	
	    for(String group :fields) {
	        sb.append("");
	        if (first) {
	        	first=false;
	        } else {
	        	sb.append(",")
	        		.append("\"").append(fileSeperator).append("\"")
	        		.append(",");		        			
	        }
	        sb.append( "\"").append("$_id.").append(group).append( "\"");
	    }	    
	    sb.insert(0, " { \"$concat\":[", 0, 14).append("]}");  	
	    logger.info("select List 1 =" + sb.toString() )	;
	    
	    StringBuilder sbFinal = new StringBuilder();
		sbFinal.append("{ $addFields: {");
		sbFinal.append("\"");
		sbFinal.append(key);
		sbFinal.append("\"");
		sbFinal.append(":");		
		sbFinal.append(sb.toString());	
		sbFinal.append(" }}");
		logger.info (" Mongo query for addFields part= " + sbFinal.toString());
		
		return BasicDBObject.parse(sbFinal.toString());
	}

	
	
	public static DBObject lookup(String from, String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ $lookup: { from:\"");
		sb.append(from);
		sb.append("\", localField:\"");
		sb.append(field);
		sb.append("\", foreignField:\"_id\", as:\"");
		sb.append(field);
		sb.append("\"}}");
		return BasicDBObject.parse(sb.toString());
	}
	
	public static DBObject unwind(String field, String indexField) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ $unwind: { \"path\": \"$");
		sb.append( field ); 
		sb.append("\"");
		
		if( indexField!=null) {
			sb.append(", includeArrayIndex: \"");
			sb.append( indexField ); 
			sb.append("\"");
		}
		
		sb.append("}}");
		
		return BasicDBObject.parse(sb.toString());
	}
	
	public static DBObject group(List<String> fields, String valueField, PivotType totalType) {
		StringBuilder sb = new StringBuilder();
		sb.append("{ $group: { \"_id\":{");		
		for(String each : fields) {
			sb.append("\"");
			sb.append(each.replace(".", "_"));
			sb.append("\": \"$");
			sb.append(each);
			sb.append("\",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("}");
		sb.append(totalType.getAggregationQuery(valueField));
		sb.append("}}");	
		return BasicDBObject.parse(sb.toString());
	}
	
	private static void expression(String expression, String field, String value, StringBuilder sb) {
		if (value.contains("|")) {
			List<String> values = Arrays.asList(value.split("\\|"));
			sb.append("$or:");
			sb.append("[{");
			boolean first = true;
			for (String v : values) {
				String rf = expression.replaceAll("\\$\\{field\\}", field);
				String rv = rf.replaceAll("\\$\\{value\\}", v);
				if (first) {
					first = false;
				} else {
					sb.append("},{");
				}
				sb.append(rv);
			}
			sb.append("}]");
		} else {
			String rf = expression.replaceAll("\\$\\{field\\}", field);
			String rv = rf.replaceAll("\\$\\{value\\}", value);
			sb.append(rv);
		}
	}
}
