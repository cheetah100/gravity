/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

public class DateTools {
	
	private static String[] dateFormats = { 
			"yyyy-MM-dd HH:mm:ss",
			"yyyy-MM-dd",
			"MM/dd/yyyy",
			"MMM-dd-yyyy",
			"MM/dd/yy",
			"EEE MMM d HH:mm:ss z yyyy" };
	
	public static Date getDateFromString(String value, String suppliedFormat) throws ParseException{
		
		if(StringUtils.isEmpty(value)){
			return null;
		}
		
		if( suppliedFormat!=null){
			Date parsedDate = getDateByFormat( value, suppliedFormat);
			if( parsedDate!=null){
				return parsedDate;
			}
		}
				
		// is this a UnixTime
		try{
			long dateInMillis = Long.parseLong(value.toString());
			return new Date( dateInMillis);
		} catch( NumberFormatException e){
			// Don't worry, be happy
		}
		
		for( String format : dateFormats ){
			Date parsedDate = getDateByFormat( value, format);
			if( parsedDate!=null){
				return parsedDate;
			}
		}

		throw new ParseException("Invalid Date", -1);
	}
	
	/**
	 * Returns a Date based on supplied value and format.
	 * Creates a new SimpleDateFomat on every call, thus grossly wasteful.
	 * 
	 * @param value
	 * @param format
	 * @return
	 */
	public static Date getDateByFormat( String value, String format ) {
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			dateFormat.setLenient(false);
			return dateFormat.parse(value.toString());
		} catch( ParseException e){
			return null;
		}
	}
	
	public static Date addDays( Date date, String days){
		DateTime dt = new DateTime(date);
		DateTime plusDays = dt.plusDays(Integer.parseInt(days));		
		return plusDays.toDate();
	}

	public static Date daysFromNow(String days){
		return addDays(new Date(), days);
	}
	
}
