package nz.devcentre.gravity.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFields;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.annotations.ConfigurationGroups;
import nz.devcentre.gravity.model.transfer.PluginField;
import nz.devcentre.gravity.model.transfer.PluginMetadata;

/**
 * Annotation Metadata Collector
 * 
 * The purpose of this tool is to collect metadata from plugins. Each element
 * of the supplied plugins map is examined for annotations and Metadata created.
 * 
 * @author peter
 *
 */
public class AnnotationTool {
	
	public static Map<String, PluginMetadata> getMetadata( Map<String,?> plugins){
		
		Map<String, PluginMetadata> metadata = new HashMap<String, PluginMetadata>();
		
		for( Entry<String,?> pluginEntry : plugins.entrySet()) {
			
			PluginMetadata pluginMetadata = new PluginMetadata();
			pluginMetadata.setId(pluginEntry.getKey());
			
			Class<?> clazz = pluginEntry.getValue().getClass();
			
			if(clazz.isAnnotationPresent(ConfigurationField.class)) {
				
				ConfigurationField annotation = clazz.getAnnotation(ConfigurationField.class);
				List<PluginField> configFieldList = new ArrayList<PluginField>();
				configFieldList.add(configurationFieldToPluginField(annotation));
				pluginMetadata.setFields(configFieldList);
		    }
			
			if(clazz.isAnnotationPresent(ConfigurationFields.class)) {
				
				ConfigurationFields annotation = clazz.getAnnotation(ConfigurationFields.class);
				List<PluginField> configFieldList = new ArrayList<PluginField>();
				
				for( ConfigurationField cf : annotation.value()) {
					configFieldList.add(configurationFieldToPluginField(cf));
				}
				
				pluginMetadata.setFields(configFieldList);
		    }
			
			if(clazz.isAnnotationPresent(ConfigurationGroup.class)) {
				
				ConfigurationGroup annotation = clazz.getAnnotation(ConfigurationGroup.class);
				List<String> groups = new ArrayList<String>();
				groups.add(annotation.group());
				pluginMetadata.setTags(groups);
		    }
			
			if(clazz.isAnnotationPresent(ConfigurationGroups.class)) {
				
				ConfigurationGroups annotation = clazz.getAnnotation(ConfigurationGroups.class);
				List<String> groups = new ArrayList<String>();
				
				for( ConfigurationGroup cg : annotation.value()) {
					groups.add(cg.group());
				}
				
				pluginMetadata.setTags(groups);
		    }
			
			if(clazz.isAnnotationPresent(ConfigurationDetail.class)) {
				ConfigurationDetail annotation = clazz.getAnnotation(ConfigurationDetail.class);
				pluginMetadata.setName(annotation.name());
				pluginMetadata.setDescription(annotation.description());
				pluginMetadata.setDocumentUrl(annotation.documentUrl());
			}
			
			metadata.put(pluginMetadata.getId(), pluginMetadata);
		}
		return metadata;
	}
	
	private static PluginField configurationFieldToPluginField( ConfigurationField cf ) {
		PluginField field = new PluginField();
		field.setField( cf.field());
		field.setLabel(cf.name());
		field.setType(cf.type());
		field.setDescription(cf.description());
		field.setRequired(cf.required());
		field.setBoard(cf.board());
		return field;
	}
	
}
