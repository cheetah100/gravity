package nz.devcentre.gravity.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.python.jline.internal.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.controllers.TransformCache;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.model.BoardResource;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.model.TransformChain;
import nz.devcentre.gravity.repository.BoardResourceRepository;
import nz.devcentre.gravity.repository.TransformRepository;
import nz.devcentre.gravity.security.SecurityTool;

@Component
public class TransformTool {

	private static final Logger logger = LoggerFactory.getLogger(TransformController.class);

	@Autowired
	private TransformRepository transformRepository;

	@Autowired
	private BoardResourceRepository boardResourceRepository;

	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	SecurityTool securityTool;

	private List<Object> getResourceParameters(BoardResource boardResource) {

		String delims = "!#";
		List<Object> lString = new ArrayList<Object>();

		if (boardResource.getResource() != null) {
			String[] tokens = boardResource.getResource().split(delims);

			int i = 0;
			int index;
			String parameter = "";

			for (String token : tokens) {
				// token [0] is not required , skip it
				if (i > 0) {

					index = token.indexOf("]"); // check if it is it last token
					if (index > 0) {
						parameter = token.substring(0, index); // and remove every thing after "]"

						lString.add(parameter.replace("\n", "").trim() + ""); // sometime token contains "/n" , remove
																				// it and trim it
						break; // come out of the loop as it is last token
					}
					index = token.indexOf(","); // it is not last token , ',' will present in the token
					if (index > 0) {
						parameter = token.substring(0, index); // remove ',' from the token

						lString.add(parameter);

					}
				}

				i++;

			}
		}

		return lString;
	}

	public Map<String,String> getTransformationChainParameters(TransformChain transformChain) {
		Map<String,String> parameters = new HashMap<String,String>();
		for( Transform transform : transformChain.getTransforms().values()) {
			if(!transform.getTransformer().equals("resource")) {
				continue;
			}
			
			Map<String, Object> config = transform.getConfiguration();
			String resourceId = (String) config.get("resource");
			String boardId = (String) config.get("board");
			
			BoardResource boardResource = boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
		
			if (boardResource != null) {
				List<Object> resourceParameters = getResourceParameters(boardResource);
				for (Object str : resourceParameters) {
					parameters.put(str.toString(),"");
				}
			}
		}
		return parameters;
	}
	
	public TransformChain upsertTransform(TransformChain transformChain, boolean isCreate) {

		TransformChain transformChainExisting = transformRepository.findByIdAndActiveTrue(transformChain.getId());
		if (transformChainExisting != null && !transformChainExisting.getVersion().equals(transformChain.getVersion())) {
			transformChainExisting.setActive(false);
			transformChainExisting.addOrUpdate();
			transformRepository.save(transformChainExisting);
		}

		IdentifierTools.newVersion(transformChain);
		transformChain.addOrUpdate();
		transformChain.setActive(true);
		
		if (isCreate) {
			// Ensure that the current user is assigned as the owner of the transformChain.
			// By default also add the administrators group as a owner.
			transformChain.setPermissions(this.securityTool.initPermissions(transformChain.getPermissions()));
		}
		
		transformRepository.save(transformChain);
		this.cacheInvalidationManager.invalidate(TransformCache.TYPE, transformChain.getId());
		Log.info("Transform chain Updated. Id=" + transformChain.getId());
		return transformChain;
	}

	public void updateAllResourceParameters() {
		List<TransformChain> lTransformChain = transformRepository.findByActiveTrue();
		for (TransformChain transformChain : lTransformChain) {
			logger.info("Analysing Transformid. id=" + transformChain.getId());
			Map<String, String> parameters = this.getTransformationChainParameters(transformChain);
			transformChain.setParameters(parameters);
			this.upsertTransform(transformChain, false);
		}
	}
	

}
