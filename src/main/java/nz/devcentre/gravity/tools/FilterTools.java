/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.TemplateField;

public class FilterTools {
		
	public static Filter createSimpleFilter( String field, Operation operation, String value) {
		Map<String,Condition> conditions = new HashMap<String,Condition>();
		conditions.put("a", new Condition(field,operation,value));
		Filter filter = new Filter();
		filter.setName("dynamic");
		filter.setConditions(conditions);
		return filter;
	}
	
	public static Filter createMapFilter( Map<String,String> map, Board board) throws Exception {
		Map<String, Condition> conditions = new HashMap<String,Condition>();
		for( Entry<String,String> entry : map.entrySet()){
			Condition c = new Condition(entry.getKey(),null,null);
			
			Operation op = null;
			String value = (String) entry.getValue();
			if(value.contains(":")) {
				int pos = value.indexOf(":");
				try {
					op = Operation.valueOf(value.substring(0, pos));
					value = value.substring(pos+1);
				} catch (IllegalArgumentException e) {
					// No worries - isn't really a OP
				}
			}
			
			if(op!=null) {
				c.setOperation(op);
				c.setValue(value);
			} else {
				c.setValue(value);
				TemplateField templateField = board.getField(entry.getKey());
				if( templateField==null ){
					c.setOperation(Operation.EQUALTO);
				} else if( templateField.getType().equals(FieldType.NUMBER)){
					c.setOperation(Operation.NUMBEREQUALTO);
				} else if( templateField.getType().equals(FieldType.DATE)){
					c.setOperation(Operation.AT);
				} else {
					c.setOperation(Operation.EQUALTO);
				}				
			}

			conditions.put(entry.getKey(), c);
		}
		
		if(conditions.size()==0){
			throw new Exception("Insufficient Criteria for Search");
		}
		
		Filter filter = new Filter();
		filter.setConditions(conditions);
		
		return filter;
	}
		
	public static Filter replaceFilterValues( Filter filter, Map<String, Object> fieldMap){
			
		Filter newFilter = new Filter();
		newFilter.setConditions(new HashMap<String, Condition>());
		newFilter.setName(filter.getName());
		
		for( Entry<String,Condition> entry: filter.getConditions().entrySet()){
			Condition condition = entry.getValue();
			Condition newCondition = new Condition(condition.getFieldName(),condition.getOperation(),null);
			newCondition.setConditionType(condition.getConditionType());
			
			if( fieldMap.containsKey(condition.getFieldName())){
				newCondition.setValue(stringFromObject(fieldMap.get(condition.getFieldName())));
			} else {
				newCondition.setValue(condition.getValue());
			}
			newFilter.getConditions().put(entry.getKey(), newCondition);
		}
		
		return newFilter;
		
	}
	
	private static String stringFromObject(Object object){
		if( object instanceof String){
			return object.toString();
		}
		
		if(object instanceof List){
			List list = (List) object;
			StringBuffer sb = new StringBuffer();
			boolean first = true;
			for( Object s: list){
				if(first){
					first = false;
				} else {
					sb.append("|");
				}
				sb.append(s.toString());
			}
			return sb.toString();
		}
		return object.toString();
	}
	
	/**
	 * Apply Stored filter conditions to dynamic filter
	 * @param filter
	 * @param storedFilter
	 */
	public static void applyStoredFilter(Filter filter, Filter storedFilter) {

		if (storedFilter!=null && storedFilter.getConditions() != null) {
			filter.getConditions().putAll(storedFilter.getConditions());
		}
		if (storedFilter!=null && storedFilter.getPhase() != null && filter.getPhase() == null) {
			filter.setPhase(storedFilter.getPhase());
		}

	}
}
