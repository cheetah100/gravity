package nz.devcentre.gravity.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.stereotype.Component;

@Component
public class LdapUserAttributesMapper implements AttributesMapper<Map<String,Object>> {

	private static final Logger logger = LoggerFactory.getLogger(LdapUserAttributesMapper.class);
	
	@Override
	public Map<String,Object> mapFromAttributes(Attributes attrs) throws NamingException {
		Map<String, Object> userMap = new HashMap<String, Object>(); 

		NamingEnumeration<? extends Attribute> all = attrs.getAll();
		while( all.hasMore()) {
			Attribute next = all.next();
			try {
				userMap.put( next.getID(), next.get());
			}
			catch(NoSuchElementException e) {
				//Attribute value not found, read next attribute 
			}
		}

		return userMap;
	}

	protected String parseLdapTimestamp(String ldapTimestamp) {
    	String[] parts = ldapTimestamp.split("[.]");
        String dateTimePart = parts[0];  
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));  
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
            Date tempDate = sdf.parse(dateTimePart); 
            return formatter.format(tempDate);  
        } catch (ParseException ex) {
            logger.error("Parsing LDAP Date exception \\n", ex);
        }
        return null;
    }
	
	protected String getManager(String managerString) {
		String result ="";
		
		if (managerString.indexOf("CN=")!= -1 ) {
			if( managerString.indexOf(',')!= -1) 
				result = managerString.substring(managerString.indexOf("CN=") + 3, managerString.indexOf(','));
			else
				result = managerString.substring(managerString.indexOf("CN=") + 3);
		}  	
    	return result;
    }
	
	
}



