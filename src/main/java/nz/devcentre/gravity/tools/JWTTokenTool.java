/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import nz.devcentre.gravity.model.Credential;

public class JWTTokenTool {
	
	public static String makeJwtToken(Credential credential) throws UnsupportedEncodingException {

		Date expiryDate =calculateExpiryTime(credential.getResponseFields().get("jwtExpireTime"));

		byte[] decodedSecret = Base64.getDecoder().decode(credential.getSecret().getBytes("UTF-8"));

		String token =  Jwts.builder()
				.setSubject(credential.getResponseFields().get("jwtSubject"))
				.claim("name", credential.getResponseFields().get("jwtName"))
				.setIssuer(credential.getIdentifier())
				.setExpiration(expiryDate)
		     	.signWith(SignatureAlgorithm.HS256, decodedSecret).compact();
		
		return token;
	}
	
	static Date calculateExpiryTime( String expiry ) {
		long expiryTime = Long.parseLong(expiry);
		Date expiryDate = new Date(System.currentTimeMillis() + expiryTime);
		return expiryDate;
	}

}
