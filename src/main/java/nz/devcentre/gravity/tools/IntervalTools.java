/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import nz.devcentre.gravity.model.transfer.Interval;

@Service
public class IntervalTools {

	public static final String STARTDATE = "startDate";

	public static final String ENDDATE = "endDate";

	@Autowired
	private MongoTemplate mongoTemplate;
	protected static final Logger logger = LoggerFactory.getLogger(IntervalTools.class);

	/*
	 * This method gets the name of quarter or name of fiscal month based on board
	 * id and startdate the boarfdId should be either fiscal_months or quarter it
	 * find the name by getting the data by running the query to Mondo DB The query
	 * is -- get the record whose startDate is less than or equal to given a date
	 * and end date is greater than equal to given date for a given board
	 *
	 * ( "$and" : [ { "startDate" : { "$lte" : { "$date" :
	 * "2016-08-29T04:00:00.000Z"}}} , { "endDate" : { "$gte" : { "$date" :
	 * "2016-08-29T04:00:00.000Z"}}}]})
	 */

	/*
	 * The method returns Interval object for given boardId and InoputDate
	 */

	public Interval getInterval(String boardId, Date inputDate) throws ParseException {

		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);

		BasicDBObject getQuarterQueryPart1 = new BasicDBObject();
		getQuarterQueryPart1.put(STARTDATE, new BasicDBObject("$lte", inputDate));

		BasicDBObject getQuarterQueryPart2 = new BasicDBObject();
		getQuarterQueryPart2.put(ENDDATE, new BasicDBObject("$gte", inputDate));

		BasicDBList and = new BasicDBList();

		and.add(getQuarterQueryPart1);

		and.add(getQuarterQueryPart2);

		DBObject query = new BasicDBObject("$and", and);

		logger.debug("quarter/fiscal Month Tools query  for fiscal Month/quarter Name =" + query.toString());

		DBCursor cursor = collection.find(query);
		Interval interval = new Interval();
		while (cursor.hasNext()) {

			DBObject currentRecord = cursor.next();

			Date startDate = (Date) currentRecord.get(STARTDATE);
			Date endDate = (Date) currentRecord.get(ENDDATE);

			long daysElapsed = (inputDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);

			interval.setId((String) currentRecord.get("_id"));
			interval.setStartDate(startDate);
			interval.setEndDate(endDate);
			interval.setName((String) currentRecord.get("name"));
			interval.setDaysElapsed(daysElapsed);

		}

		return interval;
	}

}
