/**	
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import nz.devcentre.gravity.exceptions.CardValidationException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.security.SecurityTool;

public class CardTools {

	private static final Logger logger = LoggerFactory.getLogger(CardTools.class);
	
	private static DecimalFormat DF = new DecimalFormat("#");
		
	public static List<Card> orderCards(Collection<Card> cards, String field, boolean descending, View viewObj, Board boardObj) {
		String orderField = field;
		
		List<Card> list = null;
		if (cards instanceof List) {
			list = (List<Card>) cards;
		} else {
			list = new ArrayList<Card>(cards);
		}
		
		if (orderField == null) {
			if (viewObj != null && viewObj.getOrderField() != null) {
				orderField = viewObj.getOrderField();
			}
			else if (boardObj.getOrderField() != null) {
				orderField = boardObj.getOrderField();
			}
		}
		
		Collections.sort(list, new OrderedCardComparator(orderField,descending));
		return list;
	}

	static class OrderedCardComparator implements Comparator<Card> {

		private String field;
		
		private boolean descending;

		public OrderedCardComparator(String field, boolean descending) {
			this.field = field;
			this.descending = descending;
		}

		public int compare(Card c1, Card c2) {
			Object n1 = c1.getFields().get(field);
			String s1 = null;
			Date t1 = null;
			Double d1 = null;
			Long l1 = null;
			if (n1 != null) {
				if (n1 instanceof Double) {
					d1 = (Double) n1;
				} else if (n1 instanceof Long) {
					l1 = (Long) n1;
				} else if (n1 instanceof String) {
					s1 = (String) n1;
				} else if (n1 instanceof Date) {
					t1 = (Date) n1;
				} else {
					s1 = n1.toString();
				}
			} else {
				s1 = "";
			}

			Object n2 = c2.getFields().get(field);
			Double d2 = null;
			Long l2 = null;
			String s2 = null;
			Date t2 = null;
			if (n2 != null) {
				if (n2 instanceof Double) {
					d2 = (Double) n2;
				} else if (n2 instanceof Long) {
					l2 = (Long) n2;
				} else if (n2 instanceof String) {
					s2 = (String) n2;
				} else if (n1 instanceof Date) {
					t2 = (Date) n2;
				} else {
					s2 = n2.toString();
				}
			} else {
				s2 = "";
			}

			if( !descending) {
				if (d1 != null && d2 != null) {
					return d1.compareTo(d2);
				} else if (l1 != null && l2 != null) {
					return l1.compareTo(l2);
				} else if (s1 != null && s2 != null) {
					return s1.compareTo(s2);
				} else if (t1 != null && t2 != null) {
					return t1.compareTo(t2);
				} else {
					logger.warn("Comparitor: Types Do Not match");
					throw new IllegalArgumentException();
				}
			} else {
				if (d1 != null && d2 != null) {
					return d2.compareTo(d1);
				} else if (l1 != null && l2 != null) {
					return l2.compareTo(l1);
				} else if (s1 != null && s2 != null) {
					return s2.compareTo(s1);
				} else if (t1 != null && t2 != null) {
					return t2.compareTo(t1);
				} else {
					logger.warn("Comparitor: Types Do Not match");
					throw new IllegalArgumentException();
				}
				
			}
		}
	}

	/**
	 * Update Card Metadata
	 */
	public static void updateModified(Card card) {
		if (card != null) {
			card.setModified(new Date());
			card.setModifiedby(SecurityTool.getCurrentUser());
		}
	}
	
	public static double getNumberFromObject(Object number) {
		if (number instanceof Number) {
			return ((Number) number).doubleValue();
		} else {
			try {
				return Double.parseDouble(number.toString());
			} catch( NumberFormatException e) {
				return 0d;
			}
		}
	}

	public static boolean getBooleanFromObject(Object object) {
		if (object instanceof Boolean) {
			return ((Boolean) object).booleanValue();
		} else {
			boolean newBool = Boolean.parseBoolean(object.toString());
			return newBool;
		}
	}

	public static Map<String, Object> cardToMap(Card card) {
		if (card == null) {
			logger.warn("Null Card in CardToMap");
			return null;
		}
		Map<String, Object> fields = card.getFields();
		fields.put("id", card.getId());
		fields.put("phase", card.getPhase());
		fields.put("board", card.getBoard());
		return fields;
	}

	public static Card mongoDBObjectToCard(DBObject o, String boardId) {
		Card card = new Card();
		card.setId(o.get("_id").toString());
		card.setBoard(boardId);

		Object metadataObject = o.get("metadata");
		if (metadataObject != null) {
			DBObject md = (DBObject) metadataObject;
			card.setCreated((Date) md.get("created"));
			card.setCreator((String) md.get("creator"));
			card.setModified((Date) md.get("modified"));
			card.setModifiedby((String) md.get("modifiedby"));
			card.setPhase((String) md.get("phase"));

			Object deleted = md.get("deleted");
			if (deleted != null && ((Boolean) deleted == true)) {
				card.setDeleted(true);
			} else {
				card.setDeleted(false);
			}

		}

		Map<String, Object> fields = new HashMap<String, Object>();
		for (String key : o.keySet()) {
			if (key.equals("_id") || key.equals("metadata")) {
				continue;
			}
			Object value = o.get(key);
			if (value instanceof BasicDBList) {
				BasicDBList list = (BasicDBList) value;
				fields.put(key, list);
			} else {
				fields.put(key, value);
			}
		}
		card.setFields(fields);
		return card;
	}

	public static DBObject cardToMongoDBObject(Card card) {

		BasicDBObject o = new BasicDBObject();

		o.append("_id", card.getId());

		for (Entry<String, Object> field : card.getFields().entrySet()) {
			// Fields that contain . are transient view fields like
			// capabilities.target_quarter etc. these need not be saved
			if (!field.getKey().contains("."))
				o.append(field.getKey(), field.getValue());
		}

		BasicDBObject metadata = new BasicDBObject();
		metadata.append("created", card.getCreated());
		metadata.append("creator", card.getCreator());
		metadata.append("modified", card.getModified());
		metadata.append("modifiedby", card.getModifiedby());
		metadata.append("phase", card.getPhase());
		metadata.append("deleted", card.isDeleted());
		//metadata.append("alerted", card.isAlerted());
		o.put("metadata", metadata);

		return o;
	}

	public static List<Map<String, Object>> cardsToList(Collection<Card> cards) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for (Card card : cards) {
			result.add(cardToMap(card));
		}
		return result;
	}

	public static Map<String, CardEventField> getCardFieldChanges(Map<String, Object> before, Map<String, Object> updated) {
		Map<String, CardEventField> fieldMap = new HashMap<String, CardEventField>();
		for (String key : updated.keySet()) {
			Object beforeValue = before.get(key);

			Object updatedValue = updated.get(key);
			/*
			 * If Before Value and Updated Value both are NULL skip update If new value is
			 * Set to NULL explicitly allow Update to Happen
			 */
			if ((beforeValue == null && updatedValue != null) || (beforeValue != null && updatedValue == null)
					|| (updatedValue != null && !updatedValue.equals(beforeValue))) {
				fieldMap.put(key, new CardEventField(key, before.get(key), updated.get(key)));
			}
		}
		return fieldMap;
	}

	/*
	public Map<String, Object> applyViewToCard(Card card, View view) throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();

		if (view == null) {
			logger.warn("No View Object supplied to applyViewToCard: " + card.getId());
			return result;
		}

		Map<String, ViewField> fields = view.getFields();

		if (fields == null) {
			logger.warn("No Fields in View Object supplied to applyViewToCard: " + card.getId());
			return result;
		}
		for (ViewField viewField : fields.values()) {
			try {
				Object value = null;
				String field = viewField.getName();

				if (field == null) {
					logger.info("Field Name Not Found: " + viewField.getId());
					continue;
				}

				if (viewField.getChildboard() != null) {

					// Find Children
					Map<String, String> searchParameters = new HashMap<String, String>();
					searchParameters.put(viewField.getChildfield(), card.getId());

					// Create new filter from parameters
					Map<String, Condition> conditions = new HashMap<String,Condition>();
					conditions.put("A", new Condition(viewField.getChildboard(),Operation.EQUALTO,card.getId()));
					Filter filter = new Filter();
					filter.setConditions(conditions);
					
					Collection<Card> cards = listTools.dynamicQuery(viewField.getChildboard(), null, filter, null, true );
					
					if (logger.isDebugEnabled()) {
						logger.debug("ChildBoard:" + viewField.getChildboard());
						if (cards != null) {
							logger.info("Card: " + card.getId() + " :Childboard:" + viewField.getChildboard()
									+ "Number of children:" + cards.size());
						} else {
							logger.info("Card: " + card.getId() + " :Childboard:" + viewField.getChildboard()
									+ " No children found: ");
						}
					}

					List<String> idList = new ArrayList<String>();
					for (Card childCard : cards) {
						idList.add(childCard.getId());
					}
					value = idList;

				} else {
					value = card.getField(field);
				}
				if (value != null) {
					if (logger.isDebugEnabled()) {
						logger.debug("Value found: " + viewField.getId() + ":" + value);
					}
					result.put(viewField.getId(), value);
				}

			} catch (Exception e) {
				// logger.warn("Getting Field, Path Not Found: ", e);
			}
		}

		return result;
	}
	*/



	/*
	 * Number of cards By Phase for a Board
	 */
	
	/*
	public Long getCardCountByPhase(String boardId, String phaseId) {
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.put("metadata.phase", phaseId);
		return this.mongoTemplate.getDb().getCollection(boardId).getCount(basicDBObject);
	}
	*/

	/*
	public List<DBObject> getCardsByPhase(String boardId, String phaseId) {
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.put("metadata.phase", phaseId);
		return this.mongoTemplate.getDb().getCollection(boardId).find(basicDBObject).toArray();
	}
	*/

	/*
	public int moveCardsPhase(String boardId, String fromPhase, String toPhase, List<String> c) {
		Query query = new Query(Criteria.where("metadata.phase").is(fromPhase).and("_id").in(c));
		Update update = new Update();
		update.set("metadata.phase", toPhase);
		update.set("metadata.modifiedby", SecurityToolImpl.getCurrentUser());
		update.set("metadata.modified", new Date());
		WriteResult updateResult = this.mongoTemplate.updateMulti(query, update, boardId);
		return updateResult.getN();
	}
	*/
		
	
	public static boolean validateParameters(Board board, String viewId, Map<String, Object> body) {
		View view = board.getView(viewId);
		if (view == null) {
			logger.warn("Null View Specified in Card Update: ");
			return false;
		}

		for (String fieldName : body.keySet()) {
			if (fieldName.equals("phase")) {
				continue;
			}
			ViewField field = view.getField(fieldName);
			if (field == null) {
				logger.warn("Unauthorized Field in Card Update: " + fieldName);
				return false;
			}
		}
		return true;
	}
	

	/*
	public Map<String, Map<String, Boolean>> explain( String boardId, String cardId) throws Exception {
		return this.automationEngine.explain(boardId, cardId);
	}
	*/
	
	/*
	public String resolveReferenceField(Board board, String fieldName, String value) {

		if (StringUtils.isBlank(value) || !board.isReferenceField(fieldName)) {
			logger.info("resolveReferenceField " + board.getName() + ":" + fieldName + "value" + value);
			return "";
		}

		TemplateField field = board.getField(fieldName);
		Card referenceCard;
		try {
			referenceCard = getCard(field.getOptionlist(), value);
			if (referenceCard != null) {
				return (String) referenceCard.getField("name");
			}
		} catch (Exception e) {
			logger.error("Failed to resolve the reference for fieldName " + fieldName + " on board "
					+ board.getName() + " for value " + value, e);
		}
		return "";
	}
	*/
	
	public static void correctCardFieldTypes(Board board, Map<String, Object> values) throws Exception{
		for( String fieldName : values.keySet() ){
			Object value = correctFieldType( fieldName, values.get(fieldName), board);
			if( value!=null){
				values.put( fieldName, value);
			}
		}
	}
	
	public static Object correctFieldType(String fieldName, Object value, Board board) throws Exception {
		
		Map<String,Object> error=new HashMap<>();
		TemplateField templateField =new TemplateField();

		try{
			if( value==null){
				return null;
			}
		
			templateField= board.getField(fieldName);
			
			if( templateField==null){
				return value;
			}
			
			switch( templateField.getType()){
			case STRING:
				return fieldToString(value, templateField.getValidation());
			case NUMBER:
				return value.equals("")?0d:Double.valueOf(value.toString());
			case BOOLEAN:
				return value.equals("")?false:Boolean.parseBoolean(value.toString());
			case DATE:
				if( value instanceof String){
					return DateTools.getDateFromString( (String) value, templateField.getValidation());
				} else if( value instanceof Number){
					return new Date(((Number)value).longValue());
				} else if( value instanceof Date){
					return value;
				} else {
					throw new Exception("Value not String, Number or Date");
				}
			case LIST:
				return value;
			case MAP:
				return value;
			}
		} catch(Exception e) {
			error.put("Invalid Value Provided For Field:"+fieldName+" Not a valid "+templateField.getType().name(), value);
			throw new CardValidationException(error);
		}
		
		return value;
	}
	
	/**
	 * When converting a field to a String, such as a Number, we need to control the format
	 * 
	 * @param o
	 * @return
	 * @throws Exception 
	 */
	public static String fieldToString( Object o, String format) throws Exception {
		
		if( o instanceof String) {
			return (String) o;
		}
		
		if( o instanceof Number) {
			if( format==null) {
				return DF.format(o);
			} else {
				DecimalFormat df = new DecimalFormat(format);
				return df.format(o);
			}
		}
	
		throw new Exception( "Unsupported Type");
	}
	
	
}
