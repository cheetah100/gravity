/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.tools;

import java.text.DecimalFormat;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.managers.ClusterManager;
import nz.devcentre.gravity.model.AbstractBoardClass;
import nz.devcentre.gravity.model.AbstractConfigurationClass;
import nz.devcentre.gravity.model.AbstractNamedModelClass;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardSequence;
import nz.devcentre.gravity.repository.BoardSequenceRepository;

/**
 * This class is used to get unqiue identifiers for new entities.
 * 
 * @author peter
 */
@Service
public class IdentifierTools {
	
	private static DecimalFormat df = new DecimalFormat("000000");

	@Autowired
	private BoardSequenceRepository boardSequenceRepository;
	
	@Autowired
	private ClusterManager clusterManager;
	
	@Autowired
	private BoardsCache boardsCache;
	
	public String getNewId(String boardId, String field) throws Exception{
		Board board = this.boardsCache.getItem(boardId);
		return board.getPrefix() + df.format(this.clusterManager.getId(boardId, field));
	}
	
	public String getId(String boardId, String prefix, String format, String field) throws Exception{
		DecimalFormat ldf = new DecimalFormat(format);
		return prefix + ldf.format(this.clusterManager.getId( boardId, field));
	}
	
	/**
	 * This method takes a English name with spaces and capitalization and 
	 * returns a lower case trimmed string with spaces replaced with hypens. 
	 * 
	 * @param name - English Name or Label to convert
	 * @return - Identifier created from name
	 */
	public static String getIdFromName(String name){
		
		if(name==null){
			return null;
		}
		
		String s = name.toLowerCase().trim();
		StringBuilder b = new StringBuilder(); 
		
		for(int i = 0, n = s.length() ; i < n ; i++) { 
		    char c = s.charAt(i);
		    
		    if( isAlphaNumeric(c) ){
		    	b.append(c);
		    } else {
		    	b.append("_");
		    }
		}
		
		return b.toString();
	}
	
	public static boolean isAlphaNumeric( char c){	
		if( c>=48 && c<=57){
			return true;
		}

		if( c>=65 && c<=90){
			return true;
		}

		if( c>=97 && c<=122){
			return true;
		}

		return false;
	}
	
	public static void newVersion(AbstractConfigurationClass config){

		if( config.getVersion()==null) {
			UUID uuid = UUID.randomUUID();
			String version = uuid.toString();
			config.setVersion(version);
		}
        
        if( config.getId()==null) {
        	if(config.getTenantId()==null) {
        		config.setId(getIdFromName(config.getName()));
        	} else {
        		config.setId(config.getTenantId() + "_" + getIdFromName(config.getName()));
        	}
        }
        
        config.setUniqueid(config.getId() + "_" + config.getVersion());
	}	
	
	public static String getIdFromNamedModelClass(AbstractNamedModelClass dataObject){
		if( StringUtils.isNotEmpty(dataObject.getId())){
			return dataObject.getId();
		}
		return getIdFromName(dataObject.getName());
	}
	
	public static String getIdFromBoardClass(AbstractBoardClass dataObject){
		if( StringUtils.isNotEmpty(dataObject.getId())){
			return dataObject.getId();
		}
		return getIdFromName(dataObject.getName());
	}
	
	public synchronized Long getNextSequence(String boardId, String name) throws Exception{
		
		BoardSequence boardSequence = this.boardSequenceRepository.findByBoardidAndName(boardId, name);
		if( boardSequence==null){
			boardSequence = new BoardSequence();
			boardSequence.setBoardid(boardId);
			boardSequence.setName(name);
			boardSequence.setValue(1l);
		}
		Long returnValue = boardSequence.getValue();
		boardSequence.setValue(boardSequence.getValue() + 1l);
		this.boardSequenceRepository.save(boardSequence);	
		return returnValue;
		
		/*
		 * This implementation may be more efficient and threadsafe
		 * 
		listTools.ensureCollection("counters");
		DBCollection collection = mongoTemplate.getDb().getCollection("counters");
	    BasicDBObject find = new BasicDBObject();
	    find.put("_id", name);
	    BasicDBObject update = new BasicDBObject();
	    update.put("$inc", new BasicDBObject("seq", 1L));
	    //DBObject obj =  collection.findAndModify(find, update);
	    DBObject obj = collection.findAndModify(find, null, null, false, update, true, true, false, 0, TimeUnit.SECONDS);
	    return (Long)obj.get("seq");
	    */
	}
	
	public static String getIdFromPath( String path ){
		if(path==null) return "";
		String[] split = path.split("/");
		return split[split.length-1];
	}

	public static String getFromPath( String path, int position ){
		if(path==null) return "";
		String[] split = path.split("/");
		return split[position];
	}

	public static void newVersion(AbstractBoardClass config) {
		if( config.getVersion()==null) {
			UUID uuid = UUID.randomUUID();
			String version = uuid.toString();
			config.setVersion(version);
		}
        
        if( config.getId()==null) {
        	config.setId(getIdFromName(config.getName()));
        }
        
        config.setUniqueid(config.getId() + "_" + config.getVersion());
	}
	
}
