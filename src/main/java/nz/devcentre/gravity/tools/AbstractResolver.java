/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.tools;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractResolver<T extends Registerable<?>> implements Resolver<T> {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractResolver.class);

	Map<String,T> registered = new HashMap<String, T>();;
	
	@Override
	public void register(String name, T registerable) {
		this.registered.put(name, registerable);
		logger.info("Cache " + name + " Registered with " + this.getClass().getSimpleName());
	}

	@Override
	public T getRegistered(String name) {
		return registered.get(name);
	}
	
	@Override
	public Object[] getRegistrations() {
		return registered.keySet().toArray();
	}
	
}
