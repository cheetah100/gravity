/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.security;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Component;

import com.google.common.primitives.Bytes;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.config.ContextProvider;
import nz.devcentre.gravity.controllers.Cache;
import nz.devcentre.gravity.controllers.CredentialCache;
import nz.devcentre.gravity.controllers.TeamCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.SecurityPermissionEntity;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.repository.TeamRepository;
import nz.devcentre.gravity.repository.UserRepository;
import nz.devcentre.gravity.tools.IdentifierTools;

@Component
public class SecurityTool {
	
	private static final Logger logger = LoggerFactory.getLogger(SecurityTool.class);
	
	private static byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	
	private static final String TEAM = "TEAM";
	
	public static final String SYSTEM = "system"; 
	
	public static final Random rnd = new Random();
	
	@Autowired
	TeamCache teamCache;
	
	@Autowired
	CredentialCache credentialCache;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	CacheInvalidationInterface cacheInvalidationManager;

	@Value("${keystore:keystore.jks}")
	private String sysKeystore;

	@Value("${keystorePassword:password}")
	private String sysKeystorePass;
	
	private SecretKey key = null;
	
	public String getKeyStorePath() {
		return this.getSysKeystore();
	}
	
	public String encrypt(String plaintext ) throws Exception {
		if(plaintext==null) return null;
		SecretKey key = getSystemKey();
		logger.info("Algorithm: {}", key.getAlgorithm());
		Cipher cipher = Cipher.getInstance( key.getAlgorithm() + "/CBC/PKCS5Padding" );
		cipher.init( Cipher.ENCRYPT_MODE, key, new IvParameterSpec( iv ) );
		byte[] encrypted = cipher.doFinal( plaintext.getBytes() );
		return new String(Base64.encode(encrypted));
	}

	public String decrypt(String ciphertext ) throws Exception {
		if(ciphertext==null) return null;
		SecretKey key = getSystemKey();
		byte[] encrypted = Base64.decode(ciphertext.getBytes());
		Cipher cipher = Cipher.getInstance( key.getAlgorithm() + "/CBC/PKCS5Padding" );
		cipher.init( Cipher.DECRYPT_MODE, key, new IvParameterSpec( iv ) );
		return new String(cipher.doFinal( encrypted ));
	}
	
	public String hash(String text) throws Exception {
		SecretKey key = getSystemKey();
		byte[] salt = key.getEncoded();
		byte[] toDisgest = Bytes.concat(text.getBytes(), salt);
		
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(toDisgest)));
	}
	
	/**
	 * This method will not only return a credential, but will decrypt the protected fields.
	 * USE WITH CAUTION - Do not return this data to the user.
	 * 
	 * @param credId
	 * @return
	 * @throws Exception
	 */
	public Credential getSecureCredential(String credId) throws Exception {
		Credential cred = credentialCache.getItem(credId);
		
		// Could use clone?
		Credential actual = (Credential) cred.clone();
		actual.setIdentifier(decrypt(cred.getIdentifier()));
		actual.setSecret(decrypt(cred.getSecret()));
		
		return actual;
	}
	
	/**
	 * JKS type keystores cannot store the SecretKey aka Symmetric keys.
	 * We need to use the JCEKS type keystores for this funtionality.
	 * Change the "javax.net.ssl.keyStore" system property setting above as
	 * appropriate if passing JCEKS keystore separately.
	 * 
	 */
	protected SecretKey getSystemKey() throws Exception {
		synchronized(this) {
			if (this.key != null) return this.key;
			try(FileInputStream inputStream = new FileInputStream(new File(getSysKeystore()))) {
				KeyStore ks = KeyStore.getInstance("JCEKS"); //KeyStore.getDefaultType() does not work
				ks.load(inputStream, sysKeystorePass.toCharArray());
				this.key = (SecretKey) ks.getKey("gravitykey", sysKeystorePass.toCharArray());
			}
			catch (Exception e) {
				logger.error("Exception getting system key", e);
				throw e;
			}
		}
		return this.key;
	}
	
	
	public static String getCurrentUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();		
		String username = SYSTEM;
		if(authentication!=null){
			Object principal = authentication.getPrincipal();
			if( principal instanceof DefaultOidcUser) {
				username = IdentifierTools.getIdFromName(((DefaultOidcUser) authentication.getPrincipal()).getEmail());
			} else {
				username = principal.toString();
			}
		}
		return username;
	}
	
	public void iAmSystem() throws Exception {
		
		SecurityContext context = SecurityContextHolder.getContext();
		Collection<? extends GrantedAuthority> authorities = this.getPermissions(SYSTEM);
		
		UsernamePasswordAuthenticationToken authentication = 
			new UsernamePasswordAuthenticationToken(SYSTEM, "", authorities);
		
		context.setAuthentication(authentication);
		
	}
	
	public void iAm( String user ) throws Exception {
		
		SecurityContext context = SecurityContextHolder.getContext();
		Collection<? extends GrantedAuthority> authorities = this.getPermissions(user);
		
		UsernamePasswordAuthenticationToken authentication = 
			new UsernamePasswordAuthenticationToken(user, "", authorities);
		
		context.setAuthentication(authentication);
		
	}
	
	public boolean isAuthorised( Map<String,String> permissions, String filter){
		
		if( permissions==null ){
			logger.warn("Role Permissions Not Found for Security");
			return false;
		}
		
		SecurityContext context = SecurityContextHolder.getContext();
		
		if(context==null || context.getAuthentication()==null){
			logger.warn("No Available Context for Security");
			return false;
		}
		
		context.getAuthentication().getAuthorities();
		List<String> methodPermissions = new ArrayList<String>(Arrays.asList(filter.toString().split(",")));
		
		try {
			return isAuthorised(context.getAuthentication(), permissions, methodPermissions);
		} catch (Exception e) {
			return false;
		}
		 
	}
	
	public List<GrantedAuthority> getPermissions(String userName) throws Exception {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Map<String, String> listTeams = teamCache.list();
		//get users of team and then compare to find out teams where user belongs.
		Set<String> keySet = listTeams.keySet();
		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String teamId = iterator.next();
			Team team = teamCache.getItem(teamId);
			if(team!=null){				
				Map<String, String> members = team.getMembers();
				if(members !=null){
					for( Entry<String,String> permission : members.entrySet() ) {
						if( permission.getKey().equals(userName)){
							String role = permission.getValue();
							if( logger.isDebugEnabled()) logger.debug("Permission for {} is {}.{} ", userName, teamId, role);
							GrantedAuthority grantedAuthority = new TeamAuthority(teamId, role);
							authorities.add(grantedAuthority);
						}
					}
				}
			} else {
				logger.warn("Team Not Found : " + teamId);
			}
		}
		return authorities;
	}
	
	public Map<String,String> initPermissions(Map<String,String> permissions ){

		if( permissions==null){
			permissions = new HashMap<String,String>();
		}
		
		if( permissions.get("administrators")==null){
			permissions.put("administrators", "ADMIN");
		}
		
		if( permissions.get("system")==null){
			permissions.put("system", "WRITE");
		}
		
		String username = getCurrentUser();
		if( permissions.get(username)==null){
			permissions.put(username, "ADMIN");
		}
		
		return permissions;
	}

	public static Authentication getCurrentAuthentication() {
		SecurityContext context = SecurityContextHolder.getContext();
		return context.getAuthentication();		
	}
	
	
	public static boolean isAuthorised( Authentication authentication, Map<String,String> permissions, List<String> filter) throws Exception{
		
		if(authentication==null){
			logger.warn("No Authentication");
			return false;
		}
		
		String username;
		
		Object principal = authentication.getPrincipal();
		if( principal instanceof DefaultOidcUser) {
			DefaultOidcUser oidcUser = (DefaultOidcUser) authentication.getPrincipal();
			username = IdentifierTools.getIdFromName(oidcUser.getEmail());
		} else if ( principal instanceof String) {
			username = (String) principal;
		} else {
			logger.warn("No Valid Principal");
			return false;
		}
		
		logger.debug("Username: " + username);
		
		List<String> usersTeams = getTeams(username);
		logger.debug("user teams :" + usersTeams.toString());
		logger.debug("permissions :" + permissions.toString());
				
		for( Entry<String,String> entry : permissions.entrySet()){
			if(filter==null || filter.contains(entry.getValue())){
				if(username.equals(entry.getKey())){
					if( logger.isDebugEnabled()) logger.debug("Access Authorized based on user " + username);
					return true;
				}
				
				String team = entry.getKey();
				
				for( String userTeam : usersTeams ) {
					if(userTeam.equals(team)){
						if( logger.isDebugEnabled()) logger.debug("Access Authorized for " + username + " based on team " + team);
						return true;
					}					
				}
			}
		}
		logger.warn("Unauthorized: " + username);
		return false;
	}
	
	private static List<String> getTeams( String username ) throws Exception{
		
		CacheManager cacheManager = (CacheManager) ContextProvider.getBean("cacheManager");
		TeamCache teamCache = (TeamCache) cacheManager.getRegistered("TEAM");	
		
		return teamCache.list().keySet().stream()
			.filter(teamId -> {
				try {
					return teamCache.getItem(teamId).getPermissions().keySet().contains(username);
				} catch (Exception e) {
					logger.warn("Exception getting teams ", e);
					return false;	
				}
			})
			.collect(Collectors.toList());
	}
	
	public boolean isAuthorizedViewAndFilter( String viewId, String filterId, Board board, String types ){
		
		// First see if user has board level access
		Map<String, String> rootPermissions = board.getRootPermissions();
		boolean authorised = isAuthorised(rootPermissions, types);
		if(authorised) return true;
		
		// Is there view access?
		boolean viewAuthorised = false;
		if(viewId!=null && board.getViews()!=null){
			View view = board.getViews().get(viewId);
			if(view!=null){
				viewAuthorised = isAuthorised(view.getPermissions(), types);
				if(!viewAuthorised) return false;
			}
		}
		
		// Is there filter access?
		boolean filterAuthorised = false;
		if(filterId!=null && board.getFilters()!=null){
			Filter filter = board.getFilters().get(filterId);
			if(filter!=null){
				filterAuthorised = isAuthorised(filter.getPermissions(), types);
				if(!filterAuthorised) return false;
			}
		}
		
		// You need access to at least one
		return filterAuthorised || viewAuthorised;
	}
	
	public User getUser(String userId) throws Exception {
		if (StringUtils.isEmpty(userId)) {
			throw new ValidationException("Invalid or null userId, id: " + Encode.forHtmlContent(userId));
		}

		User user = userRepository.findById(userId);
		if (user == null) {
			user = userRepository.findByName(userId);
			if (user == null) {
				user = userRepository.findByEmail(userId);
				if (user == null) {
					throw new ResourceNotFoundException();	
				}
			}
		}

		user.setPasswordhash(null);
		user.setTeams(this.getUserTeams(userId));
		return user;
	}
	
	public boolean userExists(String userId ) {
		if (StringUtils.isEmpty(userId)) {
			return false;
		}
		User user = userRepository.findById(userId);
		if (user == null) {
			user = userRepository.findByName(userId);
			if (user == null) {
				user = userRepository.findByEmail(userId);
				if (user == null) {
					return false;	
				}
			}
		}
		return true;
	}
	
	public String resetPassword( String userId ) throws Exception {
		User user = getUser(userId);
		String token = TokenTools.createToken(user);
		user.setResetToken(token);
		userRepository.save(user);
		return token;
	}
	
	public void setPassword( String userId, String newPassword, String token ) throws Exception {
		User user = getUser(userId);
		
		if( user==null) {
			logger.warn("User Not Found: " + userId);
			throw new Exception("User Not Found");
		}
		
		if( user.getResetToken()==null || !user.getResetToken().equals(token)) {
			logger.info("Invalid Token: "+ token + " UserToken:" + user.getResetToken());
			throw new Exception("Invalid Token");
		}
		
		if( !TokenTools.validateToken(token, user)) {
			logger.info("Invalid Token: "+ token );
			throw new Exception("Invalid Token");
		}
		
		user.setPasswordhash(hash(newPassword));
		user.setResetToken(null);
		userRepository.save(user);
	}
	
	public User login(String userId, String password) throws Exception {
		
		if (StringUtils.isEmpty(userId)) {
			throw new ValidationException("Invalid or null userId, id: " + Encode.forHtmlContent(userId));
		}

		User user = userRepository.findById(userId);
		if (user == null) {
			user = userRepository.findByName(userId);
			if (user == null) {
				user = userRepository.findByEmail(userId);
				if (user == null) {
					throw new ResourceNotFoundException();	
				}
			}
		}
		
		String newHash = hash(password);
		
		if(!user.getPasswordhash().equals(newHash)) {
			throw new AccessDeniedException("Wrong username or password");
		}
		
		user.setPasswordhash(null);
		return user;
	}
	
	public String signUp( User user ) throws Exception {
		String token = TokenTools.createToken(user);
		user.setPasswordhash(hash(user.getKey()));
		user.setKey(null);
		user.setResetToken(token);
		this.userRepository.save(user);
		return token;
	}
	
	public void validate( String userId, String token ) throws Exception {
		User user = getUser(userId);
		
		if( user.getResetToken()!=null && user.getResetToken().equals(token)){
			user.setResetToken(null);
			user.setEmailVerified(true);
			this.userRepository.save(user);
		} else {
			throw new Exception("Invalid Token");
		}
	}
	
	public Map<String, String> getUserTeams(String userId) throws Exception {
		Map<String, String> userTeams = new HashMap<String, String>();
		List<GrantedAuthority> authorities = getPermissions(userId);
		for (GrantedAuthority authority : authorities) {
			if (authority instanceof TeamAuthority) {
				TeamAuthority teamAuthority = (TeamAuthority) authority;
				userTeams.put(teamAuthority.getTeam(), teamAuthority.getRole());
			}
		}
		return userTeams;
	}
	
	public void addUserToTeams(String userId, Map<String, String> newTeams) {
		if (newTeams == null) {
			return;
		}

		Set<String> keySetNew = newTeams.keySet();
		for (Iterator<String> iterator = keySetNew.iterator(); iterator.hasNext();) {
			String teamId = iterator.next();
			String teamRole = newTeams.get(teamId);
			Map<String, String> roles = new HashMap<String, String>();
			roles.put(userId, teamRole);
			try {
				addMembers(teamId, roles);
			} catch (Exception e) {
				logger.warn("Adding users to teams exception", e);
			}
		}
	}
	
	public void deleteMember(String teamId, String member) throws Exception {
		Team team = teamCache.getItem(teamId);
		Map<String,String> existing = team.getMembers();
		if (existing!=null)
			existing.remove(member);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TEAM, teamId);
	}
	
	public void addMembers(String teamId, Map<String,String> members) throws Exception {
		Team team = teamCache.getItem(teamId);
		Map<String,String> existing = team.getMembers();
		
		if( existing==null){
			existing = new HashMap<String,String>();
			team.setMembers(existing);
		}
		
		for( Entry<String,String> entry : members.entrySet()){
			existing.put(entry.getKey(), entry.getValue());
		}
		
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TEAM, teamId);
	}

	public String getSysKeystore() {
		return sysKeystore;
	}

	public void setSysKeystore(String sysKeystore) {
		this.sysKeystore = sysKeystore;
	}
	
	public Map<String,String> filterMap(Map<String,String> map, String types, Cache<?> cache) throws Exception{
		return map.entrySet().stream()
			.filter( item -> {
				try {
					Object o = cache.getItem(item.getKey());
					if( o instanceof SecurityPermissionEntity) {
						Map<String, String> permissions = ((SecurityPermissionEntity) o).getPermissions();
						return isAuthorised(permissions, types);						
					}
				} catch (Exception e) {
					logger.warn("FilterMap Couldn't find " + item.getKey());
				}
				return false;
			})
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
}
