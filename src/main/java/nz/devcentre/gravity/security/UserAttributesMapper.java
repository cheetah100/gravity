/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.model.User;

@Component
public class UserAttributesMapper implements AttributesMapper<User> {
	
	@Override
	public User mapFromAttributes(Attributes attrs) throws NamingException {
		User user = new User();
		if((attrs.get("cn")!= null))user.setId((String)attrs.get("cn").get());
		if((attrs.get("name")!= null))user.setName((String)attrs.get("name").get());
		if((attrs.get("givenName")!= null))user.setFirstname((String)attrs.get("givenName").get());
		if((attrs.get("sn")!= null))user.setSurname((String)attrs.get("sn").get());
		if((attrs.get("mail")!= null))user.setEmail((String)attrs.get("mail").get());	
		return user;
	}
}
