/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import nz.devcentre.gravity.config.ContextProvider;
import nz.devcentre.gravity.controllers.Cache;
import nz.devcentre.gravity.controllers.TeamCache;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.SecurityPermissionEntity;

public class GravityPermissionEvaluator implements PermissionEvaluator {
	
	private static final Logger logger = LoggerFactory.getLogger(GravityPermissionEvaluator.class);

	CacheManager cacheManager;
	
	TeamCache teamCache;
	
	@Override
	public boolean hasPermission(Authentication authentication,
								 Object targetDomainObject, 
								 Object permission) {
				
		logger.warn("Has Permission with Wrong Method - Gravity");
		
		return false;
	}

	@Override
	public boolean hasPermission(Authentication authentication,
								 Serializable targetId, 
								 String targetType, 
								 Object permission) {
				
		if( logger.isDebugEnabled()){
			logger.debug( "HASPERMISSION - targetId:" + targetId + " targetType:" + targetType + " permission:" + permission );
		}
		
		try {
			Map<String,String> rolePermissions = getPermissionsById(targetType, targetId.toString());
			
			if(rolePermissions==null){
				logger.warn("No Permissions Found for " + authentication.getName());
				return false;
			}
			
			List<String> methodPermissions = new ArrayList<String>(Arrays.asList(permission.toString().split(",")));
			
			if(logger.isDebugEnabled()){
				logger.debug("permissions: " + methodPermissions);
			}
			
			return SecurityTool.isAuthorised(authentication, rolePermissions, methodPermissions);			
			
		} catch (Exception e){
			logger.warn("Exception running auth: " + e.getClass().getSimpleName() + " " + e.getMessage());
			return false;
		}
	}
	
	protected List<String> getTeams( String username ) throws Exception{
		
		if(teamCache==null) {
			if (cacheManager== null) {
				cacheManager = (CacheManager) ContextProvider.getBean("cacheManager");
			}
			teamCache = (TeamCache) this.cacheManager.getRegistered("TEAM");	
		}
		
		return teamCache.list().keySet().stream()
			.filter(teamId -> {
				try {
					return teamCache.getItem(teamId).getPermissions().keySet().equals(username);
				} catch (Exception e) {
					return false;	
				}
			})
			.collect(Collectors.toList());
	}
	
	
	protected Map<String, String> getPermissionsById(String cacheName, String id) throws Exception {
		if (cacheManager== null) {
			cacheManager = (CacheManager) ContextProvider.getBean("cacheManager");
		}
		
		Cache<?> cache = cacheManager.getRegistered(cacheName);
		if( cache==null) {
			throw new Exception("Cache Not Found:" + cacheName);
		}
		
		Object item = cache.getItem(id);
		
		if( item==null || !(item instanceof SecurityPermissionEntity)){
			return null;
		}
		
		SecurityPermissionEntity securityRollEntity = (SecurityPermissionEntity) item;
		return securityRollEntity.getPermissions();
	}
	
}