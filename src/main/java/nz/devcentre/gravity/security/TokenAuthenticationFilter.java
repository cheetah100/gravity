/**
 * GRAVITY WORKFLOW AUTOMATION
 * http://www.intellitech.pro/tutorial-1-spring-security-authentication-using-token/
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import nz.devcentre.gravity.model.User;

/**
 * 
 * Example of Token Authentication
 * http://www.intellitech.pro/tutorial-1-spring-security-authentication-using-token/
 * 
 * @author peter
 *
 */
public class TokenAuthenticationFilter extends GenericFilterBean {

	private SecurityTool securityTool;
	
	private String authTokenHeaderName = "x-auth-token";
 
	public TokenAuthenticationFilter( SecurityTool tool ) {
		this.securityTool = tool;
	}
 
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
			String authToken = httpServletRequest.getHeader(authTokenHeaderName);
 
			if (StringUtils.hasText(authToken)) {
				
				if(logger.isDebugEnabled()) {
					logger.debug("Auth:" + authToken);
				}
				
				String username = TokenTools.getUserNameFromToken(authToken);
				User user = this.securityTool.getUser(username);
				if (TokenTools.validateToken(authToken, user)) {
					List<GrantedAuthority> permissions = this.securityTool.getPermissions(user.getId());
					UsernamePasswordAuthenticationToken token = 
							new UsernamePasswordAuthenticationToken(user.getName(),
							user.getPasswordhash(), permissions);
					SecurityContextHolder.getContext().setAuthentication(token);
				}
			}
 
			filterChain.doFilter(servletRequest, servletResponse);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

}
