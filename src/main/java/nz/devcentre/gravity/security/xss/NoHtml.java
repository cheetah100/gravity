package nz.devcentre.gravity.security.xss;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CssValidator.class)
@Documented
public @interface NoHtml {
    String message() default "{nz.devcentre.gravity.security.xss.nohtml}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
