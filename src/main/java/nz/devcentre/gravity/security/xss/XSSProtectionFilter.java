/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.security.xss;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;

public class XSSProtectionFilter extends OncePerRequestFilter {
	
	private static final Logger logger = LoggerFactory.getLogger(XSSProtectionFilter.class);	
	
	ArrayList<String> contentTypes = null;	
	
	/**
	 * Doing minimalistic check for javascripts. Ideally, Encode.forHtmlContent should be used to check for any HTML
	 * but it will also suppress any usage of < and > characters disallowing expressions like startDate < endDate etc.
	 * 
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		if(contentTypes==null) init();
		BufferedRequestWrapper requestWrapper = new BufferedRequestWrapper(request);
		
		String value = null;
		Enumeration<String> names = requestWrapper.getParameterNames();
		while (names.hasMoreElements()) {
			String name = names.nextElement();
			value = requestWrapper.getParameter(name);
			if (!isValid(value)) {
				logger.warn("XSS: Parameter: "+ name + "value: " +  value + " rejected as HTML/Javascript content");
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "XSS: HTML/Javascript not allowed.");
				return;
			}
		}
		if (contentTypes.contains(requestWrapper.getContentType())) {
			value = IOUtils.toString(requestWrapper.getInputStream());
			if (!this.isValid(value)) {
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "XSS: HTML/Javascript not allowed.");
				return;
			}

		} else {
			if( logger.isDebugEnabled()) {
				logger.debug("Unrecognized content type " + request.getContentType());
			}
		}
		filterChain.doFilter(requestWrapper, response); 
	}
	
	protected boolean isValid(String value) {
		value = value.replaceAll("\n", "");
		value = value.replaceAll("\r", "");
		value = value.replaceAll("\t", "");
		value = value.toLowerCase();
		
		String script = "script";
		String javascript = "javascript" ;
		String languageOrType =  "(language|type)";

		boolean scriptTag = Pattern.matches(".*< *" + script + " *(" + languageOrType + " *= *['\"].*['\"])* *>.*", value);
		boolean javascriptAttr = Pattern.matches(".*= *['\"] *" + javascript + " *: *\\{.*", value);
		boolean valid = !(scriptTag || javascriptAttr);
		if (!valid) {
			logger.warn("XSSProtectionFilter: Value " + value + " rejected as HTML/Javascript content");
		}		
		return valid;
	}
	
	protected void init() {
		this.contentTypes = new ArrayList<String> ();
		this.contentTypes.add(MediaType.APPLICATION_JSON.toString());
		this.contentTypes.add(MediaType.TEXT_HTML.toString());
		this.contentTypes.add(MediaType.TEXT_PLAIN.toString());
	}
}

class BufferedRequestWrapper extends HttpServletRequestWrapper {

	private static final Logger logger = LoggerFactory.getLogger(BufferedRequestWrapper.class);
    private ByteArrayInputStream bais = null;
    private ByteArrayOutputStream baos = null;
    private BufferedServletInputStream bsis = null;
    private byte[] buffer = null;


    public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
        super(req);
        // Read InputStream and store its content in a buffer.
        InputStream is = req.getInputStream();
        this.baos = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int letti;
        while ((letti = is.read(buf)) > 0) {
            this.baos.write(buf, 0, letti);
        }
        this.buffer = this.baos.toByteArray();
    }

    
    @Override
    public ServletInputStream getInputStream() {
        this.bais = new ByteArrayInputStream(this.buffer);
        this.bsis = new BufferedServletInputStream(this.bais);
        return this.bsis;
    }

    

    String getRequestBody() throws IOException  {
        BufferedReader reader = new BufferedReader(new InputStreamReader(this.getInputStream()));
        StringBuilder inputBuffer = new StringBuilder();
        try {
	        String line = null;
	        do {
	        	line = reader.readLine();
	        	if (null != line) {
	        		inputBuffer.append(line.trim());
	        	}
	        } while (line != null);
        } catch (Exception e) {
        	logger.error("Exception while reading request.." + e);
        	throw e;
        }
        finally {
            reader.close();
        }
        return inputBuffer.toString().trim();
    }
}


class BufferedServletInputStream extends ServletInputStream {

    private ByteArrayInputStream bais;

    public BufferedServletInputStream(ByteArrayInputStream bais) {
        this.bais = bais;
    }

    @Override
    public int available() {
        return this.bais.available();
    }

    @Override
    public int read() {
        return this.bais.read();
    }

    @Override
    public int read(byte[] buf, int off, int len) {
        return this.bais.read(buf, off, len);
    }

	@Override
	public boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isReady() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setReadListener(ReadListener listener) {
		// TODO Auto-generated method stub
		
	}


}

class TeeServletOutputStream extends ServletOutputStream {

	private final TeeOutputStream targetStream;

	public TeeServletOutputStream( OutputStream one, OutputStream two ) {
		targetStream = new TeeOutputStream( one, two);
	}
	
	@Override
	public void write(int arg0) throws IOException {
		this.targetStream.write(arg0);
	}
	
	public void flush() throws IOException {
		super.flush();
		this.targetStream.flush();
	}

	public void close() throws IOException {
		super.close();
		this.targetStream.close();
	}

	@Override
	public boolean isReady() {
		return false;
	}

	@Override
	public void setWriteListener(WriteListener listener) {
		// TODO Auto-generated method stub
		
	}		
}

class BufferedResponseWrapper implements HttpServletResponse {

	HttpServletResponse original;
	TeeServletOutputStream teeStream;
	PrintWriter teeWriter;
	ByteArrayOutputStream bos;
	
	// ** Dummy Methods - Begin
	
	@Override
	public String getHeader(String name) {
		return original.getHeader(name);
	}
	
	@Override
	public Collection<String> getHeaderNames() {
		return original.getHeaderNames();
	}
	
	@Override
	public Collection<String> getHeaders(String name) {
		return original.getHeaders(name);
	}
	
	@Override
	public int getStatus() {
		return original.getStatus();
	}
	
	// ** Dummy Methods - End

	public BufferedResponseWrapper(HttpServletResponse response) {
		original = response;
	}

	public String getContent() throws IOException {
		return bos.toString();
		
	}
	
	@Override
	public PrintWriter getWriter() throws IOException {

	if (this.teeWriter == null) {
	this.teeWriter = new PrintWriter(new OutputStreamWriter(getOutputStream()));
	}
	return this.teeWriter;
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {

	if (teeStream == null) {
	bos = new ByteArrayOutputStream();
	teeStream = new TeeServletOutputStream(original.getOutputStream(), bos);
	}
	return teeStream;
	}

	@Override
	public String getCharacterEncoding() {
		return original.getCharacterEncoding();
	}

	@Override
	public String getContentType() {
		return original.getContentType();
	}

	@Override
	public void setCharacterEncoding(String charset) {
		original.setCharacterEncoding(charset);
	}

	@Override
	public void setContentLength(int len) {
		original.setContentLength(len);
	}

	@Override
	public void setContentType(String type) {
		original.setContentType(type);
	}

	@Override
	public void setBufferSize(int size) {
		original.setBufferSize(size);
	}

	@Override
	public int getBufferSize() {
		return original.getBufferSize();
	}

	@Override
	public void flushBuffer() throws IOException {
		if (teeStream != null) {
		teeStream.flush();
		System.err.println("teeStream flush");
		}
		if (this.teeWriter != null) {
		this.teeWriter.flush();
		System.err.println("teeWriter flush");
		}
		}

	@Override
	public void resetBuffer() {
		original.resetBuffer();
	}

	@Override
	public boolean isCommitted() {
		return original.isCommitted();
	}

	@Override
	public void reset() {
		original.reset();
	}

	@Override
	public void setLocale(Locale loc) {
		original.setLocale(loc);
	}

	@Override
	public Locale getLocale() {
		return original.getLocale();
	}

	@Override
	public void addCookie(Cookie cookie) {
		original.addCookie(cookie);
	}

	@Override
	public boolean containsHeader(String name) {
		return original.containsHeader(name);
	}

	@Override
	public String encodeURL(String url) {
		return original.encodeURL(url);
	}

	@Override
	public String encodeRedirectURL(String url) {
		return original.encodeRedirectURL(url);
	}

	@SuppressWarnings("deprecation")
	@Override
	public String encodeUrl(String url) {
		return original.encodeUrl(url);
	}

	@SuppressWarnings("deprecation")
	@Override
	public String encodeRedirectUrl(String url) {
		return original.encodeRedirectUrl(url);
	}

	@Override
	public void sendError(int sc, String msg) throws IOException {
		original.sendError(sc, msg);
	}

	@Override
	public void sendError(int sc) throws IOException {
		original.sendError(sc);
	}

	@Override
	public void sendRedirect(String location) throws IOException {
		original.sendRedirect(location);
	}

	@Override
	public void setDateHeader(String name, long date) {
		original.setDateHeader(name, date);
	}

	@Override
	public void addDateHeader(String name, long date) {
		original.addDateHeader(name, date);
	}

	@Override
	public void setHeader(String name, String value) {
		original.setHeader(name, value);
	}

	@Override
	public void addHeader(String name, String value) {
		original.addHeader(name, value);
	}

	@Override
	public void setIntHeader(String name, int value) {
		original.setIntHeader(name, value);
	}

	@Override
	public void addIntHeader(String name, int value) {
		original.addIntHeader(name, value);
	}

	@Override
	public void setStatus(int sc) {
		original.setStatus(sc);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void setStatus(int sc, String sm) {
		original.setStatus(sc, sm);
	}

	@Override
	public void setContentLengthLong(long length) {
		original.setContentLengthLong(length);
	}

}

