/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.security.xss;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cross Site Scripting Protection Validator 
 * @author rajdixit
 *
 */
public class CssValidator implements ConstraintValidator<NoHtml, String> {
 
   private static final Logger logger = LoggerFactory.getLogger(CssValidator.class);

   // http://owasp-java-html-sanitizer.googlecode.com/svn/trunk/distrib/javadoc/org/owasp/html/HtmlPolicyBuilder.html
   // builder is not thread safe, so make local
   private static final PolicyFactory DISALLOW_ALL = new HtmlPolicyBuilder().toFactory();
 
   @Override
   public void initialize(NoHtml constraintAnnotation)
   {
      // TODO specify the policy as an annotation attribute
      // to use them, values from annotation are stored in private properties here
   }
 
   @Override
   public boolean isValid(String value, ConstraintValidatorContext context)
   {
	   //String sanitized = DISALLOW_ALL.sanitize(value);
	   //String sanitized = Encode.forHtmlContent(value);
	   /* Ideally either of the above methods should be used but since we may want to allow < and > characters
	    * as valid inputs in cases where we may need to store expressions in the gravity we are using the below simpler
	    * yet reasonably sufficient check.
	    * 
	    * This is done to allow the script code in the resources controller to use the < and > in expressions like
	    * if startDate < endDate { ... }
	    * 
	    */

		value = value.replaceAll("\n", "");
		value = value.replaceAll("\r", "");
		value = value.replaceAll("\t", "");
		value = value.toLowerCase();
		
		String script = "script";
		String javascript = "javascript" ;
		String languageOrType =  "(language|type)";

		boolean scriptTag = Pattern.matches(".*< *" + script + " *(" + languageOrType + " *= *['\"].*['\"])* *>.*", value);
		boolean javascriptAttr = Pattern.matches(".*= *['\"] *" + javascript + " *: *\\{.*", value);
		boolean valid = !(scriptTag || javascriptAttr);
		if (!valid) {
			logger.warn("XSS: Value " + value + "rejected as HTML/Javascript content");
		}
		return valid;
	}
}
