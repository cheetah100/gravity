/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import org.springframework.security.core.GrantedAuthority;

public class TeamAuthority implements GrantedAuthority {

	private final String team;
	
	private final String role;
	
	public TeamAuthority(String team, String role) {
		this.team = team;
		this.role = role;
	}
	
	@Override
	public String getAuthority() {
		if(role==null){
			return team;
		}
		return team + "." + role;
	}
	
	@Override
	public int hashCode() {
		return getAuthority().hashCode();
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(!(arg0 instanceof TeamAuthority) ) return false;
		return getAuthority().equals(((TeamAuthority)arg0).getAuthority());
	}

	public String toString() {
		return getAuthority();
	}
	
	public String getTeam(){
		return team;
	}
	
	public String getRole(){
		return role;
	}

}
