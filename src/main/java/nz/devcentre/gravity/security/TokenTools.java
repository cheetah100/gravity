/**
 * GRAVITY WORKFLOW AUTOMATION
 * http://www.intellitech.pro/tutorial-1-spring-security-authentication-using-token/
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.codec.Hex;

import nz.devcentre.gravity.model.User;

public class TokenTools {
	
	public static final String MAGIC_KEY = "Flippy";
	 
	public static String createToken(User user) {
		long expires = System.currentTimeMillis() + 1000L * 60 * 60 * 8;
		return user.getName() + ":" + expires + ":" + computeSignature(user, expires);
	}
 
	public static String computeSignature(User user, long expires) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(user.getName()).append(":");
		signatureBuilder.append(expires).append(":");
		signatureBuilder.append(user.getPasswordhash()).append(":");
		signatureBuilder.append(MAGIC_KEY);
 
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}
 
	public static String getUserNameFromToken(String authToken) {
		if (authToken == null) {
			return null;
		}
		String[] parts = authToken.split(":");
		return parts[0];
	}
 
	public static boolean validateToken(String authToken, User user) {
		String[] parts = authToken.split(":");
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];
		String signatureToMatch = computeSignature(user, expires);
		return expires >= System.currentTimeMillis() && signature.equals(signatureToMatch);
	}

}
