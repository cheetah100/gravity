/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.ClusterManager;
import nz.devcentre.gravity.model.Application;
import nz.devcentre.gravity.model.ApplicationGroup;
import nz.devcentre.gravity.model.Module;
import nz.devcentre.gravity.model.Widget;
import nz.devcentre.gravity.model.transfer.ItemResult;
import nz.devcentre.gravity.repository.AppRepository;
import nz.devcentre.gravity.repository.WidgetRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/app")
@ApiModel(value="Application")

public class ApplicationController {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);
	
	private static final DecimalFormat df = new DecimalFormat("000000");
	
	private static final String MODULE_NOT_FOUND = "Module not found, moduleId: {}";
	
	private static final String APPGROUP_NOT_FOUND = "ApplicationGroup not found, id: {}";
	
	private static final String WIDGET_NOT_FOUND = "Widget not found, id: {}";

	@Autowired 
	private ApplicationCache applicationCache;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
		
	@Autowired
	private WidgetRepository widgetRepository;
	
	@Autowired
	private AppRepository appRepository;
	
	@Autowired
	private ClusterManager clusterManager;
	

	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}", method=RequestMethod.GET)
	@ApiOperation(value="Get Application")
	public @ResponseBody Application getApplication(@PathVariable String appId) throws Exception {
		return applicationCache.getItem(appId);		
	}	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/history", method=RequestMethod.GET)
	@ApiOperation(value="Get Application History")
	public @ResponseBody List<Application> getOldApplication(@PathVariable String appId) throws Exception {
		return appRepository.findById(appId);
	}	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}", method=RequestMethod.GET)
	@ApiOperation(value="Get Module")
	public @ResponseBody Module getModule(@PathVariable String appId, @PathVariable String moduleId) throws Exception {
		Application app = applicationCache.getItem(appId);
		if( app.getModules()!=null && app.getModules().containsKey(moduleId)){
			return  app.getModules().get(moduleId);
		} else {
			throw new ResourceNotFoundException(String.format(MODULE_NOT_FOUND,moduleId));
		}
	}

	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}/ui", method=RequestMethod.GET)
	@ApiOperation(value="Get Module UI")
	public @ResponseBody Map<String,Object> getModuleUI(@PathVariable String appId, @PathVariable String moduleId) throws Exception {
		Application app = applicationCache.getItem(appId);
		return app.getModules().get(moduleId).getUi();
	}

	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/modules", method=RequestMethod.GET)
	@ApiOperation(value="List Application Modules (id, description)")
	public @ResponseBody Map<String,String> listModules(@PathVariable String appId) throws Exception {
		Map<String,String> result = new HashMap<>();
		Application app = applicationCache.getItem(appId);
		Map<String, Module> modules = app.getModules();
		if (app.getModules()!=null) {
			for (String id: modules.keySet()) {
				Module module = modules.get(id);
				result.put(module.getId(), module.getDescription());
			}
		}
		return result;
	}
	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}/ui/{id}", method=RequestMethod.GET)
	@ApiOperation(value="Get Module UI Element")
	public @ResponseBody Object getModuleUI(@PathVariable String appId, 
			@PathVariable String moduleId, @PathVariable String id) throws Exception {

		Module module = getModule( appId, moduleId);
		Map<String, Object> ui = module.getUi();
		Object object = ui.get(id);
		return object;
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/groups", method=RequestMethod.GET)
	@ApiOperation(value="List groups")
	public @ResponseBody Map<String, ApplicationGroup> listgroups(@PathVariable String appId) throws Exception {
		Application app = applicationCache.getItem(appId);
		if( app.getGroups()!=null){
			return  app.getGroups();
		}
		return Collections.emptyMap();
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/groups/{groupId}", method=RequestMethod.GET)
	@ApiOperation(value="Get ApplicationGroup")
	public @ResponseBody ApplicationGroup getGroup(@PathVariable String appId, @PathVariable String groupId) throws Exception {
		Application app = applicationCache.getItem(appId);
		if( app.getGroups()!=null && app.getGroups().containsKey(groupId)){
			return app.getGroups().get(groupId);
		} else {
			throw new ResourceNotFoundException(String.format(APPGROUP_NOT_FOUND,groupId));
		}
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/groups", method=RequestMethod.POST)
	@ApiOperation(value="Create Groups")
	public @ResponseBody ApplicationGroup createGroup(@RequestBody ApplicationGroup group, @PathVariable String appId,  
			@RequestParam(required=false) boolean replace) throws Exception {
		
		Application app = applicationCache.getItem(appId);
		Map<String,ApplicationGroup> groups = app.getGroups();
		if (groups==null) {
			groups = new HashMap<String,ApplicationGroup>();
			app.setGroups(groups);
		}
		String newId = IdentifierTools.getIdFromNamedModelClass(group);
		group.setId(newId);
		group.addOrUpdate();
		groups.put(group.getId(), group);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		return group;
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/groups/{groupId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Group")
	public @ResponseBody void deleteGroup(@PathVariable String appId, @PathVariable String groupId) throws Exception {
		
		if(StringUtils.isEmpty(appId) || StringUtils.isEmpty(groupId)){
			throw new ResourceNotFoundException(String.format(APPGROUP_NOT_FOUND,groupId));
		}
		
		Application app = applicationCache.getItem(appId);
		app.getGroups().remove(groupId);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);		
	}

	protected ApplicationGroup copyApplicationGroup(ApplicationGroup from, ApplicationGroup to) {
		if (from.getBoards() != null && from.getBoards().size() > 0)
			to.setBoards(from.getBoards());
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		to.setOrder(from.getOrder());
		if (StringUtils.isNotEmpty(from.getParent()))
			to.setParent(from.getParent());
		return to;

	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	@ApiOperation(value="Create Application")
	public @ResponseBody Application createApplication(@RequestBody Application app) throws Exception {
		
		try{
			
			if(app.getId()!=null) {
				Application existing = appRepository.findByIdAndActiveTrue(app.getId());
				if( existing != null) {
					existing.setActive(false);
					existing.addOrUpdate();
					appRepository.save(existing);
				}
			}
			
			IdentifierTools.newVersion(app);
			// Ensure that the current user is assigned as the owner of the new app.
			// By default also add the administrators group as a owner.
			app.setPermissions(this.securityTool.initPermissions(app.getPermissions()));
			app.setActive(true);
			app.addOrUpdate();
			appRepository.save(app);
			this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, app.getId());
			
		} finally {
		}
		return app;
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/modules", method=RequestMethod.POST)
	@ApiOperation(value="Create Module")
	public @ResponseBody Module createModule(@RequestBody Module module, @PathVariable String appId) throws Exception {
		
		Application app = getApplication(appId);
		
		Map<String,Module> modules = app.getModules();
		if (modules == null) {
			modules = new HashMap<String,Module>();
			app.setModules(modules);
		}
		
		try{
			if (module.getId() == null) {
				module.setId(IdentifierTools.getIdFromNamedModelClass(module));
			}
			module.addOrUpdate();
			modules.put(module.getId(), module);
			appRepository.save(app);
			this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		} finally {
		}
		return module;
	}
	

	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}", method=RequestMethod.PUT)
	@ApiOperation(value="Update Application")
	public @ResponseBody Application updateApplication(@RequestBody Application app, @PathVariable String appId) throws Exception {
		
		Application existing = getApplication(appId);
		copyApplication(app, existing);

		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		return app;
	}
	
	protected void copyApplication(Application from, Application to) {
		if (StringUtils.isNotEmpty(from.getName()))
				to.setName(from.getName());
		if (from.getPermissions() != null)
			to.setPermissions(from.getPermissions());
		if (from.getModules() != null)
			to.setModules(from.getModules());
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}", method=RequestMethod.PUT)
	@ApiOperation(value="Update Module")
	public @ResponseBody Module updateModule(@RequestBody Module module, 
			@PathVariable String appId, @PathVariable String moduleId) throws Exception {
		
		Application app = getApplication(appId);
		Module mod = this.getModule(appId, moduleId);
		copyModule(module, mod);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		return module;
	}
	
	protected void copyModule(Module from, Module to) {
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		if (StringUtils.isNotEmpty(from.getDescription()))
			to.setDescription(from.getDescription());
		if (from.getPermissions() != null)
			to.setPermissions(from.getPermissions());
		if (from.getUi() != null)
			to.setUi(from.getUi());
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}/ui/{id}", method=RequestMethod.PUT)
	@ApiOperation(value="Update Module UI")
	public @ResponseBody List<Object> updateModuleUI(@RequestBody List<Object> list, 
			@PathVariable String appId, @PathVariable String moduleId, @PathVariable String id) throws Exception {
		
		Application app = getApplication(appId);
			
		Module module = app.getModules().get(moduleId);
						
		if(module!=null){
			Map<String, Object> ui = module.getUi();
			ui.put(id, list);
			appRepository.save(app);
			this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		} else {
			throw new ResourceNotFoundException(String.format(MODULE_NOT_FOUND,moduleId));
		}
		return list;
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}/ui/{id}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Module UI")
	public @ResponseBody void deleteModuleUI( @PathVariable String appId, @PathVariable String moduleId, @PathVariable String id) throws Exception {
		
		if(StringUtils.isEmpty(appId) || StringUtils.isEmpty(moduleId)){
			return;
		}
		
		Application app = getApplication(appId);
		Module module = this.getModule(appId, moduleId);
		Map<String, Object> ui = module.getUi();
		
		// Here for the UI element there is no concept of soft delete. On delete request the UI will be permanently removed
		logger.info("Deleting UI from the module, moduleId: " + moduleId + " " + "ui-id: " + id);
		ui.remove(id);
		
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Application")
	public @ResponseBody void deleteApplication(@PathVariable String appId) throws Exception {
		if( StringUtils.isEmpty(appId)){
			return;
		}
		Application app = getApplication(appId);
		app.setActive(false);
		app.addOrUpdate();
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/activate", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Application   activateApplication(
			@PathVariable String appId,
			@RequestBody String version) throws Exception {
		
		Application app = appRepository.findByIdAndVersion(appId, version);
		
		if(app!= null ) {   
			//Get the Current Active Version
			Application existing = appRepository.findByIdAndActiveTrue(appId);
			if(existing!=null) {
				//Set Active to False for The current one
				existing.setActive(false);
				existing.addOrUpdate();
				appRepository.save(existing);
			}
		
			app.addOrUpdate();
			app.setActive(true);
			appRepository.save(app);
			this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
			return app;

		}
		
		throw new ResourceNotFoundException();
	}	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/modules/{moduleId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Module")
	public @ResponseBody void deleteModule(@PathVariable String appId, @PathVariable String moduleId) throws Exception {
		
		if(StringUtils.isEmpty(appId) || StringUtils.isEmpty(moduleId)){
			return;
		}
		this.getModule(appId, moduleId);
		Application app = getApplication(appId);		
		app.getModules().remove(moduleId);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
	}
	
	public void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String appId = it.next();
			Map<String, String> permissions = this.applicationCache.getItem(appId).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}
	
	/**
	 * Same as filterMap, only with a different parameter interface.
	 * 
	 * @param map
	 * @param types
	 * @throws Exception
	 */
	public void filterObjectMap(Map<String,Object> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String appId = it.next();
			Map<String, String> permissions = this.applicationCache.getItem(appId).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}

	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/permissions", method=RequestMethod.POST)
	public @ResponseBody void addPermissions(@PathVariable String appId, 
			@RequestParam(required=false) boolean replace,
			@RequestBody Map<String,String> permissions) throws Exception {

			Application app = getApplication(appId);
		
			Map<String, String> curPermissions = app.getPermissions();
			if (curPermissions==null) {
				curPermissions = new HashMap<String,String>();
				app.setPermissions(curPermissions);
			}
			if (replace) {
				Iterator<String> properties = curPermissions.keySet().iterator();
				while (properties.hasNext()) {
					String nextProperty = properties.next();
			
					if( !permissions.containsKey(nextProperty) 
							&& !nextProperty.equals("administrators")
							&& !nextProperty.equals("admin")
							&& !nextProperty.equals("system")){
						properties.remove();
					}
				}
			}
			for (Entry<String, String> entry : permissions.entrySet()) {
				curPermissions.put(entry.getKey(), entry.getValue());
			}			
			appRepository.save(app);
			this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/permissions/{member}", method=RequestMethod.DELETE)
	public @ResponseBody void deletePermission(@PathVariable String appId, @PathVariable String member) throws Exception {

		Application app = getApplication(appId);
		Map<String, String> curPermissions = app.getPermissions();
		curPermissions.remove(member);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN,WRITE,READ')")
	@ApiOperation(value="Get Widget")
	@RequestMapping(value = "/{appId}/widgets/{widgetId}", method=RequestMethod.GET)
	public @ResponseBody Widget getWidget(
			@PathVariable String appId,
			@PathVariable String widgetId) throws Exception {
		
		Widget widget = widgetRepository.findByApplicationIdAndIdAndActiveTrue(appId, widgetId);
		
		if(widget==null){
			throw new ResourceNotFoundException(String.format(WIDGET_NOT_FOUND,widgetId));
		}
		
		return widget;		
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/widgets", method=RequestMethod.POST)
	@ApiOperation(value="Create Widget")
	public @ResponseBody Widget createWidget(@RequestBody Widget widget, 
			@PathVariable String appId) throws Exception {
		
		if (StringUtils.isEmpty(widget.getName() )) {
			throw new ValidationException("Widget name cannot be empty.");
		}
		
		if (StringUtils.isEmpty(widget.getTitle() )) {
			throw new ValidationException("Widget title cannot be empty.");
		}
		
		if(widget.getId() == null) {
			widget.setId(getNewId(appId, "widgetno", "WI"));
		}
		
		Widget existing = widgetRepository.findByApplicationIdAndIdAndActiveTrue(appId, widget.getId());
		
		if (existing != null) {
			// Disactivate Old Widget
			existing.setActive(false);
			widgetRepository.save(existing);
		}
		
		// get application will throw resourcenotfound exception if app is not found, so no checks
		getApplication(appId);
				
		try{
			IdentifierTools.newVersion(widget);
			widget.setApplicationId(appId);
			widget.addOrUpdate();
			widget.setActive(true);
			widgetRepository.save(widget);
		} finally {
		}
		return widget;
	}
	
	public String getNewId(String appId, String field, String prefix) throws Exception{
		return prefix + df.format(this.clusterManager.getId(appId, field));
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@ApiOperation(value="Create Multiple Widgets")
	@RequestMapping(value = "/{appId}/widgets/all", method=RequestMethod.POST)
	public @ResponseBody List<ItemResult<Widget>> createAllWidgets( 
										 @RequestBody List<Widget> widgets, @PathVariable String appId) throws Exception {
		
		List<ItemResult<Widget>> results = new ArrayList<ItemResult<Widget>>();
		
		if(widgets != null){
			for (Widget widget : widgets) {
				try {
					Widget result = null;
					result = createWidget(widget, appId);
					ItemResult<Widget> itemResult = new ItemResult<Widget>();
					itemResult.setItem(result);
					itemResult.setSuccess(true);
					results.add(itemResult);
				} catch( Exception e){
					ItemResult<Widget> itemResult = new ItemResult<Widget>();
					itemResult.setItem(widget);
					itemResult.setSuccess(false);
					itemResult.setError(e.getMessage());
				}
			}
		}
		return results;
	}

	@ApiOperation(value="Get Widgets By Application")
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/widgets", method=RequestMethod.GET)
	public @ResponseBody List<Widget> getWidgetsByApplication(
			@PathVariable String appId) throws Exception {
		
		List<Widget> widgets = widgetRepository.findByApplicationIdAndActiveTrue(appId);
		
		if(widgets==null){
			throw new ResourceNotFoundException(String.format(WIDGET_NOT_FOUND,""));
		}
		
		return widgets;		
	}	
	
	@ApiOperation(value="Get Widgets Names By Application")
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/widgets/list", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> getWidgetListByApplication(
			@PathVariable String appId) throws Exception {
		
		List<Widget> widgets = widgetRepository.findByApplicationIdAndActiveTrue(appId);
		 
		if(widgets==null){
			throw new ResourceNotFoundException(String.format(WIDGET_NOT_FOUND,""));
		}
		
		Map<String,String> returnMap = new LinkedHashMap<String,String>();
		for( Widget w : widgets) {
			returnMap.put(w.getId(), w.getTitle());
		}
		
		return returnMap;		
	}	
	
	@ApiOperation(value="Get Widgets By Application and Phase")
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/widgets/phase/{phaseId}", method=RequestMethod.GET)
	public @ResponseBody List<Widget> getWidgetsByApplicationAndPhase(
			@PathVariable String appId,
			@PathVariable String phaseId) throws Exception {
		
		return widgetRepository.findByApplicationIdAndPhaseAndActiveTrue(appId, phaseId);
	}	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/widgets/{widgetId}", method=RequestMethod.PUT)
	@ApiOperation(value="Update Widget")
	public @ResponseBody Widget updateWidget(@RequestBody Widget widget, 
			@PathVariable String appId, @PathVariable String widgetId) throws Exception {
			
		Widget existing = getWidget(appId, widgetId);

		if (existing == null) {
			throw new ResourceNotFoundException(String.format(WIDGET_NOT_FOUND, widgetId));
		}
		
		getApplication(appId);
		
		copyWidget(widget, existing);
		widget.addOrUpdate();
		widgetRepository.save(existing);
		return existing;
	}

	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/widgets/{widgetId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Widget")
	public @ResponseBody void deleteWidget(@PathVariable String appId, @PathVariable String widgetId) throws Exception {
		
		if(StringUtils.isEmpty(appId) || StringUtils.isEmpty(widgetId)){
			return;
		}
		Widget widget = getWidget(appId, widgetId);
		
		if (widget == null) {
			throw new ResourceNotFoundException(String.format(WIDGET_NOT_FOUND, widgetId));
		}
		widgetRepository.delete(widget);
	}
	
	public Widget copyWidget(Widget from, Widget to) {

		if (StringUtils.isNotEmpty(from.getApplicationId()))
			to.setApplicationId(from.getApplicationId());
		
		if (StringUtils.isNotEmpty(from.getDescription()))
			to.setDescription(from.getDescription());
		
		if (StringUtils.isNotEmpty(from.getGroup()))
			to.setGroup(from.getGroup());

		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		
		if (StringUtils.isNotEmpty(from.getPhase()))
			to.setPhase(from.getPhase());
		
		if (StringUtils.isNotEmpty(from.getTitle()))
			to.setTitle(from.getTitle());
		
		if (StringUtils.isNotEmpty(from.getWidgetType()))
			to.setWidgetType(from.getWidgetType());
		
		if (from.getHeight() != null)
			to.setHeight(from.getHeight());
		
		if (from.getWidth() != null)
			to.setWidth(from.getWidth());
		
		if (from.getFields() != null)
			to.getFields().putAll(from.getFields());
		
		return to;
	}

	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value ="/{appId}/groups/{groupId}/permissions", method=RequestMethod.GET)
	@ApiOperation(value="Get Groups Permissions")
	public @ResponseBody Map<String,String> getGroupPermissions(@PathVariable String appId ,@PathVariable String groupId) throws Exception {
		ApplicationGroup group = getGroup(appId, groupId);
		return group.getPermissions();
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/groups/{groupId}/permissions", method=RequestMethod.POST)
	@ApiOperation(value="Add Group Permissions")
	public @ResponseBody void addGroupPermissions(@PathVariable String appId, 
			@PathVariable String groupId,
			@RequestParam(required=false) boolean replace,
			@RequestBody Map<String,String> permissions) throws Exception {

		Application app = applicationCache.getItem(appId);
		ApplicationGroup group = getGroup(appId, groupId);
	
		if( group==null){
			throw new ResourceNotFoundException();
		}
		
		Map<String, String> curPermissions = group.getPermissions();
		if (curPermissions==null) {
			curPermissions = new HashMap<String,String>();
			group.setPermissions(curPermissions);
		}
		if(replace){
			Iterator<String> properties = curPermissions.keySet().iterator();
			while( properties.hasNext()){
				String nextProperty = properties.next();
				if( !permissions.containsKey(nextProperty) 
					&& !nextProperty.equals("administrators")
					&& !nextProperty.equals("system")){
					properties.remove();
				}
			}
		}
		
		for( Entry<String,String> entry : permissions.entrySet()){
			curPermissions.put(entry.getKey(), entry.getValue());
		}
		group.setPermissions(curPermissions);
		Map<String,ApplicationGroup> groups = app.getGroups();
		groups.put(group.getId(), group);
		app.setGroups(groups);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId, group.getId());
	}	
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'ADMIN')")
	@RequestMapping(value = "/{appId}/groups/{groupId}/permissions/{member}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Group Permission")
	public @ResponseBody void deleteGroupPermissions(@PathVariable String appId, 
			@PathVariable String groupId, 
			@PathVariable String member) throws Exception {

		Application app = applicationCache.getItem(appId);
		ApplicationGroup group = getGroup(appId, groupId);
		
		if( group==null){
			throw new ResourceNotFoundException();
		}
		
		Map<String, String> curPermissions = group.getPermissions();
		curPermissions.remove(member);
		appRepository.save(app);
		this.cacheInvalidationManager.invalidate(ApplicationCache.TYPE, appId);	
	}

	@RequestMapping(value = "", method=RequestMethod.GET)
	@ApiOperation(value="List Applications")
	public @ResponseBody Map<String,String> listApplications(
			@RequestParam(required=false) boolean all ) throws Exception {
		if(!all) {
			Map<String, String> map = this.applicationCache.list();
			filterMap(map,"READ,WRITE,ADMIN");
			return map;
		} else {
			List<Application> listAll = this.appRepository.findAll();
			Map<String, String> map = new HashMap<String,String>();
			for( Application app: listAll) {
				map.put(app.getId(), app.getName());
			}
			return map;
		}
	}
	
	@RequestMapping(value = "/descriptions", method=RequestMethod.GET)
	@ApiOperation(value="List Applications with Descriptions")
	public @ResponseBody Map<String,Object> listApplicationDescriptions() throws Exception {
		
		List<Application> appList = this.appRepository.findByActiveTrue();
		
		Map<String,Object> returnMap = new TreeMap<String,Object>();
		
		for( Application app : appList) {
			Map<String,String> appItem = new HashMap<String,String>();
			appItem.put("name", app.getName());
			appItem.put("description", app.getDescription());
			returnMap.put(app.getId(), appItem);
		}
		 
		filterObjectMap(returnMap,"READ,WRITE,ADMIN");
		return returnMap;
	}
	
	@PreAuthorize("hasPermission(#appId, 'APP', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{appId}/permissions", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> getPermissions(@PathVariable String appId) throws Exception {
		Application app = applicationCache.getItem(appId);
		return app.getPermissions();
	}
	
}

