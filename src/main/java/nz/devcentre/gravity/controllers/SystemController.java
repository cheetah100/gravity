/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.crypto.Cipher;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.validation.ValidationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.python.core.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.automation.EngineState;
import nz.devcentre.gravity.automation.EventToQueue;
import nz.devcentre.gravity.config.RabbitConfiguration;
import nz.devcentre.gravity.config.SystemConfiguration;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.managers.ClusterManager;
import nz.devcentre.gravity.managers.ExceptionManager;
import nz.devcentre.gravity.managers.PluginManager;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardAlert;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.ExceptionEvent;
import nz.devcentre.gravity.model.SystemReport;
import nz.devcentre.gravity.model.transfer.ExamineData;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.model.transfer.PurgeData;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.AlertService;
import nz.devcentre.gravity.services.QueryService;

@RestController
@RequestMapping("/system")
public class SystemController {

	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);

	@Autowired
	private ClusterManager clusterManager;

	@Autowired
	private BoardController boardController;

	@Autowired
	private EventToQueue eventToQueue;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private CardTaskController cardTaskController;

	@Autowired
	private CardListener cardListener;

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private ExceptionManager exceptionManager;
	
	@Autowired
	private RabbitConfiguration rabbitConfiguration;

	@Autowired
	private SecurityTool securityTool;

	@Autowired
	private TimerManager timerManager;

	@Autowired
	private SystemConfiguration systemConfiguration;

	@Autowired
	private Environment environment;

	@Autowired
	private QueryService queryService;
	
	@Autowired
	private PluginManager pluginManager;
	
	@Autowired
	private AlertService alertService;
	
	@Autowired
	private JavaMailSenderImpl emailSender;
	
	private static List<Map<String, Object>> systemStateTrail = new ArrayList<Map<String, Object>>();

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> getSystemState() throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("timestamp", new Date());
		result.put("server", clusterManager.getServerId());
		result.put("version", systemConfiguration.getVersion());
		result.put("build", systemConfiguration.getBuild());
		result.put("branch", systemConfiguration.getBranch());
		result.put("environment", systemConfiguration.getEnvironment());
		result.put("leader", Boolean.toString(clusterManager.isLeader()));
		result.put("couptime", Long.toString(clusterManager.getStartTime()));
		result.put("automation", eventToQueue.getEngineState().toString());
		result.put("maxcardsperbatch", this.cardListener.getMaxCardsPerBatch());
		result.put("automationdelay", this.cardListener.getAutomationDelay());
		result.put("automationbacklog", this.cardListener.getAutomationBacklog());
		
		result.put("smpt_server", this.emailSender.getHost());
		result.put("smpt_port", this.emailSender.getPort());
		result.put("smpt_protocol", this.emailSender.getProtocol());
		result.put("smpt_username", this.emailSender.getUsername());

		Map<String, Object> appStatus = new HashMap<String, Object>();

		Map<String, Integer> alerts = getAlertStatistics();
		appStatus.put("alerts", alerts);
		Map<String, Integer> tasks = cardTaskController.getTaskStatistics();
		appStatus.put("tasks", tasks);

		result.put("app_status", appStatus);

		Map<String, Object> nodeStatus = new HashMap<String, Object>();

		nodeStatus.put("caches", getCacheStats());
		// nodeStatus.put("automation_engine_status", automationController.state()); to
		// correct kasi

		appStatus.put("node_status", nodeStatus);
		Map<String, Object> queues = new HashMap<String, Object>();
		appStatus.put("queue_status", queues);

		appStatus.put("queue_env", rabbitConfiguration.getEnvironment());
		appStatus.put("queue_host", rabbitConfiguration.getHostName());
		appStatus.put("queue_user", rabbitConfiguration.getUserName());
		appStatus.put("queue_api", rabbitConfiguration.getApiUrl());

		appStatus.put("path", new java.io.File(".").getCanonicalPath());

		appStatus.put("keystore", securityTool.getKeyStorePath());
		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/path", method = RequestMethod.POST)
	public @ResponseBody String getPath(@RequestBody String path) throws Exception {
		return new java.io.File(path).getCanonicalPath();
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/state/trail", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getSystemStateTrail() throws Exception {
		return systemStateTrail;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/exceptions", method = RequestMethod.GET)
	public @ResponseBody List<ExceptionEvent> getExceptionEvents() throws Exception {
		return this.exceptionManager.getActiveExceptions();
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/engines", method = RequestMethod.GET)
	public @ResponseBody List<String> getEngines() throws Exception {

		List<ScriptEngineFactory> engineFactories = new ScriptEngineManager().getEngineFactories();
		List<String> names = new ArrayList<String>();

		for (ScriptEngineFactory factory : engineFactories) {
			names.addAll(factory.getNames());
			names.add(factory.getEngineName());
			names.add(factory.getLanguageName());
		}
		return names;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/engines/{engine}", method = RequestMethod.GET)
	public @ResponseBody String getEngine(@PathVariable String engine) throws Exception {

		Options.importSite = false;
		ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName(engine);
		if (scriptEngine == null) {
			throw new ResourceNotFoundException();
		}
		return ScriptEngine.ENGINE;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/exceptions/dismiss", method = RequestMethod.GET)
	public @ResponseBody void dismissExceptionEvents() throws Exception {
		this.exceptionManager.dismissExceptionEvents();
	}

	/**
	 * Get the cached items statistics from the CacheManager
	 * 
	 * @return cached items statistics as Map
	 */
	protected Map<String, Integer> getCacheStats() {
		Object[] regns = cacheManager.getRegistrations();
		Map<String, Integer> result = new HashMap<String, Integer>();
		for (Object regn : regns) {
			String cacheId = (String) regn;
			Cache<?> cache = cacheManager.getRegistered(cacheId);
			result.put(cacheId, cache.size());
		}
		return result;
	}

	/**
	 * Connect to the RabbitMQ server and retrieve the queue statistics. This
	 * requires RabbitMQ server's 15672 (API) port to be accessible to make the REST
	 * call.
	 * 
	 * @return Queue statistics as a map
	 */
	protected List<Map<String, Object>> getQueueStats() {
		String rabbitApiUrl = rabbitConfiguration.getApiUrl();
		String rabbitUser = rabbitConfiguration.getUserName();
		String rabbitPassword = rabbitConfiguration.getPassword();
		String request = rabbitApiUrl + "/api/queues";

		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(rabbitUser, rabbitPassword);
		provider.setCredentials(AuthScope.ANY, credentials);

		HttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();

		HttpGet httpGet = new HttpGet(request);
		httpGet.addHeader("content-type", "application/json");
		HttpResponse response = null;
		try {
			response = client.execute(httpGet);
		} catch (Exception e) {
			logger.error("Error connecting to the RabbitMQ server", e);
			return null;
		}

		int statusCode = response.getStatusLine().getStatusCode();

		// if (statusCode == HttpStatus.SC_OK) {
		if (statusCode == HttpStatus.OK.value()) {
			StringBuffer sb = new StringBuffer();
			HttpEntity entity = response.getEntity();
			JsonNode rootNode = null;
			BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(entity.getContent()));
				String line;
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				ObjectMapper objectMapper = new ObjectMapper();
				rootNode = objectMapper.readTree(sb.toString());
			} catch (Exception e) {
				logger.error("Exception parsing the JSON response from RabbitMQ server", e);
				return null;
			} finally {
				try {
					if (in != null)
						in.close();
				} catch (IOException ignore) {
					logger.error("Exception while closing inputstream", ignore);
				}
			}

			if (rootNode == null) {
				return null;
			}

			List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
			Iterator<JsonNode> elements = rootNode.elements();
			while (elements.hasNext()) {
				JsonNode queue = elements.next();
				JsonNode node = queue.path("name");
				String qName = node.asText();

				node = queue.path("consumers");
				Integer consumers = node.asInt();

				node = queue.path("messages");
				Integer messages = node.asInt();

				node = queue.path("messages_ready_details");
				node = node.path("messages_ready");
				Integer msgs_ready = node.asInt();

				Map<String, Object> status = new HashMap<String, Object>();
				status.put("name", qName);
				status.put("consumers", consumers);
				status.put("messages", messages);
				status.put("messages_ready", msgs_ready);

				result.add(status);
			}
			return result;
		}

		return null;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/collections/verify", method = RequestMethod.GET)
	public @ResponseBody List<String> verifyCollections() throws Exception {

		List<String> result = new ArrayList<String>();
		Map<String, String> listBoards = this.boardController.listBoards();

		for (String board : listBoards.keySet()) {
			if (!this.mongoTemplate.getDb().collectionExists(board)) {
				logger.info("Creating New Collection: " + board);
				DBObject o = new BasicDBObject();
				DBCollection createCollection = this.mongoTemplate.getDb().createCollection(board, o);
				if (createCollection == null) {
					logger.warn("Collecti" + "" + "on Creation Failed: " + board);
				}
				result.add(board);
			} else {
				logger.info("Collection Exists: " + board);
			}
		}
		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/automation", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public EngineState automation(@RequestBody String engineState) {
		try {
			EngineState state = EngineState.valueOf(engineState);
			eventToQueue.setAction(state);
			return eventToQueue.getEngineState();
		} catch( java.lang.IllegalArgumentException e) {
			throw new ValidationException(e);
		}
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/automation/state", method = RequestMethod.GET)
	public @ResponseBody EngineState state() {
		return eventToQueue.getEngineState();
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/automation/examine", method = RequestMethod.POST)
	@ApiOperation(value = "Trigger Board Automation on all/some Cards")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void examineBoard(@RequestBody ExamineData examineData) throws Exception {

		if (examineData.isAll()) {
			for (Card card : queryService.query(examineData.getBoardId(), null, null, null, true)) {
				CardHolder cardHolder = new CardHolder(examineData.getBoardId(), card.getId());
				cardListener.addCardHolder(cardHolder);
				if (examineData.getDelay() > 0) {
					Thread.sleep(examineData.getDelay());
				}
			}
		} else {
			for (String cardId : examineData.getCards()) {
				CardHolder cardHolder = new CardHolder(examineData.getBoardId(), cardId);
				cardListener.addCardHolder(cardHolder);
				if ((examineData.getDelay()) > 0) {
					Thread.sleep(examineData.getDelay());
				}
			}
		}
	}
	
	@RequestMapping(value = "/automation/plugins", method = RequestMethod.GET)
	public @ResponseBody Map<String,PluginMetadata> getAutomationPluginMetadata() {
		return this.pluginManager.getPluginMetadata();
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/automation/backlog", method = RequestMethod.GET)
	public @ResponseBody Set<CardHolder> automationBacklog() {
		return this.cardListener.getReadOnlyCardSet();
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/automation/backlog", method = RequestMethod.POST)
	public @ResponseBody boolean purgeBacklog(@RequestBody PurgeData  purgeData) {
		this.cardListener.purgeBacklog(purgeData);
		return true;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/timers", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getTimers() throws Exception {
		return this.timerManager.getTimers();
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/report", method = RequestMethod.GET)
	public @ResponseBody SystemReport getSystemReportDetail() throws Exception {

		Map<String, String> boardList = boardController.listBoards();
		SystemReport report = new SystemReport();
		for (Entry<String, String> entry : boardList.entrySet()) {
			List<CardAlert> events = (List<CardAlert>) this.alertService.getBoardAlerts(entry.getKey());
			if (events != null && !events.isEmpty()) {
				report.addAlertsPerBoard(entry.getKey(), events);
				report.addAlertCountsPerBoard(entry.getKey(), events.size());
			}
		}
		return report;
	}

	/**
	 * Periodically takes a snapshot of the node/system state
	 * 
	 * @throws Exception
	 */
	// @Scheduled( fixedDelay=300000 )
	public void snapSystemState() throws Exception {
		final int rate = 5; // Read state per 5 mins
		final int maxEntries = 2 /* hours */ * (60 / rate);
		if (!clusterManager.isLeader())
			return;

		securityTool.iAmSystem();

		Map<String, Object> sysState = getSystemState();
		systemStateTrail.add(sysState);
		if (systemStateTrail.size() > maxEntries) {
			for (int i = maxEntries; i < systemStateTrail.size(); i++) {
				systemStateTrail.remove(systemStateTrail.size() - 1);
			}
		}
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/keysizes", method = RequestMethod.GET)
	public @ResponseBody Map<String, Integer> getKeySizes() throws NoSuchAlgorithmException {
		Map<String, Integer> returnMap = new HashMap<String, Integer>();
		Set<String> algorithms = Security.getAlgorithms("Cipher");
		for (String algorithm : algorithms) {
			int max;
			max = Cipher.getMaxAllowedKeyLength(algorithm);
			returnMap.put(algorithm, max);	
		}		
		return returnMap;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/revision", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> getCommitId() {
		Map<String, Object> result = new LinkedHashMap<>();

		result.put("git.branch", this.environment.getProperty("git.branch"));
		result.put("git.commit.id", this.environment.getProperty("git.commit.id"));
		result.put("git.commit.message.short", this.environment.getProperty("git.commit.message.short"));
		result.put("git.commit.time", this.environment.getProperty("git.commit.time"));
		result.put("git.commit.user.name", this.environment.getProperty("git.commit.user.name"));
		result.put("git.commit.user.email", this.environment.getProperty("git.commit.user.email"));
		result.put("git.commit.time", this.environment.getProperty("git.commit.time"));
		result.put("git.build.user.name", this.environment.getProperty("git.build.user.name"));
		result.put("git.build.user.email", this.environment.getProperty("git.build.user.email"));
		result.put("git.build.time", this.environment.getProperty("git.build.time"));
		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/driver", method = RequestMethod.POST)
	public @ResponseBody String getDriver(@RequestBody String driver) {

		try {
			Class<?> forName = Class.forName(driver);
			if (forName == null) {
				return "Not Found";
			}
			return forName.getName();
		} catch (ClassNotFoundException e) {
			return "ClassNotFoundException: " + e.getMessage();
		}
	}
	
	/**
	 * Return the total number of cards that have alerts raised on it. Also, return the 
	 * total alerts in the system.
	 * @return Map containing alert statistics
	 * @throws Exception
	 */
	public Map<String,Integer> getAlertStatistics() throws Exception {
		List<DBObject> pipeline = new ArrayList<DBObject>();
		StringBuffer sb = new StringBuffer();
		sb.append("{$group:{_id:{board:'$board',card:'$card'},count:{ $sum:1}}}");
		BasicDBObject group = BasicDBObject.parse(sb.toString());
		pipeline.add(group);

		DBCollection collection = mongoTemplate.getDb().getCollection("gravity_alerts");
		AggregationOptions options = AggregationOptions.builder().build();
		Cursor aggregate = collection.aggregate(pipeline, options);
		
		int cardCount = 0;
		int totalAlerts = 0;
		
		while( aggregate.hasNext()) {
			DBObject result = aggregate.next();
			cardCount++;
			totalAlerts += (Integer)result.get("count");
		}
		
		aggregate.close();
		
		Map<String,Integer> result = new HashMap<String,Integer>();
		result.put("cards_in_alert", cardCount);
		result.put("total_alerts", totalAlerts);
		return result;
	}
	
	@GetMapping("/oauth2")
    public ResponseEntity<Authentication> hello(Authentication currentUser) {
        return ResponseEntity.ok(currentUser);
    }

}
