/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import java.util.Map;
import java.util.UUID;

import javax.validation.ValidationException;

import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.model.Tenant;
import nz.devcentre.gravity.repository.TenancyRepository;
import nz.devcentre.gravity.security.SecurityTool;

@RestController
@RequestMapping("/tenancy")
public class TenancyController {
	
	@Autowired
	private TenancyRepository repository;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private TenancyCache cache;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@PostMapping(value = "")
	public @ResponseBody Tenant createTenant(@RequestBody Tenant tenant) throws Exception {
		
		String newId = tenant.getId();
		if(newId==null) {
			newId = UUID.randomUUID().toString();
		}
		
		if (repository.exists(newId)) 
			throw new ValidationException("Team already exists, id: " + Encode.forHtmlContent(newId));
		
		tenant.setId(newId);
		tenant.addOrUpdate();
		
		// Ensure that the current user is assigned as the owner of the new board.
		// By default also add the administrators group as a owner.
		tenant.setPermissions(this.securityTool.initPermissions(tenant.getPermissions()));
		
		repository.save(tenant);
		this.cacheInvalidationManager.invalidate(TenancyCache.TYPE, newId);
		return tenant;
	}
	
	@PreAuthorize("hasPermission(#tenantId, 'TENANCY', 'READ,WRITE,ADMIN')")
	@GetMapping(value = "/{tenantId}")
	public @ResponseBody Tenant getTenant(@PathVariable String tenantId) throws Exception {
		return cache.getItem(tenantId);
	}
	
	@GetMapping(value = "")
	public @ResponseBody Map<String,String> listTenants() throws Exception {
		return this.securityTool.filterMap(cache.list(), "READ,WRITE,ADMIN", cache);
	}
	
	@PreAuthorize("hasPermission(#tenantId, 'TENANCY', 'ADMIN')")
	@DeleteMapping(value = "/{tenantId}")
	public void deleteTenant(@PathVariable String tenantId) throws Exception {
		this.repository.delete(tenantId);
	}
	
}
