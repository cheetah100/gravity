/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrrison
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Map;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.tools.Registerable;

public interface Cache<T> extends Registerable<CacheManager>{

	public void invalidate( String... itemIds );
	
	public T getItem(String... itemIds) throws Exception;
	
	public Map<String,String> list(String... prefixs) throws Exception;
	
	public void storeItem(T item, String... itemIds ) throws Exception;
	
	public int size();

}
