/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.model.Preference;
import nz.devcentre.gravity.repository.UserPreferenceRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/preferences")
public class UserPreferencesController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserPreferencesController.class);
	
	@Autowired
	private UserPreferenceRepository userPreferenceRepository;
	
	@Autowired
	private SecurityTool securityTool;
 	
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody Preference createPreference(@RequestBody Preference preference) throws Exception {
		
		String userid = SecurityTool.getCurrentUser();
			
		String newId = IdentifierTools.getIdFromName(preference.getName());
		preference.setUserId(userid);
		preference.setId(newId);
		
		Preference existing = userPreferenceRepository.findByUserIdAndId(userid, newId);
		
		if( existing!=null){
			existing.getValues().putAll(preference.getValues());
			userPreferenceRepository.save(existing);
			preference = existing;
		} else {
			userPreferenceRepository.save(preference);
		}
		return preference;
	}
	
	@RequestMapping(value = "/{preferenceid}", method=RequestMethod.PUT)
	public @ResponseBody Preference updatePreference(@RequestBody Preference preference, @PathVariable String preferenceid) throws Exception {
		
		String userid = SecurityTool.getCurrentUser();	
		
		Preference existing = userPreferenceRepository.findByUserIdAndId(userid, preferenceid);
		if(existing!=null){
			existing.getValues().clear();
			existing.getValues().putAll(preference.getValues());
		} else {
			existing = preference;
		}
		userPreferenceRepository.save(existing);

		return preference;
	}

	@RequestMapping(value = "/{preferenceid}", method=RequestMethod.GET)
	public @ResponseBody Preference getPreference(@PathVariable String preferenceid) throws Exception {
		
		String userid = SecurityTool.getCurrentUser();
		
		Preference preference = userPreferenceRepository.findByUserIdAndId(userid, preferenceid);
		if(preference==null){
			preference = new Preference();
			preference.setId(preferenceid);
			preference.setName(preferenceid);
		}
		return preference;	
	}
	
	@RequestMapping(value = "/{preferenceid}", method=RequestMethod.DELETE)
	public @ResponseBody void deletePreference(@PathVariable String preferenceid) throws Exception {
		
		if(StringUtils.isEmpty(preferenceid)){
			throw new FileNotFoundException();
		}
		
		String userid = SecurityTool.getCurrentUser();
		
		Preference preference = userPreferenceRepository.findByUserIdAndId(userid, preferenceid);
		userPreferenceRepository.delete(preference);
	}
	
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listPreferences() throws Exception {
		
		logger.info("Getting Preferences List");
	
		Map<String,String> result = new HashMap<String,String>();
		
		String userid = SecurityTool.getCurrentUser();
	
		List<Preference> preferences = userPreferenceRepository.findByUserId(userid);
		for (Preference p : preferences) {
			result.put(p.getId(), p.getName());
		}
		return result;			
	}

	@RequestMapping(value = "/all", method=RequestMethod.GET)
	public @ResponseBody Collection<Preference> getAllPreferences() throws Exception {
		
		String userid = SecurityTool.getCurrentUser();
		List<Preference> preferences = userPreferenceRepository.findByUserId(userid);
			
		return preferences;	
	}
	@ApiOperation(value="Get Preference for an User")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/user/{userId}", method=RequestMethod.GET)
	public @ResponseBody Collection<Preference> getPreferenceForUserId(@PathVariable(value="userId") String userId) throws Exception {
		checkUserId(userId);		
		return userPreferenceRepository.findByUserId(userId);
		
	}
	@ApiOperation(value="Get Specific Preference for an User")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/user/{userId}/{preferenceid}", method=RequestMethod.GET)
	public @ResponseBody Preference getPreferenceForUserId(@PathVariable(value="userId") String userId,@PathVariable String preferenceid) throws Exception {
		checkUserId(userId);
		return userPreferenceRepository.findByUserIdAndId(userId, preferenceid);
			
	}
	@ApiOperation(value="Create New Preference Entry for An User")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/user/{userId}", method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Preference createPreference(@PathVariable String userId,@RequestBody Preference preference) throws Exception {
			
		checkUserId(userId);
		String newId = IdentifierTools.getIdFromName(preference.getName());
		preference.setUserId(userId);
		preference.setId(newId);
		
		Preference existing = userPreferenceRepository.findByUserIdAndId(userId, newId);
		
		if( existing!=null){
			existing.getValues().putAll(preference.getValues());
			userPreferenceRepository.save(existing);
			preference = existing;
		} else {
			userPreferenceRepository.save(preference);
		}
		return preference;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Update Existing Preference Entry for An User")
	@RequestMapping(value = "/user/{userId}/{preferenceid}", method=RequestMethod.PUT,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Preference updatePreference(@PathVariable String userId ,@RequestBody Preference preference, @PathVariable String preferenceid) throws Exception {
		
		checkUserId(userId);
		Preference existing = userPreferenceRepository.findByUserIdAndId(userId, preferenceid);
		if(existing!=null){
			existing.getValues().clear();
			existing.getValues().putAll(preference.getValues());
		} else {
			throw new ValidationException(Encode.forHtmlContent("No Such Preference:-"+preferenceid+" Found for "+userId));
		}
		userPreferenceRepository.save(existing);
		return existing;
	}
	
	private void checkUserId(String userId) throws ValidationException  {
		if(!this.securityTool.userExists(userId)) {
			throw new ValidationException(Encode.forHtmlContent("No Such User:-"+userId+" Found"));
		}
	}
	
}
