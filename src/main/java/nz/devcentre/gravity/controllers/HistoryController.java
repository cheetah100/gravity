/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.QueryService;


/**
 * 
 */
@RestController
@RequestMapping("/history")
public class HistoryController {

	private static final Logger logger = LoggerFactory.getLogger(HistoryController.class);

	@Autowired
	private CardEventRepository cardEventRepository;

	@Autowired 
	private QueryService queryService;
	
	@Autowired
	private BoardService boardTools;
	
	@Autowired
	private BoardsCache boardCache;

	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Get History for Board based on Dates
	 */
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/execute", method = RequestMethod.GET)
	public @ResponseBody List<CardEvent> getHistory(@PathVariable String configId,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before) throws Exception {

		return this.getHistoryFromDb(configId, category, detail, after, before,true);

	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/jcrexecute", method = RequestMethod.GET)
	public @ResponseBody List<CardEvent> getHistoryFromJCR(@PathVariable String configId,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before) throws Exception {

		List<CardEvent> result = null;
		if (after != null)
			after = after + "T00:00:00.000";
		if (before != null)
			before = before + "T00:00:00.000";
		String scope = "";
		result = new ArrayList<CardEvent>();

		for (String boardId : getHistoryBoards()) {

			scope = String.format(URI.BOARD_URI, boardId + "//");
			
			Date from = queryService.decodeShortDate(after);
			Date to = queryService.decodeShortDate(before);
			List<CardEvent> events = cardEventRepository.findByBoardAndOccuredTimeBetweenAndCategoryOrDetail(scope, from, to, category, detail);
			
			for (CardEvent event : events) {
				if (!("alert").equals(event.getCategory())) {
					result.add(event);
				}
			}
			logger.info("History Events for Board: " + boardId + " : found: " + events.size());
		}

		return result;

	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/allhistorybydate", method = RequestMethod.GET)
	public @ResponseBody List<CardEvent> getAllHistoryFromDb(
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before) throws Exception {

		List<CardEvent> result = null;

		Date parsedFromDate = null;
		Date parsedToDate = null;

		if (after != null) {
			parsedFromDate = formatter.parse(after);
		} else {
			parsedFromDate = DateTime.now().minusDays(30).toDate();
		}
		if (before != null) {
			parsedToDate = formatter.parse(before);
		} else {
			parsedToDate = DateTime.now().toDate();
		}

		result = cardEventRepository.findByOccuredTimeBetween(parsedFromDate, parsedToDate);

		if (result != null) {
			logger.info("History Events  : found: " + result.size());
		}

		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/dbexecute", method = RequestMethod.GET)
	public @ResponseBody List<CardEvent> getHistoryFromDb(@PathVariable String configId,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before,
			@RequestParam(value = "viewall", required = false) boolean viewAll) throws Exception {

		List<CardEvent> result = null;
		//try {

			Date parsedFromDate = null;
			Date parsedToDate = null;

			if (after != null) {
				parsedFromDate = formatter.parse(after);
			} else {
				parsedFromDate = DateTime.now().minusDays(30).toDate();
			}
			if (before != null) {
				parsedToDate = formatter.parse(before);
			} else {
				parsedToDate = DateTime.now().toDate();
			}

			result = new ArrayList<CardEvent>();
			List<CardEvent> events = new ArrayList<>();
			if(viewAll) {
				logger.info("Do not exclude System Generate Events");
				events.addAll(cardEventRepository.findByOccuredTimeBetween(parsedFromDate, parsedToDate));
			}else {
				logger.info("Exclude System Genarated Events");
				events.addAll(cardEventRepository.findByOccuredTimeBetweenAndUserNotIgnoreCase(parsedFromDate, parsedToDate, "system"));
				
			}
			
			Map<String, String> boardsMap = new HashMap<String, String>();
			for (String boardId : getHistoryBoards()) {
				boardsMap.put(boardId, boardId);
			}
			
			for (CardEvent event : events) {
				if (!("alert").equals(event.getCategory()) && boardsMap.containsKey(event.getBoard())) {
					result.add(event);
				}
			}
			logger.info("History Events  : found: " + events.size());
			logger.info("History Events(Excluding History disabled Boards)  : found: " + result.size());

		//} catch (Exception e) {
		//	e.printStackTrace();
		//}
		return result;

	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/dbexecute/_paginate", method = RequestMethod.GET)
	public @ResponseBody Page<CardEvent> getHistoryFromDbPage(@PathVariable String configId,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before, Pageable pageable) throws Exception {

		//try {

			Date parsedFromDate = null;
			Date parsedToDate = null;

			if (after != null) {
				parsedFromDate = formatter.parse(after);
			} else {
				parsedFromDate = DateTime.now().minusDays(30).toDate();
			}
			if (before != null) {
				parsedToDate = formatter.parse(before);
			} else {
				parsedToDate = DateTime.now().toDate();
			}

			return cardEventRepository.findByOccuredTimeBetween(parsedFromDate, parsedToDate, pageable);

		//} catch (Exception e) {
		//	throw new ValidationException( e.getMessage(),e);
		//}

	}

	/*
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/externalList", method = RequestMethod.POST)
	public @ResponseBody List<CardEvent> saveExternalHistoryList(
			@RequestBody List<Map<String, Object>> cardEventMapList) throws Exception {
		List<CardEvent> cardEvents = new ArrayList<>();
		for (Map<String, Object> cardEventMap : cardEventMapList) {
			cardEvents.add(saveExternalHistory(cardEventMap));
		}
		return cardEvents;
	}
	*/

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/external_list/migration", method = RequestMethod.POST)
	public @ResponseBody List<CardEvent> saveExternalHistoryMigrationList(
			@RequestBody List<Map<String, Object>> cardEventMapList) throws Exception {
		List<CardEvent> cardEvents = new ArrayList<>();
		for (Map<String, Object> cardEventMap : cardEventMapList) {
			cardEvents.add(saveExternalMigrationHistory(cardEventMap));
		}
		return cardEvents;
	}

	/*
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	// Migrate history from another system
	@RequestMapping(value = "/external", method = RequestMethod.POST)
	public @ResponseBody CardEvent saveExternalHistory(@RequestBody Map<String, Object> cardEventMap) throws Exception {

		CardEvent cardEvent = null;
		String boardId = getField("boardId", cardEventMap);
		String phaseId = getField("phaseId", cardEventMap);
		String cardId = getField("cardId", cardEventMap);
		String userName = getField("user", cardEventMap);
		String detail = getField("detail", cardEventMap);
		String category = getField("category", cardEventMap);
		String date = getField("occuredTime", cardEventMap);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		Date dateDate = sdf.parse(date);
		Map<String, CardEventField> fieldsMap = new HashMap<String, CardEventField>();

		Card card = cardTools.getCard(boardId, cardId);
		Board board = this.boardCache.getItem(boardId);

		if (cardEventMap.get("fields") != null) {
			Object reasons = (Object) cardEventMap.get("fields");
			if (reasons instanceof List) {
				List reasonsList = (List) reasons;
				if (!reasonsList.isEmpty()) {
					Iterator i = reasonsList.iterator();
					while (i.hasNext()) {
						Map<String, String> map = (Map<String, String>) i.next();
						CardEventField cef = new CardEventField();
						cef.setFieldName(map.get("fieldName"));
						// cef.setId(cef.getFieldName());
						TemplateField tf = null;
						cef.setBefore(map.get("before"));
						cef.setAfter(map.get("after"));
						if (cef.getFieldName() != null && (cef.getAfter() != null || cef.getBefore() != null)) {
							if (null != board.getFieldFromLabel(cef.getFieldName())) {
								tf = board.getFieldFromLabel(cef.getFieldName());
							} else if (null != board.getField(cef.getFieldName())) {
								tf = board.getField(cef.getFieldName());
							}
							if (tf != null) {
								// cef.setId(tf.getName());
							}
							if (tf != null && tf.isReferenceField()) {
								String before = cardTools.resolveReferenceField(board, tf.getName(), cef.getBefore());
								String after = cardTools.resolveReferenceField(board, tf.getName(), cef.getAfter());
								if (before != null) cef.setBefore(before);
								if (after != null) cef.setAfter(after);
							}
						}
						cef.setReason(map.get("reason"));
						fieldsMap.put(cef.getFieldName(), cef);
					}
				}
			}
		}

		cardEvent = cardTools.storeCardEvent(detail, boardId, cardId, "info", category, userName, dateDate, fieldsMap);

		return cardEvent;
	}
	*/

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/external/migration", method = RequestMethod.POST)
	public @ResponseBody CardEvent saveExternalMigrationHistory(@RequestBody Map<String, Object> cardEventMap)
			throws Exception {

		CardEvent event = new CardEvent();
		event.setBoard(getField("board", cardEventMap));
		event.setPhase(getField("phase", cardEventMap));
		event.setCard(getField("card", cardEventMap));
		event.setUser(getField("user", cardEventMap));
		event.setDetail(getField("detail", cardEventMap));
		event.setCategory(getField("category", cardEventMap));
		event.setLevel(getField("level", cardEventMap));

		try {
			event.setOccuredTime(new Date(Long.parseLong(getField("occuredTime", cardEventMap))));
		} catch (Exception e) {
			logger.info("Wrong Date Format: " + getField("occuredTime", cardEventMap));
		}

		Map<String, CardEventField> fieldsMap = new HashMap<String, CardEventField>();

		if (cardEventMap.get("fields") != null) {
			Object reasons = (Object) cardEventMap.get("fields");
			if (reasons instanceof List) {
				List reasonsList = (List) reasons;
				if (!reasonsList.isEmpty()) {
					Iterator i = reasonsList.iterator();
					while (i.hasNext()) {
						Map<String, String> map = (Map<String, String>) i.next();
						CardEventField cef = new CardEventField();
						cef.setFieldName(map.get("fieldName"));
						// cef.setId(cef.getFieldName());
						cef.setBefore(map.get("before"));
						cef.setAfter(map.get("after"));
						cef.setReason(map.get("reason"));
						fieldsMap.put(cef.getFieldName(), cef);
					}
				}
			} else if (reasons instanceof Map) {
				Map reasonsList = (Map) reasons;
				if (!reasonsList.isEmpty()) {
					Iterator i = reasonsList.keySet().iterator();
					while (i.hasNext()) {
						Object key = i.next();
						Map<String, String> map = (Map<String, String>) reasonsList.get(key);

						if (map != null) {
							CardEventField cef = new CardEventField();
							cef.setFieldName(getStringField("fieldName", map));
							cef.setBefore(getStringField("before", map));
							cef.setAfter(getStringField("after", map));
							cef.setReason(getStringField("reason", map));
							fieldsMap.put(key.toString(), cef);
						}
					}
				}
			}
		}
		event.setFields(fieldsMap);

		logger.info("Card Event: Saving History " + event.toString());
		cardEventRepository.save(event);
		return event;
	}

	private String getField(String name, Map<String, Object> list) {
		Object ob = list.get(name);
		if (ob != null) {
			return ob.toString();
		}
		return null;
	}

	private String getStringField(String name, Map<String, String> list) {
		Object ob = list.get(name);
		if (ob != null) {
			return ob.toString();
		}
		return null;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public @ResponseBody List<CardEvent> getAllHistory() throws Exception {

		List<CardEvent> result = null;

		//try {
			result = cardEventRepository.findAll();
			if (result != null)
				logger.info("History Events  : found: " + result.size());

		//} catch (Exception e) {
		//	e.printStackTrace();
		//}
		return result;

	}

	private List<String> getHistoryBoards() throws Exception {
		List<String> boards = new ArrayList<String>();
		Map<String, String> listBoards = boardCache.getListFromStore();
		String types = "READ,WRITE,ADMIN";
		boardTools.filterMap(listBoards, types);
		for (String boardId : listBoards.keySet()) {
			Board board = boardCache.getItem(boardId);
			if (board.isHistory()) {
				boards.add(boardId);
			}
		}
		return boards;
	}

}
