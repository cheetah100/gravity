/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.ListResource;
import nz.devcentre.gravity.model.Option;
import nz.devcentre.gravity.model.PivotTable;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.transfer.ListData;
import nz.devcentre.gravity.model.transfer.PivotData;
import nz.devcentre.gravity.repository.PivotTableRepository;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.FilterTools;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/board/{boardId}/pivots")
public class PivotTableController {
	
	private static final Logger logger = LoggerFactory.getLogger(PivotTableController.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
		
	@Autowired 
	private QueryService queryService;
	
	@Autowired 
	private PivotDataCache pivotDataCache;
	
	@Autowired 
	private BoardsCache boardsCache;
		
	@Autowired
	private ListController listController;
	
	@Autowired
	private TransformController transformController;
	
	@Autowired
	private PivotTableRepository pivotTableRepository;
	
	@Autowired
	private FilterController filterController;
	
	@Autowired
	private PivotTableTools pivotTableTools;
		
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody PivotTable createPivot(@PathVariable String boardId,
										  @RequestBody PivotTable pivot) throws Exception {
		/*String newId = pivot.getId();
		if (newId == null) {
			newId = IdentifierTools.getIdFromBoardClass(pivot);
		}
		pivot.setId(newId);*/
		
		IdentifierTools.newVersion(pivot);
		pivot.setBoardId(boardId);
		pivot.addOrUpdate();
		
		pivot.setActive(true);
		
		//Disable Current Active
		if(pivot.isActive()) {
			pivotTableTools.disableCurrentActive(boardId, pivot.getId());
		}
		
		pivotTableRepository.save(pivot);	
		return pivot;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")	
	@RequestMapping(value = "/{pivotId}", method=RequestMethod.PUT)
	public @ResponseBody PivotTable updatePivot(@PathVariable String boardId,
										  @PathVariable String pivotId,
										  @RequestBody PivotTable pivot) throws Exception {
		
		PivotTable existing = pivotTableRepository.findByBoardIdAndIdAndActiveTrue(boardId, pivotId);
		copyPivot(pivot, existing);
		
		
		//Disable Current Active
		if(existing.isActive()) {
			pivotTableTools.disableCurrentActive(boardId,pivotId);
		}
		
		IdentifierTools.newVersion(existing);
		pivotTableRepository.save(existing);

		this.cacheInvalidationManager.invalidate(PivotDataCache.TYPE, boardId, pivotId);
		return existing;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/history", method=RequestMethod.GET)
	public @ResponseBody List<PivotTable> getPivotHistory(@PathVariable String boardId, 
			@PathVariable String pivotId) throws Exception {
		
		List<PivotTable> pivot = pivotTableRepository.findByBoardIdAndId(boardId, pivotId);
		if(pivot==null){
			throw new ResourceNotFoundException("Pivot not found, id: " + Encode.forHtmlContent(pivotId));
		}		
		return pivot;		
	}
	
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")

	@RequestMapping(value = "/{pivotId}/activate/{version}", method = RequestMethod.GET)
	public @ResponseBody PivotTable activatePivotVersion(@PathVariable String boardId, @PathVariable String pivotId,
			@RequestParam String version) throws Exception {

		PivotTable pivot = pivotTableRepository.findByBoardIdAndIdAndVersion(boardId, pivotId, version);
		if (pivot == null) {
			throw new ResourceNotFoundException("Pivot not found, id: " + Encode.forHtmlContent(pivotId));
		}
		//Version Already Active
		if(pivot.isActive()) {
			return pivot;
		}
		
		// disable current active
		pivotTableTools.disableCurrentActive(boardId, pivotId);
		pivot.setActive(true);
		this.pivotTableRepository.save(pivot);
		this.cacheInvalidationManager.invalidate(PivotDataCache.TYPE, boardId, pivotId);
		return pivot;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{pivotId}", method=RequestMethod.DELETE)
	public @ResponseBody void deletePivot(@PathVariable String boardId, 
			@PathVariable String pivotId) throws Exception {
		
		if( StringUtils.isBlank(pivotId)){
			return;
		}
		
		PivotTable pivot = pivotTableTools.getPivot(boardId, pivotId);
		if(pivot==null){
			throw new ResourceNotFoundException("Pivot not found, id: " + Encode.forHtmlContent(pivotId));
		}
		pivot.setActive(false);
		this.pivotTableRepository.save(pivot);
		//pivotTableRepository.delete(pivot);
		this.cacheInvalidationManager.invalidate(PivotDataCache.TYPE, boardId, pivotId);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")	
	@RequestMapping(value = "/invalidate", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> invalidatePivotCache(@PathVariable String boardId) 
														throws Exception {
		
		Map<String,String> result = new HashMap<String, String>();
		List<PivotTable> pivots = pivotTableRepository.findByBoardIdAndActiveTrue(boardId);
		for (PivotTable pivot: pivots) {
			String pivotId = pivot.getId();
			logger.info("Invalidating Pivot Cache" + boardId + ":" + pivotId);
			this.cacheInvalidationManager.invalidate(PivotDataCache.TYPE, boardId, pivotId);
			result.put(pivot.getId(), "true");
			logger.info("Pivot Cache cleared for " + boardId );
			
		}
		return result;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")	
	@RequestMapping(value = "/invalidate/{pivotId}", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> invalidatePivotCacheById(@PathVariable String boardId,
														@PathVariable String pivotId) 
														throws Exception {
		
		Map<String,String> result = new HashMap<String, String>();
		logger.info("Invalidating Pivot Cache" + boardId + ":" + pivotId);
		this.cacheInvalidationManager.invalidate(PivotDataCache.TYPE, boardId, pivotId);
		result.put(pivotId, "true");
		return result;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ROLLUP,READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/aggregate", method=RequestMethod.GET)
	public @ResponseBody Object aggregatePivot(@PathVariable String boardId, 
												@PathVariable String pivotId) throws Exception {
		
		PivotTable pivot = pivotTableTools.getPivot(boardId, pivotId);

		Criteria xAxisCriteria = new Criteria(pivot.getxAxis()).exists(true);
		Criteria yAxisCriteria = new Criteria(pivot.getyAxis()).exists(true);
		MatchOperation match = Aggregation.match(new Criteria().andOperator(xAxisCriteria, yAxisCriteria));

		GroupOperation group = Aggregation.group(pivot.getxAxis(), pivot.getyAxis()).count().as("value");

		SortOperation sort = Aggregation.sort(Direction.ASC, pivot.getxAxis(), pivot.getyAxis());

		Aggregation agg = Aggregation.newAggregation(match, group, sort);

		AggregationResults<Object> aggregate = mongoTemplate.aggregate(agg, pivot.getBoardId(), Object.class);
		return new Gson().toJson(aggregate);
		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ROLLUP,READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/execute", method=RequestMethod.GET)
	public @ResponseBody PivotData executePivot(@PathVariable String boardId, 
												@PathVariable String pivotId) throws Exception {
		
		PivotData pivotData = null;
		String pid = IdentifierTools.getIdFromName(pivotId);
		
		
		try {
			try{
				pivotData = pivotDataCache.getItem(boardId, pid);
			}catch(ResourceNotFoundException re){
				//pivotData is null Go ahead and create it
			}
			
			if(pivotData == null){
				PivotTable pivot = pivotTableTools.getPivot(boardId, pid); 
				//PivotTable pivot = (PivotTable) ocm.getObject(PivotTable.class,String.format(URI.PIVOT_TABLE_URI, boardId, pid));
				
				pivotData = pivot.getType().getPivotDataInstance();
			
				// Get raw data with applicable filter
				Collection<Card> queryResultSet = null;
				if(StringUtils.isEmpty(pivot.getTransform())){
					queryResultSet = queryService.query(boardId, pivot.getPhase(), pivot.getFilter(), null, false);
					pivotData.addDatasource(boardId);
				} else {
					Map<String, Object> executeTransform = transformController.executeRollupTransform(pivot.getTransform());
					queryResultSet = new ArrayList<Card>();
					for( Object card : executeTransform.values()){
						queryResultSet.add((Card)card);
					}
				}
				
				runPivot( boardId, queryResultSet, pivotData, pivot);
								
				pivotDataCache.storeItem(pivotData, boardId, pid);
			}
		} finally {
		}
		
		return pivotData;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ROLLUP,READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/execute/filter", method=RequestMethod.POST)
	public @ResponseBody PivotData executeFilteredPivot(@PathVariable String boardId, 
												@PathVariable String pivotId,
												@RequestBody Filter filter) throws Exception {
		
		String pid = IdentifierTools.getIdFromName(pivotId);
		PivotTable pivot = pivotTableRepository.findByBoardIdAndIdAndActiveTrue(boardId, pid);
	
		Filter storedFilter = null;
		if( !StringUtils.isEmpty(pivot.getFilter())){
			storedFilter = filterController.getFilter(boardId, pivot.getFilter());
		}
		queryService.resolveDynamicFilter(boardId, filter, storedFilter);

		Map<String,Condition> conditions = filter.getConditions();
		if(conditions== null || conditions.size()==0){
			return executePivot( boardId, pivotId);
		}
		
		PivotData pivotData = null;
			
			pivotData = (pivot.getType()).getPivotDataInstance();
			
			// Get Filter
			if( filter.getConditions()==null){
				filter.setConditions(new HashMap<String,Condition>());
				logger.warn("No Filter Conditions Found in Request");
			}
			
			if( !StringUtils.isEmpty(pivot.getFilter())){
				
				if(storedFilter!=null && storedFilter.getConditions() != null) {
					filter.getConditions().putAll(storedFilter.getConditions());
				} else {
					logger.warn("No filter conditions found for pivot " + boardId + "." + pivotId);
				}
			}
		
			// Get raw data with applicable filter
			Collection<Card> queryResultSet = null;
			if(StringUtils.isEmpty(pivot.getTransform())){
				queryResultSet = queryService.dynamicQuery(boardId, null, filter, null, false);
				pivotData.addDatasource(boardId);
			} else {
				Map<String, Object> executeTransform = transformController.executeRollupTransform(pivot.getTransform());
				queryResultSet = new ArrayList<Card>();
				for( Object card : executeTransform.values()){
					queryResultSet.add((Card)card);
				}
			}

			runPivot( boardId, queryResultSet, pivotData, pivot);
								
		return pivotData;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ROLLUP,READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/execute/dynamic", method=RequestMethod.POST)
	public @ResponseBody PivotData executeDynamicFilteredPivot(@PathVariable String boardId, 
					@PathVariable String pivotId,
					@RequestBody Map<String, Object> fieldMap) throws Exception {
		
		String pid = IdentifierTools.getIdFromName(pivotId);
		PivotTable pivot = pivotTableRepository.findByBoardIdAndIdAndActiveTrue(boardId, pid);
	
		Filter storedFilter = null;
		if( !StringUtils.isEmpty(pivot.getFilter())){
			storedFilter = filterController.getFilter(boardId, pivot.getFilter());
		}
		
		if( storedFilter!=null){
			Map<String,Condition> conditions = storedFilter.getConditions();
			if(conditions== null || conditions.size()==0){
				return executePivot(boardId, pivotId);
			}
		
			Filter filter = FilterTools.replaceFilterValues(storedFilter, fieldMap);
		
			queryService.resolveDynamicFilter(boardId, filter, storedFilter);

			PivotData pivotData = (pivot.getType()).getPivotDataInstance();
								
			// Get raw data with applicable filter
			Collection<Card> queryResultSet = null;
			if(StringUtils.isEmpty(pivot.getTransform())){
				queryResultSet = queryService.dynamicQuery(boardId, null, filter, null, false);
				pivotData.addDatasource(boardId);
			} else {
				Map<String, Object> executeTransform = transformController.executeRollupTransform(pivot.getTransform());
				queryResultSet = new ArrayList<Card>();
				for( Object card : executeTransform.values()){
					queryResultSet.add((Card)card);
				}
			}

			runPivot( boardId, queryResultSet, pivotData, pivot);
								
			return pivotData;
		} else {
			return executePivot(boardId, pivotId);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void runPivot(String boardId,
			Collection<Card> queryResultSet,
			PivotData pivotData, 
			PivotTable pivot) throws Exception{
		
		// Construct the Pivot Data
		for( Card card : queryResultSet ){
			
			Object xAxisValue = card.getFields().get(pivot.getxAxis());
			Object yAxisValue = card.getFields().get(pivot.getyAxis());
			Object value = card.getFields().get(pivot.getField());
			
			String xName = "";
			String yName = "";
			
			Number n = 0;
			if(value!=null && value instanceof Number){
				n = (Number) value;
			}
			if( xAxisValue!=null && yAxisValue!=null) {
				// Only one of the x and y can be a list
				if(xAxisValue instanceof Object[]){						
					yName = yAxisValue.toString();
					Object[] xListValues = (Object[])xAxisValue;
					for (Object xListValue : xListValues){
						xName = xListValue.toString();
						pivotData.addData(xName,yName, n);
					}
				} else if(xAxisValue instanceof List){						
					yName = yAxisValue.toString();
					List<Object> xListValues = (List<Object>)xAxisValue;
					for (Object xListValue : xListValues){
						xName = xListValue.toString();
						pivotData.addData(xName,yName, n);
					}
				} else if(yAxisValue instanceof Object[]){
					xName = xAxisValue.toString();
					Object[] yListValues = (Object[])yAxisValue;
					for (Object yListValue : yListValues){
						yName = yListValue.toString();
						pivotData.addData(xName,yName, n);
					}
				} else if(yAxisValue instanceof List){
					xName = xAxisValue.toString();
					List<Object> yListValues = (List<Object>)yAxisValue;
					for (Object yListValue : yListValues){
						yName = yListValue.toString();
						pivotData.addData(xName,yName, n);
					}	
				}else{
					xName = xAxisValue.toString();
					yName = yAxisValue.toString();	
					pivotData.addData(xName,yName, n);
				}
			}
		}
		
		// Correct X Axis if a list
		String xDataSource = queryService.getDatasource(boardId, pivot.getxAxis());
		if( xDataSource!=null){
			Map<String, Option> orderedXList = getList(xDataSource, pivot.getxAxis());
			if (orderedXList != null) {
				pivotData.reorderxAxis(orderedXList);
				pivotData.addDatasource(xDataSource);
			}
		} else {
			logger.warn("Datasource Not Found for xAxis : " + pivot.getxAxis());
		}

		// Correct Y Axis if a list
		String yDataSource = queryService.getDatasource(boardId, pivot.getyAxis());
		if( yDataSource!=null){
			Map<String, Option> orderedYList = getList(yDataSource, pivot.getyAxis());
			if (orderedYList != null) {
				pivotData.reorderyAxis(orderedYList);
				pivotData.addDatasource(yDataSource);
			}
		} else {
			logger.warn("Datasource Not Found for yAxis : " + pivot.getyAxis());
		}
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/diagnose", method=RequestMethod.GET)
	public @ResponseBody Map<String, List<Card>> diagnosePivot(@PathVariable String boardId, 
												@PathVariable String pivotId) 
														throws Exception {
		
		//String pid = IdentifierTools.getIdFromName(pivotId);
		ListData pivotData = new ListData();	
		PivotTable pivot = pivotTableTools.getPivot(boardId, pivotId);
		if(pivot==null){
			throw new ResourceNotFoundException();
		}
		
		// Get raw data with applicable filter
		Collection<Card> queryResultSet = null;
		if(StringUtils.isEmpty(pivot.getTransform())){
			queryResultSet = queryService.query(boardId, null, pivot.getFilter(), null, false);
		} else {
			Map<String, Object> executeTransform = transformController.executeRollupTransform(pivot.getTransform());
			queryResultSet = new ArrayList<Card>();
			for( Object obj : executeTransform.values()){
				Card card = (Card) obj;
				queryResultSet.add(card);
			}
		}
		
		runPivot(boardId, queryResultSet, pivotData, pivot );							
		return pivotData.getList();
	}
	
	private Map<String, Option> getList( String boardId,String fieldName) {
		
		String[] fieldElements = fieldName.split("\\.");
		String lastField = fieldElements[fieldElements.length-1];
		
		try{
			// Load Board
			Board board = boardsCache.getItem(boardId);
			
			// Find the xaxis field. Does it have a optionList
			TemplateField field = board.getField(lastField);
			if(field==null || StringUtils.isEmpty(field.getOptionlist())){
				return null;
			}
			
			ListResource list = listController.getList(boardId,field.getOptionlist(),field.getOptionlistfilter());
			
			if(list==null){
				logger.warn("List not found for " + boardId + "." + field.getOptionlist());
				return null;
			}
			
			return list.getItems();
		} catch( Exception e){
			logger.warn("Exception running Pivot getList: " +e.getMessage(), e);
		}
		
		return null;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}/execute", method=RequestMethod.POST)
	public @ResponseBody PivotData executeDynamicPivot(@PathVariable String boardId, 
													@PathVariable String pivotId,
													@RequestBody PivotTable pivot) 
														throws Exception {
		
		PivotData pivotData = null;
		String pid = IdentifierTools.getIdFromName(pivotId);
		
		try{
			pivotData = pivotDataCache.getItem(boardId, pid);
		}catch(ResourceNotFoundException re){
			//pivotData is null Go ahead and create it
		}
		if(pivotData == null){
			pivotData = pivot.getType().getPivotDataInstance();
		
			// Get raw data with applicable filter
			Collection<Card> queryResultSet = null;
			if(pivot.getTransform()==null){
				queryResultSet = queryService.query(boardId, null, pivot.getFilter(), null, false);
			} else {
				Map<String, Object> executeTransform = transformController.executeRollupTransform(pivot.getTransform());
				queryResultSet = new ArrayList<Card>();
				for( Object card : executeTransform.values()){
					queryResultSet.add((Card)card);
				}
			}
			
			// Construct the Pivot Data
			runPivot(boardId, queryResultSet, pivotData, pivot );
		}

		pivotDataCache.storeItem(pivotData, boardId, pid);
		return pivotData;
	}

	/**
	 * Do not allow id, Path and Deleted flag to be updated here
	 * @param from
	 * @param to
	 * @return
	 */
	protected static PivotTable copyPivot(PivotTable from, PivotTable to) {
		if (StringUtils.isNotEmpty(from.getBoardId()))
			to.setBoardId(from.getBoardId());
		if (StringUtils.isNotEmpty(from.getField()))
			to.setField(from.getField());
		if (StringUtils.isNotEmpty(from.getFilter()))
			to.setFilter(from.getFilter());
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		if (StringUtils.isNotEmpty(from.getPhase()))
			to.setPhase(from.getPhase());
		if (StringUtils.isNotEmpty(from.getTransform()))
			to.setTransform(from.getTransform());
		if (StringUtils.isNotEmpty(from.getxAxis()))
			to.setxAxis(from.getxAxis());
		if (StringUtils.isNotEmpty(from.getyAxis()))
			to.setyAxis(from.getyAxis());
		if (from.getType() != null)
			to.setType(from.getType());
		return to;
	}
}
