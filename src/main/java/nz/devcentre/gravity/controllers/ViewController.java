/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/views")
public class ViewController {
	
	private static final Logger logger = LoggerFactory.getLogger(ViewController.class);
		
	@Autowired
	BoardsCache boardCache;
	
	@Autowired
	BoardService boardTools;
	
	@Autowired
	CacheInvalidationInterface cacheInvalidationManager;
	
	Map<String,TemplateField> quaziFields;
		
	public ViewController() throws Exception {
		quaziFields = new HashMap<String,TemplateField>();
		quaziFields.put("id", getQuaziTemplateField("id","id","Unique ID",FieldType.STRING));
		quaziFields.put("phase", getQuaziTemplateField("phase","phase","Phase",FieldType.STRING));
		quaziFields.put("created", getQuaziTemplateField("created","created","Created Date",FieldType.DATE));
		quaziFields.put("modified", getQuaziTemplateField("modified","modified","Modified Date",FieldType.DATE));
		quaziFields.put("creator", getQuaziTemplateField("creator","creator","Creator",FieldType.STRING));
		quaziFields.put("modifiedby", getQuaziTemplateField("modifiedby","modifiedby","Modified By",FieldType.STRING));
		
	}
	
	private TemplateField getQuaziTemplateField(String id, String name, String label, FieldType fieldType) throws Exception{
		TemplateField tf = new TemplateField();
		tf.setId(id);
		tf.setName(name);
		tf.setLabel(label);
		tf.setRequired(false);
		tf.setType(fieldType);
		return tf;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody View createView(@PathVariable String boardId,
										  @RequestBody View view) throws Exception {
		
		Board board = boardCache.getItem(boardId);
		String newId = IdentifierTools.getIdFromNamedModelClass(view);
		view.setId(newId);
		view.setBoardId(boardId);
		view.addOrUpdate();
		Map<String,View> views = board.getViews();
		if (views == null) {
			views = new HashMap<String,View>();
			board.setViews(views);
		}
		if (views.get(view.getId())!= null) {
			throw new ValidationException("View already exists: " + Encode.forHtmlContent(view.getId()));
		}
		views.put(view.getId(), view);
		this.boardTools.saveBoard(board);
		return view;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}", method=RequestMethod.PUT)
	public @ResponseBody View updateView(@PathVariable String boardId,
										@PathVariable String viewId,
										  @RequestBody View view) throws Exception {
		
		Board board = boardCache.getItem(boardId);			
		
		Map<String,View> views = board.getViews();
		if (views == null)
			throw new ResourceNotFoundException("View not found, id: " + Encode.forHtmlContent(viewId));
		View existView = views.get(viewId);
				
		if (existView == null) {
			throw new ResourceNotFoundException("View not found, id: " + Encode.forHtmlContent(viewId));
		}
			
		
		view.addOrUpdate();
		views.put(view.getId(), view);
		this.boardTools.saveBoard(board);
			
		return view;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}", method=RequestMethod.GET)
	public @ResponseBody View getView(@PathVariable String boardId, 
			@PathVariable String viewId) throws Exception {
		
		View view = null;
		Board board = boardCache.getItem(boardId);
		Map<String,View> views = board.getViews();
		if (views == null)
			throw new ResourceNotFoundException("View not found, id: " + Encode.forHtmlContent(viewId));
		view = views.get(viewId);
				
		if (view == null) {
			throw new ResourceNotFoundException("View not found, id: " + Encode.forHtmlContent(viewId));
		}
			
		return view;		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, View> listViewDetails(@PathVariable String boardId) throws Exception {
		
		Map<String, String> viewIds = this.listViews(boardId);
		Map<String, View> viewDetails = new HashMap<String, View>();
		if(viewIds != null) {
			for( String viewId : viewIds.keySet()){
				View view = this.getView(boardId, viewId);
				viewDetails.put(viewId, view);	
			}
		}
		return viewDetails;
	}
	
	/*
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}/template", method=RequestMethod.GET)
	public @ResponseBody Template getViewWithTemplateData(@PathVariable String boardId, 
			@PathVariable String viewId) throws Exception {
		
		View view = getView(boardId, viewId);
		
		Board board = this.boardCache.getItem(boardId);
						
		Map<String,TemplateField> fields = new HashMap<String,TemplateField>();
		
		if( view!=null){
			newTemplate.setUi(merge(template.getUi(), view.getUi()));
			if(view.getType()!=null){
				newTemplate.setType(view.getType());
			}
			for( ViewField vf : view.getFields().values()){
				
				String fieldName = vf.getName();
				if(fieldName==null){
					fieldName = vf.getId();
				}
				
				TemplateField tf = getTemplateFieldByReference(fieldName, template );
				
				if(tf==null){
					tf = this.quaziFields.get(vf.getId());
					if(tf==null){
						tf = getQuaziTemplateField(vf.getId(),vf.getName(),vf.getLabel(),FieldType.STRING);
					}
				}
				
				TemplateField newTf = null;
				if(tf!=null){
					newTf = tf.copy();
					newTf.setUi(merge(tf.getUi(), vf.getUi()));
					newTf.setIndex(vf.getIndex());
					if(vf.getLabel()!=null){
						newTf.setLabel(vf.getLabel());
					}
					fields.put(newTf.getId(), newTf);
				} else {
					logger.warn("View Field not found in Template: " + viewId + "." + vf.getId());
				}
			}
		} else {
			newTemplate.setUi(merge(template.getUi(), null));
			for( TemplateField tf : template.getFields().values()){
				
				TemplateField newTf = null;
				newTf = tf.copy();
				newTf.setUi(merge(tf.getUi(), tf.getUi()));
				newTf.setIndex(tf.getIndex());
				fields.put(newTf.getId(), newTf);
			}
		}
		
		TemplateGroup templateGroup = new TemplateGroup();
		templateGroup.setId(viewId);
		if(view!=null){
			templateGroup.setName(view.getName());
		} else {
			templateGroup.setName("Default");
		}
		templateGroup.setIndex(1);
		templateGroup.setFields(fields);
		
		Map<String,TemplateGroup> newGroups = new HashMap<String,TemplateGroup>();
		newGroups.put(templateGroup.getId(), templateGroup);
		newTemplate.setGroups(newGroups);
		return newTemplate;		
	}
	
	*/
	
	/*
	private TemplateField getTemplateFieldByReference(String field, Template template ) throws Exception{
		
		if(field==null){
			logger.info("Template: No Supplied Field");
			return null;
		}
		
		logger.info("Finding Reference TemplateField on " + field);
		
		String[] fieldPath = field.split("\\.");
		Template currentTemplate = template;
		for( int f=0; f<(fieldPath.length-1); f++){
			TemplateField templateField = currentTemplate.getField(fieldPath[f]);
			if(templateField==null){
				logger.warn("No TemplateField Found for " + fieldPath[f]);
				return null;
			}
			currentTemplate = templateCache.getItem(templateField.getOptionlist(),templateField.getOptionlist());
			if(currentTemplate==null){
				logger.warn("No Template Found for " + templateField.getOptionlist());
				return null;
			}
		}
		
		String finalField = fieldPath[fieldPath.length-1];
		TemplateField fieldSearch = currentTemplate.getField(finalField);
		TemplateField newField = null;
		if( fieldSearch!=null){
			newField = fieldSearch.copy();
			newField.setName(field);
			logger.info("Final TemplateField Found for " + finalField + " in template " + template.getId());
		} else {
			logger.warn("No Final TemplateField Found for " + finalField + " in template " + template.getId());
		}
		return newField;

	}
	*/
	
	private Map<String,Object> merge(Map<String,Object> m1, Map<String,Object> m2 ){
		Map<String,Object> result = new HashMap<String,Object>();
		if(m1!=null){
			result.putAll(m1);
		}
		if(m2!=null){
			result.putAll(m2);
		}
		return result;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{viewId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteView(@PathVariable String boardId, 
			@PathVariable String viewId) throws Exception {
		
		if(StringUtils.isEmpty(boardId) || StringUtils.isEmpty(viewId)){
			throw new ResourceNotFoundException("View Not Found");
		}

		Board board = boardCache.getItem(boardId);
		View view = getView(boardId, viewId);
		board.getViews().remove(view.getId());
		this.boardTools.saveBoard(board);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}/fields/{fieldId}", method=RequestMethod.GET)
	public @ResponseBody ViewField getViewField(@PathVariable String boardId, 
			@PathVariable String viewId,
			@PathVariable String fieldId) throws Exception {
		
		View view = getView(boardId, viewId);
		ViewField viewField;
		Map<String, ViewField> fields = view.getFields();
		viewField = fields.get(fieldId);
		if(viewField==null){
			throw new ResourceNotFoundException();
		}			
		return viewField;		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{viewId}/fields", method=RequestMethod.POST)
	public @ResponseBody ViewField saveViewField(@PathVariable String boardId, 
			@PathVariable String viewId,
			@RequestBody ViewField viewField) throws Exception {
		
		Board board = boardCache.getItem(boardId);
		View view = getView(boardId, viewId);
		view.getFields().put(viewField.getName(), viewField);
		this.boardTools.saveBoard(board);
		return viewField;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{viewId}/fields/{fieldId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteViewField(@PathVariable String boardId, 
			@PathVariable String viewId,
			@PathVariable String fieldId) throws Exception {

		if (StringUtils.isEmpty(boardId) || StringUtils.isEmpty(viewId) || StringUtils.isEmpty(fieldId)) {
			throw new ResourceNotFoundException("View Not Found");
		}

		Board board = boardCache.getItem(boardId);
		View view = getView(boardId, viewId);
		if (view == null) {
			logger.error("View not found: " + boardId + ":" + viewId);
			throw new ResourceNotFoundException(boardId + ":" + Encode.forHtmlContent(viewId));
		}
		view.getFields().remove(fieldId);
		this.boardTools.saveBoard(board);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listViews(@PathVariable String boardId) throws Exception {
	
		Map<String,String> result = new HashMap<String, String>();
		Board board = boardCache.getItem(boardId);
		Map<String,View> views = board.getViews();
		if (views == null)
			return result;
		for (View view: views.values()) {
			result.put(view.getId(), view.getName());
		}
		return result;			
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}/permissions", method=RequestMethod.GET)
	@ApiOperation(value="Get View Permissions")
	public @ResponseBody Map<String,String> getPermissions(@PathVariable String boardId,@PathVariable String viewId) throws Exception {
		Board board = boardCache.getItem(boardId);
		View view = board.getViews().get(viewId);
		return view.getPermissions();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{viewId}/permissions", method=RequestMethod.POST)
	@ApiOperation(value="Add View Permissions")
	public @ResponseBody void addPermissions(@PathVariable String boardId, 
			@PathVariable String viewId,
			@RequestParam(required=false) boolean replace,
			@RequestBody Map<String,String> permissions) throws Exception {

		Board board = boardCache.getItem(boardId);
		View view = getView(boardId, viewId);
	
		if( view==null){
			throw new ResourceNotFoundException();
		}
		
		Map<String, String> curPermissions = view.getPermissions();
		if (curPermissions==null) {
			curPermissions = new HashMap<String,String>();
			view.setPermissions(curPermissions);
		}
		if(replace){
			//PropertyIterator properties = node.getProperties();
			Iterator<String> properties = curPermissions.keySet().iterator();
			while( properties.hasNext()){
				String nextProperty = properties.next();
				if(!permissions.containsKey(nextProperty) 
					&& !nextProperty.equals("administrators")
					&& !nextProperty.equals("system")){
					properties.remove();
				}
			}
		}
		
		for( Entry<String,String> entry : permissions.entrySet()){
			curPermissions.put(entry.getKey(), entry.getValue());
		}
		this.boardTools.saveBoard(board);
	}	
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{viewId}/permissions/{member}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete View Permission")
	public @ResponseBody void deletePermissions(@PathVariable String boardId, 
			@PathVariable String viewId, 
			@PathVariable String member) throws Exception {

		Board board = boardCache.getItem(boardId);
		View view = getView(boardId, viewId);
		
		if( view==null){
			throw new ResourceNotFoundException();
		}
		
		Map<String, String> curPermissions = view.getPermissions();
		curPermissions.remove(member);
		this.boardTools.saveBoard(board);
	}
}
