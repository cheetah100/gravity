/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.FilterTools;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/filters")
public class FilterController {

	private static final Logger logger = LoggerFactory.getLogger(FilterController.class);

	@Autowired
	private QueryService queryService;

	@Autowired
	private BoardsCache boardCache;
	
	@Autowired
	private BoardService boardTools;

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody Filter createFilter(@PathVariable String boardId, @RequestBody Filter filter)
			throws Exception {
		filter.setBoardId(boardId);
		String newId = IdentifierTools.getIdFromNamedModelClass(filter);
		filter.setId(newId);

		Board board = this.boardCache.getItem(boardId);
		Map<String, Filter> filters = board.getFilters();
		if (filters == null) {
			filters = new HashMap<String, Filter>();
			board.setFilters(filters);
		}
		if (filters.get(filter.getId()) != null) {
			throw new ValidationException("Filter already exists: " + Encode.forHtmlContent(filter.getId()));
		}

		filters.put(filter.getId(), filter);
		this.boardTools.saveBoard(board);
		
		return filter;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}", method = RequestMethod.PUT)
	public @ResponseBody Filter updateFilter(@PathVariable String boardId, @PathVariable String filterId,
			@RequestBody Filter filter) throws Exception {
		Filter existing = null;

		Board board = this.boardCache.getItem(boardId);
		existing = getFilter(boardId, filterId);
		if (existing == null) {
			throw new ResourceNotFoundException("Filter does not exist: " + Encode.forHtmlContent(filter.getId()));
		}
		copyFilter(filter, existing);
		this.boardTools.saveBoard(board);

		return existing;
	}

	@ApiOperation(value = "Execute existing filter with dynamic field values")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{filterId}/dynamic", method = RequestMethod.POST)
	public @ResponseBody Collection<Card> dynamicFilter(@PathVariable String boardId,
			@RequestParam(required = false) String viewId, @PathVariable String filterId,
			@RequestBody Map<String, Object> fieldMap) throws Exception {

		Board board = this.boardCache.getItem(boardId);
		Filter storedFilter = board.getFilter(filterId);
		if (storedFilter == null) {
			throw new ResourceNotFoundException("Filter does not exist: " + Encode.forHtmlContent(filterId));
		}

		Filter filter = FilterTools.replaceFilterValues(storedFilter, fieldMap);
		View view = board.getView(viewId);

		return queryService.dynamicQuery(boardId, null, filter, view, false);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{filterId}", method = RequestMethod.GET)
	public @ResponseBody Filter getFilter(@PathVariable String boardId, @PathVariable String filterId)
			throws Exception {

		Filter filter = null;
		Board board = this.boardCache.getItem(boardId);
		Map<String, Filter> filters = board.getFilters();
		if (filters == null)
			throw new ResourceNotFoundException("Filter not found, id: " + Encode.forHtmlContent(filterId));

		filter = filters.get(filterId);
		if (filter == null) {
			throw new ResourceNotFoundException("Filter not found, id: " + Encode.forHtmlContent(filterId));
		}
		return filter;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}/conditions", method = RequestMethod.POST)
	public @ResponseBody Condition saveFilterField(@PathVariable String boardId, @PathVariable String filterId,
			@RequestBody Condition filterField) throws Exception {

		Board board = this.boardCache.getItem(boardId);
		Filter filter = getFilter(boardId, filterId);
		Map<String, Condition> conditions = filter.getConditions();
		if (conditions == null) {
			conditions = new HashMap<String, Condition>();
			filter.setConditions(conditions);
		}
		conditions.put(filterField.getFieldName(), filterField);
		this.boardTools.saveBoard(board);
		return filterField;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}/conditions/{fieldId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteFilterField(@PathVariable String boardId, @PathVariable String filterId,
			@PathVariable String fieldId) throws Exception {

		Board board = this.boardCache.getItem(boardId);
		Filter filter = getFilter(boardId, filterId);
		Map<String, Condition> conditions = filter.getConditions();
		if (conditions == null) {
			logger.info("Cannot delete field, Conditions set is already empty");
			return;
		}
		conditions.remove(fieldId);
		this.boardTools.saveBoard(board);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> listFilters(@PathVariable String boardId) throws Exception {

		Map<String, String> result = new HashMap<String, String>();
		Board board = this.boardCache.getItem(boardId);
		Map<String, Filter> filters = board.getFilters();
		if (filters == null)
			return result;
		for (Filter filter : filters.values()) {
			result.put(filter.getId(), filter.getName());
		}
		return result;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{filterId}/execute", method = RequestMethod.GET)
	public @ResponseBody Collection<Card> executeFilter(@PathVariable String boardId,
			@RequestParam(required = false) String viewId, @PathVariable String filterId) throws Exception {

		return queryService.query(boardId, null, filterId, viewId, false);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteFilter(@PathVariable String boardId, @PathVariable String filterId)
			throws Exception {

		if (StringUtils.isBlank(filterId)) {
			return;
		}

		Board board = this.boardCache.getItem(boardId);
		Map<String, Filter> filters = board.getFilters();
		if (filters == null)
			throw new ResourceNotFoundException("Filter not found:" + Encode.forHtmlContent(filterId));

		Filter filter = filters.get(filterId);
		if (filter == null)
			throw new ResourceNotFoundException("Filter not found:" + Encode.forHtmlContent(filterId));

		filters.remove(filter.getId());
		this.boardTools.saveBoard(board);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{filterId}/permissions", method = RequestMethod.GET)
	@ApiOperation(value = "Get Filter Permissions")
	public @ResponseBody Map<String, String> getPermissions(@PathVariable String boardId, @PathVariable String filterId)
			throws Exception {
		Board board = boardCache.getItem(boardId);
		if (board.getFilters() != null) {
			Filter filter = board.getFilters().get(filterId);
			if (filter != null)
				return filter.getPermissions();
		}
		return new HashMap<String, String>();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}/permissions", method = RequestMethod.POST)
	@ApiOperation(value = "Add Filter Permissions")
	public @ResponseBody void addPermissions(@PathVariable String boardId, @PathVariable String filterId,
			@RequestParam(required = false) boolean replace, @RequestBody Map<String, String> permissions)
			throws Exception {

		Board board = this.boardCache.getItem(boardId);
		Filter filter = getFilter(boardId, filterId);

		Map<String, String> curPermissions = filter.getPermissions();
		if (curPermissions == null) {
			curPermissions = new HashMap<String, String>();
			filter.setPermissions(curPermissions);
		}
		if (replace) {
			Iterator<String> properties = curPermissions.keySet().iterator();
			while (properties.hasNext()) {
				String nextProperty = properties.next();
				if (!permissions.containsKey(nextProperty)
					&& !nextProperty.equals("administrators") 
					&& !nextProperty.equals("system")) {
					properties.remove();
				}
			}
		}

		for (Entry<String, String> entry : permissions.entrySet()) {
			curPermissions.put(entry.getKey(), entry.getValue());
		}
		this.boardTools.saveBoard(board);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{filterId}/permissions/{member}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Filter Permission")
	public @ResponseBody void deletePermissions(@PathVariable String boardId, @PathVariable String filterId,
			@PathVariable String member) throws Exception {

		Board board = this.boardCache.getItem(boardId);
		Filter filter = getFilter(boardId, filterId);
		Map<String, String> curPermissions = filter.getPermissions();
		curPermissions.remove(member);
		this.boardTools.saveBoard(board);
	}

	/**
	 * copyFilter copies attributes of the Filter object leaving out the deleted, id
	 * and boardId properties. Name can be changed but the id still will be based on
	 * the original name
	 * 
	 * @param from
	 * @param to
	 * @return Filter
	 */
	public Filter copyFilter(Filter from, Filter to) {
		if (from.getAccess() != null)
			to.setAccess(from.getAccess());

		// from.getBoardId(); - Don't allow change of boardId and deleted flag
		if (from.getConditions() != null)
			to.setConditions(from.getConditions());
		if (StringUtils.isNotEmpty(from.getExpression()))
			to.setExpression(from.getExpression());
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		if (StringUtils.isNotEmpty(from.getOwner()))
			to.setName(from.getName());
		if (from.getPermissions() != null && !from.getPermissions().isEmpty())
			to.setPermissions(from.getPermissions());
		if (StringUtils.isNotEmpty(from.getPhase()))
			to.setPhase(from.getPhase());
		return to;
	}

}
