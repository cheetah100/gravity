/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.gviz.GVGraph;
import nz.devcentre.gravity.gviz.GVNode;
import nz.devcentre.gravity.gviz.GVShape;
import nz.devcentre.gravity.gviz.GVStyle;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Action;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.repository.RuleRepository;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/board/{boardId}/rules")
public class RuleController {
	
	private static final Logger logger = LoggerFactory.getLogger(RuleController.class);
		
	@Autowired 
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	private RuleCache ruleCache;
		
	@Autowired
	BoardsCache boardsCache;
		
	@Autowired
	private RuleRepository ruleRepository;
	
	@Autowired
	private AutomationEngine automationEngine;
	
	@Autowired
	private TimerManager timerManager;
		
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody Rule createRule(@PathVariable String boardId,
										  @RequestBody Rule rule) throws Exception {
		/*
		 * String ruleId = rule.getId(); if (ruleId == null) { ruleId =
		 * IdentifierTools.getIdFromBoardClass(rule); }
		 */
		
		IdentifierTools.newVersion(rule);
	
		rule.addOrUpdate();
		rule.setBoardId(boardId);
		
		//Disable Current Active
		this.disableCurrentActive(boardId, rule.getId());
		rule.setActive(true);
		
		ruleRepository.save(rule);
		this.cacheInvalidationManager.invalidate(RuleCache.TYPE, boardId, rule.getId());
		this.timerManager.activateRule(rule);
		return rule;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{ruleId}", method=RequestMethod.GET)
	public @ResponseBody Rule getRule(@PathVariable String boardId, 
									  @PathVariable String ruleId) throws Exception {
		try {
			Rule item = ruleCache.getItem(boardId, ruleId);
			return item;
		} catch (ResourceNotFoundException e) {
			throw new ResourceNotFoundException("Active version for Rule:-" + ruleId + " Not found");
		}
	}
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{ruleId}/history", method=RequestMethod.GET)
	public @ResponseBody List<Rule> getRuleHistory(@PathVariable String boardId, 
									  @PathVariable String ruleId) throws Exception {
		
		List<Rule> item =this.ruleRepository.findByBoardIdAndId(boardId, ruleId);
		if(item==null || item.isEmpty()) {
			throw new ResourceNotFoundException("Rule:-"+ruleId+" Not found");
		}
		return item;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{ruleId}/execute", method=RequestMethod.GET)
	@ApiOperation(value = "Run an ONDEMAND Rule for a Board", notes = "Rule Execution will run asynchronously,"
			+ "\nIf one occurence of a board and rule execution is in progress "
			+ "\nany newly requested run for the same combination will wait till previous instance is completed")
	public ResponseEntity<Object> executeOnDemandRule(@PathVariable String boardId, 
									  @PathVariable String ruleId) throws Exception {
		
		Rule rule = ruleCache.getItem(boardId, ruleId);
		if (rule==null) {
			throw new ResourceNotFoundException(ruleId+" not found");
		}
		if (!rule.getRuleType().equals(RuleType.ONDEMAND) && !rule.getRuleType().equals(RuleType.SCHEDULED)) {
			throw new ValidationException("Rule is not On Demand or Scheduled");
		}
		
		Map<String,Object> inputContext = new HashMap<>();
		inputContext.put("ruleid", ruleId);
		inputContext.put("boardid", boardId);
		
		Map<String, Object> context = this.automationEngine.executeOnDemandScriptAction(rule, inputContext);
		
		if( context.containsKey("response")) {
			return ResponseEntity.ok(context.get("response"));
		} else {
			return ResponseEntity.ok("ok");
		}
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{ruleId}/execute", method=RequestMethod.POST)
	@ApiOperation(value = "Run an ONDEMAND Rule for a Board", notes = "Rule Execution will run asynchronously,"
			+ "\nIf one occurence of a board and rule execution is in progress "
			+ "\nany newly requested run for the same combination will wait till previous instance is completed")
	public ResponseEntity<Object> executeOnDemandRule(@PathVariable String boardId, 
									  @PathVariable String ruleId,
									  HttpServletRequest request) throws Exception {
		
		Rule rule = ruleCache.getItem(boardId, ruleId);
		if (rule==null) {
			throw new ResourceNotFoundException(ruleId+" not found");
		}
		if (!rule.getRuleType().equals(RuleType.ONDEMAND) && !rule.getRuleType().equals(RuleType.SCHEDULED)) {
			throw new ValidationException("Rule is not On Demand or Scheduled");
		}
		
		Map<String,Object> inputContext = new HashMap<>();
		BufferedReader reader = request.getReader();
		String collect = reader.lines().collect(Collectors.joining());
		inputContext.put("request", collect);
		inputContext.put("ruleid", ruleId);
		inputContext.put("boardid", boardId);
		
		Map<String, Object> context = this.automationEngine.executeOnDemandScriptAction(rule, inputContext);
		
		if( context.containsKey("response")) {
			return ResponseEntity.ok(context.get("response"));
		} else {
			return ResponseEntity.ok(context);
		}
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{ruleId}", method=RequestMethod.PUT)
	public @ResponseBody Rule updateRule(@PathVariable String boardId,
			 							 @PathVariable String ruleId,
										 @RequestBody Rule rule) throws Exception {
		
		List<Rule> ruleHistory = this.getRuleHistory(boardId, ruleId);
		if(ruleHistory==null || ruleHistory.isEmpty()) {
			throw new ResourceNotFoundException("Rule "+ruleId+ " Not found for Board "+boardId);
		}
		rule.setId(ruleId);
		IdentifierTools.newVersion(rule);
		rule.addOrUpdate();
		rule.setBoardId(boardId);
		if(rule.isActive()) {
			this.disableCurrentActive(boardId, rule.getId());
		}
		ruleRepository.save(rule);
		this.cacheInvalidationManager.invalidate(RuleCache.TYPE, boardId, rule.getId());
		this.timerManager.activateRule(rule);
		return rule;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{ruleId}/activate/{version}", method=RequestMethod.GET)
	public @ResponseBody Rule activateVersion(@PathVariable String boardId,
			 							 @PathVariable String ruleId,
										 @PathVariable String version) throws Exception {
		
		
		this.disableCurrentActive(boardId, ruleId);
		Rule rule = this.ruleRepository.findByBoardIdAndIdAndVersion(boardId, ruleId, version);
		if(rule==null) {
			throw new ResourceNotFoundException("Rule "+ruleId+ "with version "+version+" Not found for Board "+boardId);
		}
		//If provided Version is already Active
		if(rule.isActive()) {
			return rule;
		}
		
		rule.setActive(true);
		//Disable Current Active Version (If Any)
		this.disableCurrentActive(boardId, ruleId);
		ruleRepository.save(rule);
		this.cacheInvalidationManager.invalidate(RuleCache.TYPE, boardId, rule.getId());
		return rule;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listRules(@PathVariable String boardId,
			@RequestParam(defaultValue = "false") boolean all) throws Exception {
		if(all) {
			return this.ruleCache.getAllRulesForBoard(boardId);
		}
		Map<String, String> list = ruleCache.list(boardId);
		return list;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/full", method=RequestMethod.GET)
	public @ResponseBody List<Rule> listFullRules(@PathVariable String boardId) throws Exception {
		//return this.ruleRepository.findByBoardId(boardId);
		return this.ruleRepository.findByBoardIdAndActiveTrue(boardId);
	}
		
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{ruleId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteRule(@PathVariable String boardId, 
			@PathVariable String ruleId) throws Exception {
		
		if( StringUtils.isBlank(ruleId)){
			return;
		}
		
		Rule rule = getRule(boardId, ruleId);
		this.disableCurrentActive(boardId, rule.getId());
		this.cacheInvalidationManager.invalidate(RuleCache.TYPE, boardId, ruleId);
		this.timerManager.disactivateRule(rule);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")	
	@RequestMapping(value = "/processgraph", method=RequestMethod.GET)
	public String processGraph(@PathVariable String boardId, Model model) throws Exception {
		
		Map<String, String> rules = ruleCache.list(boardId);

		// Construct a GVGraph
		GVGraph graph = new GVGraph(boardId);
		
		// Loop over rules
		for( String ruleId : rules.keySet()){
			Rule rule = ruleCache.getItem(boardId, ruleId);
			
			GVNode node = new GVNode(rule.getId());
			node.setColor("yellow");
			node.setStyle(GVStyle.filled);
			node.setShape(GVShape.hexagon);
			graph.addNode(node);

			if(rule.getAutomationConditions()!=null){
				for(Condition condition : rule.getAutomationConditions().values()){
					String nodeName = condition.getFieldName() + 
							"-" + condition.getOperation() + "-" + condition.getValue();
					
					if( condition.getConditionType().equals(ConditionType.TASK)){
						nodeName = condition.getFieldName();
					}
					
					GVNode conditionNode = graph.getNode(nodeName);
					if(conditionNode==null){
						conditionNode = new GVNode(nodeName);
						conditionNode.setStyle(GVStyle.filled);
						
						if( condition.getConditionType().equals(ConditionType.PROPERTY)){
							conditionNode.setShape(GVShape.oval);
							conditionNode.setColor("green");
						}
						if( condition.getConditionType().equals(ConditionType.TASK)){
							conditionNode.setShape(GVShape.octagon);
							conditionNode.setColor("cyan");
						}
						if( condition.getConditionType().equals(ConditionType.PHASE)){
							conditionNode.setShape(GVShape.box);
							conditionNode.setColor("purple");
						}
						graph.addNode(conditionNode);
					}
					graph.linkNodes(conditionNode.getName(),node.getName());
				}
			}

			if(rule.getTaskConditions()!=null){
				for(Condition condition : rule.getTaskConditions().values()){
					String nodeName = condition.getFieldName() + 
							"-" + condition.getOperation() + "-" + condition.getValue();
					
					if( condition.getConditionType().equals(ConditionType.TASK)){
						nodeName = condition.getFieldName();
					}
					
					GVNode conditionNode = graph.getNode(nodeName);
					if(conditionNode==null){
						conditionNode = new GVNode(nodeName);
						conditionNode.setStyle(GVStyle.filled);
						
						if( condition.getConditionType().equals(ConditionType.PROPERTY)){
							conditionNode.setShape(GVShape.oval);
							conditionNode.setColor("green");
						}
						if( condition.getConditionType().equals(ConditionType.TASK)){
							conditionNode.setShape(GVShape.octagon);
							conditionNode.setColor("cyan");
						}
						if( condition.getConditionType().equals(ConditionType.PHASE)){
							conditionNode.setShape(GVShape.box);
							conditionNode.setColor("purple");
						}
						graph.addNode(conditionNode);
					}
					graph.linkNodes(conditionNode.getName(),node.getName(),"blue");
				}
			}

			
			if( rule.getActions()!=null){
				for( Action action : rule.getActions().values()){
					
					/*
					// Is The Action a Complete Task?
					if( action.getType().equals("execute") && action.getConfigField("method").equals("completeTask")){
						
						String taskname = (action.getConfigField("properties")).get("taskname");
						String nodeName = taskname + "-EQUALTO-" + taskname;
						
						GVNode childNode = graph.getNode(nodeName);
						if(childNode==null){
							childNode = new GVNode(nodeName);
							childNode.setStyle(GVStyle.filled);
							childNode.setColor("cyan");
							childNode.setShape(GVShape.octagon);
							graph.addNode(childNode);
						}
						graph.linkNodes(node.getName(),childNode.getName());
					}
					
					// Is The Action a Move to Phase?

					if( action.getType().equals("execute") && action.getMethod().equals("moveCard")){
						
						String destination = action.getProperties().get("destination");
						String nodeName = "phase-EQUALTO-" + destination;
						
						GVNode childNode = graph.getNode(nodeName);
						if(childNode==null){
							childNode = new GVNode(nodeName);
							childNode.setStyle(GVStyle.filled);
							childNode.setColor("purple");
							childNode.setShape(GVShape.box);						
							graph.addNode(childNode);
						}
						graph.linkNodes(node.getName(),childNode.getName());
					}
					
					// Is The Action a Store Propperty?
					
					if( action.getType().equals("execute") && action.getMethod().equals("updateValue")){
						
						String propertyName = action.getProperties().keySet().iterator().next();
						String fieldName = action.getProperties().get(propertyName);
						String nodeName = fieldName + "-NOTNULL-" + fieldName;
						
						GVNode childNode = graph.getNode(nodeName);
						if(childNode==null){
							childNode = new GVNode(nodeName);
							childNode.setStyle(GVStyle.filled);
							childNode.setColor("green");
							childNode.setShape(GVShape.oval);						
							graph.addNode(childNode);
						}
						graph.linkNodes(node.getName(),childNode.getName());
					}
					
					// Is The Action a Persist?
					if( action.getType().equals("persist") ){
						
						for( String fieldName : action.getParameters()){
							String nodeName = fieldName + "-NOTNULL-" + fieldName;						
							GVNode childNode = graph.getNode(nodeName);
							if(childNode==null){
								childNode = new GVNode(nodeName);
								childNode.setStyle(GVStyle.filled);
								childNode.setColor("green");
								childNode.setShape(GVShape.oval);						
								graph.addNode(childNode);
							}
							graph.linkNodes(node.getName(),childNode.getName());
						}
					}
					*/
				}
			}
		}
		model.addAttribute("graph", graph);
		return "graph";
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/invalidate", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> invalidateRuleCache(@PathVariable String boardId) 
														throws Exception {
		
		List<Rule> boardRules = ruleRepository.findByBoardId(boardId);
		Map<String,String> result = new HashMap<String, String>();
		try {
			for (Rule rule : boardRules) {
				String ruleId = rule.getId();
				try{
					logger.info("Invalidating Rule Cache for board: " + boardId + ":" + ruleId);
					this.cacheInvalidationManager.invalidate(RuleCache.TYPE, boardId, ruleId);
					result.put(ruleId, "true");
				}catch(Exception re){
					logger.info("Something went wrong while clearing Rule Cache for " + boardId );
					result.put(ruleId, "false");
				}
			}
		} finally {
			logger.info("Rule Cache cleared for " + boardId );
		}
		return result;
	}
	
	@ApiOperation(value="Process Model")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/model", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> getModel(@PathVariable String boardId) throws Exception {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		Map<String, Object> nodes = new HashMap<String,Object>();
		Map<String, Object> links = new HashMap<String,Object>();
		
		// Add Phases (mentioned in rules)
		
		// Add Fields (mentioned in rules)
		
		// Add Rules / Tasks

		
		Map<String, String> rules = ruleCache.list(boardId);
		for(Entry<String,String> ruleEntry: rules.entrySet()){
			
			Rule rule = this.getRule(boardId, ruleEntry.getKey());
			nodes.put(rule.getId(), getNode(rule.getId(),rule.getName(),1,100));
			
			// Loop over Task Conditions
			if(rule.getTaskConditions()!=null){
				Map<String, Condition> taskConditions = rule.getTaskConditions();
				for( Condition condition : taskConditions.values()){
					nodes.put(condition.getFieldName(), getNode(condition.getFieldName(),condition.getFieldName(),2,50));
					links.put(rule.getId() + "-" + condition.getFieldName(), getLink(rule.getId(),condition.getFieldName(),1));
				}
			}
			
			// Loop over Automation Conditions
			if(rule.getAutomationConditions()!=null){
				Map<String, Condition> automationConditions = rule.getAutomationConditions();
				for( Condition condition : automationConditions.values()){
					nodes.put(condition.getFieldName(), getNode(condition.getFieldName(),condition.getFieldName(),2,50));
					links.put(rule.getId() + "-" + condition.getFieldName(), getLink(rule.getId(),condition.getFieldName(),1));
				}
			}
		
			// Loop over Completion Conditions?
			if(rule.getCompletionConditions()!=null){
				Map<String, Condition> completionConditions = rule.getCompletionConditions();
				for( Condition condition : completionConditions.values()){
					nodes.put(condition.getFieldName(), getNode(condition.getFieldName(),condition.getFieldName(),2,50));
					links.put(rule.getId() + "-" + condition.getFieldName(), getLink(rule.getId(),condition.getFieldName(),1));
	}
}
		}
		
		returnMap.put("nodes", nodes.values());
		returnMap.put("links", links.values());
		
		return returnMap;
	}
	
	private Map<String,Object> getNode( String id, String title, Integer group, Integer cards){
		Map<String,Object> node = new HashMap<String,Object>();
		node.put("id", id);
		node.put("title", title);
		node.put("group", group);
		node.put("cards", cards);
		return node;
	}

	private Map<String,Object> getLink( String source, String target, Integer value){
		Map<String,Object> link = new HashMap<String,Object>();
		link.put("source", source);
		link.put("target", target);
		link.put("value", value);
		return link;
	}
	
	private void disableCurrentActive(String boardId, String ruleId) {
		Rule currentActiveRule = this.ruleRepository.findByBoardIdAndIdAndActiveTrue(boardId, ruleId);
		if (currentActiveRule != null) {
			currentActiveRule.setActive(false);
			currentActiveRule.addOrUpdate();
			ruleRepository.save(currentActiveRule);
		}

	}
	
}
