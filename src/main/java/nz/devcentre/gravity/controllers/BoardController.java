/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AbstractNamedModelClass;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardSequence;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardHistoryStat;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.KeyMechanism;
import nz.devcentre.gravity.model.Lock;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.transfer.BoardData;
import nz.devcentre.gravity.repository.BoardRepository;
import nz.devcentre.gravity.repository.BoardSequenceRepository;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.repository.UserRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.FilterTools;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/board")
@ApiModel(value = "Board")
public class BoardController {

	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

	@Autowired
	private BoardsCache boardCache;

	@Autowired
	private TeamCache teamCache;

	@Autowired
	private CardLockInterface cardsLockCache;

	@Autowired
	private QueryService queryService;

	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;

	@Autowired
	private CardService cardService;

	@Autowired
	private SecurityTool securityTool;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private BoardRepository boardRepository;

	@Autowired
	private BoardService boardService;

	@Autowired
	private BoardSequenceRepository boardSequenceRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CardEventRepository eventRepository;

	@Autowired
	private CardHistoryController cardHistoryController;

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board")
	public @ResponseBody Board getBoard(@PathVariable String boardId) throws Exception {
		Board board = boardCache.getItem(boardId);
		return board;
	}

	private String disableCurrentActive(String boardId) {
		Board currentActive = this.boardRepository.findByIdAndActiveTrue(boardId);
		if (currentActive != null) {
			currentActive.setActive(false);
			currentActive.addOrUpdate();
			this.boardRepository.save(currentActive);
			return currentActive.getUniqueid();
		}
		return null;
	}

	private void activateBoard(String uniqueBoardId) throws ResourceNotFoundException {
		
		Board targetBoard = this.boardRepository.findOne(uniqueBoardId);
		
		if(targetBoard==null) {
			throw new ResourceNotFoundException();
		}
		
		Board currentActive = this.boardRepository.findByIdAndActiveTrue(targetBoard.getId());
		if(currentActive!=null && targetBoard.getUniqueid().equals(currentActive.getId())) {
			currentActive.setActive(false);
			currentActive.addOrUpdate();
			this.boardRepository.save(currentActive);
		}
		
		targetBoard.setActive(false);
		targetBoard.addOrUpdate();
		this.boardRepository.save(currentActive);
	}

	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/fieldvalues/{field}", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board field values")
	public @ResponseBody Set<Object> getFieldValues(@PathVariable String boardId, @PathVariable String field,
			@RequestParam(required = false) String filter, @RequestParam(required = false) boolean allcards)
			throws Exception {

		Set<Object> result = new TreeSet<Object>();
		Collection<Card> query = queryService.query(boardId, null, filter, null, allcards);
		for (Card card : query) {
			Object fieldValue = card.getFields().get(field);
			if (fieldValue != null) {
				result.add(fieldValue);
			}
		}
		return result;
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ApiOperation(value = "Create Board")
	public @ResponseBody Board createBoard(@RequestBody Board board) throws Exception {

		// Going to create the intended new ID
		IdentifierTools.newVersion(board);
		
		try { 
			Board existing = this.boardCache.getItem(board.getId());
			if( !this.securityTool.isAuthorised(existing.getPermissions(), "ADMIN") ) {
				throw new AccessDeniedException("User does not have permissions to modify Board");
			}
		} catch( ResourceNotFoundException e) {
			// Fine
		}
		
		board.addOrUpdate();
		
		// Ensure that the current user is assigned as the owner of the new board.
		// By default also add the administrators group as a owner.
		board.setPermissions(this.securityTool.initPermissions(board.getPermissions()));
		board.setActive(true);

		if( board.getPhases()==null || board.getPhases().size()==0) {
			Phase phase = new Phase();
			phase.setId("current");
			phase.setName("Current");
			phase.setInvisible(false);
			phase.setIndex(1);
			Map<String,Phase> phases = new HashMap<>();
			board.setPhases(phases);
			phases.put("current", phase);			
			board.setPhases(phases);
		}
		
		if( board.getKeyMechanism()==null) {
			if( BoardType.OPTIONLIST.equals(board.getBoardType())) {
				board.setKeyMechanism(KeyMechanism.TITLE);
				if(board.getTitleField()==null) {
					board.setTitleField("name");
				}
			} else {
				board.setKeyMechanism(KeyMechanism.UUID);
			}
		}
		
		// This is required as the queryWithFilter method fails if the ids are not set
		// on the phases
		setPhaseIds(board.getPhases());

		this.boardService.saveBoard(board);

		if (!BoardType.REMOTE.equals(board.getBoardType()) && !mongoTemplate.getDb().collectionExists(board.getId())) {
			BasicDBObject o = new BasicDBObject();
			mongoTemplate.getDb().createCollection(board.getId(), o);
		}

		this.boardService.checkIndexesForBoard(board.getId());

		return board;
	}

	protected void setPhaseIds(Map<String, Phase> objects) throws Exception {
		if (objects == null)
			return;
		for (String key : objects.keySet()) {
			AbstractNamedModelClass object = (AbstractNamedModelClass) objects.get(key);
			object.setId(key);
		}
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}", method = RequestMethod.PUT)
	@ApiOperation(value = "Update Board")
	public @ResponseBody Board updateBoard(@RequestBody Board board, @PathVariable String boardId) throws Exception {
		
		if(board.getPermissions()==null || board.getPermissions().isEmpty()) {
			Board existingBoard = this.boardCache.getItem(boardId);
			board.setPermissions(existingBoard.getPermissions());
		}
		
		Board saveBoard = boardService.saveBoard(board);
		return saveBoard;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/fields", method = RequestMethod.POST)
	@ApiOperation(value = "Add / Update Field")
	public @ResponseBody TemplateField updateField(@RequestBody TemplateField field, @PathVariable String boardId) 
			throws Exception {
		Board board = this.boardCache.getItem(boardId);
		board.getFields().put(field.getId(), field);
		Board saveBoard = boardService.saveBoard(board);
		return saveBoard.getField(field.getId());
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	@ApiOperation(value = "List Boards")
	public @ResponseBody Map<String, String> listBoards() throws Exception {
		
		Map<String, String> map = this.boardCache.list();
		this.boardService.filterMap(map, "READ,WRITE,ADMIN");
		return map;
		//return securityTool.filterMap(map, "READ,WRITE,ADMIN", boardCache);

	}

	@RequestMapping(value = "/{boardId}/status", method = RequestMethod.GET)
	@ApiOperation(value = "Board Access")
	public @ResponseBody boolean IsUserAccess(@PathVariable String boardId,
			@RequestParam(required = false) String permission) throws Exception {
		
		return this.boardService.isUserAccess(boardId, permission);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/lock/{cardId}", method = RequestMethod.GET)
	@ApiOperation(value = "Lock Card")
	public @ResponseBody boolean lock(@PathVariable String boardId, @PathVariable String cardId) {
		return cardsLockCache.lock(boardId, cardId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/unlock/{cardId}", method = RequestMethod.GET)
	@ApiOperation(value = "Unlock Card")
	public @ResponseBody boolean unlock(@PathVariable String boardId, @PathVariable String cardId) {
		return cardsLockCache.unlock(boardId, cardId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/getlock/{cardId}", method = RequestMethod.GET)
	@ApiOperation(value = "Get Lock Detail for Card")
	public @ResponseBody Lock getLockDetails(@PathVariable String boardId, @PathVariable String cardId) {
		Lock lock = cardsLockCache.getLock(boardId, cardId);
		return lock;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/getlocks", method = RequestMethod.GET)
	@ApiOperation(value = "Get All Locks for Board")
	public @ResponseBody Map<String, Lock> getLocks(@PathVariable String boardId) {
		return cardsLockCache.getLocks(boardId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/deleteallcards", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete All Cards")
	public @ResponseBody void deleteAllCards(@PathVariable String boardId) throws Exception {

		// Remove all cards from the board
		mongoTemplate.remove(new Query(), boardId);

		// TODO Remove card history
		cardHistoryController.deleteAllHistoryForBoard(boardId);
	}

	/**
	 * 
	 * The purpose of the method is to search for a single record which matches the
	 * supplied criteria. If a single card is not found it will return null. If a
	 * single card it found it is returned.
	 * 
	 * @param boardId
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/search/forone", method = RequestMethod.POST)
	@ApiOperation(value = "Search for One Card")
	public @ResponseBody Card searchForOne(@PathVariable String boardId, @RequestBody Map<String, String> map)
			throws Exception {
		Collection<Card> results = this.searchWithMap(boardId, map);
		if (results.size() != 1) {
			return null;
		}
		return results.iterator().next();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/search/bymap", method = RequestMethod.POST)
	@ApiOperation(value = "Search for Cards via Map")
	public @ResponseBody Collection<Card> searchWithMap(@PathVariable String boardId,
			@RequestBody Map<String, String> map) throws Exception {
		Board board = boardCache.getItem(boardId);
		String view = null;
		String order = null;
		boolean descending = false;
		
		if( map.containsKey("view")) {
			view = map.get("view");
			map.remove("view");
		}

		if( map.containsKey("order")) {
			order = map.get("order");
			map.remove("order");
		}
		
		if( map.containsKey("descending")) {
			descending = Boolean.parseBoolean(map.get("descending"));
			map.remove("descending");
		}

		Filter filter = FilterTools.createMapFilter(map, board);
		return search(boardId, filter, view, null, order, false, descending);
	}

	public Collection<Card> searchWithMapObj(String boardId, Map<String, Object> map) throws Exception {
		Map<String, Condition> conditions = new HashMap<String, Condition>();
		for (Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() instanceof String) {
				conditions.put(entry.getKey(), new Condition(entry.getKey(),Operation.EQUALTO,(String) entry.getValue()));
			} else if (entry.getValue() instanceof List) {
				List values = (List) entry.getValue();
				for (Object item : values) {
					conditions.put(entry.getKey(), new Condition(entry.getKey(),Operation.EQUALTO,(String) item));
				}
			}
		}

		if (conditions.size() == 0) {
			throw new Exception("Insufficient Criteria for Search");
		}

		Filter filter = new Filter();
		filter.setConditions(conditions);
		return search(boardId, filter, "all", null, null, false, false);
	}

	// Permissions enforced within method
	@RequestMapping(value = "/{boardId}/search", method = RequestMethod.POST)
	@ApiOperation(value = "Search Board")
	public @ResponseBody Collection<Card> search(@PathVariable String boardId, @RequestBody Filter filter,
			@RequestParam(required = false) String view, @RequestParam(required = false) String filterId,
			@RequestParam(required = false) String order, @RequestParam(required = false) boolean allphases,
			@RequestParam(required = false) boolean descending) throws Exception {

		Board board = boardCache.getItem(boardId);

		if (!securityTool.isAuthorizedViewAndFilter(view, filterId, board, "READ,WRITE,ADMIN")) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Search Cards " + boardId + " filter " + filter);
		}

		Filter storedFilter = board.getFilter(filterId);  
		
		String cardId = queryService.resolveDynamicFilter(boardId, filter, storedFilter);

		if (cardId != null) {
			Card card = cardService.getCard(boardId, cardId);
			Collection<Card> returnCollection = new ArrayList<Card>();
			returnCollection.add(card);
			return returnCollection;
		}

		Collection<Card> result = null;

		if (filterId != null) {

			if (storedFilter != null) {
				if (storedFilter.getConditions() != null) {
					filter.getConditions().putAll(storedFilter.getConditions());
				}
				if (storedFilter.getPhase() != null && filter.getPhase() == null) {
					filter.setPhase(storedFilter.getPhase());
				}
			} else {
				logger.warn("No filter conditions found for filter " + boardId + "." + filterId);
			}
		}

		View viewObj = board.getView(view);
		result = queryService.dynamicQuery(boardId, null, filter, viewObj, allphases);
		
		View viewObject = board.getView(view);
		return CardTools.orderCards(result, order, descending, viewObject, board);

	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/search/{field}/{operation}/{value}", method = RequestMethod.GET)
	@ApiOperation(value = "Search Board")
	public @ResponseBody Map<String, String> search(@PathVariable String boardId, @PathVariable String field,
			@PathVariable Operation operation, @PathVariable String value,
			@RequestParam(required = false) String titleField, @RequestParam(required = false) boolean allcards)
			throws Exception {

		if (titleField == null) {
			titleField = "metadata.phase";
		}

		Map<String, String> basicQuery;
		if ("undefined".equals(field)) {
			basicQuery = new HashMap<String, String>();
		} else {
			basicQuery = queryService.search(boardId, field, operation, value, titleField, allcards);
		}
		return basicQuery;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/search/{field}/{operation}/{value}/cards", method = RequestMethod.GET)
	@ApiOperation(value = "Search Board")
	public @ResponseBody Collection<Card> searchCards(@PathVariable String boardId, @PathVariable String field,
			@PathVariable Operation operation, @PathVariable String value,
			@RequestParam(required = false) boolean allcards) throws Exception {

		if ("undefined".equals(field)) {
			return null;
		} else {
			Filter filter = FilterTools.createSimpleFilter(field, operation, value);
			return queryService.dynamicQuery(boardId, null, filter, null, allcards);
		}
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Board [WARNING: Use with extreme caution]")
	public @ResponseBody void deleteBoard(@PathVariable String boardId,
			@RequestParam(required = false) boolean removeCards) throws Exception {
		if (StringUtils.isEmpty(boardId)) {
			return;
		}

		// Disactivate all for this board
		this.boardService.disactivateBoard(boardId);

		if (removeCards) {
			mongoTemplate.dropCollection(boardId);
		}
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/fields/{fieldId}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Field")
	public @ResponseBody void deleteField(@PathVariable String boardId, @PathVariable String fieldId ) throws Exception {
		if (StringUtils.isEmpty(boardId) || StringUtils.isEmpty(fieldId)) {
			return;
		}
		Board board = this.boardCache.getItem(boardId);
		board.getFields().remove(fieldId);
		this.boardService.saveBoard(board);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/permissions", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board Permissions")
	public @ResponseBody Map<String, String> getPermissions(@PathVariable String boardId) throws Exception {
		Board board = boardCache.getItem(boardId);
		return board.getPermissions();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/permissions", method = RequestMethod.POST)
	@ApiOperation(value = "Add Permissions")
	public @ResponseBody void addPermissions(@PathVariable String boardId,
			@RequestParam(required = false) boolean replace, @RequestBody Map<String, String> permissions)
			throws Exception {

		Board board = boardCache.getItem(boardId);
		if (board == null) {
			throw new ResourceNotFoundException("Board not found, id:" + Encode.forHtmlContent(boardId));
		}

		Map<String, String> curPermissions = board.getPermissions();
		if (curPermissions == null) {
			curPermissions = new HashMap<String, String>();
			board.setPermissions(curPermissions);
		}
		if (replace) {
			Iterator<String> properties = curPermissions.keySet().iterator();
			while (properties.hasNext()) {
				String nextProperty = properties.next();
				if ( !permissions.containsKey(nextProperty)
						&& !nextProperty.equals("administrators") 
						&& !nextProperty.equals("system")) {
					properties.remove();
				}
			}
		}

		for (Entry<String, String> entry : permissions.entrySet()) {
			curPermissions.put(entry.getKey(), entry.getValue());
		}

		// Disable Current Active
		this.disableCurrentActive(boardId);

		try {
			board.setPermissions(curPermissions);
			board.setVersion(null);
			IdentifierTools.newVersion(board);
			board.setActive(true);
			board.addOrUpdate();
			this.boardService.saveBoard(board);
		} catch( Exception e ){
			
		}
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/permissions/{member}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete Board Permission")
	public @ResponseBody void deletePermissions(@PathVariable String boardId, @PathVariable String member)
			throws Exception {

		Board board = getBoard(boardId);
		Map<String, String> curPermissions = board.getRootPermissions();
		curPermissions.remove(member);
		this.boardService.saveBoard(board);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ROLLUP,READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/users", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board Users")
	public @ResponseBody Map<String, User> getBoardUsers(@PathVariable String boardId,
			@RequestParam(required = false) String permission) throws Exception {

		logger.info("Getting Users for Board: " + boardId);

		Board board = boardCache.getItem(boardId);
		Map<String, String> permissions = board.getPermissions();

		Map<String, User> userMap = new HashMap<String, User>();
		for (String id : permissions.keySet()) {

			String rolePermission = permissions.get(id);
			if (permission != null && !rolePermission.equals(permission)) {
				logger.info("Skipping User: " + id + " assigned permission is " + rolePermission);
				continue;
			}

			logger.info("Getting Users in Team: " + id);
			try {
				Team team = teamCache.getItem(id);

				logger.info("Getting Users in Team: " + id + ":" + team.getMembers().size());
				Set<String> keys = team.getMembers().keySet();
				for (String userid : keys) {
					try {
						User user = userRepository.findById(userid);
						if (user != null) {
							userMap.put(userid, user);
						}
					} catch (Exception e) {
						logger.info("Team " + id + " member " + userid + " -- Cannot lookup User " + userid);
					}

				}
			} catch (ResourceNotFoundException e) {
				try {
					User user = userRepository.findById(id);
					if (user != null) {
						userMap.put(id, user);
					}
				} catch (ResourceNotFoundException ignore) {

				}
			} catch (Exception e) {
				logger.trace("Some exception", e);
			}
		}
		return userMap;
	}

	/**
	 * Get History for Board based on Dates
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/history", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board History")
	public @ResponseBody Map<String, CardEvent> getHistory(@PathVariable String boardId,
			@RequestParam(value = "category", required = false) String category,
			@RequestParam(value = "detail", required = false) String detail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before) throws Exception {

		Map<String, CardEvent> result = null;
		result = getHistoryAsMap(category, detail, after, before, boardId);
		return result;
	}

	
	/**
	 * Get History for Board based on Dates with Injected OCM
	 */
	public Map<String, CardEvent> getHistoryAsMap(String category, String detail, String after, String before,
			String scope) throws Exception {

		List<CardEvent> events = getHistoryList(category, detail, after, before, scope);
		Map<String, CardEvent> list = new HashMap<String, CardEvent>();
		for (CardEvent event : events) {
			list.put(event.getCard(), event);
		}

		return list;
	}

	public List<CardEvent> getHistoryList(String category, String detail, String after, String before, String scope)
			throws Exception {

		Date from = queryService.decodeShortDate(after);
		Date to = queryService.decodeShortDate(before);
		List<CardEvent> events = eventRepository.findByBoardAndOccuredTimeBetweenAndCategoryOrDetail(scope, from, to,
				category, detail);
		return events;
	}

	public Map<String, CardEvent> getHistoryEvent(String boardId, String phaseId, String cardId, String category,
			String detail) throws Exception {

		List<CardEvent> events = eventRepository.findByBoardAndPhaseAndCardAndCategoryOrDetail(boardId, phaseId, cardId,
				category, detail);

		if (logger.isDebugEnabled())
			logger.debug("Setting Scope: " + boardId + "," + phaseId + "," + cardId);

		Map<String, CardEvent> map = new HashMap<String, CardEvent>();
		for (CardEvent event : events) {
			map.put(event.getCard(), event);
		}
		return map;
	}

	/**
	 * Get History for Board based on Dates
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/statsstart", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board Statistics By Start Time")
	public @ResponseBody Map<String, CardHistoryStat> getStatsByStart(@PathVariable String boardId,
			@RequestParam(value = "start", required = false) String start,
			@RequestParam(value = "startdetail", required = false) String startdetail,
			@RequestParam(value = "end", required = false) String end,
			@RequestParam(value = "enddetail", required = false) String enddetail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before,
			@RequestParam(value = "view", required = false) String view) throws Exception {

		Map<String, CardHistoryStat> returnList = new HashMap<String, CardHistoryStat>();

		Map<String, CardEvent> startList = this.getHistory(boardId, start, startdetail, after, before);

		for (String cardId : startList.keySet()) {
			CardEvent startEvent = startList.get(cardId);
			Card card = cardService.getCard(startEvent.getBoard(), startEvent.getCard(), view);

			CardHistoryStat stat = new CardHistoryStat();
			stat.setCardId(cardId);
			stat.setStartTime(startEvent.getOccuredTime());
			stat.setCard(card);

			Map<String, CardEvent> historyEvents = getHistoryEvent(startEvent.getBoard(), startEvent.getPhase(),
					startEvent.getCard(), end, enddetail);

			if (historyEvents.size() > 0) {
				CardEvent endEvent = historyEvents.values().iterator().next();
				stat.setEndTime(endEvent.getOccuredTime());
			}

			returnList.put(cardId, stat);
		}

		logger.info("Complete with :" + returnList.size());

		return returnList;
	}

	/**
	 * Get History for Board based on Dates - based on End
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/statsend", method = RequestMethod.GET)
	@ApiOperation(value = "Get Board Hstory based on End Time")
	public @ResponseBody Map<String, CardHistoryStat> getStatistics(@PathVariable String boardId,
			@RequestParam(value = "start", required = false) String start,
			@RequestParam(value = "startdetail", required = false) String startdetail,
			@RequestParam(value = "end", required = false) String end,
			@RequestParam(value = "enddetail", required = false) String enddetail,
			@RequestParam(value = "after", required = false) String after,
			@RequestParam(value = "before", required = false) String before,
			@RequestParam(value = "view", required = false) String view) throws Exception {

		Map<String, CardHistoryStat> returnList = new HashMap<String, CardHistoryStat>();
		Map<String, CardEvent> endList = this.getHistory(boardId, end, enddetail, after, before);

		for (String cardId : endList.keySet()) {
			CardEvent endEvent = endList.get(cardId);
			Card card = cardService.getCard(endEvent.getBoard(), endEvent.getCard(), view);

			CardHistoryStat stat = new CardHistoryStat();
			stat.setCardId(cardId);
			stat.setEndTime(endEvent.getOccuredTime());
			stat.setCard(card);

			Map<String, CardEvent> historyEvents = getHistoryEvent(endEvent.getBoard(), endEvent.getPhase(),
					endEvent.getCard(), start, startdetail);

			if (historyEvents.size() > 0) {
				CardEvent startEvent = historyEvents.values().iterator().next();
				stat.setStartTime(startEvent.getOccuredTime());
			}

			returnList.put(cardId, stat);
		}

		logger.info("Complete with :" + returnList.size());

		return returnList;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/id/{id}", method = RequestMethod.GET)
	@ApiOperation(value = "Get Current ID")
	public @ResponseBody Long getCurrentId(@PathVariable String boardId, @PathVariable String id) throws Exception {

		if (StringUtils.isEmpty(boardId)) {
			throw new ResourceNotFoundException();
		}

		BoardSequence boardSequence = this.boardSequenceRepository.findByBoardidAndName(boardId, id);

		if (boardSequence == null) {
			throw new ResourceNotFoundException();
		}

		return boardSequence.getValue();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/id", method = RequestMethod.POST)
	@ApiOperation(value = "Set Board ID Value")
	@ResponseStatus(HttpStatus.CREATED)
	public long setId(@PathVariable String boardId, @RequestBody BoardData boardData)
			throws Exception {

		if (StringUtils.isEmpty(boardId) || ( boardData.getValue() <= 0L )){
			new ResourceNotFoundException();  // this should be 400 instead of 404
		}

		BoardSequence boardSequence = this.boardSequenceRepository.findByBoardidAndName(boardId, boardData.getId());

		if (boardSequence == null) {
			boardSequence = new BoardSequence();
			boardSequence.setBoardid(boardId);
			boardSequence.setName(boardData.getId());
		}
		
		boardSequence.setValue(boardData.getValue());
		boardSequenceRepository.save(boardSequence);
		return boardSequence.getValue();
	}
	
	/**
	 * Specifically for Heatmap Use currently
	 * 
	 * @param boardId
	 * @param fields
	 * @return
	 * @throws Exception
	 */
	public Map<String, List<Card>> getCachedBoard(String boardId, String... fields) throws Exception {

		logger.info("Creating Card Cache on Board: " + boardId);
		for (int f = 0; f < fields.length; f++) {
			if (logger.isDebugEnabled())
				logger.debug("Field " + f + " = " + fields[f]);
		}

		Map<String, List<Card>> result = new HashMap<String, List<Card>>();

		Collection<Card> query = queryService.query(boardId, null, null, null, false);
		Iterator<Card> i = query.iterator();
		while (i.hasNext()) {
			Card card = i.next();
			String index = getIndexFromCard(card, fields);

			if (StringUtils.isNotEmpty(index)) {
				List<Card> currentList = result.get(index);
				if (currentList == null) {
					currentList = new ArrayList<Card>();
					result.put(index, currentList);
					if (logger.isDebugEnabled())
						logger.debug("New List: " + index);
				}
				currentList.add(card);
				if (logger.isDebugEnabled())
					logger.debug("Adding Card to Cache: " + card.toString() + " to " + index);
			} else {
				if (logger.isDebugEnabled())
					logger.debug("Empty Index for Card " + card.toString());
			}
		}

		return result;
	}

	public String getIndexFromCard(Card card, String... fields) {
		StringBuffer buf = new StringBuffer();
		for (String field : fields) {
			Object value = null;
			if (field.equals("id")) {
				value = card.getId();
			} else {
				value = card.getFields().get(field);
			}
			if (value != null) {
				if (value instanceof String) {
					buf.append(value.toString());
				}
				if (value instanceof String[]) {
					String[] values = (String[]) value;
					for (int f = 0; f < values.length; f++) {
						buf.append(value.toString());
					}
				}
			}
		}
		return buf.toString();
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/{fieldId}/unique", method = RequestMethod.GET)
	@ApiOperation(value = "Get Unique Values for a field On the Board")
	public @ResponseBody Set<String> getBoard(@PathVariable String boardId, @PathVariable String fieldId)
			throws Exception {
		return this.cardService.fetchUniqueFieldValues(boardId, fieldId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{boardId}/{fieldId}/distinct", method = RequestMethod.GET)
	@ApiOperation(value = "Get Unique Values for a field On the Board")
	public @ResponseBody List<?> getDistinctFieldsFromBoard(@PathVariable String boardId, @PathVariable String fieldId)
			throws Exception {
		return this.cardService.fetchDistinctFieldValues(boardId, fieldId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@ApiOperation(value = "Get Board References")
	@RequestMapping(value = "/{boardId}/references", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getBoardRefrences(@PathVariable String boardId) throws Exception {
		return this.boardService.getBoardRefrences(boardId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@ApiOperation(value = "Get Configuration Change History of the Board")
	@RequestMapping(value = "/{boardId}/config/history", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> boardConfigHistory(@PathVariable String boardId) {
		List<Map<String, Object>> result = new LinkedList<Map<String, Object>>();
		List<Board> boards = this.boardRepository.findById(boardId);
		for (Board board : boards) {
			Map<String, Object> currentEntry = new LinkedHashMap<>();
			currentEntry.put("version", board.getVersion());
			currentEntry.put("active", board.isActive());
			currentEntry.put("metadata", board.getMetadata());
			result.add(currentEntry);
		}
		return result;
	}

	@ApiOperation(value = "Activate a Version of Board")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{boardId}/activate", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Board activateBoardVersion(@PathVariable String boardId, @RequestBody String version) {

		Board board = this.boardRepository.findByIdAndVersion(boardId, version);
		if (board == null) {
			throw new ResourceNotFoundException("Board " + boardId + " not found for Version " + version);
		}
		if (board.isActive()) {
			return  board;
		}

		// Disable Current Active
		this.disableCurrentActive(boardId);
		board.setActive(true);
		board.addOrUpdate();
		this.boardRepository.save(board);
		this.cacheInvalidationManager.invalidate(BoardsCache.TYPE, boardId);
		return board;

	}
}
