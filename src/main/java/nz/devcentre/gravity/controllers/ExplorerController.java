/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.TemplateField;

@RestController
@RequestMapping("/explore")
public class ExplorerController {
	
	private static final Logger logger = LoggerFactory.getLogger(ExplorerController.class);
	
	@Autowired
	BoardsCache boardsCache;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/mongo/collections", method=RequestMethod.GET)
	public @ResponseBody Set<String> mongoCollections() throws Exception {
		return mongoTemplate.getCollectionNames();
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/mongo/get/{collectionName}", method=RequestMethod.POST)
	public @ResponseBody Collection<DBObject> mongoQuery(@PathVariable String collectionName, @RequestBody String queryString) throws Exception {
		
		Collection<DBObject> cards = new ArrayList<DBObject>(); 
		DBCollection collection = mongoTemplate.getDb().getCollection(collectionName);
		
		BasicDBObject q = BasicDBObject.parse(queryString);
		DBCursor find = collection.find(q);
		
		int c = 0;
		while(find.hasNext() && c<100){
			DBObject next = find.next();
			cards.add(next);
			c++;
		}
		
		return cards;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/mongo/query/{collectionName}", method=RequestMethod.GET)
	public @ResponseBody Collection<Card> mongoQuery(@PathVariable String collectionName) throws Exception {
		
		Collection<Card> cards = new ArrayList<Card>(); 
		DBCollection collection = mongoTemplate.getDb().getCollection(collectionName);
		
		BasicDBObject q = BasicDBObject.parse("{}");
		DBCursor find = collection.find(q);
		while(find.hasNext()){
			DBObject next = find.next();
			cards.add(mongoDBObjectToCard(next));
		}
		
		return cards;
	}
		
	
	/**
	 * Gets data directly, but limited to 100 rows
	 * 
	 * @param collectionName
	 * @return
	 * @throws Exception
	 */
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/mongo/data/{collectionName}", method=RequestMethod.GET)
	public @ResponseBody List<Map> mongoDataQuery(@PathVariable String collectionName) throws Exception {
	
		List<Map> result = new ArrayList<Map>();
		Collection<Card> cards = new ArrayList<Card>(); 
		DBCollection collection = mongoTemplate.getDb().getCollection(collectionName);
		
		BasicDBObject q = BasicDBObject.parse("{}");
		DBCursor find = collection.find(q).limit(100);
		
		while(find.hasNext()){
			DBObject next = find.next();
			Map newMap = new HashMap<String,Object>();
			newMap.putAll(next.toMap());
			newMap.remove("metadata");
			newMap.put("id", newMap.get("_id"));
			newMap.remove("_id");
			result.add(newMap);
		}
		
		return result;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/mongo/filter/{collectionName}", method=RequestMethod.POST)
	public @ResponseBody Collection<Card> mongoFilter(@PathVariable String collectionName, @RequestBody Filter filter) throws Exception {
		
		Collection<Card> cards = new ArrayList<Card>(); 
		DBCollection collection = mongoTemplate.getDb().getCollection(collectionName);
		
		String queryString = mongoQueryFromFilter(filter);
		logger.info("Running Query: " + queryString);
		
		BasicDBObject q = BasicDBObject.parse(queryString);
		DBCursor find = collection.find(q);
		
		while(find.hasNext() ){
			DBObject next = find.next();
			cards.add(mongoDBObjectToCard(next));
		}
		
		return cards;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/er/analysis", method=RequestMethod.GET)
	public @ResponseBody Collection<Map> erDiagramAnalysis() throws Exception {
		
		List<Map> returnList = new ArrayList<Map>();
		Map<String, String> list = boardsCache.list();
		for( String boardId: list.keySet()){
			Board board = boardsCache.getItem(boardId);
			for( TemplateField templateField : board.getFields().values()){
				
				if( templateField.getOptionlist()!=null){
					if( ! checkIfBoardExists(templateField.getOptionlist())){
						Map<String,String> map = new HashMap<String,String>();
						map.put("board", boardId);
						map.put("optionlist", templateField.getOptionlist());
						map.put("field", templateField.getId());
						returnList.add(map);
					}
				}
				
			}
		}
		return returnList;
	}
	
	@ApiOperation(value="Entity Relationship Model (For H3 Graphs)")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/er/{boardId}", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> er(@PathVariable String boardId) throws Exception {
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		List<Object> nodes = new ArrayList<Object>();
		returnMap.put("nodes", nodes);
		Set<String> nodeIds = new HashSet<String>();
		List<Object> links = new ArrayList<Object>();
		returnMap.put("links", links);
		
		// Begin by getting the root board
		Board board = this.boardsCache.getItem(boardId);
		this.addNode(nodes, nodeIds, boardId, this.getBoardNode(board));
		List<Map<String, Object>> boardLinks = this.getBoardLinks(board, null);
		links.addAll(boardLinks);
		for(Map<String, Object> value : boardLinks ){
			String targetId = (String) value.get("target");
			Board childBoard = this.boardsCache.getItem(targetId);
			this.addNode(nodes, nodeIds, targetId, this.getBoardNode(childBoard));
		}
		
		// Now loop over all
		Map<String, String> map = this.boardsCache.list();
		
		for(Entry<String,String> entry: map.entrySet()){
			Board childBoard = boardsCache.getItem(entry.getKey());
			List<Map<String,Object>> childBoardLinks = this.getBoardLinks(childBoard, boardId);
			if(childBoardLinks.size()>0){
				this.addNode(nodes, nodeIds, childBoard.getId(), this.getBoardNode(childBoard));
				links.addAll(childBoardLinks);
			}			
		}
		return returnMap;
	}
	
	private Map<String,Object> getBoardNode( Board board ){
		long totalCards = 0;
		Map<String,Object> newNode = new HashMap<String,Object>();
		newNode.put("id", board.getId());
		newNode.put("title", board.getName());
		newNode.put("group", board.getPhases().size());
		newNode.put("cards", totalCards);
		return newNode;
	}
	
	private List<Map<String,Object>> getBoardLinks( Board board, String filter ) {
		List<Map<String,Object>> linkList = new ArrayList<Map<String,Object>>();
		for( TemplateField field :board.getFields().values()){
			if( field.getOptionlist()!=null && (filter==null || filter.equals(field.getOptionlist()))){
				Map<String,Object> link = new HashMap<String,Object>();
				link.put("source", board.getId());
				link.put("target", field.getOptionlist());
				link.put("value", .1);
				linkList.add(link);
			}
		}
		return linkList;
	}
	
	private void addNode(List<Object> nodes, Set<String> idList, String id, Map<String,Object> node ){
		if(!idList.contains(id)){
			nodes.add(node);
			idList.add(id);
		}
	}
	
	private boolean checkIfBoardExists( String boardId ){
		try{	
			boardsCache.getItem(boardId);
			return true;
		} catch( Exception e) {
			return false;
		}
	}
	
	private Card mongoDBObjectToCard( DBObject o){
		Card card = new Card();
		
		card.setId(o.get("_id").toString());
		
		Map<String,Object> fields = new HashMap<String,Object>();
		for( String key :o.keySet()){
			if(!key.equals("metadata") && !key.equals("_id") ){
				fields.put(key, o.get(key));
			}
		}
		card.setFields(fields);
		return card;
	}
	
	private String mongoQueryFromFilter( Filter filter ){
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		boolean first = true;
		for( Condition condition : filter.getConditions().values()){
			if(first){
				first = false;
			} else {
				sb.append(",");
			}
			genEx(condition.getOperation().getMongoExpression(), condition.getFieldName(), condition.getValue(), sb);
		}
		sb.append("}");
		return sb.toString();
	}
	
	private void genEx(String expression, String field, String value, StringBuilder sb) {
		if(value.contains("|")){
			List<String> values = Arrays.asList(value.split("\\|"));
			sb.append("((");
			boolean first = true;
			for( String v : values){
				String rf = expression.replaceAll("\\$\\{field\\}", field);
				String rv = rf.replaceAll("\\$\\{value\\}", v.toLowerCase());
				if(first){
					first = false;
				} else {
					sb.append(") or (");
				}
				sb.append(rv);
			}
			sb.append("))");
		} else {
			String rf = expression.replaceAll("\\$\\{field\\}", field);
			String rv = rf.replaceAll("\\$\\{value\\}", value.toLowerCase());
			sb.append(rv);
		}
	}	
	
	@ApiOperation(value="Entity Relationship Model (For H3 Graphs)")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/er", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> er() throws Exception {
		Map<String,Object> returnMap = new HashMap<String,Object>();
		List<Object> nodes = new ArrayList<Object>();
		returnMap.put("nodes", nodes);		
		List<Object> links = new ArrayList<Object>();
		returnMap.put("links", links);
		
		Map<String, String> map = this.boardsCache.list();
		
		for(Entry<String,String> entry: map.entrySet()){
			
			Board board = boardsCache.getItem(entry.getKey());
			long phases = 1;
			if( board.getPhases()!=null){
				for( Phase phase : board.getPhases().values()){
					phases = board.getPhases().size();
				}
			}			
			Map<String,Object> node = new HashMap<String,Object>();
			node.put("id", entry.getKey());
			node.put("title", entry.getValue());
			node.put("group", phases);
			nodes.add(node);
			
			for( TemplateField field :board.getFields().values()){
				if( field.getOptionlist()!=null){
					Map<String,Object> link = new HashMap<String,Object>();
					link.put("source", entry.getKey());
					link.put("target", field.getOptionlist());
					link.put("value", 1);
					links.add(link);
				}
			}
		}
		return returnMap;
	}
}
