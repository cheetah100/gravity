/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;


import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Tenant;
import nz.devcentre.gravity.repository.TenancyRepository;

@Service
public class TenancyCache extends CacheImpl<Tenant> {
	
	public static final String TYPE = "TENANCY";
		
	@Autowired
	private TenancyRepository repository;
	
	@Override
	protected Tenant getFromStore(String... itemIds) throws Exception {
		String tenantId = itemIds[0];
		Tenant tenant = repository.findOne(tenantId);
		return tenant;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		
		StringBuffer pre = new StringBuffer();
		pre.append(Integer.toString(prefixs.length) + " - ");
		for( int x=0; x < prefixs.length; x++){
			pre.append(prefixs[x]);
			pre.append(", ");
		}
		
		Map<String,String> result = new TreeMap<String,String>();
		List<Tenant> list = repository.findAll();
		for (Tenant tenant: list) {
			result.put(tenant.getId(), tenant.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}	

}
