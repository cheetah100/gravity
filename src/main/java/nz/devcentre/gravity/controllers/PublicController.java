/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.model.transfer.LoginRequest;
import nz.devcentre.gravity.repository.TeamRepository;
import nz.devcentre.gravity.repository.UserRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.security.TokenTools;
import nz.devcentre.gravity.services.EmailService;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * The purpose of the Public Controller is to allow calls which do
 * not require authentication. This is typically for login calls, 
 * and signups.
 * 
 * @author peter
 *
 */
@RestController
@RequestMapping("/public")
@ApiModel(value = "Public")
public class PublicController {

	private static final Logger logger = LoggerFactory.getLogger(PublicController.class);
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TeamRepository teamRepository;

	
	@ApiOperation(value = "Ping - Return Request Headers")
	@RequestMapping(value = "/ping", method = RequestMethod.GET)
	public @ResponseBody List<String> ping(HttpServletRequest request) throws Exception {

		Enumeration<String> headers = request.getHeaderNames();
		List<String> returnList = new ArrayList<String>();

		while (headers.hasMoreElements()) {
			String name = headers.nextElement();
			String value = request.getHeader(name);
			returnList.add("header: " + name + ": " + value);
		}

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = parameterNames.nextElement();
			String value = request.getParameter(name);
			returnList.add("parameter: " + name + ": " + value);
		}

		returnList.add("Context: " + request.getContextPath());
		returnList.add("URI: " + request.getRequestURI());
		returnList.add("Method: " + request.getMethod());
		returnList.add("User: " + SecurityTool.getCurrentUser());
		
		return returnList;
	}

	
	@ApiOperation(value = "Ping - Return Request Headers")
	@RequestMapping(value = "/pong", method = RequestMethod.POST)
	public @ResponseBody List<String> pong(HttpServletRequest request) throws Exception {

		Enumeration<String> headers = request.getHeaderNames();
		List<String> returnList = new ArrayList<String>();

		while (headers.hasMoreElements()) {
			String name = headers.nextElement();
			String value = request.getHeader(name);
			returnList.add("header: " + name + ": " + value);
		}

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = parameterNames.nextElement();
			String value = request.getParameter(name);
			returnList.add("parameter: " + name + ": " + value);
		}

		returnList.add("Context: " + request.getContextPath());
		returnList.add("URI: " + request.getRequestURI());
		returnList.add("Method: " + request.getMethod());
		returnList.add("User: " + SecurityTool.getCurrentUser());
		
		String body = new String(request.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
		
		returnList.add("Body: " + body);
		
		return returnList;
	}

	
	@ApiOperation(value = "Ping - Return Request Headers")
	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public @ResponseBody List<String> post(HttpServletRequest request, @RequestBody String body) throws Exception {

		Enumeration<String> headers = request.getHeaderNames();
		List<String> returnList = new ArrayList<String>();

		while (headers.hasMoreElements()) {
			String name = headers.nextElement();
			String value = request.getHeader(name);
			returnList.add("header: " + name + ": " + value);
		}

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = parameterNames.nextElement();
			String value = request.getParameter(name);
			returnList.add("parameter: " + name + ": " + value);
		}

		returnList.add("Context: " + request.getContextPath());
		returnList.add("URI: " + request.getRequestURI());
		returnList.add("Method: " + request.getMethod());
		returnList.add("Body: " + body);
		return returnList;
	}
	
	
	@ApiOperation(value = "Login - Log into the system")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody Map<String,String> login(@RequestBody final LoginRequest login) throws Exception {
		
		User user = this.securityTool.login( login.getUsername(), login.getPassword());
		
		String token = TokenTools.createToken(user);
		Map<String,String> returnMap = new HashMap<>();
		returnMap.put("username", user.getName());
		returnMap.put("name", user.getFullName());
		returnMap.put("token", token);
		return returnMap;
	}

	/**
	 * In this method a temporary password reset token is provided.
	 * This is provided to the change password call. An email is sent to
	 * the users email account.
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "ResetPassword - Request Reset of users password. (does not actually eliminate password)")
	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public @ResponseBody void resetPasswordRequest(@RequestBody final LoginRequest reset) throws Exception {
		
		String token = this.securityTool.resetPassword(reset.getUsername());
		
		User user = this.securityTool.getUser(reset.getUsername());

		String body = "Password Reset\nCode: " + token + "\n\nPlease enter this code to validate password reset."; 
		
		emailService.sendEmail("Devcentre Password Reset", 
				body, 
				user.getEmail(), 
				null, 
				"security@devcentre.nz", 
				"security@devcentre.nz", 
				false, null, null );
	}
	
	@ApiOperation(value = "SetPassword - Set New Password with Token from ResetPassword")
	@RequestMapping(value = "/password/set", method = RequestMethod.POST)
	public @ResponseBody void setPassword(@RequestBody final LoginRequest reset) throws Exception {		
		this.securityTool.setPassword( reset.getUsername(), reset.getPassword(), reset.getResetToken());
	}

	@ApiOperation(value = "Signup - Sign Up for a Gravity Account")
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public @ResponseBody void signUp(@RequestBody final User user) throws Exception {
		
		String newId = IdentifierTools.getIdFromNamedModelClass(user);
		if (this.securityTool.userExists(newId)) {
			throw new ValidationException("User already exists, id: " + Encode.forHtmlContent(newId));
		}
		
		if (this.securityTool.userExists(user.getEmail())) {
			throw new ValidationException("Email Address already exists, id: " + Encode.forHtmlContent(user.getEmail()));
		}
		
		user.setId(newId);
		
		String token = this.securityTool.signUp(user);
		String body = "Devcentre Workflow Signup Confirmation: "+ token;
		emailService.sendEmail("Devcentre Workflow Signup Confirmation", 
				body, user.getEmail(), null, "operations@devcentre.nz", "operations@devcentre.nz", false, null, null );		
	}

	@ApiOperation(value = "Validate Email")
	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public @ResponseBody void validate(@RequestBody final LoginRequest request) throws Exception {		
		this.securityTool.validate(request.getUsername(), request.getResetToken());
		User user = this.securityTool.getUser(request.getUsername());
		
		String body = "Email Address Validated";
		
		emailService.sendEmail("Devcentre Email Address Confirmation", 
				body, user.getEmail(), null, "operations@devcentre.nz", "operations@devcentre.nz", false, null, null );
	}
	
	/*
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value = "Migration to Access Control Policy")
	@RequestMapping(value = "/acp", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public List<String> acpMigration(HttpServletRequest request) throws Exception {

		if (acpRepository.count() > 1) {
			throw new Exception("Cannot run Access Control Policy Migration more than once");
		}

		List<String> acps = new ArrayList<String>();

		// Loop over each team to generate one ACP per team
		List<Team> teams = teamRepository.findAll();

		for (Team team : teams) {
			AccessControlPolicy acp = new AccessControlPolicy();

			acp.setId(team.getId() + "_policy");
			acp.setName(team.getName() + " Access Control Policy");
			acp.setOwners(new ArrayList<String>());
			acp.getOwners().add(SecurityTool.getCurrentUser());
			acp.setResources(new ArrayList<AccessControlResource>());
			acp.setTeams(new ArrayList<String>());
			acp.getTeams().add(team.getId());

			// Check Applications
			List<Application> appList = this.appRepository.findAll();
			for (Application app : appList) {
				evaluatePermissions(app, "APP", team.getId(), acp);
			}

			// Within each ACP check each board to see if the team is a member
			Map<String, String> boardList = boardsCache.list();
			for (String boardId : boardList.keySet()) {
				Board board = boardsCache.getItem(boardId);
				evaluatePermissions(board, "BOARD", team.getId(), acp);
			}

			acpRepository.save(acp);
			acps.add(acp.getName());
		}

		return acps;
	}
	*/

	/*
	private void evaluatePermissions(SecurityPermissionEntity entity, String type, String teamId,
			AccessControlPolicy acp) {
		Map<String, String> permissions = entity.getPermissions();
		if (permissions != null) {
			logger.debug("permissions is not null");
			if (permissions.containsKey(teamId)) {
				try {
					AccessControlResource ar = new AccessControlResource();
					ar.setAccessType(AccessType.valueOf(permissions.get(teamId)));
					ar.setResourceId(entity.getId());
					ar.setResourceType(type);
					acp.getResources().add(ar);
				} catch (Exception e) {
					logger.warn("Invalid permission in " + type + ": " + entity.getId() + " for team " + teamId
							+ " value=" + permissions.get(teamId));
					throw e; // raise exception
				}
			}
		}
	}
	*/

	@ApiOperation(value = "Init Gravity Data")
	@RequestMapping(value = "/init", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	public boolean bootstrap() throws Exception {

		String userId = SecurityTool.getCurrentUser();
		// Determine if there are new users
		List<User> userList = userRepository.findAll();

		if (userList.isEmpty()) {
			User user = new User();
			user.setId(userId);
			user.setName(userId);
			user.setFirstname(userId);
			user.setSurname("");
			user.setEmail("");

			userRepository.insert(user);
			logger.info("Bootstrapping New Admin User: " + user.getName());
		}

		List<Team> teams = teamRepository.findAll();
		if (teams.isEmpty()) {
			Map<String, String> members = new HashMap<String, String>();
			members.put(userId, "ADMIN");
			Team team = new Team();
			team.setId("administrators");
			team.setName("Administrators");
			team.setMembers(members);
			team.setOwners(members);

			teamRepository.insert(team);
			logger.info("Bootstrapping New Administration Team");
		}

		return true;
	}
}
