/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.BoardListener;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.CardTools;

@Service
public class CardListCache extends CacheImpl<Collection<Card>> implements BoardListener{
	
	public static final String TYPE = "CARDLIST";
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired 
	private QueryService queryService;
	
	@Autowired 
	private BoardsCache boardCache;
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		return null;
	}

	@Override
	protected Collection<Card> getFromStore(String... itemIds) throws Exception {
		Collection<Card> result = null;
		String boardId = itemIds[0];
		String order = null;
		if(itemIds.length>1){
			order = itemIds[1];
		}
		
		result = queryService.query(boardId, null, null, null, false);

		Board board = boardCache.getItem(boardId);
		return CardTools.orderCards(result, order, false, null, board);
	}

	@Override
	public void onCardHolderEvent(CardHolder cardHolder) throws Exception {
		this.cacheInvalidationManager.invalidate("CARDLIST", cardHolder.getBoardId());
	}
	
}
