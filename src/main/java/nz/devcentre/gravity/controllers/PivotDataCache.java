/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.BoardListener;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.PivotTable;
import nz.devcentre.gravity.model.transfer.PivotData;
import nz.devcentre.gravity.repository.PivotTableRepository;

/**
 * Pivot Cache
 * 
 * Purpose of this class is to cache the results of pivots so that we do not need to recalculate
 * pivots every time one is requested. This class also included a mechanism to invalidate the pivot
 * but listening to the event queue.
 * 
 * @author peter
 */
@Service
public class PivotDataCache extends CacheImpl<PivotData> implements BoardListener{
	
	public static final String TYPE = "PIVOT";
				
	@Autowired
	private PivotTableRepository pivotTableRepository;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
		
	@Override
	protected PivotData getFromStore(String... itemIds) throws Exception {
		return null;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {

		String boardId = prefixes[0];
		List<PivotTable> pivots = pivotTableRepository.findByBoardIdAndActiveTrue(boardId);
		Map<String,String> result = new TreeMap<String, String>();
		for (PivotTable pivot: pivots) {
			result.put(pivot.getId(), pivot.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}

	public void invalidatePivots(String boardId) throws Exception{
		for( Entry<String, PivotData> entry : this.cacheMap.entrySet()){
			PivotData pivotData = entry.getValue();
			if( pivotData.getDatasources().contains(boardId) ){
				this.cacheInvalidationManager.invalidate(TYPE, entry.getKey());
			}
		}
	}
	
	/**
	 * This method receives cardHolder events and determines if there are related pivots.
	 * If so it sends invalidation messages via topics to invalidate all pivots on the board.
	 */
	@Override
	public void onCardHolderEvent(CardHolder cardHolder) throws Exception {
		invalidatePivots(cardHolder.getBoardId());
	}
	
}
