/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.PivotTable;
import nz.devcentre.gravity.repository.PivotTableRepository;


@Component
public class PivotTableTools {
	
	@Autowired
	PivotTableRepository pivotTableRepository;
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listPivots(@PathVariable String boardId,@RequestParam(required = false,defaultValue = "false") boolean all) throws Exception {
		
		Map<String,String> result = new HashMap<String, String>();
		List<PivotTable> pivots = all?this.pivotTableRepository.findByBoardId(boardId):pivotTableRepository.findByBoardIdAndActiveTrue(boardId);
		for (PivotTable pivot: pivots) {
			result.put(pivot.getId(), pivot.getName());
		}
		return result;			
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/{pivotId}", method=RequestMethod.GET)
	public @ResponseBody PivotTable getPivot(@PathVariable String boardId, 
			@PathVariable String pivotId) throws Exception {
		
		PivotTable pivot = pivotTableRepository.findByBoardIdAndIdAndActiveTrue(boardId, pivotId);
		if(pivot==null){
			throw new ResourceNotFoundException("Active Version for Pivot not found, id: " + Encode.forHtmlContent(pivotId));
		}		
		return pivot;		
	}
	
	public void disableCurrentActive(String boardId,String pivotId) {
		PivotTable activePivot = pivotTableRepository.findByBoardIdAndIdAndActiveTrue(boardId, pivotId);
		if(activePivot!=null) {
			activePivot.setActive(false);
			activePivot.addOrUpdate();
			this.pivotTableRepository.save(activePivot);
		}
	}
}
