/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2018 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.BoardResource;
import nz.devcentre.gravity.repository.BoardResourceRepository;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/board/{boardId}/resources")
public class ResourceController {
		
	private static final Logger logger = LoggerFactory.getLogger(ResourceController.class);
	
	@Autowired
	private ResourceCache resourceCache;
	
	@Autowired
	CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	BoardResourceRepository boardResourceRepository;
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{resourceId}", method=RequestMethod.GET)
	public @ResponseBody String getResource(@PathVariable String boardId, 
			@PathVariable String resourceId) throws Exception {
		return resourceCache.getItem(boardId, IdentifierTools.getIdFromName(resourceId));		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{resourceId}/file", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getResourceFile(@PathVariable String boardId, 
			@PathVariable String resourceId) throws Exception {
		BoardResource resource = this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
		
		byte[] response = null;
		
		if(resource.getFile()!=null) {
			Binary file = resource.getFile();
			response = file.getData();
		} else if(resource.getResource()!=null) {
			response = resource.getResource().getBytes("utf-8");
		}
		
		if( response == null ) {
			throw new ResourceNotFoundException();
		}
		
		logger.info("Returning content " + resource.getFilename() + " size = " + response.length);
		
		return ResponseEntity
			.ok()
			.contentLength(response.length)
			.contentType(MediaType.parseMediaType(resource.getMimetype()))
			.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + resource.getFilename())
			.body(response);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{resourceId}", method=RequestMethod.POST)
	public @ResponseBody void createResource(@PathVariable String boardId, 
			@PathVariable String resourceId,
			HttpServletRequest request,
			@RequestParam(required = false) String mimetype) throws Exception {
		
		logger.info("Saving Resource " + boardId + "/" + resourceId);
		BufferedReader reader = request.getReader();
		reader.reset();
		StringBuilder valueBuilder = new StringBuilder();
		
		while(true){
			String newValue = reader.readLine();
			if( newValue!=null){
				valueBuilder.append(newValue);
				valueBuilder.append("\n");
			} else {
				break;
			}
		}
		
		String value = valueBuilder.toString();
			
		try {
			BoardResource resource = new BoardResource();
			resource.setBoardId(boardId);
			resource.setName(resourceId);
			resource.setId(IdentifierTools.getIdFromBoardClass(resource));
			resource.setResource(value);
			resource.setMimetype(mimetype);
			resource.setActive(true);
			resource.setPublish(false);
			resource.addOrUpdate();
			
			boardResourceRepository.save(resource);
			this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, boardId,resourceId);
		} finally {
		}
		
		logger.info("Resource Saved " + resourceId);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{resourceId}/text", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create new Resource via JSON",
			notes = "Creates new Resource Body."
			+ "When Active Parameter is True will de-activate current active version(if any)."
			+ "If provided Version already exists then existing resource body will be updated")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "version",dataType ="String",required = false,
					paramType = "query",value ="Used to override System generated version" )
			}
	)
	public @ResponseBody void createResourceByJsonString(@PathVariable String boardId, 
			@PathVariable String resourceId, 
			@RequestBody String jsonText,			
			@RequestParam(required = false) String version,
			@RequestParam(required = false) String mimetype) throws Exception {
				
		try {
			
			logger.info("Saving Resource " + boardId + "/" + resourceId);
			
			//If active flag is true, set existing active version to false
			
			Gson gson = new Gson();
			Object text = gson.fromJson(jsonText, Object.class);
			
			BoardResource activeResource = 
					this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
			if(activeResource!=null) {
				activeResource.setActive(false);
				activeResource.addOrUpdate();
				boardResourceRepository.save(activeResource);
			}
			
			BoardResource resource=new BoardResource();
			//Set the Version
			if(!StringUtils.isEmpty(version)) {
				resource.setVersion(version);
			}
			
			resource.setId(IdentifierTools.getIdFromName(resourceId));
			resource.setBoardId(boardId);
			resource.setName(resourceId);
			resource.setResource(text.toString());
			resource.setMimetype(mimetype);
			IdentifierTools.newVersion(resource);
			resource.setActive(true);
			resource.setPublish(false);
			resource.addOrUpdate();
			
			boardResourceRepository.save(resource);
			
			this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, 
					boardId,resourceId);
		} finally {
		}
		
		logger.info("Resource Saved " + resourceId);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{resourceId}/plain", method=RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	@ApiOperation(value = "Create new Resource",consumes ="text/plain",
			notes = "Creates new Resource Body."
			+ "When Active Parameter is True will de-activate current active version(if any)."
			+ "If provided Version already exists then existing resource body will be updated")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "version", dataType ="String", required = false,
					paramType = "query",value ="Used to override System generated version" )
			}
	)
	public @ResponseBody void createResourceByPlainText(@PathVariable String boardId, 
			@PathVariable String resourceId, 
			@RequestBody String text,			
			@RequestParam(required = false) String version,
			@RequestParam(required = false) String mimetype) throws Exception {
				
		try {
			
			logger.info("Saving Resource " + boardId + "/" + resourceId);
			
			//If active flag is true, set existing active version to false
			
			BoardResource activeResource = 
					this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
			if(activeResource!=null) {
				activeResource.setActive(false);
				activeResource.addOrUpdate();
				boardResourceRepository.save(activeResource);
			}
			
			BoardResource resource=new BoardResource();
			//Set the Version
			if(!StringUtils.isEmpty(version)) {
				resource.setVersion(version);
			}
			
			resource.setId(IdentifierTools.getIdFromName(resourceId));
			resource.setBoardId(boardId);
			resource.setName(resourceId);
			resource.setResource(text);
			resource.setMimetype(mimetype);
			IdentifierTools.newVersion(resource);
			resource.setActive(true);
			resource.setPublish(false);
			resource.addOrUpdate();
			
			boardResourceRepository.save(resource);
			
			this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, 
					boardId,resourceId);
		} finally {
		}
		
		logger.info("Resource Saved " + resourceId);
	}

	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{resourceId}/file", method=RequestMethod.POST)
	@ApiOperation(value = "Create new Resource",consumes ="file",
			notes = "Creates new Resource Body."
			+ "When Active Parameter is True will de-activate current active version(if any)."
			+ "If provided Version already exists then existing resource body will be updated")
	@ApiImplicitParams(value = { 
			@ApiImplicitParam(name = "version",dataType ="String",required = false,
					paramType = "query",value ="Used to override System generated version" )
			}
	)
	public @ResponseBody void createResourceByFile(@PathVariable String boardId, 
			@PathVariable String resourceId, 
			@RequestParam MultipartFile file,	
			@RequestParam(required = false) String version) throws Exception {
				
		try {
			
			logger.info("Saving Resource " + boardId + "/" + resourceId + "/" + file.getOriginalFilename() + " size " + file.getSize());
			
			BoardResource activeResource = 
					this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
			if(activeResource!=null) {
				activeResource.setActive(false);
				activeResource.addOrUpdate();
				boardResourceRepository.save(activeResource);
			}
			
			BoardResource resource=new BoardResource();
			//Set the Version
			if(!StringUtils.isEmpty(version)) {
				resource.setVersion(version);
			}
			
			resource.setId(IdentifierTools.getIdFromName(resourceId));
			resource.setBoardId(boardId);
			resource.setName(resourceId);
			
			resource.setFile(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
			
			IdentifierTools.newVersion(resource);
			resource.setActive(true);
			resource.setPublish(false);
			resource.setFilename(file.getOriginalFilename());
			resource.setMimetype(file.getContentType());
			resource.addOrUpdate();
			
			boardResourceRepository.save(resource);
			
			this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, 
					boardId,resourceId);
		} finally {
		}
		
		logger.info("Resource Saved " + resourceId);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")	
	@RequestMapping(value = "/{resourceId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteResource(@PathVariable String boardId, 
			@PathVariable String resourceId) throws Exception {
		
		String rid = IdentifierTools.getIdFromName(resourceId);
		logger.info("Deleting Resource " + rid);
		BoardResource resource = 
				boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, rid);
		if (resource == null) {
			throw new ResourceNotFoundException("Resource not found, resourceId: " + 
					Encode.forHtmlContent(rid));
		}
		//boardResourceRepository.delete(resource.getUniqueid());
		resource.addOrUpdate();
		resource.setActive(false);
		boardResourceRepository.save(resource);
		this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, boardId, rid);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listResources(@PathVariable String boardId,
			@RequestParam(required = false,defaultValue = "false")boolean all) throws Exception {	
		logger.info("Getting Resource List");
		if(all) {
			return this.resourceCache.getAllResourceForBoard(boardId);
		}
		return this.resourceCache.list(boardId);		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")	
	@RequestMapping(value = "/details", method=RequestMethod.GET)
	public @ResponseBody List<BoardResource> listResourceDetails(@PathVariable String boardId,
			@RequestParam(required = false,defaultValue = "false")boolean all) throws Exception {	
		logger.info("Getting Resource Detail");
		if(all) {
			return this.boardResourceRepository.findByBoardId(boardId);
		}
		return this.boardResourceRepository.findByBoardIdAndActiveTrue(boardId);		
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{resourceId}/details", method=RequestMethod.GET)
	@ApiOperation(value = "Get the Deatils of current active version",
			notes = "Will not return the Text Body of resource, If there is no active version will not return anything")
	public @ResponseBody BoardResource getResourceJson(@PathVariable String boardId, 
			@PathVariable String resourceId) throws Exception {
		
		return boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, 
				IdentifierTools.getIdFromName(resourceId));
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{resourceId}/history", method=RequestMethod.GET)
	@ApiOperation(value = "Get the History of a Resource",
			notes = "Will not return the Text Body of resource. This lists down all the Versions available for the resource")
	public @ResponseBody List<BoardResource> getResourceJsonHistory(@PathVariable String boardId, 
			@PathVariable String resourceId) throws Exception {
		return boardResourceRepository.findByBoardIdAndId(boardId, IdentifierTools.getIdFromName(resourceId));
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{resourceId}/activate", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Activate a Specific Version of a Resource")
	public BoardResource activateResource(
			@PathVariable String boardId, 
			@PathVariable String resourceId,
			@RequestBody String version) throws Exception {
		BoardResource resourceToActivate = 
				this.boardResourceRepository.findByBoardIdAndIdAndVersion(boardId, 
						resourceId, version);
		
		if (resourceToActivate == null) {
			throw new ResourceNotFoundException("Resource " + resourceId +
					" Does not Exist For Version:-" + version);
		}
		
		BoardResource resource = 
				this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, 
						IdentifierTools.getIdFromName(resourceId));
		
		if (resource != null) {
			resource.setActive(false);
			resource.addOrUpdate();
			this.boardResourceRepository.save(resource);
		}
		
		resourceToActivate.addOrUpdate();
		resourceToActivate.setActive(true);
		this.boardResourceRepository.save(resourceToActivate);
		this.cacheInvalidationManager.invalidate(ResourceCache.TYPE, boardId, 
				resourceId);
		return resourceToActivate;
	}
}
