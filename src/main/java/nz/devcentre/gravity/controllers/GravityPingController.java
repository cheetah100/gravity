package nz.devcentre.gravity.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GravityPingController extends HttpServlet {

	@Override

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Enumeration<String> headers = request.getHeaderNames();
		List<String> returnList = new ArrayList<String>();

		while (headers.hasMoreElements()) {
			String name = headers.nextElement();
			String value = request.getHeader(name);
			returnList.add("header: " + name + ": " + value);
		}

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String name = parameterNames.nextElement();
			String value = request.getParameter(name);
			returnList.add("parameter: " + name + ": " + value);
		}

		returnList.add("Context: " + request.getContextPath());
		returnList.add("URI: " + request.getRequestURI());
		returnList.add("Method: " + request.getMethod());
		response.setHeader("content-type", MediaType.APPLICATION_JSON_VALUE);
		ObjectMapper mapper=new ObjectMapper();
		
		PrintWriter out = response.getWriter();
		out.write(mapper.writeValueAsString(returnList));
		//super.doGet(request, response);
	}

}
