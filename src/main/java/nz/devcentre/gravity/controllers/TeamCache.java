/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.repository.TeamRepository;

@Service
public class TeamCache extends CacheImpl<Team> {
	
	public static final String TYPE = "TEAM";
		
	@Autowired
	private TeamRepository teamRepository;
	
	@Override
	protected Team getFromStore(String... itemIds) throws Exception {
		String teamId = itemIds[0];
		Team team = teamRepository.findById(teamId);
		return team;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		
		StringBuffer pre = new StringBuffer();
		pre.append(Integer.toString(prefixs.length) + " - ");
		for( int x=0; x < prefixs.length; x++){
			pre.append(prefixs[x]);
			pre.append(", ");
		}
		
		Map<String,String> result = new TreeMap<String,String>();
		List<Team> list = teamRepository.findAll();
		for (Team team: list) {
			result.put(team.getId(), team.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
	
}
