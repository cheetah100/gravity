/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.ListResource;
import nz.devcentre.gravity.model.Option;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.transfer.ListOption;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/board/{boardId}/lists")
public class ListController {
	
	@Autowired
	private BoardsCache boarcdsCache;
		
	@Autowired 
	private QueryService queryService;
	
	/**
	 * Get List.
	 * There are two sources for a list:
	 * - A List within the Board itself, used for small lists.
	 * - A List generated out of another Board.
	 * 
	 * @param boardId
	 * @param listId
	 * @return
	 * @throws Exception
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{listId}", method=RequestMethod.GET)
	public @ResponseBody ListResource getList(
			@PathVariable String boardId,
			@PathVariable String listId,
			@RequestParam(required=false) String filter) throws Exception {
		
		String lid = IdentifierTools.getIdFromName(listId);
		ListResource item = new ListResource();
		item.setItems(getCardListFromBoard(lid, null, filter, "all", "name", null, null));
		return item;
	}
	
	public Map<String,Option> getCardListFromBoard( String board, 
			String phase, 
			String filter, 
			String view, 
			String field, 
			String order, 
			String fieldFilter) throws Exception{
		
		Collection<Card> cards = this.queryService.query(board, null, filter, view, false);
		Map<String,Option> optionList = new LinkedHashMap<String,Option>(); 
		
		for( Card card : cards){
			
			if(fieldFilter!=null){
				boolean success = getBooleanField(card, fieldFilter);
				if(!success){
					continue;
				}
			}
			
			Option option = new Option();
			option.setId(card.getId());
			
			
			
			Map<String, Object> fields = card.getFields();
			
			option.setName((String)card.getField("name"));
			
			Map<String,String> newAttrs = new HashMap<String,String>();
			newAttrs.put("phase", card.getPhase());
			for( Entry<String,Object> attr : fields.entrySet()){
				if(attr.getValue() != null){
					newAttrs.put(attr.getKey(), attr.getValue().toString());
				}
			}
			option.setAttributes(newAttrs);
			optionList.put(option.getId(), option);
		}
		return optionList;
	}
	
	private boolean getBooleanField( Card card, String field){
		Object object = card.getFields().get(field);
		boolean value = false;
		if(object!=null && object instanceof Boolean){
			value = (Boolean) object;
		}
		return value;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{listId}/basic", method=RequestMethod.GET)
	public @ResponseBody List<ListOption> getBasicList(
			@PathVariable String boardId, 
			@PathVariable String listId,
			@RequestParam(required=false) String filter) throws Exception {
		
		List<ListOption> returnList = new ArrayList<ListOption>();
		ListResource resource = this.getList(boardId, listId, filter);
		for( Entry<String, Option> entry : resource.getItems().entrySet()){
			Option option = entry.getValue();
			returnList.add( 
					new ListOption(entry.getKey(), 
							option.getName(), 
							option.getAttributes().get("color"),
							option.getAttributes().get("icon"),
							option.getAttributes().get("definition"),
							option.getAttributes().get("phase")	,
							option.getAttributes().get("order")
					));
		}
		return returnList;
	}
	
	public Map<String,String> getListMap( String boardId, String listId, String filter) throws Exception {
		
		Map<String,String> returnMap = new HashMap<String,String>();
		ListResource resource = this.getList(boardId, listId, filter);
		for( Entry<String, Option> entry : resource.getItems().entrySet()){
			Option option = entry.getValue();
			returnMap.put(option.getId(), option.getName());
		}
		
		return returnMap;
	}
	
	/*
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{viewId}/listsforview", method=RequestMethod.GET)
	public @ResponseBody Map<String,List<ListOption>> listsForView(
			@PathVariable String boardId,
			@PathVariable String viewId) throws Exception {
		
		
		Map<String,TemplateField> lists = new HashMap<String,TemplateField>();
		
		Template view = viewController.getViewWithTemplateData(boardId, viewId);
		for( TemplateField tf : view.getFields().values()){
			if( tf.getOptionlist()!=null){
				lists.put(tf.getOptionlist(),tf);
			}
		}
		
		Map<String,List<ListOption>> results = new HashMap<String,List<ListOption>>();
		
		for( Entry<String,TemplateField> list : lists.entrySet()){
			List<ListOption> basicList = this.getBasicList(boardId, list.getKey(), list.getValue().getOptionlistfilter());
			results.put(list.getValue().getName(), basicList);
		}
		
		return results;
	}
	*/
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/fortemplate", method=RequestMethod.GET)
	public @ResponseBody Map<String,Map<String,String>> listsForView(
			@PathVariable String boardId) throws Exception {
		
		Map<String,TemplateField> lists = new HashMap<String,TemplateField>();
		
		Board board = this.boarcdsCache.getItem(boardId);
		
		for( TemplateField tf : board.getFields().values()){
			if( tf.getOptionlist()!=null){
				lists.put(tf.getOptionlist(),tf);
			}
		}
		
		Map<String,Map<String,String>> results = new HashMap<String,Map<String,String>>();
		
		for( Entry<String,TemplateField> list : lists.entrySet()){
			Map<String, String> listMap = this.getListMap(boardId, list.getKey(), list.getValue().getOptionlistfilter());
			results.put(list.getValue().getName(), listMap);
		}
		
		return results;
	}

}
