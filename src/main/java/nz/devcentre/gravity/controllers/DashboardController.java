/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Dashboard;
import nz.devcentre.gravity.model.Metadata;
import nz.devcentre.gravity.repository.DashboardRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/dashboard")
@ApiModel(value="Dashboard")
public class DashboardController {

	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;

	@Autowired
	private SecurityTool securityTool;
	
	@Autowired 
	private DashboardCache dashboardCache;
	
	@Autowired
	private DashboardRepository dashboardRepository;
	
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{dashboardId}", method=RequestMethod.GET)
	@ApiOperation(value="Get DashBoard")
	public @ResponseBody Dashboard getDashboard(@PathVariable String dashboardId) throws Exception {
		Dashboard dashboard = dashboardCache.getItem(dashboardId);
		return dashboard;
	}
	
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{dashboardId}/history", method=RequestMethod.GET)
	@ApiOperation(value="Get DashBoard History")
	public @ResponseBody List<Map<String, Object>> getDashboardHistory(@PathVariable String dashboardId) throws Exception {
		List<Dashboard> dashboards = this.dashboardRepository.findById(dashboardId);
		List<Map<String,Object>> result=new ArrayList<>();
		for(Dashboard db:dashboards) {
			Map<String,Object> currentDB=new HashMap<>();
			Metadata metadata = db.getMetadata();
			String version = db.getVersion();
			currentDB.put("Version", version);
			currentDB.put("Active", db.isActive());
			currentDB.put("Metadata", metadata);
			result.add(currentDB);
		}
		return result;
	}
	
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{dashboardId}/activate", method=RequestMethod.POST)
	@ApiOperation(value="Activate a Dashboard Version")
	@ResponseStatus(HttpStatus.CREATED)
	public Dashboard  activateVersion(@PathVariable String dashboardId,@RequestBody String version) throws Exception {
		Dashboard dashboard = this.dashboardRepository.findByIdAndVersion(dashboardId, version);
		if(dashboard==null) {
			throw new ResourceNotFoundException("Dashboard "+dashboardId+" not found for Version "+version);
		}
		if(dashboard.isActive()) {
			return dashboard;//Already Active
		}
		//Disable Current active
		this.disableCurrentActive(dashboard.getId());
		dashboard.setActive(true);
		dashboard.addOrUpdate();
		this.dashboardRepository.save(dashboard);
		
		return dashboard;
		
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	@ApiOperation(value="Create DashBoard")
	public @ResponseBody Dashboard createDashboard(@RequestBody Dashboard dashboard) throws Exception {
		
		 IdentifierTools.newVersion(dashboard);
		//dashboard.setId(IdentifierTools.getIdFromNamedModelClass(dashboard));
		dashboard.addOrUpdate();

		// Ensure that the current user is assigned as the owner of the new board.
		// By default also add the administrators group as a owner.
		dashboard.setPermissions(this.securityTool.initPermissions(dashboard.getPermissions()));
		dashboard.setActive(true);
		//Disable Current Active
		if(dashboard.isActive()) {
			this.disableCurrentActive(dashboard.getId());
		}
		
		dashboardRepository.save(dashboard);
		this.cacheInvalidationManager.invalidate(DashboardCache.TYPE, dashboard.getId());
		return dashboard;
	}

	// TO DO Reintroduce security once we figure out a fix for saving user dashboards.
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{dashboardId}", method=RequestMethod.PUT)
	@ApiOperation(value="Edit DashBoard")
	public @ResponseBody Dashboard updateDashboard(@RequestBody Dashboard dashboard, @PathVariable String dashboardId) throws Exception {
		Dashboard existing = getDashboard(dashboardId);
		copyDashboard(dashboard, existing);
		existing.addOrUpdate();
		//Disable Current Active , Set the Updating dashboard to Active
		if(dashboard.isActive()) {
			this.disableCurrentActive(dashboard.getId());
			existing.setActive(true);
		}
		dashboardRepository.save(existing);
		this.cacheInvalidationManager.invalidate(DashboardCache.TYPE, dashboardId);
		return dashboard;
	}
	
	protected Dashboard copyDashboard(Dashboard from, Dashboard to) {
		if (StringUtils.isNotEmpty(from.getDescription())) 
			to.setDescription(from.getDescription());
		if (StringUtils.isNotEmpty(from.getIcon_path()))
			to.setIcon_path(from.getIcon_path());
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		if (from.getPermissions() != null)
			to.setPermissions(from.getPermissions());
		if (from.getUi() != null)
			to.setUi(from.getUi());
		if (from.getWidgets() != null)
			to.setWidgets(from.getWidgets());
		return to;
	}
	
	@RequestMapping(value = "", method=RequestMethod.GET)
	@ApiOperation(value="List DashBoards")
	public @ResponseBody Map<String,Dashboard> listDashboards(@RequestParam(required = false,defaultValue = "false") boolean all) throws Exception {
		
		if(all) {
			return listAllDashBoards();
		}
		
		Map<String, String> map = this.dashboardCache.list();
		filterMap(map,"READ,WRITE,ADMIN");
		Map<String,Dashboard> resultSet = new HashMap<String,Dashboard>();
		for( String id : map.keySet())
		{
			Dashboard item = dashboardCache.getItem(id);
			if(item!=null){
				resultSet.put(id,item);
			}
		}
		return resultSet;
	}

	/**
	 * map<String boardId, String description>
	 */
	public void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String dashboardId = it.next();
			Map<String, String> permissions = this.dashboardCache.getItem(dashboardId).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}

	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'ADMIN')")
	@RequestMapping(value = "/{dashboardId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete DashBoard")
	public @ResponseBody void deleteDashboard(@PathVariable String dashboardId) throws Exception {
		
		if( StringUtils.isEmpty(dashboardId)){
			return;
		}
		
		Dashboard existing = getDashboard(dashboardId);
		//dashboardRepository.delete(existing);
		existing.setActive(false);
		this.dashboardRepository.save(existing);
		this.cacheInvalidationManager.invalidate(DashboardCache.TYPE, dashboardId);
	}
	
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{dashboardId}/permissions", method=RequestMethod.GET)
	@ApiOperation(value="Get DashBoard Permissions")
	public @ResponseBody Map<String,String> getPermissions(@PathVariable String dashboardId) throws Exception {
		Dashboard dashboard = dashboardCache.getItem(dashboardId);
		return dashboard.getPermissions();
	}

	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'ADMIN')")
	@RequestMapping(value = "/{dashboardId}/permissions", method=RequestMethod.POST)
	@ApiOperation(value="Add DashBoard Permissions")
	public @ResponseBody void addPermissions(@PathVariable String dashboardId, @RequestBody Map<String,String> permissions) throws Exception {

		Dashboard dashboard = getDashboard(dashboardId);
		Map<String, String> perms = dashboard.getPermissions();
		if (perms==null) {
			perms = new HashMap<String,String>();
			dashboard.setPermissions(perms);
		}

		for (Entry<String, String> entry : permissions.entrySet()) {
			perms.put(entry.getKey(), entry.getValue());
		}
		dashboardRepository.save(dashboard);
		this.cacheInvalidationManager.invalidate(DashboardCache.TYPE, dashboardId);
	}
	
	@PreAuthorize("hasPermission(#dashboardId, 'DASHBOARD', 'ADMIN')")
	@RequestMapping(value = "/{dashboardId}/permissions/{member}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete DashBoard Permission")
	public @ResponseBody void deleteRole(@PathVariable String dashboardId, @PathVariable String member) throws Exception {

		Dashboard dashboard = getDashboard(dashboardId);
		Map<String, String> perms = dashboard.getPermissions();

		perms.remove(member);
		dashboardRepository.save(dashboard);
		this.cacheInvalidationManager.invalidate(DashboardCache.TYPE, dashboardId);
	}
	
	private  Map<String,Dashboard> listAllDashBoards(){
		 Map<String,Dashboard> result=new HashMap<>();
		 
		 List<Dashboard> dashboardList = this.dashboardRepository.findAll();
		 
		 for(Dashboard dashboard:dashboardList) {
			 Map<String, String> permissions =dashboard.getPermissions();
			 //If User has access
			 if(this.securityTool.isAuthorised(permissions, "READ,WRITE,ADMIN")){
				 result.put(dashboard.getId(), dashboard);
			}
		 }
		 return result;
	}
	
	private void disableCurrentActive(String dashBoardId) {
		Dashboard dashboard=this.dashboardRepository.findByIdAndActiveTrue(dashBoardId);
		if(dashboard!=null) {
			dashboard.setActive(false);
			dashboard.addOrUpdate();
			this.dashboardRepository.save(dashboard);
		}
		
	}
}
