/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.repository.RuleRepository;
import nz.devcentre.gravity.services.QueryService;

@Service
public class RuleCache extends CacheImpl<Rule> {
	
	private static final Logger LOG = LoggerFactory.getLogger(RuleCache.class);
	
	public static final String TYPE = "RULE";

	@Autowired 
	QueryService queryService;
	
	@Autowired
	RuleRepository ruleRepository;
	
	@Override
	protected Rule getFromStore(String... itemIds) throws Exception {
		
		LOG.info("Get Rule Store: "+ getStringFromArray(itemIds));
		String boardId = itemIds[0];
		String templateId = itemIds[1];
		Rule rule = ruleRepository.findByBoardIdAndIdAndActiveTrue(boardId, templateId);
		return rule;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {
		
		LOG.info("Get Rule List Store: "+ getStringFromArray(prefixes));
		Map<String,String> result = new TreeMap<String,String>();
		String boardId = prefixes[0];
		List<Rule> rules = ruleRepository.findByBoardIdAndActiveTrue(boardId);
		for (Rule rule : rules) {
			result.put(rule.getId(), rule.getName());
		}
		return result;
	}
	
	protected Map<String, String> getAllRulesForBoard(String boardId){
		Map<String,String> result = new TreeMap<String,String>();
		List<Rule> rules = this.ruleRepository.findByBoardId(boardId);
		for (Rule rule : rules) {
			result.put(rule.getId(), rule.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
}
