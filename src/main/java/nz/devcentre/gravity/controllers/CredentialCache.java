/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.repository.CredentialRepository;

@Service
public class CredentialCache extends CacheImpl<Credential> {

	private static final Logger logger = LoggerFactory.getLogger(CredentialCache.class);
	
	public static final String TYPE = "CRED";
		
	@Autowired
	private CredentialRepository credentialRepository;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Override
	protected Credential getFromStore(String... itemIds) throws Exception {
		Credential cred = credentialRepository.findOne(itemIds[0]);
		return cred;
	}
	
	public void updateRefreshToken( String credId, String refreshToken) throws Exception {
		Credential credential = this.getItem(credId);
		credential.getResponseFields().put("refresh_token", refreshToken);
		this.credentialRepository.save(credential);
		this.cacheInvalidationManager.invalidate(TYPE, credId);
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {
		
		logger.info("Getting Credential List");
		
		List<Credential> creds = credentialRepository.findAll();
		
		Map<String,String> result = new TreeMap<String,String>();
		for (Credential cred: creds)
			result.put(cred.getId(), cred.getName());
		
		return result;
	}

	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}

}
