/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.BoardResource;
import nz.devcentre.gravity.repository.BoardResourceRepository;
import nz.devcentre.gravity.services.QueryService;

/**
 * List Cache
 * 
 * Purpose of this class is to keep lists available.
 * 
 * @author peter
 */
@Service
public class ResourceCache extends CacheImpl<String> {
	
	public static final String TYPE = "RESOURCE";
		
	@Autowired 
	QueryService queryService;
	
	@Autowired
	BoardResourceRepository boardResourceRepository;
				
	@Override
	protected String getFromStore(String... itemIds) throws Exception {
		String boardId = itemIds[0];
		String resourceId = itemIds[1];
		BoardResource resource = boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);

		if( resource.getFile()!=null) {
			Binary file = resource.getFile();
			byte[] data = file.getData();
			return new String(data, "UTF-8");
		}
		
		if( resource.getResource()!=null) {
			return resource.getResource();
		}
		
		return null;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		Map<String,String> result = new TreeMap<String,String>();
		String boardId = prefixs[0];
		List<BoardResource> resources = boardResourceRepository.findByBoardIdAndActiveTrue(boardId);//Fetch Only Active Entries
		for (BoardResource resource: resources) {
			result.put(resource.getId(), resource.getName());
		}
		return result;
	}
	
	protected Map<String, String> getAllResourceForBoard(String boardId) throws Exception {
		Map<String,String> result = new TreeMap<String,String>();
		List<BoardResource> resources = boardResourceRepository.findByBoardId(boardId);//Fetch All Entries(Active & Inactive)
		for (BoardResource resource: resources) {
			result.put(resource.getId(), resource.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
	
	public List<BoardResource> getBoardResource(String boardId) throws Exception {
		List<BoardResource> resources = boardResourceRepository.findByBoardId(boardId);//Fetch All Entries(Active & Inactive)
		return resources;
	}
	
}
