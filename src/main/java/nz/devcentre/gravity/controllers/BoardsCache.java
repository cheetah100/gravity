/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.repository.BoardRepository;
import nz.devcentre.gravity.security.SecurityTool;

@Service
public class BoardsCache extends CacheImpl<Board> {

	private static final Logger logger = LoggerFactory.getLogger(BoardsCache.class);
	
	public static final String TYPE = "BOARD";

	@Autowired
	BoardRepository boardRepository;
	
	@Autowired
	SecurityTool securityTool;

	@Override
	protected Board getFromStore(String... itemIds) throws Exception {
		String boardId = itemIds[0];
		Board board = null;
		board = boardRepository.findByIdAndActiveTrue(boardId);
		return board;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {
		
		logger.info("Getting Board List");
		
		Map<String,String> result = new TreeMap<String,String>();
		List<Board> list = boardRepository.findByActiveTrue();
		for (Board board: list)  {
				result.put(board.getId(), board.getName());
		}
		return result;
	}
	
	public List<Board> getAllBoards() throws Exception {
		List<Board> returnList= new ArrayList<Board>();
		Map<String, String> list = list(null);
		for( Entry<String,String> e : list.entrySet()) {
			returnList.add(getItem(e.getKey()));
		}
		return returnList;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
}
