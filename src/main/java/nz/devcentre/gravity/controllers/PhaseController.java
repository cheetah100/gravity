/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.repository.BoardRepository;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/phases")
public class PhaseController {

	private static final Logger logger = LoggerFactory.getLogger(PhaseController.class);

	@Autowired
	private CardService cardService;

	@Autowired
	private BoardsCache boardsCache;

	@Autowired
	private QueryService queryService;

	@Autowired
	private CardLockInterface cardsLockCache;
	
	@Autowired 
	private BoardsCache boardCache;
		
	@Autowired
	private BoardService boardTools;
	
	@Autowired
	private BoardRepository boardRepository;

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{phaseId}", method = RequestMethod.GET)
	public @ResponseBody Phase getPhase(@PathVariable String boardId, @PathVariable String phaseId) throws Exception {

		Phase phase = null;
		Board board = boardCache.getItem(boardId);
		Map<String, Phase> phases = board.getPhases();
		if (phases == null) {
			throw new ResourceNotFoundException("Phase not found, id: " + Encode.forHtmlContent(phaseId));
		}

		phase = phases.get(phaseId);
		if (phase == null) {
			throw new ResourceNotFoundException("Phase not found, id: " + Encode.forHtmlContent(phaseId));
		}
		return phase;
	}
	
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{phaseId}/history", method = RequestMethod.GET)
	public @ResponseBody List<Map<String, Object>> getPhaseHistory(@PathVariable String boardId, @PathVariable String phaseId) throws Exception {

		List<Map<String, Object>> result=new LinkedList<Map<String,Object>>();
		List<Board> boardList = this.boardRepository.findById(boardId);
		for(Board  board:boardList) {
			
			Map<String, Phase> phase=board.getPhases();
			if(phase!=null) {
				Map<String,Object> currentEntry=new LinkedHashMap<>();
				currentEntry.put("version", board.getVersion());
				currentEntry.put("active", board.isActive());
				currentEntry.put("metadata", board.getMetadata());
				currentEntry.put("template", phase);
				result.add(currentEntry);
			}
		}
		return result;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody Phase createPhase(@PathVariable String boardId, @RequestBody Phase phase) throws Exception {

		String newId = IdentifierTools.getIdFromNamedModelClass(phase);
		phase.setId(newId);

		Board board = boardCache.getItem(boardId);
		Map<String, Phase> boardPhases = board.getPhases();
		if (boardPhases == null) {
			boardPhases = new HashMap<String, Phase>();
			board.setPhases(boardPhases);
		}

		if (boardPhases.get(phase.getId()) != null) {
			throw new ValidationException("Phase already exists: " + Encode.forHtmlContent(phase.getId()));
		}

		boardPhases.put(phase.getId(), phase);
		
		//Disable current active version and generate new version for Board
		boardTools.disableCurrentActiveAndCreateNewVersion(board);
		board.addOrUpdate();
		boardTools.saveBoard(board);
		return phase;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{phaseId}", method = RequestMethod.PUT)
	public @ResponseBody Phase updatePhase(@PathVariable String boardId, @PathVariable String phaseId,
			@RequestBody Phase phase) throws Exception {

		Board board = boardCache.getItem(boardId);
		board.getPhases().put(phaseId, phase);		
		boardTools.saveBoard(board);
		return phase;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Collection<Phase> getPhaseList(@PathVariable String boardId) throws Exception {
		List<Phase> result = new ArrayList<Phase>();
		Board board = boardCache.getItem(boardId);
		Map<String, Phase> phases = board.getPhases();
		if (phases == null)
			return result;
		for (Phase phase : phases.values()) {
			result.add(phase);
		}
		return result;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{phaseId}", method = RequestMethod.DELETE)
	public @ResponseBody void deletePhase(@PathVariable String boardId, @PathVariable String phaseId) throws Exception {

		if (StringUtils.isEmpty(boardId) || StringUtils.isEmpty(phaseId)) {
			throw new Exception("Parameters Not Set");
		}
		Board board = boardCache.getItem(boardId);
		Phase phase = getPhase(boardId, phaseId);
		
		/*
		 * Check if there is at least one visibile phase If there is no SUCH PHASE
		 * prevent user from deleting
		 */
		long visiblePhaseCount = board.getPhases().entrySet().stream()
				.filter(Phase -> !Phase.getValue().isInvisible() && phase.getId() != Phase.getValue().getId()).count();
		if (visiblePhaseCount < 1) {
			logger.error("No Other Visible Phase Found");
			throw new ValidationException("At Leaset One Visible Phase Should be Available on the Board Template");
		}
		board.getPhases().remove(phase.getId());
		
		//Disable current active version and generate new version for Board
		boardTools.disableCurrentActiveAndCreateNewVersion(board);
		board.addOrUpdate();
		
		boardTools.saveBoard(board);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{phaseId}/cardlist", method = RequestMethod.POST)
	public @ResponseBody List<Card> getCards(@PathVariable String boardId, @PathVariable String phaseId,
			@RequestParam(required = false) String view, @RequestParam(required = false) boolean allcards,
			@RequestBody List<String> cards) throws Exception {

		logger.info("Card List with view " + view);

		// If the phase is invisible no cards are returned.
		// Archived cards must be found by search.

		Board board = boardsCache.getItem(boardId);
		Phase phase = board.getPhases().get(phaseId);
		if (phase.isInvisible() && !allcards) {
			return new ArrayList<Card>();
		}

		List<Card> cardList = new ArrayList<Card>();
		for (String cardId : cards) {
			try {
				Card card = cardService.getCard(boardId, cardId, view);
				if (card != null) {
					cardsLockCache.applyLockState(card);
					cardList.add(card);
				}
			} catch (Exception e) {
				logger.warn("CardList - requested card not found: " + cardId, e);
			}
		}
		return cardList;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{phaseId}/cardsdetail", method = RequestMethod.GET)
	public @ResponseBody Collection<Card> getCards(@PathVariable String boardId, @PathVariable String phaseId,
			@RequestParam(required = false) String filter, @RequestParam(required = false) String view,
			@RequestParam(required = false) boolean allcards) throws Exception {

		Board board = boardsCache.getItem(boardId);
		Phase phase = board.getPhases().get(phaseId);
		if (phase.isInvisible() && !allcards) {
			return new ArrayList<Card>();
		}

		return queryService.query(boardId, phaseId, filter, view, allcards);
	}
	
	protected void copyPhase(Phase from, Phase to) {
		if (from == null || to == null)
			return;
		// don't let the id and deleted flag be updated from here

		if (StringUtils.isNotBlank(from.getDescription()))
			to.setDescription(from.getDescription());
		if (from.getIndex() != null)
			to.setIndex(from.getIndex());
		if (StringUtils.isNotBlank(from.getName()))
			to.setName(from.getName());
	}

	
}
