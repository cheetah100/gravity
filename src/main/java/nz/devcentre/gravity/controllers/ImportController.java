/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.ErrorDetails;
import nz.devcentre.gravity.model.ImportResponse;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.Upload;
import nz.devcentre.gravity.model.UploadSheetErrorDetails;
import nz.devcentre.gravity.model.importer.ImportProcess;
import nz.devcentre.gravity.model.importer.SheetUtil;
import nz.devcentre.gravity.model.importer.TransferStatus;
import nz.devcentre.gravity.model.importer.TransferStatusEnum;
import nz.devcentre.gravity.model.importer.UploadProcess;
import nz.devcentre.gravity.model.importer.UploadService;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;

@RestController
@RequestMapping("/import")
public class ImportController {
	
	private static final Logger logger = LoggerFactory.getLogger(ImportController.class);

	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private SheetUtil sheetUtil;
		
	@Autowired
	public UploadService uploadService;
	
	private TransferStatus status;
	
	//Workbook(Sheet) with Error Cards along with the Error Messages
	
	public ImportController() {
		status = new TransferStatus();
		status.setStatus(TransferStatusEnum.READY);
		status.setMessages(new ArrayList<String>());
		status.setErrorMessage("");
	}
		
	// Permissions enforced within method
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody String uploadFile(@RequestParam MultipartFile file, HttpServletRequest request,
			@RequestParam(required = false) String mode) throws Exception {

		if (file.getSize() == 0) {
			throw new Exception("No Content");
		}

		if (this.status.getStatus().equals(TransferStatusEnum.INPROGRESS)) {
			throw new Exception("Upload Process In Progress");
		}

		InputStream excelFile = file.getInputStream();
		Workbook wb = new XSSFWorkbook(excelFile);

		this.status.newId();
		this.status.setFileName(file.getOriginalFilename());
		this.status.setUser(SecurityTool.getCurrentUser());

		ImportProcess process = new ImportProcess(this, wb, SecurityTool.getCurrentAuthentication(), mode);
		Thread processThread = new Thread(process);
		processThread.start();

		return this.status.getId();
	}

	// Permissions enforced within method
	@ApiOperation(value = "Submit Excel File Upload")
	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	public @ResponseBody String uploadFileSubmit(@RequestParam MultipartFile file, HttpServletRequest request,
			@RequestParam(required = false) String mode) throws Exception {
		
		if (file.getSize() == 0) {
			throw new Exception("No Content");
		}

		InputStream excelFile = file.getInputStream();
		Workbook wb = new XSSFWorkbook(excelFile);

		TransferStatus transferStatus = new TransferStatus();
		transferStatus.newId();
		transferStatus.setFileName(file.getOriginalFilename());

	
		UploadProcess uploadProcess = new UploadProcess(wb, mode, SecurityTool.getCurrentAuthentication(),
				transferStatus, this);
		Thread processThread = new Thread(uploadProcess);
		processThread.start();
		return transferStatus.getId();
	}
	
	@RequestMapping(value = "/upload/board/{boardId}/template/{templateId}/dataxlsx", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	public @ResponseBody ImportResponse uploadXcelFile(
	        @RequestParam MultipartFile file,
	        HttpServletRequest request,
	        @PathVariable String boardId,
	        @PathVariable String templateId,
	        @RequestParam(required=false) String mode) throws Exception {
		
		List<ErrorDetails> errList = new ArrayList<ErrorDetails>();
		ImportResponse importResponse = new ImportResponse();	
		InputStream excelFile = file.getInputStream();
        Workbook wb = new XSSFWorkbook(excelFile);
        Sheet sheet = wb.getSheetAt(0);
        Board board = boardsCache.getItem(boardId);        
        Map<Integer, TemplateField> headerMap = sheetUtil.readHeadersFromSheet(sheet, boardId, board);
        Iterator<Row> iterator = sheet.iterator();
        importResponse.setTotalRows(sheet.getLastRowNum());
        int fail = 0;
        int success = 0;
        
        Card card = null;
        
        try {
        
	        while (iterator.hasNext()) {
	        	Row dataRow = iterator.next();
	        	if(dataRow.getRowNum() == 0){
	        		continue;
	        	}
	        		
	        	try{
		        	card = sheetUtil.readCardRow(dataRow, headerMap);
		        	Card existingCard = null;
		        	boolean idExists = false;
		        	
		        	logger.info("Upload Process: " + card.getFields().toString());
		        	
		        	if(card.getId() != null && !card.getId().equals("")){
		        		try{
		        			existingCard = this.cardService.getCard(boardId, card.getId());
		        			if( existingCard==null ){
		        				idExists = false;
		        			} else {
		        				logger.info("Upload Process: Found existing card" + card.getId());
		        				idExists = true;
		        			}
		        		}catch(Exception e){
		        			idExists = false;
		        			logger.warn("ID Exists",e);
		        		}
		        	}
		        	
		        	if(idExists){
		        		//PUT CALL
		        		String existingPhase = existingCard.getPhase();
		        		//String newPhase = card.getPhase();
		        		
		        		if(!card.getPhase().equalsIgnoreCase(existingPhase)){
		        			logger.info("Upload Process: MOVE CARD" + card.getId()+ " From phase: " +existingPhase +" : To Phase: " +  card.getPhase());
		        			cardService.moveCard(card.getBoard(), card.getId(), card.getPhase(),null);
		        		}
		        		logger.info("Upload Process: EDIT CARD" + card.getId());
		        		cardService.updateCard(card, null, false);
		        		
		        		success++;
		        	}else{
		        		//POST CALL
		        		logger.info("Upload Process: CREATE CARD");
		        		
		        		this.cardService.createCard(boardId, card,false);
		        		success++;
		        		
		        	}
	        	}catch(Exception e){
	        		logger.info("Upload Process: Error Row: " + dataRow.getRowNum() + e.getMessage(), e);
	        		if( card!=null) {
	        			errList.add(new ErrorDetails(""+ dataRow.getRowNum(), card.getFields().toString(), e.getMessage()));
	        		}
	        		fail++;
	        	}
	        }
        } finally {
        }
        
        wb.close();
        importResponse.setUser("");
        importResponse.setFailDetails(errList);
        importResponse.setTotalSuccess(success);
        importResponse.setTotalFail(fail);
		return importResponse;
	}	
	
	/**
	 * Process Import of Sheets
	 * Called by the upload file process
	 * @throws Exception 
	 */
	public void processSheets( Workbook wb, String mode) throws Exception{
		
		this.status.setStatus(TransferStatusEnum.INPROGRESS);
		this.status.setErrorMessage("");
		this.status.getMessages().clear();
		this.status.resetCounts();
        
        int numberOfSheets = wb.getNumberOfSheets();
        
        try {
        for( int sheetno = 0; sheetno<numberOfSheets; sheetno++){
            Sheet sheet = wb.getSheetAt(sheetno);
            String sheetName = sheet.getSheetName();
            int successCount = 0;
            int failCount = 0;
            
            Board board = null;
            try {
            	board = boardsCache.getItem(sheetName);
            } catch( ResourceNotFoundException rnf){
            	logger.warn("Board Not Found: " + sheetName);
            	this.status.getMessages().add("Board Not Found: " + sheetName);
            	continue;
            }
            Map<Integer, TemplateField> headerMap = sheetUtil.readHeadersFromSheet(sheet, sheetName, board);
            Iterator<Row> iterator = sheet.rowIterator();
            Card card = null;
              
	        while (iterator.hasNext()) {
	        	Row dataRow = iterator.next();
	        	
	        	//Skip the Header Row
	        	if(dataRow.getRowNum() == 0){
	        		continue;
	        	}
	        
	        	//if the entire row is blank need to Skip the row from processing
	        	if(dataRow==null || dataRow.getPhysicalNumberOfCells()==0) {
	        		logger.info("Blank Row found So ignore");
	        		continue;
	        	}
	
	        	try{
		        	card = sheetUtil.readCardRow(dataRow, headerMap);
		        	Card existingCard = null;
		        	
		        	logger.info("Upload Process: " + card.getFields().toString());
		        	
		        	if(card.getId() != null && !card.getId().equals("")){
		        		try{
		        			existingCard = this.cardService.getCard(sheetName, card.getId());
		        			logger.info("Upload Process: Found existing card: " + card.getId());
		        		}catch(ResourceNotFoundException e){
		        			logger.info("Upload Process: Card not found: " + card.getId());
		        		}
		        	}
		        	if(existingCard!=null){
		        		//PUT CALL
		        		String existingPhase = existingCard.getPhase();
		        		
		        		if(!card.getPhase().equalsIgnoreCase(existingPhase)){
		        			logger.info("Upload Process: MOVE CARD" + card.getId()+ " From phase: " +existingPhase +" : To Phase: " +  card.getPhase());
		        			cardService.moveCard(sheetName, card.getId(), card.getPhase(),null);
		        		}
		        		logger.info("Upload Process: EDIT CARD" + card.getId());
		        		cardService.updateCard(card, null, false);
		        		
		        		this.status.incTotalSuccess();
		        		successCount++;
		        	}else{
		        		//POST CALL
		        		logger.info("Upload Process: CREATE CARD");
		        		
		        		this.cardService.createCard(sheetName, card,false);
		        		this.status.incTotalSuccess();
		        		successCount++;
		        	}
	        	}catch(Exception e){
	        		
	        		this.status.incTotalFailed();
	        		failCount++;
	        		
	        		String st = ExceptionUtils.getStackTrace(e);
					logger.warn("Upload Process: Error Row: " + dataRow.getRowNum() + e.getMessage() + " StackTrace: \n" + st);
	        		
					StringBuilder msg = new StringBuilder();
					
	        		msg.append("Row: " + dataRow.getRowNum());
	        		if(card != null)
	        			msg.append( " Card: " + card.getId());
	        		
	        		if(e.getMessage()!=null){
	        			msg.append(" " + e.getMessage());
	        		}
	        		
	        		if(e.getCause()!=null){
	        			msg.append(" " + e.getCause().getMessage());
	        		}

	        		logger.warn("Upload Process: Message: " + msg);
	        		this.status.getMessages().add(msg.toString());
	        	}
	        }
	        logger.info("Upload Process Complete: for sheet " + sheetName);
	        if( successCount > 0){
	        	this.status.getMessages().add("Nicely done! " + successCount + " " + sheetName + " were imported successfully.");
	        }
	        if( failCount > 0){
	        	this.status.getMessages().add("Unfortunately a total of " + failCount + " " + sheetName + " had an error.  Please review the details below, correct the specified issues, and try importing a delta file to factor in the rest of your changes. Be sure you don't attempt to add any records that were already imported successfully or you will create duplicate records.");
	        }
        } 
        logger.info("Upload Process Complete: for Workbook" );
        } finally {
        	wb.close();
        	this.status.setStatus(TransferStatusEnum.READY);
        }
	}
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public @ResponseBody TransferStatus status() throws Exception {
		return this.status;
	}
	
	@ApiOperation(value = "status of the uploads done by user")
	@RequestMapping(value = "/status/myupload", method = RequestMethod.GET)
	public @ResponseBody List<Upload> getUploadForUser(){
		return this.uploadService.getUploadForUser();
	}
	
	@ApiOperation(value = "filter by status of the uploads done by user")
	@RequestMapping(value = "/status/myupload/{status}", method = RequestMethod.GET)
	public @ResponseBody List<Upload> getUploadForUser(@PathVariable TransferStatusEnum status){
		return this.uploadService.getUploadForUserByStatus(status);
	}
	
	@ApiOperation(value = "Error Records by Workbook for currrent user")
	@RequestMapping(value = "/status/myupload/{uploadId}/errors", method = RequestMethod.GET)
	public @ResponseBody List<UploadSheetErrorDetails> getUploadErrorsForUserByUploadId(@PathVariable String uploadId){
		return this.uploadService.getUploadForError(uploadId,null);
	}
	
	@ApiOperation(value = "Error Records by SheetId for current user")
	@RequestMapping(value = "/status/myupload/{uploadId}/{uploadSheetId}/errors", method = RequestMethod.GET)
	public @ResponseBody List<UploadSheetErrorDetails> getUploadErrorsForUserByUploadId(@PathVariable String uploadId,@PathVariable String uploadSheetId){
		return this.uploadService.getUploadForError(uploadId,uploadSheetId);
	}
	
}
