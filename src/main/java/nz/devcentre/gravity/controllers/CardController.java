/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited 
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.transfer.CardResult;
import nz.devcentre.gravity.model.transfer.CardUpdateEnvelope;
import nz.devcentre.gravity.model.transfer.MoveCard;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.services.TaskService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.FilterTools;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/cards")
public class CardController {

	private static final Logger logger = LoggerFactory.getLogger(CardController.class);
	
	private static final String READ_ACCESS = "READ,WRITE,ADMIN";

	@Autowired
	private BoardsCache boardsCache;

	@Autowired
	private CardService cardService;

	@Autowired
	private QueryService queryService;

	@Autowired
	private CardLockInterface cardsLockCache;

	@Autowired
	private CardListener cardListener;

	@Autowired
	private SecurityTool securityTool;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private BoardService boardTools;
	
	@Autowired
	TaskService taskService;

	protected String[] arr = { "created", "creator", "modified", "modifiedby", "template", "phase", "color", "alerted",
			"deleted" };
	
	protected List<String> metaFields = Arrays.asList(arr);

	// Permissions enforced within method
	@ApiOperation(value = "Get Card")
	@RequestMapping(value = "/{cardId}", method = RequestMethod.GET)
	public @ResponseBody Card getCard(@PathVariable String boardId, @PathVariable String cardId,
			@RequestParam(required = false) String view, @RequestParam(required = false) boolean validateReferences,
			@RequestParam(required = false) boolean allfields) throws Exception {

		Board board = boardsCache.getItem(boardId);
		if (StringUtils.isEmpty(view) || "null".equals(view)) {
			view = null;
		}
		if (!securityTool.isAuthorizedViewAndFilter(view, null, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		if (StringUtils.isEmpty(cardId)) {
			throw new ResourceNotFoundException("Card Not Found - Card ID Not Supplied");
		}

		Card card = cardService.getCard(boardId, cardId, view);

		if (card == null) {
			throw new ResourceNotFoundException("Card Not Found: " + boardId + "." + cardId);
		}

		if (validateReferences) {
			cardService.validateCardReferenceFields(boardId, card, validateReferences);
		}

		cardsLockCache.applyLockState(card);
		return card;
	}

	public Card getCard2(String boardId, String cardId) throws Exception {

		if (StringUtils.isEmpty(cardId)) {
			throw new ResourceNotFoundException("Card Not Found - Card ID Not Supplied");
		}

		Card card = null;
		card = cardService.getCard(boardId, cardId);

		if (card == null) {
			throw new ResourceNotFoundException("Card Not Found: " + boardId + "." + cardId);
		}

		/* TODO REMOVE THIS CODE
		templateCache.applyTemplate(card, false);
		*/
		
		cardsLockCache.applyLockState(card);
		
		return card;
	}

	@ApiOperation(value = "Trigger Automation on Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{cardId}/examine", method = RequestMethod.GET)
	public @ResponseBody Boolean examineCard(@PathVariable String boardId, @PathVariable String cardId) {
		logger.info("Automation to Examine Card: {}.{} ", boardId, cardId);
		CardHolder cardHolder = new CardHolder(boardId, cardId);
		cardListener.addCardHolder(cardHolder);
		return true;
	}
	
	/*
	@ApiOperation(value = "Execute Rule on Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{cardId}/ruleexecute/{ruleId}", method = RequestMethod.GET)
	public @ResponseBody String executeRuleOnCard(@PathVariable String boardId, @PathVariable String cardId,
			@PathVariable String ruleId)
			throws Exception {
		logger.info("Executing Rule on Card: {}.{}.{}", boardId, cardId, ruleId);
	
		// Check that the board exists
		this.boardsCache.getItem(boardId);
		
		// Check that the card exists
		Card card = this.cardTools.getCard(boardId, cardId);
		if (card == null) {
			throw new ResourceNotFoundException("Card Not Found: " + boardId + "." + cardId);
		}
		
		// Check that the rule exists
		Rule rule = ruleCache.getItem(boardId, ruleId);
		
		// Pre-execution evaluations
		Map<String, Rule> rules = new HashMap<>();
		rules.put(rule.getId(), rule);
		automationEngine.checkTaskRuleAssignments(rules, card);
		automationEngine.evaluateTaskRules(rules, card);
		Map<String, Boolean> ruleExplanation = this.cardTools.explain(boardId, 
				cardId).get(rule.getName());
		
		RuleType ruleType = rule.getRuleType();
		if (ruleType.equals(RuleType.TASK)) {
			// Check if task meets conditions and was added to the card
			Collection<CardTask> tasks = taskService.getTasks(card.getBoard(), card.getId());
			CardTask task = null;
			for (CardTask curTask: tasks) {
				if (rule.getId().equals(curTask.getTaskid())) {
					task = curTask;
					break;
				}
			}
			if (task == null) {
				return "Task conditions not met for Rule: " + ruleId + 
						" on Card: " + boardId + "." + cardId + ". Explanation: " +						
						automationEngine.getRuleExplanationStr(ruleExplanation); 
			}

			if (rule.getAutomationConditions() == null || 
					automationEngine.conditionsMet(card, rule, rule.getAutomationConditions(), null, null)) {
				automationEngine.executeActions(card, rule);
				this.taskService.taskAction(task, TaskAction.COMPLETE, null);
			} else {
				return "Automation conditions not met for Rule: " + ruleId + 
						" on Card: " + boardId + "." + cardId + ". Explanation; " +
						automationEngine.getRuleExplanationStr(ruleExplanation);
			}
		} else {
			if (rule.getAutomationConditions() == null || 
					automationEngine.conditionsMet(card, rule, rule.getAutomationConditions(), null, null)) {
				automationEngine.executeActions(card, rule);
			} else {
				return "Automation conditions not met for Rule: " + ruleId + 
						" on Card: " + boardId + "." + cardId + ". Explanation: " +
						automationEngine.getRuleExplanationStr(ruleExplanation);
			}
		}
		return "Successfully executed Rule: " + ruleId + " on Card: " + boardId + "." + cardId;
	}
	*/

	@ApiOperation(value = "Explain Current Automation State")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{cardId}/explain", method = RequestMethod.GET)
	public @ResponseBody Map<String, Map<String, Boolean>> explainCard(@PathVariable String boardId,
			@PathVariable String cardId) throws Exception {
		logger.info("Explain Card: {}.{}", boardId, cardId);
		return this.cardService.explain(boardId, cardId);
	}
	
	@ApiOperation(value = "Move Card to Another Phase with Reason")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{cardId}/move", method = RequestMethod.POST)
	public @ResponseBody Card moveCardWithReasons(@PathVariable String boardId, 
			@PathVariable String cardId, @RequestBody MoveCard move) throws Exception {
		
		return cardService.moveCard(boardId, cardId, move.getPhase(), move.getReason());
	}

	@ApiOperation(value = "Create New Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody Card createCard(@PathVariable String boardId, @RequestBody Card card,
			@RequestParam(required = false, value = "allfields") boolean allfields) throws Exception {

		return this.cardService.createCard(boardId, card, allfields);		
	}

	@ApiOperation(value = "Create Multiple Cards")
	@RequestMapping(value = "/all", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	public @ResponseBody List<CardResult> createAllCards(@PathVariable String boardId, @RequestBody List<Card> cards,
			@RequestParam(required = false, value = "allfields") boolean allfields) throws Exception {

		List<CardResult> results = new ArrayList<>();

		if (cards != null) {
			for (Card card : cards) {
				try {
					Card result = cardService.createCard(boardId, card, allfields);
					CardResult cardResult = new CardResult();
					cardResult.setCard(result);
					cardResult.setSuccess(true);
					results.add(cardResult);
				} catch (Exception e) {
					logger.warn("Error in Bulk Add/Edit.", e);
					CardResult cardResult = new CardResult();
					cardResult.setCard(card);
					cardResult.setSuccess(false);
					cardResult.setError(e.getClass().getSimpleName() + " " + e.getMessage());
					results.add(cardResult);
				}
			}
		}
		return results;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@ApiOperation(value = "Create Card from Map")
	@RequestMapping(value = "/fields/{templateId}", method = RequestMethod.POST)
	public @ResponseBody Card createCardFromFields(@PathVariable String boardId, @PathVariable String templateId,
			@RequestParam(required = false, defaultValue = "white") String color, @RequestBody Map<String, Object> data)
			throws Exception {

		logger.info(data.toString());
		Card card = new Card();
		card.setFields(data);
		return cardService.createCard(boardId, card, false);
	}

	// Permissions enforced within method
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@ApiOperation(value = "Get Cards")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Collection<Card> getCards(@PathVariable String boardId,
			@RequestParam(required = false) String phaseId, 
			@RequestParam(required = false) String filter,
			@RequestParam(required = false) String view, 
			@RequestParam(required = false) String order,
			@RequestParam(required = false) boolean allfields, 
			@RequestParam(required = false) boolean allcards,
			@RequestParam(required = false) boolean descending)
			throws Exception {

		// TODO Is it really a good idea to hide invisible phases when expressly
		// requested?

		Board board = boardsCache.getItem(boardId);

		if (!securityTool.isAuthorizedViewAndFilter(view, filter, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		if (phaseId != null) {
			Phase phase = board.getPhases().get(phaseId);
			if (phase == null) {
				throw new ResourceNotFoundException("Phase Not Found");
			}
			if (phase.isInvisible() && !allcards) {
				return new ArrayList<Card>();
			}
		}

		Collection<Card> result = null;
		result = queryService.query(boardId, phaseId, filter, view, allcards);		
		View viewObject = board.getView(view);
		return CardTools.orderCards(result, order, descending, viewObject, board);
	}

	// Permissions enforced within method
	@ApiOperation(value = "Get Cards with Dynamic Filter Value Replacement")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public @ResponseBody Collection<Card> getCardsDynamicFilter(@PathVariable String boardId,
			@RequestParam(required = false) String phaseId, 
			@RequestParam(required = false) String filter,
			@RequestParam(required = false) String view, 
			@RequestParam(required = false) String order,
			@RequestParam(required = false) boolean allfields, 
			@RequestParam(required = false) boolean allcards,
			@RequestParam(required = false) boolean allphases,
			@RequestParam(required = false) boolean descending,
			@RequestBody Map<String, Object> parameters)
			throws Exception {

		// TODO Is it really a good idea to hide invisible phases when expressly
		// requested?

		Board board = boardsCache.getItem(boardId);

		if (!securityTool.isAuthorizedViewAndFilter(view, filter, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		if (phaseId != null) {
			Phase phase = board.getPhases().get(phaseId);
			if (phase == null) {
				throw new ResourceNotFoundException("Phase Not Found");
			}
			if (phase.isInvisible() && !allcards) {
				return new ArrayList<>();
			}
		}

		Filter filterObj = board.getFilter(filter);
		Filter dynamicFilter = FilterTools.replaceFilterValues(filterObj, parameters);
		View viewObj = board.getView(view);
		Collection<Card> result = null;
		result = queryService.dynamicQuery(boardId, phaseId, dynamicFilter, viewObj, allphases);		
		View viewObject = board.getView(view);
		return CardTools.orderCards(result, order, descending, viewObject, board);
	}

	@ApiOperation(value = "Delete Card [Admin Only]")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/{cardId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteCard(@PathVariable String boardId, @PathVariable String cardId) throws Exception {

		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);
		DBObject query = new BasicDBObject();
		query.put("_id", cardId);
		collection.remove(query);
		cardListener.addCardHolder(new CardHolder(boardId, cardId));
	}

	/*
	@ApiOperation(value = "Update Card Field")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{cardId}/fields/{field}", method = RequestMethod.POST)
	public @ResponseBody void updateField(@PathVariable String boardId, @PathVariable String cardId,
			@PathVariable String field, @RequestBody Map<String, Object> body) throws Exception {

		if (cardsLockCache.isLocked(boardId, cardId)) {
			logger.info("Card is Locked : {}.{}", boardId, cardId);
			throw new Exception("Card is Locked");
		}
		Object value = getField("value", body);
		Map<String, CardEventField> fieldMap = new HashMap<String, CardEventField>();
		CardEventField cef = updateValue(boardId, cardId, field, value);
		if (cef != null) {
			fieldMap.put(cef.getFieldName(), cef);
			this.cardTools.storeCardEvent(null, boardId, cardId, "info", "updateField",
					SecurityToolImpl.getCurrentUser(), new Date(), fieldMap);

		}
		cardTools.updateModified(boardId, cardId);
		cardsLockCache.unlock(boardId, cardId);
	}
	*/

	// @PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	// @RequestMapping(value = "/{cardId}", method=RequestMethod.PUT)
	// @ApiOperation(value="Update Card")
	public @ResponseBody Card putCard(@PathVariable String boardId, @PathVariable String cardId, @RequestBody Card card,
			@RequestParam(required = false) String view) throws Exception {
		return this.cardService.updateCard(card, view, false);
	}

	@ApiOperation(value = "Update Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{cardId}", method = RequestMethod.PUT)
	public @ResponseBody Card updateCard(@PathVariable String boardId, @PathVariable String cardId,
			@RequestBody Map<String, Object> body, @RequestParam(required = false) String view,
			@RequestParam(required = false) boolean allfields) throws Exception {
		
		Card card = new Card();
		card.setBoard(boardId);
		card.setId(cardId);
		card.setFields(body);
		
		return this.cardService.updateCard(card, view, allfields);
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/bulkupdate", method = RequestMethod.PUT)
	@ApiOperation(value = "Bulk Update Multiple Cards with Same Values")
	public @ResponseBody Map<String, String> updateCards(@PathVariable String boardId,
			@RequestBody CardUpdateEnvelope envelope, @RequestParam(required = false) String view) throws Exception {

		Map<String, String> errors = new HashMap<String, String>();

		for (String id : envelope.getIds()) {
			try {
				Map<String, Object> shallowCopy = new HashMap<String, Object>();
				shallowCopy.putAll(envelope.getFields());
				this.updateCard(boardId, id, shallowCopy, view, false);
			} catch (Exception e) {
				errors.put(id, e.getMessage());
			}
		}
		return errors;
	}

	/*
	public CardEventField updateValue(String boardId, String cardId, String field, Object value) throws Exception {
		logger.info("Setting {} = {}", field, value);
		if (value != null) {
			logger.info(" Value Type: {}", value.getClass().getCanonicalName());
		}

		boolean success = false;
		Card card = this.cardService.getCard(boardId, cardId);
		Map<String, Object> fields = card.getFields();

		CardEventField cef = null;
		if (!metaFields.contains(field)) {
			fields.put(field, value);
			success = true;
		} else {
			// set the field in the metadata object
			String methodName = "set" + field.toLowerCase();
			if (!"deleted".equals(field)) {
				Method[] methods = Card.class.getMethods();
				for (Method method : methods) {
					if (methodName.equals(method.getName().toLowerCase())) {
						method.invoke(card, value);
						success = true;
						break;
					}
				}
			}
		}
		if (success) {
			cardTools.updateCard(card);
		} else {
			logger.error("Error setting field, {} on card {}", field, card.getId());
		}

		return cef;
	}
	*/

	// Permissions enforced within method
	@RequestMapping(value = "/cardlist", method = RequestMethod.POST)
	@ApiOperation(value = "Get Cards by List of Card ID.")
	public @ResponseBody List<Card> getCardListById(@PathVariable String boardId,
			@RequestParam(required = false) String view, @RequestParam(required = false) boolean allcards,
			@RequestBody List<String> cards) throws Exception {

		Board board = boardsCache.getItem(boardId);
		if (!securityTool.isAuthorizedViewAndFilter(view, null, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		logger.info("Card List with view: {} ", view);
		List<Card> cardList = new ArrayList<Card>();

		for (String cardId : cards) {
			try {
				Card card = cardService.getCard(boardId, cardId, view);
				if (card != null) {
					cardsLockCache.applyLockState(card);
					cardList.add(card);
				}
			} catch (Exception e) {
				logger.warn("CardList - requested card not found: {}", cardId, e);
			}
		}
		return cardList;
	}

	// Overloaded The existing Methods for the Resource Scripts to Work As Expected
	// - Start
	public @ResponseBody Card getCard(@PathVariable String boardId, @PathVariable String cardId,
			@RequestParam(required = false) String view, @RequestParam(required = false) boolean validateReferences)
			throws Exception {
		return this.getCard(boardId, cardId, view, validateReferences, false);
	}

	public @ResponseBody Card createCard(@PathVariable String boardId, @RequestBody Card card) throws Exception {
		return this.cardService.createCard(boardId, card, false);
	}

	public @ResponseBody List<CardResult> createAllCards(@PathVariable String boardId, @RequestBody List<Card> cards)
			throws Exception {
		return this.createAllCards(boardId, cards, false);
	}

	public @ResponseBody Card updateCard(@PathVariable String boardId, @PathVariable String cardId,
			@RequestBody Map<String, Object> body, @RequestParam(required = false) String view) throws Exception {
		return this.updateCard(boardId, cardId, body, view, false);
	}
	// Overloaded The existing Methods for the Resource Scripts to Work As Expected
	// - End
	
	// Permissions enforced within method
	@ApiOperation(value = "Get Cards With Paginated Options")
	@RequestMapping(value = "/paginate", method = RequestMethod.GET)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "phaseId", dataType = "String", required = false, paramType = "query"),
			@ApiImplicitParam(name = "filter", dataType = "String", required = false, paramType = "query"),
			@ApiImplicitParam(name = "view", dataType = "String", required = false, paramType = "query"),
			@ApiImplicitParam(name = "order", dataType = "String", required = false, paramType = "query", value ="Not Used ,Future Enhancement, Currently Sorted on Card ID"),
			@ApiImplicitParam(name = "allfields", dataType = "Boolean", required = false, paramType = "query",value ="Allfields present in Template"),
			@ApiImplicitParam(name = "allcards", dataType = "Boolean", required = false, paramType = "query"),
			@ApiImplicitParam(name = "pageSize", dataType = "Numeric", required = false, paramType = "query",value = "No of Cards On a Single Page",defaultValue="20"),
			@ApiImplicitParam(name = "skip", dataType = "Numeric", required = false, paramType = "query",value = "No of Cards To Skip",defaultValue="0"),
			@ApiImplicitParam(name = "ignorePage", dataType = "Boolean", required = false, paramType = "query",value="No Pagination") })
	public @ResponseBody PaginatedCards getCardsPaginated(@PathVariable String boardId,
			@RequestParam(required = false) String phaseId, @RequestParam(required = false) String filter,
			@RequestParam(required = false) String view, @RequestParam(required = false) String order,
			@RequestParam(required = false) boolean allfields, @RequestParam(required = false) boolean allcards,
			@RequestParam(required = false, defaultValue = "20") long pageSize,
			@RequestParam(required = false, defaultValue = "0") long skip,
			@RequestParam(required=false)boolean ignorePage) throws Exception {

		// TODO Is it really a good idea to hide invisible phases when expressly
		// requested?
		logger.info("Page Size: {}, Record to Skip: {}", pageSize, skip);
		
		Board board = boardsCache.getItem(boardId);

		if (!securityTool.isAuthorizedViewAndFilter(view, filter, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}
		if (phaseId != null) {
			Phase phase = board.getPhases().get(phaseId);
			if (phase == null) {
				throw new ResourceNotFoundException("Phase Not Found");
			}
			if (phase.isInvisible() && !allcards) {
				return new PaginatedCards();
			}
		}
		
		PaginatedCards paginatedCards= this.queryService.query(boardId, phaseId, filter, view, allcards,pageSize,skip,ignorePage);
		
		return paginatedCards;	
	}

	// Permissions enforced within method
	@ApiOperation(value = "Get Cards with Dynamic Filter Value Replacement with Pagination")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "phaseId", dataType = "String", required = false, paramType = "query"),
		@ApiImplicitParam(name = "filter", dataType = "String", required = false, paramType = "query"),
		@ApiImplicitParam(name = "view", dataType = "String", required = false, paramType = "query"),
		@ApiImplicitParam(name = "order", dataType = "String", required = false, paramType = "query", value ="Not Used ,Future Enhancement, Currently Sorted on Card ID"),
		@ApiImplicitParam(name = "allfields", dataType = "Boolean", required = false, paramType = "query",value ="Allfields present in Template"),
		@ApiImplicitParam(name = "allcards", dataType = "Boolean", required = false, paramType = "query"),
		@ApiImplicitParam(name = "pageSize", dataType = "Numeric", required = false, paramType = "query",value = "No of Cards On a Single Page",defaultValue="20"),
		@ApiImplicitParam(name = "skip", dataType = "Numeric", required = false, paramType = "query",value = "No of Cards To Skip",defaultValue="0"),
		@ApiImplicitParam(name = "ignorePage", dataType = "Boolean", required = false, paramType = "query",value="No Pagination") })
	@RequestMapping(value = "/list/paginate", method = RequestMethod.POST)
		public @ResponseBody PaginatedCards getCardsPaginatedWithDynamicFilter(@PathVariable String boardId,
				@RequestParam(required = false) String phaseId, @RequestParam(required = false) String filter,
				@RequestParam(required = false) String view, @RequestParam(required = false) String order,
				@RequestParam(required = false) boolean allfields, @RequestParam(required = false) boolean allcards,
				@RequestParam(required = false, defaultValue = "20") long pageSize,
				@RequestParam(required = false, defaultValue = "0") long skip,
				@RequestParam(required=false)boolean ignorePage,@RequestBody Map<String, Object> parameters) throws Exception {
		// TODO Is it really a good idea to hide invisible phases when expressly
		// requested?

		logger.info("Page Size: {} Record to Skip: {}", pageSize, skip);
		
		Board board = boardsCache.getItem(boardId);

		if (!securityTool.isAuthorizedViewAndFilter(view, filter, board, READ_ACCESS)) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}
		if (phaseId != null) {
			Phase phase = board.getPhases().get(phaseId);
			if (phase == null) {
				throw new ResourceNotFoundException("Phase Not Found");
			}
			if (phase.isInvisible() && !allcards) {
				return new PaginatedCards();
			}
		}

		Filter filterObj = board.getFilter(filter);
		Filter dynamicFilter = FilterTools.replaceFilterValues(filterObj, parameters);
		View viewObj = board.getView(view);
		PaginatedCards paginatedCards = queryService.dynamicQuery(boardId, phaseId, dynamicFilter, viewObj, allcards, pageSize, skip, ignorePage);
		
		/* TODO REMOVE CARDS
		for (Card card:paginatedCards.getData()) {
			templateCache.applyTemplate(card, allfields);
		}
		*/
		return paginatedCards;	
	}
	
	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Get Card References",notes="Returns the Field Used to refer to the Board with the List of Cards")
	@RequestMapping(value = "/{cardId}/references", method = RequestMethod.GET, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")

	public @ResponseBody List<Map<String, Object>> getCardDetailsExtra(@PathVariable String boardId,
			@PathVariable String cardId) throws Exception {
		
		if (StringUtils.isEmpty(cardId)) {
			throw new ResourceNotFoundException("Card Not Found - Card ID Not Supplied");
		}

		Card card = cardService.getCard(boardId, cardId, null);
		if (card == null) {
			throw new ResourceNotFoundException("Card Not Found: " + boardId + "." + cardId);
		}
		
		List<Map<String, Object>> result = new ArrayList<>();

		List<Map<String, Object>> boardReferences = this.boardTools.getBoardRefrences(boardId);

		for (Map<String, Object> boardReference : boardReferences) {
			String currentBoardId = boardReference.get("referringBoard").toString();
			Map<String, Object> currentResult = new LinkedHashMap<>();
			currentResult.put("referringBoard", currentBoardId);
			if (this.boardTools.isUserAccess(currentBoardId, null)) {
				List<TemplateField> fields = (List<TemplateField>) boardReference.get("templateField");
				List<Map<String, Object>> content = new ArrayList<>();
				for (TemplateField tf : fields) {
					Filter filter = FilterTools.createSimpleFilter(tf.getId(), Operation.EQUALTO, cardId);
					Collection<Card> cards = queryService.dynamicQuery(currentBoardId, null, filter, null, false);
					content.add(ImmutableMap.of("templateField", tf, "cards", cards));
				}
				currentResult.put("boardAccess", true);
				currentResult.put("content", content);
			} else {
				currentResult.put("boardAccess", false);
			}
			result.add(currentResult);
		}

		return result;
	}
	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Get Card References",notes="Does not return the Field Used to refer to the Board, returns Consolidated List of Cards irrespective of Field")
	@RequestMapping(value = "/{cardId}/references/consolidated", method = RequestMethod.GET, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	public @ResponseBody List<Map<String, Object>> getCardDetailsConsolidated(@PathVariable String boardId,
			@PathVariable String cardId) throws Exception {
		
		if (StringUtils.isEmpty(cardId)) {
			throw new ResourceNotFoundException("Card Not Found - Card ID Not Supplied");
		}

		Card card = null;
		card = cardService.getCard(boardId, cardId, null);

		if (card == null) {
			throw new ResourceNotFoundException("Card Not Found: " + boardId + "." + cardId);
		}

		List<Map<String, Object>> result = new ArrayList<>();

		List<Map<String, Object>> boardReferences = this.boardTools.getBoardRefrences(boardId);

		for (Map<String, Object> boardReference : boardReferences) {
			String currentBoardId = boardReference.get("referringBoard").toString();
			Map<String, Object> currentResult = new LinkedHashMap<>();
			currentResult.put("referringBoard", currentBoardId);
			Set<Card> cardSet=new HashSet<>();
			if (this.boardTools.isUserAccess(currentBoardId, null)) {
				List<TemplateField> fields = (List<TemplateField>) boardReference.get("templateField");
				for (TemplateField tf : fields) {
					Filter filter = FilterTools.createSimpleFilter(tf.getId(), Operation.EQUALTO, cardId);
					cardSet.addAll(queryService.dynamicQuery(currentBoardId, null, filter, null, false));
				}
				currentResult.put("boardAccess", true);
				currentResult.put("cards", cardSet);
			} else {
				currentResult.put("boardAccess", false);
			}
			result.add(currentResult);
		}

		return result;

	}
	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Examine all the cards referring to the current card",notes="Submit Events to run the Business Rule on the cards which refers to the current card")
	@RequestMapping(value = "/{cardId}/references/examine", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	public @ResponseBody boolean examineCardReferences(@PathVariable String boardId, @PathVariable String cardId)
			throws Exception {
		List<Map<String, Object>> boardReferences = this.boardTools.getBoardRefrences(boardId);
		Map<String, Set<String>> boardCardMap = new HashMap<>();
		//Set Current Authentication User to System as all the cards referring needs to be re-examined 
		this.securityTool.iAmSystem();
		for (Map<String, Object> boardReference : boardReferences) {
			String currentBoardId = boardReference.get("referringBoard").toString();
			List<TemplateField> fields = (List<TemplateField>) boardReference.get("templateField");
			Set<String> cards = new HashSet<>();
			for (TemplateField tf : fields) {
				
				Filter filter = FilterTools.createSimpleFilter(tf.getId(), Operation.EQUALTO, cardId);
				Collection<Card> cardList = queryService.dynamicQuery(currentBoardId, null, filter, null, false);
				Collection<String> cardIdList = new ArrayList<>();
				for( Card card : cardList) {
					cardIdList.add(card.getId());
				}				
				cards.addAll(cardIdList);
			}
			boardCardMap.put(currentBoardId, cards);
		}
		for (Entry<String, Set<String>> entry : boardCardMap.entrySet()) {
			String boardToExamine = entry.getKey();
			Set<String> cardsToExamine = entry.getValue();
			logger.info("Board to Examine: {},  No. of Cards to Examine: ", boardToExamine, cardsToExamine.size());
			for (String card : cardsToExamine) {
				this.examineCard(boardToExamine, card);
			}

		}
		return true;
	}
}
