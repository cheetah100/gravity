/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco Limited
 * (C) Copyright 2021 Devcente Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AccessControlPolicy;
import nz.devcentre.gravity.repository.AccessControlPolicyRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/acp")
@ApiModel(value="Access Control Policy Controller")
public class AccessControlPolicyController {
	
	@Autowired
	private AccessControlPolicyRepository repository;
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@PostMapping(value = "")
	@ApiOperation(value="Create Access Control Policy")
	public @ResponseBody AccessControlPolicy createACP(@RequestBody AccessControlPolicy acp) throws Exception {
		String newId = IdentifierTools.getIdFromNamedModelClass(acp);
		if (repository.exists(newId))
			throw new ValidationException("Access Control Policy Already Exists, id: " + Encode.forHtmlContent(newId));
		
		acp.setId(newId);
		
		// Make current user an owner.
		if( acp.getOwners()==null){
			acp.setOwners(new ArrayList<String>());
		}
		if(!acp.getOwners().contains(SecurityTool.getCurrentUser())){
			acp.getOwners().add(SecurityTool.getCurrentUser());
		}
		
		AccessControlPolicy savedAcp = repository.save(acp);
		return savedAcp;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@GetMapping(value = "/{acpId}")
	@ApiOperation(value="Get Access Control Policy")
	public @ResponseBody AccessControlPolicy getACP(@PathVariable String acpId) throws Exception {
		
		if (StringUtils.isEmpty(acpId))
			throw new ValidationException("Invalid or null ID, id: " + Encode.forHtmlContent(acpId));
		
		AccessControlPolicy acp = repository.findById(acpId);
			
		if(acp==null){
			throw new ResourceNotFoundException();
		}
		
		return acp;	
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@GetMapping(value = "")
	@ApiOperation(value="Get Access Control Policy List")
	public @ResponseBody Map<String,String> list() throws Exception {
		
		List<AccessControlPolicy> acpList = repository.findAll();
		
		Map<String,String> returnMap = new TreeMap<String,String>();
		for( AccessControlPolicy acp : acpList ){
			returnMap.put(acp.getId(), acp.getName());
		}
				
		return returnMap;	
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@PutMapping(value = "/{acpId}")
	@ApiOperation(value="Update Access Control Policy")
	public @ResponseBody AccessControlPolicy updateACP(@PathVariable String acpId, @RequestBody AccessControlPolicy acp) throws Exception {
		if(!acp.getId().equals(acpId)){
			throw new ValidationException("Invalid or null ID, id: " + Encode.forHtmlContent(acpId));
		}
		return repository.save(acp);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@DeleteMapping(value = "/{acpId}")
	@ApiOperation(value="Delete Access Control Policy")
	public @ResponseBody void updateACP(@PathVariable String acpId) throws Exception {
		repository.delete(acpId);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@GetMapping(value = "/test/{targetType}/id/{targetId}/access/{reqAccess}")
	@ApiOperation(value="Test Access Control Policy")
	public @ResponseBody Boolean isAuthorisedAcp( @PathVariable String targetType,   
			@PathVariable String targetId, @PathVariable List<String> reqAccess) throws Exception {
		throw new Exception("Method not implemented");
		// return securityTool.isAuthorisedAcp(securityTool.getCurrentAuthentication(), targetId, targetType, reqAccess);
	}

}
