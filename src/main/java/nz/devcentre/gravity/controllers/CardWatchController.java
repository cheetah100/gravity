/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardWatch;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.repository.CardWatchRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/watch")
public class CardWatchController {
	
	private static final Logger logger = LoggerFactory.getLogger(CardWatchController.class);
	
	public static String EVENT_CATEGORY = "watch";
		
	@Autowired
	private CardWatchRepository cardWatchRepository;
	
	@Autowired 
	private CardEventRepository eventRepository;
	
	@ApiOperation(value="Set Watch on Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/board/{boardId}/card/{cardId}/{name}", method=RequestMethod.POST)
	public @ResponseBody CardWatch create(@PathVariable String boardId,
										  	 @PathVariable String cardId,
										  	 @PathVariable String name) throws Exception {
		if (boardId == null || cardId == null)
			throw new ValidationException("Invalid / null params");
		
		CardWatch watch = new CardWatch();
		watch.setBoard(boardId);
		watch.setCard(cardId);
		watch.setWatchDate(new Date());
		watch.setActive(true);
		watch.setUser(SecurityTool.getCurrentUser());
		watch.setId(IdentifierTools.getIdFromName(name));
		cardWatchRepository.save(watch);
		return watch;
	}
	
	
	@ApiOperation(value="Get Board Watches")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/board/{boardId}", method=RequestMethod.GET)
	public @ResponseBody List<CardWatch> getBoardWatches(@PathVariable String boardId) throws Exception {
		return cardWatchRepository.findByBoard(boardId);
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Get All Watches")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody List<CardWatch> getAllWatches() throws Exception {
		return cardWatchRepository.findAll();
	}
	
	@ApiOperation(value="Get Watch list by Board and Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/board/{boardId}/card/{cardId}", method=RequestMethod.GET)
	public @ResponseBody List<CardWatch> getCardWatches(@PathVariable String boardId, 
										  			  @PathVariable String cardId
										  			  ) throws Exception {
		return cardWatchRepository.findByBoardAndCard(boardId, cardId);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Get User Watch list")
	@RequestMapping(value = "/user/{userId}", method=RequestMethod.GET)
	public @ResponseBody List<CardWatch> getUserWatches(@PathVariable String userId
										  			  ) throws Exception {
		return cardWatchRepository.findByUser(userId);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Get Watch By Id")
	@RequestMapping(value = "/{watchId}", method=RequestMethod.GET)
	public @ResponseBody CardWatch getWatchById(@PathVariable String watchId) 
			throws Exception {
		return cardWatchRepository.findById(watchId);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Delete watch")
	@RequestMapping(value="/{watchId}",method=RequestMethod.DELETE)
	public @ResponseBody void deleteWatch(@PathVariable String watchId) throws Exception {
		CardWatch watch = getWatchById(watchId);
		if (watch==null)
			throw new ResourceNotFoundException("Watch not found, watchId:" + Encode.forHtmlContent(watchId));
		cardWatchRepository.delete(watch);
		logger.info("Watch: " + watchId + " deleted successfully");
	}
	
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@ApiOperation(value="Dismiss notification")
	@RequestMapping(value = "/notification/dismiss/board/{boardId}/card/{cardId}/user/{userId}", method=RequestMethod.GET)
	public @ResponseBody CardEvent dismissNotification(@PathVariable String boardId,
														@PathVariable String cardId,
														@PathVariable String userId) throws Exception {
		List<CardEvent> list = eventRepository.findByBoardAndCardAndCategoryAndLevel(boardId, cardId, EVENT_CATEGORY, "New");
		CardEvent found = null;
		for (CardEvent event: list) {
			if (userId.equals(event.getUser())) {
				found = event;
				break;
			}
		}
		if (found==null)
			throw new ResourceNotFoundException(Encode.forHtmlContent("Notification not found, boardId:" + boardId + " cardId:"+cardId+" userId:"+userId));
		found.setLevel("Dismiss");
		eventRepository.save(found);
		return found;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Get Active Notifications by user")
	@RequestMapping(value = "/notification/user/{userId}", method=RequestMethod.GET)
	public @ResponseBody List<CardEvent> getUserNotifications(@PathVariable String userId) throws Exception {
		List<CardEvent> list = eventRepository.findByCategoryAndLevelAndUser(EVENT_CATEGORY, "New", userId);
		List<CardEvent> result = new ArrayList<CardEvent>();
		for (CardEvent event: list) {
			if (userId.equals(event.getUser())) {
				result.add(event);
			}
		}
		return result;
	}

}
