/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco 
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.transfer.TaskActionBody;
import nz.devcentre.gravity.services.TaskService;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}")
public class CardTaskController {
	
	@Autowired 
	TaskService taskService;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	/**
	 * This method gets the tasks for a Card, and validates again the current Rules.
	 * Tasks cannot be actioned without a valid associated Rule.
	 * 
	 * @param boardId - ID of the Board
	 * @param cardId  - ID of the Card
	 * @return        - List of Valid Tasks
	 * @throws Exception
	 */
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasks", method=RequestMethod.GET)
	public @ResponseBody Collection<CardTask> getTasks(@PathVariable String boardId, 
										  			  @PathVariable String cardId) throws Exception {
		
		return this.taskService.getTasks(boardId, cardId);
	}
	
	/**
	 * Return the total number of pending tasks in the system. Also, provide a break up
	 * of open tasks per user. 
	 * @return Map containing the task statistics
	 */
	public Map<String,Integer> getTaskStatistics() {
		List<DBObject> pipeline = new ArrayList<DBObject>();
		StringBuffer sb = new StringBuffer();
		sb.append("{$match:{'complete':false}}");
		BasicDBObject match = BasicDBObject.parse(sb.toString());
		pipeline.add(match);
		
		sb = new StringBuffer();
		sb.append("{$group:{_id:{user:'$user'},count:{$sum:1}}}");
		BasicDBObject group = BasicDBObject.parse(sb.toString());
		pipeline.add(group);

		DBCollection collection = mongoTemplate.getDb().getCollection("gravity_tasks");
		AggregationOptions options = AggregationOptions.builder().build();
		Cursor aggregate = collection.aggregate(pipeline,options);
		
		int totalTasks = 0;
		Map<String,Integer> result = new HashMap<String,Integer>();
		while(aggregate.hasNext()) {
			DBObject row = aggregate.next();
			Integer cnt = (Integer)row.get("count");
			DBObject o  = (DBObject) row.get("_id");
			String user = (String) o.get("user");
			if (user == null)
				result.put("Unassigned", cnt);
			else
				result.put(user, cnt);
			totalTasks += cnt;
		}
		aggregate.close();
		result.put("total_tasks", totalTasks);
		return result;
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'ADMIN')")
	@RequestMapping(value = "/tasks/revert", method=RequestMethod.POST)
	public @ResponseBody void revertTasks(@PathVariable String boardId, 
								@RequestBody String taskId) throws Exception {

		this.taskService.revertTasks(boardId, taskId);
	}
		
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasks/summary", method=RequestMethod.GET)
    public @ResponseBody Map<String,Integer> getTaskSummary(@PathVariable String boardId,
                                                        @PathVariable String cardId) throws Exception {

		Map<String,Integer> result = null;
		Collection<CardTask> cardTasks = this.taskService.getTasks(boardId, cardId);
		
		if(cardTasks != null && cardTasks.size() > 0) {
			int complete = 0;
	        int incomplete = 0;
	        int all = cardTasks.size();
	        
			for(CardTask cardTask : cardTasks) {
				if(cardTask.getComplete()) {
					complete++;
                } else {
                    incomplete++;
				}
			}
			
			result = new HashMap<String,Integer>();
	        result.put("complete", complete);
	        result.put("incomplete", incomplete);
	        result.put("all", all);
		}
       
        return result;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasks/{taskId}", method=RequestMethod.GET)
	public @ResponseBody CardTask getTask(@PathVariable String boardId, 
										  			  @PathVariable String cardId,
										  			  @PathVariable String taskId) throws Exception {
		return this.taskService.getTask(boardId, cardId, taskId);
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasks/{taskId}", method=RequestMethod.POST)
	public @ResponseBody boolean taskAction(@PathVariable String boardId, 
										  			  @PathVariable String cardId,
										  			  @PathVariable String taskId,
										  			  @RequestBody TaskActionBody taskActionBody) throws Exception {
		
		CardTask task = this.taskService.getTask(boardId, cardId, taskId);
		if( task!=null) {
			return this.taskService.taskAction(task, taskActionBody.getTaskAction(), taskActionBody.getUser());
		} else {
			return false;
		}
	}
	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasks/{taskId}/state", method=RequestMethod.GET)
	public @ResponseBody boolean isTaskComplete(@PathVariable String boardId, 
										  			  @PathVariable String cardId,
										  			  @PathVariable String taskId) throws Exception {
		
		CardTask cardTask;
		cardTask = taskService.getTask(boardId, cardId, taskId);			
		if( cardTask == null) return false;
		return cardTask.getComplete();
	}
		
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/cards/{cardId}/tasksearch", method=RequestMethod.GET)
	public @ResponseBody Collection<CardTask> getTasksByStatusForCardId(@PathVariable String boardId, 
			   @PathVariable String cardId,@RequestParam Boolean isComplete) throws Exception{
				
		return this.taskService.getTasksByStatus(boardId, cardId, false);
	}
	
}
