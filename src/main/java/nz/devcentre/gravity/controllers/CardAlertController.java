/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.model.CardAlert;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.services.AlertService;


/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/alerts")
public class CardAlertController {
	
	@Autowired
	private AlertService alertService;
	
	@ApiOperation(value="Save Alert on Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody CardEvent saveAlert(@RequestBody CardAlert cardAlert) throws Exception {				
		CardAlert newAlert = this.alertService.storeAlert( cardAlert);
		return newAlert;
	}
		
	@ApiOperation(value="Get Alerts for Board")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Collection<CardAlert> getAlertsOnBoard(@PathVariable String boardId) throws Exception {
		return this.alertService.getBoardAlerts(boardId);		
	}
	
	@ApiOperation(value="Get Alerts for Card")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/card/{cardId}", method=RequestMethod.GET)
	public @ResponseBody Collection<CardAlert> getAlerts(@PathVariable String boardId, 
										  			  @PathVariable String cardId) throws Exception {
		return this.alertService.getCardAlerts(boardId,cardId);		
	}
	
	@ApiOperation(value="Get Specific Alert")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{alertId}", method=RequestMethod.GET)
	public @ResponseBody CardEvent getAlert(@PathVariable String alertId) throws Exception {
		return this.alertService.getCardAlert(alertId);
	}

	@ApiOperation(value="Dismiss an Alert")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{alertId}/dismiss", method=RequestMethod.GET)
	public @ResponseBody void dismissAlert(@PathVariable String boardId,@PathVariable String alertId) throws Exception {
		this.alertService.dismissAlert(alertId);
	}
	
	@ApiOperation(value="Dismiss all Alerts on Board")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "/dismiss", method=RequestMethod.GET)
	public @ResponseBody void dismissAlerts(@PathVariable String boardId) throws Exception {
		this.alertService.dismissBoardAlerts(boardId);
	}
}
