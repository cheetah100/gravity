/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.JdbcTemplateManager;
import nz.devcentre.gravity.managers.MongoTemplateManager;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.DatabaseDriver;
import nz.devcentre.gravity.repository.CredentialRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.HttpCallService;
import nz.devcentre.gravity.services.HttpCallService.AccessToken;
import nz.devcentre.gravity.tools.IdentifierTools;

/**
 * 
 */
@RestController
@RequestMapping("/cred")
@ApiModel(value="Credential")
public class CredentialController {
	
	private static final Logger logger = LoggerFactory.getLogger(CredentialController.class);
	
	@Autowired 
	private CredentialCache credentialCache;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	private CredentialRepository credentialRepository;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Autowired
	private MongoTemplateManager mongoTemplateManager;
	
	@Autowired
	private HttpCallService httpCallService;

	@RequestMapping(value = "/{credId}", method=RequestMethod.GET)
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	public @ResponseBody Credential getCredential(@PathVariable String credId) throws Exception {
		Credential origin = credentialCache.getItem(credId);
		Credential clone = (Credential) origin.clone();
		clone.setSecret("<removed for security purposes>");
		clone.setIdentifier("<removed for security purposes>");		
		return clone;
	}

	@RequestMapping(value = "/{credId}/verifyjdbc", method=RequestMethod.GET)
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	public @ResponseBody List<Object> verifyJdbcCredential(String credId) throws Exception {
		
		List<Object> results = new ArrayList<>();				
		JdbcTemplate template = this.jdbcTemplateManager.getTemplate(credId);
		Credential credential = securityTool.getSecureCredential(credId); 
		DatabaseDriver driver = DatabaseDriver.fromUrl(credential.getResource());	
		String query = driver.getSchemaQuery();
		
		template.query(query, new RowCallbackHandler() {

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				
				Map<String,Object> row = new HashMap<>();
				int fieldCount = rs.getMetaData().getColumnCount();				
				if( fieldCount>0) {
					for(int n=1; n<fieldCount+1; n++) {
						String fieldName = rs.getMetaData().getColumnName(n);
						Object fieldValue = rs.getObject(n);
						if( fieldValue!=null) {
							row.put(fieldName, fieldValue);
						}
					}
				} else {
					logger.info("No Fields in ResultSet");
				}
				results.add(row);
			}
		});
		return results;
	}

	@RequestMapping(value = "/{credId}/verifymongo", method=RequestMethod.GET)
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	public @ResponseBody Set<String> verifyMongoCredential(String credId) throws Exception {
		MongoTemplate template = this.mongoTemplateManager.getTemplate(credId);
		Set<String> collectionNames = template.getCollectionNames();
		return collectionNames;
	}

	@RequestMapping(value = "/{credId}/verifyrest", method=RequestMethod.GET)
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	public @ResponseBody String verifyRestCredential(String credId) throws Exception {
		Credential credential = this.securityTool.getSecureCredential(credId);
		String ssoToken = this.httpCallService.getSsoToken(credential);
		return ssoToken;
	}
	
	@RequestMapping(value = "/{credId}/release", method=RequestMethod.POST)
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	public @ResponseBody void releaseCredential(String credId) throws Exception {
		this.httpCallService.releaseToken(credId);
		this.jdbcTemplateManager.releaseTemplate(credId);
		this.mongoTemplateManager.releaseTemplate(credId);
	}
	
	@RequestMapping(value = "", method=RequestMethod.POST)
	@ApiOperation(value="Create Credential")
	public @ResponseBody Credential createCredential(@RequestBody Credential cred) throws Exception {
		
		try{
			if (cred.getId() == null) {
				if( cred.getTenantId()==null) {
					cred.setId(IdentifierTools.getIdFromNamedModelClass(cred));
				} else {
					cred.setId(cred.getTenantId() + "_" + IdentifierTools.getIdFromNamedModelClass(cred));
				}
			}
				
			try {
				Credential existing = getCredential(cred.getId());
				if (existing != null)
					throw new ValidationException("Credential already exists! " + cred.getId());
			} catch (ResourceNotFoundException ignore) {
				
			}
			
			// Ensure that the current user is assigned as the owner of the new Credentials.
			// By default also add the administrators group as a owner.
			cred.setPermissions(this.securityTool.initPermissions(cred.getPermissions()));
			cred.addOrUpdate();
			
			cred.setIdentifier(securityTool.encrypt(cred.getIdentifier()));
			cred.setSecret(securityTool.encrypt(cred.getSecret()));
			
			credentialRepository.save(cred);
			cred.setSecret("<removed for security purposes>");
			cred.setIdentifier("<removed for security purposes>");
			
			
			this.cacheInvalidationManager.invalidate(CredentialCache.TYPE, cred.getId());
		} finally {
		}
		return cred;
	}
	

	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/{credId}", method=RequestMethod.DELETE)
	@ApiOperation(value="Delete Credential")
	public @ResponseBody void deleteCredential(@PathVariable String credId) throws Exception {
		
		if( StringUtils.isEmpty(credId)){
			return;
		}
		
		Credential cred= getCredential(credId);
		credentialRepository.delete(cred);
	
		this.cacheInvalidationManager.invalidate(CredentialCache.TYPE, credId);
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/{credId}", method=RequestMethod.PUT)
	@ApiOperation(value="Update Credential")
	public @ResponseBody Credential  updateCredential(@PathVariable String credId,
			@RequestBody Credential cred) throws Exception {
		// Get the current credential with the encrypted identifier and secret
		Credential existingCred = credentialCache.getItem(credId);
		
		if (existingCred == null) {
			throw new ResourceNotFoundException("Credential " + credId + " not found");
		}
		
		if (!credId.equals(cred.getId())) {
			throw new ValidationException("Credential IDs don't match!");
		}
		
		// Ensure that the current user is assigned as the owner of the new Credentials.
		// By default also add the administrators group as a owner.
		cred.setPermissions(this.securityTool.initPermissions(cred.getPermissions()));
		
		cred.addOrUpdate();
		
		if (cred.getIdentifier() == null) {
			cred.setIdentifier(existingCred.getIdentifier());
		}
		else {
			cred.setIdentifier(securityTool.encrypt(cred.getIdentifier()));
		}
		
		if (cred.getSecret() == null) {
			cred.setSecret(existingCred.getSecret());
		}
		else {
			cred.setSecret(securityTool.encrypt(cred.getSecret()));
		}

		credentialRepository.save(cred);
		
		// Don't show the identifier and secret in the response
		cred.setSecret("<removed for security purposes>");
		cred.setIdentifier("<removed for security purposes>");
		
		
		this.cacheInvalidationManager.invalidate(CredentialCache.TYPE, cred.getId());

		return cred;
	}
	
	public void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String credId = it.next();
			Map<String, String> permissions = this.credentialCache.getItem(credId).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/{credId}/permissions", method=RequestMethod.POST)
	public @ResponseBody void addPermissions(@PathVariable String credId, 
			@RequestParam(required=false) boolean replace,
			@RequestBody Map<String,String> permissions) throws Exception {

			Credential cred = getCredential(credId);
		
			Map<String, String> curPermissions = cred.getPermissions();
			if (curPermissions==null) {
				curPermissions = new HashMap<String,String>();
				cred.setPermissions(curPermissions);
			}
			if (replace) {
				Iterator<String> properties = curPermissions.keySet().iterator();
				while (properties.hasNext()) {
					String nextProperty = properties.next();
			
					if(!permissions.containsKey(nextProperty) 
						&& !nextProperty.equals("administrators")
						&& !nextProperty.equals("system")){
						properties.remove();
					}
				}
			}
			for (Entry<String, String> entry : permissions.entrySet()) {
				curPermissions.put(entry.getKey(), entry.getValue());
			}			
			credentialRepository.save(cred);
			this.cacheInvalidationManager.invalidate(CredentialCache.TYPE, credId);
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/{credId}/permissions/{member}", method=RequestMethod.DELETE)
	public @ResponseBody void deletePermission(@PathVariable String credId, @PathVariable String member) throws Exception {

		Credential cred = getCredential(credId);
		Map<String, String> curPermissions = cred.getPermissions();
		curPermissions.remove(member);
		credentialRepository.save(cred);
		this.cacheInvalidationManager.invalidate(CredentialCache.TYPE, credId);
	}
	
	@RequestMapping(value = "", method=RequestMethod.GET)
	@ApiOperation(value="List Credentials")
	public @ResponseBody Map<String,String> listCredentials() throws Exception {
		Map<String, String> map = this.credentialCache.list();
		filterMap(map,"READ,WRITE,ADMIN");
		return map;
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/{credId}/permissions", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> getPermissions(@PathVariable String credId) throws Exception {
		Credential cred = credentialCache.getItem(credId);
		return cred.getPermissions();
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/cached", method=RequestMethod.GET)
	public @ResponseBody Map<String,Double> getCached() throws Exception {
		Map<String,Double> cachedItems = new HashMap<String,Double>();
		
		for( Entry<String,AccessToken> entry : this.httpCallService.getTokens().entrySet()) {
			Double expiration = entry.getValue().getExpiration();
			Double timeToExpiry = expiration - System.currentTimeMillis();
			if( timeToExpiry < 0 ) timeToExpiry = 0d;
			cachedItems.put(entry.getKey(), timeToExpiry);
		}
		
		for( Entry<String,JdbcTemplate> entry : this.jdbcTemplateManager.getCachedTemplates().entrySet()) {
			cachedItems.put(entry.getKey(), 1000000d);
		}
		
		for( Entry<String,MongoTemplate> entry : this.mongoTemplateManager.getCachedTemplates().entrySet()) {
			cachedItems.put(entry.getKey(), 1000000d);
		}

		return cachedItems;
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{credId}/authorize", method=RequestMethod.GET)
	public RedirectView authorize(HttpServletRequest request, 
			@PathVariable String credId
			) throws Exception {
		
		Credential cred = this.securityTool.getSecureCredential(credId);
		
		if(cred==null) {
			throw new ResourceNotFoundException("Credential Not Found");
		}
		
		if(!cred.getMethod().equals("authorize")){
			throw new Exception("Wrong Credential Type");
		}
		
		// construct URL
		String referer = request.getHeader("referer");
		int p1 = referer.indexOf("//");
		String snd = referer.substring(p1+2);
		int p2 = snd.indexOf("/");
		String server = referer.substring(0, p1+p2+2);
		
		String redirect = server + "/api/cred/redirect";
		String scope = cred.getResponseFields().get("scope");
		String tokenUrl = cred.getResponseFields().get("tokenUrl");
		String returnUrl = cred.getResponseFields().get("returnUrl");
		String state = cred.getId() + "_" + SecurityTool.getCurrentUser();
		
		String url = cred.getResourceWithCreds()
				.replace("{redirect}", redirect)
				.replace("{scope}", scope)
				.replace( "{state}", state);
		
		Credential stateCred = null;
		try {
			stateCred = this.credentialCache.getItem(state);
		} catch( Exception e) {		
			stateCred = new Credential();
			stateCred.setId(state);
			stateCred.setName(cred.getName() + " obtained for " + SecurityTool.getCurrentUser());
			stateCred.setMethod("oauth_code");
			stateCred.setRequestMethod("POST");
			stateCred.setResource(tokenUrl);
			stateCred.setIdentifier(cred.getIdentifier());
			stateCred.setSecret(cred.getSecret());
			
			Map<String,String> fields = new HashMap<>();
			fields.put("scope", scope);
			fields.put("returnUrl", returnUrl);
			fields.put("redirectUrl", redirect);
			
			stateCred.setResponseFields(fields);
			this.createCredential(stateCred);
		}
						
		return new RedirectView(url);
	}

	@RequestMapping(value = "/redirect", method=RequestMethod.GET)
	public RedirectView redirect(
			@RequestParam String code, 
			@RequestParam String scope, 
			@RequestParam String state) throws Exception {
		
		Credential cred = this.securityTool.getSecureCredential(state);
		
		if (cred==null) {
			throw new ResourceNotFoundException("Credential Not Found");
		}
		
		if (!cred.getMethod().equals("oauth_code")) {
			throw new Exception("Credential not OAuth Code");
		}
		
		cred.getResponseFields().put("code", code);
		httpCallService.ssoLogon(cred);
				
		return new RedirectView(cred.getResponseFields().get("returnUrl"));
	}
}

