/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardLock;
import nz.devcentre.gravity.model.Lock;
import nz.devcentre.gravity.security.SecurityTool;

@Component
public class CardsLockCache implements MessageListener, CardLockInterface {
	
	private Map<String,Lock> cardsCache = new ConcurrentHashMap<String,Lock>();
		
	private static final Logger logger = LoggerFactory.getLogger(CardsLockCache.class);
	
	private final Object lock = new Object();
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@Autowired
	FanoutExchange gravityCardLockingExchange;
	
	@Override
	public boolean lock(String boardId, String cardId){
		String boardCardId = getBoardCardId(boardId, cardId);
		String username = SecurityTool.getCurrentUser();
		
		synchronized (lock) {
			if(cardsCache.containsKey(boardCardId)){
				if(username.equals(((Lock)cardsCache.get(boardCardId)).getUserName())){
					if( logger.isDebugEnabled()){
						logger.debug("Card already locked : " + boardCardId + " by " + username);
					}
				} else if (username.equals("system")){
					if( logger.isDebugEnabled()){
						logger.debug("Card lock bypass : " + boardCardId + " by " + username);
					}
					return true;					
				} else {
					logger.warn("Card lock FAILED : " + boardCardId + " by " + username);
					return false;
				}
			}
		}
		
		CardLock cardLock = new CardLock( boardId, cardId, username, true);
		rabbitTemplate.convertAndSend(gravityCardLockingExchange.getName(),"",cardLock);
		return true;
	}
	
	@Override
	public boolean unlock(String boardId, String cardId){
		String boardCardId = getBoardCardId(boardId, cardId);
		String username = SecurityTool.getCurrentUser();
		
		synchronized (lock) {
			if(cardsCache.containsKey(boardCardId)){
				if(username.equals(((Lock)cardsCache.get(boardCardId)).getUserName())){
					if( logger.isDebugEnabled()){
						logger.debug("Card unlocked : " + boardCardId + " by " + username);
					}
					CardLock cardLock = new CardLock( boardId, cardId, username, false);
					rabbitTemplate.convertAndSend(gravityCardLockingExchange.getName(),"",cardLock);
					return true;
				} else {
					logger.warn("Card unlock FAILED : " + boardCardId + " by " + username);
					return false;
				}
			}
		}		
		return true;
	}
	
	@Override
	public boolean isLocked(String boardId, String cardId){
		String boardCardId = getBoardCardId(boardId, cardId);
		synchronized (lock) {
			if(cardsCache.containsKey(boardCardId)){
				String username = SecurityTool.getCurrentUser();
				if(username.equals(((Lock)cardsCache.get(boardCardId)).getUserName())){
					return false;
				}else{
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void applyLockState(Card card){
		card.setLock(isLocked(card.getBoard(),card.getId()));
	}

	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{gravityCardLockingQueue.name}")
	@Override
	public void onMessage(Message message) {

		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
			ObjectInputStream ois = new ObjectInputStream(bis);
			CardLock cardLock = (CardLock) ois.readObject();
			actualLock( cardLock);
		} 
		catch (ResourceNotFoundException ignore) {
			// This is required since, during Test executions at the time of builds some stale messages are received from the queues 
			logger.warn("ResourceNotFoundException in CardLockCache onMessage ignored");
		}
		catch (Exception e) {
			logger.error("JMS Exception on Card Locking Reception ",e);
		}
	}
	
	public void actualLock(CardLock cardLock){
		String boardCardId = getBoardCardId(cardLock.getBoardId(), cardLock.getCardId());
		synchronized (lock) {
			if( cardLock.isLock()){
				cardsCache.put(boardCardId,new Lock(cardLock.getUser(), new Date()));
				if( logger.isDebugEnabled()){
					logger.debug("Card Locked : " + boardCardId + " by " + cardLock.getUser());
				}
			} else {
				cardsCache.remove(boardCardId);
				if( logger.isDebugEnabled()){
					logger.debug("Card UnLocked : " + boardCardId + " by " + cardLock.getUser());
				}
			}
		}
	}
	
	private String getBoardCardId(String boardId, String cardId){
		StringBuilder boardCard = new StringBuilder(boardId);
		boardCard.append("/");
		boardCard.append(cardId);
		return boardCard.toString();
	}
	
	@Scheduled(fixedDelay=60000)
	public void unlock(){
		synchronized (lock) {
			Set<String> keySet = cardsCache.keySet();
			for (String boardCardId : keySet) {
				Lock lock = cardsCache.get(boardCardId);
				// 60 mins has passed since acquiring lock so remove it 
				if(new Date().getTime() - lock.getTimeStamp().getTime() > 3600000){
					if( logger.isDebugEnabled()){
						logger.info("Unlocking card : " + boardCardId);
					}
					cardsCache.remove(boardCardId);
				}
			}
		}
	}

	@Override
	public Lock getLock(String boardId, String cardId) {
		String boardCardId = getBoardCardId(boardId, cardId);
		synchronized (lock) {
			if(cardsCache.containsKey(boardCardId))
				return (Lock)cardsCache.get(boardCardId);

		}
		return null;
	}
	
	@Override
	public Map<String,Lock> getLocks(String boardId) {
		synchronized (lock) {
			Map<String,Lock> locks = new HashMap<String,Lock>();
			String boardCardId = getBoardCardId(boardId, "");
			for( Entry<String,Lock> entry : cardsCache.entrySet()){
				if( entry.getKey().startsWith(boardCardId)){
					String cardId = entry.getKey().substring(entry.getKey().indexOf("/")+1);
					locks.put(cardId, entry.getValue());
				}
			}			
			return locks;
		}
	}
}
