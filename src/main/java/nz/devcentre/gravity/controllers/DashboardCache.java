/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2016 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Dashboard;
import nz.devcentre.gravity.repository.DashboardRepository;

@Service
public class DashboardCache extends CacheImpl<Dashboard>{

	private static final Logger logger = LoggerFactory.getLogger(DashboardCache.class);
	
	public static final String TYPE = "DASHBOARD";
	
	@Autowired
	DashboardRepository dashboardRepository;
			
	@Override
	protected Dashboard getFromStore(String... itemIds) throws Exception {
		//Dashboard board = dashboardRepository.findById(itemIds[0]);
		Dashboard board = dashboardRepository.findByIdAndActiveTrue(itemIds[0]);//Fetch Active version Only
		return board;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {
		
		logger.info("Getting Dashboard List");
		
		//List<Dashboard> list = dashboardRepository.findAll();
		List<Dashboard> list = dashboardRepository.findByActiveTrue();//Fetch the Active version Only
		Map<String,String> result = new TreeMap<String,String>();
		for (Dashboard dashboard: list) {
			result.put(dashboard.getId(), dashboard.getName());
		}
		return result;
	}
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
	
}
