/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.TransformChain;
import nz.devcentre.gravity.repository.TransformRepository;

/**
 * Transform Cache
 * 
 * Purpose of this class is to cache Transforms
 * 
 * @author peter
 */
@Service
public class TransformCache extends CacheImpl<TransformChain>{
	
	public static final String TYPE = "TRANSFORM";

	@Autowired
	TransformRepository transformRepository; 
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}

	@Override
	protected TransformChain getFromStore(String... itemIds) throws Exception {
		String transformId = itemIds[0];
		TransformChain transform = null;
		transform = transformRepository.findByIdAndActiveTrue(transformId);
		return transform;		
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		Map<String,String> result = new TreeMap<String,String>();
		List<TransformChain> list = transformRepository.findByActiveTrue();
		for (TransformChain transform: list)  {
				result.put(transform.getId(), transform.getName());
		}
		return result;
	}
	
}
