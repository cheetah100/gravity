/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.BoardListener;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.services.CardService;

@Service
public class CardCache extends CacheImpl<Card> implements BoardListener{
	
	private static final Logger logger = LoggerFactory.getLogger(CardCache.class);
	
	public static final String TYPE = "CARD";

	@Autowired 
	private CardService cardService;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	CacheInvalidationInterface cacheInvalidationManager;
	
	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}

	@Override
	protected Card getFromStore(String... itemIds) throws Exception {
		Card card;

		String boardId = itemIds[0];
		String cardId = itemIds[1];
		
		card = cardService.getCard(boardId, cardId);
		
		if(card==null){
			throw new ResourceNotFoundException();
		}
			
		return card;
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixs) throws Exception {
		logger.info("Getting Board List");
		
		return boardsCache.list(prefixs);
		
	}

	@Override
	public void onCardHolderEvent(CardHolder cardHolder) throws Exception {
		this.cacheInvalidationManager.invalidate(TYPE, cardHolder.getBoardId(),cardHolder.getCardId());
		logger.info("Sending CARD Clear Cache " + cardHolder.getBoardId() + "." + cardHolder.getCardId());
	}
	
}
