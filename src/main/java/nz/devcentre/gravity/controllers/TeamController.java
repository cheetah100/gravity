/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.repository.TeamRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/team")
public class TeamController {
	
	private static final Logger logger = LoggerFactory.getLogger(TeamController.class);
		
	@Autowired
	private TeamCache teamCache;
		
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody Team createTeam(@RequestBody Team team) throws Exception {
		String newId = IdentifierTools.getIdFromNamedModelClass(team);
		if (teamRepository.exists(newId)) 
			throw new ValidationException("Team already exists, id: " + Encode.forHtmlContent(newId));
		team.setId(newId);
		team.addOrUpdate();
		
		// Ensure that the current user is assigned as the owner of the new board.
		// By default also add the administrators group as a owner.
		team.setOwners(this.securityTool.initPermissions(team.getOwners()));
		
		teamRepository.save(team);
		
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, newId);
		return team;
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{teamId}", method=RequestMethod.GET)
	public @ResponseBody Team getTeam(@PathVariable String teamId) throws Exception {
		return teamCache.getItem(teamId);
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{teamId}", method=RequestMethod.PUT)
	public @ResponseBody Team updateTeam(@RequestBody Team team,@PathVariable String teamId) throws Exception {
		
		
		Team oldTeam = teamCache.getItem(teamId);
		try{
			
			if( team.getDescription()!=null){
				oldTeam.setDescription(team.getDescription());
			}
			
			teamRepository.save(oldTeam);
			
			this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
		} finally {
			
		}
		return oldTeam;
	}


	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteTeam(@PathVariable String teamId) throws Exception {
		if(StringUtils.isEmpty(teamId)){
			throw new FileNotFoundException();
		}
		//Team team = getTeam(teamId);
		//team.setDeleted(true);
		//teamRepository.save(team);
		teamRepository.delete(teamId);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}
	
	// TODO: Add permission enforcement within method
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listTeams() throws Exception {
		return this.teamCache.list();
	}
	
	// TODO: Add permission enforcement within method
	@RequestMapping(value = "/teamdetails", method=RequestMethod.GET)
	public @ResponseBody Map<String,Team> getTeamDetails() throws Exception {
		Map<String,Team> result = new HashMap<String, Team>();
		Map<String,String> teams;
		
			teams = this.teamCache.list();
			for( Entry<String,String> entry : teams.entrySet()){
				result.put(entry.getKey(), getTeam(entry.getKey()));
			}
		
		return result;
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/owners", method=RequestMethod.POST)
	public @ResponseBody void addOwners(@PathVariable String teamId, @RequestBody Map<String,String> roles) throws Exception {

		Team team = getTeam(teamId);
		Map<String,String> owners = team.getOwners();
		if( owners==null){
			owners = new HashMap<String,String>();
			team.setOwners(owners);
		}
		
		for( Entry<String,String> entry : roles.entrySet()){
			owners.put(entry.getKey(), entry.getValue());
		}
		
		teamRepository.save(team);
		
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}

	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/members", method=RequestMethod.POST)
	public @ResponseBody void addMembers(@PathVariable String teamId, @RequestBody Map<String,String> members) throws Exception {
		this.securityTool.addMembers(teamId, members);		
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/roles", method=RequestMethod.POST)
	public @ResponseBody void addRoles(@PathVariable String teamId, @RequestBody Map<String,String> roles) throws Exception {

		Team team = getTeam(teamId);
		Map<String,String> existing = team.getRoles();
			
		if( existing==null){
			existing = new HashMap<String,String>();
			team.setRoles(existing);
		}
		
		for( Entry<String,String> entry : roles.entrySet()){
			existing.put(entry.getKey(), entry.getValue());
		}
		
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}

	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/members/{member}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteMember(@PathVariable String teamId, @PathVariable String member) throws Exception {
		this.securityTool.deleteMember(teamId, member);		
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/roles/{role}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteRole(@PathVariable String teamId, @PathVariable String role) throws Exception {

		Team team = getTeam(teamId);
		Map<String,String> existing = team.getRoles();
		if (existing!=null)
			existing.remove(role);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/owners/{owner}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteOwner(@PathVariable String teamId, @PathVariable String owner) throws Exception {

		Team team = getTeam(teamId);
		Map<String,String> existing = team.getOwners();
		if (existing!=null)
			existing.remove(owner);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}
	
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{teamId}/users", method=RequestMethod.GET)
	@ApiOperation(value="Get Team Users")
	public @ResponseBody Map<String,User> getTeamUsers(@PathVariable String teamId) throws Exception {
		logger.info("Getting Users for Team: " + teamId);
		Map<String,User> userMap = new HashMap<String,User>();
		Team team = teamCache.getItem(teamId);
		for( String userid: team.getMembers().keySet() ) {
			User user = securityTool.getUser(userid);
			if(user!=null){
				userMap.put(userid, user);
			}
		}
		return userMap;
	}
	
	@ApiOperation(value="Team Membership Model")
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/model", method=RequestMethod.GET)
	public @ResponseBody Map<String,Object> getModel() throws Exception {
		
		Map<String,Object> returnMap = new HashMap<String,Object>();
		
		List<Object> nodes = new ArrayList<Object>();
		returnMap.put("nodes", nodes);		
		List<Object> links = new ArrayList<Object>();
		returnMap.put("links", links);
		
		Map<String, String> teams = this.teamCache.list();
		
		for(Entry<String,String> entry: teams.entrySet()){
			Map<String,Object> node = new HashMap<String,Object>();
			node.put("id", entry.getKey());
			node.put("title", entry.getValue());
			node.put("group", 1);
			node.put("cards", 100);
			nodes.add(node);
		}
		
		Map<String, String> map = this.boardsCache.list();
		
		for(Entry<String,String> entry: map.entrySet()){
			Board board = boardsCache.getItem(entry.getKey());		
			Map<String,Object> node = new HashMap<String,Object>();
			node.put("id", entry.getKey());
			node.put("title", entry.getValue());
			node.put("group", 2);
			node.put("cards", 50);
			nodes.add(node);
			
			Map<String, String> permissions = board.getPermissions();
			for(Entry<String,String> permission: permissions.entrySet()){
				Map<String,Object> link = new HashMap<String,Object>();
				if( teams.containsKey(permission.getKey())){
					link.put("source", entry.getKey());
					link.put("target", permission.getKey());
					link.put("value", 1);
					links.add(link);
				}
			}
		}
		return returnMap;
	}
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/members/refresh", method=RequestMethod.PUT)
	public @ResponseBody void refreshMembers(@PathVariable String teamId, @RequestBody Map<String,String> members) throws Exception {

		Team team = getTeam(teamId);
		team.setMembers(members);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
	}
	

	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/alias", method=RequestMethod.POST)
	public @ResponseBody Team addTeamAlias(@PathVariable String teamId, @RequestBody List<String> aliasToAdd) throws Exception {
		
		Team team = getTeam(teamId);
		
		if(aliasToAdd==null || aliasToAdd.isEmpty()) {
			logger.warn("Nothing to Add");
			return team;
		}
		List<String> aliases = team.getAliases();
		if(aliases==null) {
			aliases=new LinkedList<>();
		}
		aliases.addAll(aliasToAdd);
		team.setAliases(aliases);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
		return team;
	}
	
	@PreAuthorize("hasPermission(#teamId, 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{teamId}/alias", method=RequestMethod.DELETE)
	public @ResponseBody Team removeTeamAlias(@PathVariable String teamId, @RequestBody List<String> aliasToRemove) throws Exception {
		
		Team team = getTeam(teamId);
		
		if(aliasToRemove==null || aliasToRemove.isEmpty()) {
			logger.warn("Nothing to Remove");
			return team;
		}
		List<String> aliases = team.getAliases();
		if(aliases==null) {
			logger.warn("Nothing to Remove");
			return team;
		}
		aliases.removeAll(aliasToRemove);
		team.setAliases(aliases);
		teamRepository.save(team);
		this.cacheInvalidationManager.invalidate(TeamCache.TYPE, teamId);
		return team;
	}
}
