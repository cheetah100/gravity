/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.model.CardComment;
import nz.devcentre.gravity.repository.CardCommentRepository;
import nz.devcentre.gravity.security.SecurityTool;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/cards/{cardId}/comments")
public class CardCommentController {
	
	public static String COMMENT_CATEGORY = "comment";
	
	@Autowired
	CardCommentRepository cardCommentRepository;
	
	@ApiOperation(value="Get Card Comments")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Collection<CardComment> getComments(@PathVariable String boardId, 
										  			  @PathVariable String cardId) throws Exception {

		return cardCommentRepository.findByBoardAndCard(boardId, cardId);	
	}

	@ApiOperation(value="Create Card Comment")
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody CardComment saveComment(@PathVariable String boardId,
										  			  @PathVariable String cardId,
										  			  @RequestBody String body) throws Exception {
		
		CardComment comment = new CardComment();
		comment.setBoard(boardId);
		comment.setCard(cardId);
		comment.setLevel("info");
		comment.setOccuredTime(new Date());
		comment.setDetail(body);
		comment.setUser(SecurityTool.getCurrentUser());
		CardComment saved = cardCommentRepository.save(comment);
		return saved;	
	}
}
