/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.IntegrationManager;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.repository.IntegrationRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;
import nz.devcentre.gravity.services.IntegrationService;

@RestController
@RequestMapping("/integration")
public class IntegrationController {
	
	private static final Logger logger = LoggerFactory.getLogger(IntegrationController.class);
		
	@Autowired
	private IntegrationRepository integrationRepository;
		
	@Autowired
	private IntegrationManager integrationManager;

	@Autowired
	private IntegrationService integrationService;
	
	@Autowired
	private IntegrationCache integrationCache;
	
	@Autowired 
	private CacheInvalidationInterface cacheInvalidationManager;
	
	@Autowired
	private SecurityTool securityTool;
	
	/**
	 * Create a notification type
	 * @param integration
	 * @throws Exception
	 */
	//@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')") - Anyone can create a Integration.
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody void createIntegration(@RequestBody Integration integration) throws Exception {
		if( logger.isDebugEnabled()) {
			logger.debug("Attempting to create new Notification Type:"+ integration.getName());
		}
		
		if( integration.getId()==null) {
			if( integration.getTenantId()==null) {
				integration.setId(IdentifierTools.getIdFromNamedModelClass(integration));
			} else {
				integration.setId(integration.getTenantId() + "_" + IdentifierTools.getIdFromNamedModelClass(integration));
			}
		}
		
		if(this.integrationRepository.exists(integration.getId())) {
			throw new IllegalArgumentException("Notification with this id already exists");
		}
		
		integration.addOrUpdate();
		integration.setPermissions(this.securityTool.initPermissions(integration.getPermissions()));
		
		integrationRepository.save(integration);
		this.cacheInvalidationManager.invalidate(IntegrationCache.TYPE, integration.getId());
		integrationManager.start(integration);
	}
	
	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN')")
	@RequestMapping(value = "/{integrationId}", method=RequestMethod.PUT)
	public @ResponseBody void updateIntegration(@PathVariable String integrationId,
			@RequestBody Integration integration) throws Exception {
		
		if( logger.isDebugEnabled()) {
			logger.debug("Attempting to update Integration:"+ integration.getName());
		}
		
		if( !integration.getId().equals(integrationId)) {
			throw new IllegalArgumentException("Integration ID Mismatch between body");
		}
		
		if(!this.integrationRepository.exists(integration.getId())) {
			throw new IllegalArgumentException("Integration with this id doesn't exist");
		}
		
		integrationRepository.save(integration);
		this.cacheInvalidationManager.invalidate(IntegrationCache.TYPE, integration.getId());
		integrationManager.start(integration);
	}
	
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Map<String,String> listIntegrations() throws Exception{
		Map<String, String> list = this.integrationCache.list();
		filterMap(list,"READ,WRITE,ADMIN");
		return list;
	}
	
	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN')")
	@RequestMapping(value = "/{integrationId}", method=RequestMethod.GET)
	public @ResponseBody Integration getIntegration(@PathVariable String integrationId) throws Exception{
		return this.integrationCache.getItem(integrationId);
	}
	
	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN')")
	@RequestMapping(value = "/{integrationId}", method=RequestMethod.DELETE)
	public @ResponseBody boolean deleteIntegration(@PathVariable String integrationId) throws Exception{
		try {
			this.integrationManager.stop(integrationId);
			this.integrationRepository.delete(integrationId);
			this.cacheInvalidationManager.invalidate(IntegrationCache.TYPE, integrationId);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN')")
	@RequestMapping(value = "/{integrationId}", method=RequestMethod.POST)
	public @ResponseBody void createNotification(@PathVariable String integrationId,
											  	 @RequestBody  Map<String,Object> context) throws Exception {
		
		Integration integration = this.integrationRepository.findById(integrationId);
		Batch batch = this.integrationService.createBatch(integration, 1, new Date());
		this.integrationService.execute(context, integration, batch);
	}
	
	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN,WRITE')")
	@PostMapping(value = "/execute/{integrationId}")
	public @ResponseBody void executeIntegration(@PathVariable String integrationId, @RequestBody String body) throws Exception {
		Integration integration = this.integrationRepository.findById(integrationId);
		if(integration!= null) {
			integrationManager.execute(integration);
		} else {
			throw new ResourceNotFoundException();
		}
	}

	@PreAuthorize("hasPermission( #integrationId, 'INTEGRATION', 'ADMIN')")
	@PostMapping(value = "/upload/{integrationId}")
	@ApiOperation(value = "Upload Data File",consumes ="file")
	public @ResponseBody void executeByUpload( @PathVariable String integrationId, 
			@RequestParam MultipartFile file) throws Exception {
		
		Integration integration = this.integrationRepository.findById(integrationId);
		Batch batch = this.integrationService.createBatch(integration, 1, new Date());
		Map<String,Object> context = new HashMap<>();
		context.put("file", file.getBytes());
		context.put("filename",file.getOriginalFilename());
		context.put("content_type",file.getContentType());
		this.integrationService.execute(context, integration, batch);
		
	}
	
	@RequestMapping(value = "/plugins", method=RequestMethod.GET)
	public @ResponseBody Map<String,PluginMetadata> integrationPluginMetadata() {
		return integrationManager.getMetadata();
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/tables/{credId}", method=RequestMethod.GET)
	public @ResponseBody List<String> getTables(@PathVariable String credId) throws Exception{
		return this.integrationService.getTables(credId);
	}
	
	@PreAuthorize("hasPermission(#credId, 'CRED', 'ADMIN')")
	@RequestMapping(value = "/table/{credId}/{tableId}", method=RequestMethod.GET)
	public @ResponseBody Board getTable(@PathVariable String credId, String tableId, @RequestParam(required = false) boolean save) throws Exception{
		return this.integrationService.getBoardFromTable(credId, tableId, save);
	}
	
	@PreAuthorize("hasPermission(#integrationId, 'INTEGRATION', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{integrationId}/permissions", method = RequestMethod.GET)
	@ApiOperation(value = "Get Integration Permissions")
	public @ResponseBody Map<String, String> getPermissions(@PathVariable String integrationIdId) throws Exception {
		Integration integration = this.integrationCache.getItem(integrationIdId);
		return integration.getPermissions();
	}
	
	@PreAuthorize("hasPermission(#integrationId, 'INTEGRATION', 'ADMIN')")	
	@RequestMapping(value = "/{integrationId}/permissions", method = RequestMethod.POST)
	@ApiOperation(value = "Add Permissions to Integration")
	public @ResponseBody void addPermissions(@PathVariable String integrationId,
			@RequestParam(required = false) boolean replace, 
			@RequestBody Map<String, String> permissions)
			throws Exception {

		Integration integration = this.integrationCache.getItem(integrationId);
		if (integration == null) {
			throw new ResourceNotFoundException("Integration not found, id:" + integrationId);
		}

		Map<String, String> curPermissions = integration.getPermissions();
		if (curPermissions == null) {
			curPermissions = new HashMap<String, String>();
			integration.setPermissions(curPermissions);
		}
		if (replace) {
			Iterator<String> properties = curPermissions.keySet().iterator();
			while (properties.hasNext()) {
				String nextProperty = properties.next();
				if ( !permissions.containsKey(nextProperty)
						&& !nextProperty.equals("administrators")
						&& !nextProperty.equals("system")) {
					properties.remove();
				}
			}
		}

		for (Entry<String, String> entry : permissions.entrySet()) {
			curPermissions.put(entry.getKey(), entry.getValue());
		}

		integration.setPermissions(curPermissions);
		integration.addOrUpdate();
		
		Integration savedIntegration = integrationRepository.save(integration);
		this.cacheInvalidationManager.invalidate(IntegrationCache.TYPE, savedIntegration.getId());
		integrationManager.start(integration);
	}
	
	public void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String id = it.next();
			Map<String, String> permissions = this.integrationCache.getItem(id).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}

}
