/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.managers.CacheManager;
import nz.devcentre.gravity.model.Application;
import nz.devcentre.gravity.repository.AppRepository;

@Service
public class ApplicationCache extends CacheImpl<Application> {

	private static final Logger logger = LoggerFactory.getLogger(ApplicationCache.class);
	
	public static final String TYPE = "APP";
		
	@Autowired
	private AppRepository appRepository;

	@Override
	protected Application getFromStore(String... itemIds) throws Exception {
		return appRepository.findByIdAndActiveTrue(itemIds[0]);
	}

	@Override
	protected Map<String, String> getListFromStore(String... prefixes) throws Exception {
		
		logger.info("Getting Application List");
		
		List<Application> apps = appRepository.findByActiveTrue();
		
		Map<String,String> result = new TreeMap<String,String>();
		for (Application app: apps)
			result.put(app.getId(), app.getName());
		
		return result;
	}

	@Override
	@Autowired
	public void register(CacheManager resolver) {
		resolver.register(TYPE, this);
	}
	
}
