/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.repository.CardEventRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.AuditService;

/**
 * 
 */
@RestController
@RequestMapping("/board/{boardId}/cards/{cardId}/history")
public class CardHistoryController {
		
	@Autowired
	private AuditService auditService;
	
	@Autowired 
	private CardEventRepository eventRepository;
	
	
	public void setEventRepository(CardEventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.GET)
	public @ResponseBody Collection<CardEvent> getHistoryList(@PathVariable String boardId, 
										  			  @PathVariable String cardId) throws Exception {
		
		return eventRepository.findByBoardAndCard(boardId, cardId);
		
	}

	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{historyId}", method=RequestMethod.GET)
	public @ResponseBody CardEvent getHistory(@PathVariable String boardId, 
										  			  @PathVariable String cardId,
										  			  @PathVariable String historyId) throws Exception {

		return eventRepository.findByUuid(historyId);
		
	}
	
	public @ResponseBody void deleteHistory(String boardId, 
										  			  String cardId,
										  			  String historyId) throws Exception {
		
		CardEvent dbEvent = eventRepository.findByUuid(historyId);
		if (dbEvent == null)
			throw new ResourceNotFoundException("History not found, id : " + Encode.forHtmlContent(historyId));
		
		eventRepository.delete(dbEvent);
		
	}
	
	public @ResponseBody void deleteAllHistoryForBoard(String boardId) throws Exception {
		
		List<CardEvent> dbEventList = eventRepository.findByBoard(boardId);
		if (dbEventList != null)
			eventRepository.delete(dbEventList);
		
	}

	
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'WRITE,ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody CardEvent saveHistory(@PathVariable String boardId, 
										  			  @PathVariable String cardId,
										  			  @RequestBody Map<String,Object> body) throws Exception {

		String detail = getField("detail",body);
		String category = getField("category",body);
		String level = getField("level",body);
		
		if(level==null){
			level="info";
		}
		
		CardEvent event = auditService.storeCardEvent(detail, boardId, 
				cardId, level, category, SecurityTool.getCurrentUser(), new Date(), null);
		
		return event;
	}
	
	private String getField( String name, Map<String,Object> list){
		Object ob = list.get(name);
		if(ob!=null){
			return ob.toString();
		} 
		return null;	
	}
	
}
