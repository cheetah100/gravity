/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco 
 * (C) Copyright 2021 Devcentre limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.ExportConfiguration;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.UploadSheetErrorDetails;
import nz.devcentre.gravity.model.importer.ExportProcess;
import nz.devcentre.gravity.model.importer.SheetUtil;
import nz.devcentre.gravity.model.importer.TransferStatus;
import nz.devcentre.gravity.model.importer.TransferStatusEnum;
import nz.devcentre.gravity.model.importer.UploadService;
import nz.devcentre.gravity.repository.ExportConfigRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/export")
public class ExportController {
	
	private static final Logger logger = LoggerFactory.getLogger(ExportController.class);
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private BoardsCache boardsCache;
		
	@Autowired
	private SheetUtil sheetUtil;
		
	@Autowired
	private QueryService queryService;
	
	@Autowired
	private ExportConfigRepository exportConfigRepository;
	
	private TransferStatus status;
	
	private Workbook workbook;
	
	@Autowired
	private UploadService uploadService;

	@Autowired
	private BoardService boardTools;
	
	public ExportController() {
		setStatus(new TransferStatus());
		getStatus().setStatus(TransferStatusEnum.READY);
		getStatus().setMessages(new ArrayList<String>());
		getStatus().setErrorMessage("");
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method=RequestMethod.POST)
	public @ResponseBody ExportConfiguration createExportConfiguration(@RequestBody ExportConfiguration config) throws Exception {
		try { 
			String newId = IdentifierTools.getIdFromNamedModelClass(config);
			config.setId(newId);
			config.addOrUpdate();
			exportConfigRepository.save(config);
		} finally {
		}
		return config;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}", method=RequestMethod.DELETE)
	public @ResponseBody void deleteExportConfiguration(@PathVariable String configId) throws Exception {
		if(StringUtils.isEmpty(configId)){
			throw new FileNotFoundException();
		}
		
		ExportConfiguration config = getExportConfiguration(configId);
		exportConfigRepository.delete(config);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}", method=RequestMethod.GET)
	public @ResponseBody ExportConfiguration getExportConfiguration(@PathVariable String configId) throws Exception {
		ExportConfiguration config = null;
		config = exportConfigRepository.findById(configId);
		if (config == null)
			throw new ResourceNotFoundException("ExportConfig not found, id: " + Encode.forHtmlContent(configId));
		return config;	
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/xlsx", method=RequestMethod.GET)
	public void export(HttpServletResponse response,
						@PathVariable String configId,
						@RequestParam(required=false) String mode,
		  				@RequestParam(required=false) String filter,
		  				@RequestParam(required=false,defaultValue="all") String view) throws Exception {
		
		//Validate Board and User Access before Creating Export Process
		this.boardTools.validateAccess(configId, mode, filter, view);
		
		Workbook wb = generateWorkbook( configId, mode, filter, view);
		sendWorkbook(configId, wb, response);
	}
	
	public Workbook generateWorkbook(String configId, String mode, String filter, String view) throws Exception {
		
		ExportConfiguration config = null;
		try {
			config = getExportConfiguration(configId);
		}catch(Exception e) {
			//Do Nothing
		}
		
		Workbook wb = new XSSFWorkbook();
			if( config == null){
				Collection<Card> cards = this.queryService.fetchCardsApplyTemplateAndOrder(configId, null, filter, view, null, true, false, false);
				boardToSheet(wb,configId,view,cards);
			} else {
				for( Entry<String,String> entry : config.getBoards().entrySet()){
					Collection<Card> cards = this.queryService.fetchCardsApplyTemplateAndOrder(entry.getKey(), null, null, entry.getValue(), null, true, false, false);
					boardToSheet(wb,entry.getKey(),entry.getValue(),cards);
				}
			}
			return wb;
		
	}	
	
	/*
	 * Used to apply dynamic filter
	 * 
	 */
	public Workbook generateWorkbook(String configId, String mode, String filter, String view,Filter dynamicFilter) throws Exception {

		ExportConfiguration config = null;
		try {
			config = getExportConfiguration(configId);
		} catch (Exception e) {
			// Do Nothing
		}

		Workbook wb = new XSSFWorkbook();
			if (config == null) {
				
				Collection<Card> cards =this.queryService.search(configId, dynamicFilter, view, filter, null, false, false);
				boardToSheet(wb, configId, view, cards);
			} else {
				for (Entry<String, String> entry : config.getBoards().entrySet()) {
					Collection<Card> cards =this.queryService.search(entry.getKey(), dynamicFilter, entry.getValue(), filter, null, false, false);
					boardToSheet(wb, entry.getKey(), entry.getValue(), cards);
				}
			}
			return wb;
		
	}	
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/xlsx/startjob", method=RequestMethod.GET)
	public @ResponseBody String startExportJob(HttpServletResponse response,
						@PathVariable String configId,
						@RequestParam(required=false) String mode,
		  				@RequestParam(required=false) String filter,
		  				@RequestParam(required=false,defaultValue="all") String view) throws Exception {
		
		if( this.getStatus().getStatus().equals(TransferStatusEnum.INPROGRESS)){
			throw new Exception("Download Process In Progress");
		}
		
		this.getStatus().newId();
		this.getStatus().setUser(SecurityTool.getCurrentUser());
		
		//Validate Board and User Access before Creating Export Process
		this.boardTools.validateAccess(configId, mode, filter, view);

		ExportProcess process = new ExportProcess(this, SecurityTool.getCurrentAuthentication(), configId, mode, filter, view);
		Thread processThread = new Thread(process);
		processThread.start();
		return this.getStatus().getId();
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/status", method=RequestMethod.GET)
	public @ResponseBody TransferStatus status() throws Exception {
		return this.status;
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/xlsx/download", method=RequestMethod.GET)
	public void status(HttpServletResponse response, @PathVariable String configId) throws Exception {
		sendWorkbook(configId, this.workbook, response);
	}
	
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{configId}/xlsx", method=RequestMethod.POST)
	public void exportCards(HttpServletResponse response,
						@PathVariable String configId,
						@RequestParam(required=false) String mode,
						@RequestBody List<String> cardList) throws Exception {
		
		Workbook wb = new XSSFWorkbook();
		Collection<Card> cards = new ArrayList<Card>();
		for( String cardId : cardList){
			Card card = cardService.getCard(configId, cardId);
			if(card!=null){
				cards.add(card);
			}
		}
		
		boardToSheet(wb,configId,null,cards);
		sendWorkbook(configId, wb, response);
	}

	/*
	 * Use to execute dynamic filter while export
	 * Creates an Export Process and Starts
	 */
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value = "Submit an Export Request With Dynamic Filter", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.TEXT_PLAIN_VALUE, 
			notes = "Accepts Dynamic Filter as Request Body, applies filter on board and returns the filtered list of cards")
	@ApiImplicitParams({
			@ApiImplicitParam(value = "configId(Board ID Most of the cases)", paramType = "path", name = "configId"),
			@ApiImplicitParam(value = "mode", paramType = "query", name = "mode", required = false),
			@ApiImplicitParam(value = "Valid Gravity view To Apply", paramType = "query", name = "view", required = false),
			@ApiImplicitParam(value = "Valid gravity stored filter(future enhancement)", paramType = "query", name = "filter", required = false),
			@ApiImplicitParam(paramType = "body", name = "dynamicFilter", required = true,dataType ="Filter" ,dataTypeClass =Filter.class, value = "Valid Gravity dynamic Filter that will be applied"), })
	@RequestMapping(value = "/{configId}/xlsx/filter/startjob", method=RequestMethod.POST)
	public @ResponseBody String startExportJobDynamicFilter(HttpServletResponse response,
						@PathVariable String configId,
						@RequestParam(required=false) String mode,
		  				@RequestParam(required=false,defaultValue="all") String view,
		  				@RequestParam(required=false) String filter,
		  				@RequestBody(required = true) Filter dynamicFilter
		  				) throws Exception {
		
		if( this.getStatus().getStatus().equals(TransferStatusEnum.INPROGRESS)){
			throw new Exception("Download Process In Progress");
		}
		
		this.getStatus().newId();
		this.getStatus().setUser(SecurityTool.getCurrentUser());
		/*
		 * Create New Export Process
		 * TODO:- Future Enhancement to Support Stored Gravity Filter
		 */
		ExportProcess process = new ExportProcess(this, SecurityTool.getCurrentAuthentication(), configId, mode, null, view,dynamicFilter);
		Thread processThread = new Thread(process);
		processThread.start();
		return this.getStatus().getId();
	}
	
	private void boardToSheet(Workbook wb, String boardId, String viewId, Collection<Card> cards ) throws Exception{
		Sheet sheet = wb.createSheet(boardId);
		if(cards!=null){
			try {
				Board board =  boardsCache.getItem(boardId);
				
				sheetUtil.writeHeaderRow(sheet, getHeadersFromBoard(board));
				int rowCounter = 1;
				for(Card card: cards){
					try{
					sheetUtil.writeCardRow(sheet, card, board, rowCounter);
					rowCounter++;
					}catch(Exception e){
						logger.info("Unable to download " + card.toString());
					}
				}
			} finally {
			}
		}
	}
	
	private void sendWorkbook(String configId, Workbook wb, HttpServletResponse response) throws IOException{
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition",
                "attachment; filename=" + configId + ".xlsx");
        if(wb!=null) {
        	wb.write(response.getOutputStream());
        	wb.close();
        }else {
        	throw new ResourceNotFoundException();
        }
    	response.flushBuffer();
	}	
	
	@RequestMapping(value = "/{boardId}/template/{templateId}/headerxlsx", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,ADMIN')")
	public void downloadTemplateHeaders(
	        HttpServletResponse response,
	        @PathVariable String boardId,
	        @PathVariable String templateId,
	        @RequestParam(required=false) String mode,
			@RequestParam(required=false) String filter,
			@RequestParam(required=false,defaultValue="all") String view) throws Exception {
				
		try(Workbook wb = new XSSFWorkbook()) {
			Sheet sheet = wb.createSheet(boardId);
			Board board = boardsCache.getItem(boardId, boardId);
			sheetUtil.writeHeaderRow(sheet, getHeadersFromBoard(board));
			sendWorkbook(boardId, wb, response);
		}
	}
	
	
	@RequestMapping(value = "/{boardId}/template/{templateId}/dataxlsx", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,ADMIN')")
	public void downloadTemplateData(
	        
	        HttpServletResponse response,
	        @PathVariable String boardId,
	        @PathVariable String templateId,
	        @RequestParam(required=false) String mode) throws Exception {
				
		try(Workbook wb = new XSSFWorkbook()){
			Collection<Card> cards = this.queryService.fetchCardsApplyTemplateAndOrder(boardId, null, null, "all", null, true, false, false);
			boardToSheet(wb, boardId, null, cards );
			sendWorkbook(boardId, wb, response);
		}
	}
	
	@RequestMapping(value = "/{boardId}/template/{templateId}/cards/{cardId}/dataxlsx", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#boardId, 'BOARD', 'READ,ADMIN')")
	public void downloadTemplateCard(
	        
	        HttpServletResponse response,
	        @PathVariable String boardId,
	        @PathVariable String cardId,
	        @PathVariable String templateId,
	        @RequestParam(required=false) String mode) throws Exception {
				
		try(Workbook wb = new XSSFWorkbook()) {
			Collection<Card> cards = new ArrayList<Card>();
			Card card = cardService.getCard(boardId, cardId);
			if(card!=null){
				cards.add(card);
			}
			boardToSheet(wb, boardId, templateId, cards);
			sendWorkbook(boardId, wb, response);
		}
	}
	
	private Map<String,String> getHeadersFromBoard(Board board){
		Map<String, String> colHeaders = new LinkedHashMap<String,String>();
		try {
			colHeaders.put("Id","Id");
			colHeaders.put("Phase","Phase");
			colHeaders.put("Card Title","Card Title");
			for (Entry<String, TemplateField> entry : board.getFields().entrySet()){
				colHeaders.put(entry.getValue().getName(), entry.getValue().getLabel());
			}
			
		} catch (Exception e) {
			logger.warn("Title Header Exception",e);
		}
		return colHeaders;	
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public TransferStatus getStatus() {
		return status;
	}

	public void setStatus(TransferStatus status) {
		this.status = status;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value = "Error Records by SheetId for currrent user")
	@RequestMapping(value = "/myupload/{uploadId}/{uploadSheetId}/errors", method = RequestMethod.GET)
	public @ResponseBody String exportUploadErrorsForUserByUploadId(@PathVariable String uploadId,@PathVariable String uploadSheetId) throws Exception{
		
		if( this.getStatus().getStatus().equals(TransferStatusEnum.INPROGRESS)){
			throw new Exception("Download Process In Progress");
		}
		
		this.getStatus().newId();
		this.getStatus().setUser(SecurityTool.getCurrentUser());
	
		ExportProcess process = new ExportProcess(this, SecurityTool.getCurrentAuthentication(), uploadId, UploadService.ERRORDOWNLOAD, null, uploadSheetId);
		Thread processThread = new Thread(process);
		processThread.start();
		return this.getStatus().getId();
	}
	/**
	 * Used to generate Error File for a Sheet ID
	 * @param config
	 * @return
	 */
	public Workbook generateWorkbook(String uploadId,String uploadSheetId) {
		Workbook wb = new XSSFWorkbook();
		Collection<Card> cards=new LinkedList<Card>();
		List<UploadSheetErrorDetails> uploadErrors = this.uploadService.getUploadForError(uploadId, uploadSheetId);
		String boardId=uploadSheetId;
		for(UploadSheetErrorDetails error:uploadErrors) {
			Card card=error.getErrorCard();
			card.addField(UploadService.ERRORMESSAGEFIELD, error.getErrorMessage());
			cards.add(error.getErrorCard());
			if (!boardId.equals(error.getBoardId())) {
				boardId=error.getBoardId();
			}
		}
		try {
			errorToSheet(wb, boardId,cards);
		} catch (Exception e) {
			logger.error("Error While Downloading:-"+e.getMessage(),e);
		}
		return wb;
	}
	/**
	 * Called for Downloading Error CARDS for an Upload and Sheet
	 * @param wb
	 * @param boardId
	 * @param cards
	 * @throws Exception
	 */
	private void errorToSheet(Workbook wb, String boardId, Collection<Card> cards) throws Exception{
		Sheet sheet = wb.createSheet(boardId);
		if(cards!=null){
			try {
				Board board = boardsCache.getItem(boardId);
				Map<String, String> headersFromTemplate = getHeadersFromBoard(board);
				headersFromTemplate.put(UploadService.ERRORMESSAGEFIELD, UploadService.ERRORMESSAGEFIELD);
				sheetUtil.writeHeaderRow(sheet, headersFromTemplate);
				int rowCounter = 1;
				for(Card card: cards){
					try{
					sheetUtil.writeCardRow(sheet, card, board, rowCounter);
					rowCounter++;
					}catch(Exception e){
						logger.info("Unable to download " + card.toString());
					}
				}
			} finally {
			}
		}
	}
	

}
