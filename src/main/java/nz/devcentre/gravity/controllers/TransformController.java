/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.automation.TransformEngine;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Transform;
import nz.devcentre.gravity.model.TransformChain;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.repository.TransformRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;
import nz.devcentre.gravity.tools.TransformTool;

@RestController
@RequestMapping("/transform")
public class TransformController {

	private static final Logger logger = LoggerFactory.getLogger(TransformController.class);

	@Autowired
	BoardsCache boardsCache;

	@Autowired
	TransformEngine transformEngine;

	@Autowired
	TransformRepository transformRepository;

	@Autowired
	TransformCache transformCache;

	@Autowired
	CacheInvalidationInterface cacheInvalidationManager;

	@Autowired
	TransformTool transformTool;
	
	@Autowired
	SecurityTool securityTool;
	

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody TransformChain createTransformChain(@RequestBody TransformChain transform, 
			@RequestParam boolean findParams ) throws Exception {

		if( findParams) {
			transform.setParameters(this.transformTool.getTransformationChainParameters(transform));
		}
		
		return transformTool.upsertTransform(transform, true);
	}

	@PreAuthorize("hasPermission(#transformId, 'TRANSFORM', 'ADMIN')")
	@RequestMapping(value = "/{transformId}", method = RequestMethod.PUT)
	public @ResponseBody TransformChain updateTransformChain(@PathVariable String transformId,
			@RequestBody TransformChain transform, 
			@RequestParam boolean findParams) throws Exception {

		if( findParams) {
			transform.setParameters(this.transformTool.getTransformationChainParameters(transform));
		}
		
		return transformTool.upsertTransform(transform, false);
	}

	@PreAuthorize("hasPermission(#transformId, 'TRANSFORM', 'ADMIN')")
	@RequestMapping(value = "/{transformId}/activate", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public TransformChain activateTransformChain(@PathVariable String transformId, @RequestBody String version)
			throws Exception {
		TransformChain transform = transformRepository.findByIdAndVersion(transformId, version);

		TransformChain existing = getTransformChain(transformId, null);

		if (existing.getVersion().equals(version)) {
			return existing;
		}

		existing.setActive(false);
		existing.addOrUpdate();
		transformRepository.save(existing);

		IdentifierTools.newVersion(transform);
		transform.addOrUpdate();
		transform.setActive(true);
		transformRepository.save(transform);

		this.cacheInvalidationManager.invalidate(TransformCache.TYPE, transform.getId());

		return transform;
	}

	@PreAuthorize("hasPermission(#transformId, 'TRANSFORM', 'READ,WRITE,ADMIN')")
	@RequestMapping(value = "/{transformId}", method = RequestMethod.GET)
	public @ResponseBody TransformChain getTransformChain(@PathVariable String transformId,
			@RequestParam(required = false) String version) throws Exception {
		if (StringUtils.isBlank(transformId)) {
			throw new ValidationException("Null or empty transform id");
		}

		TransformChain transform = null;
		if (version == null) {
			transform = transformCache.getItem(transformId);
		} else {
			transform = transformRepository.findByIdAndVersion(transformId, version);
		}

		if (transform == null) {
			logger.info("Falling back to FindOne: " + transformId);
			transform = transformRepository.findOne(transformId);
			if (transform == null) {
				throw new ResourceNotFoundException(
						"TransformChain not found, id: " + Encode.forHtmlContent(transformId));
			}
		}

		return transform;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> listTransformChains(@RequestParam(required = false) boolean all)
			throws Exception {
		Map<String, String> result = new HashMap<String, String>();
		List<TransformChain> list = null;
		if (all) {
			list = transformRepository.findAll();
		} else {
			Map<String, String> map = transformCache.list();
			filterMap(map,"READ,WRITE,ADMIN");
			return map;
		}
		for (TransformChain transform : list) {
			if(this.securityTool.isAuthorised(transform.getPermissions(), "READ,WRITE,ADMIN")){
				result.put(transform.getId(), transform.getName());
			}
		}
		return result;
	}

	@RequestMapping(value = "/plugins", method = RequestMethod.GET)
	public @ResponseBody Map<String, PluginMetadata> listTransformMetadata() throws Exception {
		Map<String, PluginMetadata> metadata = transformEngine.getMetadata();
		return metadata;
	}

	// TODO Add permission enforcement within method
	@RequestMapping(value = "/{transformId}/transformParameters", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> listTransformParameters(@PathVariable String transformId) throws Exception {
		TransformChain transform = this.transformCache.getItem(transformId);
		return this.transformTool.getTransformationChainParameters(transform);		
	}

	@PreAuthorize("hasPermission(#transformId, 'TRANSFORM', 'WRITE,ADMIN')")
	@RequestMapping(value = "/{transformId}/transformParameters", method = RequestMethod.PUT)
	public @ResponseBody TransformChain updateTransformParameters(@PathVariable String transformId) throws Exception {
		TransformChain transform = this.transformCache.getItem(transformId);
		Map<String, String> parameters = this.transformTool.getTransformationChainParameters(transform);		
		transform.setParameters(parameters);
		this.transformTool.upsertTransform(transform, false);
		return transform;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/transformParameters/all", method = RequestMethod.PUT)
	public void updateAllTransformParameters() throws Exception {
		transformTool.updateAllResourceParameters();
	}

	@PreAuthorize("hasPermission(#transformId, 'TRANSFORM', 'ADMIN')")
	@RequestMapping(value = "/{transformId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteTransformChain(@PathVariable String transformId) throws Exception {
		if (StringUtils.isBlank(transformId)) {
			return;
		}

		TransformChain transform = getTransformChain(transformId, null);
		transform.addOrUpdate();
		transform.setActive(false);
		transformRepository.save(transform);
		this.cacheInvalidationManager.invalidate(TransformCache.TYPE, transform.getId());
	}

	// TODO Add permission enforcement within method
	/*
	@RequestMapping(value = "/{transformId}/template", method = RequestMethod.GET)
	public @ResponseBody Board getBoardForTransform(@PathVariable String transformId) throws Exception {

		TransformChain transformChain = getTransformChain(transformId, null);
		Map<String, TemplateField> fields = new HashMap<String, TemplateField>();
		// Iterate through each transform

		int index = 1;
		for (Transform transform : transformChain.getTransforms().values()) {
			if (transform.getTransformer().equals("concat")) {
				String boardId = (String) transform.getConfiguration().get("board");

				try {
					Board board = boardsCache.getItem(boardId);
					for (TemplateField templateField : board.getFields().values()) {
						if (!fields.containsKey(templateField.getName())) {
							TemplateField tf = templateField.copy();
							tf.setIndex(index);
							index++;
							fields.put(tf.getName(), tf);
						} else {
							TemplateField existingField = fields.get(templateField.getName());
							if (existingField.getOptionlist() != null
									&& !existingField.getOptionlist().equals(templateField.getOptionlist())) {

								logger.warn("Transform:" + transformId + " :Board:" + board.getId()
										+ ":Duplicate Name in Transform with Different Optionlists :"
										+ templateField.getName() + " from " + board.getName());
							} else {
								logger.warn("Transform:" + transformId + " :Template:" + board.getId()
										+ ":Duplicate Name :" + templateField.getName() + " from "
										+ board.getName());
							}
						}
					}
				} catch (AccessDeniedException e) {
					logger.warn("Access Denied for Template on board: " + boardId);
				}
			}
		}

		TemplateGroup group = new TemplateGroup("group-name");
		group.setId("default-group-id");
		group.setName("default-group-name");
		Map<String, TemplateGroup> groups = new HashMap<String, TemplateGroup>();
		groups.put("default", group);
		Template newTemplate = new Template();
		newTemplate.setId(transformId);
		newTemplate.setGroups(groups);
		group.setFields(fields);
		return newTemplate;
	}
	*/

	// TODO Add permission enforcement within method
	@RequestMapping(value = "/{transformId}/datasources", method = RequestMethod.GET)
	public @ResponseBody Set<String> getDatasourcesForTransform(@PathVariable String transformId) throws Exception {
		Set<String> result = new HashSet<String>();
		TransformChain transformChain = getTransformChain(transformId, null);

		for (Transform transform : transformChain.getTransforms().values()) {
			if (transform.getTransformer().equals("concat")) {
				String board = (String) transform.getConfiguration().get("board");
				if (board != null)
					result.add(board);
			}
		}
		return result;
	}

	@RequestMapping(value = "/{transformId}/execute", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> executeTransform(@PathVariable String transformId) throws Exception {
		TransformChain transformChain = getTransformChain(transformId, null);
		Map<String, Object> result = transformEngine.execute(transformChain, null, false);
		return result;
	}

	@RequestMapping(value = "/{transformId}/execute", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> executeDynamicTransform(@PathVariable String transformId,
			@RequestBody Map<String, Object> values) throws Exception {
		TransformChain transformChain = getTransformChain(transformId, null);
		Map<String, Object> result = transformEngine.execute(transformChain, values, false);
		return result;
	}

	// TODO Add permission enforcement within method
	@RequestMapping(value = "/execute", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> executeDesigntimeTransform(@RequestBody TransformChain transformChain)
			throws Exception {
		Map<String, Object> result = transformEngine.execute(transformChain, null, false);
		return result;
	}

	// TODO Add permission enforcement within method
	@RequestMapping(value = "/{transformId}/execute/filter", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> executeFilteredTransform(@PathVariable String transformId,
			@RequestBody Filter filter) throws Exception {
		TransformChain transformChain = getTransformChain(transformId, null);
		Map<String, Object> dynaFilter = new HashMap<String, Object>();
		dynaFilter.put("filter", filter);
		Map<String, Object> result = transformEngine.execute(transformChain, dynaFilter, false);
		return result;
	}

	// TODO Add permission enforcement within method
	@RequestMapping(value = "/{transformId}/execute/aslist", method = RequestMethod.GET)
	public @ResponseBody Collection<Object> executeTransformAsList(@PathVariable String transformId) throws Exception {
		Map<String, Object> result = executeTransform(transformId);
		if (result != null) {
			return result.values();
		} else {
			return null;
		}
	}

	/**
	 * This execute transform method is only available internally to perform
	 * rollups.
	 * 
	 * @param transformId
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> executeRollupTransform(String transformId) throws Exception {
		TransformChain transformChain = getTransformChain(transformId, null);
		Map<String, Object> result = transformEngine.execute(transformChain, null, true);
		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/type/{type}", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> listTransformChainsByType(@PathVariable String type,
			@RequestParam(required = false) boolean all) throws Exception {

		if (StringUtils.isBlank(type)) {
			throw new ValidationException("Null or empty transform-type");
		}

		Map<String, String> result = new HashMap<String, String>();
		List<TransformChain> list = null;
		if (all) {
			list = transformRepository.findByType(type);
		} else {
			list = transformRepository.findByTypeAndActiveTrue(type);
		}

		for (TransformChain transform : list) {
			if(this.securityTool.isAuthorised(transform.getPermissions(), "READ,WRITE,ADMIN")){
				result.put(transform.getId(), transform.getName());
			}
		}
		return result;
	}
	
	private void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String id = it.next();
			Map<String, String> permissions = this.transformCache.getItem(id).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}
		}
	}
}
