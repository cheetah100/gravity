/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.controllers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.validation.ValidationException;

import org.apache.commons.lang3.StringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Application;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.Preference;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.repository.UserRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.BoardService;
import nz.devcentre.gravity.services.TaskService;
import nz.devcentre.gravity.tools.IdentifierTools;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Value("${default_application:idp}")
	private String defaultApplication;

	@Autowired
	private TaskService taskService;

	@Autowired
	protected ApplicationCache applicationCache;

	@Autowired
	private SecurityTool securityTool;

	@Autowired
	protected UserPreferencesController userPreferencesController;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BoardService boardTools;

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.POST)
	public @ResponseBody User createUser(@RequestBody User user) throws Exception {

		String newId = IdentifierTools.getIdFromNamedModelClass(user);
		if (userRepository.exists(newId))
			throw new ValidationException("UserId already exists, id: " + Encode.forHtmlContent(newId));

		user.setId(newId);
		user.setPasswordhash(this.securityTool.hash(user.getKey()));
		
		userRepository.save(user);
		user.setPasswordhash(null);

		// Assign Teams if info present
		if (user.getTeams() != null) {
			this.addUserToTeams(user.getId(), user.getTeams());
		}
		return user;
	}

	@RequestMapping(value = "/current", method = RequestMethod.GET)
	public @ResponseBody User getLoggedInUser() throws Exception {
		User user = null;
		String userId = SecurityTool.getCurrentUser();
		user = this.getUser(userId);
		return user;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public @ResponseBody User getUser(@PathVariable String userId) throws Exception {
		return securityTool.getUser(userId);
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
	public @ResponseBody User updateUser(@PathVariable String userId, @RequestBody User user) throws Exception {
		User existing = getUser(userId);
		//add null check before updating teams
		if(user.getTeams()!=null) {
			this.updateUserTeams(userId, user.getTeams());
		}
		copyUser(user, existing);
		//After Copy Save existing User
		return this.userRepository.save(existing);

	}

	protected User copyUser(User from, User to) {
		// Passwordhash & id not allowed to be updated from this method
		if (StringUtils.isNotEmpty(from.getEmail()))
			to.setEmail(from.getEmail());
		if (StringUtils.isNotEmpty(from.getFirstname()))
			to.setFirstname(from.getFirstname());
		if (StringUtils.isNotEmpty(from.getName()))
			to.setName(from.getName());
		if (StringUtils.isNotEmpty(from.getSurname()))
			to.setSurname(from.getSurname());
		if (from.getTeams() != null)
			to.setTeams(from.getTeams());
		return to;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}/tasks", method = RequestMethod.GET)
	public @ResponseBody Collection<CardTask> getUserTasks(@PathVariable String userId) throws Exception {
		return taskService.getOpenTasksByUser(userId);
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
	public @ResponseBody void deleteUser(@PathVariable String userId) throws Exception {
		if (StringUtils.isEmpty(userId)) {
			throw new ResourceNotFoundException("User Not Found");
		}
		userRepository.delete(userId);
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> listUsers() throws Exception {

		logger.info("Getting User List");

		Map<String, String> result = new TreeMap<String, String>();
		List<User> list = userRepository.findAll();
		for (User user : list)
			result.put(user.getId(), user.getFullName());
		return result;
	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}/teams", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getUserTeams(@PathVariable String userId) throws Exception {
		return securityTool.getUserTeams(userId);
	}

	/**
	 * Replace team membership for a user with new team memberships
	 * 
	 * NOTE: this requires the submittor has ownership permissions on all the
	 * respective teams.
	 * 
	 * NOTE: Permissions are evaluated downstream, meaning this request does not
	 * need PreAuthorize Annotation
	 * 
	 * 
	 * @param userId
	 * @param newTeams
	 * @return
	 * @throws Exception
	 */
	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@RequestMapping(value = "/{userId}/teams", method = RequestMethod.PUT)
	public @ResponseBody Map<String, String> updateUserTeams(@PathVariable String userId,
			@RequestBody Map<String, String> newTeams) throws Exception {

		Map<String, String> oldTeams = this.getUserTeams(userId);
		// Delete Old Existing Memberships
		Set<String> keySet = oldTeams.keySet();
		for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
			String teamId = iterator.next();
			if (!newTeams.containsKey(teamId)) {
				securityTool.deleteMember(teamId, userId);
			}
		}
		addUserToTeams(userId, newTeams);
		return newTeams;
	}

	@RequestMapping(value = "/app", method = RequestMethod.GET)
	public @ResponseBody Application getApplication() throws Exception {
		Preference preference = this.userPreferencesController.getPreference("app");
		if (preference.getValues() != null) {
			String appid = preference.getValues().get("app").toString();
			return applicationCache.getItem(appid);
		} else {
			return applicationCache.getItem(this.defaultApplication);
		}
	}

	private void addUserToTeams(String userId, Map<String, String> newTeams) {
		// Update to new memberships
		if (newTeams == null) {
			return;
		}

		Set<String> keySetNew = newTeams.keySet();
		for (Iterator<String> iterator = keySetNew.iterator(); iterator.hasNext();) {
			String teamId = iterator.next();
			String teamRole = newTeams.get(teamId);
			Map<String, String> roles = new HashMap<String, String>();
			roles.put(userId, teamRole);
			try {
				securityTool.addMembers(teamId, roles);
			} catch (Exception e) {
				logger.warn("User not added to team", e);
			}
		}

	}

	@PreAuthorize("hasPermission('administrators', 'TEAM', 'ADMIN')")
	@ApiOperation(value="Get Consolidated List of Users Having Permission on board(s)")
	@RequestMapping(value="/search/board/permissions",method=RequestMethod.POST)
	public @ResponseBody Collection<User> getUsersForBoard(@RequestBody List<String> boardIds,
			@RequestParam(required = false) String permission) throws Exception {
		Map<String,User> result=new HashMap<>();
		for(String boardId:boardIds) {
			try {			
				if (this.boardTools.isUserAccess(boardId, null)) {
					result.putAll(this.boardTools.getBoardUsers(boardId, permission));
				}
			}catch (ResourceNotFoundException e) {
				logger.warn(boardId+" "+e.getClass().getSimpleName());	
			}catch(AccessDeniedException e) {
				logger.warn(boardId+" "+e.getClass().getSimpleName());
			}
		}
		return result.values();
	}
}
