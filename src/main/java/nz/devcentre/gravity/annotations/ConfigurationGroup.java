package nz.devcentre.gravity.annotations;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ConfigurationGroups.class)
public @interface ConfigurationGroup {
	public String group();
}
