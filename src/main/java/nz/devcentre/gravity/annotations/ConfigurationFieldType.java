package nz.devcentre.gravity.annotations;

public enum ConfigurationFieldType {
	STRING,
	BOARD,
	RESOURCE,
	CREDENTIAL,
	NUMBER,
	BOOLEAN,
	MAP,
	LIST,
	FIELD,
	PHASE,
	CRONEXPRESSION
}
