/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import nz.devcentre.gravity.managers.ExceptionManager;

@ControllerAdvice
public class GlobalExceptionHandlers {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandlers.class);

	@Autowired
	private ExceptionManager exceptionManager;
	
	@ExceptionHandler(CardValidationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody Object handleDataValidationException(HttpServletRequest request, CardValidationException e) {    
		logger.warn("Validation Exception Handled: " + e.getClass().getCanonicalName() + " - " + e.getMessage()+getLoggerWarningMessage(request),e);
		return e.getMessages();
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public @ResponseBody Object handleResourceNotFoundException(HttpServletRequest request, ResourceNotFoundException e) {    
		logger.warn("Resource Not Found Exception Handled: " + e.getClass().getCanonicalName()+getLoggerWarningMessage(request));
		return "Resource Not Found";
	}

	@ExceptionHandler(AutomationExecutionException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody Object handleAutomationExecutionException(HttpServletRequest request, AutomationExecutionException e) {    
		logger.warn("Automation Execution Exception Handled: " + e.getClass().getCanonicalName()+getLoggerWarningMessage(request));
		return "Automation Execution Exception: " + e.getMessage();
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public @ResponseBody Object handleAccessDeniedException(HttpServletRequest request, AccessDeniedException e) {
		String warning = getLoggerWarningMessage(request);
		logger.warn("Access Denied Exception Handled: " + e.getClass().getCanonicalName() + warning);
		this.exceptionManager.raiseException("access_denied", warning, null, true);
		return "Access Denied";
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody Object handleHttpMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException e) {    
		logger.warn(" HttpMessageNotReadable Exception Handled: " + e.getClass().getCanonicalName()+getLoggerWarningMessage(request));
		return "Validation Error";
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Object handleException(HttpServletRequest request, Exception e) {
		logger.warn("Exception Handled: " + e.getClass().getCanonicalName() + " - " + e.getMessage()+getLoggerWarningMessage(request),e);
		return "Server Error";
	}

	private String getLoggerWarningMessage(HttpServletRequest request) {
		if(request==null) {
			return "";
		}
		
		String loggerWarningMessage=" On Resource URL=" + request.getRequestURL();
		
		String user = request.getRemoteUser();
		if(StringUtils.isNotEmpty(request.getRemoteUser())) {
			loggerWarningMessage = loggerWarningMessage + " for user=" + user;
		} 

		return loggerWarningMessage;
	}
}
