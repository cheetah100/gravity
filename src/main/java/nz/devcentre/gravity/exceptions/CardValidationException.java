/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.exceptions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpException;
import org.owasp.encoder.Encode;

public class CardValidationException extends HttpException {

	Map<String, Object> messages = new HashMap<String,Object>();
	
	public CardValidationException( ){
		super();
	}
	
	public CardValidationException( String message ){
		super(Encode.forHtmlContent(message));
	}
	
	public CardValidationException(Map<String, Object> messages){
		super();
		this.messages = messages;
	}

	public Map<String, Object> getMessages() {
		return messages;
	}

	public void setMessages(Map<String, Object> messages) {
		this.messages = messages;
	}
	
	public String getMessage(){
		StringBuilder builder = new StringBuilder();
		for(Entry<String,Object> entry : this.messages.entrySet()){
			builder.append("(");
			builder.append(Encode.forHtmlContent(entry.getKey()));
			builder.append(" : ");
			builder.append(entry.getValue());
			builder.append(")");
		}
		return builder.toString();
	}

}