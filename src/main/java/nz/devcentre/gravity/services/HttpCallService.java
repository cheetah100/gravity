/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import nz.devcentre.gravity.controllers.CredentialCache;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.CallResponse;
import nz.devcentre.gravity.tools.JWTTokenTool;
import nz.devcentre.gravity.tools.SAMLTokenTool;

@Component
public class HttpCallService {

	protected static final Logger logger = LoggerFactory.getLogger(HttpCallService.class);
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private SAMLTokenTool samlTokentool;
	
	@Autowired 
	private CredentialCache credentialCache;
	
	//@Autowired
	//private CredentialRepository credentialRepository;
		
	private Map<String, AccessToken> tokenMap = new HashMap<>();
	
	private Map<String, String> cookies = new HashMap<>();

	public class AccessToken {
		
		private String accessToken;
		
		private String idToken;
		
		private String refreshToken;
		
		private Double expiration;

		public AccessToken(String accessToken, String idToken, String refreshToken, Double expiration, boolean usmImportFlag) {
			this.accessToken = accessToken;
			this.idToken = idToken;
			this.refreshToken = refreshToken;
			
			// Set time that token expires
			if (usmImportFlag) { // USM case usmImport is true
				this.expiration = expiration * 1000;
				if( logger.isDebugEnabled()) {
					logger.debug("Expiry (Unix): " + this.expiration);
				}
			} else { // USM case usmImport is false, AKA Normal
				this.expiration = System.currentTimeMillis() + (expiration * 1000);
				if( logger.isDebugEnabled()) {
					logger.debug("Expiry (normal):" + this.expiration);
				}
			}
		}
		
		private String getAccessToken() {
			return this.accessToken;
		}

		public String getIdToken() {
			return this.idToken;
		}

		public String getRefreshToken() {
			return this.refreshToken;
		}

		public Double getExpiration() {
			return this.expiration;
		}
	}

	public CallResponse execute(String body, Map<String, Object> parameters, String endPoint, String method,
			Map<String, String> headers, String credentialId, int retries, int waitPeriod) throws Exception {

		if( logger.isDebugEnabled()) {
			logger.debug("HTTP Call: " + endPoint 
					+ " body: " + body 
					+ " Authorization: " + headers.get("Authorization") 
					+ " Content-Type: " + headers.get("Content-Type") 
					+ " Method: " + method);
		}
		
		int numberOfRetries = 0;
		Credential credential = null;
		if (credentialId != null) {
			credential = this.securityTool.getSecureCredential(credentialId);
		}

		CallResponse callResponse = new CallResponse();		
		
		while (numberOfRetries < retries) {
			String oauthToken = null;
			if (credential != null) {
				oauthToken = getSsoToken(credential);
			}

			HttpURLConnection uc = null;
			String resultUrl = ((String) CardService.parseField(endPoint, parameters, null)).replaceAll(" ", "%20");
			if(logger.isDebugEnabled()) {
				logger.debug("HTTP url ->" + method + " " + resultUrl);
			}

			URL url;
			try {
				url = new URL(resultUrl);
				uc = (HttpURLConnection) url.openConnection();
				uc.setRequestMethod(method);
				uc.setDoOutput(true);

				if (oauthToken != null) {
					uc.setRequestProperty("Authorization", "Bearer " + oauthToken);
				}

				if ("POST".equalsIgnoreCase(method) || "PUT".equalsIgnoreCase(method)) {
					uc.setDoInput(true);
					uc.setRequestProperty("Content-Length", "" + Integer.toString(body.getBytes().length));
					if( headers!=null && !headers.containsKey("Content-Type")) {
						uc.setRequestProperty("Content-Type", "application/json");
					}
				}

				if (headers != null) {
					for (Entry<String, String> entry : headers.entrySet()) {
						Object value = CardService.parseField(entry.getValue(), parameters, null);
						if(logger.isDebugEnabled()) {
							logger.debug("Adding Header: " + entry.getKey() + ":"+ value.toString());
						}
						uc.setRequestProperty(entry.getKey(), value.toString());
					}
				}
				
				String cookie = getCookie(credentialId);
				if (cookie != null) {
					uc.setRequestProperty("Cookie", cookie);
				}

				uc.connect();

				if ("POST".equalsIgnoreCase(method) || "PUT".equalsIgnoreCase(method)) {
					DataOutputStream wr = new DataOutputStream(uc.getOutputStream());
					wr.writeBytes(body);
					wr.flush();
					wr.close();
				}

				InputStream is = uc.getInputStream();
				callResponse.setBody(stringFromContentOfInputStream(is));
				callResponse.setHeaders(uc.getHeaderFields());
				storeCookie(credentialId, uc.getHeaderField("Set-Cookie"));
				
				if( logger.isDebugEnabled()) {
					logger.debug("HTTP response ->" + callResponse.getBody());
				}
				break; // successful case break out of while loop
				
			} catch (IOException e) {
				
				logger.info("Exception on HTTP Call:" + e.getMessage(),e);
				if(tokenMap != null) {
					if(credential != null) {
						if(tokenMap.containsKey(credential.getId())) {	
							tokenMap.remove(credential.getId());
						}
					}
				}
				
				Thread.sleep(waitPeriod*1000);				
				numberOfRetries++;
				
			} finally {
				if (null != uc) {
					uc.disconnect();
				}
			}
		}
		return callResponse;
	}

	private static String stringFromContentOfInputStream(InputStream is) throws IOException {
		if (null == is) {
			throw new IllegalArgumentException("cannot produce a string from a null input stream");
		}

		InputStreamReader isr = new InputStreamReader(is, "UTF-8");
		StringBuffer sb = new StringBuffer();
		char buffer[] = new char[1024];
		int dataRead;

		while ((dataRead = isr.read(buffer, 0, buffer.length)) >= 0) {
			sb.append(new String(buffer, 0, dataRead));
		}

		return sb.toString();
	}

	public String getSsoToken(Credential credential) throws Exception {
		AccessToken lastToken = tokenMap.get(credential.getId());

		if (lastToken != null) {
			Double lastFetchTime = lastToken.expiration;
			long now = System.currentTimeMillis();
			if (lastFetchTime.longValue() > now) {
				if(logger.isDebugEnabled()) {
					logger.debug("Cached Token: " + lastToken.getAccessToken() + " expiry: " + lastFetchTime.longValue());
				}
				return lastToken.getAccessToken();
			}
		}

		AccessToken resultToken = ssoLogon(credential);
		if (resultToken != null) {
			tokenMap.put(credential.getId(), resultToken);
			if(logger.isDebugEnabled()) {
				logger.debug("New Token: " + resultToken.getAccessToken() + " expiry: " + resultToken.expiration.longValue());
			}
			return resultToken.getAccessToken();
		}

		return null;
	}

	public AccessToken ssoLogon(Credential credential) throws Exception {

		if (credential.getMethod() == null) {
			throw new InvalidParameterException("Credential Lacks Method");
		}

		if (credential.getMethod().equalsIgnoreCase("basic")) {
			return new AccessToken(credential.getBasicAuth(), null, null, Double.MAX_VALUE, credential.getResponseFields().containsKey("expirationIsUnix"));
		}

		if (credential.getMethod().equalsIgnoreCase("bearer")) {
			String userPass = "Bearer " + credential.getSecret();
			return new AccessToken(userPass, null, null, Double.MAX_VALUE, credential.getResponseFields().containsKey("expirationIsUnix"));
		}
		
		if (credential.getMethod().toLowerCase().startsWith("oauth")) {

			String result = null;
			Map<String, String> headers = new HashMap<>();
			String url = credential.getResourceWithCreds();
			
			String body = "";

			if (credential.getMethod().equalsIgnoreCase("oauth")) {
				url += ("?" + "client_id=" + credential.getIdentifier() + "&client_secret=" + credential.getSecret()
						+ "&grant_type=client_credentials");
			} else if (credential.getMethod().equalsIgnoreCase("oauth_basic")) {
				headers.put("Authorization", credential.getBasicAuth());
				if( credential.getRequestMethod().equals("POST")) {
					headers.put("Content-Type", "application/x-www-form-urlencoded");				
					body = "grant_type=client_credentials";
					if(credential.getResponseFields().containsKey("scope")) {
						body = body + "&scope=" + credential.getResponseFields().get("scope"); 
					}
				}
			} else if (credential.getMethod().equalsIgnoreCase("oauth_code")) {
				
				headers.put("Authorization", credential.getBasicAuth());
				headers.put("Content-Type", "application/x-www-form-urlencoded");
				headers.put("Accept", "*/*");
				
				if( credential.getRequestMethod().equals("POST")) {
					if(credential.getResponseFields().get("refresh_token")!=null) {
						body = "grant_type=refresh_token&refresh_token=" 
						+ credential.getResponseFields().get("refresh_token");
					} else if(credential.getResponseFields().get("code")!=null){
						body = "grant_type=authorization_code&code=" 
						+ credential.getResponseFields().get("code")
						+ "&redirect_uri=" + URLEncoder.encode(credential.getResponseFields().get("redirectUrl"),"UTF-8");
					} else {
						throw new Exception("Expecting code or refresh_token");
					}
				}
				
			} else if (credential.getMethod().equalsIgnoreCase("oauth_jwt")) {
				String jwtToken =	JWTTokenTool.makeJwtToken(credential);
				headers.put("Authorization", jwtToken);
			} else if (credential.getMethod().equalsIgnoreCase("oauth_saml")) {
				String samlToken =	samlTokentool.getSamlToken(credential);
				url = url.replace("{saml_token}", samlToken);
			}
			
			if(credential.getResponseFields()!=null) {
				for( Entry<String,String> entry: credential.getResponseFields().entrySet()) {
					if( entry.getKey().startsWith("header:")) {
						headers.put(entry.getKey().substring(7), entry.getValue());
					}
				}
			}
			
			//TODO create a  logic to read saml token from cache and check its vallidity.
			if (credential.getRequestMethod() != null) {
				result = execute(body, null, url, credential.getRequestMethod(), headers, null, 1, 1).getBody();
			} else {
				result = execute(body, null, url, "POST", headers, null, 1, 1).getBody();
			}
			
			if (result != null) {
				if(logger.isDebugEnabled()) {
					logger.debug("SSO Login Result: {}", result);
				}
				
				Map<String, Object> tokenMap = new HashMap<>();
				tokenMap = (Map<String, Object>) new Gson().fromJson(result, tokenMap.getClass());
				Map<String, String> responseFields = credential.getResponseFields();
				
				String accessTokenField = "access_token";
				if(responseFields.containsKey("accessTokenField")) { 
					accessTokenField = responseFields.get("accessTokenField");
				}
				
				String expirationField = "expires_in";
				if(responseFields.containsKey("expirationField")) {
					expirationField = responseFields.get("expirationField");
				}
				
				String refreshToken = null;
				if( tokenMap.containsKey("refresh_token")) {
					refreshToken = (String) tokenMap.get("refresh_token");
					this.credentialCache.updateRefreshToken(credential.getId(), refreshToken);
				}

				String idToken = null;
				if( tokenMap.containsKey("id_token")) {
					idToken = (String) tokenMap.get("id_token");
				}
				
				return new AccessToken( (String) tokenMap.get(accessTokenField),
						idToken,
						refreshToken,
						(Double) tokenMap.get(expirationField), 
						credential.getResponseFields().containsKey("expirationIsUnix"));
			}
			
			throw new Exception("Empty SSO Login Response");
			
		}
		throw new Exception("Invalid Credential");
	}

	public String getCookie(String id) {
		if (id == null) {
			return null;
		}
		if (cookies.containsKey(id)) {
			return cookies.get(id);
		}
		return null;
	}

	public void storeCookie(String id, String cookie) {
		if (id != null) {
			this.cookies.put(id, cookie);
		}
	}

	public Map<String, AccessToken> getTokens() {
		return this.tokenMap;
	}

	public void releaseToken(String tokenId) {
		this.tokenMap.remove(tokenId);
		this.cookies.remove(tokenId);
	}

}
