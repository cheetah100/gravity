/**	
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.assignment.AssignmentStrategy;
import nz.devcentre.gravity.assignment.DirectAssignment;
import nz.devcentre.gravity.assignment.PropertyAssignment;
import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.AssignmentStrategyName;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.CardTask;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.transfer.TaskAction;
import nz.devcentre.gravity.repository.CardTaskRepository;
import nz.devcentre.gravity.security.SecurityTool;

@Service
public class TaskService {
	
	private static final Logger logger = LoggerFactory.getLogger(TaskService.class);
	
	@Autowired
	private CardTaskRepository cardTaskRepository;
	
	@Autowired
	private AuditService auditService;
	
	@Autowired 
	private RuleCache ruleCache;
	
	@Autowired
	private CardListener cardListener;
	
	@Autowired
	private DirectAssignment directAssignment;
	
	@Autowired
	private PropertyAssignment propertyAssignment;

	public CardTask getTask(String boardId, String cardId, String taskId) throws ResourceNotFoundException {
		return this.cardTaskRepository.findByBoardAndCardAndTaskid(boardId, cardId, taskId);
	}

	public Collection<CardTask> getOpenTasksByUser(String user) throws ResourceNotFoundException {
		return this.cardTaskRepository.findByUserAndComplete(user, false);
	}

	public Collection<CardTask> getOpenTasksByRuleId( String boardId, String ruleId){
		return this.cardTaskRepository.findByBoardAndTaskidAndComplete(boardId, ruleId, false);
	}
	
	
	/**
	 * Method adds a task to a card based on a rule. Note that this method swallows any exceptions.
	 * 
	 * @param rule
	 * @param card
	 */
	public void createTaskFromRule(Rule rule, Card card) {
		try {
			
			if( this.getTask(card.getBoard(), card.getId(), rule.getId()) != null ){
				logger.warn("Task already exists when Create Task is called: " + card.getBoard() + "." + card.getId() + "." + rule.getId());
				return;
			}
			
			CardTask cardTask = new CardTask();
			cardTask.setBoard(card.getBoard());
			cardTask.setCard(card.getId());
			cardTask.setTaskid(rule.getId());
			
			String title = null;
			if( rule.getTaskName()!=null){
				title = rule.getTaskName();
			} else {
				title = rule.getName();	
			}
			
			title = title.replaceAll("\\{id\\}", card.getId());
			for(Entry<String,Object> entry : card.getFields().entrySet()){
				if(entry.getKey()!=null && entry.getValue()!=null) {
					title = title.replaceAll("\\{"+ entry.getKey() +"\\}", entry.getValue().toString());
				}
			}
			
			cardTask.setDetail(title);		
			cardTask.setComplete(false);
			
			if(rule.getAssignmentStrategy()!=null){
				AssignmentStrategy assignmentStrategy = getAssignmentStrategy(rule.getAssignmentStrategy().toString());
				if (assignmentStrategy != null)
					cardTask.setUser(assignmentStrategy.getAssignment(rule.getAssignto(), card));
			}
			
			cardTaskRepository.save(cardTask);
			
			logger.info("Added Task: " + cardTask.getTaskid() + " on card " + card.getId());
		} catch( Exception e){
			logger.warn("Add New Task threw Exception with Rule " + rule.getId(), e);
		}
		
	}
	
	public boolean taskAction( CardTask task, TaskAction action, String user ) throws Exception {
		
		if( task.getComplete()) {
			if(action.equals(TaskAction.COMPLETE) || action.equals(TaskAction.ASSIGN)){
				return false;
			}
		} else {
			if( action.equals(TaskAction.REVERT)){
				return false;
			}	
		}
		
		if( action.equals(TaskAction.ASSIGN) ||  action.equals(TaskAction.COMPLETE)) {
			if(user!= null){
				task.setUser(user);
			} else {
				task.setUser(SecurityTool.getCurrentUser());
			}
		}
		
		if( action.equals(TaskAction.COMPLETE)) {
			task.setComplete(true);
			task.setOccuredTime(new Date());
		}
		
		if( action.equals(TaskAction.REVERT)) {
			task.setComplete(false);
			task.setOccuredTime(null);
			task.setUser(null);
		}

		cardTaskRepository.save(task);

		String detail = String.format("{} {} by {}", action.toString(), task.getDetail(), task.getUser()); 
		auditService.storeCardEvent( detail, task.getBoard(), task.getCard(), 
				"info", "complete-" + task.getTaskid(), 
				SecurityTool.getCurrentUser(), new Date(), null);

		cardListener.addCardHolder(new CardHolder(task.getBoard(), task.getCard()));
		return true;
		
		
	}
	
	public void revertTasks(String boardId, String taskId) throws Exception {
		Collection<CardTask> tasks = this.cardTaskRepository.findByBoardAndTaskidAndComplete(boardId, taskId, true);
		for( CardTask task : tasks) {
			taskAction(task, TaskAction.REVERT, null);
		}
	}
	
	public void revertTask(String boardId, String cardId,String taskId) throws Exception {
		CardTask task = this.cardTaskRepository.findByBoardAndCardAndTaskid(boardId, cardId,taskId);
		taskAction(task, TaskAction.REVERT, null);
	}
	
	public AssignmentStrategy getAssignmentStrategy( String name ){
		if( name.equals(AssignmentStrategyName.DIRECT.toString())){
			return this.directAssignment;
		}
		if( name.equals(AssignmentStrategyName.PROPERTY.toString())){
			return this.propertyAssignment;
		}
		return null;
	}
	
	public Collection<CardTask> getTasks(String boardId, String cardId) throws Exception {
		Collection<CardTask> tasks = cardTaskRepository.findByBoardAndCard(boardId, cardId); 
		return validateTasksHaveRules(tasks);
	}
	
	public Collection<CardTask> getTasksByStatus(String boardId, String cardId, boolean complete) throws Exception {
		Collection<CardTask> tasks = cardTaskRepository.findByBoardAndCardAndComplete(boardId, cardId,complete); 
		return validateTasksHaveRules(tasks);
	}

	/**
	 * This method filters out tasks with no associated rule,
	 * and also orders the resulting collection based on the
	 * index of the rule.
	 * 
	 * @param tasks
	 * @return
	 * @throws Exception 
	 */
	private Collection<CardTask> validateTasksHaveRules( Collection<CardTask> tasks) throws Exception{
		
		Map<String, CardTask> tasksAdded = new HashMap<String,CardTask>();
		
		if(tasks!=null){				
			for (CardTask task : tasks) {
				try {
					
					// Has a task already been added for this taskid? If so delete this task.
					if( tasksAdded.containsKey(task.getTaskid())) {
						logger.warn("Redundant CardTask Record to be deleted: " + 
								task.getBoard() + "." + 
								task.getCard() + "." + 
								task.getTaskid() + "." +
								task.getUuid());
						
						this.cardTaskRepository.delete(task);
						continue;
					}
					
					Rule rule = ruleCache.getItem(task.getBoard(),task.getTaskid());
					
					if(rule!=null){
						tasksAdded.put(task.getTaskid(),task);
					}
					
				}catch (ResourceNotFoundException e) {
					logger.warn("TASK Exists but no Rule" + task.getBoard() + ":" + task.getCard() + ":" + task.getTaskid());
				}
			}
		}
		
		return tasksAdded.values();
	}
	
	public void checkAssignment(Rule rule, Card card, CardTask task) throws Exception{

		if(rule.getAssignmentStrategy()==null){
			return;
		}
		
		AssignmentStrategy assignmentStrategy = getAssignmentStrategy(rule.getAssignmentStrategy().toString());
		
		String userId = null;
		if (assignmentStrategy != null)
			userId = assignmentStrategy.getAssignment(rule.getAssignto(), card);
		
		if(userId!=null && !userId.equals(task.getUser())){
			task.setUser(userId);
			cardTaskRepository.save(task);
			logger.info("updated task assignment: " + task.getTaskid() + " on card " + task.getCard() + " to user " + userId);
		}		
	}
	
	public boolean deleteTask(CardTask task) throws Exception {
		try {
			cardTaskRepository.delete(task);
			return true;
		} catch( Exception e) {
			return false;
		}
	}
	
}
