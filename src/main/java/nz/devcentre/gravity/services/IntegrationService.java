/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.managers.JdbcTemplateManager;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.DatabaseDriver;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.repository.BatchRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;

@Component
public class IntegrationService {
	
	private static final Logger logger = LoggerFactory.getLogger(IntegrationService.class);
	
	@Autowired
	private BatchRepository batchRepository;
	
	@Lazy
	@Autowired
	private AutomationEngine automationEngine;
	
	@Autowired
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private BoardService boardService;
	
	public Batch createBatch(Integration integration, int rows, Date maxProcessDate) {
		Batch batch = new Batch();
		batch.setNotificationId(integration.getId());
		batch.setMaxProcessDate(maxProcessDate);
		batch.setRowsImported(rows);
		batch.addOrUpdate();
		Batch insert = batchRepository.insert(batch);
		return insert;
	}

	public void updateBatch(Batch batch) {
		batchRepository.save(batch);
	}
	
	/**
	 * Gets the latest batch for the integration
	 * Because there is an index this should get the latest based on ordering.
	 * 
	 * @param integration
	 * @return
	 */
	public Batch getLatestBatch(Integration integration) {
		return batchRepository.findByNotificationIdOrderByMaxProcessDateDesc(integration.getId());
	}
	
	public Object execute(Map<String,Object> context, Integration integration, Batch batch) throws Exception {
		return this.automationEngine.executeIntegrationActions(context, integration, batch);
	}
	
	/**
	 * This is used exclusively for SQL Tables - to list them.
	 * @param credentialId
	 * @return
	 * @throws Exception 
	 */
	public List<String> getTables( String credId) throws Exception{
		List<String> results = new ArrayList<>();				
		JdbcTemplate template = this.jdbcTemplateManager.getTemplate(credId);
		
		Credential credential = securityTool.getSecureCredential(credId); 
		DatabaseDriver driver = DatabaseDriver.fromUrl(credential.getResource());		
		String query = driver.getSchemaQuery();
		
		template.query(query, new RowCallbackHandler() {

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				results.add(rs.getString("name"));
			}
		});
		return results;
	}
	
	public Board getBoardFromTable( String credId, String tableName, boolean save) throws Exception{

		Board board = new Board();
		board.addOrUpdate();
		
		board.setFields(new HashMap<>());
		board.setId(IdentifierTools.getIdFromName(tableName));
		board.setName(tableName);
		board.setTableName(tableName);
		board.setBoardType(BoardType.REMOTE);
		board.setCredentialId(credId);
		
		board.setPermissions(this.securityTool.initPermissions(board.getPermissions()));
		board.setActive(true);
		
		Phase current = new Phase();
		current.setId("current");
		current.setName("Current");
		current.setInvisible(false);
		current.setIndex(1);
		
		Phase archived = new Phase();
		archived.setId("archived");
		archived.setName("Archived");
		archived.setInvisible(true);
		archived.setIndex(2);

		Map<String, Phase> phases= new HashMap<>();;
		phases.put("current", current);
		phases.put("archived", archived);
		board.setPhases(phases);
		
		JdbcTemplate template = this.jdbcTemplateManager.getTemplate(credId);
		
		Credential credential = securityTool.getSecureCredential(credId); 
		DatabaseDriver driver = DatabaseDriver.fromUrl(credential.getResource());	
		
		String query = String.format(driver.getTableQuery(), tableName);
		
		logger.info( "Get Table -" +  driver + " " + query);
		
		template.query(query, new RowCallbackHandler() {

			@Override
			public void processRow(ResultSet rs) throws SQLException {
				
				TemplateField field = new TemplateField();
				
				String fieldName = rs.getString(driver.getFieldKey());
				field.setId(fieldName);
				field.setName(fieldName);
				field.setLabel(fieldName);
				
				String type = rs.getString(driver.getTypeKey());
				
				String nullable = rs.getString(driver.getNullKey());
				
				logger.info(fieldName + "." + type);
				
				if(type.contains("char")) {
					field.setType(FieldType.STRING);
				}
				if(type.contains("int")) {
					field.setType(FieldType.NUMBER);
				}
				if(type.contains("float")) {
					field.setType(FieldType.NUMBER);
				}
				if(type.contains("money")) {
					field.setType(FieldType.NUMBER);
				}	
				if(type.contains("date")) {
					field.setType(FieldType.DATE);
				}
				if( nullable.equals("NO")) {
					field.setRequired(true);
				} else {
					field.setRequired(false);
				}
				
				if( !driver.getKeyKey().equals("") && rs.getString(driver.getKeyKey()).equals("PRI")) {
					board.setKeyField(field.getId());
				}
				
				board.getFields().put(field.getId(), field);
			}
		});
		
		if( save ) {
			return boardService.saveBoard(board);
		}
		
		return board;	
	}
}
