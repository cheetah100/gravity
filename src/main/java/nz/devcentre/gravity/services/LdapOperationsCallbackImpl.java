/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.naming.directory.SearchControls;

import org.springframework.ldap.control.PagedResultsDirContextProcessor;
import org.springframework.ldap.core.LdapOperations;
import org.springframework.ldap.core.support.LdapOperationsCallback;

import nz.devcentre.gravity.tools.LdapUserAttributesMapper;



public class LdapOperationsCallbackImpl implements LdapOperationsCallback<List<Map<String, Object>>> {
	
	public LdapOperationsCallbackImpl(String collectionQuery) {
		this.collectionQuery = collectionQuery;
	}

	final SearchControls searchControls = new SearchControls();

	// used processor instead of ldapTemplate for pagination
	final PagedResultsDirContextProcessor processor = new PagedResultsDirContextProcessor(1000);
	  
	
	private LdapUserAttributesMapper userAttributesMapper = new LdapUserAttributesMapper();
	  
	private String collectionQuery;
	  
	@Override
    public List<Map<String, Object>> doWithLdapOperations(LdapOperations operations) {
		 
       List<Map<String, Object>> result = new LinkedList<Map<String, Object>>();
       
       searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
       
       do {
       	List<Map<String, Object>> oneResult = operations.search(
           "",
           collectionQuery,
           searchControls,
           userAttributesMapper,
           processor);
         result.addAll(oneResult);
         
       } while(processor.hasMore());

       return result;
     }

}
