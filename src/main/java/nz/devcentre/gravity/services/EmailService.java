/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.services;

import javax.activation.DataSource;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.BoardResource;
import nz.devcentre.gravity.repository.BoardResourceRepository;

@Component
public class EmailService {

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	JavaMailSenderImpl mailSender;
	
	@Autowired
	BoardResourceRepository boardResourceRepository;
	
	public EmailService() {
		super();
	}

	public EmailService(JavaMailSenderImpl mailSender) {
		super();
		this.mailSender = mailSender;
	}
	
	public void sendEmail( 
			String subject, 
			String emailBody, 
			String to, 
			String bcc, 
			String from, 
			String replyTo,
			boolean html,
			String attachmentResourceId,
			String attachmentBoardId) throws Exception {
		
		MimeMessage mailMessage = mailSender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

		if(StringUtils.isNotBlank(to)){
			helper.setTo(to);
		}
		
		if(StringUtils.isNotBlank(bcc)){
			helper.setBcc(bcc);
		}
		
		if(StringUtils.isNotBlank(from)){
			helper.setFrom(from);
		}
		
		if(StringUtils.isNotBlank(replyTo)){
			helper.setReplyTo(replyTo);
		}
		
		if(StringUtils.isNotBlank(subject)){
			helper.setSubject(subject);
		}
		
		helper.setText(emailBody, html);
		
		if( attachmentResourceId!=null) {
			DataSource ds = getDataSourceFromResource( attachmentBoardId, attachmentResourceId );	
			helper.addAttachment(ds.getName(), ds);
			logger.info(ds.getName() + " " + ds.getContentType());
		}
		
		mailSender.send(mailMessage);
		logger.info("Email Message has been sent to " + to + " and " + bcc);
	}
	
	
	private DataSource getDataSourceFromResource( String boardId, String resourceId) throws Exception {
		
		BoardResource resource = this.boardResourceRepository.findByBoardIdAndIdAndActiveTrue(boardId, resourceId);
		
		if ( resource==null) {
			throw new ResourceNotFoundException( "Resource Not Found: " + boardId + "." + resourceId);
		}

		ByteArrayDataSource ds = null;
		if(resource.getFile()!=null) {
			Binary file = resource.getFile();	
			ds = new ByteArrayDataSource(file.getData(),resource.getMimetype());
		} else if(resource.getResource()!=null) {
			ds = new ByteArrayDataSource(resource.getResource().getBytes("utf-8"),resource.getMimetype());
		}
		ds.setName( resource.getFilename());
		
		return ds;
	}
}
