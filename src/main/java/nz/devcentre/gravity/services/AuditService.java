/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.repository.CardEventRepository;

@Service
public class AuditService {
	
	private static final Logger logger = LoggerFactory.getLogger(AuditService.class);
	
	@Autowired
	private CardEventRepository cardEventRepository;
	
	public CardEvent storeCardEvent(String detail, String boardId, String cardId, String level, String category,
			String user, Date time, Map<String, CardEventField> fields) throws Exception {

		if (time == null)
			time = new Date();

		CardEvent event = new CardEvent();
		event.setBoard(boardId);
		event.setCard(cardId);
		event.setUser(user);
		event.setOccuredTime(time);
		event.setDetail(detail);
		event.setLevel(level);
		event.setCategory(category);
		event.setFields(fields);
		logger.info("Card Event: Saving History " + event.toString());
		cardEventRepository.save(event);
		return event;	
	}
	
}
