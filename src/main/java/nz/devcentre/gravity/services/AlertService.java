/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardAlert;
import nz.devcentre.gravity.model.CardEvent;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.repository.CardAlertRepository;
import nz.devcentre.gravity.security.SecurityTool;

@Service
public class AlertService {
	
	private static final Logger logger = LoggerFactory.getLogger(AlertService.class);

	@Autowired
	private CardAlertRepository cardAlertRepository;
	
	@Autowired
	private CardListener cardListener;
	
	@Autowired
	private AuditService auditService;
	
	public List<CardAlert> getBoardAlerts(String boardId) throws Exception {
		return cardAlertRepository.findByBoard(boardId);
	}

	public List<CardAlert> getCardAlerts(String boardId, String cardId) throws Exception {
		return cardAlertRepository.findByBoardAndCard(boardId, cardId);
	}

	public List<CardAlert> getCardAlerts(Card card) throws Exception {
		return cardAlertRepository.findByBoardAndCard(card.getBoard(), card.getId());
	}
	
	public CardAlert getCardAlert(String alertId) throws Exception {
		return cardAlertRepository.findOne(alertId);
	}
	
	public boolean isAlerted(String boardId, String cardId) throws Exception {
		return cardAlertRepository.findByBoardAndCard(boardId, cardId).size()>0;
	}

	public boolean isAlerted(Card card) throws Exception {
		return cardAlertRepository.findByBoardAndCard(card.getBoard(), card.getId()).size()>0;
	}
	
	public CardAlert storeAlert(String detail, Card card, String level, String user, Date time,
			Map<String, CardEventField> fields) throws Exception {
		return this.storeAlert(detail, card.getBoard(),  card.getId(), level, user, time, fields);
	}
	
	public CardAlert storeAlert(String detail, String boardId, String cardId, String level, String user, Date time,
			Map<String, CardEventField> fields) throws Exception {

		CardAlert alert = new CardAlert();
		alert.setUser(user);
		alert.setOccuredTime(time);
		alert.setDetail(detail);
		alert.setLevel(level);
		alert.setFields(fields);
		alert.setBoard(boardId);
		alert.setCard(cardId);
		return storeAlert(alert);
	}
	
	public CardAlert storeAlert(CardAlert cardAlert) throws Exception {
		if (cardAlert.getOccuredTime() == null) {
			cardAlert.setOccuredTime(new Date());
		}

		CardAlert save = cardAlertRepository.save(cardAlert);
		return save;
	}

	public boolean dismissAlert(String alertId) throws Exception {
		CardAlert cardAlert = cardAlertRepository.findOne(alertId);
		if( cardAlert == null){
			throw new ResourceNotFoundException();	
		}
		this.deleteAlert(cardAlert);
		return this.isAlerted(cardAlert.getBoard(), cardAlert.getCard());
	}
	
	public void dismissBoardAlerts(String boardId) throws Exception {
		List<CardAlert> alerts = cardAlertRepository.findByBoard(boardId);
		for( CardAlert cardAlert : alerts) {
			this.deleteAlert(cardAlert);
		}
	}
	
	private void deleteAlert( CardAlert cardAlert ) throws Exception {
		logger.info("CardAlert: deleting cardAlert with id:" + cardAlert.getUuid());
		cardAlertRepository.delete(cardAlert);
		cardListener.addCardHolder(new CardHolder(cardAlert.getBoard(), cardAlert.getCard()));

		String detail = String.format("Moving Alert {} to History",cardAlert.getUuid());
		CardEvent event = 
				this.auditService.storeCardEvent( detail , cardAlert.getBoard(), cardAlert.getCard(), "info", "dismiss-alert", SecurityTool.getCurrentUser(), new Date(), null);
		
		if (event == null) {
			logger.warn("CardAlert: failed to save history record while deleting cardAlert with id:" + cardAlert.getUuid());
		}		
	}

}
