/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.BsonDocument;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Facet;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.UnwindOptions;

import nz.devcentre.gravity.automation.AutomationEngine;
import nz.devcentre.gravity.automation.CardListener;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.CardsLockCache;
import nz.devcentre.gravity.exceptions.CardValidationException;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.StorageManager;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.CardEventField;
import nz.devcentre.gravity.model.CardHolder;
import nz.devcentre.gravity.model.KeyMechanism;
import nz.devcentre.gravity.model.Phase;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.storage.StorageInstance;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.IdentifierTools;

@Service
public class CardService {

	private final static Log logger = LogFactory.getLog(CardService.class);
	
	private static String REASONSFIELD = "reasons";

	private static final String PHASEFIELD = "phase";
	
	@Autowired
	private IdentifierTools identifierTools;
	
	@Autowired
	private AuditService auditService;
			
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private CardListener cardListener;
	
	@Autowired
	@Lazy
	private AutomationEngine automationEngine;
	
	@Autowired
	private CardsLockCache cardsLockCache;
	
	@Autowired
	private StorageManager storageManager;
	
	@Autowired
	private SecurityTool securityTool;
	
	public Card getCard(String boardId, String cardId) throws Exception{
		StorageInstance storageInstance = storageManager.getStorageForBoard(boardId);
		return storageInstance.getPlugin().getCard(storageInstance,boardId, cardId);
	}
	
	public Card getCard(String boardId, String cardId, String view) throws Exception {
		return applyViewToCard(this.getCard(boardId, cardId), view);
	}
	
	public Card createCard( String boardId, Card card, boolean allfields) throws Exception {
		
		StorageInstance storageInstance = storageManager.getStorageForBoard(boardId);
		
		Board board = boardsCache.getItem(boardId);
		if (board == null) {
			throw new ResourceNotFoundException("Board Not Found");
		}
		
		Map<String,Object> fields = card.getFields();
		if( fields.containsKey("id")) {
			card.setId(CardTools.fieldToString(fields.get("id"),null));
			fields.remove("id");
		}
		if( fields.containsKey("_id")) {
			card.setId(CardTools.fieldToString(fields.get("id"),null));
			fields.remove("_id");
		}
		
		Object newPhase = card.getField("phase");
		if (newPhase != null) {
			card.getFields().remove("phase");
			card.setPhase(newPhase.toString());
		}
				
		if (StringUtils.isEmpty(card.getBoard())) {
			card.setBoard(boardId);
		}

		if (StringUtils.isEmpty(card.getPhase())) {
			card.setPhase(board.getDefaultPhase());
		}
		
		CardTools.correctCardFieldTypes(board, card.getFields());
		
		if (card.getId() == null) {
			KeyMechanism keyMechanism = board.getKeyMechanism();
			switch (keyMechanism) {
				case UUID:
					card.setId ( UUID.randomUUID().toString());
					break;
				case TITLE:
					String title = card.getField(board.getKeyField()).toString();
					String newId = IdentifierTools.getIdFromName(title);
					logger.info("Title from field: " + board.getKeyField() + " = " + title + " transforms to " + newId);
					card.setId(newId);
					break;
				case GENERATOR:
					card.setId(this.identifierTools.getNewId(boardId, "cardno"));
					break;
				case NONE:
					break;
				default:
					card.setId(this.identifierTools.getNewId(boardId, "cardno"));
					break;
			}
			logger.info("New Card ID: " + card.getId()+ " based on " + board.getKeyMechanism());
			
		}
		
		Map<String, Object> errors = automationEngine.validateCardFields(boardId, card);

		if (errors != null && errors.size() > 0  && board.getInvalidCardPhase()== null) {
			logger.warn("Submitted Card FAILED Validation: " + card.getId() + " - " + errors.toString());
			throw new CardValidationException(errors);
		}else if (errors != null && errors.size() > 0 && board.getInvalidCardPhase() != null){
			//set card's invalid phase to boardPhase. 
			card.setPhase(board.getInvalidCardPhase());
			card.getFields().put("broken_fields", errors.keySet());
		}
		
		if( card.getId()!=null){
			logger.info("Card ID Present, checking existence of " + card.getId());
			Card searchCard = getCard(boardId, card.getId());
			if (searchCard != null) {
				if (card.getPhase() != null) {
					fields.put("phase", card.getPhase());
				}
				card.setBoard(boardId);
				return updateCard(card, null, allfields);
			}
		}

		card.setCreated(new Date());
		card.setCreator(SecurityTool.getCurrentUser());
		
		storageInstance.getPlugin().createCard(storageInstance, boardId, card, allfields);
		
		auditService.storeCardEvent(null, card.getBoard(), card.getId(), "info", "create", SecurityTool.getCurrentUser(), new Date(), null);
		cardListener.addCardHolder(new CardHolder(card));
		logger.info("New Card - board:" + card.getBoard() + " ID: " + card.getId());

		return card;
		
	}
	
	public Card updateCard(Card card, String view, boolean allfields) throws Exception {

		StorageInstance storageInstance = storageManager.getStorageForBoard(card.getBoard());
		//return storageInstance.getPlugin().updateCard(storageInstance, card, view, allfields);

		
		Board board = boardsCache.getItem(card.getBoard());
		if (!securityTool.isAuthorizedViewAndFilter(view, null, board, "WRITE,ADMIN")) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}

		if (view != null && ! CardTools.validateParameters(board, view, card.getFields())) {
			throw new AccessDeniedException("Permissions Invalid for Update Parameters");
		}

		Card updatedCard = null;
		if (cardsLockCache.isLocked(card.getBoard(), card.getId())) {
			logger.info("Card is Locked : " + card.getBoard() + "/" + card.getId());
			throw new Exception("Card is Locked");
		}

		Map<String, Object> fields = card.getFields();
		
		Map<String, String> reasons = null;
		if (fields.containsKey(REASONSFIELD)) {
			reasons = (Map<String, String>) card.getField(REASONSFIELD);
			fields.remove(REASONSFIELD);
		}

		updatedCard = getCard(card.getBoard(), card.getId());
		if( updatedCard==null) {
			throw new ResourceNotFoundException();
		}

		String currentPhase = updatedCard.getPhase();// Capture the Current Phase

		Object newPhase = fields.get(PHASEFIELD);
		if (newPhase != null) {
			fields.remove(PHASEFIELD);
			updatedCard.setPhase((String) newPhase);
		}

		CardTools.correctCardFieldTypes(board, fields);
		Map<String, CardEventField> fieldChanges = CardTools.getCardFieldChanges(updatedCard.getFields(), fields);

		if (fieldChanges.isEmpty() && (newPhase == null)) {
			this.cardsLockCache.unlock(updatedCard.getBoard(), updatedCard.getId());
			this.cardsLockCache.applyLockState(updatedCard);
			return updatedCard;
		}

		updatedCard.getFields().putAll(fields);
		CardTools.correctCardFieldTypes(board, updatedCard.getFields());

		Map<String, Object> errors = automationEngine.validateCardUpdate(card.getBoard(), updatedCard);
		if (errors != null && errors.size() > 0 && board.getInvalidCardPhase()== null) {
			throw new CardValidationException(errors);
		}else if (errors != null && errors.size() > 0 && board.getInvalidCardPhase() != null){
			//set card's invalid phase to boardPhase. 
			updatedCard.setPhase(board.getInvalidCardPhase());
		}

		CardTools.updateModified(updatedCard);
		
		storageInstance.getPlugin().updateCard(storageInstance, updatedCard, view, allfields);
		
		if (reasons != null) {
			for (Entry<String, String> entry : reasons.entrySet()) {
				String fieldName = entry.getKey();

				if (fieldChanges.containsKey(fieldName)) {
					CardEventField toupdate = fieldChanges.get(fieldName);
					toupdate.setReason(entry.getValue());
				} else if (fieldName.equals(PHASEFIELD)) {// If Reason specified for Phase
					CardEventField toupdate = new CardEventField(PHASEFIELD, currentPhase, newPhase);
					toupdate.setReason(entry.getValue());
					fieldChanges.put(PHASEFIELD, toupdate);
				}
			}
		}

		this.cardsLockCache.unlock(updatedCard.getBoard(), updatedCard.getId());
		Card returnCard = getCard(updatedCard.getBoard(), updatedCard.getId());
		this.cardsLockCache.applyLockState(returnCard);
				
		cardListener.addCardHolder(new CardHolder(returnCard));
		return returnCard;
		
	}
	
	public Card moveCard(String boardId, String cardId, String newPhaseId, String reason) throws Exception {

		logger.info("Moving Card - card:" + boardId + "." + cardId + " -> " + newPhaseId);
		
		Card card = getCard(boardId, cardId);
		if (card == null) {
			throw new ResourceNotFoundException("Card not found, board: " + boardId + " card: " + cardId);
		}

		if (StringUtils.isEmpty(newPhaseId)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Move Card: Empty Phase - Card: " + cardId);
			}
			return card;
		}
		
		if (newPhaseId.equals(card.getPhase())) {
			if (logger.isDebugEnabled()) {
				logger.debug("Move Card redundant - Card " + cardId + " already in phase " + newPhaseId);
			}
			return card;
		}

		card.getFields().put("phase", newPhaseId);
		updateCard(card, null, false);
		
		cardsLockCache.applyLockState(card);
		cardListener.addCardHolder(new CardHolder(card));
		
		return card;
	}

	public Card applyViewToCard(Card card, String viewId) throws Exception {
		if (viewId == null || viewId.equals("all")) {
			return card;
		}

		Board board = boardsCache.getItem(card.getBoard());
		View view = board.getView(viewId);
		Map<String, Object> fieldMap = null;

		if (view != null) {
			Map<String, ViewField> viewfields = view.getFields();

			fieldMap = new HashMap<>();

			for (Entry<String, Object> cardFeildEntry : card.getFields().entrySet()) {
				if (viewfields.containsKey(cardFeildEntry.getKey())) {
					fieldMap.put(cardFeildEntry.getKey(), cardFeildEntry.getValue());
				}
			}

			card.setFields(fieldMap);
		}

		return card;
	}

	// Returns error Message used for ID validation
	public String referenceCheck(TemplateField fld, Object fieldValue) {

		if (fieldValue == null) {
			return null;
		}

		try {
			if (fieldValue instanceof List) {
				List<String> arrayl = (List<String>) fieldValue;
				for (String id : arrayl) {
					Card referenceCard = getCard(fld.getOptionlist(), id);
					if (referenceCard == null) {
						return ("Reference Error on Field List: " + fld.getName() + ": Id " + id
								+ " does not exist on board " + fld.getOptionlist());
					}
				}
			} else if (fieldValue instanceof String[]) {
				String[] stringArray = (String[]) fieldValue;
				for (int f = 0; f < stringArray.length; f++) {
					String id = stringArray[f];
					Card referenceCard = getCard(fld.getOptionlist(), id);
					if (referenceCard == null) {
						return ("Reference Error on Field Array: " + fld.getName() + ": Id " + id
								+ " does not exist on board " + fld.getOptionlist());
					}
				}
			} else if (fieldValue instanceof Object[]) {
				Object[] array = (Object[]) fieldValue;
				for (int f = 0; f < array.length; f++) {
					if (array[f] instanceof String) {
						String id = (String) array[f];
						Card referenceCard = getCard(fld.getOptionlist(), id);
						if (referenceCard == null) {
							return ("Reference Error on Field Array: " + fld.getName() + ": Id " + id
									+ " does not exist on board " + fld.getOptionlist());
						}
					}
				}
			} else if (fieldValue instanceof String && StringUtils.isNotEmpty((String) fieldValue)) {
				Card referenceCard = getCard(fld.getOptionlist(), (String) fieldValue);
				if (referenceCard == null) {
					return ("Reference Error on Field: " + fld.getName() + ": Id " + fieldValue
							+ " does not exist on board " + fld.getOptionlist());
				}
			}
		} catch (Exception e) {
			return ("Reference Error on field: " + fld.getName() + ": Id " + fieldValue + " does not exist on board "
					+ fld.getOptionlist());
		}

		return null;
	}
	
	public Map<String, Object> validateCardReferenceFields(String boardId, Card card, boolean removeFailed) {
		Map<String, Object> messages = new HashMap<String, Object>();
		try {
			Board board = boardsCache.getItem(boardId);
			Map<String, TemplateField> reference = board.getReferenceFields();
			for (Entry<String, TemplateField> entry : reference.entrySet()) {
				TemplateField fld = entry.getValue();
				if (card.getFields().containsKey(fld.getName())) {
					Object fieldValue = card.getFields().get(fld.getName());
					String checkResult = referenceCheck(fld, fieldValue);
					if (checkResult != null) {
						messages.put(fld.getName(), checkResult);
						if (removeFailed) {
							card.getFields().remove(fld.getName());
						}
					}

				}
			}
		} catch (Exception e) {
			logger.warn("Exception During Reference Validation:" + e.getMessage());
			messages.put("Validation", "Exception During Reference Validation:" + e.getMessage());
		}
		return messages;

	}
	
	public Map<String, Object> validateCardRequiredFields(String boardId, Card card) {
		Map<String, Object> messages = new HashMap<String, Object>();
		try {
			Board board = boardsCache.getItem(boardId);
			Map<String, TemplateField> required = board.getAllRequiredFields();
			for (Entry<String, TemplateField> entry : required.entrySet()) {
				TemplateField fld = entry.getValue();
				if (fld.isRequired() || isRequiredInPhase(board.getId(), fld.getRequiredPhase(), card.getPhase())) {
					if (card.getFields().containsKey(fld.getName())) {
						Object fieldValue = card.getFields().get(fld.getName());
						if (fieldValue == null || ("").equals(fieldValue.toString()))
							messages.put(fld.getName(), "Field is required. Cannot be blank or null: " + fld.getName());
					} else {
						messages.put(fld.getName(), "Field is required: " + fld.getName());
					}
				}
			}
		} catch (Exception e) {
			if (e.getCause() != null) {
				logger.warn("Exception During Required Fields Validation:" + e.getClass().getName() + " "
						+ e.getCause().getMessage() + " " + e.getMessage());
			} else {
				logger.warn(
						"Exception During Required Fields Validation:" + e.getClass().getName() + " " + e.getMessage());
			}
			messages.put("Validation", "Exception During Required Fields Validation:" + e.getMessage());
		}
		return messages;
	}
	
	private boolean isRequiredInPhase(String boardId, String requiredPhase, String currentPhase) throws Exception {
		if (requiredPhase == null || boardId==null) {
			return false;
		}
		Board board = this.boardsCache.getItem(boardId);
		
		Phase current = board.getPhases().get(currentPhase);
		if (current.isInvisible()) {
			return false;
		}
		Phase required = board.getPhases().get(requiredPhase);
		return required.getIndex() <= current.getIndex();
	}
	
	/*
	 * Validate Unique Fields
	 */
	public Map<String, Object> validateCardUniqueFields(String boardId, Map<String, Object> fieldValues,
			String cardId) {
		
		return new HashMap<>();
		
		/*
		StorageInstance storageInstance = storageManager.getStorageForBoard(boardId);
		
		logger.info("fieldValues:-" + fieldValues);
		Map<String, Object> messages = new HashMap<String, Object>();
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.putAll(fieldValues);
		// Exclude Current CardId from Query
		basicDBObject.put("_id", ImmutableMap.of("$ne", cardId));
		// Execute MongoDB Query to check Unique Entry
		if (this.mongoTemplate.getDb().getCollection(boardId).find(basicDBObject).count() > 0) {
			logger.error(fieldValues + " Not Unique");
			messages.put("Validation", fieldValues + " Combination Already Exists");
		}
		return messages;
		*/
		
	}
	
	//Return Unique Values for a Field In a Board
	public Set<String> fetchUniqueFieldValues(String boardId, String fieldId) {
		/*
		Set<String> result=new HashSet<>();
		DBObject groupFields = new BasicDBObject("_id", "$" + fieldId);
		DBObject group = new BasicDBObject("$group", groupFields);
		Iterable<DBObject> output = this.mongoTemplate.getDb().getCollection(boardId)
				.aggregate(group, new BasicDBObject("$sort", new BasicDBObject("_id", 1))).results();
		output.forEach(dbObject->{
			if(dbObject!=null && dbObject.get("_id")!=null && !StringUtils.isEmpty(dbObject.get("_id").toString()))
				result.add(dbObject.get("_id").toString());
		});
		return result;
		*/
		return new HashSet<>();

	}
	
	//Build the Facet Query
	public String buildFacetQuery(long skip, long pageSize, boolean ignorePage) {
		List<Bson> data = new ArrayList<>();
		Aggregation.sort(new Sort(Sort.Direction.DESC, "_id")).toDBObject(Aggregation.DEFAULT_CONTEXT);
		Bson sortBson=BsonDocument.parse(Aggregation.sort(new Sort(Sort.Direction.DESC, "_id")).toDBObject(Aggregation.DEFAULT_CONTEXT).toString());
		data.add(sortBson);
		// Ignore Pagination
		if (!ignorePage) {
			data.add(BsonDocument.parse(Aggregation.skip(Math.toIntExact(skip)).toDBObject(Aggregation.DEFAULT_CONTEXT).toString()));
			data.add(BsonDocument.parse(Aggregation.limit(pageSize).toDBObject(Aggregation.DEFAULT_CONTEXT).toString()));
		}
		
		BsonDocument facetQuery = Aggregates
				.facet(Arrays.asList(new Facet("data", data),
						new Facet("pageInfo",
								Aggregates.count("totalRecords").toBsonDocument(BsonDocument.class,
										MongoClient.getDefaultCodecRegistry()))))
				.toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry());
		return facetQuery.toJson();

	}
	
	/**
	 * Utility Method to order cards in the collection
	 */
	public List<Card> orderCards(Collection<Card> result, String order, boolean descending, String view, Board board) {
		View viewObject = board.getView(view);
		List<Card> results = CardTools.orderCards(result,order,descending,viewObject,board);
		return results;
	}
	
	public Map<String, Map<String, Boolean>> explain( String boardId, String cardId) throws Exception {
		return this.automationEngine.explain(boardId, cardId);
	}
	
	/**
	 * Number of cards for Phase of a Board
	 */
	public Long getCardCountByPhase(String boardId, String phaseId) {
		/*
		BasicDBObject basicDBObject = new BasicDBObject();
		basicDBObject.put("metadata.phase", phaseId);
		return this.mongoTemplate.getDb().getCollection(boardId).getCount(basicDBObject);
		*/
		return 0l;
	}	

	/**
	 * Return Distinct Values for a Field In a Board
	 */
	public List<?> fetchDistinctFieldValues(String boardId, String fieldId) {
		return null;
		//return this.mongoTemplate.getDb().getCollection(boardId).distinct(fieldId);
	}

	private String buildLookUpQuery(String boardId,String localField,String alias) {
		LookupOperation operation = LookupOperation.newLookup().from(boardId).localField(localField).foreignField("_id")
				.as(alias);
		String lookupString = operation.toDBObject(Aggregation.DEFAULT_CONTEXT).toString();
		return lookupString;
	}
	
	private String buildUnWindString(String unWindObject) {
		
		return Aggregates.unwind("$" + unWindObject, new UnwindOptions().preserveNullAndEmptyArrays(true)).toString();
	}
	
	private String buildAddFieldString(String fieldName,String fieldValue) {
		BsonDocument addField = Aggregates
				.addFields(new Field<>(fieldName,fieldValue))
				.toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry());
		return addField.toJson();
	}
	
	private String buildProjectionString(List<String> projectionStringList) throws JsonProcessingException {
		
		Map<String,String> projectionMap=new HashMap<>();
		for (String projectionString : projectionStringList) {
			projectionMap.put(projectionString, "$".concat(projectionString));
		}
		ObjectMapper mapper=new ObjectMapper();
		String jsonString=mapper.writeValueAsString(projectionMap);
		Bson projection= BsonDocument.parse(jsonString);
		return Aggregates.project(projection).toBsonDocument(BsonDocument.class, MongoClient.getDefaultCodecRegistry()).toJson();
		
	}
	
	public static Map<String, String> resolveStringValues(Map<String, String> fields, Map<String, Object> context) {

		Map<String, Object> resolveValues = resolveValues(fields, context);

		return resolveValues.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, e -> (String) e.getValue()));

	}

	public static Map<String,Object> resolveValues( Map<String,String> fields, Map<String,Object> context){
		Map<String,Object> result = new HashMap<>();
		for( Entry<String,String> entry : fields.entrySet()) {
			String reference = entry.getValue();
			String targetField = entry.getKey();
			Object value = null;
			if(reference.startsWith("#")){
				String fieldName = reference.substring(1);
				value = getValueByReference(fieldName, context);
			} else {
				value = parseField(reference, context, null);
			}
			result.put(targetField, value);
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static Object getValueByReference( String reference, Map<String, Object> data) {
		String[] references = StringUtils.split(reference,"."); 
		List<String> referenceList = Arrays.asList(references);
		Map<String, Object> thing = data;
		for( String item : referenceList) {
			Object itemObject = thing.get(item);
			if( itemObject instanceof Map) {
				thing = (Map<String, Object>) itemObject;
			} else {
				return itemObject;
			}
		}
		return thing;
	}
	
	public static Object parseField( String configured, Map<String,Object> properties, Map<String,Object> parentProperties ){
		String result = configured;
		int found = 0;
		int last = 0;
		while(found>-1){
			found = result.indexOf("[");
			if(found>-1){
				last = result.indexOf("]");
				String field = result.substring(found+1, last);
				if(StringUtils.isNotEmpty(field)) {
					Object valueObj = getValueByReference(field,properties);
					if( valueObj == null && parentProperties != null) {
						valueObj = getValueByReference(field,parentProperties);				
					}
					if( valueObj!=null) {
						String value = valueObj.toString();
						result = result.substring(0, found) + value + result.substring(last+1);					
					} else {
						return "";
					}
				}
			}
		}
		return result;
	}
	
	public String getTitle( Card card ) throws Exception {
		Board board = boardsCache.getItem(card.getBoard());
		Object value = card.getField(board.getTitleField());
		return value.toString();
	}
	
}
