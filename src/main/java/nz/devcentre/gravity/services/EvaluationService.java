/**	
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.VariableInterpreter;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.DateInterpreter;

@Service
public class EvaluationService {
	
	private static final Logger logger = LoggerFactory.getLogger(EvaluationService.class);
	
	@Autowired
	private VariableInterpreter variableInterpreter;
	
	@Autowired
	private DateInterpreter dateInterpreter;

	private boolean eval(Object cardValue, Condition condition, List<Object> conditionValues,
			Map<String, Object> fields) {

		final Operation operation = condition.getOperation();

		if (cardValue instanceof String) {
			final String stringValue = (String) cardValue;

			switch (operation) {
			case NOTNULL:
				return !StringUtils.isEmpty(stringValue);
			case ISNULL:
				return StringUtils.isEmpty(stringValue);
			}

			for (Object conditionValue : conditionValues) {
				boolean result = false;

				switch (operation) {
				case EQUALTO:
					result = stringValue.equals(conditionValue);
					break;
				case CONTAINS:
					result = stringValue.contains((String) conditionValue);
					break;
				case NOTEQUALTO:
					result = !stringValue.equals(conditionValue);
					break;
				}
				if (result == true)
					return true;
			}
			return false;

		} else if (cardValue instanceof Integer || cardValue instanceof Double) {

			final double cardValueNumber = CardTools.getNumberFromObject(cardValue);

			switch (operation) {
			case NOTNULL:
				return true;
			case ISNULL:
				return false;
			}

			for (Object value : conditionValues) {
				final double conditionNumber = CardTools.getNumberFromObject(value);
				boolean result = false;

				switch (operation) {
				case EQUALTO:
					result = new Double(cardValueNumber).equals(new Double(conditionNumber));
					break;
				case NOTEQUALTO:
					result = !(new Double(cardValueNumber).equals(new Double(conditionNumber)));
					break;
				case GREATERTHAN:
					result = cardValueNumber > conditionNumber;
					break;
				case GREATERTHANOREQUALTO:
					result = cardValueNumber >= conditionNumber;
					break;
				case LESSTHAN:
					result = cardValueNumber < conditionNumber;
					break;
				case LESSTHANOREQUALTO:
					result = cardValueNumber <= conditionNumber;
					break;
				}

				if (result)
					return true;
			}
			return false;

		} else if (cardValue instanceof Boolean) {

			boolean cardBoolean = CardTools.getBooleanFromObject(cardValue);

			switch (operation) {
			case NOTNULL:
				return true;
			case ISNULL:
				return false;
			}

			for (Object value : conditionValues) {

				final boolean conditionBoolean = CardTools.getBooleanFromObject(value);
				boolean result = false;

				switch (operation) {
				case EQUALTO:
					result = (cardBoolean == conditionBoolean);
					break;
				case NOTEQUALTO:
					result = !(cardBoolean == conditionBoolean);
					break;
				}

				if (result)
					return true;
			}
			return false;

		} else if (cardValue instanceof Date) {

			switch (operation) {
			case NOTNULL:
				return true;
			case ISNULL:
				return false;
			}

			Date cardDate = (Date) cardValue;

			for (Object value : conditionValues) {
				boolean result = false;

				final Date conditionDate = getConditionDate(fields, value.toString());

				if (null != conditionDate) {
					int compareDates = cardDate.compareTo(conditionDate);
					switch (operation) {
					case EQUALTO:
						result = compareDates == 0;
						break;
					case NOTEQUALTO:
						result = !(compareDates == 0);
						break;
					case GREATERTHAN:
						result = compareDates > 0;
						break;
					case GREATERTHANOREQUALTO:
						result = compareDates >= 0;
						break;
					case LESSTHAN:
						result = compareDates < 0;
						break;
					case LESSTHANOREQUALTO:
						result = compareDates <= 0;
						break;
					case BEFORE:
						result = compareDates < 0;
						break;
					case AFTER:
						result = compareDates > 0;
						break;
					}
				}
				if (result)
					return true;
			}
			return false;
		} else {
			if (cardValue != null) {
				logger.warn("Condition Data Type not Recognized: " + condition.getFieldName() + " type "
						+ cardValue.getClass().getName());				
				switch (operation) {
				case NOTNULL:
					return true;
				case ISNULL:
					return false;
				}
			} else {
				logger.warn("Null Data for field: " + condition.getFieldName());		
				switch (operation) {
				case NOTNULL:
					return false;
				case ISNULL:
					return true;
				case NOTEQUALTO:
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean evalField(Object cardValue, Condition condition) {
		final Map<String, Object> fields = new HashMap<String, Object>();
		final List<Object> conditionValues = 
				variableInterpreter.resolveValues(fields, condition.getValue());
		return eval(cardValue, condition, conditionValues, fields);
	}
	
	public boolean evalProperty(Map<String,Object> fields, Condition condition) {
		Object cardValue = fields.get(condition.getFieldName());
		final List<Object> conditionValues = 
				variableInterpreter.resolveValues(fields, condition.getValue());
		return eval(cardValue, condition, conditionValues, fields);
	}
	
	/**
	 * @param card
	 * @param condition
	 * @param conditionDate
	 * @return
	 */
	private Date getConditionDate(Map<String, Object> fields, String value) {
		Date conditionDate = null;
		if (variableInterpreter.isVariableExpression(value)) {
			// backwards compatibility in case date is stored as string when comparing dates
			// and to prevent
			// classcast exception
			Object conditionValue = variableInterpreter.resolve(fields, value);
			if (conditionValue instanceof String) {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				try {
					conditionDate = df.parse(conditionValue.toString());
				} catch (ParseException e) {
					logger.warn("Condition Value Invalid - " + value);
				}
			} else if (conditionValue instanceof Date) {
				conditionDate = (Date) conditionValue;
			}
		} else if (dateInterpreter.isDateFormula(value)) {
			conditionDate = dateInterpreter.interpretDateFormula(value);
		} else {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				conditionDate = df.parse(value);
			} catch (ParseException e) {
				logger.warn("Condition Value Invalid - " + value);
			}
		}

		return conditionDate;
	}
}
