/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.TeamCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Team;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.repository.BoardRepository;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.tools.IdentifierTools;


@Component
public class BoardService {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardService.class);
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private TeamCache teamCache;
	
	@Autowired
	private BoardRepository boardRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private CacheInvalidationInterface cacheInvalidationManager;
	
	public synchronized Board saveBoard(Board board) throws Exception {
		
		// Disactivate all for this board
		disactivateBoard(board.getId());
				
		// Save New Board Config
		IdentifierTools.newVersion(board);
		board.setActive(true);
		Board save = this.boardRepository.save(board);
		this.cacheInvalidationManager.invalidate(BoardsCache.TYPE, board.getId());
		return save;
	}
	
	public void disactivateBoard( String boardId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(("active")).is(true));
		query.addCriteria(Criteria.where(("id")).is(boardId));
		Update update = new Update();
		update.set("active", false);	
		this.mongoTemplate.updateMulti(query, update, "gravity_boards");
		this.cacheInvalidationManager.invalidate(BoardsCache.TYPE, boardId);
	}
	
	public void checkIndexesForBoard(String boardId) throws Exception{
		Board board = boardsCache.getItem(boardId);
		if(board==null) return;
				
		if(!mongoTemplate.collectionExists(boardId)) return;
		
		List<String> indexes = new ArrayList<String>();
		indexes.add("metadata.phase");
		
		Map<String, TemplateField> fields = board.getFields();
		for( TemplateField field : fields.values()){
			if( field.isIndexed()){
				indexes.add(field.getName());
			}
		}
		
		logger.info("Checking Indexes on board: " + boardId + " indexes: " + indexes.toString());
		DBCollection collection = mongoTemplate.getCollection(boardId);
		
		List<DBObject> indexInfo = collection.getIndexInfo();
		List<String> existingIndexes = new ArrayList<String>();
		for( DBObject dbObj : indexInfo) {
			BasicDBObject keyObj = (BasicDBObject) dbObj.get("key");
			Set<Entry<String,Object>> entrySet = keyObj.entrySet();
			for( Entry<String,Object> entry :entrySet) {
				existingIndexes.add(entry.getKey());
				logger.info("value: " + entry.getKey());
			}	
		}
		
		for( String index : indexes){
			if(!existingIndexes.contains(index)) {
				logger.info("Creating Indexes on board: " + boardId + " field: " + index);
				collection.createIndex(index);
			} else {
				logger.info("Indexes exists on board: " + boardId + " field: " + index);
			}
		}
	}
	
	/**
	 * 
	 * @param board
	 * @return
	 * Used by Template,Phase controllers as every time there is a change in Template or Phase New version of Board will be created
	 */
	public Board disableCurrentActiveAndCreateNewVersion(Board board) {
		
		String boardId=board.getId();
		Board currentActive=this.boardRepository.findByIdAndActiveTrue(boardId);
		
		/*
		 * currentActive will be NULL if there is no active version for Board exist in Gravity
		 */
		if(currentActive!=null ) {
			currentActive.setActive(false);
			currentActive.addOrUpdate();
			this.boardRepository.save(currentActive);
		}
		board.setVersion(null);
		IdentifierTools.newVersion(board);
		board.setActive(true);
		return board;
	}


	/**
	 * Validate user access 
	 * @param configId
	 * @param mode
	 * @param filter
	 * @param view
	 * @throws Exception
	 */
	public void validateAccess(String configId, String mode, String filter, String view) throws Exception {
		Board board = this.boardsCache.getItem(configId);
		if (!securityTool.isAuthorizedViewAndFilter(view, filter, board, "READ,WRITE,ADMIN")) {
			throw new AccessDeniedException("View and Filter Permissions Invalid");
		}
	}
	
	/**
	 * map<String boardId, String description>
	 */
	public void filterMap(Map<String,String> map, String types) throws Exception{
		Iterator<String> it = map.keySet().iterator();
		while(it.hasNext()){
			String boardId = it.next();
			Map<String, String> permissions = this.boardsCache.getItem(boardId).getPermissions();
			if(!this.securityTool.isAuthorised(permissions, types)){
				it.remove();
			}  
		}
	}
	
	public boolean isUserAccess(String boardId,String permission) throws Exception {
		if (permission == null) {
			permission = "READ,WRITE,ADMIN";
		}
		Board board = this.boardsCache.getItem(boardId);
		Map<String, String> permissions = board.getPermissions();
		return this.securityTool.isAuthorised(permissions, permission);
	}
	
	public List<Map<String, Object>> getBoardRefrences(String boardId) throws Exception {
		
		List<Map<String, Object>> result = new ArrayList<>();
		Map<String, String> list = this.boardsCache.list();
		
		for(String bId : list.keySet()) {
			Board board = this.boardsCache.getItem(bId);
			List<TemplateField> tfs = board.getReferenceFields().values().stream()
					.filter(tfIeld -> tfIeld.getOptionlist().equals(boardId)).collect(Collectors.toList());
			
			if (tfs != null && !tfs.isEmpty()) {
				result.add(ImmutableMap.of("referringBoard", boardId, "templateField", tfs));
			}	
		}
		return result;
	}
	
	public @ResponseBody Map<String, User> getBoardUsers(@PathVariable String boardId,
			@RequestParam(required = false) String permission) throws Exception {

		logger.info("Getting Users for Board: " + boardId);

		Board board = this.boardsCache.getItem(boardId);
		Map<String, String> permissions = board.getPermissions();

		Map<String, User> userMap = new HashMap<String, User>();
		for (String id : permissions.keySet()) {

			String rolePermission = permissions.get(id);
			if (permission != null && !rolePermission.equals(permission)) {
				logger.info("Skipping User: " + id + " assigned permission is " + rolePermission);
				continue;
			}

			logger.info("Getting Users in Team: " + id);
			try {
				Team team = teamCache.getItem(id);
				logger.info("Getting Users in Team: " + id + ":" + team.getMembers().size());
				Set<String> keys = team.getMembers().keySet();
				for (String userid : keys) {
					try {
						User user = this.securityTool.getUser(userid);
						userMap.put(userid, user);
					} catch (ResourceNotFoundException e) {
						logger.info("Team " + id + " member " + userid + " -- User Not Found " + userid);
					}
				}
			} catch (ResourceNotFoundException e) {
				try {
					User user = this.securityTool.getUser(id);
					userMap.put(id, user);
				} catch (ResourceNotFoundException ignore) {
				}
			} catch (Exception e) {
				logger.trace("Some exception", e);
			}
		}
		return userMap;
	}	
}
