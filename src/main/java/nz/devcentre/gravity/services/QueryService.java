/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.managers.StorageManager;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.Orderable;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.model.transfer.LookupDetails;
import nz.devcentre.gravity.model.transfer.QueryRequest;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.storage.StorageInstance;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.FilterTools;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@Service
public class QueryService {

	private static final Logger logger = LoggerFactory.getLogger(QueryService.class);

	@Autowired
	private BoardService boardService;
	
	@Autowired
	private BoardsCache boardsCache;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private StorageManager storageManager;

	/* - not required ?
	public void ensureCollection(String collectionName) {
		if (!mongoTemplate.getDb().collectionExists(collectionName)) {
			BasicDBObject o = new BasicDBObject();
			mongoTemplate.getDb().createCollection(collectionName, o);
		}
	}
	*/
	
	public PaginatedCards query( QueryRequest query) throws Exception {
		StorageInstance storageInstance = storageManager.getStorageForBoard(query.getBoardId());
		return storageInstance.getPlugin().query(storageInstance, query);
	}



	/**
	 * Query for cards.
	 * 
	 * TODO - Remove Phase from interfaces
	 * 
	 * @param boardId
	 * @param phaseId
	 * @param filterId
	 * @param allPhases
	 * @return
	 * @throws Exception
	 */
	public Collection<Card> query(String boardId, String phaseId, String filter, String view, boolean allPhases)
			throws Exception {

		Board board = boardsCache.getItem(boardId);
		Filter filterObj = board.getFilter(filter);
		View viewObj = board.getView(view);
		return dynamicQuery(boardId, phaseId, filterObj, viewObj, allPhases);
	}

	/**
	 * Query for cards.
	 * 
	 * TODO - Remove Phase from interfaces
	 * 
	 * @param boardId
	 * @param phaseId
	 * @param filter
	 * @param view
	 * @param allPhases
	 * @return
	 * @throws Exception
	 */
	public Collection<Card> dynamicQuery(String boardId, String phaseId, Filter filter, View view, boolean allPhases)
			throws Exception {

		Collection<Card> cards = mongoQuery(boardId, phaseId, filter, view, allPhases);
		long startTime=System.currentTimeMillis();
		logger.info("Time Taken to Apply View:-"+(System.currentTimeMillis()-startTime));
		return cards;

	}

	/*
	public Map<String, String> basicQuery(String boardId, String phaseId, Filter filter, String property,
			boolean allPhases) throws Exception {

		Collection<Card> cards = mongoQuery(boardId, phaseId, filter, null, allPhases);
		Map<String, String> list = new HashMap<String, String>();

		for (Card card : cards) {
			if ("_id".equals(property))
				list.put(card.getId(), card.getId());
			else
				list.put(card.getId(), card.getField(property).toString());
		}

		return list;
	}
	*/

	public static Condition buildPhaseCondition(Board board, String phaseId, boolean allPhases, Filter filter)
			throws Exception {
		List<String> excludedPhases = null;
		String includedPhases = null;

		if (filter != null) {
			excludedPhases = filter.getExcludedPhases();
			includedPhases = filter.getPhase();
		}

		List<String> visiblePhasesForBoard = null;

		if (StringUtils.isNotEmpty(includedPhases)) {
			visiblePhasesForBoard = MongoQueryBuilderUtility.getVisiblePhasesForBoard(board, includedPhases, excludedPhases, allPhases);
		} else {
			visiblePhasesForBoard = MongoQueryBuilderUtility.getVisiblePhasesForBoard(board, phaseId, excludedPhases, allPhases);
		}

		StringBuffer phaseIdSb = new StringBuffer();
		boolean first = true;

		if (!allPhases) {
			for (String pId : visiblePhasesForBoard) {
				if (!first) {
					phaseIdSb.append('|');
				}

				phaseIdSb.append(pId);
				first = false;
			}
		}

		Condition phaseCondition = null;
		if (!StringUtils.isEmpty(phaseIdSb.toString())) {
			phaseCondition = new Condition("metadata.phase",Operation.EQUALTO,phaseIdSb.toString());
		} else if (StringUtils.isNotEmpty(phaseId)) {
			phaseCondition = new Condition("metadata.phase",Operation.EQUALTO,phaseId);
		}

		return phaseCondition;
	}

	public static List<Condition> buildTeamConditions(Board board)
			throws Exception {
		
		Authentication currentAuthentication = SecurityTool.getCurrentAuthentication();
		Collection<? extends GrantedAuthority> authorities = currentAuthentication.getAuthorities();
		
		StringBuilder teams = new StringBuilder();
		boolean first = true;
		for( GrantedAuthority ga : authorities) {
			if(!first) {
				teams.append("|");
			}
			int i = ga.getAuthority().indexOf(".");
			if(i>=0) {
				teams.append(ga.getAuthority().substring(0, i));
			} else {
				teams.append(ga.getAuthority());
			}
			first = false;
		}
		
		List<Condition> conditions = new ArrayList<Condition>(); 
		Map<String, TemplateField> teamConstraints = board.getTeamConstraints();
		
		for( TemplateField field : teamConstraints.values()) {
			conditions.add(new Condition(field.getName(),Operation.IN,teams.toString()));
		}
		
		return conditions;
	}

	private Collection<Card> mongoQuery(String boardId, String phaseId, Filter filter, View view, boolean allPhases)
			throws Exception {

		if (boardId == null) {
			throw new ResourceNotFoundException("Invalid or null Board");
		}

		long startTime = System.currentTimeMillis();
		Board board = boardsCache.getItem(boardId);
		Condition phaseCondition = buildPhaseCondition(board, phaseId, allPhases, filter);
		
		List<Condition> teamConditions = buildTeamConditions(board);

		String mongoQueryStr = null;
		Map<String, String> replaces = new HashMap<String, String>();
		Map<String, String> fieldNameMapping = new HashMap<String, String>();

		List<DBObject> pipeline = new ArrayList<DBObject>();

		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);

		if (filter != null  && filter.getConditions()!=null) {
			
			// Determine Required Lookups.
			for (Condition condition : filter.getConditions().values()) {
				if (condition.getConditionType() != null && ConditionType.DYNAMIC.equals(condition.getConditionType()))
					continue;
				if (condition.getFieldName().contains(".") && !condition.getFieldName().startsWith("metadata.")) {
					String[] split = condition.getFieldName().split("\\.");
					String localField = split[0];
					String subField = split[1];

					// Don't need to process the same lookup twice, so skip if already done.
					if (replaces.containsKey(localField))
						continue;

					TemplateField tf = board.getField(localField);
					if (tf != null && tf.getOptionlist() != null) {
						StringBuilder sb = new StringBuilder();
						sb.append("{ $lookup: { from:\"");
						sb.append(tf.getOptionlist());
						sb.append("\", localField:\"");
						sb.append(localField);
						sb.append("\", foreignField:\"_id\", as:\"");
						sb.append(localField);
						sb.append("\"}}");

						BasicDBObject lookup = BasicDBObject.parse(sb.toString());
						pipeline.add(lookup);

						sb = new StringBuilder();

						// Include Null Values
						sb.append("{$unwind:{");
						sb.append("\"path\":\"$");
						sb.append(localField);
						sb.append("\"");
						sb.append(",\"preserveNullAndEmptyArrays\": true}}");

						BasicDBObject unwind = BasicDBObject.parse(sb.toString());
						pipeline.add(unwind);
						// Don't replace the parent object name with child object name
						if (!"name".equals(subField)) { 
							replaces.put(subField, condition.getFieldName());
						}
						replaces.put(localField, localField + "._id");
						
					} else {

						// Otherwise it is a key in another collection
						// localField = the collection
						// subField = the field to get
						// "_id"

						// Get template of target.
						Board targetBoard = boardsCache.getItem(localField);
						tf = targetBoard.getFieldByOptionList(boardId);

						if (tf != null && tf.getOptionlist() != null) {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(tf.getName());
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);
						} else {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(subField);
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);

							sb = new StringBuilder();
							sb.append("{ \"$addFields\": { \"");
							sb.append(localField);
							sb.append("_size\" :{ $size: \"$");
							sb.append(localField);
							sb.append("\"}}}");
							String addFieldsStr = sb.toString();
							logger.info(addFieldsStr);
							BasicDBObject addFields = BasicDBObject.parse(addFieldsStr);
							pipeline.add(addFields);
							fieldNameMapping.put(condition.getFieldName(), split[2]);
						}
					}
				}
			}

			List<Condition> conditions = new ArrayList<Condition>();
			conditions.addAll(filter.getConditions().values());
			if (phaseCondition != null) {
				conditions.add(phaseCondition);
			}
			conditions.addAll(teamConditions);
			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, fieldNameMapping);
		} else {
			List<Condition> conditions = new ArrayList<Condition>();
			if (phaseCondition != null) {
				conditions.add(phaseCondition);
			}
			conditions.addAll(teamConditions);
			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, null);
		}

		BasicDBObject match = new BasicDBObject();
		BasicDBObject q = BasicDBObject.parse(mongoQueryStr);

		match.append("$match", q);
		pipeline.add(match);

		boolean first = true;
		if (replaces.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append("{ \"$addFields\" : { ");

			first = true;
			for (Entry<String, String> item : replaces.entrySet()) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				sb.append("\"");
				sb.append(item.getKey());
				sb.append("\" : \"$");
				sb.append(item.getValue());
				sb.append("\"");
			}
			sb.append("}}");
			BasicDBObject addFields = BasicDBObject.parse(sb.toString());
			pipeline.add(addFields);
		}

		Map<String, List<LookupDetails>> joins = null;
		if (view != null) {
			joins = retrieveJoinDetails(boardId, view);
			if (joins != null && joins.size() > 0) {
				Set<String> processed = new HashSet<String>();
				for(Map.Entry<String, List<LookupDetails>> entry:joins.entrySet()) {
					MongoQueryBuilderUtility.addLookup(entry.getValue(), processed, pipeline, entry.getKey());
				}
			}
		}
				
		long queryBuiltTime = System.currentTimeMillis();
		logger.info("Board:-"+boardId);
		logger.info("Query:" + pipeline.toString());
		
		
		AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();
		Cursor cursor = collection.aggregate(pipeline, options);
		Collection<Card> cards = new ArrayList<Card>();
		List<DBObject> dbResults=new ArrayList<>();
		while (cursor.hasNext()) {
			dbResults.add(cursor.next());
		}
		cursor.close();
		logger.info("Board:-"+boardId);
		logger.info("Query Performance - MongoDB Result Cursor Loop Time: " + (queryBuiltTime - System.currentTimeMillis()));
		
		for(DBObject next:dbResults) {
			Card card = CardTools.mongoDBObjectToCard(next, boardId);
			if (!card.isDeleted()) {
				if (joins != null && !joins.isEmpty()) {
					populateCardForReferenceFields(card, next, joins);
				}
			}
			cards.add(card);
		}
		long completeTime = System.currentTimeMillis();
		logger.info("Board:-"+boardId);
		logger.info("Query Performance - query build time: " + (queryBuiltTime - startTime) + "   query run time: "
				+ (completeTime - queryBuiltTime));

		return cards;
	}

	private void populateCardForReferenceFields(Card card, DBObject item, Map<String, List<LookupDetails>> joins) {
		Map<String, Object> refFieldValues = getReferenceFieldValues(item, joins);
		card.getFields().putAll(refFieldValues);
	}

	private static Map<String, Object> getReferenceFieldValues(DBObject item, Map<String, List<LookupDetails>> joins) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (String field : joins.keySet()) {

			String[] elements = field.split("\\.");
			Object value = null;
			int i = 0;
			for (String element : elements) {
				if (i == 0) {
					value = item.get(element);
					
				}	
				else {
					try {
						value = ((DBObject) value).get(element);
						//logger.info("Value:-"+value.toString());
					} catch (NullPointerException e) {
						value = null;
					} catch (Exception e) {
						//logger.info("Inside Exception:"+element);
					
					}
				}
				i++;
			}
			result.put(field, value);
		}
		return result;
	}

	// Retrieve join details for all the reference fields used in a view
	private Map<String, List<LookupDetails>> retrieveJoinDetails(String boardId, View view) throws Exception {
		
		Map<String, ViewField> fields = view.getFields();
		Map<String, ViewField> referenceFields = new HashMap<String, ViewField>();
		Map<String, List<LookupDetails>> joins = new HashMap<String, List<LookupDetails>>();

		// Check if there are any reference fields in the view
		for (ViewField field : fields.values()) {
			String fieldName = field.getName();
			if (fieldName != null && fieldName.contains(".")) {
				referenceFields.put(fieldName, field);
			}
		}

		// If no reference fields return
		if (referenceFields.size() == 0)
			return null;

		Board currentboard = null;

		// For each reference field in the view, iterate over all . elements
		for (String key : referenceFields.keySet()) {

			currentboard = boardsCache.getItem(boardId);
			ViewField vf = referenceFields.get(key);
			String fieldName = vf.getName();
			String[] fieldElements = fieldName.split("\\.");

			for (int i = 0; i < (fieldElements.length - 1); i++) {
				String element = fieldElements[i];
				// resolve the element for it's board and card
				TemplateField elementTf = currentboard.getField(element);
				if (elementTf == null) {
					logger.error("Template field definition not found for " + element);
					throw new ResourceNotFoundException("Template Field not found: " + element);
				}
				
				String elementBoard = elementTf.getOptionlist();
				LookupDetails ld = new LookupDetails(element, elementBoard, elementTf.getType());
				List<LookupDetails> list = joins.get(fieldName);
				if (list == null) {
					list = new ArrayList<LookupDetails>();
					joins.put(fieldName, list);
				}
				list.add(ld);
				currentboard = boardsCache.getItem(elementBoard);
			}
		}

		return joins;
	}


	public Date decodeShortDate(String date) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.parse(date);
	}

	public Map<String, String> search(String boardId, String field, Operation operation, String value, String property,
			boolean allcards) throws Exception {
		if (boardId == null) {
			throw new ResourceNotFoundException("Invalid or null Board");
		}

		Map<String, Condition> conditionsMap = new HashMap<>();
		conditionsMap.put("search", new Condition(field, operation, value));

		Filter filter = new Filter();
		filter.setConditions(conditionsMap);

		Collection<Card> cards = mongoQuery(boardId, null, filter, null, allcards);

		Map<String, String> list = new HashMap<String, String>();
		for (Card card : cards) {
			Object cardValue = card.getField(property);
			if (cardValue != null) {
				list.put(card.getId(), cardValue.toString());
			} else if (property.equals("metadata.phase")) {
				list.put(card.getId(), card.getPhase());
			}
		}
		return list;
	}

	/**
	 * @param         <T>
	 * @param actions
	 * @return
	 */
	public static <T extends Orderable> List<T> order(Collection<T> origin) {
		List<T> list = new ArrayList<T>();
		if (null != origin) {
			for (T orderable : origin) {
				list.add(orderable);
			}
			Collections.sort(list, new Comparator<Orderable>() {
				@Override
				public int compare(Orderable o1, Orderable o2) {
					return o1.getOrder() - o2.getOrder();
				}
			});
		}
		return list;
	}

	public String getDatasource(String boardId, String fieldName) throws Exception {

		if( "phase".equals(fieldName)) {
			return null;
		}
		
		Board board = boardsCache.getItem(boardId);
		if (board == null) {
			return null;
		}
		
		String[] fieldElements = fieldName.split("\\.");		

		Board currentBoard = board;
		
		try {
			for (int f = 0; f < (fieldElements.length-1); f++) {
				String currentField = fieldElements[f];
				TemplateField tf = currentBoard.getField(currentField);
				if(tf!=null) {
					currentBoard = boardsCache.getItem(tf.getOptionlist());
				} else {
					logger.warn("Requested board not found : " + currentBoard);
					return null;
				}
			}
			if (logger.isDebugEnabled()) {
				logger.info("Returning : " + currentBoard);
			}
			return currentBoard.getId();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.warn("Exception Finding Datasource: " + boardId + " " + fieldName, e);
			}
			return null;
		}
	}

	public PaginatedCards query(String boardId, String phaseId, String filter, String view, boolean allPhases,
			long pageSize,long skip,boolean ignorePage) throws Exception {

		Board board = boardsCache.getItem(boardId);
		Filter filterObj = board.getFilter(filter);
		View viewObj = board.getView(view);
		return dynamicQuery(boardId, phaseId, filterObj, viewObj, allPhases, pageSize,skip,ignorePage);
	}

	public PaginatedCards dynamicQuery(String boardId, String phaseId, 
			Filter filter, View view, boolean allPhases,
			long pageSize, long skip, boolean ignorePage) throws Exception {

		return mongoQueryPaginated(boardId, phaseId, filter, view, allPhases,pageSize,skip,ignorePage);
	}
	
	@SuppressWarnings("unchecked")
	private PaginatedCards mongoQueryPaginated(String boardId, String phaseId, 
			Filter filter, View view, boolean allPhases, long pageSize, long skip, 
			boolean ignorePage) throws Exception {
		
		PaginatedCards result=new PaginatedCards();
		if (boardId == null) {
			throw new ResourceNotFoundException("Invalid or null Board");
		}

		long startTime = System.currentTimeMillis();
		Board board = boardsCache.getItem(boardId);
		Condition phaseCondition = buildPhaseCondition(board, phaseId, allPhases, filter);

		String mongoQueryStr = null;
		Map<String, String> replaces = new HashMap<String, String>();
		Map<String, String> fieldNameMapping = new HashMap<String, String>();

		List<DBObject> pipeline = new ArrayList<DBObject>();

		DBCollection collection = mongoTemplate.getDb().getCollection(boardId);

		if (filter != null) {
			//logger.info("***********Inside Filter************");
			// Determine Required Lookups.
			for (Condition condition : filter.getConditions().values()) {
				if (condition.getConditionType() != null && ConditionType.DYNAMIC.equals(condition.getConditionType()))
					continue;
				if (condition.getFieldName().contains(".") && !condition.getFieldName().startsWith("metadata.")) {
					String[] split = condition.getFieldName().split("\\.");

					// $lookup: { from:"capabilities", localField:"capability", foreignField:"_id",
					// as:"capability" } }

					String localField = split[0];
					String subField = split[1];

					// Don't need to process the same lookup twice, so skip if already done.
					if (replaces.containsKey(localField))
						continue;

					TemplateField tf = board.getField(localField);
					if (tf != null && tf.getOptionlist() != null) {
						StringBuilder sb = new StringBuilder();
						sb.append("{ $lookup: { from:\"");
						sb.append(tf.getOptionlist());
						sb.append("\", localField:\"");
						sb.append(localField);
						sb.append("\", foreignField:\"_id\", as:\"");
						sb.append(localField);
						sb.append("\"}}");

						BasicDBObject lookup = BasicDBObject.parse(sb.toString());
						pipeline.add(lookup);

						sb = new StringBuilder();
						/*
						 * sb.append("{ $unwind:\"$"); sb.append( localField ); sb.append("\" }");
						 */
						// Include Null Values
						sb.append("{$unwind:{");
						sb.append("\"path\":\"$");
						sb.append(localField);
						sb.append("\"");
						sb.append(",\"preserveNullAndEmptyArrays\": true}}");
						//logger.info("Unwind:-" + sb.toString());
						BasicDBObject unwind = BasicDBObject.parse(sb.toString());
						if (!tf.getType().equals(FieldType.LIST)) {// No Unwind for LIST Type
							pipeline.add(unwind);
						}
						if (!"name".equals(subField)) // Don't replace the parent object name with child object name
							replaces.put(subField, condition.getFieldName());
						replaces.put(localField, localField + "._id");
					} else {

						// Otherwise it is a key in another collection
						// localField = the collection
						// subField = the field to get
						// "_id"

						// Get template of target.
						Board targetBoard = boardsCache.getItem(localField);
						tf = targetBoard.getFieldByOptionList(boardId);

						if (tf != null && tf.getOptionlist() != null) {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(tf.getName());
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);
						} else {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(subField);
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);

							// { "$addFields": { "offer_gtm_analyses_size": { $size: "$offer_gtm_analyses" }
							// } }
							// { $addFields: { "offer_gtm_analyses_size" :{ " $size:
							// "$offer_gtm_analyses"}}}

							sb = new StringBuilder();
							sb.append("{ \"$addFields\": { \"");
							sb.append(localField);
							sb.append("_size\" :{ $size: \"$");
							sb.append(localField);
							sb.append("\"}}}");
							String addFieldsStr = sb.toString();
							logger.info(addFieldsStr);
							BasicDBObject addFields = BasicDBObject.parse(addFieldsStr);
							pipeline.add(addFields);
							fieldNameMapping.put(condition.getFieldName(), split[2]);
						}
					}
				}
			}

			List<Condition> conditions = new ArrayList<Condition>();
			conditions.addAll(filter.getConditions().values());
			if (phaseCondition != null) {
				conditions.add(phaseCondition);
			}
			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, fieldNameMapping);
		} else {
			//logger.info("***********Non Filter******************");
			List<Condition> conditions = new ArrayList<Condition>();
			if (phaseCondition != null) {
				conditions.add(phaseCondition);
			}

			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, null);
		}

		BasicDBObject match = new BasicDBObject();
		BasicDBObject q = BasicDBObject.parse(mongoQueryStr);

		match.append("$match", q);
		pipeline.add(match);

		boolean first = true;
		if (replaces.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append("{ \"$addFields\" : { ");

			first = true;
			for (Entry<String, String> item : replaces.entrySet()) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				sb.append("\"");
				sb.append(item.getKey());
				sb.append("\" : \"$");
				sb.append(item.getValue());
				sb.append("\"");
			}
			sb.append("}}");
			BasicDBObject addFields = BasicDBObject.parse(sb.toString());
			pipeline.add(addFields);
		}

		Map<String, List<LookupDetails>> joins = null;
		if (view != null) {
			joins = retrieveJoinDetails(boardId, view);
			/*logger.info("Join Keys:-"+joins.keySet().toString());
			logger.info("Join Keys:-"+joins.toString());*/
			if (joins != null && joins.size() > 0) {
				Set<String> processed = new HashSet<String>();
				for(Map.Entry<String, List<LookupDetails>> entry:joins.entrySet()) {
				/*	logger.info("Key:-" + entry.getKey());
					logger.info("Lookup List:-" + entry.getValue().toString());*/
					MongoQueryBuilderUtility.addLookup(entry.getValue(), processed, pipeline, entry.getKey());
				}
			}
		}

		long queryBuiltTime = System.currentTimeMillis();
				
		try {
			// Get the Facet Query
			BasicDBObject facetObject = BasicDBObject
					.parse(this.cardService.buildFacetQuery(skip, pageSize, ignorePage));
			pipeline.add(facetObject);
		} catch (Exception e) {
			StringBuilder facet = new StringBuilder();
			facet.append("{'$facet':{'data':[{'$sort':{'_id':1}}");
			// Ignore Pagination
			if (!ignorePage) {
				facet.append(",{'$skip':");
				facet.append(skip);
				facet.append("},{'$limit':");
				facet.append(pageSize);
				facet.append("}");
			}
			facet.append("],'pageInfo':[{'$count':'totalRecords'}]}}");
			BasicDBObject facetObject = BasicDBObject.parse(facet.toString());
			pipeline.add(facetObject);
		}
		logger.info("Query:" + pipeline.toString());
		
		AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();
		Cursor cursor = collection.aggregate(pipeline, options);
		
		Collection<Card> cards = new ArrayList<Card>();
		while (cursor.hasNext()) {
			DBObject next = cursor.next();
			List<DBObject> data=(List<DBObject>) next.get("data");
			List<Map<String,Object>> pageInfo=(List<Map<String, Object>>) next.get("pageInfo");
			for(DBObject cardObj:data) {
				Card card = CardTools.mongoDBObjectToCard(cardObj, boardId);
				if (!card.isDeleted()) {
					//if(!card.getFields().containsKey("gravityCardTitle")) {
					//	cardTools.setCardFriendlyTitle(boardId, card.getTemplate(), card);
					//}	
					if (joins != null && !joins.isEmpty()) {
						populateCardForReferenceFields(card, cardObj, joins);
					}
					cards.add(card);
				}
			}
			
			result.setData(cards);
			result.setPageInfo(pageInfo);
		}
	
		
		long completeTime = System.currentTimeMillis();

		logger.info("Query Performance - query build time: " + (queryBuiltTime - startTime) + "   query run time: "
				+ (completeTime - queryBuiltTime));

		return result;
	}

	/**
	 * 
	 * @param boardId
	 * @param filter
	 * @param view
	 * @param filterId
	 * @param order
	 * @param allphases
	 * @param descending
	 * @return
	 * @throws Exception
	 */
	public Collection<Card> search(String boardId, Filter filter, String view, String filterId, String order,
			boolean allphases, boolean descending) throws Exception {
		
		Board board = this.boardsCache.getItem(boardId);
		this.boardService.validateAccess(boardId, null, filterId, view);

		Filter storedFilter = board.getFilter(filterId);
		String cardId = resolveDynamicFilter(boardId, filter, storedFilter);

		if (cardId != null) {
			Card card = cardService.getCard(boardId, cardId);
			Collection<Card> returnCollection = new ArrayList<>();
			returnCollection.add(card);
			return returnCollection;
		}
		FilterTools.applyStoredFilter(filter, storedFilter);
		
		Collection<Card> result = null;
		View viewObj = board.getView(view);
		
		result = this.dynamicQuery(boardId, null, filter, viewObj, allphases);
		/*
		 * Return empty collection if no data found
		 */
		if(result==null ||result.isEmpty()) {
			return new ArrayList<>();
		}
				
		//Sort the Cards
		this.cardService.orderCards(result, order, descending, view, board);
		
		return result;
	}
	/**
	 * Return a collection of cards
	 * @param configId
	 * @param phaseId
	 * @param filter
	 * @param view
	 * @param order
	 * @param allfields
	 * @param allcards
	 * @param descending
	 * @return
	 * @throws Exception 
	 */
	public Collection<Card> fetchCardsApplyTemplateAndOrder(String boardId,String phaseId,String filter,String view,String order,boolean allfields,boolean allcards,boolean descending) throws Exception{
		
		Board board = this.boardsCache.getItem(boardId);
		
		Collection<Card> result = null;
		result = this.query(boardId, phaseId, filter, view, allcards);
		
		//Order Cards
		this.cardService.orderCards(result, order, descending, view, board);
		
		return result;
	}
	
	/**
	 * 
	 * @param boardId
	 * @param dynamicfilter
	 * @return cardId if a condition specifies one.
	 * @throws Exception
	 */
	public String resolveDynamicFilter(String boardId, Filter dynamicfilter, Filter storedFilter) throws Exception{
		
		if(dynamicfilter != null && dynamicfilter.getConditions() != null ){
			Iterator<String> i = dynamicfilter.getConditions().keySet().iterator();
			
			while(i.hasNext()){
				String next = i.next();
				Condition condition = dynamicfilter.getConditions().get(next);
				if(condition.getFieldName().startsWith("board=")){
					String list = condition.getFieldName().substring(6);
					
					//Filter applied on the same board
					if(boardId.equals(list)){
						String cardId = condition.getValue();
						i.remove();
						return cardId;
					}
					
					//Get the optionlist that could be the field to be filtered on
					Board listBoard = this.boardsCache.getItem(boardId);
					TemplateField field = listBoard.getFieldByOptionList(list);
					
					if(field!=null){
						condition.setFieldName(field.getId());
					} else if(storedFilter != null){
						//Check if the stored Filter has the path to inheritance
						Iterator<String> it = storedFilter.getConditions().keySet().iterator(); 
						while(it.hasNext()){
							String stnext = it.next();
							Condition stcondition = storedFilter.getConditions().get(stnext);
							if(stcondition.getConditionType() != null && ConditionType.DYNAMIC.equals(stcondition.getConditionType())) {
								if(stcondition.getValue() != null && stcondition.getValue().contains(condition.getFieldName())) {
									condition.setFieldName(stcondition.getFieldName());
									break;
								}
							}
						}
					}else {
						logger.warn("Field Not Found for Board " + list);
						i.remove();
					}
				}
			}
			return null;
		} else {
			return null;
		}
	}
} 