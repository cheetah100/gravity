/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongodb.AggregationOptions;
import com.mongodb.BasicDBObject;
import com.mongodb.Cursor;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.model.transfer.LookupDetails;
import nz.devcentre.gravity.model.transfer.QueryRequest;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.MongoQueryBuilderUtility;

@Component("mongo_storage")
public class MongoStorage implements StoragePlugin {
	
	private final static Log logger = LogFactory.getLog(MongoStorage.class);
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private CardService cardService;

	@Override
	public Card getCard(StorageInstance instance, String boardId, String cardId) throws Exception {
		DBCollection collection = instance.getMongoTemplate().getDb().getCollection(boardId);
		BasicDBObject q = BasicDBObject.parse("{ _id: \"" + cardId + "\"}");
		DBObject doc = collection.findOne(q);
		if(doc == null) {
			logger.warn("Fall back to ObjectID");
			try {
				q = BasicDBObject.parse("{ _id: ObjectId(\"" + cardId + "\")}");
				doc = collection.findOne(q);
			} catch( Exception e ) {
				logger.warn( "Invalid ID: " + cardId);
				doc = null;
			}
		}
		
		Card card = null;
		if (doc != null) {
			card = CardTools.mongoDBObjectToCard(doc, boardId);
			if (card != null && card.isDeleted()) {
				return null;
			}
		}
		return card;
	}
	
	@Override
	public void createCard(StorageInstance instance, String boardId, Card card, boolean allfields) throws Exception {
		DBObject o = CardTools.cardToMongoDBObject(card);
		DBCollection collection = instance.getMongoTemplate().getCollection(boardId);
		collection.insert(o);
	}
	
	@Override
	public void updateCard(StorageInstance instance, Card card, String view, boolean allfields) throws Exception {
		DBObject update = CardTools.cardToMongoDBObject(card);
		DBObject query = new BasicDBObject();
		query.put("_id", card.getId());
		DBCollection collection = instance.getMongoTemplate().getCollection(card.getBoard());
		collection.update(query, update);
	}

	@Override
	public PaginatedCards query( StorageInstance instance, QueryRequest query) throws Exception {

		PaginatedCards result=new PaginatedCards();
		
		if (query == null) {
			throw new ResourceNotFoundException("Invalid Query");
		}

		long startTime = System.currentTimeMillis();
		Board board = boardsCache.getItem(query.getBoardId());
		//Condition phaseCondition = buildPhaseCondition(board, null, true, query.getConditions());

		String mongoQueryStr = null;
		Map<String, String> replaces = new HashMap<String, String>();
		Map<String, String> fieldNameMapping = new HashMap<String, String>();

		List<DBObject> pipeline = new ArrayList<DBObject>();

		DBCollection collection = instance.getMongoTemplate().getDb().getCollection(query.getBoardId());

		if (query.getConditions() != null) {
			//logger.info("***********Inside Filter************");
			// Determine Required Lookups.
			for (Condition condition : query.getConditions()) {
				if (condition.getConditionType() != null && ConditionType.DYNAMIC.equals(condition.getConditionType()))
					continue;
				if (condition.getFieldName().contains(".") && !condition.getFieldName().startsWith("metadata.")) {
					String[] split = condition.getFieldName().split("\\.");

					// $lookup: { from:"capabilities", localField:"capability", foreignField:"_id",
					// as:"capability" } }

					String localField = split[0];
					String subField = split[1];

					// Don't need to process the same lookup twice, so skip if already done.
					if (replaces.containsKey(localField))
						continue;

					TemplateField tf = board.getField(localField);
					if (tf != null && tf.getOptionlist() != null) {
						StringBuilder sb = new StringBuilder();
						sb.append("{ $lookup: { from:\"");
						sb.append(tf.getOptionlist());
						sb.append("\", localField:\"");
						sb.append(localField);
						sb.append("\", foreignField:\"_id\", as:\"");
						sb.append(localField);
						sb.append("\"}}");

						BasicDBObject lookup = BasicDBObject.parse(sb.toString());
						pipeline.add(lookup);

						sb = new StringBuilder();
						/*
						 * sb.append("{ $unwind:\"$"); sb.append( localField ); sb.append("\" }");
						 */
						// Include Null Values
						sb.append("{$unwind:{");
						sb.append("\"path\":\"$");
						sb.append(localField);
						sb.append("\"");
						sb.append(",\"preserveNullAndEmptyArrays\": true}}");
						//logger.info("Unwind:-" + sb.toString());
						BasicDBObject unwind = BasicDBObject.parse(sb.toString());
						if (!tf.getType().equals(FieldType.LIST)) {// No Unwind for LIST Type
							pipeline.add(unwind);
						}
						if (!"name".equals(subField)) // Don't replace the parent object name with child object name
							replaces.put(subField, condition.getFieldName());
						replaces.put(localField, localField + "._id");
					} else {

						// Otherwise it is a key in another collection
						// localField = the collection
						// subField = the field to get
						// "_id"

						// Get template of target.
						Board targetBoard = boardsCache.getItem(localField);
						tf = targetBoard.getFieldByOptionList(query.getBoardId());

						if (tf != null && tf.getOptionlist() != null) {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(tf.getName());
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);
						} else {
							StringBuilder sb = new StringBuilder();
							sb.append("{ $lookup: { from:\"");
							sb.append(localField);
							sb.append("\", localField:\"");
							sb.append("_id");
							sb.append("\", foreignField:\"");
							sb.append(subField);
							sb.append("\", as:\"");
							sb.append(localField);
							sb.append("\"}}");
							BasicDBObject lookup = BasicDBObject.parse(sb.toString());
							pipeline.add(lookup);

							// { "$addFields": { "offer_gtm_analyses_size": { $size: "$offer_gtm_analyses" }
							// } }
							// { $addFields: { "offer_gtm_analyses_size" :{ " $size:
							// "$offer_gtm_analyses"}}}

							sb = new StringBuilder();
							sb.append("{ \"$addFields\": { \"");
							sb.append(localField);
							sb.append("_size\" :{ $size: \"$");
							sb.append(localField);
							sb.append("\"}}}");
							String addFieldsStr = sb.toString();
							logger.info(addFieldsStr);
							BasicDBObject addFields = BasicDBObject.parse(addFieldsStr);
							pipeline.add(addFields);
							fieldNameMapping.put(condition.getFieldName(), split[2]);
						}
					}
				}
			}

			List<Condition> conditions = new ArrayList<Condition>();
			conditions.addAll(query.getConditions());
			//if (phaseCondition != null) {
			//	conditions.add(phaseCondition);
			//}
			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, fieldNameMapping);
		} else {
			//logger.info("***********Non Filter******************");
			List<Condition> conditions = new ArrayList<Condition>();
			//if (phaseCondition != null) {
			//	conditions.add(phaseCondition);
			//}

			mongoQueryStr = MongoQueryBuilderUtility.mongoQueryFromConditions(conditions, null);
		}

		BasicDBObject match = new BasicDBObject();
		BasicDBObject q = BasicDBObject.parse(mongoQueryStr);

		match.append("$match", q);
		pipeline.add(match);

		boolean first = true;
		if (replaces.size() > 0) {

			StringBuilder sb = new StringBuilder();
			sb.append("{ \"$addFields\" : { ");

			first = true;
			for (Entry<String, String> item : replaces.entrySet()) {
				if (first) {
					first = false;
				} else {
					sb.append(", ");
				}
				sb.append("\"");
				sb.append(item.getKey());
				sb.append("\" : \"$");
				sb.append(item.getValue());
				sb.append("\"");
			}
			sb.append("}}");
			BasicDBObject addFields = BasicDBObject.parse(sb.toString());
			pipeline.add(addFields);
		}

		/*
		Map<String, List<LookupDetails>> joins = null;
		if (query.getViewId() != null) {
			joins = retrieveJoinDetails(query.getBoardId(), query.getFields());
			if (joins != null && joins.size() > 0) {
				Set<String> processed = new HashSet<String>();
				for(Map.Entry<String, List<LookupDetails>> entry:joins.entrySet()) {
					MongoQueryBuilderUtility.addLookup(entry.getValue(), processed, pipeline, entry.getKey());
				}
			}
		}
		*/

		long queryBuiltTime = System.currentTimeMillis();
		
		try {
			// Get the Facet Query
			BasicDBObject facetObject = BasicDBObject
					.parse(cardService.buildFacetQuery(query.getSkip(), query.getPageSize(), false));
			
			pipeline.add(facetObject);
		} catch (Exception e) {
			StringBuilder facet = new StringBuilder();
			facet.append("{'$facet':{'data':[{'$sort':{'_id':1}}");
			// Ignore Pagination
			//if (!query.ignorePage) {
				facet.append(",{'$skip':");
				facet.append(query.getSkip());
				facet.append("},{'$limit':");
				facet.append(query.getPageSize());
				facet.append("}");
			//}
			facet.append("],'pageInfo':[{'$count':'totalRecords'}]}}");
			BasicDBObject facetObject = BasicDBObject.parse(facet.toString());
			pipeline.add(facetObject);
		}
		
		if( logger.isDebugEnabled()) {
			logger.debug("Query:" + pipeline.toString());
		}
		
		AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();
		Cursor cursor = collection.aggregate(pipeline, options);
		
		Collection<Card> cards = new ArrayList<Card>();
		while (cursor.hasNext()) {
			DBObject next = cursor.next();
			List<DBObject> data=(List<DBObject>) next.get("data");
			List<Map<String,Object>> pageInfo=(List<Map<String, Object>>) next.get("pageInfo");
			for(DBObject cardObj:data) {
				Card card = CardTools.mongoDBObjectToCard(cardObj, query.getBoardId());
				if (!card.isDeleted()) {
					//if(!card.getFields().containsKey("gravityCardTitle")) {
					//	cardTools.setCardFriendlyTitle(boardId, card.getTemplate(), card);
					//}	
					//if (joins != null && !joins.isEmpty()) {
					//	populateCardForReferenceFields(card, cardObj, joins);
					//}
					cards.add(card);
				}
			}
			
			result.setData(cards);
			result.setPageInfo(pageInfo);
		}
	
		
		long completeTime = System.currentTimeMillis();

		logger.info("Query Performance - query build time: " + (queryBuiltTime - startTime) + "   query run time: "
				+ (completeTime - queryBuiltTime));

		return result;
	}
	
	private Map<String, List<LookupDetails>> retrieveJoinDetails(String boardId, Map<String, ViewField> fields) throws Exception {
		
		//Map<String, ViewField> fields = view.getFields();
		Map<String, ViewField> referenceFields = new HashMap<String, ViewField>();
		Map<String, List<LookupDetails>> joins = new HashMap<String, List<LookupDetails>>();

		// Check if there are any reference fields in the view
		for (ViewField field : fields.values()) {
			String fieldName = field.getName();
			if (fieldName != null && fieldName.contains(".")) {
				referenceFields.put(fieldName, field);
			}
		}

		// If no reference fields return
		if (referenceFields.size() == 0)
			return null;

		Board currentboard = null;

		// For each reference field in the view, iterate over all . elements
		for (String key : referenceFields.keySet()) {

			currentboard = boardsCache.getItem(boardId);
			ViewField vf = referenceFields.get(key);
			String fieldName = vf.getName();
			String[] fieldElements = fieldName.split("\\.");

			for (int i = 0; i < (fieldElements.length - 1); i++) {
				String element = fieldElements[i];
				// resolve the element for it's board and card
				TemplateField elementTf = currentboard.getField(element);
				if (elementTf == null) {
					logger.error("Template field definition not found for " + element);
					throw new ResourceNotFoundException("Template Field not found: " + element);
				}
				
				String elementBoard = elementTf.getOptionlist();
				LookupDetails ld = new LookupDetails(element, elementBoard, elementTf.getType());
				List<LookupDetails> list = joins.get(fieldName);
				if (list == null) {
					list = new ArrayList<LookupDetails>();
					joins.put(fieldName, list);
				}
				list.add(ld);
				currentboard = boardsCache.getItem(elementBoard);
			}
		}

		return joins;
	}
	
	public static Condition buildPhaseCondition(Board board, String phaseId, boolean allPhases, Filter filter)
			throws Exception {
		List<String> excludedPhases = null;
		String includedPhases = null;

		if (filter != null) {
			excludedPhases = filter.getExcludedPhases();
			includedPhases = filter.getPhase();
		}

		List<String> visiblePhasesForBoard = null;

		if (StringUtils.isNotEmpty(includedPhases)) {
			visiblePhasesForBoard = MongoQueryBuilderUtility.getVisiblePhasesForBoard(board, includedPhases, excludedPhases, allPhases);
		} else {
			visiblePhasesForBoard = MongoQueryBuilderUtility.getVisiblePhasesForBoard(board, phaseId, excludedPhases, allPhases);
		}

		StringBuffer phaseIdSb = new StringBuffer();
		boolean first = true;

		if (!allPhases) {
			for (String pId : visiblePhasesForBoard) {
				if (!first) {
					phaseIdSb.append('|');
				}

				phaseIdSb.append(pId);
				first = false;
			}
		}

		Condition phaseCondition = null;
		if (!StringUtils.isEmpty(phaseIdSb.toString())) {
			phaseCondition = new Condition("metadata.phase",Operation.EQUALTO,phaseIdSb.toString());
		} else if (StringUtils.isNotEmpty(phaseId)) {
			phaseCondition = new Condition("metadata.phase",Operation.EQUALTO,phaseId);
		}

		return phaseCondition;
	}
}
