/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.storage;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 
 * @author peter
 *
 */
public class StorageInstance {
	
	final private StoragePlugin plugin;
	
	final private MongoTemplate mongoTemplate;
	
	final private JdbcTemplate JdbcTemplate;
	
	public StorageInstance( StoragePlugin plugin, MongoTemplate mongoTemplate ) {
		super();
		this.plugin = plugin;
		this.mongoTemplate = mongoTemplate;
		this.JdbcTemplate = null;
	}
	
	public StorageInstance( StoragePlugin plugin, JdbcTemplate jdbcTemplate ) {
		super();
		this.plugin = plugin;
		this.mongoTemplate = null;
		this.JdbcTemplate = jdbcTemplate;
	}
	
	public StoragePlugin getPlugin() {
		return plugin;
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return JdbcTemplate;
	}
}
