/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.storage;

import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.transfer.QueryRequest;

/**
 * Common Storage Interface
 * Implementations for various versions of SQL, Mongo, Cassandra etc.
 * 
 * @author peter
 *
 */
public interface StoragePlugin {
	
	/**
	 * Get a single record from the storage.
	 */
	public Card getCard( StorageInstance instance, String boardId, String cardId) throws Exception;
	
	/**
	 * Create a single record in the storage.
	 */
	public void createCard( StorageInstance instance,  String boardId, Card card, boolean allfields) throws Exception;
	
	/**
	 * Update a record in storage.
	 */
	public void updateCard( StorageInstance instance,  Card card, String view, boolean allfields) throws Exception;
		
	/**
	 * Primary Query Mechanism
	 * 
	 * @param boardId
	 * @param filter
	 * @param view
	 * @param pageSize
	 * @param skip
	 * @param ignorePage
	 * @return
	 * @throws Exception
	 */
	public PaginatedCards query( StorageInstance instance, QueryRequest query) throws Exception;

}
