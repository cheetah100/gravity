/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.storage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.PaginatedCards;
import nz.devcentre.gravity.model.View;
import nz.devcentre.gravity.model.ViewField;
import nz.devcentre.gravity.model.transfer.QueryRequest;

/**
 * This is the base class for all SQL based Storage.
 * It contains generic SQL Logic.
 * Decendent classes implement specific flavours of SQL.
 * 
 * @author peter
 *
 */
public abstract class SqlStorage implements StoragePlugin {
	
	private final static Log logger = LogFactory.getLog(SqlStorage.class);
	
	protected abstract String getOneQuery();
	
	protected abstract String insertQuery();
	
	protected abstract String selectQuery();
	
	protected abstract String whereClause();
	
	@Autowired
	private BoardsCache boardsCache;

	@Override
	public Card getCard(StorageInstance instance, String boardId, String cardId) throws Exception {
		
		Board board = boardsCache.getItem(boardId);
		
		String query =  getOneQuery()
				.replace("\\$\\{table\\}",board.getTableName())
				.replace("\\$\\{field\\}",board.getKeyField())
				.replace("\\$\\{value\\}",cardId);
		
		Card card = instance.getJdbcTemplate().query(query, new ResultSetExtractor<Card>() {
			@Override
			public Card extractData(ResultSet rs) throws SQLException {
				Card card = new Card();
				rs.next();
				Map<String,Object> row = new HashMap<>();
				int fieldCount = rs.getMetaData().getColumnCount();				
				if( fieldCount>0) {
					for(int n=1; n<fieldCount+1; n++) {
						String fieldName = rs.getMetaData().getColumnName(n);
						Object fieldValue = rs.getObject(n);
						if( fieldValue!=null) {
							row.put(fieldName, fieldValue);
						}
					}
				} else {
					logger.info("No Fields in ResultSet");
				}
				card.setFields(row);
				card.setBoard(boardId);
				
				if(board.getKeyField()!=null) {
					card.setId((row.get(board.getKeyField())).toString());
				}
				if(board.getPhaseField()!=null) {
					card.setPhase((row.get(board.getPhaseField())).toString());
				}
				return card;
			}
		});
		
		return card;
	}

	@Override
	public void createCard(StorageInstance instance, String boardId, Card card, boolean allfields) throws Exception {
	
		// INSERT INTO example (name,email) values ('jeff','jeff@test.com')
		Board board = boardsCache.getItem(boardId);
		
		StringBuilder fields = new StringBuilder();
		StringBuilder values = new StringBuilder();
		
		Map<String,Object> newMap = new HashMap<>( card.getFields());
		if(card.getPhase()!=null) {
			newMap.put(board.getPhaseField(), card.getPhase());
		}
		
		for( Entry<String,Object> item: card.getFields().entrySet()) {
			fields.append(item.getKey());
			fields.append(", ");
			values.append("\"");
			values.append(item.getValue().toString());
			values.append("\", ");
		}

		String fieldsStr = fields.substring(0, fields.length()-2);
		String valuesStr = values.substring(0, values.length()-2);
				
		//String query = String.format(insertQuery(), board.getTableName(), fieldsStr, valuesStr);
		
		String query = insertQuery()
				.replace("\\$\\{table\\}",board.getTableName())
				.replace("\\$\\{fields\\}", fieldsStr)
				.replace("\\$\\{value\\}", valuesStr);
		
		logger.info("Insert Query: " + query);
		
		instance.getJdbcTemplate().execute(query);
		
	}

	@Override
	public void updateCard(StorageInstance instance, Card card, String view, boolean allfields) throws Exception {

	}
	
	@Override
	public PaginatedCards query( StorageInstance instance, QueryRequest query) throws Exception {

		// select [fields] from [table] where [conditions]
		
		// Get Board
		Board board = boardsCache.getItem(query.getBoardId());
		
		List<Condition> conditions = new ArrayList<Condition>(); 
		if( query.getFilterId()!=null) {
			Filter filter = board.getFilter(query.getFilterId());
			conditions.addAll( filter.getConditions().values() );
		}
		
		if( query.getConditions()!=null && query.getConditions().size()>0) {
			conditions.addAll(query.getConditions());
		}
		
		// create fields string.
		// If fields are provided, otherwise *
		StringBuilder fieldBuilder = new StringBuilder();
		if( query.getViewId()!=null) {
			// Get View.
			View view = board.getView(query.getViewId());
			for( Entry<String,ViewField> item : view.getFields().entrySet()) {
				fieldBuilder.append(item.getValue().getName());
				fieldBuilder.append(", ");
			}
		} else if( query.getFields()!=null) {
			for( Entry<String,String> item : query.getFields().entrySet()) {
				fieldBuilder.append(item.getValue());
				fieldBuilder.append(" as ");
				fieldBuilder.append(item.getKey());
				fieldBuilder.append(", ");
			}
		} else {
			fieldBuilder.append("*  ");
		}
		
		String fieldStr = "";
		if( fieldBuilder.length()>2) {
			fieldStr = fieldBuilder.substring(0, fieldBuilder.length()-2);
		}
		
		
		String conditionStr = "";
		if( conditions.size()>0 ) {
		// create conditions string
			StringBuilder conditionBuilder = new StringBuilder();
		
			for( Condition condition : conditions) {
				conditionBuilder.append("(");
				String expression = condition.getOperation().getSqlExpression();
				String rf = expression.replaceAll("\\$\\{field\\}", condition.getFieldName());
				String rv = rf.replaceAll("\\$\\{value\\}", condition.getValue());
				conditionBuilder.append(rv);
				conditionBuilder.append(") and ");
			}
		
			if( conditionBuilder.length()>5) {
				//conditionStr = String.format( whereClause(), conditionBuilder.substring(0, conditionBuilder.length()-5));
				conditionStr = whereClause().replaceAll("\\$\\{condition\\}", conditionBuilder.substring(0, conditionBuilder.length()-5));
			}
		}
					
		//String sql = String.format(selectQuery(), fieldStr, board.getTableName(), conditionStr, query.getPageSize() );

		String sql = selectQuery()
				.replaceAll("\\$\\{fields\\}", fieldStr)
				.replaceAll("\\$\\{table\\}", board.getTableName())
				.replaceAll("\\$\\{conditions\\}", conditionStr)
				.replaceAll("\\$\\{limit\\}", Integer.toString(query.getPageSize()));

		
		logger.info(sql);
		
		
		List<Card> cards = new ArrayList<>();
		
		instance.getJdbcTemplate().query(sql, new RowCallbackHandler() {
			
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Card card = new Card();
				Map<String,Object> row = new HashMap<>();
				int fieldCount = rs.getMetaData().getColumnCount();				
				if( fieldCount>0) {
					for(int n=1; n<fieldCount+1; n++) {
						String fieldName = rs.getMetaData().getColumnName(n);
						Object fieldValue = rs.getObject(n);
						if( fieldValue!=null) {
							row.put(fieldName, fieldValue);
						}
					}
				} else {
					logger.info("No Fields in ResultSet");
				}
				card.setFields(row);
				card.setBoard(query.getBoardId());
			
				if(board.getKeyField()!=null) {
					card.setId((row.get(board.getKeyField())).toString());
				}
				if(board.getPhaseField()!=null) {
					card.setPhase((row.get(board.getPhaseField())).toString());
				}
				cards.add(card);
			}
		});
		
		PaginatedCards result = new PaginatedCards();
		result.setData(cards);
		return result;
	}

}
