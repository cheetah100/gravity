package nz.devcentre.gravity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages={"nz.devcentre.gravity"},exclude={DataSourceAutoConfiguration.class})
@EnableScheduling
public class Startup {
	
	public static void main(String[] args) {
		 SpringApplication.run(Startup.class, args);
	}

}