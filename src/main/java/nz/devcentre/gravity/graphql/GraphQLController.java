/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.graphql;

import static graphql.Scalars.GraphQLBoolean;
import static graphql.Scalars.GraphQLFloat;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLList.list;
import static graphql.schema.GraphQLNonNull.nonNull;
import static graphql.schema.GraphQLObjectType.newObject;
import static graphql.schema.GraphQLSchema.newSchema;
//import static nz.devcentre.gravity.graphql.CustomScalars.GraphQLDate;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLObjectType.Builder;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.controllers.FilterController;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.ConditionType;
import nz.devcentre.gravity.model.FieldType;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.TemplateField;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.services.QueryService;

@RestController
@RequestMapping("/graphql")
public class GraphQLController {
	
	private static final Logger logger = LoggerFactory.getLogger(GraphQLController.class);
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private CardController cardController;
	
	@Autowired 
	private FilterController filterController;
	
	@Autowired
	private CardService cardService;
	
	@Autowired
	private QueryService queryService;
		
	@Autowired
	private TransformController transformController;
	
	@Autowired 
	private SecurityTool securityTool;
	
	private Map<String,CardDataFetcher> cardFetchers = new HashMap<String,CardDataFetcher>();
	
	private Map<String,CardListDataFetcher> cardListFetchers = new HashMap<String,CardListDataFetcher>();
	
	private Map<String,CardListFilterDataFetcher> cardListFilterFetchers = new HashMap<String,CardListFilterDataFetcher>();
	
	private Map<String,CardChildListFilterDataFetcher> cardChildListFilterDataFetchers = new HashMap<String,CardChildListFilterDataFetcher>();
	
	private Map<String,CardSearchDataFetcher> cardSearchFetchers = new HashMap<String,CardSearchDataFetcher>();
	
	private Map<String,TransformDataFetcher> transformFetchers = new HashMap<String,TransformDataFetcher>();
	
	private DynamicCardDataFetcher dynaCardFetcher;
	
	private DynamicCardListDataFetcher dynaCardListFetcher;
	
	private GraphQL graphQL;
	
	private boolean inError;
	
	private boolean inProgress;
	
	private String errorMsg;
	
	//@PostConstruct
	// Calling loadSchema from ListenerBean as soon as the application is ready.
	public void loadSchema() {
		if( !this.inProgress){
			new Thread() {
				public void run() {
					try {
						Thread.sleep(10000);
						securityTool.iAmSystem();
						reloadGraphQL();
					} catch (Exception ignore) {}
				}
			}.start();
		}
	}
	
	@RequestMapping(value="/query/{query}", method=RequestMethod.GET)
	public ResponseEntity<Object> query(@PathVariable  String query){
		logger.debug(query);
		if(graphQL==null){
			return new ResponseEntity<Object>("GraphQL Schema Not Loaded",HttpStatus.SERVICE_UNAVAILABLE);
		}
		ExecutionResult result = graphQL.execute(query);
		if(logger.isDebugEnabled() && result.getData()!=null){
			logger.debug(result.getData().toString());
		}
		if( result.getErrors() != null && result.getErrors().size()>0){
			logger.warn(String.valueOf(result.getErrors()));
			return new ResponseEntity<Object>(result.getErrors(),HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(result.getData());
	}
	
	@RequestMapping(value="/templates", method=RequestMethod.GET)
	public @ResponseBody Map<String, String> registry() throws Exception{
		return boardsCache.list();
	}
	
	@RequestMapping(value="/reload", method=RequestMethod.GET)
	public synchronized @ResponseBody boolean reloadGraphQL(){
		this.inProgress = true;
		try{
			GraphQLSchema schema = getGravitySchema();
			GraphQL newGraphQL = GraphQL.newGraphQL(schema).build();
			graphQL = newGraphQL;
			this.inError = false;
		} catch(Exception e) {
			logger.error("Schema Generation Failed", e);
			this.inError = true;
			this.errorMsg = e.getMessage();
		}
		this.inProgress = false;
		return !this.inError;
	}
	
	public GraphQLSchema getGravitySchema() throws Exception {
		
		this.dynaCardFetcher = new DynamicCardDataFetcher( this.cardService );
		this.dynaCardListFetcher = new DynamicCardListDataFetcher( this.cardController );
		
		graphql.schema.GraphQLSchema.Builder newSchema = newSchema();
		Set<String> boards = boardsCache.list().keySet();
		Set<String> transforms = transformController.listTransformChains(false).keySet();
		Map<String,GraphQLObjectType> types = new HashMap<String,GraphQLObjectType>();
		for(String boardId: boards){
			try{
				GraphQLObjectType objectTypeFromTemplate = getObjectTypeFromTemplate(boardId);
				types.put(boardId, objectTypeFromTemplate);
				newSchema = newSchema.query(objectTypeFromTemplate);
			} catch(ResourceNotFoundException e){
				logger.warn("Template Not Found on Board " + boardId);
			}
		}
		/*
		for(String transformId: transforms){
			try{
				//Board board = this.transformController.getBoardForTransform(transformId);

				GraphQLObjectType objectTypeFromTemplate = 
						getObjectTypeFromTemplate(board, this.transformController.getDatasourcesForTransform(transformId));
				
				types.put(transformId, objectTypeFromTemplate);
				newSchema = newSchema.query(objectTypeFromTemplate);
			} catch(ResourceNotFoundException e){
				logger.warn("Transform Not Found " + transformId);
			}
		}
		*/
		newSchema = newSchema.query(getGravityWiring(boards, transforms,  types));
		
		Set<GraphQLType> additionalTypes = new HashSet<GraphQLType>();
		//additionalTypes.add(GraphQLDate);
		
		logger.info("Schema Complete, Building");
		
		return newSchema.build(additionalTypes);
	}
	
	/**
	 * Given a specific template it will generate a Object Type for GraphQL
	 * This is used as part of dynamic Schema Generation for GraphQL
	 * 
	 * @param template
	 * @return
	 * @throws Exception 
	 */
	public GraphQLObjectType getObjectTypeFromTemplate( String id ) throws Exception{
		if( logger.isDebugEnabled()){
			logger.debug("Board: " + id);
		}
		Board board = boardsCache.getItem(id);
		Set<String> references = new HashSet<String>();
		references.add(id);
		return getObjectTypeFromTemplate(board,references);
	}
	
	/**
	 * Given a specific template it will generate a Object Type for GraphQL
	 * This is used as part of dynamic Schema Generation for GraphQL
	 * 
	 * @param template
	 * @return
	 * @throws Exception 
	 */
	public GraphQLObjectType getObjectTypeFromTemplate(Board board, Set<String> references) throws Exception{
		
		if( logger.isDebugEnabled()){
			logger.debug("Template: " + board.getId());
		}
		
		Builder builder = newObject();
		builder = builder.name(validName(board.getId()));
		builder = builder.description(board.getName());
		
		builder = builder.field(newFieldDefinition()
				.name("id")
				.description("ID")
				.type(GraphQLString));
		
		builder = builder.field(newFieldDefinition()
				.name("phase")
				.description("Phase")
				.type(GraphQLString));

		builder = builder.field(newFieldDefinition()
				.name("title")
				.description("Title")
				.type(GraphQLString));
		
		builder = builder.field(newFieldDefinition()
				.name("board")
				.description("Board")
				.type(GraphQLString));

		
		for( TemplateField field : board.getFields().values()){
					
			if(!"title".equals(field.getName())){

				if( logger.isDebugEnabled()){
					logger.debug("Template: " + board.getId() + ": Field: " + field.getName());
				}
				
				if( field.getOptionlist()==null) {
					
					GraphQLOutputType type = null;
					switch(field.getType()){
						case STRING:
							type = GraphQLString;
							break;
						case BOOLEAN:
							type = GraphQLBoolean;
							break;
						case NUMBER:
							type = GraphQLFloat;
							break;
						case LIST:
							type = list(GraphQLString);
							break;
						case DATE:
							type = GraphQLString; //GraphQLDate;
							break;
					}
					
					if( type!=null) {
						builder = builder.field(newFieldDefinition()
							.name(validName(field.getName()))
							.description(field.getLabel())
							.type(type) );
					}
					
				} else {
					
					if( field.getType().equals(FieldType.LIST)){
						
						builder = builder.field(newFieldDefinition()
								.name(validName(field.getName()))
								.description(field.getLabel())
								.type(list(new GraphQLTypeReference(validName(field.getOptionlist()))))
								.dataFetcher(this.dynaCardListFetcher)
								);
						
					} else {
					
						builder = builder.field(newFieldDefinition()
								.name(validName(field.getName()))
								.description(field.getLabel())
								.type(new GraphQLTypeReference(validName(field.getOptionlist())))
								.dataFetcher(this.dynaCardFetcher)
								);
					}

				}
			}
		}
		
		// Find all children....
		for( String reference : references){
			Map<String, String> children = findChildren(reference);
			for( Entry<String,String> entry : children.entrySet()){
				
				String boardId = entry.getKey();
				String fieldName = entry.getValue();
				String name = boardId + "_" + fieldName +"_children";
				String description = boardId + " " + fieldName +" Children";
				
				builder = builder.field(newFieldDefinition()
						.name(validName(name))
						.description(description)
						.type(list(new GraphQLTypeReference(validName(boardId))))
						.dataFetcher(getCardSearchFetcher(boardId,fieldName))
						);
				
				// Now find the filters and add filtered fields
				Board childBoard = boardsCache.getItem(boardId);
				if(childBoard!=null){
					Collection<Filter> filters = childBoard.getFilters().values();
					for( Filter filter : filters){
						
						graphql.schema.GraphQLFieldDefinition.Builder field = newFieldDefinition()
								.name(validName(boardId) + "_" + validName(filter.getId()))
								.type(list(new GraphQLTypeReference(validName(boardId))))
								.dataFetcher(getCardChildListFilterDataFetcher(boardId, fieldName, filter.getId()));
						
						for( Condition condition : filter.getConditions().values()){
							if( condition.getFieldName().contains("-")){
								continue;
							}
							if( condition.getFieldName().contains(".")){
								continue;
							}
							if((condition.getConditionType() == null) || ConditionType.PROPERTY.equals(condition.getConditionType())) {
								
								field.argument(newArgument()
			                            .name(condition.getFieldName())
			                            .description("field")
			                            .type(nonNull(GraphQLString)));
							}
						}
						
						builder.field(field);
					}	
				}
			}
		}
		
		if( logger.isDebugEnabled()){
			logger.debug("Build Complete :Template: " + board.getId());
		}
		return builder.build();
	}
	
	
	/**
	 * Finds all references to a board in other boards.
	 * This is used in order to create Child DataResolvers
	 * 
	 * Returns map where key = boardId, value = fieldName
	 * 
	 * @param boardId
	 * @return
	 * @throws Exception 
	 */
	private Map<String,String> findChildren(String listId) throws Exception{
		Map<String,String> result = new HashMap<String,String>();
		for( String boardId:  boardsCache.list().keySet()){
			try{
				Board board = boardsCache.getItem(boardId, boardId);
				for( TemplateField field : board.getFields().values()){
					if( listId.equals(field.getOptionlist())){
						result.put(boardId, field.getName());
					}
				}
			} catch( ResourceNotFoundException e){
				logger.warn("Template Not Found on Board " + boardId);
			}
		}
		return result;
	}
	
	private GraphQLObjectType getGravityWiring(Collection<String> boards, Collection<String> transforms, Map<String,GraphQLObjectType> types){
		
		Builder builder = newObject();
		builder = builder.name("Query");
		builder = builder.description("Query");
		
		for( String boardId : boards){
			
			GraphQLObjectType type = types.get(boardId);
			
			if( type!=null){
				builder = builder.field(newFieldDefinition()
						.name(validName(boardId))
						.type(type)
						.argument(newArgument()
	                            .name("id")
	                            .description("id")
	                            .type(nonNull(GraphQLString)))
						.dataFetcher(getCardFetcher(boardId))
						);
				
				builder = builder.field(newFieldDefinition()
						.name(validList(boardId))
						.type(list(type))
						.dataFetcher(getCardListFetcher(boardId))
						);
				
				builder = builder.field(newFieldDefinition()
						.name(validName(boardId)+"_filtered")
						.type(list(type))
						.argument(newArgument()
	                            .name("filter")
	                            .description("filter")
	                            .type(nonNull(GraphQLString)))
						.dataFetcher(getCardListFetcher(boardId))
						);
				
				builder = builder.field(newFieldDefinition()
						.name(validName(boardId)+"_search")
						.type(list(type))
						.argument(newArgument()
	                            .name("field")
	                            .description("field")
	                            .type(nonNull(GraphQLString)))
						.argument(newArgument()
	                            .name("value")
	                            .description("value")
	                            .type(nonNull(GraphQLString)))
						.dataFetcher(getCardSearchFetcher(boardId,null))
						);
				
				// Loop over filters
				try{
					Board board = boardsCache.getItem(boardId);
					Collection<Filter> filters = board.getFilters().values();
					for( Filter filter : filters){
												
						graphql.schema.GraphQLFieldDefinition.Builder field = newFieldDefinition()
								.name(validName(boardId) + "_" + validName(filter.getId()))
								.type(list(type))
								.dataFetcher(getCardFilterFetcher(boardId,filter.getId()));
						
						for( Condition condition : filter.getConditions().values()){	
							field.argument(newArgument()
			                            .name(condition.getFieldName())
			                            .description("field")
			                            .type(nonNull(GraphQLString)));
						}
						builder.field(field);
					}
				} catch( Exception e) {
					logger.warn("Exception generating GraphQL Schema " + e.getMessage());
				}
			} else {
				logger.warn("Schema Generation Skipping: Type not found for Board " + boardId);
			}
		}
		
		for( String transform : transforms){
			GraphQLObjectType type = types.get(transform);
			if( type!=null){
				
				builder = builder.field(newFieldDefinition()
						.name(validList(transform))
						.type(list(type))
						.dataFetcher(getTransformFetcher(transform))
						);
			} else {
				logger.warn("Schema Generation Skipping: Type not found for Transform " + transform);
			}
		}
		return builder.build();
	}
	
	protected CardDataFetcher getCardFetcher(String boardId){
		CardDataFetcher cardDataFetcher = this.cardFetchers.get(boardId);
		if(cardDataFetcher==null){
			cardDataFetcher = new CardDataFetcher(boardId, cardService);
			this.cardFetchers.put(boardId, cardDataFetcher);
		}
		return cardDataFetcher;
	}

	protected CardListDataFetcher getCardListFetcher(String boardId){
		CardListDataFetcher cardListDataFetcher = this.cardListFetchers.get(boardId);
		if(cardListDataFetcher==null){
			cardListDataFetcher = new CardListDataFetcher(boardId,cardController);
			this.cardListFetchers.put(boardId, cardListDataFetcher);
		}
		return cardListDataFetcher;
	}
	
	protected CardListFilterDataFetcher getCardFilterFetcher(String boardId, String filterId){
		String id = boardId + "." + filterId;
		CardListFilterDataFetcher cardListFilterDataFetcher = this.cardListFilterFetchers.get(id);
		if(cardListFilterDataFetcher==null){
			cardListFilterDataFetcher = new CardListFilterDataFetcher(boardId, filterId, filterController);
			this.cardListFilterFetchers.put(id, cardListFilterDataFetcher);
		}
		return cardListFilterDataFetcher;
	}
	
	protected CardChildListFilterDataFetcher getCardChildListFilterDataFetcher(String boardId, String fieldName, String filterId){
		String id = boardId + "." + fieldName + "." + filterId;
		CardChildListFilterDataFetcher cardListFilterDataFetcher = this.cardChildListFilterDataFetchers.get(id);
		if(cardListFilterDataFetcher==null){
			cardListFilterDataFetcher = new CardChildListFilterDataFetcher(boardId, fieldName, filterId, boardsCache, queryService );
			this.cardChildListFilterDataFetchers.put(id, cardListFilterDataFetcher);
		}
		return cardListFilterDataFetcher;
	}
	
	protected TransformDataFetcher getTransformFetcher(String transformId){
		TransformDataFetcher transformDataFetcher = this.transformFetchers.get(transformId);
		if(transformDataFetcher==null){
			transformDataFetcher = new TransformDataFetcher(transformId, this.transformController);
			this.transformFetchers.put(transformId, transformDataFetcher);
		}
		return transformDataFetcher;
	}
	
	protected CardSearchDataFetcher getCardSearchFetcher(String boardId, String field){
		CardSearchDataFetcher cardSearchDataFetcher = this.cardSearchFetchers.get(boardId+"."+field);
		if(cardSearchDataFetcher==null){
			cardSearchDataFetcher = new CardSearchDataFetcher(boardId,field,queryService);
			this.cardSearchFetchers.put(boardId+"."+field, cardSearchDataFetcher);
		}
		return cardSearchDataFetcher;
	}
	
	protected String validName( String name){
		return name.replace("-", "_");
	}
	
	protected String validList( String name){
		return name.replace("-", "_") + "_list";
	}
	
	public boolean isInProgress() {
		return inProgress;
	}
	
	public boolean isInError() {
		return inError;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {
		logger.info("Context Refresh Event Listner Load graphqlSchema");
		this.loadSchema();
	}
	
}
