/*package nz.devcentre.gravity.graphql;

import java.time.LocalDateTime;
import java.util.Date;

import org.owasp.encoder.Encode;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

public class CustomScalars {
	
	public static GraphQLScalarType GraphQLDate = 
			new GraphQLScalarType("Date", "Date type", new Coercing<Date, String>() {
		
		private Date convertImpl(Object input) {
            if (input instanceof String) {
                LocalDateTime localDateTime = GraphQLTools.parseDate((String) input);

                if (localDateTime != null) {
                    return GraphQLTools.toDate(localDateTime);
                }
            }
            return null;
        }

        @Override
        public String serialize(Object input) {
            if (input instanceof Date) {
                return GraphQLTools.toISOString((Date) input);
            } else {
                Date result = convertImpl(input);
                if (result == null) {
                    throw new CoercingSerializeException(Encode.forHtmlContent("Invalid value '" + input + "' for Date"));
                }
                return GraphQLTools.toISOString(result);
            }
        }

        @Override
        public Date parseValue(Object input) {
            Date result = convertImpl(input);
            if (result == null) {
                throw new CoercingParseValueException(Encode.forHtmlContent("Invalid value '" + input + "' for Date"));
            }
            return result;
        }

        @Override
        public Date parseLiteral(Object input) {
            if (!(input instanceof StringValue)) return null;
            String value = ((StringValue) input).getValue();
            Date result = convertImpl(value);
            return result;
        }
    }, null, null, null, null);

}
*/