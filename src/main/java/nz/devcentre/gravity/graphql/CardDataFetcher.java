package nz.devcentre.gravity.graphql;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.tools.CardTools;

public class CardDataFetcher implements DataFetcher<Map<String,Object>>{
	
	private static final Logger logger = LoggerFactory.getLogger(CardDataFetcher.class);

	private final String boardId;
	
	private final CardService cardService;
	
	public CardDataFetcher( String boardId, CardService cardService ){
		this.boardId = boardId;
		this.cardService = cardService;
		if( logger.isDebugEnabled()){
			logger.debug("Creating CardDataFetcher for " + boardId);
		}
	}
	
	@Override
	public Map<String,Object> get(DataFetchingEnvironment env) {
		Map<String,Object> argument = env.getArguments();
		String cardId = (String) argument.get("id");
		if( logger.isDebugEnabled()){
			logger.info("Fetching Data for " + boardId + "." + cardId);
		}
		try {
			return CardTools.cardToMap(cardService.getCard(boardId, cardId));
		} catch (Exception e) {
			logger.warn("Exception in Fetching Card",e);
			return null;
		}
	}
}
