package nz.devcentre.gravity.graphql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;

//@Component
public class BoardListDataFetcher implements DataFetcher<List<Board>>{

	@Autowired
	BoardsCache boardsCache;
	
	@Override
	public List<Board> get(DataFetchingEnvironment arg0) {
		try {
			return boardsCache.getAllBoards();
		} catch (Exception e) {
			return null;
		}
	}

}
