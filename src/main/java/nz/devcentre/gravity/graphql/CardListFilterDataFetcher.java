/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.graphql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.controllers.FilterController;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.tools.CardTools;

public class CardListFilterDataFetcher implements DataFetcher<List<Map<String,Object>>>{

	private static final Logger logger = LoggerFactory.getLogger(CardListFilterDataFetcher.class);
	
	private final String boardId;
	
	private final String filterId;
	
	private final FilterController filterController;
	
	public CardListFilterDataFetcher( String boardId, String filterId, FilterController filterController ){
		this.boardId = boardId;
		this.filterId = filterId;
		this.filterController = filterController;
		logger.info("Creating CardListFilterDataFetcher for " + boardId + "." + filterId);
	}
	
	@Override
	public List<Map<String,Object>> get(DataFetchingEnvironment env) {
		logger.info("Fetching Data for " + boardId + " filtering on " + filterId);		
		try {
			List<Map<String,Object>> returnList= new ArrayList<Map<String,Object>>();
			Collection<Card> cards = this.filterController.dynamicFilter(boardId, null, filterId, env.getArguments());
			for( Card card : cards ){
				returnList.add( CardTools.cardToMap(card));
			}
			return returnList;
		} catch (Exception e) {
			logger.warn("Exception in Fetching CardList",e);
			return null;
		}
	}
}
