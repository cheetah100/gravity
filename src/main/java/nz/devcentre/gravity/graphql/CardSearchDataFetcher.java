/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited 
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.graphql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.FilterFactory;

public class CardSearchDataFetcher implements DataFetcher<List<Map<String,Object>>>{

	private static final Logger logger = LoggerFactory.getLogger(CardSearchDataFetcher.class);
	
	private final QueryService queryService;
	
	private final String boardId;
	
	private final String field;
	
	public CardSearchDataFetcher( String boardId, String field, QueryService queryService ){
		this.boardId = boardId;
		this.field = field;
		this.queryService = queryService;
		
		if( logger.isDebugEnabled()){
			logger.debug("Creating CardSearchDataFetcher for " + boardId + "." + field);
		}
	}
	
	@Override
	public List<Map<String,Object>> get(DataFetchingEnvironment env) {
		if( logger.isDebugEnabled()){
			logger.debug("Searching " + boardId);
		}
		
		String value = null;
		Object source = env.getSource();
		if( source !=null && source instanceof Map){
			Map<String,Object> vars = (Map<String,Object>) source;
			value = (String) vars.get("id");
		}
		
		if(value==null){
			Object obj = env.getArgument("value");
			if( obj!=null && obj instanceof String){
				value = (String) obj;
			}
		}
		
		String actualField = this.field;
		if( actualField==null){
			Object obj = env.getArgument("field");
			if( obj!=null && obj instanceof String){
				actualField = (String) obj;
			}
		}		
		
		try {
			List<Map<String,Object>> returnList= new ArrayList<Map<String,Object>>();
			
			Filter filter = FilterFactory.singleConditionFilter(actualField, Operation.EQUALTO, value);			
			Collection<Card> cards = queryService.dynamicQuery(boardId, null, filter, null , false);
			
			for( Card card : cards ){
				returnList.add( CardTools.cardToMap(card));
			}
			return returnList;
		} catch (Exception e) {
			logger.warn("Exception in Search",e);
			return null;
		}
	}
}
