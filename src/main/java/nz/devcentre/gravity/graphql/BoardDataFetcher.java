package nz.devcentre.gravity.graphql;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;

//@Component
public class BoardDataFetcher implements DataFetcher<Board>{

	@Autowired
	BoardsCache boardsCache;
	
	@Override
	public Board get(DataFetchingEnvironment env) {
		Map<String,Object> argument = env.getArguments();
		String id = (String) argument.get("id");
		try {
			return boardsCache.getItem(id);
		} catch (Exception e) {
			return null;
		}
	}
}
