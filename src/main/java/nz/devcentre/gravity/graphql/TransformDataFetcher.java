package nz.devcentre.gravity.graphql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.controllers.TransformController;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.tools.CardTools;

public class TransformDataFetcher implements DataFetcher<List<Map<String,Object>>>{

	private static final Logger logger = LoggerFactory.getLogger(TransformDataFetcher.class);
	
	private final String transformId;
	
	private final TransformController transformController;
	
	public TransformDataFetcher( String transformId, TransformController transformController ){
		this.transformId = transformId;
		this.transformController = transformController;
		if( logger.isDebugEnabled()){
			logger.debug("Creating TransformDataFetcher for " + transformId);
		}
	}
	
	@Override
	public List<Map<String,Object>> get(DataFetchingEnvironment env) {
		logger.info("Fetching All Data for " + transformId);
		try {
			List<Map<String,Object>> returnList= new ArrayList<Map<String,Object>>();
			Map<String, Object> objects = this.transformController.executeTransform(transformId);
			
			for( Object object : objects.values() ){
				if( object instanceof Card){
					Card card = (Card) object;
					returnList.add(CardTools.cardToMap(card));
				}
			}
			return returnList;
		} catch (Exception e) {
			logger.warn("Exception in Fetching CardList",e);
			return null;
		}
	}
}
