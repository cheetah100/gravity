/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2018 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.graphql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Condition;
import nz.devcentre.gravity.model.Filter;
import nz.devcentre.gravity.model.Operation;
import nz.devcentre.gravity.services.QueryService;
import nz.devcentre.gravity.tools.CardTools;
import nz.devcentre.gravity.tools.FilterTools;

public class CardChildListFilterDataFetcher implements DataFetcher<List<Map<String,Object>>>{

	private static final Logger logger = LoggerFactory.getLogger(CardChildListFilterDataFetcher.class);
	
	private final String boardId;
	
	private final String fieldName;
	
	private final String filterId;
	
	private final QueryService queryService;
	
	private final BoardsCache boardsCache;
	
	public CardChildListFilterDataFetcher( 
			String boardId,
			String fieldName,
			String filterId, 
			BoardsCache boardsCache, 
			QueryService queryService){
		
		this.boardId = boardId;
		this.fieldName = fieldName;
		this.filterId = filterId;
		this.boardsCache = boardsCache;
		this.queryService = queryService;
		if( logger.isDebugEnabled()){
			logger.debug("Creating CardListFilterDataFetcher for " + boardId + "." + filterId);
		}
	}
	
	@Override
	public List<Map<String,Object>> get(DataFetchingEnvironment env) {
		if( logger.isDebugEnabled()){
			logger.debug("Fetching Data for " + boardId + " filtering on " + filterId);
		}
		try {
			List<Map<String,Object>> returnList= new ArrayList<Map<String,Object>>();
			
			Object source = env.getSource();
			
			if( !(source instanceof Map)){
				logger.warn("Source Not Map in CardChildListFilterDataFetcher");
				return null;
			}
			
			Map<String,Object> vars = (Map<String,Object>) source;
			String cardId = (String) vars.get("id");
			
			Board board = boardsCache.getItem(this.boardId);
			Filter storedFilter = board.getFilter(filterId);
			Filter filter = FilterTools.replaceFilterValues(storedFilter, env.getArguments());
			
			Condition idCondition = new Condition();
			idCondition.setFieldName( this.fieldName );
			idCondition.setOperation(Operation.EQUALTO);
			idCondition.setValue(cardId);
			filter.getConditions().put("dynamic", idCondition);
			
			Collection<Card> cards = queryService.dynamicQuery(boardId, null, filter, null, false);
			
			for( Card card : cards ){
				returnList.add( CardTools.cardToMap(card));
			}
			return returnList;
		} catch (Exception e) {
			logger.warn("Exception in Fetching CardList",e);
			return null;
		}
	}
}
