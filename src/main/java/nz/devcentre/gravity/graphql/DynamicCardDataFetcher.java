/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco 
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.graphql;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.services.CardService;
import nz.devcentre.gravity.tools.CardTools;

public class DynamicCardDataFetcher implements DataFetcher<Map<String,Object>>{
	
	private static final Logger logger = LoggerFactory.getLogger(DynamicCardDataFetcher.class);

	CardService cardService;
	
	public DynamicCardDataFetcher( CardService cardService ){
		this.cardService = cardService;
		if( logger.isDebugEnabled()){
			logger.debug("Creating DynamicCardDataFetcher");
		}
	}
	
	@Override
	public Map<String,Object> get(DataFetchingEnvironment env) {
		String field = env.getFields().iterator().next().getName();
		String boardId = env.getFieldType().toString();
		Object source = env.getSource();
		
		if( !(source instanceof Map)){
			logger.warn("Source Not Map in DynamicCardDataFetcher");
			return null;
		}
		
		Map<String,Object> vars = (Map<String,Object>) source;
		String cardId = (String) vars.get(field);
		
		if( logger.isDebugEnabled()){
			logger.debug("Fetching Data for " + boardId + "." + cardId + " based on value from " + field);
		}
			
		if(cardId==null){
			return null;
		}
		
		try {
			return CardTools.cardToMap(cardService.getCard(boardId, cardId));
		} catch (ResourceNotFoundException e) {
			logger.warn("Card Not Found: " + boardId + "." + cardId );
			return null;
		} catch (Exception e) {
			logger.warn("Exception in Fetching Card",e);
			return null;
		}
	}
	
	protected String correctBoardName( String name){
		if(name==null) return null;
		return name.replace("_", "-");
	}
}
