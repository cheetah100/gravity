/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.graphql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLList;
import nz.devcentre.gravity.controllers.CardController;
import nz.devcentre.gravity.tools.CardTools;

public class DynamicCardListDataFetcher implements DataFetcher<List<Map<String,Object>>>{
	
	private static final Logger logger = LoggerFactory.getLogger(DynamicCardListDataFetcher.class);
	
	private final CardController cardController;
	
	public DynamicCardListDataFetcher( CardController cardController ){
		this.cardController = cardController;
		if( logger.isDebugEnabled()){
			logger.debug("Creating DynamicCardDataFetcher");
		}
	}
	
	@Override
	public List<Map<String,Object>> get(DataFetchingEnvironment env) {
		String field = env.getFields().iterator().next().getName();
		
		GraphQLList fieldType = (GraphQLList) env.getFieldType();
		String boardId = fieldType.getWrappedType().toString();
		
		Object source = env.getSource();
		
		if( !(source instanceof Map)){
			logger.warn("Source Not Map in DynamicCardDataFetcher");
			return null;
		}
		
		Map<String,Object> vars = (Map<String,Object>) source;
		Object value = vars.get(field);
		
		if( logger.isDebugEnabled()){
			logger.debug("Fetching Data for " + boardId + " based on value from " + field);
		}
			
		if(value==null || !(value instanceof String[] || value instanceof Object[] || value instanceof List)){
			return null;
		}
		
		List<String> cardIds = null;
		if( value instanceof String[]){
			cardIds = Arrays.asList((String[])value);
		} else if ( value instanceof Object[]){
			cardIds = new ArrayList<String>();
			Object[] array = (Object[]) value;
			for( int f=0; f<array.length; f++){
				String s = (String) array[f];
				cardIds.add(s);
			}
		} else if( value instanceof List){
			cardIds = (List<String>) value;
		}
		
		try {
			return CardTools.cardsToList(cardController.getCardListById(boardId, null, false, cardIds));
			
		} catch (Exception e) {
			logger.warn("Exception in Fetching CardList",e);
			return null;
		}
	}
	
	protected String correctBoardName( String name){
		return name.replace("_", "-");
	}
}
