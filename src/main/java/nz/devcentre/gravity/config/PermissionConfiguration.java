package nz.devcentre.gravity.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

import nz.devcentre.gravity.security.GravityPermissionEvaluator;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true, securedEnabled = true)
public class PermissionConfiguration extends GlobalMethodSecurityConfiguration  {
	
	protected static final Logger logger = LoggerFactory.getLogger(PermissionConfiguration.class);
	
	/*
	 * Security configuration happens much before the beans creation. Hence 
	 * autowiring does not work at this point. As such, creating a 
	 * GravityPermissionEvaluator bean using 
	 * new operator.
	 */
	
	//@Autowired
	GravityPermissionEvaluator evaluator = new GravityPermissionEvaluator();
	
	protected DefaultMethodSecurityExpressionHandler bean;
	
	@Bean
	public MethodSecurityExpressionHandler expressionHandler() {
		logger.info("Configure Security Expression Handler, evaluator is " + evaluator);
		bean = new DefaultMethodSecurityExpressionHandler();
		bean.setPermissionEvaluator(evaluator);
		return bean;
	}
	
	@Override
	protected MethodSecurityExpressionHandler createExpressionHandler() {
		return bean;
	}
}
