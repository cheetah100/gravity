/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.config;

import java.util.Properties;
import java.util.concurrent.Executor;

import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfigurtion {
	
	@Value("${mail.smtp_host:#{null}}")
	private String hostname;
	
	@Value("${mail.smtp_starttls_enable:false}")
	private boolean smtpStartTlsEnable;
	
	@Value("${mail.smtp_port:22}")
	private int port;
	
	@Value("${mail.smtp_username:#{null}}")
	private String username;
	
	@Value("${mail.smtp_password:#{null}}")
	private String password;
	
	@Bean
	public VelocityEngine createVelocity() {
		return new VelocityEngine();
	}
	
	@Bean
	public XStreamMarshaller createXStreamMarshaller() {
		XStreamMarshaller xStreamMarshaller = new XStreamMarshaller();
		return xStreamMarshaller;
	}
	
	@Bean
	public JavaMailSenderImpl createJavaMailSenderImpl() {

		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setHost(this.hostname); 
		mailSender.setPort(this.port);
		
		if(smtpStartTlsEnable){
			if(StringUtils.isNotBlank(this.password)&&
					StringUtils.isNotBlank(this.username)){
				mailSender.setUsername(this.username); 
				mailSender.setPassword(this.password);
			}

			mailSender.setProtocol("smtp"); 
			//Hint : Plz note port should be 587 for smtp over	TLS on most of the smtp serves.

			Properties props = new Properties();
			props.put("mail.smtp.starttls.enable",true); 
			props.put("mail.smtp.auth",	true);			
			props.put("mail.smtp.socketFactory.port", port);
			props.put("mail.smtp.debug", true);
			props.put("mail.smtp.ssl.protocols", "TLSv1.2");
			props.put("mail.smtp.ssl.trust", this.hostname);
			mailSender.setJavaMailProperties(props);
		}
		return mailSender;
	}
	
	@Bean
	public DocumentBuilderFactory createDocumentBuilderFactory() throws Exception{
		return DocumentBuilderFactory.newInstance();
	}
	
	@Bean
	public RestTemplate createRestTemplate() throws Exception{
		return new RestTemplate(); 
	}
	
	@Bean
	public Executor asyncExecutor() {
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(2);
	    executor.setMaxPoolSize(10);
	    executor.setQueueCapacity(500);
	    executor.setThreadNamePrefix("GravityPool-");
	    executor.initialize();
	    return executor;
	}
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public boolean isSmtpStartTlsEnable() {
		return smtpStartTlsEnable;
	}

	public void setSmtpStartTlsEnable(boolean smtpStartTlsEnable) {
		this.smtpStartTlsEnable = smtpStartTlsEnable;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
