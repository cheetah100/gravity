/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
public class LDAPConfiguration {
	
	private static final Logger logger = LoggerFactory.getLogger(LDAPConfiguration.class);
	
	@Value("${ldap_url:#{null}}")
	private String url;

	@Value("${ldap_partitionSuffix:#{null}}")                         
	private String partitionSuffix;
	
	@Value("${ldap_principal:#{null}}")
	private String principal;
	
	@Value("${ldap_password:#{null}}")
	private String password;

	@Bean
	public LdapContextSource contextSource() {
		
		logger.info("LDAP: " + url + " , " + partitionSuffix + ", " + principal + ", " + password);
		
	    LdapContextSource contextSource = new LdapContextSource();
	    contextSource.setUrl(url);
	    contextSource.setBase(partitionSuffix);
	    contextSource.setUserDn(principal);
	    contextSource.setPassword(password);
	    return contextSource;
	}
	
	@Bean
	public LdapTemplate ldapTemplate() {
	    return new LdapTemplate(contextSource());
	}

}

