package nz.devcentre.gravity.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

@Configuration
@PropertySource({"classpath:gravity.properties","classpath:git.properties"})
public class SystemConfiguration {
	
	@Value("${environment:local}")
	String environment;
	
	@Value( "${version:na}" )
	String version;

	@Value( "${branch:na}" )
	String branch;

	@Value( "${build:na}" )
	String build;

	public String getVersion() {
		return version;
	}

	public String getBuild() {
		return build;
	}
	
	public String getBranch() {
		return branch;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
		PropertySourcesPlaceholderConfigurer propsConfig = new PropertySourcesPlaceholderConfigurer();
		propsConfig.setLocation(new ClassPathResource("git.properties"));
		propsConfig.setIgnoreResourceNotFound(true);
		propsConfig.setIgnoreUnresolvablePlaceholders(true);
		return propsConfig;
	}

}
