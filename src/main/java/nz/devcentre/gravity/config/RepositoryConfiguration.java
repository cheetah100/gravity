/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.config;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories(basePackages="nz.devcentre.gravity.repository")
@EnableSpringDataWebSupport
public class RepositoryConfiguration {
	
	@Value("${repository.url:mongodb://localhost/gravity}")
	private String url;
	
	@Value("${javax.net.ssl.trustStore:}")
	private String truststore;
	
	@Value("${javax.net.ssl.trustStorePassword:}")
	private String truststorePassword;

	@Value("${javax.net.ssl.keyStore:}")
	private String keystore;
	
	@Value("${javax.net.ssl.keyStorePassword:}")
	private String keystorePassword;

	
	@Autowired
	private MongoMappingContext mongoMappingContext;
	
	private static final Logger logger = LoggerFactory.getLogger(RepositoryConfiguration.class);
	
	public @Bean
	MongoDbFactory mongoDbFactory() throws Exception {
		logger.info("Gravity Repository Configured At: " + this.url);
		
		if( StringUtils.isNotEmpty(keystore) && StringUtils.isNotEmpty(keystorePassword)) {
			logger.info("Keystore: " + keystore);
			System.setProperty("javax.net.ssl.keyStore", keystore);
			System.setProperty("javax.net.ssl.keyStorePassword", keystorePassword);
		}
		
		if( StringUtils.isNotEmpty(truststore) && StringUtils.isNotEmpty(truststorePassword) ) {
			logger.info("Truststore: " + truststore);
			System.setProperty("javax.net.ssl.trustStore", truststore);
	        System.setProperty("javax.net.ssl.trustStorePassword", truststorePassword);
		}
		
		MongoClientURI uri = new MongoClientURI(this.url);
		MongoClient mc = new MongoClient(uri);
		SimpleMongoDbFactory dbFactory = new SimpleMongoDbFactory(mc, uri.getDatabase());
		dbFactory.setWriteConcern(WriteConcern.MAJORITY);
		return dbFactory;
	}
	
	public @Bean
	MongoTemplate mongoTemplate() throws Exception {
		MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
		return mongoTemplate;
	}
	
	@Bean
	public MappingMongoConverter mongoConverter(MongoDbFactory mongoFactory) throws Exception {
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoFactory);
	    MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver, mongoMappingContext);
	    mongoConverter.setMapKeyDotReplacement("-");
	    return mongoConverter;
	 }

}


