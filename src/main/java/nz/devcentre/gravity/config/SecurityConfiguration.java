package nz.devcentre.gravity.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		http.cors().disable();
		http.csrf().disable();
		
		http.authorizeRequests()
		.antMatchers("/api/public/**")
		.permitAll();
		
		/* antMatcher("/api/**") */
		
		http
		.authorizeRequests()
        .anyRequest().authenticated()
        .and()
        .oauth2Login();
		
		return http.build();
	}

}