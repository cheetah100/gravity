/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2022 Devcentre Ltd
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Component
public class SwaggerInit {
	
	private static final Logger logger = LoggerFactory.getLogger(SwaggerInit.class);
	
	@Autowired
	SystemConfiguration systemConfiguration;
	
	@Bean
    public Docket apiDocket() {
		
		logger.info("Swagger Starting");
		
		ApiInfo apiInfo = new ApiInfoBuilder()
	        .title("Gravity API " + this.systemConfiguration.getEnvironment().toUpperCase())
	        .description("API Documentation for Gravity Workflow Automation Engine - " +
	        "Version " + this.systemConfiguration.getBranch() + 
	        " - Build " + this.systemConfiguration.getBuild())
	        .version(this.systemConfiguration.getVersion())
	        .build();
		
        return new Docket(DocumentationType.SWAGGER_2)
        		.apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
	
}