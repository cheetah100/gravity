package nz.devcentre.gravity.config;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Base64UrlNamingStrategy;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
public class RabbitConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(RabbitConfiguration.class);
	
	@Value("${environment:local}")
	private String environment;

	@Value("${rabbitmq.server:amqp://localhost:5672}")
	private String rabbitMQServer;
	
	@Value("${rabbitmq.api.url:http://localhost:15672}")
	private String rabbitMQApiUrl;
	
	@Value("${rabbitmq.user.name:guest}")
	private String userName;

	@Value("${rabbitmq.user.password:guest}")
	private String password;
	
	@Bean
	public ConnectionFactory connectionFactory() {
		
		logger.info("Starting RabbitMQ Connection Factory on: " + rabbitMQServer + " for user " + userName + " environment " + getEnvironment());
		
		URI uri = URI.create(rabbitMQServer);	
		CachingConnectionFactory factory = new CachingConnectionFactory(uri);
		factory.setUsername(userName);
		factory.setPassword(password);
		return factory;
	}

	@Bean
	public AmqpAdmin amqpAdmin(ConnectionFactory factory) {
		return new RabbitAdmin(factory);
	}

	@Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory factory) {
		return new RabbitTemplate(factory);
	}
	
	/* Queues */

	@Bean
	public Queue gravityIdGeneratorQueue() {
		Queue q = new AnonymousQueue(new Base64UrlNamingStrategy(getEnvironment() + "_gravityIdGenerator-"));
		return q;
	}
	
	@Bean
	public Queue inputCardQueue() {
		Queue q = new Queue(getEnvironment() + "_gravityInput");
		return q;
	}
	
	@Bean
	public Queue gravityEventsQueue() {
		Queue q = new Queue(getEnvironment() + "_gravityEvents");
		//Queue q = new AnonymousQueue(new AnonymousQueue.Base64UrlNamingStrategy(getEnvironment() + "_gravityEvents-"));
		return q;
	}
	
	@Bean
	public Queue gravityCacheInvalidationQueue() {
		Queue q = new AnonymousQueue(new Base64UrlNamingStrategy(getEnvironment() + "_gravityCacheInvalidation-"));
		return q;
	}
	
	@Bean
	public Queue gravityCardLockingQueue() {
		Queue q = new AnonymousQueue(new Base64UrlNamingStrategy(getEnvironment() + "_gravityCardLocking-"));
		return q;
	}
	
	/* Fanouts Exchanges*/

	@Bean(name = "gravityIdGeneratorExchange")
	public FanoutExchange gravityIdGeneratorExchange() {
		FanoutExchange fe = new FanoutExchange( getEnvironment() + "_gravityIdGenerator.ex");
		return fe;
	}

	@Bean
	public FanoutExchange gravityEventsExchange() {
		FanoutExchange fe = new FanoutExchange(getEnvironment() + "_gravityEvents.ex");
		return fe;
	}

	@Bean
	public FanoutExchange gravityCacheInvalidationExchange() {
		FanoutExchange fe = new FanoutExchange(getEnvironment() + "_gravityCacheInvalidation.ex");
		return fe;
	}
	
	@Bean
	public FanoutExchange gravityCardLockingExchange() {
		FanoutExchange fe = new FanoutExchange(getEnvironment() + "_gravityCardLocking.ex");
		return fe;
	}
	
	/* Bindings */
	
	@Bean
	public Binding bindGravityIdGeneratorQueue(FanoutExchange gravityIdGeneratorExchange,
			Queue gravityIdGeneratorQueue) {
		return BindingBuilder.bind(gravityIdGeneratorQueue).to(gravityIdGeneratorExchange);
	}

	@Bean
	public Binding bindGravityEventsQueue(FanoutExchange gravityEventsExchange, Queue gravityEventsQueue) {
		return BindingBuilder.bind(gravityEventsQueue).to(gravityEventsExchange);
	}

	@Bean
	public Binding bindGravityCacheInvalidationQueue(FanoutExchange gravityCacheInvalidationExchange,
			Queue gravityCacheInvalidationQueue) {
		return BindingBuilder.bind(gravityCacheInvalidationQueue).to(gravityCacheInvalidationExchange);
	}

	@Bean
	public Binding bindGravityCardLockingQueue(FanoutExchange gravityCardLockingExchange,
			Queue gravityCardLockingQueue) {
		return BindingBuilder.bind(gravityCardLockingQueue).to(gravityCardLockingExchange);
	}

	@Bean
	public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory( ConnectionFactory conFactory) {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(conFactory);
		factory.setMaxConcurrentConsumers(10);
		return factory;
	}

	public String getHostName() {
		return rabbitMQServer;
	}
	
	public String getApiUrl() {
		return rabbitMQApiUrl;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}
}
