/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.managers;


import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.IdGeneratorReceiver;
import nz.devcentre.gravity.automation.SyncMessage;
import nz.devcentre.gravity.model.Coup;
import nz.devcentre.gravity.model.IdRequest;
import nz.devcentre.gravity.model.IdResponse;

/**
 * The purpose of the Cluster manager is to manage all the functions that need to be
 * coordinated across the Cluster.
 * 
 * - Invalidation of Caches.
 * - Phase Updates
 * - Getting Identifiers.
 * - Master Server Protocol
 * 
 * @author peter
 *
 */
@Component
public class ClusterManager {
	
	private static final Logger LOG = LoggerFactory.getLogger(ClusterManager.class);
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private ExceptionManager exceptionManager;
	
	private boolean leader;
	
	private String leaderId;
	
	private long leaderLastContact = System.currentTimeMillis();
	
	private long startTime = System.currentTimeMillis();
	
	private String serverId = RandomStringUtils.random(10, true, true);
	
	private Coup coup;
	
	@Autowired
	private IdGeneratorReceiver idSyncReceiever;
	
	@Autowired 
	TimerManager timerManager;
	
	@Autowired
	@Qualifier("gravityIdGeneratorExchange")
	FanoutExchange gravityIdGeneratorExchange;
	
	@PostConstruct
	public void startUp() throws Exception {
		LOG.info("Starting Cluster Manager");
		this.coup = new Coup();
		this.coup.setRequestId(this.serverId);
		this.coup.setCoupTime(this.startTime);
		this.executeChallenge();
	}
		
	public Long getId(String boardId, String name) throws Exception {
		if( LOG.isDebugEnabled()){
			LOG.debug("Generating ID from " + boardId + "." + name);
		}
		
		Message message = requestId(boardId,name);
		
		if(message==null){
			executeChallenge();
			message = requestId(boardId,name);
		}
		ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
		ObjectInputStream ois = new ObjectInputStream(bis);

		IdResponse idResponse  = (IdResponse) ois.readObject();
		return idResponse.getId();
	}
	
	private Message requestId(String boardId, String name){
		String requestId = RandomStringUtils.random(10, true, true);
		SyncMessage idSync = idSyncReceiever.register(requestId);
		
		IdRequest request = new IdRequest();
		request.setBoardId(boardId);
		request.setName(name);
		request.setRequestId(requestId);
		rabbitTemplate.convertAndSend(gravityIdGeneratorExchange.getName(),"", request);
		
		return idSync.receiveMessage(10000l);
	}
	
	public String getIdString(String path, String field, String prefix) throws Exception{
		Long id = getId( path, field );
		return prefix + id.toString();
	}
	
	/**
	 * Note that this method is simply a wrapper to eat the return value of executeChallenge.
	 * This is needed to be able to schedule the task.
	 * 
	 * @throws Exception
	 */
	@Scheduled( fixedDelay=5000l )
	public void pollLeader() throws Exception{
		this.executeChallenge();
	}
	
	public synchronized void executeChallenge() throws Exception{
		if( LOG.isDebugEnabled()){
			LOG.debug("Sending Heatbeat: " + this.serverId +", Time: " + this.coup.getCoupTime() + ", ID: " + this.coup.getRequestId());
		}
		
		// If I'm the leader ping everyone, or
		// If it has been 20 seconds since I last heard from the leader
		if( this.leader || ((this.leaderLastContact + 15000) < System.currentTimeMillis())) {
			rabbitTemplate.convertAndSend(gravityIdGeneratorExchange.getName(), "", this.coup);
		}
		
		// If I'm not the leader and we time out set myself as leader.
		if(!this.leader && ((this.leaderLastContact + 25000) < System.currentTimeMillis())){
			this.setNewLeader(this.serverId, this.startTime);
		}
	}

	public synchronized void setLeader(String leaderId) {
		this.leaderId = leaderId;
		this.leader = leaderId.equals(this.serverId);
		
		if(this.leader) {
			this.exceptionManager.raiseException("Node Promoted to Leader", this.serverId, null, true);
		} else {
			this.exceptionManager.raiseException("Node demoted", this.serverId + "demoted by" + leaderId, null, true);
		}
		
		try {
			if( leader) {
				this.timerManager.startup();
				LOG.info("Started Time Manager");
			} else {
				this.timerManager.stopAll();
				LOG.info("Stopped Time Manager");
			}
		} catch (Exception e) {
			LOG.warn("Exception setting Leader " + e.getMessage());
		}
	}

	public synchronized boolean isLeader() {
		return leader;
	}

	public long getStartTime() {
		return startTime;
	}

	public String getServerId() {
		return serverId;
	}
	
	public synchronized void  setNewLeader(String leaderId, long startTime) {
		if(!leaderId.equals(this.leaderId)) {
			LOG.info("NEW LEADER " + leaderId + " start time: " + startTime);
			this.setLeader(leaderId);
		}
		this.leaderLastContact = System.currentTimeMillis();
	}

}
