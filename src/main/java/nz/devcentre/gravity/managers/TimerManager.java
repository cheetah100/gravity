/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2015 Peter Harrison
 * (C) Copyright 2016 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.managers;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.PreDestroy;

import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.controllers.RuleCache;
import nz.devcentre.gravity.integration.IntegrationPlugin;
import nz.devcentre.gravity.integration.AbstractPollingIntegration;
import nz.devcentre.gravity.integration.IntegrationJob;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.Rule;
import nz.devcentre.gravity.model.RuleType;
import nz.devcentre.gravity.services.BoardService;

/**
 * On startup it will recurse all the boards in Gravity to find rules which have timers.
 * 
 * For each timer found it will instantiate a Timer. 
 * 
 * If a board is invalidated this should trigger update of the Timers related to the Board. 
 * This should be plumbed into the Board Invalidation in the CacheInvalidationManager.
 * 
 * When the Timer triggers it should run the ActionChain of the rule.
 * 
 * @author peter
 */

@Component
public class TimerManager {
	
	private static final Logger logger = LoggerFactory.getLogger(TimerManager.class);
	
	public static String INTEGRATION = "gravity_integration";
	
	@Autowired
	private RabbitTemplate template;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private RuleCache ruleCache;
	
	@Autowired
	FanoutExchange gravityEventsExchange;
	
	@Autowired
	IntegrationManager integrationManager;
	
	@Autowired
	BoardService boardTools;
	
	private Scheduler scheduler;

	public void startup() throws Exception {
		
		logger.info("Starting Board Timers - init");
		if(this.scheduler!=null){
			return;
		}
					
		this.scheduler = StdSchedulerFactory.getDefaultScheduler();
		logger.info("Starting Board Timers");
		for( String boardId : this.boardsCache.list().keySet()){
			this.boardTools.checkIndexesForBoard(boardId);
			loadTimersForBoard(boardId);
		}
			
		this.scheduler.start();
		logger.info("Timers Started");
		
		this.integrationManager.startup();
		logger.info("Notifications Started");
	}
		
	public void loadTimersForBoard(String boardId) throws Exception{

		if(this.scheduler==null){
			return;
		}
		
		Map<String, String> rules;
		try {
			rules = ruleCache.list(boardId);
		} catch (Exception e ){
			logger.info("No Rule for Board: "+ boardId);
			return;
		}
		
		// Start Timers		
		for( String ruleId : rules.keySet()){
			try {
				Rule rule = ruleCache.getItem(boardId,ruleId);
				activateRule(rule);
			} catch( Exception e) {
				logger.warn("Exception Starting Timer for : "+ boardId + "." + ruleId, e);
			}
		}
	}
	
	public void disactivateRule( Rule rule ) throws Exception{
		if( null==this.scheduler || null==rule) {
			return;
		}

		logger.info("Timer " + rule.getBoardId() + "." + rule.getId() + " stopping ");
		this.scheduler.deleteJob(new JobKey(rule.getId(), rule.getBoardId()));
	}
		
	public void activateRule( Rule rule ) throws Exception{
		if( null==this.scheduler || null==rule || rule.getRuleType()!=RuleType.SCHEDULED) {
			return;
		}

		logger.info("Rule Schedule " + rule.getBoardId() + "." + rule.getId() + " stopping");
		this.scheduler.deleteJob(new JobKey(rule.getId(), rule.getBoardId()));
		if( null==rule.getSchedule()) {	
			return;
		}
		
		JobDataMap jdm = new JobDataMap();
		jdm.put("board", rule.getBoardId());
		jdm.put("rule", rule.getId());
		jdm.put("exchange", this.gravityEventsExchange.getName());
		jdm.put("template", this.template);
		
		JobDetail job = JobBuilder.newJob().ofType(TimerExecutionJob.class)
	      .withIdentity(new JobKey(rule.getId(), rule.getBoardId()))
	      .withDescription(rule.getName())
	      .usingJobData(jdm)
	      .build();
		
		Trigger trigger = getTrigger( rule.getId(), rule.getBoardId(), rule.getSchedule());
		this.scheduler.scheduleJob(job, trigger);
		logger.info("Timer " + rule.getBoardId() + "." + rule.getId() + " activated with schedule: " + rule.getSchedule());
	}
	
	public void activateIntegration( Integration integration, IntegrationPlugin plugin) throws Exception {

		if( null==this.scheduler || null==integration) {
			return;
		}
		
		if( !(plugin instanceof AbstractPollingIntegration)) {
			logger.warn("Schedule configured with non polling plugin");
			return;
		}

		ConfigContainer config = new ConfigContainer(integration.getConfig());
		String schedule = config.getStringValue("schedule");
		
		logger.info("Polling Integration " + integration.getId() + " stopping");
		this.scheduler.deleteJob(new JobKey(integration.getId(), INTEGRATION));
		if( schedule==null) {
			return;
		}
		
		JobDataMap jdm = new JobDataMap();
		jdm.put("integration", integration);
		jdm.put("plugin", plugin);
		
		JobDetail jobDetail = JobBuilder.newJob(IntegrationJob.class)
			    .withIdentity(integration.getId(), INTEGRATION).setJobData(jdm).build();
		
		Trigger trigger = getTrigger(integration.getId(), INTEGRATION, schedule);
		this.scheduler.scheduleJob(jobDetail, trigger);
		logger.info("Integration Timer " + integration.getId() + " activated with schedule: " + schedule);
	}
	
	public void disactivateIntegration( String integrationId ) throws Exception{
		if( null==this.scheduler || null==integrationId) {
			return;
		}
		if (INTEGRATION.equals((String)getTimers().get(integrationId))){
			logger.info("Timer " + integrationId + "." + INTEGRATION + " stopping ");
			this.scheduler.deleteJob(new JobKey(integrationId, INTEGRATION));
		}
	}
	
	public Map<String,String> getTimers() throws Exception {
		
		Map<String,String> returnMap = new TreeMap<String,String>();
		if(this.scheduler==null) {
			return returnMap;
		}
		
		List<String> jobGroups = this.scheduler.getJobGroupNames();
		
		for( String group : jobGroups) {
			Set<JobKey> keys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(group));
			
			for( JobKey key: keys) {
				returnMap.put(key.getName(), key.getGroup());	
			}	
		}
		return returnMap;
	}
	
	public boolean isStarted() throws Exception {
		if( this.scheduler==null) {
			return false;
		}
		return this.scheduler.isStarted();
	}
		
	private CronTrigger getTrigger(String name, String group, String cronExpression) throws ParseException{
		CronTriggerImpl trigger = new CronTriggerImpl();
		trigger.setCronExpression(cronExpression);
		trigger.setName(name);
		trigger.setGroup(group);
		return trigger;
	}
	
	/**
	 * Stop All Active Managed Timers
	 * @throws SchedulerException 
	 */
	@PreDestroy
	public void stopAll() throws SchedulerException{
		if(this.scheduler!=null){
			this.scheduler.shutdown();
			this.scheduler = null;
		}
	}
}
