/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;

import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.security.SecurityTool;

@Component
public class MongoTemplateManager {
	
	private static final Logger logger = LoggerFactory.getLogger(MongoTemplateManager.class);
	
	@Autowired
	private SecurityTool securityTool;
	
	Map<String,MongoTemplate> templates = new ConcurrentHashMap<String, MongoTemplate>();
	
	public MongoTemplate getTemplate(String credentialId) throws Exception{
				
		if( credentialId==null ) throw new Exception("Invalid Config");
		
		if(templates.containsKey(credentialId)) {
			return templates.get(credentialId);
		}
		
		Credential 	credential = this.securityTool.getSecureCredential(credentialId);
		if( credential==null) {
			throw new ResourceNotFoundException("Credential Not Found: " + credentialId);
		}
		
		String serverNames = credential.getResponseFields().get("servers");
		String port = credential.getResponseFields().get("port");
		boolean ssl = "true".equals(credential.getResponseFields().get("ssl"));
		
		if( logger.isDebugEnabled()) {
			logger.debug( "URL: " + serverNames + " Port: " + port);
			logger.debug("username/password: " + credential.getIdentifier() + " / " + credential.getSecret());
		}
		
		MongoCredential mongoCredential = 
				MongoCredential.createCredential(
						credential.getIdentifier(), 
						credential.getResource(), 
						credential.getSecret().toCharArray());
		
		List<MongoCredential> credentialList = new ArrayList<MongoCredential>();
		credentialList.add(mongoCredential);
		
		List<String> serverList = Arrays.asList(serverNames.split(","));
		
		List<ServerAddress> serverAddressList = new ArrayList<ServerAddress>();
		for( String server : serverList) {
			serverAddressList.add(new ServerAddress(server, Integer.parseInt(port)));
		}
		
		MongoClientOptions options = MongoClientOptions.builder()
	            .readPreference(ReadPreference.primaryPreferred())
	            .retryWrites(true)
	            .maxConnectionIdleTime(6000)
	            .sslEnabled(ssl)
	            .build();
		
		MongoClient client = new MongoClient(serverAddressList, credentialList, options);
		MongoDbFactory factory = new SimpleMongoDbFactory( client, credential.getResource());
		MongoTemplate mongoTemplate = new MongoTemplate(factory);
		
		templates.put(credential.getId(), mongoTemplate);
		return mongoTemplate;
	}
	
	public Map<String,MongoTemplate> getCachedTemplates() {
		return this.templates;
	}

	public void releaseTemplate( String templateId) {		
		this.templates.remove(templateId);
	}
	
}
