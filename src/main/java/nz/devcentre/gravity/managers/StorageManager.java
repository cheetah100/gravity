/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.controllers.BoardsCache;
import nz.devcentre.gravity.model.Board;
import nz.devcentre.gravity.model.BoardType;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.DatabaseDriver;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.storage.StorageInstance;
import nz.devcentre.gravity.storage.StoragePlugin;

@Component
public class StorageManager {
	
	private static final Logger logger = LoggerFactory.getLogger(StorageManager.class);
	
	private static final String DEFAULT_STORAGE = "mongo_storage";
	
	@Autowired
	private Map<String, StoragePlugin> plugins;
	
	@Autowired
	private BoardsCache boardsCache;
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private MongoTemplateManager mongoTemplateManager;
	
	@Autowired
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private Map<String, StorageInstance> credentialMapping = new ConcurrentHashMap<>();
	
	private Map<String, StorageInstance> boardMapping = new ConcurrentHashMap<>();
	
	private StorageInstance defaultStorageInstance;
		
	@PostConstruct
	private void init() {
		this.defaultStorageInstance = 
				new StorageInstance( this.plugins.get(DEFAULT_STORAGE), mongoTemplate); 
	}
	
	public StoragePlugin getPlugins(String id){
		return this.plugins.get(id);
	}
	
	public StorageInstance getStorageForBoard( String boardId ) {
		// first try the mapping
		if( boardMapping.containsKey(boardId)) {
			return boardMapping.get(boardId);
		}
		
		try {
			// If the board is type REMOTE and has a credential
			Board board = boardsCache.getItem(boardId);
			if( BoardType.REMOTE.equals(board.getBoardType())) {
				if( credentialMapping.containsKey(board.getCredentialId())) {
					StorageInstance storageInstance = credentialMapping.get(board.getCredentialId());
					boardMapping.put(boardId, storageInstance);
					return storageInstance;
				}
				// Need new Storeage Instance
				Credential credential = securityTool.getSecureCredential(board.getCredentialId());
				DatabaseDriver driver = DatabaseDriver.fromUrl(credential.getResource());
				if( driver==null) {
					logger.warn("No Driver for " + credential.getResource());
					throw new Exception("No Driver for " + credential.getResource());
				} else {
					logger.info("Database Driver for " + credential.getResource() + " = " + driver.name() + " " + driver.getStoragePlugin());
				}
				
				StoragePlugin plugin = this.getPlugins(driver.getStoragePlugin());
				if( plugin!=null) {
					logger.info("Storage Plugin" + plugin.getClass().getName());
				} else {
					logger.warn("No Plugin Found");
					throw new Exception("No Plugin Found");
				}
				
				StorageInstance storageInstance = null;
				if( DatabaseDriver.MONGO.equals(driver)) {
					// Get Mongo Template
					MongoTemplate template = mongoTemplateManager.getTemplate(board.getCredentialId());
					storageInstance = new StorageInstance(plugin, template);
				} else {
					// Get Jdbc Template
					JdbcTemplate template = jdbcTemplateManager.getTemplate(board.getCredentialId());
					storageInstance = new StorageInstance(plugin, template);
				}
				credentialMapping.put(board.getCredentialId(), storageInstance);
				boardMapping.put(boardId, storageInstance);
				return storageInstance;
			} else {
				boardMapping.put(boardId, defaultStorageInstance);
				return defaultStorageInstance;
			}
		} catch (Exception e) {
			logger.error("Exception Loading Storage", e);
		}
		
		return defaultStorageInstance;
	}
}
