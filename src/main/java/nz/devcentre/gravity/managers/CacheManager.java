/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2017 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.managers;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nz.devcentre.gravity.automation.CacheInvalidationInterface;
import nz.devcentre.gravity.controllers.Cache;
import nz.devcentre.gravity.exceptions.ResourceNotFoundException;
import nz.devcentre.gravity.model.CacheInvalidationInstruction;
import nz.devcentre.gravity.tools.AbstractResolver;

@Service
public class CacheManager extends AbstractResolver<Cache<?>> implements MessageListener, CacheInvalidationInterface {

	private static final Logger logger = LoggerFactory.getLogger(CacheManager.class);
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@Autowired
	private FanoutExchange gravityCacheInvalidationExchange;
	
	@RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = "#{gravityCacheInvalidationQueue.name}")
	public void onMessage(Message message) {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(message.getBody());
			ObjectInputStream ois = new ObjectInputStream(bis);
			
			CacheInvalidationInstruction instruction  = 
					(CacheInvalidationInstruction)ois.readObject();
			
			if( logger.isDebugEnabled()){
				logger.debug( "Cache Invalidation Instruction Received : " 
						+ instruction.getCacheType() + " " + Arrays.toString(instruction.getIds()));
			}
			
			actualInvalidate(instruction.getCacheType().toString(), instruction.getIds());
		}
		catch (ResourceNotFoundException ignore) {
			// This is required since, during Test executions at the time of builds some stale 
			// messages are received from the queues 
			logger.warn("ResourceNotFoundException on Cache Invalidation Reception ignored");
		}
		catch (Exception e) {
			logger.error("JMS Exception on Cache Invalidation Reception ",e);
		}
	}
	
	private void actualInvalidate( String type, String... ids) {
		Cache<?> cache = getRegistered(type);
		if(cache != null) {
			cache.invalidate(ids);
		} else {
			logger.warn("Cache: No Registered Cache Type : " + type);
		}
	}

	@Override
	public void invalidate( String type, String... ids){
		this.actualInvalidate( type, ids);
		CacheInvalidationInstruction instruction = 
			new CacheInvalidationInstruction(type, ids);
		rabbitTemplate.convertAndSend(gravityCacheInvalidationExchange.getName(),"",instruction);
		if( logger.isDebugEnabled()){
			logger.debug("Invalidation Message Sent to Queue: " + type);
		}
	}
}
