/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.model.Credential;


@Component
public class LdapTemplateManager {
	
	private static final Logger logger = LoggerFactory.getLogger(LdapTemplateManager.class);
	
	
	private Map<String, LdapContextSource> contextSources = new ConcurrentHashMap<String, LdapContextSource>();
	
	public LdapContextSource contextSource(Credential credential) {
		
		LdapContextSource contextSource = null;
		if(credential!=null) {
			
			if(contextSources.containsKey(credential.getId())) {
				return contextSources.get(credential.getId());
			}
			
			contextSource = new LdapContextSource();
			contextSource.setUrl(credential.getResource());
			contextSource.setBase(credential.getRequestMethod());
			contextSource.setUserDn(credential.getIdentifier());
			contextSource.setPassword(credential.getSecret());
			contextSource.afterPropertiesSet();
			logger.info("LDAP: " + credential.getResource() + " , " + credential.getRequestMethod() + ", " + credential.getIdentifier() );
			
			contextSources.put(credential.getId(), contextSource);
		}
		else {
			logger.info("LDAP crdential is invalid.");
		}
		
	    return contextSource;
	}
	
}
