/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.automation.plugin.Plugin;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.tools.AnnotationTool;

@Component
public class PluginManager {
	
	@Autowired
	private Map<String, Plugin> plugins;
	
	private Map<String, PluginMetadata> metadata;
	
	public PluginManager() {
		super();
	}
	
	public PluginManager(Map<String, Plugin> plugins) {
		super();
		this.plugins = plugins;
	}
	
	@PostConstruct
	public void init() {
		if( this.metadata==null) {
			this.metadata = AnnotationTool.getMetadata(this.plugins);
		}
	}
	
	public Map<String, Plugin> getPlugins(){
		return this.plugins;
	}
	
	public Map<String, PluginMetadata> getPluginMetadata() {
		return this.metadata;
	}


}
