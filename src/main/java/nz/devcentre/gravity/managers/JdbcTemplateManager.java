/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.DatabaseDriver;
import nz.devcentre.gravity.security.SecurityTool;

@Component
public class JdbcTemplateManager {
	
	@Autowired
	private SecurityTool securityTool;
	
	Map<String,JdbcTemplate> templates = new ConcurrentHashMap<String, JdbcTemplate>();
		
	public JdbcTemplate getTemplate(String credentialId) throws Exception{
		
		// Ensure drivers are loaded.
		DatabaseDriver.values();
		if(credentialId==null) throw new Exception("Invalid Config");
		
		if(templates.containsKey(credentialId)) {
			return templates.get(credentialId);
		}
		
		Credential 	credential = this.securityTool.getSecureCredential(credentialId);

		BasicDataSource datasource = new BasicDataSource();
    	datasource.setUrl(credential.getResourceWithCreds());
    	datasource.setDriverClassName(credential.getMethod());
    	datasource.setUsername(credential.getIdentifier());
    	datasource.setPassword(credential.getSecret());

    	JdbcTemplate jdbcTemplate = new JdbcTemplate(datasource);
		
		templates.put(credential.getId(), jdbcTemplate);
		return jdbcTemplate;
	}
	
	public Map<String, JdbcTemplate> getCachedTemplates() {
		return this.templates;
	}
	
	public void releaseTemplate( String templateId) {		
		this.templates.remove(templateId);
	}

}
