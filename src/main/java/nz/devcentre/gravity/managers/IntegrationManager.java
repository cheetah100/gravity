/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import nz.devcentre.gravity.integration.IntegrationPlugin;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.transfer.PluginMetadata;
import nz.devcentre.gravity.repository.BatchRepository;
import nz.devcentre.gravity.repository.IntegrationRepository;
import nz.devcentre.gravity.tools.AnnotationTool;

/**
 * Service to Manage Integrations and connections to JMS
 * 
 * @author peterjha
 *
 */
@Controller
public class IntegrationManager {
	
	private static final Logger logger = LoggerFactory.getLogger(IntegrationManager.class);
	
	Map<String, PluginMetadata> metadata;
	
	@Autowired
	Map<String, IntegrationPlugin> plugins;
	
	@Autowired
	IntegrationRepository integrationRepository;
	
	@Autowired
	BatchRepository batchRepository;
	
	@Autowired
	TimerManager timerManager;
	
	@PostConstruct
	private void init() {
		if( this.metadata==null) {
			this.metadata = AnnotationTool.getMetadata(this.plugins);
		}
	}
		
	public void startup() throws Exception {
		logger.info("Integration Manager Starting");
		
		List<Integration> integrations = this.integrationRepository.findAll();
		for(Integration integration : integrations) {
			try {
				this.start(integration);
			} catch( Exception e) {
				logger.warn("Exception Starting Integration " + integration.getId(),e);
			}
		}
		logger.info("Integration Manager Startup Complete");
	}
	
	public void start(Integration integration) throws Exception{
		IntegrationPlugin integrationPlugin = this.plugins.get(integration.getConnector());
		if( integrationPlugin!=null) {
			logger.info("Integration Plugin starting " + integration.getId() + " " + integration.getConnector());
			integrationPlugin.start(integration);
		} else {
			logger.warn("No Plugin Found for " + integration.getId() + " " + integration.getConnector());
		}
	}
	
	public void stop(String integrationId) throws Exception{
		this.timerManager.disactivateIntegration(integrationId);			
	}	
			
	public void execute(Integration integration) throws Exception{
		IntegrationPlugin integrationPlugin = this.plugins.get(integration.getConnector());
		if( integrationPlugin!=null) {
			logger.info("Integration Plugin executing " + integration.getId() + " " + integration.getConnector());
			Thread notificationThread = new Thread(new Runnable() {
			    @Override
			    public void run() {
			    	try {
						integrationPlugin.execute(integration);
					} catch (Exception e) {
						logger.warn("Exception running Integration " + integration.getId(), e);
					}
			    }
			});  
			notificationThread.start();
		} else {
			logger.warn("No Plugin Found for " + integration.getId() + " " + integration.getConnector());
		}
	}
	
	public Map<String, IntegrationPlugin> getPlugins(){
		return this.plugins;
	}
	
	public Map<String, PluginMetadata> getMetadata(){
		return this.metadata;
	}
	
	public void shutdown() {
		for( IntegrationPlugin plugin: this.plugins.values()) {
			plugin.shutdown();
		}
	}
}
