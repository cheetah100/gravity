/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.managers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import nz.devcentre.gravity.config.SystemConfiguration;
import nz.devcentre.gravity.model.ExceptionEvent;
import nz.devcentre.gravity.repository.ExceptionRepository;
import nz.devcentre.gravity.services.EmailService;

@Controller
public class ExceptionManager {
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionManager.class);

	@Value("${exception_email_to:}")
	private String email_to;
	
	@Value("${exception_email_from:}")
	private String email_from;
	
	@Autowired
	private ExceptionRepository exceptionRepository;
	
	@Autowired
	private EmailService emailSender;
	
	@Autowired
	private SystemConfiguration systemConfirguration;
	
	public void raiseException( String name, String description, String body, boolean email) {
		ExceptionEvent ee = new ExceptionEvent();
		ee.setActive(true);
		ee.setDescription(description);
		ee.setName(name);
		ee.setBody(body);
		ee.setActive(true);
		ee.setEnvironment(this.systemConfirguration.getEnvironment() + " build " + this.systemConfirguration.getBuild());
		ee.addOrUpdate();
		ee.setId(null);
		exceptionRepository.insert(ee);
		logger.warn(name + ": " + description);
		
		if( email && StringUtils.isNotEmpty(this.getEmail_to()) 
				&& StringUtils.isNotEmpty(this.getEmail_from())) {
			
			try {
				this.emailSender.sendEmail(ee.getName(), 
						ee.getDescription() + "\n\n\n" + ee.getBody(),
						this.getEmail_to(), 
						null, getEmail_from(), 
						getEmail_from(), 
						false, 
						null, 
						null);
				
			} catch (Exception e) {
				logger.error("Messaging Exception sending Exception to Admin : " + ee.getName() + " " + ee.getDescription());
			}
		}
		
	}
	
	public List<ExceptionEvent> getActiveExceptions(){	
		return this.exceptionRepository.findByActive(true);
	}
	
	public List<ExceptionEvent> getAllExceptions(){	
		return this.exceptionRepository.findAll();
	}
	
	public void dismissExceptionEvents() throws Exception {
		List<ExceptionEvent> active = this.exceptionRepository.findByActive(true);
		for (ExceptionEvent e : active) {
			e.setActive(false);
		}
		this.exceptionRepository.save(active);
	}

	
	public String getEmail_to() {
		return email_to;
	}

	public void setEmail_to(String email_to) {
		this.email_to = email_to;
	}

	public String getEmail_from() {
		return email_from;
	}

	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}

	
}
