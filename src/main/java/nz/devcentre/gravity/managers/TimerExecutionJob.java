/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2018 Cisco
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.managers;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import nz.devcentre.gravity.model.BoardRule;

/**
 * This is the execution task that calls the Automation Engine to execute a ActionChain
 * 
 * @author peter
 */
public class TimerExecutionJob implements Job{
	
	/**
	 * Sends Notification Message on Event Queue to execute Rule.
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try{
			
			JobDataMap data = context.getJobDetail().getJobDataMap();
			String boardId = (String) data.get("board");
			String ruleId = (String) data.get("rule");
			String exchange = (String) data.get("exchange"); 
			RabbitTemplate template = (RabbitTemplate) data.get("template");

			if( boardId==null || ruleId==null || exchange==null || template==null){
				throw new Exception("Timer Execution Fields Not Found");
			}
						
			BoardRule boardRule = new BoardRule(boardId,ruleId);
			template.convertAndSend(exchange, "", boardRule);
		} catch( Exception e){
			throw new JobExecutionException(e);
		}
	}
}
