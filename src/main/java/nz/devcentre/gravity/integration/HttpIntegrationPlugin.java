/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.controllers.ResourceCache;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.services.HttpCallService;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.tools.CallResponse;

@Component("polling_http")
@ConfigurationDetail( name="Polling HTTP Call", 
	description = "")
@ConfigurationField( field = "url", name = "URL to call")
@ConfigurationField( field = "date_format", name = "Date Format if {date} is used in url.")
@ConfigurationField( field = "date_diff", name = "Date difference in seconds.")
@ConfigurationField( field = "records", name = "Number of records for each call. Insert {limit} into url.")
@ConfigurationField( field = "total_field", name = "Name of field that specifies Total Records. Insert {offset} into url.")
@ConfigurationField( field = "type", name = "Method Type")
@ConfigurationField( field = "board", name = "Board containing Request Body Resource", type = ConfigurationFieldType.BOARD)
@ConfigurationField( field = "resource", name = "Request Body Resource", type = ConfigurationFieldType.RESOURCE)
@ConfigurationField( field = "credential", name = "Credential", type = ConfigurationFieldType.CREDENTIAL)
@ConfigurationField( field = "list", name = "Location of Dataset")
@ConfigurationField( field = "fields", name = "List of fields for list data", type = ConfigurationFieldType.LIST)
@ConfigurationField( field = "properties", name = "HTTP Parameters", type = ConfigurationFieldType.LIST)
@ConfigurationField( field = "headers", name = "Additional HTTP Headers", type = ConfigurationFieldType.LIST)
@ConfigurationField( field = "retries", name = "Number of retries", type = ConfigurationFieldType.NUMBER)
@ConfigurationField( field = "waitperiod", name = "Wait Period in seconds between retries", type = ConfigurationFieldType.NUMBER)
@ConfigurationField( field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
@ConfigurationGroup( group = "polling")
public class HttpIntegrationPlugin extends AbstractPollingIntegration{

	private static final Logger logger = LoggerFactory.getLogger(HttpIntegrationPlugin.class);

	@Autowired
	private HttpCallService httpCallService;
	
	@Autowired
	private ResourceCache resourceCache;
	
	@Autowired
	private IntegrationService integrationService;

	@Override
	public void execute(Integration integration) throws Exception {
	
		ConfigContainer config = new ConfigContainer(integration.getConfig());
		
		int retries = config.getIntValue("retries");
		int waitPeriod = config.getIntValue("waitperiod");
		
		//URL To Call
		String url = config.getStringValue("url");
		if (StringUtils.isEmpty(url)) {
			throw new Exception("Invalid Config - Missing Request URL");
		}
		
		int recordsToGet = config.getIntValue("records");
		if( recordsToGet==0) {
			recordsToGet = 100;
		}
					
		Batch batch = this.integrationService.createBatch(integration, recordsToGet, new Date() );
		
		if( url.contains("{date}") && config.contains("date_format")) {
			String dateFormat = config.getStringValue("date_format");
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			int dateDiff = config.getIntValue("date_diff");
			Date date = new Date( System.currentTimeMillis()+(dateDiff*1000));
			String dateStr = sdf.format(date);
			url = url.replace("{date}", dateStr);
		}
		
		// Check the URL for replacements.
		if( url.contains("{offset}") && url.contains("{limit}")) {
			
			if(!config.contains("total_field")){
				logger.error("Invalid Configuration - Missing Total Field");
				throw new Exception("Invalid Config - Missing Total Field");
			}
			
			int offset = 0;
			int totalRecords = 1;
			
			while( offset < totalRecords ) {
				String currentUrl = url.replaceFirst("\\{limit\\}", Integer.toString(recordsToGet));
				currentUrl = currentUrl.replaceFirst("\\{offset\\}", Integer.toString(offset));
				totalRecords = processSingleUrl( currentUrl, integration, config, batch, retries, waitPeriod).getTotalRecords();				
				offset = offset + recordsToGet;
			}
		} else if( url.contains("{page}") ) {
			int page = 1;
			while( true ) {
				String currentUrl = url.replaceFirst("\\{limit\\}", Integer.toString(recordsToGet));
				currentUrl = currentUrl.replaceFirst("\\{page\\}", Integer.toString(page));
				try {
					processSingleUrl( currentUrl, integration, config, batch, retries, waitPeriod);
				} catch( IOException  e) {
					break;
				}
				page++;
			}
		} else if( url.contains("{page_link}")) {
			String currentUrl = url.replaceFirst("\\{page_link\\}", "");
			while( true ) {
				try {
					CallResponse response = processSingleUrl( currentUrl, integration, config, batch, retries, waitPeriod);
					List<String> links = response.getHeaders().get("Link");
					if(links!=null && links.size()>0) {
						currentUrl = getLink( links.get(0), "next");
						logger.info("New Link: " + currentUrl);
					} else {
						currentUrl = null;
					}
											
					if( currentUrl==null) {
						break;
					}
				} catch( IOException  e) {
					break;
				}
			}
		} else {
			processSingleUrl( url, integration, config, batch, retries, waitPeriod);
		}
	}
	
	private String getLink(String link, String type) {
		// Begin by splitting the String
		String[] split = link.split(",");
		List<String> splitList = Arrays.asList(split);
		
		// Search For
		String search = "rel=\"" + type + "\"";
		
		for( String s: splitList) {
			if(s.contains(search)) {
				int start = s.indexOf("<")+1;
				int end = s.indexOf(">");
				return s.substring(start, end);
			}
		}
		return null;
	}
	
	private CallResponse processSingleUrl( String url,
			Integration integration,
			ConfigContainer config,
			Batch batch,
			int retries,
			int waitPeriod) throws Exception {
		
		CallResponse response = null;
		Map<String,Object> params = config.getObjectMapValue("properties");
		Map<String,String> headers = config.getStringMapValue("headers");
		
		String method = config.getStringValue("type");
		if (StringUtils.isEmpty(method)) {
			method = "GET";
		}
		
		List<String> fields = config.getListValue("fields");
		
		String board = config.getStringValue("board");
		String resource = config.getStringValue("resource");
		String requestBody = null;
		
		if( board!=null && resource!=null) {
			requestBody = this.resourceCache.getItem(board, resource);
		}
		
		response = 
				this.httpCallService.execute(requestBody, params, url, method,
						headers, config.getStringValue("credential"), retries, waitPeriod);
		
		String responseBody = response.getBody();
		
		Gson gson = new Gson();
		Object parsedJson = gson.fromJson(responseBody, Object.class);
		
		Collection<Map<String,Object>> dataList = null;
		int totalRecords = 0;
		
		if( parsedJson instanceof Map) {
			Map map = (Map) parsedJson;
			Map<String,Object> data = new HashMap<String,Object>();
			for( Object key : map.keySet()  ) {
				if( key instanceof String) {
					Object object = map.get(key);
					data.put((String)key, object);
				}
			}
			dataList = getListByReference( config.getStringValue("list"), data);
			Object totalRecordsObject = data.get(config.getStringValue("total_field"));
			if( totalRecordsObject != null) {
				if(totalRecordsObject instanceof String) {
					totalRecords = Integer.parseInt((String)totalRecordsObject);
				} else if(totalRecordsObject instanceof Double) {
					totalRecords = ((Double) totalRecordsObject).intValue(); 
				}
			}	
			
		} else if( parsedJson instanceof List) {
			dataList = (List) parsedJson;
			totalRecords = dataList.size();
		} else {
			response.setTotalRecords(0);
			return response;	
		}
		
		for( Object obj : dataList) {
			if( obj instanceof Map) {
				
				try {
					this.integrationService.execute((Map<String,Object>)obj, integration, batch);
				} catch (Exception e) {
					logger.warn("Notification Execution Failed: ",e);
				}
				
			} else if( obj instanceof List && fields != null) {
				
				try {
					List<Object> list = (List) obj;
					Map<String,Object> record = new HashMap<String,Object>();
					
					if( fields.size() >= list.size()) {
						int f = 0;
						for( Object o : list) {
							record.put(fields.get(f), o);
							f++;
						}
						this.integrationService.execute(record, integration, batch);
					} else {
						logger.warn("Input data size mismatch with supplied fields." + fields.size() + " <= " + list.size());
					}
					
				} catch (Exception e) {
					logger.warn("Notification Execution Failed: ",e);
				}	
			}	
		}
		
		response.setTotalRecords(totalRecords);		
		return response;
	}
	
	protected Collection getListByReference( String reference, Map<String, Object> data) {
		
		String[] references = StringUtils.split(reference,"."); 
		List<String> referenceList = Arrays.asList(references);
		
		Map<String, Object> thing = data;
		List<Object>list = null;
		
		for( String item : referenceList) {
			Object itemObject = thing.get(item);
			if( itemObject instanceof Map) {
				thing = (Map) itemObject;
			} else if(itemObject instanceof List) {
				list = (List) itemObject;
				Object innerObj = list.get(0);
				if( innerObj instanceof Map) {
					thing = (Map) innerObj;
				} else {
					return list;
				}
			}
		}
		
		if( list!=null) {
			return list;
		}
		return thing.values();
	}

}
