/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.managers.LdapTemplateManager;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.services.LdapService;

/**
 * Notification Plugin for external Mongo databases from which records must be
 * pulled. ConnectionPool is established for each database. A periodic Timer is
 * configured to run the query job periodically and send the messages to the
 * action chain.
 * 
 * @author ypirta
 */
@Component("polling_ldap")
@ConfigurationDetail(name = "Polling Ldap Database", description = "LDAP Database Integration")
@ConfigurationField(field = "collection", name = "Collection")
@ConfigurationField(field = "credential", name = "Credential", type = ConfigurationFieldType.CREDENTIAL)
@ConfigurationField(field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
@ConfigurationGroup(group = "polling")
public class LdapIntegrationPlugin implements IntegrationPlugin {

	private static final Logger logger = LoggerFactory.getLogger(LdapIntegrationPlugin.class);

	@Autowired
	private SecurityTool securityTool;

	@Autowired
	private TimerManager timerManager;

	@Autowired
	private LdapTemplateManager ldapTemplateManager;

	@Autowired
	private LdapService ldapService;
	
	@Autowired
	private IntegrationService integrationService;

	@Override
	public void execute(Integration integration) throws Exception {
	
			securityTool.iAmSystem();
			
			ConfigContainer config = new ConfigContainer(integration.getConfig());
			
			String collectionQuery = config.getStringValue("collection");
			if (collectionQuery == null || "".equals(collectionQuery.trim())) {
				logger.error("LDAP Query for integration can not be empty=' " + collectionQuery);
				throw new Exception("LDAP Query for integration is empty.");
			}
			
			String credentialId = config.getStringValue("credential");
			if( credentialId==null ) {
				throw new Exception("Invalid Config");
			}
			
			Credential credential = securityTool.getSecureCredential(credentialId);
			LdapContextSource contextSource = ldapTemplateManager.contextSource(credential);
			List<Map<String, Object>> ldapResultList= ldapService.getAllUsers(collectionQuery, contextSource);
			Iterator<Map<String, Object>> ldapResultListIterator = ldapResultList.iterator();
			Batch batch = this.integrationService.createBatch(integration, ldapResultList.size(), new Date());
					
			while (ldapResultListIterator.hasNext()) { 
				Map<String, Object> context = ldapResultListIterator.next();  
				context.put("_id", context.get("cn"));
				this.integrationService.execute(context, integration, batch);
			}
					 
			logger.info("LDAP Integration execution is Complete");
	}

	@Override
	public void start(Integration integration) throws Exception {
		logger.info("Starting Integration " + integration.getId());
		timerManager.activateIntegration(integration, this);
	}

	@Override
	public void shutdown() {
	}
}
