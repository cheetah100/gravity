/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.model.User;
import nz.devcentre.gravity.security.SecurityTool;

@Component("sftp")
@ConfigurationDetail( name="Secure FTP Server", 
	description = "SFTP Service to receive data")
@ConfigurationField( field = "credential", name = "Credential", type=ConfigurationFieldType.CREDENTIAL)
public class FtpServerIntegrationPlugin implements IntegrationPlugin {
	
	private static final Logger logger = LoggerFactory.getLogger(AmqpIntegrationPlugin.class);
	
	@Autowired
	SecurityTool securityTool;
	
	SshServer sshd;

	@Override
	public void start(Integration integration) throws Exception {
		
		this.startSshdServer();

	}
	
	private void startSshdServer() throws IOException{
		if(sshd==null) {
			SshServer sshd = SshServer.setUpDefaultServer();
			sshd.setPort(2222);
			sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(new File("host.ser")));
			sshd.setSubsystemFactories(Collections.singletonList(new SftpSubsystemFactory()));
			sshd.setPasswordAuthenticator((username, password, session) -> {
				try {
					User user = this.securityTool.login(username, password);
					if(user!=null) {
						return true;
					} 
					return false;
				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			} );
			sshd.start();
			logger.info("SFTP server started");
		}
		
	}

	@Override
	public void shutdown() {
		if(sshd!=null) {
			try {
				sshd.stop();
			} catch( Exception e) {
				logger.warn("SFTP failed to stop cleanly");
			}
		}

	}

	@Override
	public void execute(Integration integration) throws Exception {
		
	}

}
