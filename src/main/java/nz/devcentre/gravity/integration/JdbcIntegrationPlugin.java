/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.controllers.ResourceController;
import nz.devcentre.gravity.managers.JdbcTemplateManager;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;

@Component("polling_jdbc")
@ConfigurationDetail( name="Polling SQL Database", 
	description = "")
@ConfigurationField( field = "board", name = "Board containing Query Resource", type = ConfigurationFieldType.BOARD)
@ConfigurationField( field = "resource", name = "Query Resource ", type = ConfigurationFieldType.RESOURCE)
@ConfigurationField( field = "credential", name = "Credential",type = ConfigurationFieldType.CREDENTIAL)
@ConfigurationField( field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
@ConfigurationGroup( group = "polling")
public class JdbcIntegrationPlugin extends AbstractPollingIntegration {
	
	private static final Logger logger = LoggerFactory.getLogger(JdbcIntegrationPlugin.class);
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired 
	private ResourceController resourceController;
	
	@Autowired
	private JdbcTemplateManager jdbcTemplateManager;
	
	@Autowired
	private IntegrationService integrationService;

	@Override
	public void execute(Integration integration) throws Exception {
		securityTool.iAmSystem();
		
		ConfigContainer config = new ConfigContainer(integration.getConfig());
		
		String credentialId = config.getStringValue("credentialId");
		if(credentialId==null) {
			logger.warn("Credential Not Found");
			return;
		}
		
		JdbcTemplate jdbcTemplate = 
				this.jdbcTemplateManager.getTemplate(credentialId);
		
		String boardId = config.getStringValue("board");
		String resourceId = config.getStringValue("resource");
		String query = resourceController.getResource(boardId, resourceId);

		logger.info("Starting Resource Transform on " + boardId + ": " + query);
		
		JdbcCallbackHandler rse = 
				new JdbcCallbackHandler(integration,
						this.integrationService);
		
		jdbcTemplate.query(query, rse);
		
		logger.info("JDBC Integration Complete");

	}
}
