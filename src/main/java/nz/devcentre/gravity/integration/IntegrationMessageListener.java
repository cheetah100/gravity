/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.MessageProperties;

import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.services.IntegrationService;

public class IntegrationMessageListener implements MessageListener {
	
	private static final Logger logger = LoggerFactory.getLogger(IntegrationMessageListener.class);
	
	private Integration integration;
	
	private IntegrationService integrationService;
	
	public IntegrationMessageListener(
			Integration integration,
			IntegrationService integrationService) {
		this.integration = integration;
		this.integrationService = integrationService;
	}

	@Override
	public void onMessage(Message message) {
		MessageProperties messageProperties = message.getMessageProperties();
		Map<String, Object> headers = messageProperties.getHeaders();
		String body = new String(message.getBody());
		headers.put("body", body);		
		Batch batch = this.integrationService.createBatch(integration, 1, new Date());
		
		try {
			this.integrationService.execute(headers, integration, batch);
		} catch (Exception e) {
			logger.warn("Exception on Message Processing:" + e.getMessage() );
		}
	}

}
