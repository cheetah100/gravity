/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2022 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowCallbackHandler;

import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.services.IntegrationService;

public class JdbcCallbackHandler implements RowCallbackHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(JdbcCallbackHandler.class);
	
	private Integration integration;
	
	private IntegrationService integrationService;
	
	public JdbcCallbackHandler(Integration integration, IntegrationService integrationService) {
		this.integration = integration;
		this.integrationService = integrationService;
	}

	@Override
	public void processRow(ResultSet rs) throws SQLException {
		Map<String, Object> context = new HashMap<String, Object>();
		
		ResultSetMetaData metaData = rs.getMetaData();
		int columns = metaData.getColumnCount();
	    for(int i = 1; i <= columns; ++i){
	    	if(rs.getObject(i)!=null) {
	    		context.put(metaData.getColumnName(i), rs.getObject(i).toString());
	    	}
	    }
	    
		Batch batch = this.integrationService.createBatch(integration, 1, new Date());
		try {
			this.integrationService.execute(context, this.integration, batch);
		} catch (Exception e) {
			logger.warn("Exception in JDBC Row handler",e);
		}
	}

}
