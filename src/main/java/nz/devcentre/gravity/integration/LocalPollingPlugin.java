/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2021 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Card;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;
import nz.devcentre.gravity.services.QueryService;

@Component("polling_local")
@ConfigurationDetail( name="Polling Local Board", description = "")
@ConfigurationField( field = "board", name = "Board to query", type = ConfigurationFieldType.BOARD)
@ConfigurationField( field = "filter", name = "Name of Filter to use", type = ConfigurationFieldType.STRING)
@ConfigurationField( field = "phase", name = "Phase of Board to use", type = ConfigurationFieldType.PHASE, board = "board")
@ConfigurationField( field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
@ConfigurationGroup( group = "polling")
public class LocalPollingPlugin extends AbstractPollingIntegration {

	@Autowired
	private QueryService queryService;

	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private IntegrationService integrationService;
	
	@Override
	public void execute(Integration integration) throws Exception {
		
		securityTool.iAmSystem();
		
		ConfigContainer config = new ConfigContainer(integration.getConfig());
		
		String boardId = config.getStringValue("board");
		String filterId = config.getStringValue("filter");
		String phaseId = config.getStringValue("phase");
		Collection<Card> results = queryService.query(boardId, phaseId, filterId, null, false);
		
		Batch batch = this.integrationService.createBatch(integration, results.size(), new Date());
		
		for( Card card : results) {
			Map<String,Object> context = new HashMap<>();
			context.putAll(card.getFields());
			context.put("boardid", card.getBoard());
			context.put("phaseid", card.getPhase());
			context.put("cardid", card.getId());
			context.put("creation-date", card.getCreated());
			context.put("creator", card.getCreator());
			context.put("modified-date", card.getModified());
			context.put("modified-by", card.getModifiedby());
			this.integrationService.execute(context, integration, batch);
		}
	}
}
