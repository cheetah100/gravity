package nz.devcentre.gravity.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.managers.TimerManager;
import nz.devcentre.gravity.model.Integration;

@ConfigurationField( field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
public abstract class AbstractPollingIntegration implements IntegrationPlugin {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractPollingIntegration.class);
	
	@Autowired
	private TimerManager timerManager;

	@Override
	public void start(Integration integration) throws Exception {
		logger.info("Starting Polling Integration " + integration.getId());
		timerManager.activateIntegration(integration, this);
	}
	
	@Override
	public void shutdown() {
		
	}
}
