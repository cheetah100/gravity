/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.managers.MongoTemplateManager;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;

/**
 * Notification Plugin for external Mongo databases from which records must be pulled.
 * ConnectionPool is established for each database.
 * A periodic Timer is configured to run the query job periodically and send the 
 * messages to the action chain.
 * 
 * @author peterjha
 */
@Component("polling_mongo")
@ConfigurationDetail( name="Polling Mongo Database", 
	description = "Mongo Database Integration")
@ConfigurationField( field = "collection", name = "Collection")
@ConfigurationField( field = "max_records", name = "Maximum Records")
@ConfigurationField( field = "date_fieldname", name = "Date Fieldname")
@ConfigurationField( field = "date_format", name = "Date Format")
@ConfigurationField( field = "credential", name = "Credential", type = ConfigurationFieldType.CREDENTIAL)
@ConfigurationField( field = "schedule", name = "Schedule (Cron Expression)", type = ConfigurationFieldType.CRONEXPRESSION)
@ConfigurationGroup( group = "polling")
public class MongoIntegrationPlugin extends AbstractPollingIntegration {
	
	private static final Logger logger = LoggerFactory.getLogger(MongoIntegrationPlugin.class);
	
	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private MongoTemplateManager mongoTemplateManager;
	
	@Autowired
	private IntegrationService integrationService;
	
	@Override
	public void execute(Integration integration) throws Exception {
		
		securityTool.iAmSystem();
		
		ConfigContainer config = new ConfigContainer(integration.getConfig());

		// Get Mongo Template
		MongoTemplate mongoTemplate = this.mongoTemplateManager.getTemplate(config.getStringValue("credential"));

		// Execute Query
		String collectionName = config.getStringValue("collection");
		String dateFieldName = config.getStringValue("date_fieldname");
		String dateFormat = config.getStringValue("date_format");
		
		int maxRecords = config.getIntValue("max_records");
		if(maxRecords==0) {
			maxRecords = 1000;
		}
		
		// Get an existing Batch
		Batch latestBatch = this.integrationService.getLatestBatch(integration);
		
		logger.info("Starting Mongo Import on " + collectionName);
		
		Date processFrom = new Date(1l);
		if( latestBatch!=null && latestBatch.getMaxProcessDate()!=null) {
			processFrom = latestBatch.getMaxProcessDate();
			logger.info("Latest batch: " + latestBatch.getId() + " : " + latestBatch.getMaxProcessDate().toString());
		}
		else if (latestBatch == null) {
			latestBatch = this.integrationService.createBatch(integration, maxRecords, new Date());
		}

		DBCollection collection = mongoTemplate.getDb().getCollection(collectionName);
		String queryString = String.format("{\"%s\": { $gte: new ISODate(\"%s\")}}", dateFieldName, formatDate(processFrom,dateFormat));
		DBObject query = BasicDBObject.parse(queryString);
		String sortByString = String.format("{\"%s\":1}", dateFieldName);
		DBObject sort =  BasicDBObject.parse(sortByString);	
		DBCursor cursor = collection.find(query).sort(sort).limit(maxRecords);
		Date maxDate = null;
		int rowsImported = 0;
		
		while( cursor.hasNext()) {
			DBObject result = cursor.next();
			
			Date recordDate = (Date) result.get(dateFieldName);
			if( maxDate==null || (recordDate!=null && maxDate.before(recordDate))) {
				maxDate = recordDate;
			}
			
			Map<String, Object> context = new HashMap<String, Object>();				
			context.putAll(result.toMap());
			Object id = context.get("_id");
			if( id!=null) {
				if( !(id instanceof String)) {
					context.put("_id", id.toString());
				}
			}
			this.integrationService.execute(context, integration, latestBatch);
			rowsImported++;
		}						
		
		cursor.close();
		
		// Store the batch.
		if( maxDate!=null) {
			integrationService.createBatch(integration,rowsImported, maxDate );
		}
		
		logger.info("MongoDB Integration Complete");
	}
	
	private String formatDate( Date date, String dateFormat) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		return format.format(date);
	}

}
