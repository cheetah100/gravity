/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2015 Orcon Limited
 * (C) Copyright 2016 Peter Harrison
 * (C) Copyright 2020 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */

package nz.devcentre.gravity.integration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.model.Batch;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("polling_imap")
@ConfigurationDetail( name="Polling IMAP Email Receiver", 
	description = "Periodically gets emails from a IMAP server")
@ConfigurationField( field = "from_filter", name = "From Filter")
@ConfigurationField( field = "subject_filter", name = "Subject Filter")
@ConfigurationField( field = "processed_folder", name = "Folder name for Processed Emails")
@ConfigurationField( field = "credential", name = "Credential", type=ConfigurationFieldType.CREDENTIAL)
@ConfigurationGroup( group = "polling")
public class EmailReceiverPlugin extends AbstractPollingIntegration {

	private static final Logger logger = LoggerFactory.getLogger(EmailReceiverPlugin.class);

	@Autowired
	private SecurityTool securityTool;
	
	@Autowired
	private IntegrationService integrationService;

	@Override
	public void execute(Integration integration) throws Exception {
		
		ConfigContainer config = new ConfigContainer(integration.getConfig());
				
		String credId = config.getStringValue("credential");
		Credential credential = this.securityTool.getSecureCredential(credId);
		
		String fromFilter = config.getStringValue("from_filter");
		String subjectFilter = config.getStringValue("subject_filter");
		String processedFolder = config.getStringValue("processed_folder");
		
		this.checkEmails(credential.getMethod(), 
				credential.getResource(), 
				credential.getIdentifier(), 
				credential.getSecret(), 
				fromFilter, 
				subjectFilter, 
				processedFolder,
				integration);
		
	}

	/**
	 * @param mailStoreProtocol
	 * @param mailStoreHost
	 * @param mailStoreUserName
	 * @param mailStorePassword
	 * @return
	 */
	private Session getMailStoreSession(String mailStoreProtocol,String mailStoreHost, String mailStoreUserName,String mailStorePassword) {
		Properties properties = new Properties();
		properties.put("mail.store.protocol", mailStoreProtocol);
		properties.put("mail.store.host", mailStoreHost);
		properties.put("mail.store.username", mailStoreUserName);
		properties.put("mail.store.pasword", mailStorePassword);
		
		properties.put("mail.imaps.host", mailStoreHost);
		properties.put("mail.imaps.port", "993");
		properties.put("mail.imaps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.imaps.socketFactory.fallback", "false");
		
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailStoreUserName, mailStorePassword);
			}
		};

		return Session.getInstance(properties, auth);
	}

	public void checkEmails(String mailStoreProtocol,
			String mailStoreHost, 
			String mailStoreUserName,
			String mailStorePassword,
			String fromFilter, 
			String subjectFilter,
			String processedFolderName,
			Integration integration) throws Exception {
		
		Session session = getMailStoreSession(mailStoreProtocol, mailStoreHost,mailStoreUserName, mailStorePassword);
		try {
			// connects to the message store
			Store store = session.getStore(mailStoreProtocol);
			store.connect(mailStoreHost, mailStoreUserName, mailStorePassword);

			logger.info("connected to message store");

			// opens the inbox folder
			Folder folderInbox = store.getFolder("INBOX");
			folderInbox.open(Folder.READ_WRITE);

			// check if fromFilter is specified
			List<String> fromFilterList = null;

			if (StringUtils.isNotBlank(fromFilter)) {
				String[] fromFilterArray = StringUtils.split(fromFilter, "|");
				fromFilterList = Arrays.asList(fromFilterArray);
			}

			// check if subjectFilter is specified
			List<String> subjectFilterList = null;
			if (StringUtils.isNotBlank(subjectFilter)) {
				String[] subjectFilterArray = StringUtils.split(subjectFilter,"|");
				subjectFilterList = Arrays.asList(subjectFilterArray);
			}
			
			Map<String, Object> context = new HashMap<String, Object>();
			// fetches new messages from server
			Message[] messages = folderInbox.getMessages();
			List<Message> processedMessages = new ArrayList<Message>();
			
			Batch batch = this.integrationService.createBatch(integration, messages.length, new Date());
			
			for (int i = 0; i < messages.length; i++) {
				Message msg = messages[i];
				Address[] fromAddress = msg.getFrom();
				String from = fromAddress[0].toString();
				if(StringUtils.isBlank(from)){
					logger.warn("From Address is not proper " + from);
					return;
				}
				boolean isValidFrom = isValidMatch(fromFilterList,from);
				
				// filter based on fromFilter specified
				if (null == fromFilterList || isValidFrom) {
					String subject = msg.getSubject();
					
					boolean isValidSubject = isValidMatch(subjectFilterList, subject);					
					if (null == subjectFilterList || isValidSubject) {
						String toList = parseAddresses(msg.getRecipients(RecipientType.TO));
						String ccList = parseAddresses(msg.getRecipients(RecipientType.CC));
						String sentDate = msg.getSentDate().toString();

						String messageContent = "";
						try {
							Object content = msg.getContent();
							if (content != null) {
								messageContent = content.toString();
							}
						} catch (Exception ex) {
							messageContent = "[Error downloading content]";
							logger.trace("Error downloading email content", ex);
						}
						context.put("from", from);
						context.put("to", toList);
						context.put("cc", ccList);
						context.put("subject", subject);
						context.put("messagebody", messageContent);
						context.put("sentdate", sentDate);

						this.integrationService.execute(context, integration, batch);
						
						processedMessages.add(msg);
						
					} else {
						logger.warn("subjectFilter doesn't match");
					}
				} else {
					logger.warn("this email originated from " + from
							+ " , which does not match fromAddress specified in the rule, it should be "
							+ fromFilter.toString());
				}
			}
			
			if( processedMessages.size() > 0) {
				Message[] processedMessagesArray = new Message[processedMessages.size()];
				processedMessages.toArray(processedMessagesArray);
				if( StringUtils.isNotEmpty(processedFolderName)) {
					Folder processedFolder = folderInbox.getFolder(processedFolderName);
					if(!processedFolder.exists()) {
						logger.info("Creating folder: {}", processedFolderName);
						boolean create = processedFolder.create(Folder.HOLDS_MESSAGES);
						if(!create) {
							logger.warn("Folder creation failed: {}", processedFolderName);
						}
					}
					processedFolder.open(Folder.READ_WRITE);
					logger.info("Copying to folder: {}", processedFolderName);
					folderInbox.copyMessages(processedMessagesArray, processedFolder);
					processedFolder.close(true);
				}
				logger.info("Deleting from folder: {}", folderInbox.getFullName());
				folderInbox.setFlags(processedMessagesArray, new Flags(Flags.Flag.DELETED), true);
			}
			
			// disconnect and delete messages marked as DELETED
			folderInbox.close(true);
			store.close();
		} catch (NoSuchProviderException ex) {
			logger.warn("No provider for protocol: " + mailStoreProtocol + " " + ex);
			throw ex;
		} catch (MessagingException ex) {
			logger.error("Could not connect to the message store" + ex);
			throw ex;
		}
	}
	
	protected boolean isValidMatch(List<String> list, String fieldToMatch){
		if(list != null){
			for( String value: list) {
				if(fieldToMatch.toLowerCase().contains(value.toLowerCase())){
					return true;
				}
			}
			return false;
		}else{
			return true;
		}
	}
	
	/**
	 * Returns a list of addresses in String format separated by comma
	 * 
	 * @param address
	 *            an array of Address objects
	 * @return a string represents a list of addresses
	 */
	private String parseAddresses(Address[] address) {
		String listAddress = "";

		if (address != null) {
			for (int i = 0; i < address.length; i++) {
				listAddress += address[i].toString() + ", ";
			}
		}
		if (listAddress.length() > 1) {
			listAddress = listAddress.substring(0, listAddress.length() - 2);
		}

		return listAddress;
	}
}
