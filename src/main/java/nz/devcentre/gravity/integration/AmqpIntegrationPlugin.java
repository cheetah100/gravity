/**
 * GRAVITY WORKFLOW AUTOMATION
 * (C) Copyright 2019 Cisco
 * (C) Copyright 2021 Devcentre Limited
 * 
 * This file is part of Gravity Workflow Automation.
 *
 * Gravity Workflow Automation is free software: you can redistribute it 
 * and/or modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Gravity Workflow Automation is distributed in the hope that it will be 
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *    
 * You should have received a copy of the GNU General Public License
 * along with Gravity Workflow Automation.  
 * If not, see <http://www.gnu.org/licenses/>. 
 */
package nz.devcentre.gravity.integration;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nz.devcentre.gravity.annotations.ConfigurationDetail;
import nz.devcentre.gravity.annotations.ConfigurationField;
import nz.devcentre.gravity.annotations.ConfigurationFieldType;
import nz.devcentre.gravity.annotations.ConfigurationGroup;
import nz.devcentre.gravity.automation.plugin.ConfigContainer;
import nz.devcentre.gravity.model.Credential;
import nz.devcentre.gravity.model.Integration;
import nz.devcentre.gravity.security.SecurityTool;
import nz.devcentre.gravity.services.IntegrationService;


/**
 * This notification plugin sets up a listener on a AMQP queue and routes the incoming messages
 * it a action chain. Because the card has not been persisted the usual card related fields will
 * not be present in the context during processing.
 * 
 * @author peterjha
 */
@Component("amqp")
@ConfigurationDetail( name="JMS Listener (amqp)", 
	description = "Sets up listener to a queue on a JMS server")
@ConfigurationField( field = "credential", name = "Credential", type=ConfigurationFieldType.CREDENTIAL)
@ConfigurationField( field = "queue", name = "Queue Name")
@ConfigurationField( field = "vhost", name = "Virtual Host Name")
@ConfigurationGroup( group = "listener")
@ConfigurationGroup( group = "jms")
public class AmqpIntegrationPlugin implements IntegrationPlugin {
	
	private static final Logger logger = LoggerFactory.getLogger(AmqpIntegrationPlugin.class);
	
	@Autowired
	SecurityTool securityTool;
	
	@Autowired
	IntegrationService integrationService;
	
	Map<String,RabbitTemplate> templateMap = new HashMap<String,RabbitTemplate>();

	@Override
	public void start(Integration integration) throws Exception {
		
		ConfigContainer config = new ConfigContainer(integration.getConfig());
		
		String credentialName = config.getStringValue("credential");
		String queue = config.getStringValue("queue");
		String vhost = config.getStringValue("vhost");
		
		if( StringUtils.isEmpty(credentialName) || StringUtils.isEmpty(queue)) {
			return;
		}
		RabbitTemplate rabbitTemplate = templateMap.get(credentialName);
		
		if(rabbitTemplate==null) {
			logger.info("Creating new JMS Connection Pool for Credential " 
					+ credentialName);
			
			Credential credential = this.securityTool.getSecureCredential(credentialName);
			
			URI uri = URI.create(credential.getResource());
			CachingConnectionFactory connectionFactory = 
					new CachingConnectionFactory(uri);
			connectionFactory.setUsername(credential.getIdentifier());
			connectionFactory.setPassword(credential.getSecret());
			if( StringUtils.isNotEmpty(vhost)) {
				connectionFactory.setVirtualHost(vhost);
			}
			rabbitTemplate =  new RabbitTemplate(connectionFactory);
			rabbitTemplate.afterPropertiesSet();
			templateMap.put(credentialName, rabbitTemplate);
		}
		
		SimpleMessageListenerContainer container = 
				new SimpleMessageListenerContainer();
		
		IntegrationMessageListener messageListener = 
				new IntegrationMessageListener(integration, integrationService);
		
        container.setConnectionFactory(rabbitTemplate.getConnectionFactory());
        container.setQueueNames(queue);
        container.setMessageListener(messageListener);
        container.afterPropertiesSet();
        container.initialize();
        container.start();
        
        logger.info("Message Listener configured for " + queue);
	}

	@Override
	public void shutdown() {		
	}

	@Override
	public void execute(Integration integration) throws Exception{
	}

}
