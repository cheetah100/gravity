FROM openjdk:11
WORKDIR /app/
ADD target/gravity.jar gravity.jar
ADD keystore.jks keystore.jks
ADD application_docker.yml application.yml
EXPOSE 8080
ENTRYPOINT ["java","-jar","gravity.jar"]