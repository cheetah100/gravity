<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>nz.net.gravity</groupId>
	<artifactId>gravity</artifactId>
	<packaging>jar</packaging>
	<version>5.0.0</version>
	<name>Gravity Workflow Automation</name>
	 
	<repositories>
	</repositories>
	
	<distributionManagement>
	</distributionManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring.version>5.3.20</spring.version>
		<spring.security.version>5.7.1</spring.security.version>
		<java.version>11</java.version>
		<junit.version>4.12</junit.version>
		<camel.version>2.17.2</camel.version>
		<rabbitmq-version>2.4.6</rabbitmq-version> <!-- 2.0.2.RELEASE -->
		<fongo.version>2.2.0-RC1</fongo.version>
		<failOnMissingWebXml>false</failOnMissingWebXml>
		<spring-boot-version>2.7.2</spring-boot-version>
	</properties>

	<licenses>
		<license>
			<name>GPL, Version 3.0</name>
			<url>http://www.gnu.org/licenses/gpl-3.0.en.html</url>
			<distribution>repo</distribution>
			<comments></comments>
		</license>
	</licenses>

	<organization>
		<name>Devcentre Limited</name>
		<url>http://devcentre.nz</url>
	</organization>

	<developers>
		<developer>
			<id>cheetah100</id>
			<name>Peter Harrison</name>
			<email>cheetah100@gmail.com</email>
			<organization>Devcentre Limited</organization>
			<organizationUrl>http://devcentre.nz</organizationUrl>
			<roles>
				<role>architect</role>
				<role>developer</role>
			</roles>
			<timezone>New Zealand/Auckland</timezone>
		</developer>
	</developers>
	
	<scm>
		<connection>scm:git:https://cheetah100@bitbucket.org/cheetah100/gravity.git</connection>
		<tag>HEAD</tag>
		<url>https://bitbucket.org/cheetah100/gravity/src/master/</url>
	</scm>

	<dependencies>

		<!-- Bean Validation Dependencies -->

		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
			<version>1.1.0.Final</version>
			<scope>compile</scope>
		</dependency>


		<!-- https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator -->
		<dependency>
			<groupId>org.hibernate.validator</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>7.0.4.Final</version>
		</dependency>

		<!-- Spring Dependencies -->
		 		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web-services</artifactId>
			<version>${spring-boot-version}</version>
		</dependency>		
		
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-web</artifactId>
		    <version>${spring-boot-version}</version>
		    <exclusions>
	            <exclusion>
	            	<groupId>org.apache.logging.log4j</groupId>
	            	<artifactId>log4j-api</artifactId>
	            </exclusion>
	            <exclusion>
	            	<groupId>org.apache.logging.log4j</groupId>
	            	<artifactId>log4j-to-slf4j</artifactId>
	            </exclusion>
		    </exclusions>
		</dependency>
		 
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<version>${spring-boot-version}</version>
			<scope>test</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-security -->
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-security</artifactId>
		    <version>${spring-boot-version}</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-oauth2-client -->
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-oauth2-client</artifactId>
		    <version>2.7.2</version>
		</dependency>
		
		<dependency>
		  <groupId>org.springframework</groupId>
		  <artifactId>spring-messaging</artifactId>
		  <version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-orm</artifactId>
			<version>${spring.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk15on -->
		<dependency>
		    <groupId>org.bouncycastle</groupId>
		    <artifactId>bcprov-jdk15on</artifactId>
		    <version>1.70</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.ws</groupId>
			<artifactId>spring-ws-security</artifactId>
			<version>3.1.3</version>
		</dependency>
		

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jms</artifactId>
			<version>${spring.version}</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.springframework.ws/spring-ws-core -->
		
		<dependency>
		    <groupId>org.springframework.ws</groupId>
		    <artifactId>spring-ws-core</artifactId>
		    <version>3.1.1</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.springframework.integration/spring-integration-mail -->
		<dependency>
			<groupId>org.springframework.integration</groupId>
			<artifactId>spring-integration-mail</artifactId>
			<version>5.5.3</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-mongodb</artifactId>
			<version>1.9.0.RELEASE</version>
			<!-- <version>3.2.5</version> -->
		</dependency>		

		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib-nodep</artifactId>
			<version>2.2</version>
		</dependency>

		<!-- View Dependencies -->

		<!-- https://mvnrepository.com/artifact/org.apache.taglibs/taglibs-standard-impl -->
		<dependency>
			<groupId>org.apache.taglibs</groupId>
			<artifactId>taglibs-standard-impl</artifactId>
			<version>1.2.5</version>
			<scope>runtime</scope>
		</dependency>


		<!-- https://mvnrepository.com/artifact/org.owasp.encoder/encoder -->
		<dependency>
			<groupId>org.owasp.encoder</groupId>
			<artifactId>encoder</artifactId>
			<version>1.2.2</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.googlecode.owasp-java-html-sanitizer/owasp-java-html-sanitizer -->
		<dependency>
			<groupId>com.googlecode.owasp-java-html-sanitizer</groupId>
			<artifactId>owasp-java-html-sanitizer</artifactId>
			<version>20220608.1</version>
		</dependency>


		<!-- RabbitMQ dependencies -->
		
		<dependency>
			<groupId>org.springframework.amqp</groupId>
			<artifactId>spring-amqp</artifactId>
			<version>${rabbitmq-version}</version>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.amqp</groupId>
			<artifactId>spring-rabbit</artifactId>
			<version>${rabbitmq-version}</version>
		</dependency>
		        
        <!-- https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt-api -->
		<dependency>
		    <groupId>io.jsonwebtoken</groupId>
		    <artifactId>jjwt-api</artifactId>
		    <version>0.11.2</version>
		</dependency>
        
        
        <dependency>
            <groupId>io.jsonwebtoken</groupId>
            <artifactId>jjwt-impl</artifactId>
            <version>0.11.2</version>
            <scope>runtime</scope>
        </dependency>
        
        <dependency>
		    <groupId>io.jsonwebtoken</groupId>
		    <artifactId>jjwt-jackson</artifactId>
		    <version>0.11.2</version>
		    <scope>runtime</scope>
		</dependency>
        
		<!-- Test Dependencies -->
		 
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.9.5</version>
			<scope>test</scope>
		</dependency>
				
		<!-- Jackson JSON Mapper -->
		
		<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind --> 
		<dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.12.0</version>
        </dependency>
		
		<!-- Data Repository -->

		<dependency>
			<groupId>org.mongodb</groupId>
			<artifactId>mongo-java-driver</artifactId>
			<version>3.6.0</version>
		</dependency>

		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>jstl</artifactId>
			<version>1.2</version>
		</dependency>
		
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.0.1</version>
			<scope>provided</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/commons-beanutils/commons-beanutils -->
		<dependency>
			<groupId>commons-beanutils</groupId>
			<artifactId>commons-beanutils</artifactId>
			<version>1.9.4</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-collections4 -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-collections4</artifactId>
			<version>4.4</version>
		</dependency>

		<dependency>
			<groupId>commons-pool</groupId>
			<artifactId>commons-pool</artifactId>
			<version>1.5.5</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.4</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.11.0</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.quartz-scheduler/quartz -->
		<dependency>
		    <groupId>org.quartz-scheduler</groupId>
		    <artifactId>quartz</artifactId>
		    <version>2.3.2</version>
		</dependency>
		
		<dependency>
			<groupId>net.sf.supercsv</groupId>
			<artifactId>super-csv</artifactId>
			<version>2.1.0</version>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.5.13</version>
		</dependency>


		<!-- https://mvnrepository.com/artifact/com.thoughtworks.xstream/xstream -->
		<dependency>
			<groupId>com.thoughtworks.xstream</groupId>
			<artifactId>xstream</artifactId>
			<version>1.4.19</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.7</version>
			<scope>compile</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-text -->
		<dependency>
		    <groupId>org.apache.commons</groupId>
		    <artifactId>commons-text</artifactId>
		    <version>1.9</version>
		</dependency>


		<!-- https://mvnrepository.com/artifact/org.freemarker/freemarker -->
		<dependency>
		    <groupId>org.freemarker</groupId>
		    <artifactId>freemarker</artifactId>
		    <version>2.3.31</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.apache.velocity/velocity-engine-core -->
		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity-engine-core</artifactId>
			<version>2.3</version>
		</dependency>

		
		<dependency>
			<groupId>javax.mail</groupId>
			<artifactId>mail</artifactId>
			<version>1.4.7</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/joda-time/joda-time -->
		<dependency>
		    <groupId>joda-time</groupId>
		    <artifactId>joda-time</artifactId>
		    <version>2.10.10</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
		<dependency>
		    <groupId>com.google.code.gson</groupId>
		    <artifactId>gson</artifactId>
		    <version>2.9.0</version>
		</dependency>

		<dependency>
			<groupId>com.sun.xml.stream</groupId>
			<artifactId>sjsxp</artifactId>
			<version>1.0.1</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.reflections/reflections -->
		<dependency>
		    <groupId>org.reflections</groupId>
		    <artifactId>reflections</artifactId>
		    <version>0.9.12</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml -->
		<dependency>
		    <groupId>org.apache.poi</groupId>
		    <artifactId>poi-ooxml</artifactId>
		    <version>3.15-beta2</version>
		</dependency>

		<!-- Swagger UI Dependencies -->
		
		<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>2.9.2</version>
			<scope>compile</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>2.9.2</version>
			<scope>compile</scope>
		</dependency>
		
		<!-- GraphQL Dependendies -->

		<!-- https://mvnrepository.com/artifact/com.graphql-java/graphql-java -->
		<dependency>
		    <groupId>com.graphql-java</groupId>
		    <artifactId>graphql-java</artifactId>
		    <version>17.2</version>
		</dependency>		
		
		<!-- https://mvnrepository.com/artifact/com.graphql-java/graphql-java-tools -->
		<dependency>
		    <groupId>com.graphql-java</groupId>
		    <artifactId>graphql-java-tools</artifactId>
		    <version>5.2.4</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/com.graphql-java/graphql-java-servlet -->
		<dependency>
		    <groupId>com.graphql-java</groupId>
		    <artifactId>graphql-java-servlet</artifactId>
		    <version>6.1.3</version>
		</dependency>
		
	<!-- LDAP Dependencies -->
		    	
    	<!-- https://mvnrepository.com/artifact/org.springframework.ldap/spring-ldap-core -->
		<dependency>
		    <groupId>org.springframework.ldap</groupId>
		    <artifactId>spring-ldap-core</artifactId>
		    <version>2.3.4.RELEASE</version>
		</dependency>
    	
    	<dependency>
        	<groupId>com.unboundid</groupId>
        	<artifactId>unboundid-ldapsdk</artifactId>
        	<version>4.0.9</version>
    	</dependency>
    	

		<!-- Jython Dependencies -->    	
    	<dependency>
		    <groupId>org.python</groupId>
		    <artifactId>jython</artifactId>
		    <version>2.7.2</version>
		</dependency>
    	
		<!-- Monngo In memory database for unit testing -->


		<!-- https://mvnrepository.com/artifact/com.github.fakemongo/fongo -->
		<dependency>
			<groupId>com.github.fakemongo</groupId>
			<artifactId>fongo</artifactId>
			<version>${fongo.version}</version>
			<scope>test</scope>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/com.opencsv/opencsv -->
		<dependency>
		    <groupId>com.opencsv</groupId>
		    <artifactId>opencsv</artifactId>
		    <version>4.0</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/com.microsoft.sqlserver/mssql-jdbc -->
		<dependency>
		    <groupId>com.microsoft.sqlserver</groupId>
		    <artifactId>mssql-jdbc</artifactId>
		    <version>9.4.0.jre8</version>
		</dependency>
				
		<!-- https://mvnrepository.com/artifact/org.postgresql/postgresql -->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>42.4.0</version>
		</dependency>

		
		<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.30</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/net.snowflake/snowflake-jdbc -->
		<dependency>
		    <groupId>net.snowflake</groupId>
		    <artifactId>snowflake-jdbc</artifactId>
		    <version>3.12.8</version>
		</dependency>		
		 		
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-dbcp2</artifactId>
			<version>2.7.0</version>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.graalvm.js/js -->
		<dependency>
		    <groupId>org.graalvm.js</groupId>
		    <artifactId>js</artifactId>
		    <version>21.3.0</version>
		</dependency>
		
		<!-- FTP Server Dependencies -->
		
		<!-- https://mvnrepository.com/artifact/org.apache.mina/mina-core -->
		<dependency>
			<groupId>org.apache.mina</groupId>
			<artifactId>mina-core</artifactId>
			<version>1.1.7</version>
		</dependency>
		
		<dependency>
		   <groupId>org.apache.sshd</groupId>
		   <artifactId>sshd-core</artifactId>
		   <version>1.7.0</version>
		</dependency>
				
		<!-- https://mvnrepository.com/artifact/com.microsoft.graph/microsoft-graph -->
		<dependency>
		    <groupId>com.microsoft.graph</groupId>
		    <artifactId>microsoft-graph</artifactId>
		    <version>5.25.0</version>
		</dependency>
		
		<dependency>
		    <groupId>com.fasterxml.jackson.core</groupId>
		    <artifactId>jackson-core</artifactId>
		    <version>2.12.5</version>
		</dependency>

		<dependency>
		    <groupId>com.fasterxml.jackson.dataformat</groupId>
		    <artifactId>jackson-dataformat-xml</artifactId>
		    <version>2.12.5</version>
		</dependency>
		
		<dependency>
		    <groupId>com.fasterxml.jackson.core</groupId>
		    <artifactId>jackson-annotations</artifactId>
		    <version>2.12.5</version>
		</dependency>
				
	</dependencies>

	<build>
		<finalName>gravity</finalName>
		<plugins>
			<!-- Facilitates downloading source and javadoc in Eclipse -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.8</version>
				<configuration>
					<wtpversion>2.0</wtpversion>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>true</downloadJavadocs>
				</configuration>
			</plugin>
			
			<!-- Plugin to run and test through maven -->
			<plugin>
				<groupId>org.mortbay.jetty</groupId>
				<artifactId>maven-jetty-plugin</artifactId>
				<version>6.1.25</version>
				<configuration>
					<scanIntervalSeconds>3</scanIntervalSeconds>
				</configuration>
			</plugin>

			<!-- Ensures we are compiling at 1.8 level  -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.5.1</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<version>2.7.0</version>
				<configuration>
                    <mainClass>nz.devcentre.gravity.Startup</mainClass>
                    <layout>ZIP</layout>
                    <requiresUnpack>
						<dependency>
							<groupId>org.python</groupId>
							<artifactId>jython</artifactId>
						</dependency>
					</requiresUnpack>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M5</version>
				<configuration>
					<testFailureIgnore>true</testFailureIgnore>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>pl.project13.maven</groupId>
				<artifactId>git-commit-id-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>get-the-git-infos</id>
						<goals>
							<goal>revision</goal>
						</goals>
						<phase>initialize</phase>
					</execution>
					<execution>
						<id>validate-the-git-infos</id>
						<goals>
							<goal>validateRevision</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<failOnNoGitDirectory>false</failOnNoGitDirectory>
					<generateGitPropertiesFile>false</generateGitPropertiesFile>
					<runOnlyOnce>true</runOnlyOnce>
				</configuration>
			</plugin>
		</plugins>
		<resources>
        	<resource>
          		<directory>src/main/resources</directory>
          		<filtering>true</filtering>
        	</resource>
      	</resources>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.3</version>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>2.7</version>
				<configuration>
					<dependencyLocationsEnabled>false</dependencyLocationsEnabled>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>2.7</version>
				<configuration>
					<formats>
						<format>html</format>
						<format>xml</format>
					</formats>
				</configuration>
			</plugin>
		</plugins>
	</reporting>
</project>
