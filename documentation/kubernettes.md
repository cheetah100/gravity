# Kubernettes and GCloud


## Log into Account

First you need to select the project to use. In order to do this first list the projects available:

```
gcloud projects list
```

Then you can switch accounts with

```
gcloud config set project <project_id>
```

Once you have set the project you can find and set the cluster. First list the clusters:

```
gcloud container clusters list
```

The following commands log into a cluster 

```
gcloud container clusters get-credentials <cluster_id> --zone <zone>
```

List Kubernetes Contexts

```
kubectl config get-contexts
```


Switch Kubernetes context to that cluster.

```
kubectl config use-context <context>
```

## Create a Static IP Address using gcloud
The following command creates a static IP address which can be referenced when setting up Ingress on GCP GKE.

```
gcloud compute addresses create devcentre-ingress-ip --global
gcloud compute addresses describe devcentre-ingress-ip --global --format='value(address)
gcloud compute addresses list
```

## Create Secret with Certificates

The following commands create and apply a SSL certificate

```
kubectl create secret tls devcentre-tls --cert ~/cert/STAR_devcentre_nz.crt --key ~/cert/devcentre.org.key --dry-run -oyaml > k8s/devcentre-secret.yaml
kubectl apply -f k8s/devcentre-secret.yaml
```

## Create Service
The following command applies a service definition. See file in k8s.

```
kubectl apply -f k8s/service.yaml
kubectl get services
```

## Create Ingress
The following commands apply a Ingress, and then describe the ingress.

```
kubectl apply -f k8s/ingress.yaml
kubectl describe ingress
```

## Get Pods
The following command lists active pods in the current cluster.

```
kubectl get pods
```

## Get Nodes
The following command lists active kubernettes nodes. 
A Node is a worker machine in Kubernetes and may be either a virtual or a physical machine, depending on the cluster. Each Node is managed by the control plane. A Node can have multiple pods, and the Kubernetes control plane automatically handles scheduling the pods across the Nodes in the cluster.

```
kubectl get nodes --output wide
kubectl get nodes
```

## Update GCloud
How to update gcloud components (onlt this won't work)

```
gcloud components update
```



