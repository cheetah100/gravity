# Credential Controller

## Methods

### Basic Authentication
* method: basic
* identifier: username
* secret: password

### Bearer Authentication (bearer)
* method: bearer
* identifier: value

### OAuth2 Authentication 
* method: oauth2
* resource: url to oauth2
* requestMethod: POST
* identifier: client_id
* secret: client_secret

### OAuth2 Basic Authentication
* method: oauth2_basic
* resource: url to oauth2
* requestMethod: POST
* identifier: client_id
* secret: client_secret

### OAuth JWT Token Authentication
* method: oauth2_jwt
* resource: url to oauth2
* requestMethod: POST
* identifier: issuer
* secret: secret
* responseFields: jwtSubject, jwtName

### OAuth SAML Authentication
* method: oauth2_saml
* resource: url to oauth2
* requestMethod: POST
* identifier: samlUserId
* secret: samlUserPassword
* responseFields: samlTokenRequestBody
