# Ingress with SSL

These instructions relate to setting up Ingress to a GCP Google Kubernetes Engine cluster with SSL, with HTTP
being redirected to SSL.

## Setting up FrontEnd

Before you can configure a Ingress you will need to create a FrontEnd resource that will configure the ability to
redirect HTTP to SSL. This resource is named. The name will be used in the Ingress Defintion.

```
apiVersion: networking.gke.io/v1beta1
kind: FrontendConfig
metadata:
  name: example-frontend-config
spec:
  redirectToHttps:
    enabled: true
    responseCodeName: MOVED_PERMANENTLY_DEFAULT
```

## Setting up a Static IP Address

Also before configuring the Ingress you will need a Static IP Address. In the below command a new static IP address
is obtained and the name "devcentre-ingress-ip" specified.

```
gcloud compute addresses create devcentre-ingress-ip --global
```

## SSL Certificates

There are two ways to configure SSL certificates, either using a certificate you have externally obtained, or using
a managed certificate from Google.

### Managed Certificates

To use a managed certificate you must first create one. The below file "managed_cert.yaml" can be created with the
following content:

```
apiVersion: networking.gke.io/v1
kind: ManagedCertificate
metadata:
  name: myproject-cert
spec:
  domains:
    - myproject.com
```

The apply the file with the following command:

```
kubectl apply -f k8s/managed_cert.yaml
```

The Ingress needs to have the following attribute added:
```
networking.gke.io/managed-certificates: myproject-cert
```

### External Certificates

You can also use your own certificates via another mechanism.

The first step is to create a secret file. You will need to have access to the certificate and the private key.
```
kubectl create secret tls example-tls --cert ~/cert/example.crt --key ~/cert/example.key --dry-run -oyaml > k8s/secret.yaml
```

Second step is to apply this into your cluster:
```
kubectl apply -f k8s/secret.yaml
```

Finally you need to include the following in your Ingress under the spec:
```
spec:
  tls:
  - secretName: example-tls
```

## Ingress File

Now that you have configured all the dependencies you can upload a Ingress configuration that uses them.
This particular example configuration uses an external certificate.

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  annotations:
    kubernetes.io/ingress.global-static-ip-name: "example-ingress-ip"
    kubernetes.io/ingress.class: "gce"
    networking.gke.io/v1beta1.FrontendConfig: example-frontend-config
spec:
  tls:
  - secretName: example-tls
  defaultBackend:
    service:
      name: gravity-ui-service
      port:
        number: 8080
  rules:
  - http:
      paths:
      - path: /api
        pathType: Prefix
        backend:
          service:
            name: gravity-service
            port:
              number: 8080
```

```
kubectl apply -f k8s/ingress.yaml
```
