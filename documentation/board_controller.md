# Board Controller

## ID

Board Identifier - used for when referencing the Board in the API

## Version

Version is a unique identifier which is updated every time the board is modified.

## Name

Name is the label for the Board which is displayed.

When creating a board the Name will be used to generate the ID based on the lower case Name without spaces or other
invalid characters.

## Description

Optional description.

## Board Type

The Board Type segements boards into various flavours.

* **OBJECT** - Standard Data, used for a majority of internal boards.
* **OPTIONLIST** - Static Data, typically with a small number of cards.
* **PROCESS** - Process Board, typically used for process boards.
* **REMOTE** - A remote source of data, including external database tables.

## Key Mechanism

* **NONE** - the ID must be supplied by the caller.
* **GENERATOR** - a sequential id is generated.
* **UUID** - a uuid is a randomly generated unique identifier.
* **TITLE** - a id is generated from the title field, which is supplied.

## Title Field

This specifies the field which should be used as the title for cards.

## Permissions

Permissions are a map that specifies users or teams, and their related permission.

**example permissions**

	{ 
		"jim_smith" : "ADMIN", 
		"accounts" : "WRITE",
		"observor" : "READ"
	}


* **ADMIN** - Admin access provides the ability to modify configurations.
* **WRITE** - Write access provides the ability to write and read data.
* **READ** - Read access provides the ability to read data.


## Phases

Gravity is a business process orchestration engine. The phases in a board allow us to control the process flow. Cards
represent a process instance which is moving through a process. Phases are stages that occur through the process.

* **ID** - The ID for the phase.
* **Name** - The Label for the phase
* **Description** - Optional description of the phase
* **Index** - An index number which determines the order that phases occur.
* **Invisible** - Boolean that specifies whether cards in this phase are hidden.
* **Transitions** - A optional list of phases that cards in this phase can move to.

**example phases**

    "proposed": {
      "id": "proposed",
      "metadata": null,
      "name": "Proposed",
      "description": null,
      "index": 1,
      "cards": null,
      "invisible": false,
      "transitions": null
    },
    "complete": {
      "id": "complete",
      "metadata": null,
      "name": "Complete",
      "description": null,
      "index": 2,
      "cards": null,
      "invisible": false,
      "transitions": null
    },
    "archived": {
      "id": "archived",
      "metadata": null,
      "name": "Archived",
      "description": null,
      "index": 3,
      "cards": null,
      "invisible": true,
      "transitions": null
    }

## Filters

## Views

## Fields

**Properties**

* **ID** - The ID of the field.
* **Name** - the Name of the field
* **Description** - an optional description of the field.
* **Label** - the name displayed to the user.
* **Validation** - a regex validation to apply.
* **Type** - the data type (see below)
* **UI** - a map of UI configuration. Is open ended to support UI requirements.
* **OptionList** - name of a board which is used for selecting the value. Similar to a referential constraint.
* **OptionListFilter** - name of a filter to apply to the OptionsList Board when selecting the list to display.
* **Required** - Boolean which indicates whether the field is required in a card.
* **RequiredPhase** - ID of a phase if there a phase where this field becomes required.
* **Immutable** - Boolean that determines whether this card can be modified after initial creation?
* **Indexed** - Boolean to indicate whether this field should be indexed in Mongo.

**Data Types**

* STRING
* BOOLEAN
* NUMBER
* DATE
* LIST
* MAP

**Example JSON for fields:**

	"fields": {
    "title": {
      "id": "title",
      "metadata": null,
      "name": "title",
      "description": "Project Title",
      "label": "Title",
      "validation": null,
      "type": "STRING",
      "ui": null,
      "optionlist": null,
      "optionlistfilter": null,
      "required": true,
      "requiredPhase": null,
      "immutable": false,
      "indexed": false,
      "referenceField": false
    },
    "description": {
      "id": "description",
      "metadata": null,
      "name": "description",
      "description": "Description of Project",
      "label": "Description",
      "validation": null,
      "type": "STRING",
      "ui": null,
      "optionlist": null,
      "optionlistfilter": null,
      "required": false,
      "requiredPhase": null,
      "immutable": false,
      "indexed": false,
      "referenceField": false
    }
  }
