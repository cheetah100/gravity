# Rule Controller

## Types of Rule

There are five different types of Rule. 

### Compulsory Rule

A compulsory rule will run every time a card is modified. It does not matter if the rule has been executed before.
This kind of rule must be used with care as it is possible to create a rule which updates data on a card continuously.
A compulsory rule will evaluate the automation conditions of the rule and execute the action chain if they are met.
Compulsory rules are usually used to generate additional data from the data provided by users when data is updated.
There is no concept of a compulsory rule being 'done', as they are triggered every time data changes.

### Task Rule

Similar to the compulsory rule a task rule will run when data is modified on a card. Task cards first evaluate whether
they should be executed at all using the task conditions. If task conditions are met a task will be created for the
card. This indicates that the task is expected to run at some point. The automation conditions are then evaluated, and
if they are met the action chain is run.

### Validation Rule

Validation rules run before a data modification is made to a card. Where Compulsory and Task rules run after a modification.
They are used to implement custom validation outside of basic type and foreign key validation that is part of the data
schema. The action chain is run, and from this the result determines whether validation has passed.

### Scheduled Rule



### On Demand Rule


## Conditions

Each rule can have various conditions which must be met before automation is triggered.

### Automation Conditions

### Task Conditions

### Completion Conditions

### Validation Conditions

###

### Actions

