# Gravity API

The Gravity API is broken up into separate controllers which deal with separate aspects or objects.

## Access Control Policy Controller

Deprecated - do not use

## Application Controller

Under Development - do not use

## Basic Error Controller

This controller is exposed by Swagger-UI, and is not part of the Gravity API Proper

## Board Controller

The Board Controller is the primary controller for configuration of Boards. 

## Card Alert Controller

The Card Alert Controller deals with alerts raised against a Card. This occurs when there is an exception during
the processing of a rule. Users can also manually raise alerts through this API. An active alert will terminate
automation on a card until dismissed.

## Card Comment Controller

The Card Comment Controller allows users to save and retrieve comments on a Card. 

## Card Controller

The Card Controller is the primary API for access to data within boards.

## Card History Controller

The Card History Controller is the API that provides access to the audit trail or history of card modifications.

## Card Task Controller

The Card Task Controller is the API that allows users to interact with Tasks on a card. A Task is a job which needs
to be completed. Often a Task is automated, but for whatever reason needs to be rerun. Users can revert a task so
that the automation will rerun the task. Tasks can also be assigned to human users or manually marked as completed.

## Card Watch Controller

The Card Watch Controller allows users to set up a watch on a specific card, and to query for notifications received
as a result of a watch.

## Credential Controller

The Credential Controller stores sensitive credentials for use in automation rules or integrations. The credential
stores the type of credential so that it can only be used for a specific resource. Credentials are stored using
encryption, and neither the encrypted data or the clear text data is accessible through the API once stored.

## Dashboard Controller

The Dashboard Controller was able to store dashboard configurations which specified widgets to display on the dashboard.
The Widgets would be configured separately. The point of a dashboard would be to give users the ability to set up their
own dashboard page with the specific widgets they wanted to see. Dashboards could be shared, so that the author of a
dashboard could share it with the rest of a team.

## Explorer Controller

The Explorer Controller is a Administrator Only tool that allows direct access to Mongo, along with ER Diagrams of 
boards. Because this API can evade access restrictions it is not available to non administrators.

## Export Controller

The Export Controller was for exporting data in Microsoft Excel format. This was developed for a relatively narrow use
case.

## Filter Controller

The Filter Controller is for setting up filters on boards. A filter will restrict the cards that are returned in a
query based on the filter criteria.

## GraphQL Controller

The GraphQL Controller is different to other controllers, in that the GraphQL Controller exposes a GraphQL endpoint
that can be used to query data across multiple boards and link it together in a format specified by the GraphQL
Query. This can be very powerful, but the implementation is slow because it will generate multiple requests for
individual records.

## History Controller

The History Controller allows access to the audit trail data. It is an Administrators Only tool to query what actions
a user has performed and what data they have stored.

## Import Controller

The Import Controller is the other side of the Export Controller, allowing import of Microsoft Excel files into
Gravity Boards. While possible this is the least preferable mechanism to load data into Gravity. The process involves
initial submission, followed by processing. The API user can then query for the results of the import.

## Integration Controller

The Integration Controller allows the user to configure integration with systems that provide incoming data. This might
be a JMS source which Gravity will consume messages from, or a call to a HTTP endpoint to periodically collect data,
or a periodic connection to an external database. Many forms of connector are supported. Records collected are sent
through automation to execute actions rather than going directly to storage.

## List Controller

Deprecated - do not use. Was used to obtain simplified lists.

## Phase Controller

The Phase Controller is primarily used to modify the configuration of phases within a board.

## Pivot Table Controller

Deprecated - do not use. This was the first mechanism for creating a pivot table from data inside a Board. This has been
replaced with Transforms. See the Transform Controller below.

## Public Controller

The Public Controller API is the only controller which has no user permission security. It is used for the login
which must be able to service calls which have not yet logged in. It also has several user related calls for
password resets and signup. There is also a ping call for diagnostics and a bootstrap call to initialize the
Gravity database on initial run.

## Query Controller

In Development

## Resource Controller

The Resource Controller API is used to manage resources inside Gravity Boards. A Resource is simply text which
Gravity uses as part of automation. It could be Javascript which is run, a template for an email message, the
body of a XML message, or any other text. These resources are typically used within rules.

## Rule Controller

The Rule Controller API is used to configure rules on Boards. The most common forms of rules run when data is modified.
A COMPULSORY rule will be evaluated every time data is modified, and run if the automation conditions are met.
A TASK rule will be evaluated when data is modified, and run only if it has not run before. Once it runs it is complete.
A VALIDATION rule runs when data is added or modified, and validates the new data. If not valid it blocks modification.
A SCHEDULED rule will run at a scheduled time.
A ONDEMAND rule will run only when manually activated.

## System Controller

The System Controller API exposes system related information. It is only available to administrators.
In addition to information there are some API which can be used to purge the automation backlog.

## Team Controller

The Team Controller groups users into teams. These teams can then be given permission to Boards, Credentials etc.

## Transform Controller

The Transform Controller API is used to define data transformations, through a data transformation chain. This can be
used to create pivot tables or any number of other summary tables for visualizations.

## User Controller

The User Controller API is used to maintain users. Signup can also occur through the Public Controller, however
the User Controller gives more direct access to the user records, and is therefore available to administrators.

## User Preferences Controller

The User Preferences API is used to store data specific to the preferences of the user. This API contains calls
for the current user, along with calls that can be used by administrators to examine or modify preferences.

## View Controller

The View Controller API allows users to store views against a Board. A view is a subset of fields that the user
needs wants to see. Typically this is used to narrow down a data table or visualization to just the fields that
need to be seen.
