# Docker Push
The following docker commands are used to push an image to Docker.io

This is the first step to deploying the image into a target environment such as a Kubernettes Cluster 
in GCP or AWS.

## Tagging the image
```
docker tag devcentre/gravity cheetah100ph/gravity:5.00
```

## List Images
```
docker image ls
```

## Log In, and Push Image to Docker.io

```
docker login
docker push cheetah100ph/gravity:5.00
```

## ??

```
docker build -t vuejs-cookbook/dockerize-vuejs-app .
```